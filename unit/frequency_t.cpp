#include "unit/frequency_t.hpp"


namespace unit
{		
	frequency_t operator "" _THz (long double val)
	{
		return frequency_t(val*1e12l);
	}
	
	frequency_t operator "" _GHz (long double val)
	{
		return frequency_t(val * 1e9l);
	}
	
	frequency_t operator "" _MHz (long double val)
	{
		return frequency_t(val * 1e6l);
	}
		
	frequency_t operator "" _kHz (long double val)
	{
		return frequency_t(val * 1e3l);
	}	
		
	frequency_t operator "" _Hz (long double val)
	{
		return frequency_t(val);
	}
	
	frequency_t operator "" _mHz (long double val)
	{
		return frequency_t(val*1e-3l);
	}
	
	frequency_t operator "" _uHz (long double val)
	{
		return frequency_t(val * 1e-6l);
	}
	
	frequency_t operator "" _nHz (long double val)
	{
		return frequency_t(val * 1e-9l);
	}
	
	frequency_t operator "" _pHz (long double val)
	{
		return frequency_t(val * 1e-12l);
	}
	
	frequency_t operator "" _fHz (long double val)
	{
		return frequency_t(val * 1e-15l);
	}

	frequency_t operator "" _THz (unsigned long long val)
	{
		return frequency_t(val*1e12l);
	}
	
	frequency_t operator "" _GHz (unsigned long long val)
	{
		return frequency_t(val * 1e9l);
	}
	
	frequency_t operator "" _MHz (unsigned long long val)
	{
		return frequency_t(val * 1e6l);
	}
		
	frequency_t operator "" _kHz (unsigned long long val)
	{
		return frequency_t(val * 1e3l);
	}	
		
	frequency_t operator "" _Hz (unsigned long long val)
	{
		return frequency_t(val);
	}
	
	frequency_t operator "" _mHz (unsigned long long val)
	{
		return frequency_t(val*1e-3l);
	}
	
	frequency_t operator "" _uHz (unsigned long long val)
	{
		return frequency_t(val * 1e-6l);
	}
	
	frequency_t operator "" _nHz (unsigned long long val)
	{
		return frequency_t(val * 1e-9l);
	}
	
	frequency_t operator "" _pHz (unsigned long long val)
	{
		return frequency_t(val * 1e-12l);
	}
	
	frequency_t operator "" _fHz (unsigned long long val)
	{
		return frequency_t(val * 1e-15l);
	}
}

