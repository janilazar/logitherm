#include "unit/power_t.hpp"

namespace unit
{		
	power_t operator "" _TW (long double val)
	{
		return power_t(val*1e12l);
	}
	
	power_t operator "" _GW (long double val)
	{
		return power_t(val * 1e9l);
	}
	
	power_t operator "" _MW (long double val)
	{
		return power_t(val * 1e6l);
	}
		
	power_t operator "" _kW (long double val)
	{
		return power_t(val * 1e3l);
	}	
		
	power_t operator "" _W (long double val)
	{
		return power_t(val);
	}
	
	power_t operator "" _mW (long double val)
	{
		return power_t(val*1e-3l);
	}
	
	power_t operator "" _uW (long double val)
	{
		return power_t(val * 1e-6l);
	}
	
	power_t operator "" _nW (long double val)
	{
		return power_t(val * 1e-9l);
	}
	
	power_t operator "" _pW (long double val)
	{
		return power_t(val * 1e-12l);
	}
	
	power_t operator "" _fW (long double val)
	{
		return power_t(val * 1e-15l);
	}

	power_t operator "" _TW (unsigned long long val)
	{
		return power_t(val*1e12l);
	}
	
	power_t operator "" _GW (unsigned long long val)
	{
		return power_t(val * 1e9l);
	}
	
	power_t operator "" _MW (unsigned long long val)
	{
		return power_t(val * 1e6l);
	}
		
	power_t operator "" _kW (unsigned long long val)
	{
		return power_t(val * 1e3l);
	}	
		
	power_t operator "" _W (unsigned long long val)
	{
		return power_t(val);
	}
	
	power_t operator "" _mW (unsigned long long val)
	{
		return power_t(val*1e-3l);
	}
	
	power_t operator "" _uW (unsigned long long val)
	{
		return power_t(val * 1e-6l);
	}
	
	power_t operator "" _nW (unsigned long long val)
	{
		return power_t(val * 1e-9l);
	}
	
	power_t operator "" _pW (unsigned long long val)
	{
		return power_t(val * 1e-12l);
	}
	
	power_t operator "" _fW (unsigned long long val)
	{
		return power_t(val * 1e-15l);
	}
}

