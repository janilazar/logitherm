#include "unit/area_t.hpp"

namespace unit
{	
	area_t operator "" _Tm2 (long double val)
	{
		return area_t(val*1e24l);
	}
	
	area_t operator "" _Gm2 (long double val)
	{
		return area_t(val * 1e18l);
	}
	
	area_t operator "" _Mm2 (long double val)
	{
		return area_t(val * 1e12l);
	}
		
	area_t operator "" _km2 (long double val)
	{
		return area_t(val * 1e6l);
	}	
		
	area_t operator "" _m2 (long double val)
	{
		return area_t(val);
	}

	area_t operator "" _cm2 (long double val)
	{
		return area_t(val*1e-4l);
	}
	
	area_t operator "" _mm2 (long double val)
	{
		return area_t(val*1e-6l);
	}
	
	area_t operator "" _um2 (long double val)
	{
		return area_t(val * 1e-12l);
	}
	
	area_t operator "" _nm2 (long double val)
	{
		return area_t(val * 1e-18l);
	}
	
	area_t operator "" _pm2 (long double val)
	{
		return area_t(val * 1e-24l);
	}
	
	area_t operator "" _fm2 (long double val)
	{
		return area_t(val * 1e-30l);
	}

	area_t operator "" _Tm2 (unsigned long long val)
	{
		return area_t(val*1e24l);
	}
	
	area_t operator "" _Gm2 (unsigned long long val)
	{
		return area_t(val * 1e18l);
	}
	
	area_t operator "" _Mm2 (unsigned long long val)
	{
		return area_t(val * 1e12l);
	}
		
	area_t operator "" _km2 (unsigned long long val)
	{
		return area_t(val * 1e6l);
	}	
		
	area_t operator "" _m2 (unsigned long long val)
	{
		return area_t(val);
	}

	area_t operator "" _cm2 (unsigned long long val)
	{
		return area_t(val*1e-4l);
	}
	
	area_t operator "" _mm2 (unsigned long long val)
	{
		return area_t(val*1e-6l);
	}
	
	area_t operator "" _um2 (unsigned long long val)
	{
		return area_t(val * 1e-12l);
	}
	
	area_t operator "" _nm2 (unsigned long long val)
	{
		return area_t(val * 1e-18l);
	}
	
	area_t operator "" _pm2 (unsigned long long val)
	{
		return area_t(val * 1e-24l);
	}
	
	area_t operator "" _fm2 (unsigned long long val)
	{
		return area_t(val * 1e-30l);
	}
}
