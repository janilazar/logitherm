#include "unit/energy_t.hpp"

namespace unit
{	
	energy_t operator "" _TJ (long double val)
	{
		return energy_t(val*1e12l);
	}

	energy_t operator "" _GJ (long double val)
	{
		return energy_t(val * 1e9l);
	}

	energy_t operator "" _MJ (long double val)
	{
		return energy_t(val * 1e6l);
	}
		
	energy_t operator "" _kJ (long double val)
	{
		return energy_t(val * 1e3l);
	}	
		
	energy_t operator "" _J (long double val)
	{
		return energy_t(val);
	}

	energy_t operator "" _mJ (long double val)
	{
		return energy_t(val*1e-3l);
	}

	energy_t operator "" _uJ (long double val)
	{
		return energy_t(val * 1e-6l);
	}

	energy_t operator "" _nJ (long double val)
	{
		return energy_t(val * 1e-9l);
	}

	energy_t operator "" _pJ (long double val)
	{
		return energy_t(val * 1e-12l);
	}

	energy_t operator "" _fJ (long double val)
	{
		return energy_t(val * 1e-15l);
	}

	energy_t operator "" _TJ (unsigned long long val)
	{
		return energy_t(val*1e12l);
	}

	energy_t operator "" _GJ (unsigned long long val)
	{
		return energy_t(val * 1e9l);
	}

	energy_t operator "" _MJ (unsigned long long val)
	{
		return energy_t(val * 1e6l);
	}
		
	energy_t operator "" _kJ (unsigned long long val)
	{
		return energy_t(val * 1e3l);
	}	
		
	energy_t operator "" _J (unsigned long long val)
	{
		return energy_t(val);
	}

	energy_t operator "" _mJ (unsigned long long val)
	{
		return energy_t(val*1e-3l);
	}

	energy_t operator "" _uJ (unsigned long long val)
	{
		return energy_t(val * 1e-6l);
	}

	energy_t operator "" _nJ (unsigned long long val)
	{
		return energy_t(val * 1e-9l);
	}

	energy_t operator "" _pJ (unsigned long long val)
	{
		return energy_t(val * 1e-12l);
	}

	energy_t operator "" _fJ (unsigned long long val)
	{
		return energy_t(val * 1e-15l);
	}
}
