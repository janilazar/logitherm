#ifndef _CURRENT_T_
#define _CURRENT_T_

#include "unit/unit_t.hpp"

namespace unit
{
	current_t operator "" _TA (long double val);
	current_t operator "" _GA (long double val);
	current_t operator "" _MA (long double val);
	current_t operator "" _kA (long double val);
	current_t operator "" _A (long double val);
	current_t operator "" _mA (long double val);
	current_t operator "" _uA (long double val);
	current_t operator "" _nA (long double val);
	current_t operator "" _pA (long double val);
	current_t operator "" _fA (long double val);

	current_t operator "" _TA (unsigned long long val);
	current_t operator "" _GA (unsigned long long val);
	current_t operator "" _MA (unsigned long long val);
	current_t operator "" _kA (unsigned long long val);
	current_t operator "" _A (unsigned long long val);
	current_t operator "" _mA (unsigned long long val);
	current_t operator "" _uA (unsigned long long val);
	current_t operator "" _nA (unsigned long long val);
	current_t operator "" _pA (unsigned long long val);
	current_t operator "" _fA (unsigned long long val);
}

using unit::operator "" _TA;
using unit::operator "" _GA;
using unit::operator "" _MA;
using unit::operator "" _kA;
using unit::operator "" _A; 
using unit::operator "" _mA;
using unit::operator "" _uA;
using unit::operator "" _nA;
using unit::operator "" _pA;
using unit::operator "" _fA;

#endif //_CURRENT_T_
