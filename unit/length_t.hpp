#ifndef _LENGTH_T_
#define _LENGTH_T_

#include "unit/unit_t.hpp"

namespace unit
{
	template<>
	class unit_t<0,1,0,0,0>
	{
		public:
			long double magnitude;
			unsigned long long in_picometer;

		private:
			constexpr explicit unit_t(unsigned long long val)
			:
				magnitude(val*1e-9l),
				in_picometer(val)
			{}
		
		public:
			constexpr unit_t()
			:
				magnitude(0.0l),
				in_picometer(0)
			{}
			
			constexpr explicit unit_t(long double val)
			:
				magnitude(val),
				in_picometer(val*1e9l)
			{}

			constexpr explicit unit_t(double val)
			:
				magnitude(static_cast<long double>(val)),
				in_picometer(val*1e9l)
			{}
		
			unit_t& operator= (const unit_t& rhs)
			{
				this->magnitude = rhs.magnitude;
				this->in_picometer = rhs.in_picometer;
				return (*this);
			}
			
			bool operator < (const unit_t& rhs) const
			{
				return this->in_picometer < rhs.in_picometer;
			}
			
			bool operator > (const unit_t& rhs) const
			{
				return this->in_picometer > rhs.in_picometer;
			}

			bool operator <= (const unit_t& rhs) const
			{
				return this->in_picometer <= rhs.in_picometer;
			}
			
			bool operator >= (const unit_t& rhs) const
			{
				return this->in_picometer >= rhs.in_picometer;
			}
			
			unit_t operator+ (const unit_t& rhs) const
			{
				return unit_t(this->in_picometer + rhs.in_picometer);
			}
			
			unit_t operator- (const unit_t& rhs) const
			{
				return unit_t(this->in_picometer - rhs.in_picometer);
			}

			unit_t operator- () const
			{
				return unit_t(-this->magnitude);
			}
			
			unit_t operator* (long double rhs) const
			{
				return unit_t(this->magnitude * rhs);
			}

			template<int t, int l, int T, int Q, int V>
			unit_t<0+t,1+l,0+T,0+Q,0+V> operator* (const unit_t<t,l,T,Q,V>& rhs) const
			{
				return unit_t<0+t,1+l,0+T,0+Q,0+V>(this->magnitude * rhs.magnitude);
			}

			double operator/ (const unit_t& rhs) const
			{
				double result = static_cast<double>(this->in_picometer / rhs.in_picometer);
				return result;
			}

			template<int t, int l, int T, int Q, int V>
			unit_t<0-t,1-l,0-T,0-Q,0-V> operator/ (const unit_t<t,l,T,Q,V>& rhs) const
			{
				return unit_t<0-t,1-l,0-T,0-Q,0-V>(this->magnitude / rhs.magnitude);
			}

			unit_t operator/ (long double rhs) const
			{
				return unit_t(static_cast<long double>((this->in_picometer) / rhs)*1e-9l);
			}

			unit_t operator/ (unsigned long long rhs) const
			{
				return unit_t(this->in_picometer / rhs);
			}

			unit_t operator/ (size_t rhs) const
			{
				return unit_t(this->in_picometer / rhs);
			}
			
			unit_t& operator += (const unit_t& rhs)
			{
				(*this) = (*this) + rhs;
				return (*this);
			}
			
			unit_t& operator -= (const unit_t& rhs)
			{
				(*this) = (*this) - rhs;
				return (*this);
			}
			
			template<int t, int l, int T, int Q, int V>
			friend std::ostream& operator << (std::ostream& os, const unit_t<t,l,T,Q,V>& rhs);
			
			explicit operator double() const
			{
				return magnitude;
			}

			explicit operator float() const
			{
				return magnitude;
			}

			//explicit operator int() const
			//{
			//	std::stringstream convert;
			//	convert << magnitude;
			//	int result;
			//	convert >> result;
			//	return result;
			//}

			explicit operator size_t() const
			{
				return static_cast<size_t>(magnitude);
			}
	};

	length_t operator "" _Tm (long double val);
	length_t operator "" _Gm (long double val);
	length_t operator "" _Mm (long double val);
	length_t operator "" _km (long double val);
	length_t operator "" _m (long double val);
	length_t operator "" _mm (long double val);
	length_t operator "" _um (long double val);
	length_t operator "" _nm (long double val);
	length_t operator "" _pm (long double val);
	length_t operator "" _fm (long double val);

	length_t operator "" _Tm (unsigned long long val);
	length_t operator "" _Gm (unsigned long long val);
	length_t operator "" _Mm (unsigned long long val);
	length_t operator "" _km (unsigned long long val);
	length_t operator "" _m (unsigned long long val);
	length_t operator "" _mm (unsigned long long val);
	length_t operator "" _um (unsigned long long val);
	length_t operator "" _nm (unsigned long long val);
	length_t operator "" _pm (unsigned long long val);
	length_t operator "" _fm (unsigned long long val);
}

using unit::operator "" _Tm;
using unit::operator "" _Gm;
using unit::operator "" _Mm;
using unit::operator "" _km;
using unit::operator "" _m;
using unit::operator "" _mm;
using unit::operator "" _um;
using unit::operator "" _nm;
using unit::operator "" _pm;
using unit::operator "" _fm;

#endif //_LENGTH_T_
