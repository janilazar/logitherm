#include "unit/temperature_t.hpp"

namespace unit
{
	temperature_t operator "" _TK (long double val)
	{
		return temperature_t(val*1e12l);
	}

	temperature_t  operator "" _GK (long double val)
	{
		return temperature_t(val * 1e9l);
	}

	temperature_t operator "" _MK (long double val)
	{
		return temperature_t(val * 1e6l);
	}
		
	temperature_t operator "" _kK (long double val)
	{
		return temperature_t(val * 1e3l);
	}	
		
	temperature_t operator "" _K (long double val)
	{
		return temperature_t(val);
	}

	temperature_t operator "" _mK (long double val)
	{
		return temperature_t(val*1e-3l);
	}

	temperature_t operator "" _uK (long double val)
	{
		return temperature_t(val * 1e-6l);
	}

	temperature_t operator "" _nK (long double val)
	{
		return temperature_t(val * 1e-9l);
	}

	temperature_t operator "" _pK (long double val)
	{
		return temperature_t(val * 1e-12l);
	}

	temperature_t operator "" _fK (long double val)
	{
		return temperature_t(val * 1e-15l);
	}

	temperature_t operator "" _TK (unsigned long long val)
	{
		return temperature_t(val*1e12l);
	}

	temperature_t  operator "" _GK (unsigned long long val)
	{
		return temperature_t(val * 1e9l);
	}

	temperature_t operator "" _MK (unsigned long long val)
	{
		return temperature_t(val * 1e6l);
	}
		
	temperature_t operator "" _kK (unsigned long long val)
	{
		return temperature_t(val * 1e3l);
	}	
		
	temperature_t operator "" _K (unsigned long long val)
	{
		return temperature_t(val);
	}

	temperature_t operator "" _mK (unsigned long long val)
	{
		return temperature_t(val*1e-3l);
	}

	temperature_t operator "" _uK (unsigned long long val)
	{
		return temperature_t(val * 1e-6l);
	}

	temperature_t operator "" _nK (unsigned long long val)
	{
		return temperature_t(val * 1e-9l);
	}

	temperature_t operator "" _pK (unsigned long long val)
	{
		return temperature_t(val * 1e-12l);
	}

	temperature_t operator "" _fK (unsigned long long val)
	{
		return temperature_t(val * 1e-15l);
	}
	
	temperature_t operator "" _celsius (long double val)
	{
		return temperature_t(val+273.15l);
	}

	temperature_t operator "" _celsius (unsigned long long val)
	{
		return temperature_t(static_cast<long double>(val)+273.15l);
	}
}

