#ifndef _AREA_T_
#define _AREA_T_

#include "unit/unit_t.hpp"

namespace unit
{
	area_t operator "" _Tm2 (long double val);
	area_t operator "" _Gm2 (long double val);
	area_t operator "" _Mm2 (long double val);
	area_t operator "" _km2 (long double val);
	area_t operator "" _m2 (long double val);
	area_t operator "" _cm2 (long double val);
	area_t operator "" _mm2 (long double val);
	area_t operator "" _um2 (long double val);
	area_t operator "" _nm2 (long double val);
	area_t operator "" _pm2 (long double val);
	area_t operator "" _fm2 (long double val);
	
	area_t operator "" _Tm2 (unsigned long long val);
	area_t operator "" _Gm2 (unsigned long long val);
	area_t operator "" _Mm2 (unsigned long long val);
	area_t operator "" _km2 (unsigned long long val);
	area_t operator "" _m2 (unsigned long long val);
	area_t operator "" _cm2 (unsigned long long val);
	area_t operator "" _mm2 (unsigned long long val);
	area_t operator "" _um2 (unsigned long long val);
	area_t operator "" _nm2 (unsigned long long val);
	area_t operator "" _pm2 (unsigned long long val);
	area_t operator "" _fm2 (unsigned long long val);
}

using unit::operator "" _Tm2;
using unit::operator "" _Gm2;
using unit::operator "" _Mm2;
using unit::operator "" _km2;
using unit::operator "" _m2;
using unit::operator "" _cm2;
using unit::operator "" _mm2;
using unit::operator "" _um2;
using unit::operator "" _nm2;
using unit::operator "" _pm2;
using unit::operator "" _fm2;

#endif //_AREA_T_
