#ifndef _TIME_T_
#define _TIME_T_

#include "unit/unit_t.hpp"

namespace unit
{
	time_t operator "" _Ts (long double val);
	time_t operator "" _Gs (long double val);
	time_t operator "" _Ms (long double val);
	time_t operator "" _ks (long double val);
	time_t operator "" _s (long double val);
	time_t operator "" _ms (long double val);
	time_t operator "" _us (long double val);
	time_t operator "" _ns (long double val);
	time_t operator "" _ps (long double val);
	time_t operator "" _fs (long double val);

	time_t operator "" _Ts (unsigned long long val);
	time_t operator "" _Gs (unsigned long long val);
	time_t operator "" _Ms (unsigned long long val);
	time_t operator "" _ks (unsigned long long val);
	time_t operator "" _s (unsigned long long val);
	time_t operator "" _ms (unsigned long long val);
	time_t operator "" _us (unsigned long long val);
	time_t operator "" _ns (unsigned long long val);
	time_t operator "" _ps (unsigned long long val);
	time_t operator "" _fs (unsigned long long val);
}


using unit::operator "" _Ts;
using unit::operator "" _Gs;
using unit::operator "" _Ms;
using unit::operator "" _ks;
using unit::operator "" _s;
using unit::operator "" _ms;
using unit::operator "" _us;
using unit::operator "" _ns;
using unit::operator "" _ps;
using unit::operator "" _fs;

#endif //_TIME_T_
