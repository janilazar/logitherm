#include "unit/length_t.hpp"

namespace unit
{
	length_t operator "" _Tm (long double val)
	{
		return length_t(val*1e12l);
	}
	
	length_t operator "" _Gm (long double val)
	{
		return length_t(val * 1e9l);
	}
	
	length_t operator "" _Mm (long double val)
	{
		return length_t(val * 1e6l);
	}
		
	length_t operator "" _km (long double val)
	{
		return length_t(val * 1e3l);
	}	
		
	length_t operator "" _m (long double val)
	{
		return length_t(val);
	}
	
	length_t operator "" _mm (long double val)
	{
		return length_t(val*1e-3l);
	}
	
	length_t operator "" _um (long double val)
	{
		return length_t(val * 1e-6l);
	}
	
	length_t operator "" _nm (long double val)
	{
		return length_t(val * 1e-9l);
	}
	
	length_t operator "" _pm (long double val)
	{
		return length_t(val * 1e-12l);
	}
	
	length_t operator "" _fm (long double val)
	{
		return length_t(val * 1e-15l);
	}

	length_t operator "" _Tm (unsigned long long val)
	{
		return length_t(val*1e12l);
	}
	
	length_t operator "" _Gm (unsigned long long val)
	{
		return length_t(val * 1e9l);
	}
	
	length_t operator "" _Mm (unsigned long long val)
	{
		return length_t(val * 1e6l);
	}
		
	length_t operator "" _km (unsigned long long val)
	{
		return length_t(val * 1e3l);
	}	
		
	length_t operator "" _m (unsigned long long val)
	{
		return length_t(static_cast<long double>(val));
	}
	
	length_t operator "" _mm (unsigned long long val)
	{
		return length_t(val*1e-3l);
	}
	
	length_t operator "" _um (unsigned long long val)
	{
		return length_t(val * 1e-6l);
	}
	
	length_t operator "" _nm (unsigned long long val)
	{
		return length_t(val * 1e-9l);
	}
	
	length_t operator "" _pm (unsigned long long val)
	{
		return length_t(val * 1e-12l);
	}
	
	length_t operator "" _fm (unsigned long long val)
	{
		return length_t(val * 1e-15l);
	}
}
