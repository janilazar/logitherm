#ifndef _UNIT_T_
#define _UNIT_T_

#include <iostream>
#include <sstream>

namespace unit
{
	template<int time, int length, int temperature, int charge, int voltage>
	class unit_t;

	template<int time, int length, int temperature, int charge, int voltage>
	std::ostream& operator << (std::ostream& os, const unit::unit_t<time,length,temperature,charge,voltage>& rhs);

	// template<int t, int l, int T, int Q, int V>
	// unit_t<t,l,T,Q,V>& operator* (double scale, unit_t<t,l,T,Q,V>& rhs);

	// template<int t, int l, int T, int Q, int V>
	// unit_t<t,l,T,Q,V>& operator* (long double scale, unit_t<t,l,T,Q,V>& rhs);

	// template<int t, int l, int T, int Q, int V>
	// unit_t<t,l,T,Q,V> operator* (double scale, const unit_t<t,l,T,Q,V>& rhs);

	// template<int t, int l, int T, int Q, int V>
	// unit_t<t,l,T,Q,V> operator* (long double scale, const unit_t<t,l,T,Q,V>& rhs);

	// template<int t, int l, int T, int Q, int V>
	// unit_t<-t,-l,-T,-Q,-V> operator/ (double scale, const unit_t<t,l,T,Q,V>& rhs);

	// template<int t, int l, int T, int Q, int V>
	// unit_t<-t,-l,-T,-Q,-V> operator/ (long double scale, const unit_t<t,l,T,Q,V>& rhs);

	template<int time, int length, int temperature, int charge, int voltage>
	class unit_t
	{
		private:
			long double magnitude;
		
		public:
			unit_t()	:	magnitude(0.0l)
			{}
			
			explicit unit_t(long double val): magnitude(val)
			{}

			explicit operator long double() const
			{
				return magnitude;
			}

			explicit operator double() const
			{
				return magnitude;
			}

			explicit operator float() const
			{
				return magnitude;
			}
		
			unit_t& operator= (const unit_t& rhs)
			{
				this->magnitude = rhs.magnitude;
				return (*this);
			}
			
			bool operator < (const unit_t& rhs) const
			{
				return this->magnitude < rhs.magnitude;
			}
			
			bool operator > (const unit_t& rhs) const
			{
				return this->magnitude > rhs.magnitude;
			}

			bool operator <= (const unit_t& rhs) const
			{
				return this->magnitude <= rhs.magnitude;
			}
			
			bool operator >= (const unit_t& rhs) const
			{
				return this->magnitude >= rhs.magnitude;
			}
			
			unit_t operator+ (const unit_t& rhs) const
			{
				return unit_t(this->magnitude + rhs.magnitude);
			}
			
			unit_t operator- (const unit_t& rhs) const
			{
				return unit_t(this->magnitude - rhs.magnitude);
			}

			unit_t operator- () const
			{
				return unit_t(-this->magnitude);
			}
			
			unit_t operator* (long double rhs) const
			{
				return unit_t(this->magnitude * rhs);
			}

			unit_t operator* (double rhs) const
			{
				return unit_t(this->magnitude * rhs);
			}

			template<int t, int l, int T, int Q, int V>
			unit_t<time+t,length+l,temperature+T,charge+Q,voltage+V> operator* (const unit_t<t,l,T,Q,V>& rhs) const
			{
				return unit_t<time+t,length+l,temperature+T,charge+Q,voltage+V>(this->magnitude * static_cast<long double>(rhs));
			}

			double operator/ (const unit_t& rhs) const
			{
				return this->magnitude / rhs.magnitude;
			}

			template<int t, int l, int T, int Q, int V>
			unit_t<time-t,length-l,temperature-T,charge-Q,voltage-V> operator/ (const unit_t<t,l,T,Q,V>& rhs) const
			{
				return unit_t<time-t,length-l,temperature-T,charge-Q,voltage-V>(this->magnitude / static_cast<long double>(rhs));
			}

			unit_t operator/ (long double rhs) const
			{
				return unit_t(this->magnitude / rhs);
			}
			
			unit_t& operator += (const unit_t& rhs)
			{
				(*this) = (*this) + rhs;
				return (*this);
			}
			
			unit_t& operator -= (const unit_t& rhs)
			{
				(*this) = (*this) - rhs;
				return (*this);
			}
			
			template<int t, int l, int T, int Q, int V>
			friend std::ostream& operator << (std::ostream& os, const unit_t<t,l,T,Q,V>& rhs);

			template<int t, int l, int T, int Q, int V>
			friend unit_t<t,l,T,Q,V> operator* (double scale, const unit_t<t,l,T,Q,V>& rhs);

			template<int t, int l, int T, int Q, int V>
			friend unit_t<t,l,T,Q,V> operator* (long double scale, const unit_t<t,l,T,Q,V>& rhs);

			template<int t, int l, int T, int Q, int V>
			friend unit_t<-t,-l,-T,-Q,-V> operator/ (double scale, const unit_t<t,l,T,Q,V>& rhs);

			template<int t, int l, int T, int Q, int V>
			friend unit_t<-t,-l,-T,-Q,-V> operator/ (long double scale, const unit_t<t,l,T,Q,V>& rhs);
	};

	template<int t, int l, int T, int Q, int V>
	std::ostream& operator << (std::ostream& os, const unit_t<t,l,T,Q,V>& rhs)
	{
		os << rhs.magnitude;
		return os;
	}

	template<int t, int l, int T, int Q, int V>
	unit_t<t,l,T,Q,V> operator* (double scale, const unit_t<t,l,T,Q,V>& rhs)
	{
		return unit_t<t,l,T,Q,V>(scale*rhs.magnitude);
	}

	template<int t, int l, int T, int Q, int V>
	unit_t<t,l,T,Q,V> operator* (long double scale, const unit_t<t,l,T,Q,V>& rhs)
	{
		return unit_t<t,l,T,Q,V>(scale*rhs.magnitude);
	}

	template<int t, int l, int T, int Q, int V>
	unit_t<-t,-l,-T,-Q,-V> operator/ (double scale, const unit_t<t,l,T,Q,V>& rhs)
	{
		return unit_t<-t,-l,-T,-Q,-V>(scale / rhs.magnitude);
	}

	template<int t, int l, int T, int Q, int V>
	unit_t<-t,-l,-T,-Q,-V> operator/ (long double scale, const unit_t<t,l,T,Q,V>& rhs)
	{
		return unit_t<-t,-l,-T,-Q,-V>(scale / rhs.magnitude);
	}

	using voltage_t			= unit_t<0,0,0,0,1>;
	using current_t			= unit_t<-1,0,0,1,0>;
	using energy_t			= unit_t<0,0,0,1,1>;
	using time_t			= unit_t<1,0,0,0,0>;
	using frequency_t		= unit_t<-1,0,0,0,0>;
	using power_t			= unit_t<-1,0,0,1,1>;
	using temperature_t		= unit_t<0,0,1,0,0>;
	using length_t			= unit_t<0,1,0,0,0>;
	using area_t			= unit_t<0,2,0,0,0>;
	using volume_t			= unit_t<0,3,0,0,0>;
	using power_density_t	= unit_t<-1,-2,0,1,1>;
}



#endif //_UNIT_T_
