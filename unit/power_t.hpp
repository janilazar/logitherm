#ifndef _POWER_T_
#define _POWER_T_

#include "unit/unit_t.hpp"

namespace unit
{
	power_t operator "" _TW (long double val);
	power_t operator "" _GW (long double val);
	power_t operator "" _MW (long double val);
	power_t operator "" _kW (long double val);
	power_t operator "" _W (long double val);
	power_t operator "" _mW (long double val);
	power_t operator "" _uW (long double val);
	power_t operator "" _nW (long double val);
	power_t operator "" _pW (long double val);
	power_t operator "" _fW (long double val);

	power_t operator "" _TW (unsigned long long val);
	power_t operator "" _GW (unsigned long long val);
	power_t operator "" _MW (unsigned long long val);
	power_t operator "" _kW (unsigned long long val);
	power_t operator "" _W (unsigned long long val);
	power_t operator "" _mW (unsigned long long val);
	power_t operator "" _uW (unsigned long long val);
	power_t operator "" _nW (unsigned long long val);
	power_t operator "" _pW (unsigned long long val);
	power_t operator "" _fW (unsigned long long val);
}


using unit::operator "" _TW;
using unit::operator "" _GW;
using unit::operator "" _MW;
using unit::operator "" _kW;
using unit::operator "" _W;
using unit::operator "" _mW;
using unit::operator "" _uW;
using unit::operator "" _nW;
using unit::operator "" _pW;
using unit::operator "" _fW;

#endif //_POWER_T_
