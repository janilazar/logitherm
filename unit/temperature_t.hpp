#ifndef _TEMPERATURE_T_
#define _TEMPERATURE_T_

#include "unit/unit_t.hpp"

namespace unit
{
	temperature_t operator "" _TK (long double val);
	temperature_t operator "" _GK (long double val);
	temperature_t operator "" _MK (long double val);
	temperature_t operator "" _kK (long double val);
	temperature_t operator "" _K (long double val);
	temperature_t operator "" _mK (long double val);
	temperature_t operator "" _uK (long double val);
	temperature_t operator "" _nK (long double val);
	temperature_t operator "" _pK (long double val);
	temperature_t operator "" _fK (long double val);

	temperature_t operator "" _TK (unsigned long long val);
	temperature_t operator "" _GK (unsigned long long val);
	temperature_t operator "" _MK (unsigned long long val);
	temperature_t operator "" _kK (unsigned long long val);
	temperature_t operator "" _K (unsigned long long val);
	temperature_t operator "" _mK (unsigned long long val);
	temperature_t operator "" _uK (unsigned long long val);
	temperature_t operator "" _nK (unsigned long long val);
	temperature_t operator "" _pK (unsigned long long val);
	temperature_t operator "" _fK (unsigned long long val);

	temperature_t operator "" _celsius (long double val);
	temperature_t operator "" _celsius (unsigned long long val);
}


using unit::operator "" _TK;
using unit::operator "" _GK;
using unit::operator "" _MK;
using unit::operator "" _kK;
using unit::operator "" _K;
using unit::operator "" _mK;
using unit::operator "" _uK;
using unit::operator "" _nK;
using unit::operator "" _pK;
using unit::operator "" _fK;
using unit::operator "" _celsius;

#endif //_TIME_TYPE_
