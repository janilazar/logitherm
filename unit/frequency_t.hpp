#ifndef _FREQUENCY_T_
#define _FREQUENCY_T_

#include "unit/unit_t.hpp"

namespace unit
{
	frequency_t operator "" _THz (long double val);
	frequency_t operator "" _GHz (long double val);
	frequency_t operator "" _MHz (long double val);
	frequency_t operator "" _kHz (long double val);
	frequency_t operator "" _Hz (long double val);
	frequency_t operator "" _mHz (long double val);
	frequency_t operator "" _uHz (long double val);
	frequency_t operator "" _nHz (long double val);
	frequency_t operator "" _pHz (long double val);
	frequency_t operator "" _fHz (long double val);

	frequency_t operator "" _THz (unsigned long long val);
	frequency_t operator "" _GHz (unsigned long long val);
	frequency_t operator "" _MHz (unsigned long long val);
	frequency_t operator "" _kHz (unsigned long long val);
	frequency_t operator "" _Hz (unsigned long long val);
	frequency_t operator "" _mHz (unsigned long long val);
	frequency_t operator "" _uHz (unsigned long long val);
	frequency_t operator "" _nHz (unsigned long long val);
	frequency_t operator "" _pHz (unsigned long long val);
	frequency_t operator "" _fHz (unsigned long long val);
}


using unit::operator "" _THz;
using unit::operator "" _GHz;
using unit::operator "" _MHz;
using unit::operator "" _kHz;
using unit::operator "" _Hz;
using unit::operator "" _mHz;
using unit::operator "" _uHz;
using unit::operator "" _nHz;
using unit::operator "" _pHz;
using unit::operator "" _fHz;

#endif //_FREQUENCY_T_
