#include "unit/voltage_t.hpp"

namespace unit
{
	voltage_t operator "" _TV (long double val)
	{
		return voltage_t(val*1e12l);
	}
	
	voltage_t operator "" _GV (long double val)
	{
		return voltage_t(val * 1e9l);
	}
	
	voltage_t operator "" _MV (long double val)
	{
		return voltage_t(val * 1e6l);
	}
		
	voltage_t operator "" _kV (long double val)
	{
		return voltage_t(val * 1e3l);
	}	
		
	voltage_t operator "" _V (long double val)
	{
		return voltage_t(val);
	}
	
	voltage_t operator "" _mV (long double val)
	{
		return voltage_t(val*1e-3l);
	}
	
	voltage_t operator "" _uV (long double val)
	{
		return voltage_t(val * 1e-6l);
	}
	
	voltage_t operator "" _nV (long double val)
	{
		return voltage_t(val * 1e-9l);
	}
	
	voltage_t operator "" _pV (long double val)
	{
		return voltage_t(val * 1e-12l);
	}
	
	voltage_t operator "" _fV (long double val)
	{
		return voltage_t(val * 1e-15l);
	}

	voltage_t operator "" _TV (unsigned long long val)
	{
		return voltage_t(val*1e12l);
	}
	
	voltage_t operator "" _GV (unsigned long long val)
	{
		return voltage_t(val * 1e9l);
	}
	
	voltage_t operator "" _MV (unsigned long long val)
	{
		return voltage_t(val * 1e6l);
	}
		
	voltage_t operator "" _kV (unsigned long long val)
	{
		return voltage_t(val * 1e3l);
	}	
		
	voltage_t operator "" _V (unsigned long long val)
	{
		return voltage_t(val);
	}
	
	voltage_t operator "" _mV (unsigned long long val)
	{
		return voltage_t(val*1e-3l);
	}
	
	voltage_t operator "" _uV (unsigned long long val)
	{
		return voltage_t(val * 1e-6l);
	}
	
	voltage_t operator "" _nV (unsigned long long val)
	{
		return voltage_t(val * 1e-9l);
	}
	
	voltage_t operator "" _pV (unsigned long long val)
	{
		return voltage_t(val * 1e-12l);
	}
	
	voltage_t operator "" _fV (unsigned long long val)
	{
		return voltage_t(val * 1e-15l);
	}
}

