#ifndef _ENERGY_T_
#define _ENERGY_T_

#include "unit/unit_t.hpp"

namespace unit
{
	energy_t operator "" _TJ (long double val);
	energy_t operator "" _GJ (long double val);
	energy_t operator "" _MJ (long double val);
	energy_t operator "" _kJ (long double val);
	energy_t operator "" _J (long double val);
	energy_t operator "" _mJ (long double val);
	energy_t operator "" _uJ (long double val);
	energy_t operator "" _nJ (long double val);
	energy_t operator "" _pJ (long double val);
	energy_t operator "" _fJ (long double val);

	energy_t operator "" _TJ (unsigned long long val);
	energy_t operator "" _GJ (unsigned long long val);
	energy_t operator "" _MJ (unsigned long long val);
	energy_t operator "" _kJ (unsigned long long val);
	energy_t operator "" _J (unsigned long long val);
	energy_t operator "" _mJ (unsigned long long val);
	energy_t operator "" _uJ (unsigned long long val);
	energy_t operator "" _nJ (unsigned long long val);
	energy_t operator "" _pJ (unsigned long long val);
	energy_t operator "" _fJ (unsigned long long val);
}

using unit::operator "" _TJ;
using unit::operator "" _GJ;
using unit::operator "" _MJ;
using unit::operator "" _kJ;
using unit::operator "" _J;
using unit::operator "" _mJ;
using unit::operator "" _uJ;
using unit::operator "" _nJ;
using unit::operator "" _pJ;
using unit::operator "" _fJ;
	

#endif //_ENERGY_T_
