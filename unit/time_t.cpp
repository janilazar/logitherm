#include "unit/time_t.hpp"


namespace unit
{		
	time_t operator "" _Ts (long double val)
	{
		return time_t(val*1e12l);
	}
	
	time_t operator "" _Gs (long double val)
	{
		return time_t(val * 1e9l);
	}
	
	time_t operator "" _Ms (long double val)
	{
		return time_t(val * 1e6l);
	}
		
	time_t operator "" _ks (long double val)
	{
		return time_t(val * 1e3l);
	}	
		
	time_t operator "" _s (long double val)
	{
		return time_t(val);
	}
	
	time_t operator "" _ms (long double val)
	{
		return time_t(val*1e-3l);
	}
	
	time_t operator "" _us (long double val)
	{
		return time_t(val * 1e-6l);
	}
	
	time_t operator "" _ns (long double val)
	{
		return time_t(val * 1e-9l);
	}
	
	time_t operator "" _ps (long double val)
	{
		return time_t(val * 1e-12l);
	}
	
	time_t operator "" _fs (long double val)
	{
		return time_t(val * 1e-15l);
	}

	time_t operator "" _Ts (unsigned long long val)
	{
		return time_t(val*1e12l);
	}
	
	time_t operator "" _Gs (unsigned long long val)
	{
		return time_t(val * 1e9l);
	}
	
	time_t operator "" _Ms (unsigned long long val)
	{
		return time_t(val * 1e6l);
	}
		
	time_t operator "" _ks (unsigned long long val)
	{
		return time_t(val * 1e3l);
	}	
		
	time_t operator "" _s (unsigned long long val)
	{
		return time_t(val);
	}
	
	time_t operator "" _ms (unsigned long long val)
	{
		return time_t(val*1e-3l);
	}
	
	time_t operator "" _us (unsigned long long val)
	{
		return time_t(val * 1e-6l);
	}
	
	time_t operator "" _ns (unsigned long long val)
	{
		return time_t(val * 1e-9l);
	}
	
	time_t operator "" _ps (unsigned long long val)
	{
		return time_t(val * 1e-12l);
	}
	
	time_t operator "" _fs (unsigned long long val)
	{
		return time_t(val * 1e-15l);
	}
}

