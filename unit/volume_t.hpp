#ifndef _VOLUME_T_
#define _VOLUME_T_

#include "unit/unit_t.hpp"

namespace unit
{
	volume_t operator "" _Tm3 (long double val);
	volume_t operator "" _Gm3 (long double val);
	volume_t operator "" _Mm3 (long double val);
	volume_t operator "" _km3 (long double val);
	volume_t operator "" _m3 (long double val);
	volume_t operator "" _mm3 (long double val);
	volume_t operator "" _um3 (long double val);
	volume_t operator "" _nm3 (long double val);
	volume_t operator "" _pm3 (long double val);
	volume_t operator "" _fm3 (long double val);

	volume_t operator "" _Tm3 (unsigned long long val);
	volume_t operator "" _Gm3 (unsigned long long val);
	volume_t operator "" _Mm3 (unsigned long long val);
	volume_t operator "" _km3 (unsigned long long val);
	volume_t operator "" _m3 (unsigned long long val);
	volume_t operator "" _mm3 (unsigned long long val);
	volume_t operator "" _um3 (unsigned long long val);
	volume_t operator "" _nm3 (unsigned long long val);
	volume_t operator "" _pm3 (unsigned long long val);
	volume_t operator "" _fm3 (unsigned long long val);
}


//using manager::unit::operator*;
//using manager::unit::operator/;

using unit::operator "" _Tm3;
using unit::operator "" _Gm3;
using unit::operator "" _Mm3;
using unit::operator "" _km3;
using unit::operator "" _m3;
using unit::operator "" _mm3;
using unit::operator "" _um3;
using unit::operator "" _nm3;
using unit::operator "" _pm3;
using unit::operator "" _fm3;

#endif //_VOLUME_T_
