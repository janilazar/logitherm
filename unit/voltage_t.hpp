#ifndef _VOLTAGE_T_
#define _VOLTAGE_T_

#include "unit/unit_t.hpp"

namespace unit
{
	voltage_t operator "" _TV (long double  val);
	voltage_t operator "" _GV (long double  val);
	voltage_t operator "" _MV (long double  val);
	voltage_t operator "" _kV (long double  val);
	voltage_t operator "" _V (long double val);
	voltage_t operator "" _mV (long double  val);
	voltage_t operator "" _uV (long double  val);
	voltage_t operator "" _nV (long double  val);
	voltage_t operator "" _pV (long double  val);
	voltage_t operator "" _fV (long double  val);

	voltage_t operator "" _TV (unsigned long long val);
	voltage_t operator "" _GV (unsigned long long val);
	voltage_t operator "" _MV (unsigned long long val);
	voltage_t operator "" _kV (unsigned long long val);
	voltage_t operator "" _V (unsigned long long val);
	voltage_t operator "" _mV (unsigned long long val);
	voltage_t operator "" _uV (unsigned long long val);
	voltage_t operator "" _nV (unsigned long long val);
	voltage_t operator "" _pV (unsigned long long val);
	voltage_t operator "" _fV (unsigned long long val);
}

//using manager::unit::operator*;

using unit::operator "" _TV;
using unit::operator "" _GV;
using unit::operator "" _MV;
using unit::operator "" _kV;
using unit::operator "" _V; 
using unit::operator "" _mV;
using unit::operator "" _uV;
using unit::operator "" _nV;
using unit::operator "" _pV;
using unit::operator "" _fV;

#endif //_VOLTAGE_T_
