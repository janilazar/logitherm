#include "unit/volume_t.hpp"

namespace unit
{
	volume_t operator "" _Tm3 (long double val)
	{
		return volume_t(val*1e36l);
	}
	
	volume_t operator "" _Gm3 (long double val)
	{
		return volume_t(val * 1e27l);
	}
	
	volume_t operator "" _Mm3 (long double val)
	{
		return volume_t(val * 1e18l);
	}
		
	volume_t operator "" _km3 (long double val)
	{
		return volume_t(val * 1e9l);
	}	
		
	volume_t operator "" _m3 (long double val)
	{
		return volume_t(val);
	}
	
	volume_t operator "" _mm3 (long double val)
	{
		return volume_t(val*1e-9l);
	}
	
	volume_t operator "" _um3 (long double val)
	{
		return volume_t(val * 1e-18l);
	}
	
	volume_t operator "" _nm3 (long double val)
	{
		return volume_t(val * 1e-27l);
	}
	
	volume_t operator "" _pm3 (long double val)
	{
		return volume_t(val * 1e-36l);
	}
	
	volume_t operator "" _fm3 (long double val)
	{
		return volume_t(val * 1e-45l);
	}

	volume_t operator "" _Tm3 (unsigned long long val)
	{
		return volume_t(val*1e36l);
	}
	
	volume_t operator "" _Gm3 (unsigned long long val)
	{
		return volume_t(val * 1e27l);
	}
	
	volume_t operator "" _Mm3 (unsigned long long val)
	{
		return volume_t(val * 1e18l);
	}
		
	volume_t operator "" _km3 (unsigned long long val)
	{
		return volume_t(val * 1e9l);
	}	
		
	volume_t operator "" _m3 (unsigned long long val)
	{
		return volume_t(val);
	}
	
	volume_t operator "" _mm3 (unsigned long long val)
	{
		return volume_t(val*1e-9l);
	}
	
	volume_t operator "" _um3 (unsigned long long val)
	{
		return volume_t(val * 1e-18l);
	}
	
	volume_t operator "" _nm3 (unsigned long long val)
	{
		return volume_t(val * 1e-27l);
	}
	
	volume_t operator "" _pm3 (unsigned long long val)
	{
		return volume_t(val * 1e-36l);
	}
	
	volume_t operator "" _fm3 (unsigned long long val)
	{
		return volume_t(val * 1e-45l);
	}
}

