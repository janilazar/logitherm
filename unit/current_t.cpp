#include "unit/current_t.hpp"

namespace unit
{
	current_t operator "" _TA (long double val)
	{
		return current_t(val*1e12l);
	}
	
	current_t operator "" _GA (long double val)
	{
		return current_t(val * 1e9l);
	}
	
	current_t operator "" _MA (long double val)
	{
		return current_t(val * 1e6l);
	}
		
	current_t operator "" _kA (long double val)
	{
		return current_t(val * 1e3l);
	}	
		
	current_t operator "" _A (long double val)
	{
		return current_t(val);
	}
	
	current_t operator "" _mA (long double val)
	{
		return current_t(val*1e-3l);
	}
	
	current_t operator "" _uA (long double val)
	{
		return current_t(val * 1e-6l);
	}
	
	current_t operator "" _nA (long double val)
	{
		return current_t(val * 1e-9l);
	}
	
	current_t operator "" _pA (long double val)
	{
		return current_t(val * 1e-12l);
	}
	
	current_t operator "" _fA (long double val)
	{
		return current_t(val * 1e-15l);
	}

	current_t operator "" _TA (unsigned long long val)
	{
		return current_t(val*1e12l);
	}
	
	current_t operator "" _GA (unsigned long long val)
	{
		return current_t(val * 1e9l);
	}
	
	current_t operator "" _MA (unsigned long long val)
	{
		return current_t(val * 1e6l);
	}
		
	current_t operator "" _kA (unsigned long long val)
	{
		return current_t(val * 1e3l);
	}	
		
	current_t operator "" _A (unsigned long long val)
	{
		return current_t(val);
	}
	
	current_t operator "" _mA (unsigned long long val)
	{
		return current_t(val*1e-3l);
	}
	
	current_t operator "" _uA (unsigned long long val)
	{
		return current_t(val * 1e-6l);
	}
	
	current_t operator "" _nA (unsigned long long val)
	{
		return current_t(val * 1e-9l);
	}
	
	current_t operator "" _pA (unsigned long long val)
	{
		return current_t(val * 1e-12l);
	}
	
	current_t operator "" _fA (unsigned long long val)
	{
		return current_t(val * 1e-15l);
	}
}
