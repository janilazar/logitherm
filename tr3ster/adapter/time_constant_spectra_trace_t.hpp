#ifndef _TIME_CONSTANT_SPECTRA_TRACE_T_
#define _TIME_CONSTANT_SPECTRA_TRACE_T_

#include "util/trace_t.hpp"
#include "util/timestep_t.hpp"
#include "tr3ster/engine/class.h"

namespace layout
{
	class component_t;
}

namespace tr3ster
{	
	class time_constant_spectra_trace_t	:	public util::trace_t
	{
		private:
			layout::component_t* component;
			qv z, tau;

		public:
			
			time_constant_spectra_trace_t(const std::string& path, const std::string& postfix, layout::component_t* componet);
			
			void initialize(std::ostream& os) override;
			
			void trace(std::ostream& os) override;
			
			void reset() override;
			
			void finalize(std::ostream& os) override;
	};
}

#endif //_DYNAMIC_DISSIPATION_TRACE_
