#ifndef _TR3STER_ADAPTER_T_
#define _TR3STER_ADAPTER_T_

#include <vector>

#include "logic/base/adapter_t.hpp"

#include "thermal/base/adapter_t.hpp"
// #include "thermal/sunred/adapter/adapter_t.hpp"
// #include "thermal/sloth/adapter/adapter_t.hpp"
// #include "thermal/hotspot/adapter/adapter_t.hpp"
// #include "thermal/3dice/adapter/adapter_t.hpp"

#include "tr3ster/engine/class.h"

namespace layout
{
	class component_t;
}

namespace thermal
{
	namespace sunred
	{
		class adapter_t;
	}

	namespace sloth
	{
		class adapter_t;
	}

	namespace threed_ice
	{
		class adapter_t;
	}

	namespace hotspot
	{
		class adapter_t;
	}
}

namespace tr3ster
{
	class adapter_t
	{
		private:
			
			// layout::component_t, aminek ki akarjuk szamitani a spektrumat
			layout::component_t* const layout_component;

			// path a trace-hez
			// const std::string& path;


			logitherm::manager_t* const manager;
			util::log_t* const log;
			const thermal::adapter_t& ref_thermal_engine;


			util::timestep_t timestep;
			
			// polimorfizmus miatt kell ptr :(
			thermal::adapter_t* temp_thermal_engine;
			logic::adapter_t temp_logic_engine;

			// kell egy tracer_t modul
			

			// std::vector<double> tau;
			// std::vector<double> z;

			// activity_trace_t* add_activity_trace(const std::string& path, const std::string& id, const std::string& postfix = "");

		public:
			adapter_t() = delete;
			adapter_t(layout::component_t* component, const thermal::sunred::adapter_t& sunred_adapter);
			adapter_t(layout::component_t* component, const thermal::sloth::adapter_t& sloth_adapter);
			adapter_t(layout::component_t* component, const thermal::threed_ice::adapter_t& threed_ice_adapter) = delete;
			adapter_t(layout::component_t* component, const thermal::hotspot::adapter_t& hotspot_adapter) = delete;

			void calculate_time_constant_spectra(qv& z, qv& tau);

			~adapter_t();

	};
}


#endif //_TR3STER_ADAPTER_T_