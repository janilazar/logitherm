#include "tr3ster/adapter/time_constant_spectra_trace_t.hpp"
#include "tr3ster/adapter/adapter_t.hpp"
#include "thermal/base/layout/component_t.hpp"
#include "thermal/base/layout/adapter_t.hpp"
#include "thermal/base/adapter_t.hpp"

#include "thermal/sunred/adapter/adapter_t.hpp"
#include "thermal/sloth/adapter/adapter_t.hpp"
#include "thermal/hotspot/adapter/adapter_t.hpp"
#include "thermal/3dice/adapter/adapter_t.hpp"
// #include "tr3ster/"

namespace tr3ster
{	
	time_constant_spectra_trace_t::time_constant_spectra_trace_t(const std::string& path, const std::string& postfix, layout::component_t* component)
	:
		util::trace_t(path, component->id, "time_constant_spectra", postfix),
		component(component)
	{}


	void time_constant_spectra_trace_t::initialize(std::ostream& os)
	{
		tr3ster::adapter_t* tr3ster_engine;

		if(component->adapter->adapter == nullptr) component->error("time_constant_spectra_trace_t::trace(): adapter is nullptr");
		switch(component->adapter->adapter->type)
		{
			case util::thermal_engine_t::sloth:
				tr3ster_engine = new tr3ster::adapter_t(component, dynamic_cast<const thermal::sloth::adapter_t&>(*(component->adapter->adapter)));
				break;
			case util::thermal_engine_t::sunred:
				tr3ster_engine = new tr3ster::adapter_t(component, dynamic_cast<const thermal::sunred::adapter_t&>(*(component->adapter->adapter)));
				break;
			default:
				component->error("tr3ster::calculate_time_constant_spectra(): unsupported thermal engine type");
		}

		tr3ster_engine->calculate_time_constant_spectra(z, tau);

		delete tr3ster_engine;
		tr3ster_engine = nullptr;

		os << "z, "<< id << "_" << type << std::endl;
		for(size_t it = 0; it < z.size(); ++it) os << z.v[it] << ", " << tau.v[it] << std::endl;
	}

	void time_constant_spectra_trace_t::trace(std::ostream& os)
	{}

	void time_constant_spectra_trace_t::reset()
	{}
	
	void time_constant_spectra_trace_t::finalize(std::ostream& os)
	{}
}
