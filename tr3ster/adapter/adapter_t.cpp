#include "tr3ster/adapter/adapter_t.hpp"
#include "tr3ster/adapter/component_t.hpp"
// #include "tr3ster/engine/class.h"
#include "tr3ster/engine/engine.h"
#include "tr3ster/adapter/time_constant_spectra_trace_t.hpp"

#include "thermal/base/layout/component_t.hpp"

#include "thermal/sunred/adapter/adapter_t.hpp"
#include "thermal/sloth/adapter/adapter_t.hpp"
#include "thermal/hotspot/adapter/adapter_t.hpp"
#include "thermal/3dice/adapter/adapter_t.hpp"

namespace tr3ster
{
	adapter_t::adapter_t(layout::component_t* component, const thermal::sunred::adapter_t& sunred_adapter)
	:
		layout_component(component),
		manager(sunred_adapter.manager),
		log(sunred_adapter.log),
		ref_thermal_engine(sunred_adapter),
		timestep(1_ns, util::timescale_t::log, 30),
		temp_thermal_engine(new thermal::sunred::adapter_t(sunred_adapter,  "tr3ster::thermal", timestep)),
		temp_logic_engine(sunred_adapter.manager, sunred_adapter.log, "tr3ster::logic", util::logic_engine_t::systemc, timestep)
	{
		// new_thermal_adapter->create_layout(base_thermal_adapter.get_layout()->get_length)
	}

	adapter_t::adapter_t(layout::component_t* component, const thermal::sloth::adapter_t& sloth_adapter)
	:
		layout_component(component),
		manager(sloth_adapter.manager),
		log(sloth_adapter.log),
		ref_thermal_engine(sloth_adapter),
		timestep(1_ns, util::timescale_t::log, 30),
		temp_thermal_engine(new thermal::sloth::adapter_t(sloth_adapter, "tr3ster::thermal", timestep)),
		temp_logic_engine(sloth_adapter.manager, sloth_adapter.log, "tr3ster::logic", util::logic_engine_t::systemc, timestep)
	{

	}

	// itt string id helyett adjunk at layout::component_t*-ot
	void adapter_t::calculate_time_constant_spectra(qv& z, qv& tau)
	{
		// layout::component_t* layout_component_ptr = temp_thermal_engine->get_layout()->get_component(id);
		tr3ster::component_t logic_component(&temp_logic_engine, layout_component->id, 10_W); //ezen javitani kellene...

		qv temps, time;

		std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal> components = {{&logic_component, layout_component}};
		
		// szimulalni kell megfelelo ideig az elrendezest, elmenteni megfelelo tipusba (tokomtudja milyen vektorba) az idopontot es a homersekletet
		while(timestep.current_time() < 10_s)
		{
			timestep.advance_time();
			temp_thermal_engine->refresh_temperatures(components);
			temps.push(static_cast<double>(logic_component.temperature));
			time.push(static_cast<double>(timestep.current_time()));

			// std::cout << timestep.current_timestep() << ", "<< timestep.current_time() << ", " << logic_component.temperature << std::endl;
			timestep.advance_timestep();
		}

		// std::cout << "-----------------------------------" << std::endl;

		calculate_time_spectra(time, temps, 10.0, tau, z);
		
		// util::tracer_t tracer("tr3ster", id, log);
		// time_constant_spectra_trace_t* tau_trace_ptr = new time_constant_spectra_trace_t(path, "", id, z, tau);
		// tracer.add_trace(tau_trace_ptr);
		// tracer.initialize();
		// tracer.trace();
		// z.print();
		// tau.print();

	}

	// activity_trace_t* component_t::add_activity_trace(const std::string& path, const std::string& activity_id, const std::string& postfix)
	// {
	// 	activity_trace_t* ptr = new activity_trace_t(path, this->id + "_" + activity_id, postfix);
	// 	tracer.add_trace(ptr);
	// 	trace_component();
	// 	return ptr;
	// }

	adapter_t::~adapter_t()
	{
		delete temp_thermal_engine;
	}
}
