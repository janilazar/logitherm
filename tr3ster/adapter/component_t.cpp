#include "tr3ster/adapter/component_t.hpp"
// #include "tr3ster/adapter_t.hpp"

#include <string>

namespace tr3ster
{
	component_t::component_t(logic::adapter_t* adapter, const std::string& id, unit::power_t dissipation)
	:
		logic::component_t(adapter, id, nullptr),
		dissipation(dissipation)
	{}

	unit::power_t component_t::component_dynamic_dissipation() const
	{
		return 0_W;
	}

	unit::power_t component_t::component_static_dissipation() const
	{
		return dissipation;
	}
}