#ifndef _TR3STER_COMPONENT_T_
#define _TR3STER_COMPONENT_T_

#include "logic/base/component_t.hpp"
// #include "tr3ster/adapter_t.hpp"

namespace tr3ster
{
	// using adapter_t = logic::adapter_t;
	// typedef logic::adapter_t adapter_t;

	class component_t :	public logic::component_t
	{
		private:
			unit::power_t dissipation;
		
		public:
			component_t(logic::adapter_t* adapter, const std::string& id, unit::power_t dissipation);

			unit::power_t component_dynamic_dissipation() const override;
			unit::power_t component_static_dissipation() const override;
	};
}


#endif //_TR3STER_COMPONENT_T_