#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"
#include "tr3ster/engine/test.h"

#define BAYES 1000
#define EPPD 20

int main(int argc, char* argv[])
 {
	int i;
	if (argc<1) { cout << "Keves parameter!\n"; return 1; }
	parameters par;
	qv t,d,tau,der,tmp,newtime,newdata,nt,z,foster,rf,cf,cauer,rc,cc;

	par.eppd = EPPD;

	tau = testfileread(argv[1],2);
	nt = testfileread(argv[1],1);
	z = qvlog(nt);


/****** Foster hálózat generálása ******************/

	foster = discretize(tau, z, par);
		for(i=1;i<foster.size();i=i+2) {
			rf.push(foster.v[i-1]);
			cf.push(foster.v[i]);
		}

/****** Cauer hálózat generálása *******************/
/*
	cauer = fostcaur(rf, cf);
		for(i=1;i<cauer.size();i=i+2) {
			rc.push(cauer.v[i-1]);
			cc.push(cauer.v[i]);
		}
*/

	cf.print();
	
	return 0;
 }
