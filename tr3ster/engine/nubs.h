/**********************************
 * 	Non-Uniform-B-spline, B-spline
 *  és Bezier görbe implementáció
 **********************************/

class Vector {
//===============================================================
public:
    double x, y;   // a Descartes koordináták
	Vector( ) { x = y = 0.0; }
	Vector( double x0, double y0 ) { x = x0; y = y0; }

	Vector operator+( const Vector& v ) { // két vektor összege
		double X = x + v.x, Y = y + v.y;
		return Vector(X, Y);
	}
	Vector operator-( const Vector& v ) { 
		double X = x - v.x, Y = y - v.y;
		return Vector(X, Y);
	}	
	Vector operator*( double f ) {         // vektor és szám szorzata
		return Vector( x * f, y * f );
	}
	double operator*( const Vector& v ) {  // két vektor skaláris szorzata
		return (x * v.x + y * v.y);
	}
	double Length( ) {                     // vektor abszolút értéke
		return (double)sqrt( x * x + y * y );
	}
	void operator+=( const Vector& v ) {  // vektor összeadás
		x += v.x; y += v.y; 
	}
	void operator-=( const Vector& v ) {  // vektor különbség
		x -= v.x; y -= v.y; 
	}
	void operator*=( double f ) {		  // vektor és szám szorzata
		x *= f; y *= f; 
	}
	Vector operator/( double f ) {		  // vektor osztva egy számmal
		return Vector( x/f, y/f ); 
	}

	double& X() { return x; }
	double& Y() { return y; }
};

#define MAXPOINTS 10000
#define MAXKNOTS 10002

enum NubsType {
	NUBS, BSpline, Bezier
};

//===============================================================
class NUBSCurve {		 // NUBS görbe
//===============================================================
protected:
   Vector r[MAXPOINTS];  // vezérlõpontok tömbje
   double  t[MAXKNOTS];   // csomóvektor
   int    m;             // pontok száma 
   int    K;             // fokszám
   NubsType type;		 // csomóvektor kialakítása

public:

   NUBSCurve( ) {
	  K = 4;		// harmadfokú (negyedik szint)
	  type = NUBS;	// végpontokon átmenõ NUBS
	  m = 0;
   }

   int& Degree( ) { return K; }
   NubsType& Type( ) { return type; }
   Vector& rknot( int i ) { return r[i]; }

   void AddControlPoint( Vector& r0 ) { r[m++] = r0; }

   void SetKnotVector( ) {
	  int i;
	  if (K > m) K = m;
	  if (K < 2) K = 2;
	  switch (type) {
	  case NUBS:	
		  for(i = 0; i < K-1; i++) t[i] = 0;
		  for(i = K-1; i <= m; i++) t[i] = i-K+1;
		  for(i = m+1; i < m+K-1; i++) t[i] = m-K+1;
		  break;
	  case BSpline:
		  for(i = 0; i < m+K; i++) t[i] = i - K + 1;
		  break;
	  case Bezier:
		  K = m;
		  for(i = 0; i < m; i++) t[i] = 0;
 		  for(i = m; i < 2 * m; i++) t[i] = 1;
		  break;
	  }
	  for(i=0;i<m+K-1;i++) { cout << t[i] << endl; }
  }
   
   double B(int i, int k, double tt) {   // k-rendû i. bázisfüggvény
      if (k == 1)  {                 // triviális eset vizsgálata
	     if (i < m - 1) {
		    if (t[i] <= tt && tt < t[i+1])  return 1;
			else						    return 0; 
		 } else {
			if (t[i] <= tt)  return 1;
			else			 return 0; 
		 }
	  }
		//cout << "tt" << tt <<  endl;
	  double b1, b2;
	  if (t[i+k-1] - t[i] > 1e-16) b1 = (tt-t[i]) / (t[i+k-1] - t[i]);
	  else                           b1 = 1.0;       // Itt: 0/0 = 1
	  if (t[i+k] - t[i+1] > 1e-16) b2 = (t[i+k]-tt) / (t[i+k] - t[i+1]);
	  else                           b2 = 1.0;       // Itt: 0/0 = 1

	   				
	  return (b1 * B(i, k-1, tt) + b2 * B(i+1, k-1, tt) ); // rekurzió
  }

   virtual Vector Curve(double t) {		// a görbe egy adott pontja
	  Vector rt(0,0);
      for(int i = 0; i < m; i++) {rt += r[i] * B(i, K, t); }
	  return rt;
   }
};
