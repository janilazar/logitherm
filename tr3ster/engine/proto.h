int max(int a, int b);
int min(int a, int b);
qv convolut(qv a, qv b, int refw);
qv correlat(qv a, qv b, int refw);
qv bayes(qv deriv, int nit, double eppd);
qv csvread(char* filename, int o);
qv deconvolution_b(qv deriv, int nit, double eppd);
void csvwrite(char* filename, qv x, qv y);
qv txtread(char* filename, int o);
qv qvlog(qv a);
qv qvexp(qv a);
qv qvsqrt(qv a);
int sign(double a);
int keres(qv miben, TYPE mit);

// fileparse.cpp
parameters parfileread(char* filename);
qv rawfileread(char* filename, int s);

stringvect blowstring(string str);

// derive.cpp
qv fastsmooth(qv be, int smoothwidth);
qv derive(qv be);

//regress.cpp
qv regress(qv d, qv t, parameters par);

//discretize.cpp
qv discretize(qv Rz, qv z, parameters par);

//mpqc.cpp (Foster-Cauer)
qv fostcaur(qv rf, qv cf);

//comploci.cpp
qv comploci(qv t, qv d, qv der, parameters par);
double	wrpwt(double om,double delt);
double	wipwt(double om,double delt);
qv mirror(qv t,qv u, int sel);
qv convol(qv a,qv w,int cw);

//pulse_rth.cpp
qv pulse_rth(qv d, qv t, qv tauint, qv delt);

//fft.cpp
qv deconvolution_fft(qv deriv, qv z, double gauss, double noise);

//filter.cpp
qv filter(qv data, qv time, double start, double end, int mode, int gorbe);

//cgamma.cpp
complex double cgamma(complex double z);
