#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"

qv comploci(qv tim, qv d, qv der, parameters par) {
	qv out;
	double delt,tmin,tmax;
	delt = 0.3;
	int decr;
	int i,n,nw3,nw3p2;
	double s,t,x,tratio,dppd,rdppd,sumr;
	qv ta,ua,tarray,uarray,w3re,w3im,resre,resim;

	decr=der.sum()<0;

	n = tim.size();

	tarray.pushn(2*n-1);
	uarray.pushn(2*n-1);

  /************  Adatok másolása és interpolálása  *************/

	for ( i=0 ; i < n ; i++ )
	{
		tarray.v[2*i]=tim.v[i];
		uarray.v[2*i]=d.v[i];
	}

	for ( i=0 ; i < n-1 ; i++ )
	{
		tarray.v[2*i+1]=sqrt(tarray.v[2*i]*tarray.v[2*i+2]);
		uarray.v[2*i+1]=0.5*(uarray.v[2*i]+uarray.v[2*i+2]);
	}
	n=2*n-1;
	
  /*************** uarray tükrözése, ha csökkenő a fv *********/

	if(decr)
	for(i=0;i<n;i++)
	uarray.v[i]=-uarray.v[i];

  /*************  Pont per dekád számítása  *******************/

	tratio=tarray.v[1]/tarray.v[0];
	rdppd=log10(tratio);
	dppd=1.0/rdppd;
	t=tarray.v[0];
	for ( i = 1 ; i < n ; i++ )
	{		 
		t*=tratio;
		s=tarray.v[i]/t;
		if(s>1.01 || s<0.99)
		{
			cout << "bukta" << endl;
			return out;
		}
     }

  /************  A wr wi súlyfüggvények kiszámítása  **********/

	nw3=(int)dppd*8;
	nw3p2=nw3/2;
	for ( i = 0 ; i < nw3 ; i++ )
    {
		 
		x=log(10.0)*rdppd*(double)(i-nw3p2);
		w3re.push(wrpwt(x,delt));
		w3im.push(wipwt(x,delt));
    }

  /************  Az eredmény kiszámítása konvolúcióval  ********/

	ta = mirror(tarray,uarray,1);
	ua = mirror(tarray,uarray,0);

	resre = convol(ua,w3re,nw3p2);
  	resim = convol(ua,w3im,nw3p2);

	x=log(10.0)*rdppd;
	sumr=ua.v[n-1]-ua.v[0];
	for ( i = 0 ; i < n ; i++ )
    {
		resre.v[i]=sumr+(resre.v[i]*x);
		resim.v[i]=+(resim.v[i]*x);
		out.push(ta.v[i]);
		out.push(resre.v[i]);
		out.push(resim.v[i]);
    }
	
	return out;
}

qv convol(qv a,qv w,int cw)
{ 
	int i,j,k,na,nw;
	double s;
	qv out;
	na = a.size();
	nw = w.size();
	for ( i = 0 ; i <na ; i++ )
    {	 
		s=0.0;
		for ( j = 0 ; j < nw ; j++ )
		{		 
			k=i+cw-j;
			if ( k < 0 ) k=0;
			if(k>na-1) k=na-1;
			s+=a.v[k]*(w.v[j]);
		}
		out.push(s);
     }
	return out;
}

/*************************************************************@*/
qv mirror(qv t,qv u, int sel)        /*@@*/
/* Function: mirror the u(t) array, substitute t by 1/t,      @*/
/*           modify the offset in order to reach u[0]=0       @*/
/*           work is a working area of length n               @*/
/*************************************************************@*/
{ 
	int i,n;
	double s;
	qv work;
	n = u.size();
	work.pushn(n);
	for(i=0;i<n;i++)
		work.v[i]=-(u.v[n-1-i]);

	s=work.v[0];
	for(i=0;i<n;i++)
		u.v[i]=work.v[i]-s;

	for(i=0;i<n;i++)
		work.v[i]=t.v[n-1-i];

	for(i=0;i<n;i++)
		t.v[i]=1.0/work.v[i];

	if(sel==1) { return t;}
	else { return u; }
}

/*************************************************************@*/
double wrpwt(double om,double delt)  /* Coded by: V. Szekely @@*/
/* Function: generates the wr{deconv}wt function following    @*/
/*           the analytihic calculation: true function and    @*/
/*           triple windowing (the half-value width of the    @*/
/*           window is approx. 1.5*delt)                      @*/
/*           om: argument, uppercase omega, ln(angular fr)    @*/
/*           delt: delta value of the windowing               @*/
/*************************************************************@*/
{ 
	int i,j,k;
	double deltp2,delti,deltj,deltk,signi,signj,signk,s,sj,sk;

	deltp2=delt*0.5;
	s=0.0;
	for ( i = 0 ; i < 2 ; i++ )
    {
		 
		delti=(i==0 ? -deltp2 : deltp2);
		signi=(i==0 ? 1.0 : -1.0);
		sj=0.0;
		for ( j = 0 ; j < 2 ; j++ )
		{
			 
			deltj=(j==0 ? -deltp2 : deltp2);
			signj=(j==0 ? 1.0 : -1.0);
			sk=0.0;
			for ( k = 0 ; k < 2 ; k++ )
			{
				 
				deltk=(k==0 ? -deltp2 : deltp2);
				signk=(k==0 ? 1.0 : -1.0);
				sk+=signk*cos(exp(om+deltk-deltj-delti));
			}
			sj+=signj*exp(deltj+delti)*sk;
		}
		s+=signi*exp(delti)*sj;
     }
	s*=1.0/(delt*sinh(delt)*2.0*sinh(deltp2))*exp(-2.0*om);
	return(s);
}

/*************************************************************@*/
double wipwt(double om,double delt)  /* Coded by: V. Szekely @@*/
/* Function: generates the wi{deconv}wt function following    @*/
/*           the analythic calculation: true function and    @*/
/*           triple windowing (the half-value width of the    @*/
/*           window is approx. 1.5*delt)                      @*/
/*           om: argument, uppercase omega, ln(angular fr)    @*/
/*           delt: delta value of the windowing               @*/
/*************************************************************@*/
{ 
	int i,j,k;
	double deltp2,delti,deltj,deltk,signi,signj,signk,s,sj,sk;

	deltp2=delt*0.5;
	s=0.0;
	for ( i = 0 ; i < 2 ; i++ )
    {
		 
		delti=(i==0 ? -deltp2 : deltp2);
		signi=(i==0 ? 1.0 : -1.0);
		sj=0.0;
		for ( j = 0 ; j < 2 ; j++ )
		{
			 
			deltj=(j==0 ? -deltp2 : deltp2);
			signj=(j==0 ? 1.0 : -1.0);
			sk=0.0;
			for ( k = 0 ; k < 2 ; k++ )
			{
				 
				deltk=(k==0 ? -deltp2 : deltp2);
				signk=(k==0 ? 1.0 : -1.0);
				sk+=signk*(sin(exp(om+deltk-deltj-delti)));
			}
			sj+=signj*exp(deltj+delti)*sk;
		}
		s+=signi*exp(delti)*sj;
     }
	s*=1.0/(delt*sinh(delt)*2.0*sinh(deltp2))*exp(-2.0*om);
	return(-s);
}
