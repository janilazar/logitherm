#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"

/**********************************
 * 	Diszkretizálás, Foster hálózat
 * 	előállítása
 **********************************/

qv discretize(qv Rz, qv z, parameters par) {
	qv out,rf,cf;
	double Rmin,Rth,intp,tmp;
	int i,zi,idelt,k;
	intp = log(10.0)/par.eppd;
	Rth = Rz.sum()*(intp);
	Rmin = 1e-5*Rth;
	idelt = 1;
	i=0; 	
	do {
		tmp = 0.5*(Rz.v[i]+Rz.v[i+idelt]);
		for(k=i+1;k<i+idelt;k++) {
			tmp+=Rz.v[k];
		}
		
		tmp*=intp;
		
		if (tmp>=Rmin) {
			rf.push(tmp); }
		else {
			rf.push(Rmin); }

		cf.push(sqrt( (exp(z.v[i]) * exp(z.v[i+idelt])))/(rf.v[rf.size()-1]));
		i+=idelt;
 		if(i>Rz.size()-2) idelt=0;
	} while(idelt>0);
	
	for(i=0;i<rf.size();i++) {
		out.push(rf.v[i]);
		out.push(cf.v[i]);
	}
	
	return out;
}
