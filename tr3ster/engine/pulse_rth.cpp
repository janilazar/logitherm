#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"

qv pulse_rth(qv d, qv t, qv tauint, qv delt) {
	qv out;
	int i,j,k,l,m;
	double s,ss,fact,dzz;
	double dlowerlimit,dupperlimit,zlowerlimit,zupperlimit,dz;
	double w,z,delta,dppd;
	int nweig, kweig, npthres, nlog;
	qv weig, pthtim, pthres;

  /********************  Beállítások, allokációk  *************/

	nlog=d.size();
	s=t.v[1]/t.v[0];
	if(s<1.0) s=1.0/s;
	dppd=1.0/log10(s);

	dlowerlimit=-5.0;                    /* also hatar dekadban */
	dupperlimit=1.0;                     /* felso hatar dekadban */

	zlowerlimit=dlowerlimit*log(10.0);
	zupperlimit=dupperlimit*log(10.0);
	dz=(1.0/dppd)*log(10.0);

	nweig=(int)((zupperlimit-zlowerlimit)/dz);
	kweig=(int)(-zlowerlimit/dz);

	weig.pushn(nweig);

	npthres=nweig+nlog-1;
	
	pthtim.pushn(npthres);
	pthres.pushn(npthres*delt.size());

  /*************************  Számítás  ***********************/

	for ( i = 0 ; i < delt.size() ; i++ ) {	 
		delta=delt.v[i];
		for ( j = 0 ; j < nweig ; j++ )	{			 
			z=(double)(j-kweig)*dz;
			if(delta<1e-6) w=(1.0-exp(-exp(z)));
			else           w=(1.0-exp(-exp(z)))/(1.0-exp(-exp(z)/delta));
			weig.v[j]=w;
		}
		
				for ( l = 0 ; l < npthres ; l++ )
					pthtim.v[l]=0.0;

				dzz=1.0/(dppd/log(10.0));       /* mert NEWTI K/W/- tcn-t ad */
				for ( l = 0 ; l < npthres ; l++ ) {
					ss=0.0;
					for ( m = 0 ; m < nlog ; m++ ) {
						k=l-m;
						if(k<0) k=0;
						if(k>nweig-1) k=nweig-1;
						ss+=weig.v[k]*tauint.v[m];
						if(k==kweig) pthtim.v[l]=t.v[m];
					}
					pthres.v[i*npthres+l]=ss*dzz;
				}

				fact=pow(10.0,1.0/dppd);
				for(l=0;l<npthres;l++)
					if(pthtim.v[l]!=0.0) break;
				m=l;
				ss=pthtim.v[m];
				for ( l = m-1 ; l >=0 ; l-- ) {
					ss/=fact;
					pthtim.v[l]=ss;
				}
				for ( l = npthres - 1 ; l >=0 ; l-- )
					if(pthtim.v[l]!=0.0) break;
				m=l;
				ss=pthtim.v[m];
				for ( l = m + 1 ; l < npthres ; l++ ) {
					ss*=fact;
					pthtim.v[l]=ss;
				}
    }
	for ( i = 0 ; i < npthres ; i++ ) {		 
		out.push(pthtim.v[i]);
		for ( j = 0 ; j < delt.size() ; j++ )
			out.push(pthres.v[j*npthres+i]);
	}
	return out;
}
