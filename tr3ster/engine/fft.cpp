#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"
#include <fftw3.h>
#include <complex.h>
#include <math.h>

qv deconvolution_fft(qv deriv, qv z, double gp, double zaj) {
	qv output,tmp,tmp2;
	fftw_complex *in;
	fftw_complex *out;
	fftw_plan p;
	fftw_complex *iin, *iout;
	fftw_plan ip;	
	double noise,gparam,tmin;
	double Xmin,Xmax,DX;
	int DN,i;

	DN = deriv.size();
	Xmin = z.v[0];
	Xmax = z.v[DN-1];
	DX = Xmax - Xmin;
	
	complex double data[DN],Wt[DN],Ivar[DN];
	double dout[DN],K[DN],Phi[DN],Gauss[DN];

	for(i=0;i<DN;i++) {	data[i]=deriv.v[i]+0*I; }
	
	noise = pow(10,zaj);
	gparam = gp/log(10);

/***************  A Fourier transzformáció  ************************/	

	in  = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * DN);
	out = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * DN);
	for (i=0;i<DN;i++) { in[i] = data[i]; }
	p = fftw_plan_dft_1d(DN, in, out, FFTW_FORWARD, FFTW_ESTIMATE);
	fftw_execute(p);	

	for (i=0;i<DN;i++) { data[i] = out[i];}

// Ha az FFT eredménye érdekel minket, akkor benne van a dout-ban.
/*	for (i=0;i<DN;i++) {
		dout[i]=(cabs(data[i]));		//*(DX/DN);
		K[i]=i+1;
	}
*/
	fftw_destroy_plan(p);
	fftw_free(in); fftw_free(out);

/**  A Gauss szűrő és a Wt súlyfüggvény előállítása, dekonvolúció **/

	for(i=0;i<DN;i++) {	
		Phi[i] = (K[i]-1)/DX; 
		Gauss[i] = exp(-1*pow((Phi[i]/gparam),2));
		Wt[i] = -2*M_PI*I*Phi[i]*cgamma(-2*M_PI*I*Phi[i])+noise;
		Wt[0] = 1;
		Ivar[i] = (data[i]*Gauss[i])/Wt[i];
	}

//for (i=0;i<DN;i++) { printf("%e\n",cabs(Ivar[i]));	}

/***************  Az inverz Fourier transzformáció  ****************/	
	
	iin = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * DN);
	iout = (fftw_complex*) fftw_malloc(sizeof(fftw_complex) * DN);
	for (i=0;i<DN;i++) { iin[i] = Ivar[i]; }
	ip = fftw_plan_dft_1d(DN, iin, iout, FFTW_BACKWARD, FFTW_ESTIMATE);
	fftw_execute(ip);

	for (i=0;i<DN;i++) { tmp.push(creal(iout[i])*(DX));	}

	fftw_destroy_plan(ip);
	fftw_free(iin); fftw_free(iout);
	tmin=tmp.min();
	output = tmp - tmin;


	return output;
}
