#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"

/*******************************************
 * 	Ha mode = 1 -> Lokális minimum keresés
 * 	Ha mode = 2 -> Inflexiós pont keresés
 * 	Ha gorbe = 1 -> Egyenes illesztése
 * 	Ha gorbe = 2 -> Gyökfüggvény illesztése
 * 	Ha gorbe = 3 -> Levágás
 *******************************************/

qv filter(qv data, qv time, double start, double end, int mode, int gorbe) {
	qv out,tout,localt,localdata;
	double m;
	tout = data;
	int istart,iend,itmp,i,cut;
	cut=0;
	istart = keres(time,start);
	iend = keres(time,end);
	if(mode==1) { itmp = data.lokmin(istart, iend); }
	if(mode==2) { itmp = data.lokinf(istart, iend); }
	if(gorbe==1 or gorbe==3) { for(i=0;i<itmp;i++) { tout.v[i] = tout.v[itmp]; } }

	if(gorbe==2) {
		localt = qvsqrt(time.sel(0,iend));
		m = (data.v[iend] - data.v[itmp])/(localt.v[iend]-localt.v[itmp]);
		for(i=iend-1;i>=0;i--) {
				tout.v[i]=data.v[itmp]-m*(localt.v[iend]-localt.v[itmp]);
		}
	}
	
	if(gorbe==3) { cut=itmp; }
	if(gorbe!=2) {
		for(i=cut;i<time.size();i++) {
			out.push(time.v[i]);
			out.push(tout.v[i]);
		}
	}
	else {
		for(i=cut;i<=iend;i++) {
			out.push(localt.v[i]);
			out.push(tout.v[i]);
		}		
		for(i=iend+1;i<time.size();i++) {
			out.push(time.v[i]);
			out.push(tout.v[i]);
		}
	}
	return out;
}
