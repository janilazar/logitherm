#ifndef _TR3STER_ENGINE_H_
#define _TR3STER_ENGINE_H_

void calculate_time_spectra(qv& tt, qv& dd, double powerstep, qv& tau, qv& z);

#endif //_TR3STER_ENGINE_H_