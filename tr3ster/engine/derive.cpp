#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"

/**********************************
 * 	Egyszerű tolóablakos átlagoló
 * 	(moving average) implementáció
 * 	a zajos függvény kisimogatására
 **********************************/

using namespace std;

qv fastsmooth(qv be, int smoothwidth) {
	qv out;
	qv s;
	int k, halfw;
	double SumPoints;
	
	SumPoints = be.sum(0,smoothwidth-1);
	s.pushn(be.size());
	halfw=(int)(smoothwidth/2);
	for(k=0;k<(be.size()-smoothwidth);k++) {
		s.v[k+halfw]=SumPoints;
		SumPoints = SumPoints - be.v[k];
		SumPoints = SumPoints + be.v[k+smoothwidth];
	}
	out=s/smoothwidth;
	for(k=1;k<=halfw;k++) {
		out.pop();
		out.popf();
	}
	return out;
}
