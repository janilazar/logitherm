#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"

/**********************************
 * 	Bayes iteráció, dekonvolúció
 **********************************/

qv bayes(qv deriv, int nit, double eppd)
 {
	int i, it, itmax;
	qv weig;
	int nw;
	int w_refpos;
	double dz;
	qv tau;
	qv a, b;

	dz = log(10.0)/eppd;
	w_refpos = (int)(8.5/dz);
	nw = (int)(11.5/dz);

	weig.gen_wfunc(nw, w_refpos, dz);
	tau = deriv;
	itmax = nit;
	for( it = 0 ; it<itmax ; it++)
	 {
		a=convolut(tau,weig,w_refpos);
		b=deriv/a;
		a=correlat(b,weig,w_refpos);
		tau=tau*a;
	 }

	return tau;
 }

qv deconvolution_b(qv deriv, int nit, double eppd)
 {
	int i;
	TYPE dmax;
	qv taus;
	dmax = deriv.max()*1e-4;
	for (i=0 ; i<deriv.size() ; i++) { 
		if( deriv.v[i] < dmax ) { 
			deriv.v[i] = dmax ; 
		} 
	}
	taus = bayes(deriv, nit, eppd);

	return taus;
 }
