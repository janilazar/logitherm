#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"
#include "tr3ster/engine/nubs.h"

/***********************************************
 * 	Ha MODE = 1 -> Lokális minimum keresés
 * 	Ha MODE = 2 -> Inflexiós pont keresés
 ***********************************************/
const static int MODE = 1;

/***********************************************
 * 	Ha GORBE = 1 -> Egyenes illesztése
 * 	Ha GORBE = 2 -> Gyökfüggvény illesztése
 * 	Ha GORBE = 3 -> Levágás
 ***********************************************/
const static int GORBE = 1;

const static int BAYES = 1024;
const static int GAUSS = 2;
const static int NOISE = -1;
const static int EPPD = 60;

//atdolgozni

void calculate_time_spectra(qv& tt, qv& dd, double powerstep, qv& tau, qv& z)
 {
	int i;
	// if (argc<2) { cout << "Keves parameter!\n"; return 1; }
	parameters par;
	qv elotmp,newt,newd,newder; //,tt,dd;
	qv t,d,tauf,der,tmp,newtime,newdata,nt;
	qv foster,rf,cf,cauer,rc,cc,loci,lot,lore,loim;
	qv pulserth,delt,pt,pd1,pd2,pd3,pd4;
	qv tmp2,tauftmp;
	double factor;

/************ Tesztelés  ***************************/

	par.timescale = 1.0;
	par.eppd = EPPD;
	par.powerstep = powerstep;
	// t = csvread(argv[1], 1);
	// tt = t/par.timescale;
	// dd = csvread(argv[1], 2);
	

/********************** Paraméterek beállítása *****/

	// t = rawfileread(argv[2],1);
	// d = rawfileread(argv[2],2); 
	// par = parfileread(argv[1]);
	// d = d*(par.lsb/par.sensit);
	// par.eppd = EPPD;
	// par.timescale = 1e-6;

	// csvwrite("DATA.txt",t,d);

/********** Elősimítás *****************************/

	// elotmp = regress(d, t, par);
	// 	for(i=0;i<elotmp.size();i+=3) {
	// 		newt.push(elotmp.v[i]);
	// 		newd.push(elotmp.v[i+1]);
	// 		newder.push(elotmp.v[i+2]);
	// 	}

/*************** Szűrés ****************************/

	// tmp2 = filter(newd, newt, 3.0, 7.0, MODE, GORBE);
	// 	for(i=0;i<tmp2.size();i+=2) {
	// 		tt.push(tmp2.v[i]);
	// 		dd.push(tmp2.v[i+1]);
	// 	}

/****** Simítás, derivált függvény előállítása *****/
	tmp = regress(dd, tt, par);
//	tmp = regress(d, t, par);
		for(i=0;i<tmp.size();i+=3) {
			newtime.push(tmp.v[i]);
			newdata.push(tmp.v[i+1]);
			der.push(tmp.v[i+2]);
		}

/****** Komplex gyökhelygörbe kiszámítása **********/
	nt = newtime*par.timescale;
	loci = comploci(nt, newdata, der, par);
		for(i=0;i<loci.size();i+=3) {
			lot.push(loci.v[i]);
			lore.push(loci.v[i+1]/par.powerstep);
			loim.push(loci.v[i+2]/par.powerstep);
		}
		
/** Az időállandó spektrum kiszámítása (Bayes)  ****/
	
	tau = deconvolution_b(der,BAYES,par.eppd)/par.powerstep;
	z = qvlog(nt);

/** Az időállandó spektrum kiszámítása (Fourier)  **/
	tauftmp = deconvolution_fft(der,z,GAUSS,NOISE);
	 // Normálás
		factor = (tauftmp.sum()*(log(10)/par.eppd))/(newdata.v[newdata.size()-1]/par.powerstep);
		tauf = tauftmp / factor;

/****** Foster hálózat generálása ******************/

	// foster = discretize(tau, z, par);
	// 	for(i=1;i<foster.size();i=i+2) {
	// 		rf.push(foster.v[i-1]);
	// 		cf.push(foster.v[i]);
	// 	}

/****** Cauer hálózat generálása *******************/

	// cauer = fostcaur(rf, cf);
	// 	for(i=1;i<cauer.size();i=i+2) {
	// 		rc.push(cauer.v[i-1]);
	// 		cc.push(cauer.v[i]);
	// 	}

/********* Pulse Rth kiszámítása *******************/
/*
	delt.push(0.5);delt.push(0.25);delt.push(0.1);delt.push(0.05);
	pulserth = pulse_rth(newdata, nt, tau, delt);
		for ( i = 0 ; i < pulserth.size() ; i=i+delt.size()+1 ) 
		{		 
			pt.push(pulserth.v[i]);
			pd1.push(pulserth.v[i+1]);
			pd2.push(pulserth.v[i+2]);
			pd3.push(pulserth.v[i+3]);
			pd4.push(pulserth.v[i+4]);
		}
*/
//cc.print();
 }
