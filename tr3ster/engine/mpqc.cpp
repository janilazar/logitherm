#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"

/**********************************
 * 	Foster - Cauer transzformáció
 **********************************/

#define CONT deque 

#ifdef _double
#define TTYP double
#endif
#ifdef _mpf
#define TTYP mpf_class
#endif
#ifdef _mpq
#define TTYP mpq_class
#endif

using namespace std;

class fqv
 {
	public:
		CONT<TTYP> v;
		fqv() {}
        CONT<TTYP>::iterator iter;
        void push(TTYP x)  { v.push_back(x); }
#ifndef _double
		void push(double x) { v.push_back(TTYP(x)); }
#endif 
		void push(int x) { v.push_back(TTYP(x)); }
		void pushf(int x) { v.push_front(TTYP(x)); }
        CONT<TTYP>::iterator begin() { return v.begin(); }   // Az elso elemet adja vissza
        CONT<TTYP>::iterator end() { return v.end(); }       // Az utolsot adja vissz
        void pop() { v.pop_back(); }
		void popf() { v.pop_front(); }
		void clear() { v.clear(); }
		int size() { return v.size(); }
		fqv sel(int start, int end) {
			fqv out;
			int i;
			for(i=start;i<=end;i++) { out.push(v[i]); }		
			return out;
		 }
        void print() { for( iter = v.begin(); iter != v.end(); iter++ ) { cout << *iter << endl; } }
        fqv operator=(const fqv &a) { v = a.v; return *this; }
        fqv operator+(const fqv &a) { 
					fqv out;
					int n;
					for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]+a.v[n]); }
					return out; 
				 }
        fqv operator-(const fqv &a) { 
                        fqv out;
                        int n;
                        for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]-a.v[n]); }
                        return out;           
                 }
        fqv operator*(const fqv &a) {
                        fqv out;
                        int n;
                        for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]*a.v[n]); }
                        return out;
                 }
        fqv operator/(const fqv &a) {
                        fqv out;
                        int n;
                        for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]/a.v[n]); }
                        return out;
                 }
 };

fqv conv(fqv a, fqv b)
 {
	fqv out;
	int n, i, j;
	for(n=((a.size()-1)+(b.size()-1)+1);n>0;n--) { out.push(0); }
	for(i=a.size()-1;i>=0;i--)
	 {
		for(j=b.size()-1;j>=0;j--)
		 {
			out.v[i+j] = out.v[i+j] + (a.v[i] * b.v[j]);
		 }
	 }
	return out;
 }


qv fostcaur(qv qrf, qv qcf)
 {
	qv out;
	fqv sz0, n0, sz1, n1, n, d, tmp1, tmp2, cc, rc, Cvtmp, Rvtmp, ttt, rf, cf;
	int i, j, num, count;
	double tmp;
	TTYP Ctmp, Rtmp;
	
	for(i=0;i<qrf.size();i++) {
		rf.push(qrf.v[i]);
		cf.push(qcf.v[i]);
	}
	
/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Input parameters
%
%   sz0: the constant term of the numerators
%   n0: the constant term of the denominators
%   sz1: the s multiplied term of the numerators
%   n1: the s multiplied term of the denominators
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/	
	sz0 = rf;
	for(i=1;i<=rf.size();i++) { n0.push(1); }
	for(i=1;i<=rf.size();i++) { sz1.push(0); }
	n1 = rf*cf;
	num = rf.size();
/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate the polynom
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/
	n.push(sz1.v[0]);
	n.push(sz0.v[0]);
	d.push(n1.v[0]);
	d.push(n0.v[0]);

	for(i=1;i<=rf.size()-1;i++)
	 {
		tmp1.push(n1.v[i]); tmp1.push(n0.v[i]);
		tmp2.push(sz1.v[i]); tmp2.push(sz0.v[i]);		

		n = conv(n,tmp1) + conv(d,tmp2);
		d = conv(d,tmp1);		

		tmp1.pop();tmp1.pop();tmp2.pop();tmp2.pop();
	 }
/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Transformation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/
	count = n.size();
	for(i=1;i<=rf.size();i++)
	 {
                if (n.size()>count-i+1)
                 {
                        num = n.size()-(count-i+1);
                        for(j=1;j<=num;j++) { n.popf(); };
                 }
                if (d.size()>count-i+1)
                 {
                        num = d.size()-(count-i+1);
                        for(j=1;j<=num;j++) { d.popf(); };
                 }
		Ctmp = d.v[0] / n.v[1] ;
		cc.push(Ctmp);
		for(j=1;j<=d.size();j++) { Cvtmp.push(0); }
		Cvtmp.v[Cvtmp.size()-2] = Ctmp;
		num = Cvtmp.size()+n.size()-d.size();
		if(num>0) { for(j=1;j<num;j++) { d.pushf(0); } }
		d = d - conv(Cvtmp,n);
		if (n.size()>count-i)
		 {
			num = n.size()-(count-i);
			for(j=1;j<=num;j++) { n.popf(); };
		 }
		if (d.size()>count-i)
		 {
			num = d.size()-(count-i);
			for(j=1;j<=num;j++) { d.popf(); };
		 }
		Rtmp = n.v[0] / d.v[0] ;
		rc.push(Rtmp);
		for(j=1;j<=n.size()-1;j++) { Rvtmp.push(0); }
		Rvtmp.push(Rtmp);
		num = Rvtmp.size()+d.size()-n.size();
		if(num>0) { for(j=1;j<num;j++) { n.pushf(0); } }
		n = n - conv(Rvtmp,d);
		Rvtmp.clear();
		Cvtmp.clear();
	 }

#ifdef _double
		for(i=0;i<rf.size();i++) {
			tmp = rf.v[i];
			out.push(tmp);
			tmp = cf.v[i];
			out.push(tmp);
		}
#endif

#ifdef _mpf
		for(i=0;i<rf.size();i++) {
			tmp = rf.v[i].get_d();
			out.push(tmp);
			tmp = cf.v[i].get_d();
			out.push(tmp);
		}
#endif

#ifdef _mpq
		for(i=0;i<rf.size();i++) {
			tmp = rf.v[i].get_d();
			out.push(tmp);
			tmp = cf.v[i].get_d();
			out.push(tmp);
		}
#endif

	return out;
 }
