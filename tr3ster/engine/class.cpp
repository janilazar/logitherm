#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"

/**********************************
 * 	A qv osztaly kifejtese
 **********************************/

void qv::push(TYPE x)  { v.push_back(x); }
void qv::push(int x) { v.push_back(TYPE(x)); }
void qv::pushf(int x) { v.push_front(TYPE(x)); }
void qv::pushn(int x) {
		int i;
		for (i=0;i<x;i++) {v.push_back(TYPE(0)); }
	}
CONT<TYPE>::iterator qv::begin() { return v.begin(); }   // Az elso elemet adja vissza
CONT<TYPE>::iterator qv::end() { return v.end(); }       // Az utolsot adja vissz
void qv::pop() { v.pop_back(); }
void qv::popf() { v.pop_front(); }
void qv::clear() { v.clear(); }
int qv::size() { return v.size(); }
qv qv::sel(int start, int end) {
	qv out;
	int i;
	for(i=start;i<=end;i++) { out.push(v[i]); }
	return out;
 }
void qv::print() { for( iter = v.begin(); iter != v.end(); iter++ ) { cout << *iter << endl; } }

qv qv::operator=(const qv &a) { v = a.v; return *this; }
qv qv::operator+(const qv &a) {
	qv out;
	int n;
	for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]+a.v[n]); }
	return out;
 }
qv qv::operator+(double a) {
	qv out;
	int n;
	for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]+a); }
	return out;	
}
qv qv::operator-(const qv &a) {
	qv out;
	int n;
	for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]-a.v[n]); }
	return out;
 }
 qv qv::operator-(double a) {
	qv out;
	int n;
	for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]-a); }
	return out;	
}
qv qv::operator*(const qv &a) {
	qv out;
	int n;
	for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]*a.v[n]); }
	return out;
 }
qv qv::operator*(double a) {
	qv out;
	int n;
	for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]*a); }
	return out;
 }
qv qv::operator/(const qv &a) {
	qv out;
	int n;
	for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]/a.v[n]); }
	return out;
 }
qv qv::operator/(int a) {
	qv out;
	int n;
	for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]/a); }
	return out;
 }
qv qv::operator/(double a) {
	qv out;
	int n;
	for( n=0; n<v.size(); n++ ) { out.v.push_back(v[n]/a); }
	return out;
 }
TYPE qv::max() {
	int i;
	TYPE out = v[0];
	for(i=0;i<v.size();i++) { if(v[i]>out) { out = v[i]; } }
	return out;
 }
TYPE qv::min() {
	int i;
	TYPE out = v[0];
	for(i=0;i<v.size();i++) { if(v[i]<out) { out = v[i]; } }
	return out;
 }
TYPE qv::sum() {
	int i;
	TYPE out = 0;
	for(i=0;i<v.size();i++) { out = out + v[i]; }
	return out;
 }
TYPE qv::sum(int start, int stop) {
	int i;
	TYPE out = 0;
	for(i=start;i<=stop;i++) { out = out + v[i]; }
	return out;
 }
int max(int a, int b) {
	if (a<b) { return b; }
	else { return a; }
 }
int min(int a, int b) {
	if (a<b) { return a; }
	else { return b; }
 }
qv qv::diff() {
	qv out;
	int i;
	for(i=1;i<v.size();i++) {
		out.push(v[i]-v[i-1]);
	}
	return out;
 }
 qv qv::abs() {
	qv out;
	int i;
	for(i=0;i<v.size();i++) {
		out.push(fabs(v[i]));
	}
	return out;
 }
void qv::gen_wfunc(int num, int refpos, double dz) {
	int i;
	double z;

	for(i=0 ; i < num ; i++)
	 {
		z = dz * (double)(i-refpos);
		v.push_back(exp(z-exp(z))*dz);
	 }
 }
int qv::lokmin(int start, int stop) {
	int out,i;
	out = start;
	for(i=start;i<=stop;i++) { 
		if (v[i]<v[out]) { out = i; }
	}	
	return out;
}
int qv::lokinf(int start, int stop) {
	int out,i;
	qv tmp;
	out = start;
	for(i=start;i<=stop;i++) { 
		tmp.push(v[i]); 
	}		
	tmp.diff();
	for(i=1;i<tmp.size();i++) {
		if(sign(tmp.v[i])==-1*sign(tmp.v[i-1])) { out=i+start; }
	}
	return out;	
}

// Mas fuggvenyek.
int keres(qv miben, TYPE mit) {
	int out,i;
	qv tmp;
	out = 0;
	tmp = miben-mit;
	tmp = tmp.abs();
	for(i=1;i<tmp.size();i++) { 
		if(tmp.v[i]<tmp.v[out]) { out = i; } }	
	return out;
}
int sign(double a) {
	int out;
	out = (int)a/fabs(a);
	return out;
}
qv convolut(qv a, qv b, int refw) {
	int i, j, jmin, jmax;
	double s;
	qv out;

	for (i=0;i<a.size();i++)
	 {
		jmin=max(0,i-b.size()+1+refw);
		jmax=min(a.size()-1,i+refw);
		s=0.0;
		for(j=jmin;j<=jmax;j++) { s+=a.v[j]*b.v[i-j+refw]; }
		out.push(s);
	 }
	return out;
 }
qv correlat(qv a, qv b, int refw) {
	int i, j, jmin, jmax;
	double s;
	qv out;

	for (i=0;i<a.size();i++)
	 {
		jmin=max(0,i-refw);
		jmax=min(a.size()-1,i-refw+b.size()-1);
		s=0.0;
		for(j=jmin;j<=jmax;j++) { s+=a.v[j]*b.v[j-i+refw]; }
		out.push(s);
	 }
	return out;	
 }
qv csvread(char* filename, int o) {
	FILE *fp;
	qv a,b;
	TYPE x,y;
	int i;

	if((fp=fopen(filename,"r")) == NULL) {
	  printf("Cannot open file.\n");
	  exit(1);
	}

	while(feof(fp)==0)
	{	
		fscanf(fp, "%lf,%lf", &x, &y); /* read from file */
		i++;
		a.push(x);
		b.push(y);
	}
	fclose(fp);

	if (o==1) { return a; } else { return b; }
 }
void csvwrite(char* filename, qv x, qv y) {
	FILE *fp;
	int i;
	if((fp=fopen(filename,"w")) == NULL) {
	  printf("Cannot open file.\n");
	  exit(1);
	} 
	for(i=0;i<x.size();i++)
	 {
		fprintf(fp, "%e,%e \n",x.v[i],y.v[i]); 
	 }
	 fclose(fp);
 }
qv txtread(char* filename, int o) {
	FILE *fp;
	qv a,b,c,d,e;
	TYPE x,y,v,z,q;
	int i;

	if((fp=fopen(filename,"r")) == NULL) {
	  printf("Cannot open file.\n");
	  exit(1);
	}

	while(feof(fp)==0)
	{	
		fscanf(fp, "%lf %lf %lf %lf %lf", &x, &y, &v, &z, &q); /* read from file */
		i++;
		a.push(x);
		b.push(y);
		c.push(v);
		d.push(z);
		e.push(q);
	}
	fclose(fp);

	if (o==1) { return a; } 
	if (o==2) { return b; }
	if (o==3) { return c; } 
	if (o==4) { return d; }
	else {return e; }
 }
qv qvlog(qv a) {
	qv out;
	int i;
	for(i=0;i<a.size();i++) {
		out.push(log(a.v[i]));
	}	
	return out;	
}
qv qvexp(qv a) {
	qv out;
	int i;
	for(i=0;i<a.size();i++) {
		out.push(exp(a.v[i]));
	}	
	return out;		
}
qv qvsqrt(qv a) {
	qv out;
	int i;
	for(i=0;i<a.size();i++) {
		out.push(sqrt(a.v[i]));
	}	
	return out;		
}
// A stringvect osztaly kifejtese

void stringvect::push(string x) { v.push_back(string(x)); }
CONT<string>::iterator stringvect::begin() { return v.begin(); }
CONT<string>::iterator stringvect::end() { return v.end(); }
void stringvect::pop() { v.pop_back(); }
void stringvect::clear() { v.clear(); }
int stringvect::size() { return v.size(); }
void stringvect::print() { for( iter = v.begin(); iter != v.end(); iter++ ) { cout << *iter << endl; } }

stringvect blowstring(string str) {
	stringvect out;
	string tmp;
	char ch;
	int i,f;
	f=0;
	for(i=0;i<str.size();i++) {
			ch = str[i];
			if(ch==' ' and f==0) { out.push(tmp) ; tmp.clear(); f=1; }
			if(ch!=' ') { tmp.push_back(ch) ; f=0; }
			if(i==str.size()-1) { out.push(tmp) ; tmp.clear(); }
	}	
	return out; 
 }










