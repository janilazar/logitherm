#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"

/**********************************
 * 	Regressziós deriválás
 **********************************/

qv regress(qv d, qv time, parameters par) {
	qv out;
	int i,j,k;
	double ulsb,senscoeff,eppd,s;
	double tmin,tmax,tmult,t,tdata,timescale;
	int nlog,n,nint,nlastone,nsampl;
	double a, b;
	qv nperint, fpt, newtime, newdata, der;
    double    sx,sy,sxy,sx2,sumweight,weight,xa;
    double    logtmult,logt,ya;
	
/************ Kulcsparaméterek beállítása **************/
	eppd = par.eppd;
	ulsb = par.lsb;
	senscoeff = par.sensit;
	timescale = 1;
	
/***************** tmin tmax keresése ******************/
	for(i=0;time.v[i]==0.0;i++);
	
	tmin = time.v[i]*timescale;
	tmax = time.max()*timescale;

/*******************************************************/
	nlog = (int)(log10(tmax/tmin)*eppd);
	par.nlog = nlog;
	tmult = pow(10.0,1.0/eppd);
	t = tmin * tmult;
	n=nint=0;
	nperint.pushn(nlog+1);
	fpt.pushn(nlog+2);
	
	for(i=0;i<d.size();i++) {
		tdata = timescale * time.v[i];
		if( tdata > t ) {
			nperint.v[n]=nint;
			n++;
			fpt.v[n]=i;
			nint = 1;
			t *= tmult;
			while( tdata > t ) {
				nperint.v[n]=0;
				n++;
				fpt.v[n]=i;
				t *= tmult;
			}
		}
		else {
			nint++;
		}
	}
	
/******** Az utolsó intervallum ahol 1 adat van **********/
	nlastone=0;
    for (i=0;i<nlog;i++) {
        if (nperint.v[i]<2) {
            nlastone=i; 
        }
	}
    if (nlastone<3) { 
		nlastone=3; 
	}
	
/********** Első szakasz, lineáris interpoláció ********/
	newtime.pushn(nlog-2);
	newdata.pushn(nlog-2);
	der.pushn(nlog-2);
	s = ulsb/senscoeff;
	t = tmin;
	j = 0;
    for (i=0;i<=nlastone;i++) {
        t*=tmult;
        newtime.v[i]=t;
        while (time.v[j]<t)
            j++;
        b=(d.v[j]-d.v[j-1])/(time.v[j]-time.v[j-1]);
        newdata.v[i]=d.v[j-1]+b*(t-time.v[j-1]);
        der.v[i]=b*t;
    }
	
/** Második szakasz, függvény és derivált számítása regresszióval **/
    logtmult=log(tmult);
    for (i=nlastone+1;i<nlog-2;i++) {
        j=fpt.v[i-2];
        t*=tmult;
        logt=log(t);
        nsampl=nperint.v[i-2]+nperint.v[i-1]+nperint.v[i]+nperint.v[i+1];
        sx=sy=sxy=sx2=sumweight=0.0;
        for (k=0;k<nsampl;k++) {
            xa=log(time.v[j]);
            ya=d.v[j];
            j++;
            weight=1.0-fabs(xa-logt)/(2.0*logtmult);
            if (weight<0.0)
                weight=0.0;
            sx+=weight*xa;
            sy+=weight*ya;
            sxy+=weight*xa*ya;
            sx2+=weight*xa*xa;
            sumweight+=weight;
        }
        b=(sumweight*sxy-sx*sy)/(sumweight*sx2-sx*sx);
        a=(sy-sx*b)/sumweight;
        newtime.v[i]=t;
        newdata.v[i]=a+log(t)*b;
        der.v[i]=b;
    }
	for(i=0;i<der.size();i++) {
		out.push(newtime.v[i]);
		out.push(newdata.v[i]);
		out.push(der.v[i]);
	}	
	return out;
}
