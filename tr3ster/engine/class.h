#ifndef _CLASS_H_
#define _CLASS_H_

#include <iostream>
#include <fstream>
#include <istream>
#include <vector>
#include <deque>
#include <cstdlib>
#include <gmpxx.h>
#include <math.h>
#include <complex.h>
#include <string>
#include <sstream>

#define CONT deque 

#define TYPE double


/**********************************
 * 	Ez az alaposztálya a belső
 * 	adatfolyamnak. 
 **********************************/

using namespace std;

class qv
 {
        public:
                CONT<TYPE> v;
                qv() {}
                CONT<TYPE>::iterator iter;
                void push(TYPE x);
                void push(int x);
				void pushn(int x);
                void pushf(int x);
                CONT<TYPE>::iterator begin();
                CONT<TYPE>::iterator end();
                void pop();
                void popf();
                void clear();
                int size();
                qv sel(int start, int end);
                void print();
                qv operator=(const qv &a);
                qv operator+(const qv &a);
                qv operator+(double a);
                qv operator-(const qv &a);
                qv operator-(double a);
                qv operator*(const qv &a);
                qv operator*(double a);
                qv operator/(const qv &a);
                qv operator/(int a);
                qv operator/(double a);
        qv abs();
		TYPE max();
		TYPE min();
		TYPE sum();
		TYPE sum(int start, int stop);
		int lokmin(int start, int stop);
		int lokinf(int start, int stop);
		qv diff();

		void gen_wfunc(int num, int refpos, double dz);
 };

class stringvect
 {
        public:
                CONT<string> v;
                stringvect() {}
                CONT<string>::iterator iter;
                void push(string x);
                CONT<string>::iterator begin();
                CONT<string>::iterator end();
                void pop();
                void clear();
                int size();
                void print();
 };

class parameters
 {
	public:
		parameters() {}
		int channels;
		int drivechan;
		double powerstep;
		int sampleoct;
		double lsb;
		double uref;
		double sensit;
		double eppd;
		double nlog;
		double timescale;
 };

#endif //_CLASS_H_