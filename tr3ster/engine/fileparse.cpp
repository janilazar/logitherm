#include "tr3ster/engine/class.h"
#include "tr3ster/engine/proto.h"

/**********************************
 * 	Régi paraméter és adatállomány
 * 	beolvasása. (.PAR,.REC,.RAW)
 **********************************/

using namespace std;

parameters parfileread(char* f) {
	parameters out;
	int i=0,j,tmp;
    ifstream inFile(f);
    string strOneLine;
	stringvect test;
    while (inFile)
    {
		i++;
       getline(inFile, strOneLine);
			if(i==5) { out.channels = (int)(strOneLine[10])-48; }	
			if(i>5) {
				test = blowstring(strOneLine);	
				if(test.v[0]=="POWERSTEP=") {
					out.powerstep = strtod(test.v[1].c_str(), NULL); 
				}
				if(test.v[0]=="CH_DRIVPOI=") {
						for(j=0;j<=out.channels;j++) {
								tmp = (int)strtod(test.v[j].c_str(), NULL);
								if(tmp==1) { out.drivechan = j; }
						}
				}
				if(test.v[0]=="SAMPLE/OCT=") {
					out.sampleoct = strtod(test.v[1].c_str(), NULL); 
				}
				if(test.v[0]=="CH_SENSIT=") {
					out.sensit = strtod(test.v[1].c_str(), NULL); 
				}
				if(test.v[0]=="CH_LSB=") {
					out.lsb = strtod(test.v[out.drivechan].c_str(), NULL);
				}			
				if(test.v[0]=="CH_UREF=") {
					out.uref = strtod(test.v[out.drivechan].c_str(), NULL);
				}		
			}
    }
    inFile.close();
	return out;
}

qv rawfileread(char* f, int s) {
	int i=0,j,tmp;
	int channels,drivechan;
    ifstream inFile(f);
    string strOneLine;
	stringvect test;
	qv t,d;
    while (inFile)
    {
		i++;
       getline(inFile, strOneLine);
			test = blowstring(strOneLine);		 
			if(i==4) { channels = (int)(strOneLine[10])-48; }	
			if(i==6) {
						for(j=0;j<=channels;j++) {
							tmp = (int)strtod(test.v[j].c_str(), NULL);
							if(tmp==1) { drivechan = j; }
					}
			}		
			if(i>=6) {
				 t.push((int)strtod(test.v[0].c_str(), NULL));
				 d.push((int)strtod(test.v[drivechan].c_str(), NULL));
			 }
    }

    inFile.close();
	if(s==1) {return t;}
	else {return d;}
}
