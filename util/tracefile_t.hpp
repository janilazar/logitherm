#ifndef _TRACEFILE_T_
#define _TRACEFILE_T_

#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include "util/trace_t.hpp"

namespace util
{
	/** simple wrapper **/
	class tracefile_t
	{
		private:
			friend class tracer_t;
		
			std::vector<util::trace_t*> traces;
			std::ofstream of;
			std::stringstream buffer;
			size_t buffer_size_limit;
		public:
			const std::string filename;
			const bool binary;
			
		public:
			tracefile_t() = delete;
			tracefile_t(const std::string& filename, std::ios::openmode mode = std::ios::out);
			virtual ~tracefile_t();
			
			void initialize();
			void trace();
			void reset();
			void finalize();
	};

	/**
	 * osszehasonlito functor az std::set-hez
	 * az osszehasonlitas a nev alapjan tortenik
	**/
	struct tracefile_equal
	{	 
		bool operator() (const util::tracefile_t* lhs, const util::tracefile_t* rhs) const
		{
			return lhs->filename  ==  rhs->filename;
		}
		
	};
} //namespace util

namespace std
{
	template<>
	struct hash<util::tracefile_t*>
	{	 
		size_t operator() (const util::tracefile_t* tracefile) const
		{
			return hash<string>()(tracefile->filename);
		}
		
	};
}


#endif //_TRACEFILE_T_
