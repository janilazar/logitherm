#ifndef _STRING_MANIPULATION_
#define _STRING_MANIPULATION_

#include <string>
#include <sstream>
#include <iomanip>
#include <utility>

namespace util
{
	/** returns another string which not contains the specified character **/
	inline std::string remove(std::string original_string, char ch)
	{
		while(original_string.find(ch) != std::string::npos) original_string.erase(original_string.begin() + original_string.find(ch));
		return original_string;
	}

	/** returns another string which not contains the specified character **/
	inline std::string remove(std::string original_string, std::string remove_string)
	{
		while(original_string.find(remove_string) != std::string::npos)
		{	std::string::iterator head = original_string.begin() + original_string.find(original_string);
			std::string::iterator tail = original_string.begin() + original_string.find(original_string) + original_string.length();
			original_string.erase(head, tail);
		}
		return original_string;
	}

	/** returns a new string in which the original char is replaced with new char **/
	inline std::string replace(std::string original_string, const std::string& to_replace, const std::string& the_replacement)
	{
		while(original_string.find(to_replace) != std::string::npos)
		{	std::string::iterator head = original_string.begin() + original_string.find(to_replace);
			std::string::iterator tail = original_string.begin() + original_string.find(to_replace) + to_replace.length();
			original_string.replace(head, tail, the_replacement);
		}
		return original_string;
	}

	/** returns a new string in which the original char is replaced with new char **/
	inline std::string replace(std::string original_string, char original_char, char new_char)
	{
		for(auto &ch: original_string) if(ch == original_char) ch = new_char;
		return original_string;
	}

	/** returns a new string which contains only lowercase letters **/
	inline std::string to_lower_case(std::string original_string)
	{
		for(auto &ch: original_string) ch = std::tolower(ch);
		return original_string;
	}

	template<typename type>
	inline std::string to_string(type number, unsigned precision = 6, bool scientific = true)
	{
		std::string string_number;
		std::stringstream ss;
		if(scientific) ss << std::scientific << std::setprecision(precision) << static_cast<double>(number);
		else ss << std::setprecision(precision) << number;
		ss >> string_number;
		return string_number;
	}
}

#endif //_STRING_MANIPULATION_