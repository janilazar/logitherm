#ifndef _LOG_T_
#define _LOG_T_

#include <iostream>
#include <fstream>

namespace util
{
	enum class level_t{debug, warning, error};
	
	class log_t
	{
		private:
			std::ofstream all_file;
			std::ofstream debug_file;
			std::ofstream warning_file;
			std::ofstream error_file;
			level_t level;
			
		private:
			std::string path;
		
		public:
			log_t() = delete;
			log_t(const std::string& path="/home/lazar", level_t lvl=level_t::warning);
			~log_t();
			
			void set_log_file_path(const std::string& new_path);
			void set_notification_level(level_t lvl);
			
			void debug(const char* debug_msg);
			void debug(const std::string& debug_msg);
			
			void warning(const char* warning_msg);
			void warning(const std::string& warning_msg);
			
			void error(const char* error_msg);
			void error(const std::string& error_msg);
	};
}

#endif //_LOG_T_
