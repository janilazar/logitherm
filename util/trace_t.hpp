#ifndef _TRACE_
#define _TRACE_

#include <iostream>
#include <fstream>

namespace util
{
	/**
	 * ez az osztaly egy darab trace-t reprezental
	 * tartozik hozza egy ofstream*, hogy hova irja az eredmenyt
	**/ 
	class trace_t
	{
			
		public:
			const std::string path; /** file eleresi utja **/
			const std::string id; /** id, file neveben (jellemzoen a modul neve) **/
			const std::string type; /** milyen tipusu trace? **/
			const std::string postfix; /** postfix, file neveben, valami megkulonbozteto, szimulaciora egyedi (felbontas, timestep stb.) **/
			// const std::string id;
			const std::string file; /** ez lesz a key a tracer_t-ben **/
			
			
		public:
			trace_t() = delete;
			
			trace_t(const std::string& path, const std::string& id, const std::string& type, const std::string& postfix)
			:
				path(path),
				id(id),
				type(type),
				postfix(postfix),
				file(path + id + "_" + type + ((postfix.empty()) ? "" : "_" + postfix) + ".trace")
			{}
			
			trace_t(const std::string& path, const std::string& id, const std::string& type, const std::string& postfix, const std::string& file)
			:
				path(path),
				id(id),
				type(type),
				postfix(postfix),
				file(path+file)
			{}
			
			virtual ~trace_t(){}
			
			/** inicializaciokor mi tortenjen **/
			virtual void initialize(std::ostream& os) = 0;
			
			/** mentsuk ami mentheto **/
			virtual void trace(std::ostream& os) = 0;
			
			
			/** uj ciklus->reset **/
			virtual void reset() = 0;
			
			/** file bezarasa elott hivodik meg **/
			virtual void finalize(std::ostream& os) = 0;
	};
}

#endif //_TRACE_
