#ifndef _TRACER_T_
#define _TRACER_T_

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>

#include "util/trace_t.hpp"
#include "util/tracefile_t.hpp"

namespace util
{
	class log_t;
}

namespace util
{
	class tracer_t
	{
		private:
			/**
			 * injection dependency
			**/ 
			util::log_t* log;
			
			/** tracefile-ok, bennuk vannak a hozzajuk tartozo trace-k **/
			std::unordered_map<std::string, tracefile_t*> tracefiles;
			
			bool initialized;
			
		public:
			const std::string id;

		public:
			
			tracer_t(const std::string& prefix, const std::string& base, util::log_t* log);
			
			~tracer_t();
			
			/**
			 * ez a fv megnezi, hogy a hozzaadando trace-hez tartozo file letezik-e mar, ha nem, akkor ujat hoz letre, ha igen, akkor oda berakja
			**/
			void add_trace(trace_t* trace, std::ios::openmode mode = std::ios::out);
		
			
			/** traces alapjan megnyitha a file-okat **/
			void initialize();
			
			/** vegigmeny a traces strukturan, es mindegyik trace_t-nek meghivja a trace() tagfuggvenyet **/
			void trace();

			/** reseteli az osszes tracet (uj ciklus elott kell) **/
			void reset();
			
		
	};
	
	/**
	 * osszehasonlito functor az std::set-hez
	 * az osszehasonlitas a nev alapjan tortenik
	**/
	struct tracer_equal
	{	 
		bool operator() (const util::tracer_t* lhs, const util::tracer_t* rhs) const
		{
			return lhs->id  ==  rhs->id;
		}
		
	};
} //namespace util

namespace std
{
	template<>
	struct hash<util::tracer_t*>
	{	 
		size_t operator() (const util::tracer_t* tracer) const
		{
			return hash<string>()(tracer->id);
		}
		
	};
}

#endif //_TRACER_T_
