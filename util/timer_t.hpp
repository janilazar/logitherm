#ifndef _TIMER_T_H_
#define _TIMER_T_H_

#include "unit/time_t.hpp"
#include <utility>

namespace util
{

    class timer_t
    {
        private:
            unit::time_t start_wall_time;

            unit::time_t start_cpu_time;
            
            unit::time_t get_wall_time();
            unit::time_t get_cpu_time();

        public:
            timer_t();
            void start_timer();
            
            std::pair<unit::time_t, unit::time_t> get_elapsed_time();
    };
}
#endif //_TIMER_T_H_