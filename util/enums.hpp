#ifndef _ENGINE_T_
#define _ENGINE_T_

namespace util
{
	enum class thermal_engine_t{sunred, threedice, sloth, hotspot};
	enum class logic_engine_t{systemc, sniper, verilog, vhdl};
	enum class timescale_t{lin, log};
}
#endif //_ENGINE_T_
