#include "util/tracefile_t.hpp"

namespace util
{
	tracefile_t::tracefile_t(const std::string& filename, std::ios::openmode mode)
	:
		of(filename, mode),
		buffer(std::ios::in | std::ios::out | mode),
		buffer_size_limit(1 << 15),
		filename(filename),
		binary(mode & std::ios::binary)
	{}
	
	tracefile_t::~tracefile_t()
	{
		for(auto &it: traces)
		{
			delete it;
		}
		of.close();
	}
	
	void tracefile_t::initialize()
	{
		if(traces.size() > 0)
		{
			std::vector<trace_t*>::iterator it = traces.begin();
			(*it)->initialize(buffer);
			for(++it; it!= traces.end(); ++it)
			{
				if(!binary) buffer << ", ";
				(*it)->initialize(buffer); //files-bol kiszedjuk az ostream*-ot es azt dereferaljuk
			}		
			if(!binary) buffer << std::endl;
		}
	}
	
	void tracefile_t::trace()
	{		
		if(traces.size() > 0)
		{
			std::vector<trace_t*>::iterator it = traces.begin();
			(*it)->trace(buffer);
			for(++it; it != traces.end(); ++it)
			{
				if(!binary) buffer << ", ";
				(*it)->trace(buffer);
			}		
			if(!binary) buffer << std::endl;
		}

		if(buffer.tellp() > buffer_size_limit)
		{
			of << buffer.rdbuf();
			buffer.clear();
			buffer.str(std::string());
		}

	}
	
	void tracefile_t::reset()
	{
		for(auto &it: traces)
		{
			it->reset();
		}
	}
	
	void tracefile_t::finalize()
	{
		for(auto &it: traces)
		{			
			if(buffer.tellp() > 0)
			{
				of << buffer.rdbuf();
				buffer.clear();
				buffer.str(std::string());
			}

			it->finalize(of);
		}
	}
}

