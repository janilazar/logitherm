#include "util/log_t.hpp"

namespace util
{
	log_t::log_t(const std::string& path, const level_t lvl)
	:
		path(path),
		level(lvl)
	{}
	
	log_t::~log_t()
	{
		all_file.close();
		debug_file.close();
		warning_file.close();
		error_file.close();
	}
	
	void log_t::set_log_file_path(const std::string& new_path)
	{
		path = new_path;
		
		all_file.close();
		debug_file.close();
		warning_file.close();
		error_file.close();
	}
	
	void log_t::set_notification_level(level_t lvl)
	{
		level = lvl;
	}
	
	void log_t::debug(const char* debug_msg)
	{
		if(level == level_t::debug)
		{
			if(!all_file.is_open())
			{
				all_file.open(path+"/all.log");
				if(!all_file.is_open())
				{
					std::cerr << "All file '" << path << "/all.log' could not be opened" << std::endl;
					error("All file '" + path + "/all.log' could not be opened");
				}
				else /** meg nincs megnyitva, de most sikerult **/
				{
					all_file << debug_msg << std::endl;
				}
			}
			else /** meg van nyitva, irjunk bele **/
			{
				all_file << debug_msg << std::endl;
			}
			
			if(!debug_file.is_open())
			{
				debug_file.open(path+"/debug.log");
				if(!debug_file.is_open())
				{
					std::cerr << "Debug file '" << path << "/debug.log' could not be opened" << std::endl;
					error("Debug file '" + path + "/debug.log' could not be opened");
				}
				else /** meg nincs megnyitva, de most sikerult **/
				{
					debug_file << debug_msg << std::endl;
				}
			}
			else /** meg van nyitva, irjunk bele **/
			{
				debug_file << debug_msg << std::endl;
			}			
		}
	}
	
	void log_t::debug(const std::string& debug_msg)
	{
		debug(debug_msg.c_str());
	}
	
	void log_t::warning(const char* warning_msg)
	{
		if(level == level_t::warning || level == level_t::debug)
		{
			if(!all_file.is_open())
			{
				all_file.open(path+"/all.log");
				if(!all_file.is_open())
				{
					std::cerr << "All file '" << path << "/all.log' could not be opened" << std::endl;
					error("All file '" + path + "/all.log' could not be opened");
				}
				else /** meg nincs megnyitva, de most sikerult **/
				{
					all_file << warning_msg << std::endl;
				}
			}
			else /** meg van nyitva, irjunk bele **/
			{
				all_file << warning_msg << std::endl;
			}
			
			if(!warning_file.is_open())
			{
				warning_file.open(path+"/warning.log");
				if(!warning_file.is_open())
				{
					std::cerr << "Warning file '" << path << "/warning.log' could not be opened" << std::endl;
					error("Warning file '" + path + "/warning.log' could not be opened");
				}
				else /** meg nincs megnyitva, de most sikerult **/
				{
					warning_file << warning_msg << std::endl;
				}
			}
			else /** meg van nyitva, irjunk bele **/
			{
				warning_file << warning_msg << std::endl;
			}			
		}
	}
	
	void log_t::warning(const std::string& warning_msg)
	{
		warning(warning_msg.c_str());		
	}
	
	void log_t::error(const char* error_msg)
	{
		if(!all_file.is_open())
		{
			all_file.open(path+"/all.log");
			if(!all_file.is_open())
			{
				std::cerr << "All file '" << path << "/all.log' could not be opened while sending '" << error_msg << "'" << std::endl;
			}
			else /** meg nincs megnyitva, de most sikerult **/
			{
				all_file << error_msg << std::endl;
			}
		}
		else /** meg van nyitva, irjunk bele **/
		{
			all_file << error_msg << std::endl;
		}
		
		if(!error_file.is_open())
		{
			error_file.open(path+"/error.log");
			if(!error_file.is_open())
			{
				std::cerr << "Error file '" << path << "/error.log' could not be opened while sending '" << error_msg << "'" << std::endl;
			}
			else /** meg nincs megnyitva, de most sikerult **/
			{
				error_file << error_msg << std::endl;
			}
		}
		else /** meg van nyitva, irjunk bele **/
		{
			error_file << error_msg << std::endl;
		}
		throw(error_msg);
	}
	
	void log_t::error(const std::string& error_msg)
	{
		error(error_msg.c_str());
	}
	
}
