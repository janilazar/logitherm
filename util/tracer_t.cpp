#include "util/tracer_t.hpp"
#include "logic/base/component_t.hpp"
#include "util/log_t.hpp"

	
namespace util
{
	tracer_t::tracer_t(const std::string& prefix, const std::string& base, util::log_t* log)
	:
		log(log),
		initialized(false),
		id(prefix + "_" + base + "_" + "tracer")
	{}
	
	tracer_t::~tracer_t()
	{
		for(auto &it: tracefiles)
		{			
			it.second->finalize();
			delete it.second;
		}
	}
	
	void tracer_t::add_trace(trace_t* trace, std::ios::openmode mode)
	{
		if(true == initialized)
		{
			log->warning("util::tracer_t::add_trace(): traces must not be added after initialization");
		}
		else
		{
			if(tracefiles.find(trace->file) != tracefiles.end())
			{
				tracefiles.find(trace->file)->second->traces.push_back(trace);

				std::cout << trace->file << std::endl;
				
				log->debug("util::tracer_t::add_trace(): add new trace '" + trace->id + "' to file '" + trace->file + "'");
			}
			else
			{
				tracefile_t* tracefile = new tracefile_t(trace->file, mode);
				if(!tracefile->of.is_open())
				{
					log->warning("util::tracer_t::add_trace(): file '" + trace->file + "' could not be opened");
				}
				else
				{
					log->debug("util::tracer_t::add_trace(): add new trace '" + trace->id + "' to new file '" + trace->file + "'");
					tracefile->traces.push_back(trace);
					tracefiles.insert(std::make_pair(trace->file, tracefile)); //file-t is elmentjuk
				}
			}
		}
	}
	
	/**
	 * ezt a fv-t kell meghivni szimulacios ciklusonkent a trace-eleshez
	**/ 
	void tracer_t::initialize()
	{
		for(auto &it: tracefiles)
		{			
			it.second->initialize();
		}
		initialized = true;
	}
	
	
	/**
	 * ezt a fv-t kell meghivni szimulacios ciklusonkent a trace-eleshez
	**/ 
	void tracer_t::trace()
	{
		for(auto &it: tracefiles)
		{
			it.second->trace();
		}
	}
	
	void tracer_t::reset()
	{
		for(auto &it: tracefiles)
		{
			it.second->reset();
		}
	}
}
