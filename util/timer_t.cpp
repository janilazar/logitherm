#include "util/timer_t.hpp"
#include <cmath>

namespace util
{
	timer_t::timer_t()
	:
		start_wall_time(0_ms),
		start_cpu_time(0_ms)
	{}

    void timer_t::start_timer()
    {
    	start_wall_time = get_wall_time();
    	start_cpu_time = get_cpu_time();
    }

    std::pair<unit::time_t, unit::time_t> timer_t::get_elapsed_time()
    {
    	return std::make_pair(get_wall_time() - start_wall_time, get_cpu_time() - start_cpu_time);
    }

//  Windows
#ifdef _WIN32
#include <Windows.h>

	unit::time_t timer_t::get_wall_time()
	{
	    LARGE_INTEGER time,freq;
	    if (!QueryPerformanceFrequency(&freq))
	    {
	        //  Handle error
	        return 0_ms;
	    }
	    if (!QueryPerformanceCounter(&time))
	    {
	        //  Handle error
	        return 0_ms;
	    }
	    return static_cast<unit::time_t>(static_cast<long double>(time.QuadPart) / freq.QuadPart);
	}

	unit::time_t timer_t::get_cpu_time()
	{
	    FILETIME a,b,c,d;
	    if (GetProcessTimes(GetCurrentProcess(),&a,&b,&c,&d) != 0)
	    {
	        //  Returns total user time.
	        //  Can be tweaked to include kernel times as well.
	        return
	            static_cast<unit::time_t>(static_cast<long double>(d.dwLowDateTime | (static_cast<unsigned long long>(d.dwHighDateTime) << 32)) * 0.0000001l);
	    }
	    else
	    {
	        //  Handle error
	        return 0_ms;
	    }
	}

//  Posix/Linux
#else
#include <sys/time.h>

	unit::time_t timer_t::get_wall_time()
	{
	    struct timeval time;
	    if (gettimeofday(&time,NULL))
	    {
	        //  Handle error
	        return 0_ms;
	    }
	    return static_cast<unit::time_t>(static_cast<long double>(time.tv_sec)) + static_cast<unit::time_t>(static_cast<long double>(time.tv_usec) * 0.000001l);
	}

	unit::time_t timer_t::get_cpu_time()
	{
	    return static_cast<unit::time_t>(static_cast<long double>(clock()) / CLOCKS_PER_SEC);
	}
#endif
}