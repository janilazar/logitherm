#ifndef _TIMESTEP_T_
#define _TIMESTEP_T_

#include <cmath>

#include "unit/time_t.hpp"
#include "util/enums.hpp"

namespace util
{
	class timestep_t
	{
		private:
			/** bazis/alap beallitott ertek **/
			unit::time_t base;

			/** linearis v logaritmikus timestep **/
			util::timescale_t timescale;

			/** logaritmikus esetben pontok dekadonkent **/
			unsigned points_per_decade;
			
			/** hanyadik pontnal tartunk ebben a dekadban **/
			unsigned in_this_decade;

			double scale_factor;

			/** ebben taroljuk el a szimulacio idejet ps-ban **/
			unsigned long long current_time_in_ps;

		public:
			timestep_t() = delete;

			timestep_t(unit::time_t base)
			:
				base(base),
				timescale(util::timescale_t::lin),
				points_per_decade(1),
				in_this_decade(0),
				scale_factor(1.0),
				current_time_in_ps(0ull)
			{}

			timestep_t(unit::time_t base, util::timescale_t timescale, unsigned ppd)
			:
				base(base),
				timescale(timescale),
				points_per_decade(ppd),
				in_this_decade(0),
				scale_factor(std::pow(10.0,1.0/static_cast<double>(points_per_decade))),
				current_time_in_ps(0ull)
			{}

			void set(unit::time_t base, util::timescale_t timescale, unsigned ppd)
			{
				this->base = base;
				this->timescale = timescale;
				this->points_per_decade = ppd;
				in_this_decade = 0;
				scale_factor = std::pow(10.0,1.0/static_cast<double>(points_per_decade));
			}

			bool is_linear() const
			{
				return timescale == util::timescale_t::lin;
			}

			unit::time_t current_timestep() const
			{
				if(timescale == util::timescale_t::lin)
				{
					return base;
				}
				else
				{
					return base * std::pow(scale_factor, static_cast<double>(in_this_decade));
				}
			}

			unit::time_t next_timestep() const
			{
				if(timescale == util::timescale_t::lin)
				{
					return base;
				}
				else
				{
					if(in_this_decade == points_per_decade - 1) return base * 10.0;
					else return base * std::pow(scale_factor, static_cast<double>(in_this_decade + 1));
				}
			}

			void advance_timestep()
			{
				if(timescale == util::timescale_t::log)
				{
					in_this_decade++;
					if(in_this_decade == points_per_decade)
					{
						in_this_decade = 0;
						base = base * 10.0;
					}
				}
			}

			unit::time_t current_time() const
			{
				return static_cast<unit::time_t>(static_cast<long double>(current_time_in_ps)*1e-9);
			}


			/** manager-ben a fo ciklusban hivjuk **/
			void advance_time()
			{
				current_time_in_ps = current_time_in_ps + static_cast<unsigned long long>(static_cast<double>(current_timestep()*1e9));
			}
	};
}

#endif //_TIMESTEP_T_