INCDIR = $(shell pwd)

###############################################################################################
# BEALLITANDO VALTOZOK; HA NINCS SZUKSEG AZ ADOTT MODULRA, AKKOR KI KELL KOMMENTELNI
#
# telepites path
INSTDIR = /usr/local
#
# Sniper szimulator root directory
# SNIPER_ROOT = $(INCDIR)/../sniper
#
# OpenCL library include es library directory path-ok
# ha van, akkor allitsuk be, es akkor a paralution-ben automatikusan engedelyezve lesz az OpenCL backend
# OPENCL_INCDIR = /opt/AMDAPPSDK-3.0/include/
# OPENCL_LIBDIR = /opt/AMDAPPSDK-3.0/lib/x86_64/
#
# MKL library, ha beallitjuk, akkor 3D-ICE-hoz ezt hasznaljuk, egyebkent OpenBLAS-t hasznalunk
# MKLROOT = /opt/intel/mkl
#
#
# OpenBLAS target cpu, ha be van allitva, akkor ez lesz a target. Ha nincs beallitva, akkor OpenBLAS autodetect
# virtualis gepben van ennek a valtozonak jelentosege
# OPENBLAS_CPU = CORE2
#
# Verilog VPI library include es library path, ha be van allitva, akkor a verilog engine is belefordul az .so-ba, egyebkent nem szabad neki
# VPI_INCDIR = /home/lazar/egyetem/phd/tema/mentor_ams/questasim/v10.4c_5/include
# VPI_LIBDIR = /home/lazar/egyetem/phd/tema/mentor_ams/questasim/v10.4c_5/linux_x86_64
# VPI_INCDIR = /home/lazar/mentor_ams/questasim/v10.4c_5/include
# VPI_LIBDIR = /home/lazar/mentor_ams/questasim/v10.4c_5/linux_x86_64
# VPI_LIBNAME= mtipli
# VPI_INCDIR = /mnt/storage/egyetem/phd/cadence_eda/cadence/2016-17/RHELx86/INCISIVE_15.20.010/tools/inca/include
# VPI_LIBDIR = /mnt/storage/egyetem/phd/cadence_eda/cadence/2016-17/RHELx86/INCISIVE_15.20.010/tools/inca/lib/64bit
# VPI_LIBNAME = visadev
# VHPI_INCDIR = /mnt/storage/egyetem/phd/cadence_eda/cadence/2016-17/RHELx86/INCISIVE_15.20.010/tools/inca/include
# VHPI_LIBDIR = /mnt/storage/egyetem/phd/cadence_eda/cadence/2016-17/RHELx86/INCISIVE_15.20.010/tools/inca/lib/64bit
# VHPI_LIBNAME = visadev
###############################################################################################


LOGIC = liblogic.a
MANAGER = libmanager.a
THERMAL = libthermal.a
TR3STER = libtr3ster.a
UTIL = libutil.a
UNIT = libunit.a
FILE_IO = libfileio.a
DEPENDENCY = libdependency.a

ARCH= $(shell getconf LONG_BIT)
LT_ROOT = $(shell pwd)
# SystemC include directory
SYSC_INCDIR = $(INCDIR)/logic/systemc/digital/src

# 3D-ICE es a hozza tartozo SuperLU include directory
3DICE_INCDIR = $(INCDIR)/thermal/3dice/engine/include
SLU_INCDIR = $(INCDIR)/dependency/superlu/SRC

# SloTh-hoz felhasznalt paralution library directory
PARALUTION_INCDIR = $(INCDIR)/dependency/paralution/

# SUNRED-hez felhasznalt NTL es GMP library directory
NTL_INCDIR = $(INCDIR)/dependency/ntl/include/
GMP_INCDIR = $(INCDIR)/dependency/gmp/include/

# Eigen include dir (SloTh forward Euler-hez)
EIGEN_INCDIR = $(INCDIR)/dependency/eigen/
SPECTRA_INCDIR = $(INCDIR)/dependency/spectra/include/Spectra

# Sniperrel letoltodo python library directory
# !32 bites rendszeren valtozhat az eleresi ut!
ifdef SNIPER_ROOT
ifeq ($(ARCH), 64)
	PYTHON_INCDIR = $(SNIPER_ROOT)/simulator/python_kit/intel64/include/python2.7
	PYTHON_LIBDIR = $(SNIPER_ROOT)/simulator/python_kit/intel64/lib/
else
	PYTHON_INCDIR = $(SNIPER_ROOT)/simulator/python_kit/intel32/include/python2.7
	PYTHON_LIBDIR = $(SNIPER_ROOT)/simulator/python_kit/intel32/lib/
endif
	LINK_PYTHON = -L$(PYTHON_LIBDIR) -lpython2.7 -Wl,-rpath=$(PYTHON_LIBDIR)
	D_SNIPER = "-D SNIPER"
else
	PYTHON_INCDIR = 
	PYTHON_LIBDIR = 
	LINK_PYTHON =
	D_SNIPER=
endif

# Intel MKL
ifdef MKLROOT
	MKL=$(MKLROOT)/lib/intel64/libmkl_intel_lp64.a $(MKLROOT)/lib/intel64/libmkl_gnu_thread.a $(MKLROOT)/lib/intel64/libmkl_core.a
else
	MKL=
endif

# 3D-ICE OpenBLAS cpu target
ifdef OPENBLAS_CPU
	OPENBLAS_TARGET="TARGET=$(OPENBLAS_CPU)"
else
	OPENBLAS_TARGET =
endif

# OpenCL linking SloTh-hoz
ifdef OPENCL_LIBDIR
	LINK_OPENCL = -L$(OPENCL_LIBDIR) -lOpenCL -Wl,-rpath=$(OPENCL_LIBDIR)
else
	LINK_OPENCL =
endif

# VPI linkeles Veriloghoz
ifdef VPI_LIBDIR
	LINK_VPI = -L$(VPI_LIBDIR) -l$(VPI_LIBNAME) -Wl,-rpath=$(VPI_LIBDIR)
	INCL_VPI = -I $(VPI_INCDIR)
	D_VERILOG= -D VERILOG
else
	LINK_VPI =
	INCL_VPI =
	D_VERILOG=
endif

# VHPI linkeles VHDL-hez
ifdef VHPI_LIBDIR
	LINK_VHPI = -L$(VHPI_LIBDIR) -l$(VHPI_LIBNAME) -Wl,-rpath=$(VHPI_LIBDIR)
	INCL_VHPI = -I $(VHPI_INCDIR)
	D_VHDL= -D VHDL
else
	LINK_VHPI =
	INCL_VHPI =
	D_VHDL=
endif

H_INSTDIR = $(INSTDIR)/include/LogiTherm
L_INSTDIR = $(INSTDIR)/lib

# shared library neve
SHAREDLIB = liblogitherm.so

# static library neve
STATICLIB = liblogitherm.a

# kurva cadence incisive-nek 32 bites .so kell?? -march=native
# CXXFLAGS += -g -O3 -march=native
# CFLAGS += -g -O3 -march=native

# a feljebb beallitott pathok exportalasa, ezaltal a submake-ekben lathatoak lesznek
export	ARCH
export	INCDIR
export	LT_ROOT
export	SYSC_INCDIR
export	3DICE_INCDIR
export	SLU_INCDIR
export	SNIPER_ROOT
export	D_SNIPER
export	PYTHON_INCDIR
export	PYTHON_LIBDIR
export	PARALUTION_INCDIR
export	NTL_INCDIR
export	GMP_INCDIR
export	EIGEN_INCDIR
export	SPECTRA_INCDIR
export	MKL_ROOT
export	MKL
export	OPENBLAS_TARGET
export  OPENCL_INCDIR
export  OPENCL_LIBDIR
export  VPI_INCDIR
export  VPI_LIBNAME
export  VPI_LIBDIR
export  D_VERILOG
export  VHPI_INCDIR
export  VHPI_LIBNAME
export  VHPI_LIBDIR
export  D_VHDL
export  INCL_VPI
export	H_INSTDIR
export	L_INSTDIR

export CXXFLAGS
export CFLAGS

COMPILER_FLAGS = -std=c++11 -shared -fPIC -pthread -fopenmp -lgfortran -lm -ldl
ARF = $(ARFLAGS)scT

.PHONY: all dependency/$(DEPENDENCY) config logic/$(LOGIC) manager/$(MANAGER) thermal/$(THERMAL) tr3ster/$(TR3STER) unit/$(UNIT) util/$(UTIL) file_io/$(FILE_IO) logitherm  logitherm.pc install clean distclean doc

all: dependency config logic/$(LOGIC) manager/$(MANAGER) thermal/$(THERMAL) tr3ster/$(TR3STER) unit/$(UNIT) util/$(UTIL) file_io/$(FILE_IO) $(STATICLIB) $(SHAREDLIB)

dependency/$(DEPENDENCY):
	$(MAKE) -C dependency
	
config: dependency/$(DEPENDENCY)
	$(MAKE) config -C logic
	$(MAKE) config -C manager
	$(MAKE) config -C thermal
	$(MAKE) config -C tr3ster
	$(MAKE) config -C file_io
	
logic/$(LOGIC): 
	$(MAKE) $(LOGIC) -C logic
	
manager/$(MANAGER): 
	$(MAKE) $(MANAGER) -C manager

thermal/$(THERMAL): 
	$(MAKE) $(THERMAL) -C thermal

tr3ster/$(TR3STER):
	$(MAKE) $(TR3STER) -C tr3ster

unit/$(UNIT):
	$(MAKE) $(UNIT) -C unit

util/$(UTIL):
	$(MAKE) $(UTIL) -C util

file_io/$(FILE_IO):
	$(MAKE) $(FILE_IO) -C file_io

logitherm: $(SHAREDLIB)

$(STATICLIB): dependency/$(DEPENDENCY) logic/$(LOGIC) manager/$(MANAGER) thermal/$(THERMAL) tr3ster/$(TR3STER) unit/$(UNIT) util/$(UTIL) file_io/$(FILE_IO)
	rm -f $(STATICLIB)
	$(AR) $(ARF)  $(STATICLIB) logic/$(LOGIC) manager/$(MANAGER) thermal/$(THERMAL) tr3ster/$(TR3STER) unit/$(UNIT) util/$(UTIL) file_io/$(FILE_IO)

$(SHAREDLIB): $(STATICLIB)
	$(CXX) -Wl,--whole-archive $(STATICLIB) -Wl,--no-whole-archive -Wl,--start-group dependency/$(DEPENDENCY) -Wl,--end-group $(COMPILER_FLAGS) $(LINK_PYTHON) $(LINK_VPI) $(LINK_VHPI) $(LINK_OPENCL) -o $(SHAREDLIB)

logitherm.pc:
	rm -f logitherm.pc
	touch logitherm.pc
	printf "prefix=$(INSTDIR)\n" >> logitherm.pc
	printf "exec_prefix=\$${prefix}\n" >> logitherm.pc
	printf "includedir=\$${prefix}/include/LogiTherm\n" >> logitherm.pc
	printf "libdir=\$${prefix}/lib\n" >> logitherm.pc
	printf "\n" >> logitherm.pc
	printf "Name: LogiTherm\n" >> logitherm.pc
	printf "Description: LogiTherm library\n" >> logitherm.pc
	printf "Version: 127.0.0.1\n" >> logitherm.pc
	printf "Cflags: -I\$${includedir} -I\$${includedir}/logic/systemc/digital/src/ -I\$${includedir}/dependency/superlu/SRC/" >> logitherm.pc
	printf " -I\$${includedir}/dependency/ntl/include -I\$${includedir}/dependency/gmp/include\n" >> logitherm.pc
	printf "Libs: -L\$${libdir} -llogitherm \n" >> logitherm.pc

install: logitherm.pc
	$(MAKE) install -C dependency
	$(MAKE) install -C logic
	$(MAKE) install -C manager
	$(MAKE) install -C thermal
	$(MAKE) install -C tr3ster
	$(MAKE) install -C util
	$(MAKE) install -C unit
	$(MAKE) install -C file_io
	mkdir -p $(H_INSTDIR)/
	mkdir -p $(L_INSTDIR)/
	cp $(INCDIR)/logitherm $(H_INSTDIR)/
	cp $(INCDIR)/$(SHAREDLIB) $(L_INSTDIR)/
	mkdir -p $(L_INSTDIR)/pkgconfig    
	cp logitherm.pc $(L_INSTDIR)/pkgconfig/
	@echo -e "\033[1mBefore compiling against the $(SHAREDLIB) library execute:\033[0m export PKG_CONFIG_PATH=$(L_INSTDIR)/pkgconfig/"
	@echo -e "\033[1mTo compile:\033[0m g++ some_source_file.cpp -std=c++11 \`pkg-config --libs --cflags logitherm\` -o some_binary"

clean:
	$(MAKE) clean -C dependency
	$(MAKE) clean -C logic
	$(MAKE) clean -C manager
	$(MAKE) clean -C thermal
	$(MAKE) clean -C tr3ster
	$(MAKE) clean -C util
	$(MAKE) clean -C unit
	$(MAKE) clean -C file_io
	$(MAKE) clean -C doc/src
	rm -f $(STATICLIB)
	rm -f $(SHAREDLIB)

distclean: clean
	$(MAKE) distclean -C dependency
	$(MAKE) distclean -C logic
	$(MAKE) distclean -C manager
	$(MAKE) distclean -C thermal
	$(MAKE) distclean -C tr3ster
	$(MAKE) distclean -C file_io

doc:
	$(MAKE) pdflatex -C doc/src

