#include "manager/manager_t.hpp"	

#include "thermal/sunred/adapter/adapter_t.hpp"
#include "thermal/sloth/adapter/adapter_t.hpp"
#include "thermal/3dice/adapter/adapter_t.hpp"
#include "thermal/hotspot/adapter/adapter_t.hpp"

#include "logic/systemc/adapter/adapter_t.hpp"
#include "logic/sniper/adapter/adapter_t.hpp"
#include "logic/verilog/adapter/adapter_t.hpp"
#include "logic/vhdl/adapter/adapter_t.hpp"

#include "file_io/3dice/adapter_t.hpp"

namespace logitherm
{
	manager_t* manager_t::manager = nullptr;
	
	manager_t::manager_t()
	:
		logic_engine(nullptr),
		thermal_engine(nullptr),
		initialized(false),
		timestep(1_ms),
		log("/home/lazar", util::level_t::warning),
		id("logitherm::manager_t")
	{
		debug("manager_t()");
	}
	
	manager_t* manager_t::get_manager()
	{
		if(nullptr == manager)
		{
			manager = new manager_t;
		}
		return manager;
	}
	
	manager_t::~manager_t()
	{
		debug("~manager_t()");
		delete logic_engine;
		delete thermal_engine;
		manager = nullptr;
	}
	
	thermal::adapter_t* manager_t::get_thermal_engine()
	{
		if(thermal_engine == nullptr) warning("get_thermal_engine(): thermal engine is not initalized.");
		return thermal_engine;
	}
	
	
	thermal::adapter_t* manager_t::set_thermal_engine(const util::thermal_engine_t engine)
	{
		switch(engine)
		{
			case util::thermal_engine_t::sloth:
				thermal_engine = new thermal::sloth::adapter_t(this, &log, "thermal::sloth::adapter_t", timestep);
				break;

			case util::thermal_engine_t::sunred:
				thermal_engine = new thermal::sunred::adapter_t(this, &log, "thermal::sunred::adapter_t", timestep);
				break;

			case util::thermal_engine_t::threedice:
				thermal_engine = new thermal::threed_ice::adapter_t(this, &log);
				break;

			case util::thermal_engine_t::hotspot:
				thermal_engine = new thermal::hotspot::adapter_t(this, &log);
				break;

			default:
				error("set_thermal_engine(): unrecognized thermal engine type.");
			
		}
		return thermal_engine;
	}

	logic::adapter_t* manager_t::get_logic_engine()
	{
		if(logic_engine == nullptr) warning("get_logic_engine(): logic engine is not initalized.");
		return logic_engine;
	}
	
	logic::adapter_t* manager_t::set_logic_engine(const util::logic_engine_t engine)
	{
		switch(engine)
		{
			case util::logic_engine_t::systemc:
				logic_engine = new logic::systemc::adapter_t(this, &log);
				break;

			case util::logic_engine_t::sniper:
#ifdef SNIPER
				logic_engine = new logic::sniper::adapter_t(this, &log);
#else
				error("set_logic_engine(): sniper adapter was not compiled.");
#endif
				break;

			case util::logic_engine_t::verilog:
#ifdef VERILOG
				logic_engine = new logic::verilog::adapter_t(this, &log);
#else
				error("set_logic_engine(): verilog adapter was not compiled.");
#endif
				break;

				case util::logic_engine_t::vhdl:
#ifdef VHDL
				logic_engine = new logic::vhdl::adapter_t(this, &log);
#else
				error("set_logic_engine(): vhdl adapter was not compiled.");
#endif
				break;

			default:
				error("set_logic_engine(): unrecognized logic engine type.");
			
		}
		return logic_engine;
	}
	
	void manager_t::set_timestep(unit::time_t step, util::timescale_t timescale, unsigned ppd)
	{
		timestep.set(step, timescale, ppd);
		
		if(nullptr == logic_engine) warning("set_timstep(): logic engine is not initialized");
		else logic_engine->timestep_changed();
		
		if(nullptr == thermal_engine) warning("set_timestep(): thermal engine is not initialized");
		else thermal_engine->timestep_changed();
	}
	
	const util::timestep_t& manager_t::get_timestep()
	{
		return timestep;
	}
	
	void manager_t::set_log_file_path(const std::string& path)
	{
		log.set_log_file_path(path);
	}
	
	void manager_t::set_notification_level(util::level_t level)
	{
		log.set_notification_level(level);
	}
	
	/**
	 * egy logikai komponenst ad hozza a dissipator maphez
	 * hibaellenorzes: component egyszer mar szerepel a dissipatorban (nev az azonosito)
	 * hibaellenorzes: a component nem szerepel a components_tree-ben
	**/ //ellenorizni kene, hogy a layout component tenylegesen belefer-e a layout-ba??? mondjuk nem itt kellene ennek megjelennie...
	void manager_t::add_dissipator_component(const std::string& name)
	{
		if(logic_engine == nullptr)
		{
			warning("add_dissipator_component(): logic engine was not initialized");
			return;
		}
		
		if(thermal_engine == nullptr)
		{
			error("add_dissipator_component(): thermal engine was not initialized");
			return;
		}

		logic::component_t* logic_comp = logic_engine->get_component(name);
		if(nullptr == logic_comp)
		{
			warning("add_dissipator_component(): logic component '" + name + "' is not found");
			return;
		}
		
		if(thermal_engine->get_layout() == nullptr)
		{
			warning("add_dissipator_component(): layout structure was not initialized");
			return;
		}
		
		layout::component_t* layout_comp = thermal_engine->get_layout()->get_component(name);
		
		if(nullptr == layout_comp)
		{
			warning("add_dissipator_component(): layout component '" + name + "' is not found");
			return;
		}
		
		if(dissipator_components.find(logic_comp) != dissipator_components.end())
		{
			warning("add_dissipator_component(): component '" + name + "' is already in the dissipator components map");
			return;
		}
		
		dissipator_components.insert(std::make_pair(logic_comp, layout_comp));
		
	}
	
	/**
	 * egy logikai komponenst ad hozza a display maphez
	 * hibaellenorzes: component egyszer mar szerepel a displayben (nev az azonosito)
	 * hibaellenorzes: a component nem szerepel a components_tree-ben
	**/ //ellenorizni kene, hogy a layout component tenylegesen belefer-e a layout-ba??? mondjuk nem itt kellene ennek megjelennie...
	void manager_t::add_display_component(const std::string& name)
	{
		bool failed = false;
		
		if(thermal_engine == nullptr)
		{
			warning("add_display_component(): thermal engine was not initialized");
			return;
		}

		if(thermal_engine->get_layout() == nullptr)
		{
			warning("add_display_component(): layout structure was not initialized");
			return;
		}
		
		layout::component_t* layout_comp = thermal_engine->get_layout()->get_component(name);
		if(!failed && nullptr == layout_comp)
		{
			warning("add_display_component(): layout component '" + name + "' is not found");
			return;
		}
		
		if(display_components.find(layout_comp) != display_components.end())
		{
			warning("add_display_component(): layout component '" + name + "' is already in the display components set");
			return;
		}

		display_components.insert(layout_comp);

	}
	
	/**
	 * ha a megfelelo adatszerkezetben vannak elemek, akkor ezek a fv-ek meghivjak a trace objektum megfelelo fv-et
	**/ 
	void manager_t::trace()
	{
		if(false == initialized)
		{
			for(auto &it: tracers) it->initialize();
			initialized = true;
		}
		
		for(auto &it: tracers) it->trace();
	}
		
	/**
	 * ez a fv alap helyzetbe hozza a modulokat, hogy uj szimulacios ciklus kezdodhessen
	**/ 
	void manager_t::start_new_simulation_cycle()
	{
		/** dissipatorok resetelese **/
		for(auto &it: dissipator_components) it.first->reset();
		
		/** trace beallitasa az uj ciklushoz **/
		for(auto &it: tracers) it->reset();


		//itt kellene termikust/minden mast is nullazni....

		// ez itt minek is van??
		// timestep.next_timestep();
		
	}
	
	std::ostream& manager_t::print_dissipator_components(std::ostream& os)
	{
		for(auto &it: dissipator_components)
		{
			it.first->print_component(os);
			it.second->print_component(os);
		}
		return os;
	}
	std::ostream& manager_t::print_display_components(std::ostream& os)
	{
		for(auto &it: display_components)
		{
			it->print_component(os);
		} 
		return os;
	}
	
	std::ostream& manager_t::print_temperature_map_in_svg(std::ostream& os)
	{
		if(thermal_engine != nullptr)
		{
			return thermal_engine->print_temperature_map_in_svg(os, display_components);
		}
		else
		{
			warning("print_temperature_map_in_svg(): thermal engine is not initialized");
			return os;
		}
	}
	
	
	void manager_t::run_simulation_cycle()
	{
		timestep.advance_time();
		
		if(nullptr != thermal_engine) thermal_engine->refresh_temperatures(dissipator_components);
		else error("run_simulation_cycle(): thermal engine is not initialized");

		trace(); // -> ezt kell innen kirakni mindenhova mashova
		start_new_simulation_cycle();
		timestep.advance_timestep();
		// ide kene tenni, hogy ha tenylegesen valtozott a timestep, akkor a logic es thermal engine is tudjon rola
	}

	void manager_t::write_floorplan_file(util::thermal_engine_t engine, const std::string& filename)
	{
		switch(engine)
		{
			case util::thermal_engine_t::sloth:
				debug("write_floorplan_file(): This functionality is not yet implemented.");
				break;
			case util::thermal_engine_t::sunred:
				debug("write_floorplan_file(): This functionality is not yet implemented.");
				break;
			case util::thermal_engine_t::threedice:
			{
				file_io::threed_ice::adapter_t file_writer(&log);
				file_writer.write_floorplan_file(filename, dissipator_components);
			}
				break;
			default:
				error("write_floorplan_file():  unrecognized thermal engine type.");
			
		}
	}

	void manager_t::debug(const char* msg)
	{
		log.debug("'" + id + "' " + msg);
	}
	
	void manager_t::debug(const std::string& msg)
	{
		log.debug("'" + id + "' " + msg);
	}
	
	void manager_t::warning(const char* msg)
	{
		log.warning("'" + id + "' " + msg);
	}
	
	void manager_t::warning(const std::string& msg)
	{
		log.warning("'" + id + "' " + msg);
	}
	
	void manager_t::error(const char* msg)
	{
		log.error("'" + id + "' " + msg);
	}
	
	void manager_t::error(const std::string& msg)
	{
		log.error("'" + id + "' " + msg);
	}
	
	void manager_t::start_timer()
    {
    	timer.start_timer();
    }
    
    std::pair<unit::time_t,unit::time_t> manager_t::get_elapsed_time()
    {
    	return timer.get_elapsed_time();
    }
} //namespace logitherm
