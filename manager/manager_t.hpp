#ifndef _LOGITHERM_MANAGER_
#define _LOGITHERM_MANAGER_


#include <unordered_map>
#include <unordered_set>
#include <iostream>
#include <fstream>
#include <map>


#include "util/log_t.hpp"
#include "util/tracer_t.hpp"
#include "util/enums.hpp"
#include "util/timestep_t.hpp"
#include "util/timer_t.hpp"
#include "unit/all_units.hpp"

#include "logic/base/adapter_t.hpp"
#include "thermal/base/adapter_t.hpp"

namespace logitherm
{
	
	/**
	 * singleton osztaly
	 * ez az osztaly felelol a logikai motor es a termikus motor osszekapcsolasaert
	 * a peldanyra mutato referenciat a get_logitherm() fuggven adja
	**/ 
	
	class manager_t
	{
		private:
			/**
			 * atterek a heapen foglalt signleton osztalyra
			 * ezzel a destruktort akkor hivom, amikor nekem tetszik
			**/ 
			static manager_t* manager;
		
			/**
			 * termikus motor adaptere, ami a szamitasokat vegzi
			**/ 
			thermal::adapter_t* thermal_engine;
			
			/** 
			 * ez az osztaly tarolja a logikai komponensek hierarchiajat,
			 * a disszipator elemeket(== amik a layout-ra kerulnek),
			 * megjelenitendo elemek listajat
			 * extra map, amiben vectorokat tarolunk a komponensek nyomonkovetesere
			**/ 
			logic::adapter_t* logic_engine;			
			
			/**
			 * ebben a vectorban vannak azok a components-ek, amelyeknek szamitjuk a disszipaciojat a logitermikus szimulacional
			**/ 
			std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal> dissipator_components;
						
			
			/**
			 * ebben a vectorban azok a components-ek vannak, amelyeket feltuntetunk az eredmenyul kapott hoterkepen
			**/ 
			std::unordered_set<layout::component_t*, std::hash<layout::component_t*>, layout::components_equal> display_components;
			
			/**
			 * 
			**/
			std::unordered_set<util::tracer_t*, std::hash<util::tracer_t*>, util::tracer_equal> tracers; 
			bool initialized;
			
			util::timestep_t timestep;
			
			/** a log-olast vegzo tipus **/
			util::log_t log;

			util::timer_t timer;

		public:
			const std::string id;
			
		private:			
			/**
			 * logitherm_proxy singleton osztaly
			 * a konstruktort csak tagfuggveny hivhatja meg (get_logitherm())
			**/
			manager_t();
			
			/**
			 * logitherm_proxy singleton osztaly
			 * masolas le van tiltva
			**/ 
			manager_t(manager_t const&)	= delete;
			void operator=(manager_t const&)	= delete;

			/**
			 * ha a megfelelo adatszerekzetben vannak elemek, akkor ezek a fv-ek meghivjak a trace objektum megfelelo fv-et
			**/ 
			void trace();

			/**
			 * ez a fv alap helyzetbe hozza a modulokat, hogy uj szimulacios ciklus kezdodhessen
			**/ 
			void start_new_simulation_cycle();
			
		public:
		
			/**
			 * logitherm_proxy egy singleton osztaly, az a fv visszaadja az egyetlen peldanyt
			**/ 
			static manager_t* get_manager();
			
			~manager_t();
			
			thermal::adapter_t* get_thermal_engine();
			
			thermal::adapter_t* set_thermal_engine(const util::thermal_engine_t engine);
			
			
			logic::adapter_t* get_logic_engine();
			
			logic::adapter_t* set_logic_engine(const util::logic_engine_t engine);
			
			void set_timestep(unit::time_t step, util::timescale_t timescale = util::timescale_t::lin, unsigned ppd = 1);
			const util::timestep_t& get_timestep();
			
			void set_log_file_path(const std::string& path);
			void set_notification_level(util::level_t level);
			
			/**
			 * egy logikai komponenst ad hozza a dissipator maphez
			 * hibaellenorzes: component egyszer mar szerepel a dissipatorban (nev az azonosito)
			 * hibaellenorzes: a component nem szerepel a components_tree-ben
			**/ 
			void add_dissipator_component(const std::string& name);
			
			/**
			 * egy logikai komponenst ad hozza a display maphez
			 * hibaellenorzes: component egyszer mar szerepel a displayben (nev az azonosito)
			 * hibaellenorzes: a component nem szerepel a components_tree-ben
			**/ 
			void add_display_component(const std::string& name);

			
			/**
			 * egy logikai komponenst ad hozza a megfelelo trace-hez
			 * ha kell, akkor megnyitja a trace-hez tartozo filet
			 * hibaellenorzes: component egyszer mar szerepel a dissipation trace-ben
			 * hibaellenorzes: a component nem szerepel a components_tree-ben
			**/ 
			void trace_component(util::tracer_t* tracer)
			{
				tracers.insert(tracer);
			}			
			
			std::ostream& print_dissipator_components(std::ostream& os);
			
			std::ostream& print_display_components(std::ostream& os);
			
			std::ostream& print_temperature_map_in_svg(std::ostream& os);
			
			/**
			 * ezt a fv-t hivja meg a a logic_engine bizonyos idokozonkent
			**/
			void run_simulation_cycle();

			void write_floorplan_file(util::thermal_engine_t engine_type, const std::string& filename);
			
			// void calculate_time_constant_spectra(const std::string& path, const std::string& id);
			
			void message(util::level_t lvl, const char* msg);
			void message(util::level_t lvl, const std::string& msg);
			
			void warning(const char* msg);
			void warning(const std::string& msg);
			
			void debug(const char* msg);
			void debug(const std::string& msg);
			
			void error(const char* msg);
			void error(const std::string& msg);

			void start_timer();
		    std::pair<unit::time_t, unit::time_t> get_elapsed_time();
	};
}

#endif //_LOGITHERM_MANAGER_
