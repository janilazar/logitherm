#ifndef _PARSER_BASE_HPP_
#define _PARSER_BASE_HPP_

#include <string>

namespace logitherm
{
	class manager_t;
}

namespace util
{
	class log_t;
}

namespace file_io
{
	class adapter_t
	{
		public:
			const std::string id;
			
		private:
			//::logitherm::manager_t* const manager;
			::util::log_t* const log;
			
		public:
			adapter_t() = delete;
			adapter_t(::util::log_t* log, const std::string& id);
			
		protected:
			void debug(const char* msg) const;
			void debug(const std::string& msg) const;
			void warning(const char* msg) const;
			void warning(const std::string& msg) const;
			void error(const char*) const;
			void error(const std::string& msg) const;
	};
}

#endif
