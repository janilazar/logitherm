#include "file_io/base/adapter_t.hpp"
#include "util/log_t.hpp"

namespace file_io
{
	
	adapter_t::adapter_t(::util::log_t* log, const std::string& id)
	:
		id(id),
		log(log)
	{}
	
	void adapter_t::debug(const char* msg) const
	{
		log->debug("'" + id + "' " + msg);
	}
	
	void adapter_t::debug(const std::string& msg) const
	{
		log->debug("'" + id + "' " + msg);
	}
	
	void adapter_t::warning(const char* msg) const
	{
		log->warning("'" + id + "' " + msg);
	}
	
	void adapter_t::warning(const std::string& msg) const
	{
		log->warning("'" + id + "' " + msg);
	}
	
	void adapter_t::error(const char* msg) const
	{
		log->error("'" + id + "' " + msg);
	}
	
	void adapter_t::error(const std::string& msg) const
	{
		log->error("'" + id + "' " + msg);
	}
}
