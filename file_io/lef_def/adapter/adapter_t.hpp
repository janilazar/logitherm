#ifndef _PARSE_LEF_DEF_HPP_
#define _PARSE_LEF_DEF_HPP_

#include "file_io/base/adapter_t.hpp"
#include "file_io/lef_def/def/include/defrReader.hpp"
#include "file_io/lef_def/lef/include/lefrReader.hpp"
#include "thermal/base/layout/adapter_t.hpp"
#include "unit/all_units.hpp"

#include <string>
#include <unordered_map>
#include <unordered_set>

/* layout adapterek osztalyok forward decl-je */
namespace layout
{
	class adapter_t;

	namespace sunred
	{
		class adapter_t;
	}

	namespace sloth
	{
		class adapter_t;
	}

	namespace threed_ice
	{
		class adapter_t;
	}
}



namespace file_io
{
	namespace lef_def
	{	
		int macrocb(lefrCallbackType_e typ, lefiMacro* macro, lefiUserData ptr);
		int libendcb(lefrCallbackType_e typ, void*, lefiUserData ptr);
		int lefunitscb(lefrCallbackType_e, lefiUnits* units, lefiUserData ptr);
		int defunitscb(defrCallbackType_e, double units, defiUserData ptr);
		int dieareacb(defrCallbackType_e, defiBox *box, defiUserData ptr);
		int compcb(defrCallbackType_e typ, defiComponent* comp, defiUserData ptr);
		int designendcb(defrCallbackType_e typ, void*, defiUserData ptr);
		int dividercharcb(defrCallbackType_e typ, const char* divider, defiUserData ptr);

		class adapter_t	:	public	::file_io::adapter_t
		{
			private:
				friend int macrocb(lefrCallbackType_e typ, lefiMacro* macro, lefiUserData ptr);
				friend int libendcb(lefrCallbackType_e typ, void*, lefiUserData ptr);
				friend int lefunitscb(lefrCallbackType_e, lefiUnits* units, lefiUserData ptr);
				friend int defunitscb(defrCallbackType_e, double units, defiUserData ptr);
				friend int dieareacb(defrCallbackType_e, defiBox *box, defiUserData ptr);
				friend int compcb(defrCallbackType_e typ, defiComponent* comp, defiUserData ptr);
				friend int designendcb(defrCallbackType_e typ, void*, defiUserData ptr);
				friend int dividercharcb(defrCallbackType_e typ, const char* divider, defiUserData ptr);

			private:
				long double lefDatabaseUnit;
				long double defDatabaseUnit;

				std::string dividerchar; // ez mire is kell?
				
				/** ebbe a tablazatba mentem el az egyes cellatipusok nevet x es y iranyu meretet **/
				std::unordered_map<std::string, layout::xy_length_t> type_table;
				
				/** ebbe a tablazatba mentem el az egyes cellapeldanyok nevet, bal also sarok x, y koordinatajat, es a cella **/
				std::unordered_map<std::string, std::pair<layout::xy_length_t, layout::xy_length_t>> instance_table;
				
				/** layout x es y iranyu merete **/
				layout::xy_length_t layout_size;
				unit::area_t die_area;

			public:
				adapter_t() = delete;
				adapter_t(::util::log_t* log);

				void read_lef_files(const std::unordered_set<std::string>& lefFileNames);
				void read_def_file(const std::string& defFileName);

				//void create_components();
				//void create_components();

				//cellapeldanyok neve, bal felso sarok pozicioja, cella merete
				const std::unordered_map<std::string, std::pair<layout::xy_length_t, layout::xy_length_t>>& get_instance_table();
		};
	}
}

#endif // _PARSE_LEF_DEF_HPP_
