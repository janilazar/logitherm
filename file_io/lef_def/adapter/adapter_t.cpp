#include "file_io/lef_def/adapter/adapter_t.hpp"

#include "file_io/lef_def/def/include/defrReader.hpp"
#include "file_io/lef_def/lef/include/lefrReader.hpp"

#include "thermal/sunred/adapter/layout/adapter_t.hpp"
#include "thermal/sloth/adapter/layout/adapter_t.hpp"
#include "thermal/3dice/adapter/layout/adapter_t.hpp"

#include "util/string_manipulation.hpp"

#include <cstdio>
#include <cmath>
//#include <iostream>

namespace file_io
{
	namespace lef_def
	{
		int macrocb(lefrCallbackType_e typ, lefiMacro* macro, lefiUserData ptr)
		{
			auto* lef_def_adapter = static_cast<file_io::lef_def::adapter_t*>(ptr);
			if (typ != lefrMacroCbkType)
			{
				lef_def_adapter->error("Invalid callback type (in macrocb() function)");
				return 1;
			}
			//meg kell keresni az adott nevu komponenst
			if (macro->hasSize())
			{	
				if(lef_def_adapter->type_table.find(macro->name()) == lef_def_adapter->type_table.end())
				{
					 long double lefunit = lef_def_adapter->lefDatabaseUnit;
					layout::xy_length_t sides = {static_cast<long double>(macro->sizeX())*1e-6l,static_cast<long double>(macro->sizeY())*1e-6l};
					lef_def_adapter->type_table.insert(std::make_pair(macro->name(),sides));
				}
				//CellDatabases::cellBoundaries()[macro->name()].bounds = QRectF(0,0,macro->sizeX(),macro->sizeY());
				//logic_adapter.get_component(macro->name()) -> ha logic adapter-ben van ilyen component, akkor legyen a layout_adapter-ben is
			}
			return 0;
		}

		int libendcb(lefrCallbackType_e typ, void*, lefiUserData ptr)
		{
			auto* lef_def_adapter = static_cast<file_io::lef_def::adapter_t*>(ptr);
			if (typ == lefrLibraryEndCbkType)
			{
				lef_def_adapter->debug("Total number of library cell types so far: " + std::to_string(lef_def_adapter->type_table.size()));
			}
			return 0;
		}

		int lefunitscb(lefrCallbackType_e, lefiUnits* units, lefiUserData ptr)
		{
			auto* lef_def_adapter = static_cast<file_io::lef_def::adapter_t*>(ptr);
			lef_def_adapter->debug("LEF Database units: " + std::to_string(units->databaseNumber()));
			lef_def_adapter->lefDatabaseUnit = units->databaseNumber();
			return 0;
		}

		int defunitscb(defrCallbackType_e, double units, defiUserData ptr)
		{
			auto* lef_def_adapter = static_cast<file_io::lef_def::adapter_t*>(ptr);
			lef_def_adapter->debug("DEF Database units: " + std::to_string(units));
		    lef_def_adapter->defDatabaseUnit = units;
			return 0;
		}

		int dieareacb(defrCallbackType_e, defiBox *box, defiUserData ptr)
		{
			auto* lef_def_adapter = static_cast<file_io::lef_def::adapter_t*>(ptr);

		    defiPoints points = box->getPoint();
		    if (points.numPoints != 2)
		        return -1;

		    long double defunit = lef_def_adapter->defDatabaseUnit;
		    long double x[2] = {points.x[0]/defunit*1e-6l, points.x[1]/defunit*1e-6l}; //defunit csak dbunitpermicron, tehat egy prefix a micron ele, length_t pedig meter
		    long double y[2] = {points.y[0]/defunit*1e-6l, points.y[1]/defunit*1e-6l};

			lef_def_adapter->layout_size = layout::xy_length_t{std::fabs(x[1]-x[0]),std::fabs(-y[1]+y[0])};
		    return 0;
		}


		//ebben kell a fv-ben allitjuk be a layout component-ek tipusait
		int compcb(defrCallbackType_e typ, defiComponent* comp, defiUserData ptr)
		{
			auto* lef_def_adapter = static_cast<file_io::lef_def::adapter_t*>(ptr);
			if (typ != defrComponentCbkType)
			{
				lef_def_adapter->error("ERROR: Invalid callback type (in compcb() function)");
				return 1;
			}

			std::string type_cell = comp->name();
			/** ki kell szedni a DEF-be belegeneralt sok hanyas karaktert es Verilog-nak megfelelo divider-t kell belerakni **/
			std::string component_id = util::replace(util::remove(comp->id(), '\\'), lef_def_adapter->dividerchar, ".");

			//korabban megtalaltuk a library-ban a cellat-> szedjuk ki a meretet
			if (lef_def_adapter->type_table.find(type_cell) != lef_def_adapter->type_table.end())
			{


				
				long double defunit = lef_def_adapter->defDatabaseUnit;
				
				layout::xy_length_t bottom_left = {comp->placementX()/defunit*1e-6l, comp->placementY()/defunit*1e-6l};
				layout::xy_length_t side = lef_def_adapter->type_table.at(type_cell);
				layout::xy_length_t top_left = {bottom_left[0], lef_def_adapter->layout_size[1] - bottom_left[1] - side[1]};

				lef_def_adapter->instance_table.insert(std::make_pair(component_id, std::make_pair(top_left, side)));
			}
			else
			{
				lef_def_adapter->error("Cell type <" + type_cell + "> in DEF file cannot be found in any LEF file");
				return 1;
			}
			return 0;
		}

		int designendcb(defrCallbackType_e typ, void*, defiUserData ptr)
		{
			auto* lef_def_adapter = static_cast<file_io::lef_def::adapter_t*>(ptr);
			if (typ == defrDesignEndCbkType)
			{
		        lef_def_adapter->debug("Number of instances in design: " + std::to_string(lef_def_adapter->type_table.size()));
			}

			return 0;
		}

		int dividercharcb(defrCallbackType_e typ, const char* divider, defiUserData ptr)
		{
			auto* lef_def_adapter = static_cast<file_io::lef_def::adapter_t*>(ptr);
			if (typ == defrDividerCbkType)
			{
				lef_def_adapter->dividerchar = divider;
			}
			return 0;
		}

		/**
		 * beolvassa a megadott lef file-okat, parser hibaja eseten hibat dob
		 * beolvasas kozben a beallitott callback-ek lefutnak es kinyerik a szukseges adatokat
		**/
		void adapter_t::read_lef_files(const std::unordered_set<std::string>& lefFileNames)
		{

			/* erre kulon read lefdef fv kellene */
			int err;
			for(auto &lefFileName: lefFileNames)
			{
				FILE* leffile = fopen(lefFileName.c_str(),"rb");
				if (!leffile)
				{
					error("Couldn’t open input file ’" + lefFileName + "’");
				}

				// Initialize LEF reader
				lefrInit();
				
				int userData = 0;
				// Invoke the file_io
				err = lefrRead(leffile, lefFileName.c_str(), static_cast<void*>(this)); //itt adjuk at ezt az adaptert, ami a userdata lesz a callbacknel
				if (err != 0) {
					error("LEF file_io returned an error");
				}

				fclose(leffile);
			}
		}

		/**
		 * beolvassa a megadott def file-t, parser hibaja eseten hibat dob
		 * beolvasas kozben a beallitott callback-ek lefutnak, kinyerik a szukseges adatokat
		 * a layout_adapter tipusanak megfelelo create_component meghivodik es
		 * peldanyosodnak a component_t-k
		**/
		void adapter_t::read_def_file(const std::string& defFileName)
		{
			//   Initialize the reader. This routine has to be called first.
			defrInit();
			int err;
			FILE* deffile = fopen(defFileName.c_str(), "rb");
			if (!deffile)
			{
				error("Couldn’t open input file ’" + defFileName + "’");
			}

			err = defrRead(deffile, defFileName.c_str(), static_cast<void*>(this), 0); //itt adjuk at ezt az adaptert, ami a userdata lesz a callbacknel
			if (err != 0)
			{
				error("DEF file_io returned an error");
			}

			fclose(deffile);
		}

		//void adapter_t::create_components()
		//{
		//	for(auto &it: instance_table)
		//	{
		//		layout::xy_length_t side = type_table.at(it.first);
		//		xy_length_t bottom_left = it.second;
		//		layout::xy_length_t top_left = {bottom_left.first, layout_size[1] - bottom_left[1] - side[1]};
		//		instance_table.insert(it.first, std::make_pair(top_left, side));
		//	}
		//}

		const std::unordered_map<std::string, std::pair<layout::xy_length_t, layout::xy_length_t>>& adapter_t::get_instance_table()
		{
			return instance_table;
		}

		adapter_t::adapter_t(util::log_t* log)
		:
			file_io::adapter_t(log, "file_io::lef_def::adapter_t")
		{
			// Set callbacks
			defrSetUnitsCbk(defunitscb);
			defrSetComponentCbk(compcb);
			defrSetDesignEndCbk(designendcb);
		    defrSetDieAreaCbk(dieareacb);
			defrSetDividerCbk(dividercharcb);

			lefrSetMacroCbk(macrocb);
			lefrSetLibraryEndCbk(libendcb);
			lefrSetUnitsCbk(lefunitscb);
		}

	} //namespace lef_def
} //namespace file_io
