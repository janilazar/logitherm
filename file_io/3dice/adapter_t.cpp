#include "manager/manager_t.hpp"
#include "file_io/3dice/adapter_t.hpp"

#include <iostream>
#include <fstream>

namespace file_io
{
	namespace threed_ice
	{
		adapter_t::adapter_t(::util::log_t* log)
		:
			::file_io::adapter_t(log, "file_io::threed_ice::adapter_t")
		{}

		void adapter_t::write_floorplan_file(const std::string& filename, const std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& components)
		{
			std::ofstream of(filename);
			if(!of.is_open()) warning("write_floorplan_file(): '" + filename +"' could not be opened.");
			else
			{
				for(auto &component: components)
				{
					of << component.second->id << ":" << std::endl;
					
					for(auto &shape: component.second->get_shapes())
					{
						of << "    rectangle (" << shape->get_absolute_position()[0]*1e6l << ", " << shape->get_absolute_position()[1]*1e6l << ", ";
						of << shape->get_side_lengths()[0]*1e6l << ", " << shape->get_side_lengths()[1]*1e6l << ");" <<  std::endl;
					}
					of << std::endl;
				}
			}
			of.close();
		}

	} //namespace lef_def
} //namespace file_io
