#ifndef _PARSE_LEF_DEF_HPP_
#define _PARSE_LEF_DEF_HPP_

#include "file_io/base/adapter_t.hpp"
//#include "file_io/lef_def/def/include/defrReader.hpp"
//#include "file_io/lef_def/lef/include/lefrReader.hpp"

#include "unit/all_units.hpp"

#include <string>
#include <unordered_map>
#include <unordered_set>

namespace file_io
{
	namespace threed_ice
	{	
		class adapter_t	:	public	::file_io::adapter_t
		{
			public:
				adapter_t() = delete;
				adapter_t(::util::log_t* log);

				void write_floorplan_file(const std::string& filename, const std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& components);
				

		};
	}
}

#endif // _PARSE_LEF_DEF_HPP_
