#ifndef _PARSE_LIBERTY_HPP_
#define _PARSE_LIBERTY_HPP_


#include "file_io/liberty/engine/si2dr_liberty.h"
#include "file_io/base/adapter_t.hpp"

#include "unit/all_units.hpp"

#include <string>
#include <unordered_map>

namespace file_io
{
	namespace liberty
	{
		class adapter_t	:	public	file_io::adapter_t
		{
			private:
				/** ebbe a tablazatba mentem el az egyes cellak portjaihoz tartozo energiaertekeket **/
				std::unordered_map<std::string, std::unordered_map<std::string,::unit::energy_t>> energy_table;
				double capac_unit;
				double voltage_unit;
				double energy_unit;
			
			public:
				const std::string filename;
			
			
			private:				
				si2drGroupIdT get_library_group();
				si2drGroupIdT get_cell_group(si2drGroupIdT libgroup,const std::string& cellname);
				si2drGroupIdT get_pin_group(si2drGroupIdT cellgroup,const std::string& pinname);

				double get_capacity_unit(si2drGroupIdT libgroup);
				double get_power_unit(si2drGroupIdT libgroup);
				double get_voltage_unit(si2drGroupIdT libgroup);
				double get_energy_unit(si2drGroupIdT libgroup);
				
				si2drErrorT check_liberty();

				long double get_max_value(liberty_value_data *data);

				//long double lib_getInputCapac(const std::string& cellname,  const std::string& pinname);
				//long double get_energy(const std::string& cellname, const std::string& pinname); //-> átírni, hogy energy_t-t adjon vissza
				void read_liberty_file();
				void store_in_energy_table(const std::string& cellname, const std::string& pinname, long double energy_d);
				
			public:
				adapter_t() = delete;
				adapter_t(util::log_t* log, const std::string& filename);
				~adapter_t();

				unit::energy_t get_energy(const std::string& cellname, const std::string& pinname);
				
				//bool find_cell(const std::string& id);
				
				
		};
	}
}

#endif // _PARSE_LIBERTY_HPP_
