#include "file_io/liberty/adapter/adapter_t.hpp"
#include <cctype>
#include <limits>
#include <cmath>
#include <cstring>
#include <iostream>
#include <sstream>

#include "util/string_manipulation.hpp"

namespace file_io
{
	namespace liberty
	{
		
		adapter_t::adapter_t(util::log_t* log, const std::string& filename)
		:
			file_io::adapter_t(log, "file_io::liberty::adapter_t"),
			filename(filename)
		{
			read_liberty_file();
		}
		
		adapter_t::~adapter_t()
		{}
				
		si2drErrorT adapter_t::check_liberty()
		{
			si2drErrorT err;
			si2drGroupsIdT groups;
			si2drGroupIdT group;

			//////////////////////////////////////////
			// Check each group in the library
			//////////////////////////////////////////
			groups = si2drPIGetGroups(&err);
			while( !si2drObjectIsNull((group=si2drIterNextGroup(groups,&err)),&err) )
			{
				debug("check_liberty(): Checking the database...");

				si2drCheckLibertyLibrary(group, &err);

				if(err == SI2DR_NO_ERROR)
				{
					debug("check_liberty(): passed");
				}
				else
				{
					error("check_liberty(): errors detected");
				}
				

			}
			si2drIterQuit(groups,&err);

			return err;
		}

		double adapter_t::get_capacity_unit(si2drGroupIdT libgroup)
		{
			double ret = 0.0;
			si2drErrorT err;

			if (!si2drObjectIsNull(libgroup,&err))
			{
				si2drAttrIdT cap_attr;
				cap_attr = si2drGroupFindAttrByName(libgroup, (si2drStringT)"capacitive_load_unit", &err);
				if (err == SI2DR_NO_ERROR)
				{
					si2drValuesIdT values = si2drComplexAttrGetValues(cap_attr,&err);
					if (err == SI2DR_NO_ERROR)
					{
						si2drValueTypeT type;
						si2drInt32T i32 = 0;
						si2drFloat64T f64 = 0.0;
						si2drStringT str;
						si2drBooleanT b = SI2DR_FALSE;
						si2drExprT *exp;
						std::string unit;

						do
						{
							si2drIterNextComplexValue(values, &type, &i32, &f64, &str, &b, &exp, &err);
							switch (type)
							{
								case SI2DR_INT32: ret = i32; break;
								case SI2DR_FLOAT64: ret = f64; break;
								case SI2DR_STRING: unit = util::to_lower_case(str);
								case SI2DR_UNDEFINED_VALUETYPE:
								default: break;
							}

						}
						while (type != SI2DR_UNDEFINED_VALUETYPE);
						si2drIterQuit(values, &err);

						if (unit == "pf")
						{
							ret *= 1e-12;
						}
						else if (unit == "ff")
						{
							ret *= 1e-15;
						}
					}
				}
			}

			return ret;
		}

		double adapter_t::get_power_unit(si2drGroupIdT libgroup)
		{
			double ret =std::numeric_limits<double>::quiet_NaN();
			si2drErrorT err;

			if (!si2drObjectIsNull(libgroup,&err))
			{
				si2drAttrIdT pow_attr;
				pow_attr = si2drGroupFindAttrByName(libgroup,(si2drStringT)"leakage_power_unit", &err);
				if (err == SI2DR_NO_ERROR)
				{
					if ( si2drSimpleAttrGetValueType(pow_attr,&err) == SI2DR_STRING )
					{
						std::string powstr = util::to_lower_case(si2drSimpleAttrGetStringValue(pow_attr,&err));
						if (err == SI2DR_NO_ERROR)
						{
							if (powstr == "1mw")
							{
								ret = 1e-3;
							}
							else if (powstr == "1uw")
							{
								ret = 1e-6;
							}
							else if (powstr == "1nw")
							{
								ret = 1e-9;
							}
							else if (powstr == "1pw")
							{
								ret = 1e-12;
							}
						}
					}
				}
			}
			return ret;
		}

		double adapter_t::get_voltage_unit(si2drGroupIdT libgroup)
		{
			double ret = std::numeric_limits<double>::quiet_NaN();
			si2drErrorT err;

			si2drPIInit(&err);
			
			si2drReadLibertyFile(const_cast<char*>(filename.c_str()),&err);

			if( err == SI2DR_INVALID_NAME )
			{
				error("get_voltage_unit(): could not open " + filename + " for parsing -- exiting...");
			}
			else if (err == SI2DR_SYNTAX_ERROR )
			{
				error("get_voltage_unit(): syntax errors were detected in the input Liberty file!" );
			}

			if(!si2drObjectIsNull(libgroup,&err))
			{
				si2drAttrIdT voltage_attr;
				voltage_attr = si2drGroupFindAttrByName(libgroup, (si2drStringT)"voltage_unit", &err);
				if (err == SI2DR_NO_ERROR)
				{
					if ( si2drSimpleAttrGetValueType(voltage_attr,&err) == SI2DR_STRING )
					{
						std::string powstr = util::to_lower_case(si2drSimpleAttrGetStringValue(voltage_attr,&err));
						if (err == SI2DR_NO_ERROR)
						{
							if (powstr == "1v")
							{
								ret = 1.0;
							}
							else if (powstr == "100mv")
							{
								ret = 100e-3;
							}
							else if (powstr == "1mv")
							{
								ret = 1e-3;
							}
						}
					}
				}
			}

			return ret;
		}

		double adapter_t::get_energy_unit(si2drGroupIdT libgroup)
		{
			double v = get_voltage_unit(libgroup);
			double c = get_capacity_unit(libgroup);

			
			if (!std::isnan(v) && !std::isnan(c))
			{
				return c*v*v;
			}
			else return std::numeric_limits<double>::quiet_NaN();//itt lehet hogy errort kellene dobni
		}

		long double adapter_t::get_max_value(liberty_value_data *data)
		{
			long double ret = 0.0L;
			int num_values = 1;
			if (data->dimensions == 0)
				return ret;

			if (data->dimensions >= 1 && data->dim_sizes[0] >= 1)
				ret = data->values[0];

			for (int dims = 0; dims < data->dimensions; dims++)
			{
				num_values *= data->dim_sizes[dims];
			}

			for (int i=0; i < num_values; i++)
			{
				if (std::abs(data->values[i]) > ret)
					ret = std::abs(data->values[i]);
			}

			return ret;

		}

		void adapter_t::read_liberty_file()
		{
			si2drErrorT err;

			si2drPIInit(&err);
			
			si2drReadLibertyFile(const_cast<char*>(filename.c_str()),&err);

			if( err == SI2DR_INVALID_NAME )
			{
				error("read_liberty_file(): could not open " + filename + " for parsing -- exiting...");
			}
			else if (err == SI2DR_SYNTAX_ERROR )
			{
				error("read_liberty_file(): syntax errors were detected in the input Liberty file!" );
			}

			err = check_liberty();
			if (err == SI2DR_NO_ERROR)
			{

				si2drGroupIdT libgroup;
				si2drGroupsIdT libgroups = si2drPIGetGroups(&err);
				if(!si2drObjectIsNull(libgroup = si2drIterNextGroup(libgroups,&err), &err))
				{

					energy_unit = get_energy_unit(libgroup);

					si2drGroupIdT cellgroup;
					si2drGroupsIdT cellgroups = si2drGroupGetGroups(libgroup, &err);
					while(!si2drObjectIsNull(cellgroup = si2drIterNextGroup(cellgroups,&err), &err))
					{
						if(util::to_lower_case(si2drGroupGetGroupType(cellgroup,&err)) == "cell")
						{
							si2drNamesIdT cellnames = si2drGroupGetNames(cellgroup, &err);;
							std::string cellname = si2drIterNextName(cellnames,&err);
							si2drIterQuit(cellnames, &err);
							if(cellname == "") error("read_liberty_file(): unkown cell");

							si2drGroupIdT pingroup;
							si2drGroupsIdT pingroups = si2drGroupGetGroups(cellgroup, &err);
							while(!si2drObjectIsNull(pingroup = si2drIterNextGroup(pingroups,&err), &err))
							{
								if(util::to_lower_case(si2drGroupGetGroupType(pingroup,&err)) == "pin")
								{
									si2drNamesIdT pinnames = si2drGroupGetNames(pingroup, &err);
									std::string pinname = si2drIterNextName(pinnames,&err);
									si2drIterQuit(pinnames, &err);
									if(pinname == "") error("read_liberty_file(): unkown pin in cell '" + cellname + "'");

									long double energy = 0.0L;

									si2drGroupIdT internal_power_group;
									si2drGroupsIdT internal_power_groups = si2drGroupGetGroups(pingroup, &err);
									
									// Iterate through 'internal powers'
									while (!si2drObjectIsNull(internal_power_group = si2drIterNextGroup(internal_power_groups,&err), &err))
									{
										// If selected group IS NOT an 'internal power'
										if (util::to_lower_case(si2drGroupGetGroupType(internal_power_group,&err)) != "internal_power")
											continue;

										if (err == SI2DR_NO_ERROR)
										{
											//*****************************
											// Rise/Fall power groups	  *
											//*****************************
											si2drGroupIdT risefall_power_group;
											si2drGroupsIdT risefall_power_groups = si2drGroupGetGroups(internal_power_group, &err);
											if (err == SI2DR_NO_ERROR)
											{
												while ( !si2drObjectIsNull(risefall_power_group = si2drIterNextGroup(risefall_power_groups,&err), &err) )
												{
													// If selected group IS NOT a 'rise/fall_power'
													if ((util::to_lower_case(si2drGroupGetGroupType(risefall_power_group,&err)) != "fall_power") &&
														(util::to_lower_case(si2drGroupGetGroupType(risefall_power_group,&err)) != "rise_power"))
														continue;

													if (err == SI2DR_NO_ERROR)
													{
														liberty_value_data *values;
														values = liberty_get_values_data(risefall_power_group);

														long double curr_max = get_max_value(values);
														if (curr_max > energy) energy = curr_max;

														liberty_destroy_value_data(values);
													}

												}
												si2drIterQuit(risefall_power_groups,&err);
											}
										}
									}
									si2drIterQuit(internal_power_groups,&err);	
									store_in_energy_table(cellname, pinname, energy*energy_unit);
								}
							}
							si2drIterQuit(pingroups,&err);
						}
					}
					si2drIterQuit(cellgroups,&err);
				}
				si2drIterQuit(libgroups,&err);

			}
			si2drPIQuit(&err);
			
			if(err != SI2DR_NO_ERROR)
			{
				si2drErrorT err2;
				error(std::string("read_liberty_file(): ") + si2drPIGetErrorText(err,&err2));
			}
		}
		
		void adapter_t::store_in_energy_table(const std::string& cellname, const std::string& pinname, long double energy_d)
		{
			std::stringstream conv;
			conv << std::scientific << energy_d;
			std::string energy_str;
			conv >> energy_str;
			debug("store_in_energy_table(): cell '" + cellname + "' pin '" + pinname + "' energy=" + energy_str);
						
			auto cell_iterator = energy_table.find(cellname);
			auto energy = static_cast<unit::energy_t>(energy_d);
			if(cell_iterator == energy_table.end())
			{
				std::unordered_map<std::string, unit::energy_t> pin_table;
				pin_table.insert(std::make_pair(pinname,energy));
				energy_table.insert(std::make_pair(cellname,pin_table));
			}
			else
			{
				cell_iterator->second[pinname] = energy;
			}
		}
		
		unit::energy_t adapter_t::get_energy(const std::string& cellname, const std::string& pinname)
		{
			// debug("get_energy enter");
			auto cell_iterator = energy_table.find(cellname);
			if(cell_iterator != energy_table.end())
			{
				auto pin_iterator = cell_iterator->second.find(pinname);
				if(pin_iterator != cell_iterator->second.end())
				{
					std::stringstream conv;
					conv << std::scientific << static_cast<double>(pin_iterator->second);
					std::string energy_str;
					conv >> energy_str;
					debug("get_energy(): FOUND cell '" + cellname + "' pin '" + pinname + "' energy=" + energy_str);
					return pin_iterator->second;
				}
				else error("get_energy(): pin '" + pinname + "' was not found");
			}
			else warning("get_energy(): cell '" + cellname + "' was not found");
			return 0_J;
		}
		
	} //namespace liberty
} //namespace file_io
