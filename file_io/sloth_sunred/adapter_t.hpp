#ifndef _PARSE_SLOTH_SUNRED_HPP_
#define _PARSE_SLOTH_SUNRED_HPP_

#include "file_io/base/adapter_t.hpp"
#include "file_io/lef_def/adapter/adapter_t.hpp"
#include "unit/all_units.hpp"

#include <string>
#include <unordered_map>
#include <unordered_set>

namespace thermal
{
	namespace sloth
	{
		class adapter_t;
	}

	namespace sunred
	{
		class adapter_t;
	}
}

namespace file_io
{
	namespace sloth
	{	
		class adapter_t	:	public	file_io::adapter_t
		{
			public:
				using xy_length_t = layout::xy_length_t;
				using xy_pitch_t = layout::xy_pitch_t;

			private:
				/** floorplan neve, floorplan file utvonala **/
				std::unordered_map<std::string, std::string> floorplans;
				std::string sunred_file;

				file_io::lef_def::adapter_t lef_def_parser;

				std::unordered_set<std::string> lef_files;

			public:
				thermal::sloth::adapter_t* const thermal_engine;
				const std::string path;
				const std::string initfile;

			private:
				void read_floorplan_file(const std::string&, const std::string&);
				void read_initialization_file();

			public:
				adapter_t() = delete;
				adapter_t(thermal::sloth::adapter_t* thermal_engine, util::log_t* log, const std::string& path, const std::string& initfile);

				void read_files();
		};
	}

	namespace sunred
	{	
		class adapter_t	:	public	file_io::adapter_t
		{
			public:
				using xy_length_t = layout::xy_length_t;
				using xy_pitch_t = layout::xy_pitch_t;

			private:
				/** floorplan neve, floorplan file utvonala **/
				std::unordered_map<std::string, std::string> floorplans;
				std::string sunred_file;

				file_io::lef_def::adapter_t lef_def_parser;

				std::unordered_set<std::string> lef_files;

			public:
				thermal::sunred::adapter_t* const thermal_engine;
				const std::string path;
				const std::string initfile;

			private:
				void read_floorplan_file(const std::string&, const std::string&);
				void read_initialization_file();

			public:
				adapter_t() = delete;
				adapter_t(thermal::sunred::adapter_t* thermal_engine, util::log_t* log, const std::string& path, const std::string& initfile);

				void read_files();
		};
	}
}

#endif // _PARSE_SLOTH_SUNRED_HPP_
