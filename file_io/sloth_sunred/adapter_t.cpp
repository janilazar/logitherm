#include "manager/manager_t.hpp"
#include "file_io/sloth_sunred/adapter_t.hpp"

#include "thermal/sloth/adapter/material_t.hpp"
#include "thermal/sloth/adapter/adapter_t.hpp"
#include "thermal/sloth/adapter/layout/adapter_t.hpp"
#include "thermal/sloth/adapter/layout/component_t.hpp"

#include "thermal/sunred/engine/sunred.h"
#include "thermal/sunred/adapter/material_t.hpp"
#include "thermal/sunred/adapter/adapter_t.hpp"
#include "thermal/sunred/adapter/layout/adapter_t.hpp"
#include "thermal/sunred/adapter/layout/component_t.hpp"

#include <iostream>
#include <fstream>

namespace file_io
{
	namespace sloth
	{
		adapter_t::adapter_t(thermal::sloth::adapter_t* thermal_engine, util::log_t* log, const std::string& path, const std::string& initfile)
		:
			file_io::adapter_t(log, "file_io::sloth::adapter_t"),
			lef_def_parser(log),
			thermal_engine(thermal_engine),
			initfile(initfile),
			path(path)
		{}

		void adapter_t::read_files()
		{
			read_initialization_file();

			if(!lef_files.empty()) lef_def_parser.read_lef_files(lef_files);
			
			/** floorplan file-ok beolvasasa **/
			for(auto &it: floorplans) read_floorplan_file(it.first,it.second);
		}
		
		void adapter_t::read_initialization_file()
		{
			std::ifstream m_file(path + initfile);
			if(!m_file.is_open()) error("read_initialization_file(): '" + path + initfile + "' initialization file could not be opened");

			std::string type, filename;
			long double siz_x, siz_y;
			size_t res_x, res_y;
			while(m_file >> type)
			{
				if(type == "[floorplan]")
				{
					//floorplan_id-nek meg kell egyeznie egy layer_id-vel
					std::string floorplan_id;
					if (m_file >> floorplan_id >> filename) floorplans.insert(std::make_pair(floorplan_id, filename));
					else error("read_initialization_file(): expected floorplan id and filename after '" + type + "' command");
				}
				else if(type == "[material]")
				{
					std::string mat_name;
					double heat_cond;
					double heat_cap;
					if (m_file>>mat_name>>heat_cond>>heat_cap) thermal_engine->add_material(mat_name, heat_cond, heat_cap);
					else error("read_initialization_file(): expected a material name, heat conductivity and heat capacity after '" + type + "' command");	
				}
				else if(type == "[boundary]")
				{
					std::string side, boundary_type;
					double value;
					if (m_file >> side >> boundary_type >> value) thermal_engine->add_boundary_condition(thermal_engine->get_side(side), thermal_engine->get_boundary(boundary_type), value);
					else error("read_initialization_file(): expected side, boundary type and value after '" + type +"' command");

				}
				else if(type == "[layer]")
				{
					std::string layer_id, material_id;
					long double thickness;
					if (m_file >> layer_id >> material_id >> thickness) thermal_engine->add_layer(layer_id, material_id, static_cast<unit::length_t>(thickness));
					else error("read_initialization_file(): expected layer id, material id and thickness after '" + type + "' command");
				}
				else if(type == "[lef]")
				{
					if (m_file >> filename) lef_files.insert(filename);
					else error("read_initialization_file(): expected a filename after '" + type + "' command");
				}
				else if(type == "[layout]")
				{
					if(m_file >> siz_x >> siz_y >> res_x >> res_y) thermal_engine->create_layout(xy_length_t(siz_x, siz_y), xy_pitch_t(res_x, res_y));
					else error("read_initialization_file(): expected layout size and resolution after '" + type + "' command");
				}
				else error("read_initialization_file(): unrecognized command '" + type + "'");
			}
			
			m_file.close();
		}

		void adapter_t::read_floorplan_file(const std::string& fp_id, const std::string& file)
		{
			std::ifstream fp_file(path + file);
			if(!fp_file.is_open())
			{
				error("read_floorplan_file(): error occured while reading floorplan file: '" + path +  file + "' could not be opened");
			}
			
			std::string parent_id;
			std::string component_id;
			long double x_pos, y_pos, x_size, y_size;
			layout::sloth::adapter_t* layout_ptr = dynamic_cast<layout::sloth::adapter_t*>(thermal_engine->get_layout());


			while(fp_file >> parent_id >> component_id)
			{
				/** parent_ptr beallitasa **/
				layout::sloth::component_t* parent_ptr = nullptr;
				if("[null]" == parent_id)
				{
					parent_ptr = nullptr;
				}
				else
				{
					parent_ptr = dynamic_cast<layout::sloth::component_t*>(layout_ptr->get_component(parent_id));
					if(nullptr == parent_ptr) error("read_floorplan_file(): parent '" + parent_id + "' of component '" + component_id + "' was not found");
				}

				/** lehetoseg van a floorplan file-ban def almodulokat bepeldanyositani **/
				if("[def]" == component_id)
				{
					std::string def_file;
					if(fp_file >> def_file >> x_pos >> y_pos)
					{
						layout::xy_length_t offset = {x_pos, y_pos};
						lef_def_parser.read_def_file(def_file);
						for(auto& it: lef_def_parser.get_instance_table())
						{
							new layout::sloth::component_t(it.first, fp_id, it.second.first + offset, it.second.second, parent_ptr, layout_ptr);
						}
					}
					else
					{
						error("read_floorplan_file(): expected filename and position after '" + component_id + "' command");
					}
				}
				else
				{
					if(fp_file >> x_pos >> y_pos >> x_size >> y_size)
					{	
						
						new layout::sloth::component_t(component_id, fp_id, xy_length_t(x_pos, y_pos), xy_length_t(x_size,y_size), parent_ptr, layout_ptr);
					}
					else
					{
						error("read_floorplan_file(): expected position and size after component id");
					}
				}
			}
			if(!fp_file.eof()) error("read_floorplan_file(): '" + path + file + "' has invalid format");
			fp_file.close();
		}
	} //namespace sloth

	namespace sunred
	{
		adapter_t::adapter_t(thermal::sunred::adapter_t* thermal_engine, ::util::log_t* log, const std::string& path, const std::string& initfile)
		:
			file_io::adapter_t(log, "file_io::sunred::adapter_t"),
			lef_def_parser(log),
			thermal_engine(thermal_engine),
			initfile(initfile),
			path(path)
		{}

		void adapter_t::read_files()
		{
			read_initialization_file();

			if(!lef_files.empty()) lef_def_parser.read_lef_files(lef_files);
			
			/** floorplan file-ok beolvasasa **/
			for(auto &it: floorplans) read_floorplan_file(it.first,it.second);
		}
		
		void adapter_t::read_initialization_file()
		{
			std::ifstream m_file(path + initfile);
			if(!m_file.is_open()) error("read_initialization_file(): '" + path + initfile + "' initialization file could not be opened");

			std::string type, filename;
			long double siz_x, siz_y;
			size_t res_x, res_y;
			while(m_file >> type)
			{
				if(type == "[floorplan]")
				{
					//floorplan_id-nek meg kell egyeznie egy layer_id-vel
					std::string floorplan_id;
					if (m_file >> floorplan_id >> filename) floorplans.insert(std::make_pair(floorplan_id, filename));
					else error("read_initialization_file(): expected floorplan id and filename after '" + type + "' command");
				}
				else if(type == "[material]")
				{
					std::string mat_name;
					double heat_cond;
					double heat_cap;
					if (m_file>>mat_name>>heat_cond>>heat_cap) thermal_engine->add_material(mat_name, heat_cond, heat_cap);
					else error("read_initialization_file(): expected a material name, heat conductivity and heat capacity after '" + type + "' command");	
				}
				else if(type == "[boundary]")
				{
					std::string side, boundary_type;
					double value;
					if (m_file >> side >> boundary_type >> value) thermal_engine->add_boundary_condition(thermal_engine->get_side(side), thermal_engine->get_boundary(boundary_type), value);
					else error("read_initialization_file(): expected side, boundary type and value after '" + type +"' command");

				}
				else if(type == "[layer]")
				{
					std::string layer_id, material_id;
					long double thickness;
					if (m_file >> layer_id >> material_id >> thickness) thermal_engine->add_layer(layer_id, material_id, static_cast<::unit::length_t>(thickness));
					else error("read_initialization_file(): expected layer id, material id and thickness after '" + type + "' command");
				}
				else if(type == "[lef]")
				{
					if (m_file >> filename) lef_files.insert(filename);
					else error("read_initialization_file(): expected a filename after '" + type + "' command");
				}
				else if(type == "[sunred]")
				{
					if (m_file >> filename >> siz_x >> siz_y >> res_x >> res_y)
					{
						thermal_engine->initialize_sunred_from_file(path, filename, xy_length_t(siz_x, siz_y), xy_pitch_t(res_x, res_y));
						thermal_engine->create_layout(xy_length_t(siz_x, siz_y), xy_pitch_t(res_x, res_y));
					}
					else error("read_initialization_file(): expected a filename, layout size and resolution after '" + type + "' command");
				}
				else if(type == "[uchannel]")
				{
					if (m_file >> filename)
					{
						thermal_engine->initialize_uchannel_from_file(path, filename);
					}
					else error("read_initialization_file(): expected a filename, layout size and resolution after '" + type + "' command");
				}
				else if(type == "[layout]")
				{
					if(m_file >> siz_x >> siz_y >> res_x >> res_y) thermal_engine->create_layout(xy_length_t(siz_x, siz_y), xy_pitch_t(res_x, res_y));
					else error("read_initialization_file(): expected layout size and resolution after '" + type + "' command");
				}
				else error("read_initialization_file(): unrecognized command '" + type + "'");
			}
			
			m_file.close();
		}

		void adapter_t::read_floorplan_file(const std::string& fp_id, const std::string& file)
		{
			std::ifstream fp_file(path + file);
			if(!fp_file.is_open()) error("read_floorplan_file(): error occured while reading floorplan file: '" + path +  file + "' could not be opened");
			
			std::string parent_id;
			std::string component_id;
			long double x_pos, y_pos, x_size, y_size;
			layout::sunred::adapter_t* layout_ptr = dynamic_cast<layout::sunred::adapter_t*>(thermal_engine->get_layout());

			while(fp_file >> parent_id >> component_id)
			{
				/** parent_ptr beallitasa **/
				layout::sunred::component_t* parent_ptr = nullptr;
				if("[null]" == parent_id)
				{
					parent_ptr = nullptr;
				}
				else
				{
					parent_ptr = dynamic_cast<::layout::sunred::component_t*>(thermal_engine->get_layout()->get_component(parent_id));
					if(nullptr == parent_ptr) error("read_floorplan_file(): parent '" + parent_id + "' of component '" + component_id + "' was not found");
				}

				/** lehetoseg van a floorplan file-ban def almodulokat bepeldanyositani **/
				if("[def]" == component_id)
				{
					std::string def_file;
					if(fp_file >> def_file >> x_pos >> y_pos)
					{
						layout::xy_length_t offset = {x_pos, y_pos};
						lef_def_parser.read_def_file(def_file);
						
						for(auto& it: lef_def_parser.get_instance_table())
						{
							std::string def_component_id = ((parent_ptr != nullptr) ? parent_ptr->id + "." : "") + it.first;
							new layout::sunred::component_t(def_component_id, fp_id, it.second.first + offset, it.second.second, parent_ptr, layout_ptr);
							//if(layout_ptr->adapter->manager->get_logic_engine()->search_for_component(def_component_id))
							// {
							// 	layout_ptr->adapter->manager->add_dissipator_component(def_component_id);
							// 	//layout_ptr->adapter->manager->add_display_component(def_component_id);
							// }
						}
					}
					else
					{
						error("read_floorplan_file(): expected filename and position after '" + component_id + "' command");
					}
				}
				else
				{
					if(fp_file >> x_pos >> y_pos >> x_size >> y_size)
					{	
						
						new layout::sunred::component_t(component_id, fp_id, xy_length_t(x_pos, y_pos), xy_length_t(x_size,y_size), parent_ptr, layout_ptr);
					}
					else
					{
						error("read_floorplan_file(): expected position and size after component id");
					}
				}
			}
			
			if(!fp_file.eof()) error("read_floorplan_file(): '" + path + file + "' has invalid format");
			fp_file.close();
		}
	} //namespace sunred
} //namespace file_io
