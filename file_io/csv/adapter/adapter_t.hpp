#ifndef _PARSE_CSV_HPP_
#define _PARSE_CSV_HPP_


#include "file_io/csv/engine/include/csv_parser/csv_parser.hpp"
#include "file_io/base/adapter_t.hpp"

#include "unit/all_units.hpp"

#include <string>
#include <unordered_map>
#include <unordered_set>

namespace file_io
{
	namespace csv
	{
		class adapter_t	:	public	file_io::adapter_t
		{
			private:
				/** ebbe a tablazatba mentem el az egyes cellak portjaihoz tartozo energiaertekeket **/
				std::unordered_map<std::string, std::unordered_map<std::string,::unit::energy_t>> energy_table;
				
			public:
				adapter_t() = delete;
				adapter_t(util::log_t* log);
				~adapter_t();
				
				void read_file(const std::string& modulename, const std::string& filename);
				void read_files(const std::unordered_map<std::string, std::string>& names);
				unit::energy_t get_energy(const std::string& modulename, const std::string& portname);			
				
		};
	}
}

#endif // _PARSE_CSV_HPP_
