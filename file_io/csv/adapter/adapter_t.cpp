#include "file_io/csv/adapter/adapter_t.hpp"
#include "util/string_manipulation.hpp"

namespace file_io
{
	namespace csv
	{
		adapter_t::adapter_t(util::log_t* log)
		:
			file_io::adapter_t(log, "file_io::csv::adapter_t")
		{}

		void adapter_t::read_file(const std::string& modulename, const std::string& filename)
		{
			/* Declare the variables to be used */
			const char field_terminator = ',';
			const char line_terminator  = '\n';
			const char enclosure_char   = '"';
			const size_t first_record = 1;
			
			csv_parser file_parser;

			/* Define how many records we're gonna skip. This could be used to skip the column definitions. */
			file_parser.set_skip_lines(first_record);

			/* Specify the file to parse */
			file_parser.init(filename.c_str());

			/* Here we tell the parser how to parse the file */
			file_parser.set_enclosed_char(enclosure_char, ENCLOSURE_OPTIONAL);

			file_parser.set_field_term_char(field_terminator);

			file_parser.set_line_term_char(line_terminator);

			size_t row_count = first_record;

			std::unordered_map<std::string, unit::energy_t> port_table;

			/* Check to see if there are more records, then grab each row one at a time */
			while(file_parser.has_more_rows())
			{
			    //unsigned int i = 0;

			    /* Get the record */
			    csv_row row = file_parser.get_row();
			    if(row.size() != 2)
			    {
			    	warning("Format mismatch in file '" + filename + "' at line '" + std::to_string(row_count) + "'");
			    }
			    else
			    {
			    	std::pair<std::string, unit::energy_t> record;

			    	record.first = row[0];
			    	record.second = 0.0_J;
			    	try
			    	{
			    		record.second = static_cast<unit::energy_t>(std::stod(row[1]));
			    	}
			    	catch(const std::invalid_argument& ia)
			    	{
			    		warning("Invalid value  (" + row[1] + ") for port '" + record.first + "' in file '" + filename + "'");
			    	}

			    	if(energy_table.find(record.first) != energy_table.end())
			    	{
			    		warning("Multiple definition of energy coefficient for port '" + record.first + "'");
			    	}
			    	else
			    	{
			    		port_table.insert(record);
			    	}
			    }
			}
			energy_table.insert(std::make_pair(modulename, port_table));
		}

		void adapter_t::read_files(const std::unordered_map<std::string, std::string>& names)
		{
			for(auto &name: names)
			{
				std::string modulename = name.first;
				std::string filename = name.second;

				read_file(modulename, filename);
			}
		}

		adapter_t::~adapter_t()
		{}

		unit::energy_t adapter_t::get_energy(const std::string& modulename, const std::string& portname)
		{
			auto module_iterator = energy_table.find(modulename);
			if(module_iterator != energy_table.end())
			{
				auto port_iterator = module_iterator->second.find(portname);
				if(port_iterator != module_iterator->second.end())
				{
					std::stringstream conv;
					conv << std::scientific << static_cast<double>(port_iterator->second);
					std::string energy_str;
					conv >> energy_str;
					debug("get_energy(): FOUND module '" + modulename + "' port '" + portname + "' energy=" + energy_str);
					return port_iterator->second;
				}
				else error("get_energy(): port '" + portname + "' was not found");
			}
			else warning("get_energy(): module '" + modulename + "' was not found");
			return 0_J;
		}
	}
}