#ifndef _VERILOG_COMPONENT_H_
#define _VERILOG_COMPONENT_H_

#include "logic/base/adapter_t.hpp"

#include <vpi_user.h>

namespace logitherm
{
	class manager_t;
}

namespace logic
{
	class adapter_t;
}

namespace logic
{
	namespace verilog
	{
		/** forward decl **/
		class adapter_t;
		class activity_trace_t;
		
		class component_t	:	public ::logic::component_t
		{
			public:
				const std::string short_id;
				const std::string component_type;
				
			public:
				component_t(::logic::adapter_t* adapter, const std::string& full_id, const std::string& short_id, const std::string& component_type, ::logic::verilog::component_t* parent);
				std::ostream& print_component(std::ostream& os, std::string indent = "") override;
				
				//void add_activity_trace(const std::string& path);
				activity_trace_t* add_activity_trace(const std::string& path, const std::string& activity_id);
		};
	}
}
#endif //_VERILOG_COMPONENT_H_
