#ifndef _VERILOG_ADAPTER_H_
#define _VERILOG_ADAPTER_H_


#include "logic/base/adapter_t.hpp"
#include "file_io/liberty/adapter/adapter_t.hpp"

/** forward decl, ne kelljen include-olni **/
//typedef void *vpiHandle;

namespace logitherm
{
	class manager_t;
}

namespace util
{
	class log_t;
}

namespace logic
{
	namespace verilog
	{
		/**forward decl, ne kelljen include-olni **/
		class component_t;
		
		class adapter_t	:	public logic::adapter_t
		{
			/** /home/lazar/mentor_ams/questasim/v10.4c_5/examples/verilog/vpi/simple_vpi/vpi_test.c-bol masolom **/
			//private:
			//	::file_io::liberty::adapter_t liberty_parser;
			
			public:
				adapter_t() = delete;
				
				adapter_t(logitherm::manager_t* manager, util::log_t* log);
				
				~adapter_t();
				
				// void create_components(vpiHandle parent);

				// void create_cell_dissipation(vpiHandle parent, file_io::liberty::adapter_t& liberty_parser);
				// void register_cell_ports(vpiHandle mod_handle, logic::verilog::component_t* mod_ptr, file_io::liberty::adapter_t& liberty_parser);
		};
	}
}

#endif //_VERILOG_ADAPTER_H_
