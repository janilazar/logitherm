#ifndef _VERILOG_ACTIVITY_H_
#define _VERILOG_ACTIVITY_H_

#include "logic/base/activity_t.hpp"
#include <vpi_user.h>

namespace logic
{
	namespace verilog
	{
		class activity_t	:	public logic::activity_t
		{
			private:
				double time;
								
			public:
				/**
				 * default konstruktor
				**/ 
				activity_t() = delete;
				
				/**
				 * ez a konstruktor kell a push_back-nek
				 * (sc_dissipator_module-ban az activities vector egy ilyen tipust tartalmaz)
				**/ 
				activity_t(const std::string& id, const std::function<::unit::energy_t()> &f);
				
				~activity_t();
				
				void activity(double current_time);
		};
	}
}

#endif //_VERILOG_ACTIVITY_H_
