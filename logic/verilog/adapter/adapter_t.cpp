#include "logic/verilog/adapter/adapter_t.hpp"
#include "logic/verilog/adapter/component_t.hpp"
#include "logic/verilog/adapter/activity_t.hpp"
#include "manager/manager_t.hpp"
#include "util/log_t.hpp"
#include "util/string_manipulation.hpp"
#include <vpi_user.h>

#include "logic/verilog/adapter/activity_trace_t.hpp"

#include <limits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>

static logitherm::manager_t* manager_ptr = nullptr;
static thermal::adapter_t* thermal_engine_ptr = nullptr;
static util::thermal_engine_t thermal_engine_type;
static logic::verilog::adapter_t* logic_engine_ptr = nullptr;


static int check_error(void)
{
	int              error_code;
	s_vpi_error_info error_info;

	error_code = vpi_chk_error(&error_info);
	if (error_code && error_info.message)
	{
		vpi_printf(const_cast<PLI_BYTE8*>("  %s\n"), error_info.message); // ki irta meg a vpi_printf-et ugy, hogy nem const format-ot var? 21. szazad
	}

	return error_code;
}

PLI_INT32 activity_trace_t_callback(p_cb_data ptr)
{
	
	auto* activity_trace_ptr = reinterpret_cast<logic::verilog::activity_trace_t*>(ptr->user_data);
	activity_trace_ptr->increment(ptr->time->real);
	return 0;
}

PLI_INT32 add_min_temperature_trace(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$add_min_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$add_min_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}

		layout::adapter_t* layout_ptr = thermal_engine_ptr->get_layout();
		
		if(nullptr == layout_ptr)
		{
			std::cerr << "$add_min_temperature_trace(): layout is nullptr, layout must be initialized first" << std::endl;
			vpi_printf(const_cast<char*>("$add_min_temperature_trace(): layout is nullptr, layout must be initialized first'\n"));
			return vpiError;
		}

		
		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = util::remove(util::remove(argval.value.str, '\\'), ' ');
		
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			
			if(layout_ptr->search_for_component(comp_id))
			{
				layout_ptr->get_component(comp_id)->add_min_temperature_trace(path);
			}
			else
			{
				layout_ptr->warning("$add_min_temperature_trace(): layout::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_min_temperature_trace(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$add_min_temperature_trace(): invalid argument\n"));
			return vpiError;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$add_min_temperature_trace(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$add_min_temperature_trace(): unkown error occured\n"));
		return vpiError;
	}
	return 0;
}

PLI_INT32 add_max_temperature_trace(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$add_max_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$add_max_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}

		layout::adapter_t* layout_ptr = thermal_engine_ptr->get_layout();
		
		if(nullptr == layout_ptr)
		{
			std::cerr << "$add_max_temperature_trace(): layout is nullptr, layout must be initialized first" << std::endl;
			vpi_printf(const_cast<char*>("$add_max_temperature_trace(): layout is nullptr, layout must be initialized first'\n"));
			return vpiError;
		}

		
		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = util::remove(util::remove(argval.value.str, '\\'), ' ');
		
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			
			if(layout_ptr->search_for_component(comp_id))
			{
				layout_ptr->get_component(comp_id)->add_max_temperature_trace(path);
			}
			else
			{
				layout_ptr->warning("$add_max_temperature_trace(): layout::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_max_temperature_trace(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$add_max_temperature_trace(): invalid argument\n"));
			return vpiError;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$add_max_temperature_trace(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$add_max_temperature_trace(): unkown error occured\n"));
		return vpiError;
	}
	return 0;
}

PLI_INT32 add_avg_temperature_trace(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$add_avg_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$add_avg_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}

		layout::adapter_t* layout_ptr = thermal_engine_ptr->get_layout();
		
		if(nullptr == layout_ptr)
		{
			std::cerr << "$add_avg_temperature_trace(): layout is nullptr, layout must be initialized first" << std::endl;
			vpi_printf(const_cast<char*>("$add_avg_temperature_trace(): layout is nullptr, layout must be initialized first'\n"));
			return vpiError;
		}

		
		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = util::remove(util::remove(argval.value.str, '\\'), ' ');
		
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			
			if(layout_ptr->search_for_component(comp_id))
			{
				layout_ptr->get_component(comp_id)->add_avg_temperature_trace(path);
			}
			else
			{
				layout_ptr->warning("$add_avg_temperature_trace(): layout::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_avg_temperature_trace(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$add_avg_temperature_trace(): invalid argument\n"));
			return vpiError;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$add_avg_temperature_trace(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$add_avg_temperature_trace(): unkown error occured\n"));
		return vpiError;
	}
	return 0;
}

PLI_INT32 add_activity_trace(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == logic_engine_ptr)
		{
			std::cerr << "$add_activity_trace(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$add_activity_trace(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}
		
		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = util::remove(util::remove(argval.value.str, '\\'), ' ');
		
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string net_id = argval.value.str;
		
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			
			if(logic_engine_ptr->search_for_component(comp_id))
			{
				vpiHandle mod_handle = vpi_handle_by_name(const_cast<char*>(comp_id.c_str()), nullptr);
				if(mod_handle == nullptr) logic_engine_ptr->error("module handle of component '" + comp_id + "' was not found");
				
				/** megkeresem az adott nevu netet **/
				vpiHandle net_itr, net_handle;
				net_itr = vpi_iterate(vpiNet,mod_handle);
				std::string net_name;
				
				std::string mod_name = util::remove(vpi_get_str(vpiFullName, mod_handle), '\\');
				logic_engine_ptr->debug("$add_activity_trace(): SEARCHING net: '" + net_id + "' in module '" + mod_name + "'");
				while (net_handle = vpi_scan(net_itr))
				{
					net_name = vpi_get_str(vpiName, net_handle);
					if(net_name == net_id)
					{
						/** hozzunk letre egy uj activity_trace_t es adjuk hozza **/
						logic::verilog::component_t* comp_ptr = dynamic_cast<::logic::verilog::component_t*>(logic_engine_ptr->get_component(comp_id));
						if(nullptr == comp_ptr) logic_engine_ptr->error("component '" + comp_id + "' has invalid type");
						
						logic::verilog::activity_trace_t* activity_trace_ptr = comp_ptr->add_activity_trace(path, net_id);
						/**activity_t activity() fv-et kellene valahogy bejegyezni verilogba **/
						s_vpi_time time;
						time.type = vpiScaledRealTime;
						
						s_cb_data cb_data;
						s_vpi_value returnvalue; //visszateresi ertek tipusat allitjuk be vele
						returnvalue.format = vpiHexStrVal;
						
						cb_data.reason = cbValueChange;
						cb_data.time = &time;
						cb_data.obj	 = net_handle;
						cb_data.user_data = reinterpret_cast<PLI_BYTE8*>(activity_trace_ptr); // jaaaj de szeep... :)
						cb_data.value = &returnvalue;
						cb_data.cb_rtn = activity_trace_t_callback;
						
						vpiHandle cb_handle = vpi_register_cb(&cb_data);
						vpi_free_object(cb_handle);
						
						logic_engine_ptr->debug("$add_activity_trace(): FOUND net: " + net_id);
						break;
					}
				}
				if(nullptr == net_handle) logic_engine_ptr->warning("$add_activity_trace(): NOT FOUND net: '" + net_id + "' in module '" + mod_name + "'");
				
			}
			else
			{
				logic_engine_ptr->warning("$add_activity_trace(): logic::verilog::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_activity_trace(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$add_activity_trace(): invalid argument\n"));
			return vpiError;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "VPI add_activity_trace(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("VPI add_activity_trace(): unkown error occured\n"));
		return vpiError;
	}
	return 0;
}

PLI_INT32 add_dynamic_dissipation_trace(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == logic_engine_ptr)
		{
			std::cerr << "$add_dynamic_dissipation_trace(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$add_dynamic_dissipation_trace(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}
		
		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = argval.value.str;
		
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			
			if(logic_engine_ptr->search_for_component(comp_id))
			{
				logic_engine_ptr->get_component(comp_id)->add_dynamic_dissipation_trace(path);
			}
			else
			{
				logic_engine_ptr->warning("$add_dynamic_dissipation_trace(): logic::verilog::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_dynamic_dissipation_trace(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$add_dynamic_dissipation_trace(): invalid argument\n"));
			return vpiError;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$add_dynamic_dissipation_trace(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$add_dynamic_dissipation_trace(): unkown error occured\n"));
		return vpiError;
	}
	return 0;
}

PLI_INT32 add_static_dissipation_trace(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == logic_engine_ptr)
		{
			std::cerr << "$add_static_dissipation_trace(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$add_static_dissipation_trace(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}
		
		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = argval.value.str;
		
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			if(logic_engine_ptr->search_for_component(comp_id))
			{
				logic_engine_ptr->get_component(comp_id)->add_static_dissipation_trace(path);
			}
			else
			{
				logic_engine_ptr->warning("$add_static_dissipation_trace(): logic::verilog::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_static_dissipation_trace(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$add_static_dissipation_trace(): invalid argument\n"));
			return vpiError;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$add_static_dissipation_trace(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$add_static_dissipation_trace(): unkown error occured"));
		return vpiError;
	}
	return 0;
}

/**
 * callback-kent van bejegyezve
 * meghivja a manager_t destruktorat
**/ 
PLI_INT32 restart_simulation(p_cb_data ptr)
{
	try
	{
		if(manager_ptr != nullptr)
		{
			manager_ptr->~manager_t();
			manager_ptr = nullptr;
			logic_engine_ptr = nullptr;
			thermal_engine_ptr = nullptr;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$restart_simulation(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$restart_simulation(): unkown error occured\n"));
		return vpiError;
	}
	return 0;
}

/**
 * beolvassa verilogbol a termikus motor tipusat, inicializalo fajl utvonalat es nevet
**/ 

PLI_INT32 set_thermal_engine(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$set_thermal_engine(): LogiTherm manager is nullptr, it must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$set_thermal_engine(): LogiTherm manager is nullptr, it must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}

		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();

		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$set_thermal_engine(): invalid first argument" << std::endl;
			vpi_printf(const_cast<char*>("$set_thermal_engine(): invalid first argument\n"));
			return vpiError;
		}

		std::string thermal_engine_str = argval.value.str;
		
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();

		if(error_code)
		{
			std::cerr << "$set_thermal_engine(): invalid second argument" << std::endl;
			vpi_printf(const_cast<char*>("$set_thermal_engine(): invalid second argument\n"));
			return vpiError;
		}
		
		std::string path = argval.value.str;
		
		argh = vpi_scan(args_iter);
		error_code = check_error();
		
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$set_thermal_engine(): invalid third argument" << std::endl;
			vpi_printf(const_cast<char*>("$set_thermal_engine(): invalid third argument\n"));
			return vpiError;
		}

		std::string file = argval.value.str;

		if(thermal_engine_str == "sunred")
		{
			/* initializing the thermal engine to thermal::sunred::adapter_t */
			thermal_engine_type = util::thermal_engine_t::sunred;
			thermal_engine_ptr = manager_ptr->set_thermal_engine(thermal_engine_type);
		}
		else if(thermal_engine_str == "sloth")
		{
			/* initializing the thermal engine to thermal::sunred::adapter_t */
			thermal_engine_type = util::thermal_engine_t::sloth;
			thermal_engine_ptr = manager_ptr->set_thermal_engine(thermal_engine_type);
		}
		else if(thermal_engine_str == "3d-ice")
		{
			/* initializing the thermal engine to thermal::sunred::adapter_t */
			thermal_engine_type = util::thermal_engine_t::threedice;
			thermal_engine_ptr = manager_ptr->set_thermal_engine(thermal_engine_type);
		}
		else
		{
			std::cerr << "$set_thermal_engine(): thermal engine type can not be recognized" << std::endl;
			vpi_printf(const_cast<char*>("set_thermal_engine(): thermal engine type can not be recognized\n"));
			return vpiError;
		}
		/** termikus motor inicializalasa fajlbol **/
		thermal_engine_ptr->read_files(path, file);
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$set_thermal_engine(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("set_thermal_engine(): unkown error occured\n"));
		return vpiError;
	}
	return 0;
}

/**
 * beolvassa az adott liberty file-t es beallitja az egyezo cellak fogyasztasat
*/
PLI_INT32 print_dissipation_map(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$print_dissipation_map(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'" << std::endl;
			vpi_printf(const_cast<char*>("$print_dissipation_map(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'\n"));
			return vpiError;
		}

		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();

		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$print_temperature_map(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$print_temperature_map(): invalid argument\n"));
			return vpiError;
		}

		std::string file = argval.value.str;
		
		std::ofstream of(file);
		if(of.is_open())
		{
			thermal_engine_ptr->print_dissipation_map(of);
			of.close();
		}
		else
		{
			std::cerr << "$print_dissipation_map(): could not open '" << file << "'" << std::endl;
			vpi_printf(const_cast<char*>(("$print_dissipation_map(): could not open '" + file + "'\n").c_str()));
			return vpiError;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$print_dissipation_map(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$print_dissipation_map(): unkown error occured\n"));
		return vpiError;
	}

	return 0;
}

/**
 * beolvassa az adott liberty file-t es beallitja az egyezo cellak fogyasztasat
*/
PLI_INT32 print_temperature_map(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$print_temperature_map(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'" << std::endl;
			vpi_printf(const_cast<char*>("$print_temperature_map(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'\n"));
			return vpiError;
		}

		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();

		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$print_temperature_map(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$print_temperature_map(): invalid argument\n"));
			return vpiError;
		}

		std::string file = argval.value.str;
		
		std::ofstream of(file);
		if(of.is_open())
		{
			thermal_engine_ptr->print_temperature_map(of);
			of.close();
		}
		else
		{
			std::cerr << "$print_temperature_map(): could not open '" << file << "'" << std::endl;
			vpi_printf(const_cast<char*>(("$print_temperature_map(): could not open '" + file + "'\n").c_str()));
			return vpiError;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$print_temperature_map(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$print_temperature_map(): unkown error occured\n"));
		return vpiError;
	}

	return 0;
}


/**
 * beolvassa az adott liberty file-t es beallitja az egyezo cellak fogyasztasat
*/
PLI_INT32 print_temperature_map_in_svg(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$print_temperature_map_in_svg(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$print_temperature_map_in_svg(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}

		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$print_temperature_map_in_svg(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'" << std::endl;
			vpi_printf(const_cast<char*>("$print_temperature_map_in_svg(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'\n"));
			return vpiError;
		}

		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();

		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$print_temperature_map_in_svg(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$print_temperature_map_in_svg(): invalid argument\n"));
			return vpiError;
		}

		std::string file = argval.value.str;
		
		std::ofstream of(file);
		if(of.is_open())
		{
			manager_ptr->print_temperature_map_in_svg(of);
			of.close();
		}
		else
		{
			std::cerr << "$print_temperature_map_in_svg(): could not open '" << file << "'" << std::endl;
			vpi_printf(const_cast<char*>(("$print_temperature_map_in_svg(): could not open '" + file + "'\n").c_str()));
			return vpiError;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$print_temperature_map_in_svg(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$print_temperature_map_in_svg(): unkown error occured\n"));
		return vpiError;
	}

	return 0;
}

/**
 * svg-ben megjelenitendo component
*/
PLI_INT32 add_display_component(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$add_display_component(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'" << std::endl;
			vpi_printf(const_cast<char*>("$add_display_component(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'\n"));
			return vpiError;
		}

		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();

		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$add_display_component(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$add_display_component(): invalid argument\n"));
			return vpiError;
		}

		std::string component_id = argval.value.str;
		
		manager_ptr->add_display_component(component_id);
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$add_display_component(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$add_display_component(): unkown error occured\n"));
		return vpiError;
	}

	return 0;
}

/**
 * beallitja a log file-ok utvonalat
*/
PLI_INT32 set_log_file_path(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$set_log_file_path(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$set_log_file_path(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}

		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();

		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$set_log_file_path(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$set_log_file_path(): invalid argument\n"));
			return vpiError;
		}

		std::string path = argval.value.str;

		manager_ptr->set_log_file_path(path);
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$set_log_file_path(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$set_log_file_path(): unkown error occured\n"));
		return vpiError;
	}

	return 0;
}

/**
 * beolvassa az adott liberty file-t es beallitja az egyezo cellak fogyasztasat
*/
PLI_INT32 set_notification_level(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$set_notification_level(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$set_notification_level(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}

		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();

		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$set_notification_level(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$set_notification_level(): invalid argument\n"));
			return vpiError;
		}

		std::string level_str = argval.value.str;

		if(level_str == "debug") manager_ptr->set_notification_level(util::level_t::debug);
		else if(level_str == "warning") manager_ptr->set_notification_level(util::level_t::warning);
		else if(level_str == "error") manager_ptr->set_notification_level(util::level_t::error);
		else
		{
			std::cerr << "$set_notification_level(): invalid argument '" << level_str << "'" << std::endl;
			vpi_printf(const_cast<char*>(("$set_notification_level(): invalid argument '" + level_str + "'").c_str()));
			return vpiError;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$set_notification_level(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$set_notification_level(): unkown error occured\n"));
		return vpiError;
	}

	return 0;
}

/**
 * callback-kent van bejegyezve
 * meghivja a logic adapter run_simulation_cycle fv-et
**/ 
PLI_INT32 run_simulation_cycle(p_cb_data ptr)
{
	try
	{

		if(nullptr == manager_ptr)
		{
			std::cerr << "$run_simulation_cycle(): LogiTherm manager is nullptr, it must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$run_simulation_cycle(): LogiTherm manager is nullptr, it must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}

		manager_ptr->run_simulation_cycle();		
		
		
		
		vpiHandle topmodule_itr = vpi_iterate(vpiModule, nullptr);
		vpiHandle topmodule = vpi_scan(topmodule_itr);
		PLI_INT32 timeunit = vpi_get(vpiTimeUnit, topmodule);
		int error_code = check_error();

		if(!error_code)
		{
			double timestep = static_cast<double>(manager_ptr->get_timestep().current_timestep()) / std::pow(10.0l,static_cast<double>(timeunit));
			
			s_vpi_time callbacktime;
			callbacktime.type = vpiScaledRealTime;
			callbacktime.real = timestep;
			
			s_cb_data cb_data;
			cb_data.reason = cbReadOnlySynch;
			cb_data.time = &callbacktime;
			cb_data.obj = topmodule;
			cb_data.cb_rtn = run_simulation_cycle;
			vpiHandle cb_handle;
			cb_handle = vpi_register_cb(&cb_data);
			vpi_free_object(cb_handle);
		}
		else
		{
			std::cerr << "run_simulation_cycle(): error occured during TimeUnit request" << std::endl;
			vpi_printf(const_cast<char*>("run_simulation_cycle(): error occured during TimeUnit request\n"));
			return vpiError;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$run_simulation_cycle(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$run_simulation_cycle(): unkown error occured\n"));
		return vpiError;
	}
	return 0;
}

/**
 * beolvassa verilogbol a timestep-et es beallitja a run_simulation_cycle callbacket a megfelelo idokozre
**/ 
PLI_INT32 set_timestep(PLI_BYTE8 *user_data)
{	
	try
	{
		int       error_code;
		vpiHandle systfref, args_iter, argh;
		s_vpi_value argval;
		s_vpi_time time;
		argval.format = vpiTimeVal;
		argval.value.time = &time;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();
		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(!error_code)
		{
			/** !!! magic !!! **/
			unsigned long long int value = argval.value.time->high; 
			value = (value << (8*sizeof(argval.value.time->low))) | argval.value.time->low;
			
			
			vpiHandle topmodule_itr = vpi_iterate(vpiModule, nullptr);
			vpiHandle topmodule = vpi_scan(topmodule_itr);
			PLI_INT32 timeunit = vpi_get(vpiTimeUnit, topmodule);
			error_code = check_error();
			
			if(!error_code)
			{
				long double timestep = static_cast<long double>(value) * std::pow(10.0l,static_cast<long double>(timeunit));
			
				logitherm::manager_t::get_manager()->set_timestep(static_cast<::unit::time_t>(timestep));
				
				std::stringstream conv;
				conv << std::scientific << timestep;
				std::string step_str;
				conv >> step_str;
				manager_ptr->debug("$set_timestep(): " + step_str);
				
				s_vpi_time callbacktime;
				callbacktime.type = vpiScaledRealTime;
				callbacktime.real = static_cast<double>(value);
				
				s_cb_data cb_data;
				cb_data.reason = cbReadOnlySynch;
				cb_data.time = &callbacktime;
				cb_data.obj = topmodule; //csereltem nullptr-rol:
				// obj must specify a handle for an object if the type field in the time structure is vpiscaledrealtime.
				// the simulation time scale for the module which contains the object will be used to scale the time value.
				// if the time type is vpisimtime, the obj field is not used and should still be set to null.
				
				cb_data.cb_rtn = run_simulation_cycle;
				vpiHandle cb_handle;
				cb_handle = vpi_register_cb(&cb_data); //NULL reference handle supplied to routine vpi_register_cb()
				vpi_free_object(cb_handle);
			}
			else
			{
				std::cerr << "$set_timestep(): error occured during TimeUnit request" << std::endl;
				vpi_printf(const_cast<char*>("$set_timestep(): error occured during TimeUnit request\n"));
				return vpiError;
			}
		}
		else
		{
			std::cerr << "$set_timestep(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$set_timestep(): invalid argument\n"));
			return vpiError;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$set_timestep(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$set_timestep(): unkown error occured\n"));
		return vpiError;
	}
	
    return 0;
}

/**
 * disszipacios activity_t-hez hasznalt callback fv
*/
PLI_INT32 activity_t_callback(p_cb_data ptr)
{
	
	auto* activity_ptr = reinterpret_cast<::logic::verilog::activity_t*>(ptr->user_data);
	activity_ptr->activity(ptr->time->real);
	logic_engine_ptr->debug("'" + activity_ptr->id +"' VALUE=" + ptr->value->value.str + " CHANGED @ " + std::to_string(ptr->time->real));
	return 0;
}

static void register_cell_ports(vpiHandle mod_handle, logic::verilog::component_t* mod_ptr, file_io::liberty::adapter_t& liberty_parser)
{
	logic_engine_ptr->debug("register_cell_ports enter '" + mod_ptr->id + "'");

	vpiHandle				miter_handle;
	vpiHandle				port_handle;
	int						error_code;
	std::string				port_name="";
	unit::energy_t			energy;
	unit::energy_t			nan(std::numeric_limits<double>::quiet_NaN());
	
	
	miter_handle = vpi_iterate(vpiPort, mod_handle); //vpi_port
	// if(miter_handle != nullptr) logic_engine_ptr->debug("jo kurva anyad");

	error_code = check_error();
	if(!error_code && miter_handle)
	{
		// logic_engine_ptr->debug("if uff");
		port_handle = vpi_scan(miter_handle); /** ha vegigertunk, akkor NULL-t ad vissza **/
		error_code = check_error();
		while(!error_code && port_handle)
		{
			// logic_engine_ptr->debug("before vpi_get_str");
			port_name = vpi_get_str(vpiName, port_handle); //itt szall el ez a kisbuzi
			
			// logic_engine_ptr->debug("after vpi_get_str");
			error_code = check_error();
			
			// logic_engine_ptr->debug("miafaszomvanmar");
			if(!error_code)
			{
				// logic_engine_ptr->debug("register_cell_ports before get_energy");

				/**talaltunk egy portot, keressuk ki liberty-bol a hozza tartozo energiat **/
				energy = static_cast<::unit::energy_t>(liberty_parser.get_energy(mod_ptr->component_type, port_name)); //*3000.0; //TODO
				
				// logic_engine_ptr->debug("register_cell_ports after get_energy");

				/* csak akkor regisztralunk be esemenyt, ha a hozza tartozo disszipacio 0-nal tobb */
				if(energy > 0_J)
				{


					/** kapott fogyasztasra letrehozunk egy uj activity-t, fogyasztas fv egy lambda **/
					logic::verilog::activity_t* activity_ptr = new logic::verilog::activity_t(port_name,[energy]()->unit::energy_t{return energy;});
					
					/**add_activity a mod_ptr-re **/
					mod_ptr->add_activity(activity_ptr);
					
					/** megkeresem azt a net-et aminek ugyanaz a neve, mint a portnak **/
					vpiHandle net_itr, net_handle;
					net_itr = vpi_iterate(vpiNet,mod_handle);
					std::string net_name;
					while (net_handle = vpi_scan(net_itr))
					{
						net_name = vpi_get_str(vpiName, net_handle);
						if(net_name == port_name)
						{
							logic_engine_ptr->debug("net '" + net_name + "' was found");
							break;
						}
					}
					if(nullptr == net_handle) logic_engine_ptr->warning("net '" + port_name + "' in component '" + mod_ptr->id + "' was not found");
					
					/**activity_t activity() fv-et kellene valahogy bejegyezni verilogba **/
					s_vpi_time time;
					time.type = vpiScaledRealTime;
					
					s_cb_data cb_data;
					s_vpi_value returnvalue; //visszateresi ertek tipusat allitjuk be vele
					returnvalue.format = vpiHexStrVal;
					
					cb_data.reason = cbValueChange;
					cb_data.time = &time;
					cb_data.obj	 = net_handle;
					cb_data.user_data = reinterpret_cast<PLI_BYTE8*>(activity_ptr); // jaaaj de szeep... :)
					cb_data.value = &returnvalue;
					cb_data.cb_rtn = activity_t_callback;
					
					vpiHandle cb_handle = vpi_register_cb(&cb_data);
					vpi_free_object(cb_handle);

					logic_engine_ptr->debug("net '" + net_name + "' was registered");
				}
			}
			
			vpi_free_object(port_handle);
			
			port_handle = vpi_scan(miter_handle); /** lepunk a kovetkezo modulra ezen a szinten **/
			error_code = check_error();
		}
	}

	logic_engine_ptr->debug("register_cell_ports finish");
}

static void create_cell_dissipation(vpiHandle parent, file_io::liberty::adapter_t& liberty_parser)
{
	logic::verilog::component_t* component_ptr = nullptr;
	
	int         error_code;
	vpiHandle   miter_handle;
	vpiHandle   mod_handle;
	
	miter_handle = vpi_iterate(vpiModule, parent);
	error_code = check_error();
	
	if(!error_code && miter_handle)
	{
		mod_handle = vpi_scan(miter_handle);
		error_code = check_error();
		
		while(mod_handle)
		{					
			std::string component_full_id = util::remove(util::remove(vpi_get_str(vpiFullName, mod_handle), '\\'), ' ');
			error_code = check_error();
			
			std::string component_id = util::remove(util::remove(vpi_get_str(vpiName, mod_handle), '\\'), ' ');
			error_code = check_error();
			
			std::string component_type = vpi_get_str(vpiDefName, mod_handle);
			error_code = check_error();
			
			if(error_code) logic_engine_ptr->error("create_cell_dissipation(): error occured");

			logic::verilog::component_t* component_ptr = dynamic_cast<logic::verilog::component_t*>(logic_engine_ptr->get_component(component_full_id));
			register_cell_ports(mod_handle, component_ptr, liberty_parser);
			
			/** rekurziv melysegi bejaras **/
			create_cell_dissipation(mod_handle, liberty_parser);

			vpi_free_object(mod_handle);
			mod_handle = vpi_scan(miter_handle); //lepunk a kovetkezo modulra ezen a szinten
			error_code = check_error();
		}
	}
}

/**
 * beolvassa az adott liberty file-t es beallitja az egyezo cellak fogyasztasat
*/
PLI_INT32 read_liberty_file(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == logic_engine_ptr)
		{
			std::cerr << "$read_liberty_file(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$read_liberty_file(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}

		int			error_code;
		vpiHandle	systfref, args_iter, argh;
		s_vpi_value argval;
		argval.format = vpiStringVal;
		
		/** Obtain a handle to the argument list **/
		systfref = vpi_handle(vpiSysTfCall, nullptr);
		args_iter = vpi_iterate(vpiArgument, systfref);

		/** Grab the value of the first argument **/
		argh = vpi_scan(args_iter);
		error_code = check_error();

		vpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$read_liberty_file(): invalid argument" << std::endl;
			vpi_printf(const_cast<char*>("$read_liberty_file(): invalid argument\n"));
			return vpiError;
		}

		std::string file = argval.value.str;
		
		file_io::liberty::adapter_t liberty_parser(logic_engine_ptr->log, file);
		create_cell_dissipation(nullptr, liberty_parser);

	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$read_liberty_file(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$read_liberty_file(): unkown error occured\n"));
		return vpiError;
	}

	return 0;
}

static void create_components(vpiHandle parent)
{
	std::string	parent_full_id;
	std::string	component_full_id;
	std::string	component_id;
	std::string	component_type;
	// component_t* component_ptr = nullptr;
	
	int         error_code;
	vpiHandle   miter_handle;
	vpiHandle   mod_handle;
	
	miter_handle = vpi_iterate(vpiModule, parent);
	error_code = check_error();
	
	if(!error_code && miter_handle)
	{
		mod_handle = vpi_scan(miter_handle);
		error_code = check_error();
		
		while(mod_handle)
		{
			component_full_id = util::remove(util::remove(vpi_get_str(vpiFullName, mod_handle), '\\'), ' ');
			error_code = check_error();
			
			component_id = util::remove(util::remove(vpi_get_str(vpiName, mod_handle), '\\'), ' ');
			error_code = check_error();
			
			component_type = vpi_get_str(vpiDefName, mod_handle);
			error_code = check_error();
			
			//csak ezt kellene berakni adapter-be			
			if(!error_code)
			{
				if(parent != nullptr)
				{

					parent_full_id = util::remove(util::remove(vpi_get_str(vpiFullName, parent), '\\'), ' ');
					error_code = check_error();
												
					logic::verilog::component_t* parent_ptr = dynamic_cast<logic::verilog::component_t*>(logic_engine_ptr->get_component(parent_full_id));
					
					if (nullptr == parent_ptr)
					{
						logic_engine_ptr->error("create_components(): '" + parent_full_id + "' module is not found or invalid type");
					}
					else
					{
						new logic::verilog::component_t(logic_engine_ptr, component_full_id, component_id, component_type, parent_ptr);
					}
				}
				else
				{
					new logic::verilog::component_t(logic_engine_ptr, component_full_id, component_id, component_type, nullptr);
				}
			}
			else
			{
				logic_engine_ptr->error("create_components(): error occured");
			}
			
			/** rekurziv melysegi bejaras **/
			create_components(mod_handle);

			vpi_free_object(mod_handle);

			/** lepunk a kovetkezo modulra ezen a szinten **/
			mod_handle = vpi_scan(miter_handle);
			error_code = check_error();
		}
	}
}

/**
 * get_module_hierarchy()
 * adapter-be bepakolja a teljes modul hierarchiat
**/
PLI_INT32 initialize_logitherm(PLI_BYTE8 *user_data)
{
	try
	{
		if (nullptr == manager_ptr) manager_ptr = logitherm::manager_t::get_manager();
		if(nullptr == logic_engine_ptr) logic_engine_ptr = dynamic_cast<logic::verilog::adapter_t*>(manager_ptr->set_logic_engine(util::logic_engine_t::verilog));

		create_components(nullptr);
		logic_engine_ptr->print_tree(std::cerr);
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$initialize_logitherm(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$initialize_logitherm(): unkown error occured\n"));
		return vpiError;
	}
    return 0;
}

PLI_INT32 start_timer(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$start_timer(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$start_timer(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}
		manager_ptr->start_timer();

	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$start_timer(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$start_timer(): unkown error occured\n"));
		return vpiError;
	}
    return 0;
}

PLI_INT32 get_elapsed_time(PLI_BYTE8 *user_data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$get_elapsed_time(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vpi_printf(const_cast<char*>("$get_elapsed_time(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"));
			return vpiError;
		}
		std::pair<unit::time_t, unit::time_t> elapsed_time = manager_ptr->get_elapsed_time();
		vpi_printf(const_cast<char*>(("Wall time = " + std::to_string(static_cast<double>(elapsed_time.first)) + ", CPU time = " + std::to_string(static_cast<double>(elapsed_time.second)) + "\n" ).c_str()));
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg));
		return vpiError;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vpi_printf(const_cast<char*>(error_msg.c_str()));
		return vpiError;
	}
	catch(...)
	{
		std::cerr << "$get_elapsed_time(): unkown error occured" << std::endl;
		vpi_printf(const_cast<char*>("$get_elapsed_time(): unkown error occured\n"));
		return vpiError;
	}
    return 0;
}

extern "C"
{
	/**
	 * regisztralja a initialize_logitherm fv-t
	**/
	void register_vpi_functions(void)
	{

		s_vpi_systf_data systf_data;
	    vpiHandle        systf_handle;

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$add_dynamic_dissipation_trace");
	    systf_data.calltf      = add_dynamic_dissipation_trace;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$add_static_dissipation_trace");
	    systf_data.calltf      = add_static_dissipation_trace;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$add_activity_trace");
	    systf_data.calltf      = add_activity_trace;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$add_avg_temperature_trace");
	    systf_data.calltf      = add_max_temperature_trace;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$add_max_temperature_trace");
	    systf_data.calltf      = add_max_temperature_trace;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$add_min_temperature_trace");
	    systf_data.calltf      = add_max_temperature_trace;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

		s_cb_data cb_data;
		vpiHandle cb_handle;
		cb_data.reason = cbEndOfReset;
		cb_data.cb_rtn = restart_simulation;
		cb_handle = vpi_register_cb(&cb_data);
		vpi_free_object(cb_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$set_thermal_engine");
	    systf_data.calltf      = set_thermal_engine;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$print_dissipation_map");
	    systf_data.calltf      = print_dissipation_map;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$print_temperature_map");
	    systf_data.calltf      = print_temperature_map;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$print_temperature_map_in_svg");
	    systf_data.calltf      = print_temperature_map_in_svg;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$add_display_component");
	    systf_data.calltf      = add_display_component;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$set_log_file_path");
	    systf_data.calltf      = set_log_file_path;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$set_notification_level");
	    systf_data.calltf      = set_notification_level;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$set_timestep");
	    systf_data.calltf      = set_timestep;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$read_liberty_file");
	    systf_data.calltf      = read_liberty_file;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$initialize_logitherm");
	    systf_data.calltf      = initialize_logitherm;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$start_timer");
	    systf_data.calltf      = start_timer;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);

	    systf_data.type        = vpiSysTask;
	    systf_data.sysfunctype = 0;
	    systf_data.tfname      = const_cast<char*>("$get_elapsed_time");
	    systf_data.calltf      = get_elapsed_time;
	    systf_data.compiletf   = 0;
	    systf_data.sizetf      = 0;
	    systf_data.user_data   = 0;
	    systf_handle = vpi_register_systf( &systf_data );
	    vpi_free_object(systf_handle);
	}

	/*****************************************************************************
	 *
	 * Required structure for initializing VPI routines.
	 *
	 *****************************************************************************/

	void (*vlog_startup_routines[])() = {
	    register_vpi_functions,
	    0
	};

}

namespace logic
{
	namespace verilog
	{
		adapter_t::adapter_t(logitherm::manager_t* manager, util::log_t* log)
		:
			logic::adapter_t(manager, log, "logic::verilog::adapter_t", util::logic_engine_t::verilog, manager->get_timestep())
		{}
		
		adapter_t::~adapter_t()
		{
			debug("logic::verilog::adapter_t::~adapter_t()");
			delete_components_tree();
		}				
	}
}
