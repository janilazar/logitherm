#ifndef _VERILOG_ACTIVITY_TRACE_H_
#define _VERILOG_ACTIVITY_TRACE_H_

#include "logic/base/activity_trace_t.hpp"
#include <vpi_user.h>

namespace logic
{
	namespace verilog
	{
		
		class activity_trace_t	:	public logic::activity_trace_t
		{
			private:
				double time;
			
			public:
				activity_trace_t(const std::string& path, const std::string& id, const std::string& postfix, const std::string& filename);
				
				void increment(double current_time);
				
		};
	}
}
#endif //_VERILOG_ACTIVITY_TRACE_H_
