#include "logic/verilog/adapter/component_t.hpp"
#include "logic/verilog/adapter/adapter_t.hpp"
#include "logic/verilog/adapter/activity_trace_t.hpp"
#include "manager/manager_t.hpp"

namespace logic
{
	namespace verilog
	{
		component_t::component_t(logic::adapter_t* adapter, const std::string& full_id, const std::string& short_id, const std::string& component_type, ::logic::verilog::component_t* parent)
		:
			logic::component_t(adapter, full_id, parent),
			short_id(short_id),
			component_type(component_type)
		{}
		
		std::ostream& component_t::print_component(std::ostream& os, std::string indent)
		{
			os << indent << id << " " << short_id << " " << component_type << std::endl;
			if(!children.empty())
			{
				indent += "  ";
				for(auto &it: children)
				{
					it.second->print_component(os, indent);
				}
			}
			return os;
		}
		
		activity_trace_t* component_t::add_activity_trace(const std::string& path, const std::string& activity_id)
		{
			activity_trace_t* ptr = new activity_trace_t(path, activity_id, "", this->id + "_activity.trace");
			tracer.add_trace(ptr);
			trace_component();
			return ptr;
		}
	}
}
