#include "logic/verilog/adapter/activity_trace_t.hpp"


namespace logic
{
	namespace verilog
	{
		activity_trace_t::activity_trace_t(const std::string& path, const std::string& id, const std::string& postfix, const std::string& filename)
		:
			logic::activity_trace_t(path, id, postfix, filename),
			time(0.0)
		{}
		
		void activity_trace_t::increment(double current_time)
		{
			if(current_time > time)
			{
				logic::activity_trace_t::increment();
				time = current_time;
			}
		}
	}
}
