#include "logic/vhdl/adapter/adapter_t.hpp"
#include "logic/vhdl/adapter/component_t.hpp"
#include "logic/vhdl/adapter/activity_t.hpp"
#include "manager/manager_t.hpp"
#include "util/log_t.hpp"
#include "util/string_manipulation.hpp"
#include <vhpi_user.h>

#include "logic/vhdl/adapter/activity_trace_t.hpp"

#include <limits>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>

static logitherm::manager_t* manager_ptr = nullptr;
static thermal::adapter_t* thermal_engine_ptr = nullptr;
static util::thermal_engine_t thermal_engine_type;
static logic::vhdl::adapter_t* logic_engine_ptr = nullptr;


static int check_error(void)
{
	int            error_code = 0;
	vhpiErrorInfoT error_info;

	error_code = vhpi_check_error(&error_info);
	if (error_code && error_info.message)
	{
		vhpi_printf("  %s\n", error_info.message); // ki irta meg a vhpi_printf-et ugy, hogy nem const format-ot var? 21. szazad
	}

	return error_code;
}

void activity_trace_t_callback(const vhpiCbDataT* ptr)
{
	auto* activity_trace_ptr = reinterpret_cast<logic::vhdl::activity_trace_t*>(ptr->user_data);
	unsigned long long int current_time = 0ull;
	current_time = static_cast<unsigned long long int>(ptr->time->high) << (8*sizeof(ptr->time->low)) | ptr->time->low;
	activity_trace_ptr->increment(current_time);
}

// VHPI user guide 6.1.3-as pont (109. oldal)
void add_min_temperature_trace(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$add_min_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$add_min_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}

		layout::adapter_t* layout_ptr = thermal_engine_ptr->get_layout();
		
		if(nullptr == layout_ptr)
		{
			std::cerr << "$add_min_temperature_trace(): layout is nullptr, layout must be initialized first" << std::endl;
			vhpi_assert(const_cast<char*>("$add_min_temperature_trace(): layout is nullptr, layout must be initialized first'\n"), vhpiError);
			return;
		}

		
		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;

		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = util::remove(util::remove(argval.value.str, '\\'), ' ');
		
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			
			if(layout_ptr->search_for_component(comp_id))
			{
				layout_ptr->get_component(comp_id)->add_min_temperature_trace(path);
			}
			else
			{
				layout_ptr->warning("$add_min_temperature_trace(): layout::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_min_temperature_trace(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$add_min_temperature_trace(): invalid argument\n"), vhpiError);
			return;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$add_min_temperature_trace(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$add_min_temperature_trace(): unkown error occured\n"), vhpiError);
		return;
	}
	return;
}

void add_max_temperature_trace(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$add_max_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$add_max_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}

		layout::adapter_t* layout_ptr = thermal_engine_ptr->get_layout();
		
		if(nullptr == layout_ptr)
		{
			std::cerr << "$add_max_temperature_trace(): layout is nullptr, layout must be initialized first" << std::endl;
			vhpi_assert(const_cast<char*>("$add_max_temperature_trace(): layout is nullptr, layout must be initialized first'\n"), vhpiError);
			return;
		}

		
		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = util::remove(util::remove(argval.value.str, '\\'), ' ');
		
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			
			if(layout_ptr->search_for_component(comp_id))
			{
				layout_ptr->get_component(comp_id)->add_max_temperature_trace(path);
			}
			else
			{
				layout_ptr->warning("$add_max_temperature_trace(): layout::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_max_temperature_trace(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$add_max_temperature_trace(): invalid argument\n"), vhpiError);
			return;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$add_max_temperature_trace(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$add_max_temperature_trace(): unkown error occured\n"), vhpiError);
		return;
	}
}

void add_avg_temperature_trace(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$add_avg_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$add_min_temperature_trace(): thermal engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}

		layout::adapter_t* layout_ptr = thermal_engine_ptr->get_layout();
		
		if(nullptr == layout_ptr)
		{
			std::cerr << "$add_avg_temperature_trace(): layout is nullptr, layout must be initialized first" << std::endl;
			vhpi_assert(const_cast<char*>("$add_avg_temperature_trace(): layout is nullptr, layout must be initialized first'\n"), vhpiError);
			return;
		}

		
		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = util::remove(util::remove(argval.value.str, '\\'), ' ');
		
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			if(layout_ptr->search_for_component(comp_id))
			{
				layout_ptr->get_component(comp_id)->add_avg_temperature_trace(path);
			}
			else
			{
				layout_ptr->warning("$add_avg_temperature_trace(): layout::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_avg_temperature_trace(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$add_avg_temperature_trace(): invalid argument\n"), vhpiError);
			return;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$add_avg_temperature_trace(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$add_avg_temperature_trace(): unkown error occured\n"), vhpiError);
		return;
	}
}

void add_activity_trace(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == logic_engine_ptr)
		{
			std::cerr << "$add_activity_trace(): layout is nullptr, layout must be initialized first" << std::endl;
			vhpi_assert(const_cast<char*>("$add_activity_trace(): layout is nullptr, layout must be initialized first'\n"), vhpiError);
		}

		
		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = util::remove(util::remove(argval.value.str, '\\'), ' ');
		
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string net_id = argval.value.str;
		
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			if(logic_engine_ptr->search_for_component(comp_id))
			{
				vhpiHandleT mod_handle = vhpi_handle_by_name(comp_id.c_str(), nullptr);
				if(mod_handle == nullptr) logic_engine_ptr->error("module handle of component '" + comp_id + "' was not found");
				
				/** megkeresem az adott nevu netet **/
				vhpiHandleT net_itr, net_handle;
				net_itr = vhpi_iterator(vhpiSignals,mod_handle); //vpiNet
				std::string net_name;
				
				std::string mod_name = util::remove(vhpi_get_str(vhpiFullNameP, mod_handle), '\\');
				logic_engine_ptr->debug("$add_activity_trace(): SEARCHING net: '" + net_id + "' in module '" + mod_name + "'");
				while (net_handle = vhpi_scan(net_itr))
				{
					net_name = vhpi_get_str(vhpiNameP, net_handle);
					if(net_name == net_id)
					{
						/** hozzunk letre egy uj activity_trace_t es adjuk hozza **/
						auto* comp_ptr = dynamic_cast<logic::vhdl::component_t*>(logic_engine_ptr->get_component(comp_id));
						if(nullptr == comp_ptr) logic_engine_ptr->error("component '" + comp_id + "' has invalid type");
						
						logic::vhdl::activity_trace_t* activity_trace_ptr = comp_ptr->add_activity_trace(path, net_id);
						
						/**activity_t activity() fv-et kellene valahogy bejegyezni vhdlba **/
						vhpiTimeT time;
						int flags = 0; /* do not return a callback handle and do not disable the callback at registration */
						vhpiCbDataT cb_data;
						vhpiValueT returnvalue; //visszateresi ertek tipusat allitjuk be vele
						returnvalue.format = vhpiHexStrVal;
						
						cb_data.reason = vhpiCbValueChange;
						cb_data.time = &time;
						cb_data.obj	 = net_handle;
						cb_data.user_data = reinterpret_cast<PLI_VOID*>(activity_trace_ptr); // jaaaj de szeep... :)
						cb_data.value = &returnvalue;
						cb_data.cb_rtn = activity_trace_t_callback;
						
						vhpiHandleT cb_handle = vhpi_register_cb(&cb_data, flags);
						vhpi_release_handle(cb_handle);
						
						logic_engine_ptr->debug("$add_activity_trace(): FOUND net: " + net_id);
						break;
					}
				}
				if(nullptr == net_handle) logic_engine_ptr->warning("$add_activity_trace(): NOT FOUND net: '" + net_id + "' in module '" + mod_name + "'");
				
			}
			else
			{
				logic_engine_ptr->warning("$add_activity_trace(): logic::vhdl::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_activity_trace(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$add_activity_trace(): invalid argument\n"), vhpiError);
			return;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "VPI add_activity_trace(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("VPI add_activity_trace(): unkown error occured\n"), vhpiError);
		return;
	}
}

void add_dynamic_dissipation_trace(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == logic_engine_ptr)
		{
			std::cerr << "$add_dynamic_dissipation_trace(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$add_dynamic_dissipation_trace(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}
		
		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = util::remove(util::remove(argval.value.str, '\\'), ' ');
		
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			
			if(logic_engine_ptr->search_for_component(comp_id))
			{
				logic_engine_ptr->get_component(comp_id)->add_dynamic_dissipation_trace(path);
			}
			else
			{
				logic_engine_ptr->warning("$add_dynamic_dissipation_trace(): logic::vhdl::component_t '" + comp_id + "' was not found");
			}
		}
		else
		{
			std::cerr << "$add_dynamic_dissipation_trace(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$add_dynamic_dissipation_trace(): invalid argument\n"), vhpiError);
			return;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$add_dynamic_dissipation_trace(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$add_dynamic_dissipation_trace(): unkown error occured\n"), vhpiError);
		return;
	}
}

void add_static_dissipation_trace(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == logic_engine_ptr)
		{
			std::cerr << "$add_static_dissipation_trace(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$add_static_dissipation_trace(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}
		
		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string comp_id = util::remove(util::remove(argval.value.str, '\\'), ' ');
		
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		std::string path = argval.value.str;
		
		if(!error_code)
		{
			if(logic_engine_ptr->search_for_component(comp_id))
			{
				logic_engine_ptr->get_component(comp_id)->add_static_dissipation_trace(path);
			}
			else
			{
				logic_engine_ptr->warning("$add_static_dissipation_trace(): logic::vhdl::component_t '" + comp_id + "' was not found");
				return;
			}
		}
		else
		{
			std::cerr << "$add_static_dissipation_trace(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$add_static_dissipation_trace(): invalid argument\n"), vhpiError);
			return;
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$add_static_dissipation_trace(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$add_static_dissipation_trace(): unkown error occured"), vhpiError);
		return;
	}
}

/**
 * callback-kent van bejegyezve
 * meghivja a manager_t destruktorat
**/ 
void restart_simulation(const vhpiCbDataT* data)
{
	try
	{
		if(manager_ptr != nullptr)
		{
			manager_ptr->~manager_t();
			manager_ptr = nullptr;
			logic_engine_ptr = nullptr;
			thermal_engine_ptr = nullptr;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$restart_simulation(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$restart_simulation(): unkown error occured\n"), vhpiError);
		return;
	}
}

/**
 * beolvassa vhdlbol a termikus motor tipusat, inicializalo fajl utvonalat es nevet
**/ 

void set_thermal_engine(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$set_thermal_engine(): LogiTherm manager is nullptr, it must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$set_thermal_engine(): LogiTherm manager is nullptr, it must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}

		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();

		if(error_code)
		{
			std::cerr << "$set_thermal_engine(): invalid first argument" << std::endl;
			vhpi_assert(const_cast<char*>("$set_thermal_engine(): invalid first argument\n"), vhpiError);
			return;
		}
		
		std::string thermal_engine_str = argval.value.str;
		
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$set_thermal_engine(): invalid second argument" << std::endl;
			vhpi_assert(const_cast<char*>("$set_thermal_engine(): invalid second argument\n"), vhpiError);
			return;
		}
		
		std::string path = argval.value.str;
		
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$set_thermal_engine(): invalid third argument" << std::endl;
			vhpi_assert(const_cast<char*>("$set_thermal_engine(): invalid third argument\n"), vhpiError);
			return;
		}

		std::string file = argval.value.str;

		if(thermal_engine_str == "sunred")
		{
			/* initializing the thermal engine to thermal::sunred::adapter_t */
			thermal_engine_type = util::thermal_engine_t::sunred;
			thermal_engine_ptr = manager_ptr->set_thermal_engine(thermal_engine_type);
		}
		else if(thermal_engine_str == "sloth")
		{
			/* initializing the thermal engine to thermal::sunred::adapter_t */
			thermal_engine_type = util::thermal_engine_t::sloth;
			thermal_engine_ptr = manager_ptr->set_thermal_engine(thermal_engine_type);
		}
		else if(thermal_engine_str == "3d-ice")
		{
			/* initializing the thermal engine to thermal::sunred::adapter_t */
			thermal_engine_type = util::thermal_engine_t::threedice;
			thermal_engine_ptr = manager_ptr->set_thermal_engine(thermal_engine_type);
		}
		else
		{
			std::cerr << "$set_thermal_engine(): thermal engine type can not be recognized" << std::endl;
			vhpi_assert(const_cast<char*>("set_thermal_engine(): thermal engine type can not be recognized\n"), vhpiError);
			return;
		}
		/** termikus motor inicializalasa fajlbol **/
		thermal_engine_ptr->read_files(path, file);
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$set_thermal_engine(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>(vhpiError,"set_thermal_engine(): unkown error occured\n"), vhpiError);
		return;
	}
	return;
}

/**
 * beolvassa az adott liberty file-t es beallitja az egyezo cellak fogyasztasat
*/
void print_dissipation_map(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$print_dissipation_map(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'" << std::endl;
			vhpi_assert(const_cast<char*>("$print_dissipation_map(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'\n"), vhpiError);
			return;
		}

		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$print_temperature_map(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$print_temperature_map(): invalid argument\n"), vhpiError);
			return;
		}

		std::string file = argval.value.str;
		
		std::ofstream of(file);
		if(of.is_open())
		{
			thermal_engine_ptr->print_dissipation_map(of);
			of.close();
		}
		else
		{
			std::cerr << "$print_dissipation_map(): could not open '" << file << "'" << std::endl;
			vhpi_assert(const_cast<char*>(("$print_dissipation_map(): could not open '" + file + "'\n").c_str()), vhpiError);
			return;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$print_dissipation_map(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$print_dissipation_map(): unkown error occured\n"), vhpiError);
		return;
	}
	return;
}

/**
 * beolvassa az adott liberty file-t es beallitja az egyezo cellak fogyasztasat
*/
void print_temperature_map(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$print_temperature_map(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'" << std::endl;
			vhpi_assert(const_cast<char*>(vhpiError,"$print_temperature_map(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'\n"), vhpiError);
			return;
		}

		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$print_temperature_map(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$print_temperature_map(): invalid argument\n"), vhpiError);
			return;
		}

		std::string file = argval.value.str;
		
		std::ofstream of(file);
		if(of.is_open())
		{
			thermal_engine_ptr->print_temperature_map(of);
			of.close();
		}
		else
		{
			std::cerr << "$print_temperature_map(): could not open '" << file << "'" << std::endl;
			vhpi_assert(const_cast<char*>(vhpiError,("$print_temperature_map(): could not open '" + file + "'\n").c_str()), vhpiError);
			return;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$print_temperature_map(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$print_temperature_map(): unkown error occured\n"), vhpiError);
		return;
	}
	return;
}


/**
 * beolvassa az adott liberty file-t es beallitja az egyezo cellak fogyasztasat
*/
void print_temperature_map_in_svg(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$print_temperature_map_in_svg(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$print_temperature_map_in_svg(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}

		if(nullptr == thermal_engine_ptr)
		{
			std::cerr << "$print_temperature_map_in_svg(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'" << std::endl;
			vhpi_assert(const_cast<char*>("$print_temperature_map_in_svg(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'\n"), vhpiError);
			return;
		}

		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$print_temperature_map_in_svg(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$print_temperature_map_in_svg(): invalid argument\n"), vhpiError);
			return;
		}

		std::string file = argval.value.str;
		
		std::ofstream of(file);
		if(of.is_open())
		{
			manager_ptr->print_temperature_map_in_svg(of);
		}
		else
		{
			std::cerr << "$print_temperature_map_in_svg(): could not open '" << file << "'" << std::endl;
			vhpi_assert(const_cast<char*>(("$print_temperature_map_in_svg(): could not open '" + file + "'\n").c_str()), vhpiError);
			return;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$print_temperature_map_in_svg(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$print_temperature_map_in_svg(): unkown error occured\n"), vhpiError);
		return;
	}
	return;
}

/**
 * svg-ben megjelenitendo component
*/
void add_display_component(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$add_display_component(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'" << std::endl;
			vhpi_assert(const_cast<char*>("$add_display_component(): thermal engine is nullptr, it must be initialized first with '$set_thermal_engine()'\n"), vhpiError);
			return;
		}

		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$add_display_component(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$add_display_component(): invalid argument\n"), vhpiError);
			return;
		}

		std::string component_id = argval.value.str;
		
		manager_ptr->add_display_component(component_id);
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$add_display_component(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$add_display_component(): unkown error occured\n"), vhpiError);
		return;
	}
	return;
}

/**
 * beallitja a log file-ok utvonalat
*/
void set_log_file_path(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$set_log_file_path(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$set_log_file_path(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}

		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$set_log_file_path(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$set_log_file_path(): invalid argument\n"), vhpiError);
			return;
		}

		std::string path = argval.value.str;

		manager_ptr->set_log_file_path(path);
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$set_log_file_path(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$set_log_file_path(): unkown error occured\n"), vhpiError);
		return;
	}
	return;
}

/**
 * beolvassa az adott liberty file-t es beallitja az egyezo cellak fogyasztasat
*/
void set_notification_level(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$set_notification_level(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$set_notification_level(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}

		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$set_notification_level(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$set_notification_level(): invalid argument\n"), vhpiError);
			return;
		}

		std::string level_str = argval.value.str;

		if(level_str == "debug") manager_ptr->set_notification_level(util::level_t::debug);
		else if(level_str == "warning") manager_ptr->set_notification_level(util::level_t::warning);
		else if(level_str == "error") manager_ptr->set_notification_level(util::level_t::error);
		else
		{
			std::cerr << "$set_notification_level(): invalid argument '" << level_str << "'" << std::endl;
			vhpi_assert(const_cast<char*>(("$set_notification_level(): invalid argument '" + level_str + "'").c_str()), vhpiError);
			return;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
	}
	catch(...)
	{
		std::cerr << "$set_notification_level(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$set_notification_level(): unkown error occured\n"), vhpiError);
	}
	return;
}

/**
 * callback-kent van bejegyezve
 * meghivja a logic adapter run_simulation_cycle fv-et
**/ 
void run_simulation_cycle(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$run_simulation_cycle(): LogiTherm manager is nullptr, it must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$run_simulation_cycle(): LogiTherm manager is nullptr, it must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}

		manager_ptr->run_simulation_cycle();
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$run_simulation_cycle(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$run_simulation_cycle(): unkown error occured\n"), vhpiError);
		return;
	}
	return;
}

/**
 * beolvassa vhdlbol a timestep-et es beallitja a run_simulation_cycle callbacket a megfelelo idokozre
**/ 
void set_timestep(const vhpiCbDataT* data)
{	
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$run_simulation_cycle(): LogiTherm manager is nullptr, it must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$run_simulation_cycle(): LogiTherm manager is nullptr, it must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}

		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;		
		argval.format = vhpiTimeVal;

		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		vhpiTimeT callbacktime;
		callbacktime = argval.value.time;

		if(!error_code)
		{
			/** !!! magic !!! **/
			unsigned long long int value = callbacktime.high; 
			value = (value << (8*sizeof(callbacktime.low))) | callbacktime.low;
			
			vhpiPhysT timeunit = vhpi_get_phys(vhpiSimTimeUnitP, nullptr);
			error_code = check_error();
			
			if(!error_code)
			{

				long long int unit = timeunit.high; 
				unit = (unit << (8*sizeof(timeunit.low))) | timeunit.low;

				long double timestep = static_cast<long double>(value) * std::pow(10.0l,static_cast<long double>(unit));
			
				manager_ptr->set_timestep(static_cast<unit::time_t>(timestep));
				
				std::stringstream conv;
				conv << std::scientific << timestep;
				std::string step_str;
				conv >> step_str;
				manager_ptr->debug("$set_timestep(): " + step_str);
				
				vhpiCbDataT cb_data;
				cb_data.reason = vhpiCbRepEndOfTimeStep;
				cb_data.time = &callbacktime;
				cb_data.obj = nullptr;

				cb_data.cb_rtn = run_simulation_cycle;
				vhpiHandleT cb_handle;

				int flags = 0;
				cb_handle = vhpi_register_cb(&cb_data, flags);
				vhpi_release_handle(cb_handle);
			}
			else
			{
				std::cerr << "$set_timestep(): error occured during vhpiSimTimeUnitP request" << std::endl;
				vhpi_assert(const_cast<char*>("$set_timestep(): error occured during vhpiSimTimeUnitP request\n"), vhpiError);
				return;
			}
		}
		else
		{
			std::cerr << "$set_timestep(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$set_timestep(): invalid argument\n"), vhpiError);
			return;
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$set_timestep(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$set_timestep(): unkown error occured\n"), vhpiError);
		return;
	}
    return;
}

/**
 * disszipacios activity_t-hez hasznalt callback fv
*/
void activity_t_callback(const vhpiCbDataT* data)
{
	auto* activity_ptr = reinterpret_cast<logic::vhdl::activity_t*>(data->user_data);
	unsigned long long int current_time = 0ull;
	current_time = static_cast<unsigned long long int>(data->time->high) << (8*sizeof(data->time->low)) | data->time->low;
	activity_ptr->activity(current_time);
	return;
}

static void register_cell_ports(vhpiHandleT mod_handle, logic::vhdl::component_t* mod_ptr, file_io::liberty::adapter_t& liberty_parser)
{
	logic_engine_ptr->debug("register_cell_ports enter '" + mod_ptr->id + "'");

	vhpiHandleT				miter_handle;
	vhpiHandleT				port_handle;
	int						error_code;
	std::string				port_name="";
	unit::energy_t			energy;
	unit::energy_t			nan(std::numeric_limits<double>::quiet_NaN());
	
	
	miter_handle = vhpi_iterator(vhpiPortDecls, mod_handle); //vhpi_port
	// if(miter_handle != nullptr) logic_engine_ptr->debug("jo kurva anyad");

	error_code = check_error();
	if(!error_code && miter_handle)
	{
		// logic_engine_ptr->debug("if uff");
		port_handle = vhpi_scan(miter_handle); /** ha vegigertunk, akkor NULL-t ad vissza **/
		error_code = check_error();
		while(!error_code && port_handle)
		{
			// logic_engine_ptr->debug("before vhpi_get_str");
			port_name = vhpi_get_str(vhpiNameP, port_handle); //itt szall el ez a kisbuzi
			
			// logic_engine_ptr->debug("after vhpi_get_str");
			error_code = check_error();
			
			// logic_engine_ptr->debug("miafaszomvanmar");
			if(!error_code)
			{
				// logic_engine_ptr->debug("register_cell_ports before get_energy");

				/**talaltunk egy portot, keressuk ki liberty-bol a hozza tartozo energiat **/
				energy = static_cast<unit::energy_t>(liberty_parser.get_energy(mod_ptr->component_type, port_name)); //*3000.0; //TODO
				
				// logic_engine_ptr->debug("register_cell_ports after get_energy");

				/* csak akkor regisztralunk be esemenyt, ha a hozza tartozo disszipacio 0-nal tobb */
				if(energy > 0_J)
				{
					/** kapott fogyasztasra letrehozunk egy uj activity-t, fogyasztas fv egy lambda **/
					logic::vhdl::activity_t* activity_ptr = new logic::vhdl::activity_t(port_name,[energy]()->unit::energy_t{return energy;});
					
					/**add_activity a mod_ptr-re **/
					mod_ptr->add_activity(activity_ptr);
					
					/** megkeresem azt a net-et aminek ugyanaz a neve, mint a portnak **/
					// vhpiHandleT net_itr, net_handle;
					// net_itr = vhpi_iterator(vhpiNet,mod_handle);
					// std::string net_name;
					// while (net_handle = vhpi_scan(net_itr))
					// {
					// 	net_name = vhpi_get_str(vhpiNameP, net_handle);
					// 	if(net_name == port_name)
					// 	{
					// 		logic_engine_ptr->debug("net '" + net_name + "' was found");
					// 		break;
					// 	}
					// }
					// if(nullptr == net_handle) logic_engine_ptr->warning("net '" + port_name + "' in component '" + mod_ptr->id + "' was not found");
					
					/**activity_t activity() fv-et kellene valahogy bejegyezni vhdlba **/
					vhpiTimeT time;
					// time.type = vhpiScaledRealTime;
					
					vhpiCbDataT cb_data;
					vhpiValueT returnvalue; //visszateresi ertek tipusat allitjuk be vele
					returnvalue.format = vhpiHexStrVal;
					
					cb_data.reason = vhpiCbValueChange;
					cb_data.time = &time;
					cb_data.obj	 = port_handle; // net_handle;
					cb_data.user_data = reinterpret_cast<PLI_VOID*>(activity_ptr); // jaaaj de szeep... :)
					cb_data.value = &returnvalue;
					cb_data.cb_rtn = activity_t_callback;
					
					int flags = 0;
					vhpiHandleT cb_handle = vhpi_register_cb(&cb_data, flags);
					vhpi_release_handle(cb_handle);

					logic_engine_ptr->debug("port '" + port_name + "' was registered");
				}
			}
			
			vhpi_release_handle(port_handle);
			
			port_handle = vhpi_scan(miter_handle); /** lepunk a kovetkezo modulra ezen a szinten **/
			error_code = check_error();
		}
	}

	logic_engine_ptr->debug("register_cell_ports finish");
}

static void create_cell_dissipation(vhpiHandleT parent, file_io::liberty::adapter_t& liberty_parser)
{
	logic::vhdl::component_t* component_ptr = nullptr;
	
	int         error_code;
	vhpiHandleT   miter_handle;
	vhpiHandleT   mod_handle;
	
	miter_handle = vhpi_iterator(vhpiDesignUnits, parent);
	error_code = check_error();
	
	if(!error_code && miter_handle)
	{
		mod_handle = vhpi_scan(miter_handle);
		error_code = check_error();
		
		while(mod_handle)
		{					
			std::string component_full_id = util::remove(util::remove(vhpi_get_str(vhpiFullNameP, mod_handle), '\\'), ' ');
			error_code = check_error();
			
			std::string component_id = util::remove(util::remove(vhpi_get_str(vhpiNameP, mod_handle), '\\'), ' ');
			error_code = check_error();
			
			std::string component_type = vhpi_get_str(vhpiDefNameP, mod_handle);
			error_code = check_error();
			
			if(error_code) logic_engine_ptr->error("create_cell_dissipation(): error occured");

			auto* component_ptr = dynamic_cast<logic::vhdl::component_t*>(logic_engine_ptr->get_component(component_full_id));
			register_cell_ports(mod_handle, component_ptr, liberty_parser);
			
			/** rekurziv melysegi bejaras **/
			create_cell_dissipation(mod_handle, liberty_parser);

			vhpi_release_handle(mod_handle);
			mod_handle = vhpi_scan(miter_handle); //lepunk a kovetkezo modulra ezen a szinten
			error_code = check_error();
		}
	}
}

/**
 * beolvassa az adott liberty file-t es beallitja az egyezo cellak fogyasztasat
*/
void read_liberty_file(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == logic_engine_ptr)
		{
			std::cerr << "$read_liberty_file(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$read_liberty_file(): logic engine is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}

		int			error_code;
		vhpiHandleT	systfref = data->obj, args_iter, argh;

		vhpiValueT argval;
		argval.format = vhpiStrVal;
		
		/** Obtain a handle to the argument list **/
		args_iter = vhpi_iterator(vhpiParamDecls, systfref);

		/** Grab the value of the first argument **/
		argh = vhpi_scan(args_iter);
		error_code = check_error();
		
		vhpi_get_value(argh, &argval);
		error_code = check_error();
		
		if(error_code)
		{
			std::cerr << "$read_liberty_file(): invalid argument" << std::endl;
			vhpi_assert(const_cast<char*>("$read_liberty_file(): invalid argument\n"), vhpiError);
			return;
		}

		std::string file = argval.value.str;
		
		file_io::liberty::adapter_t liberty_parser(logic_engine_ptr->log, file);
		create_cell_dissipation(nullptr, liberty_parser);

	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$read_liberty_file(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$read_liberty_file(): unkown error occured\n"), vhpiError);
		return;
	}
	return;
}

static void create_components(vhpiHandleT parent)
{
	std::string	parent_full_id;
	std::string	component_full_id;
	std::string	component_id;
	std::string	component_type;
	// component_t* component_ptr = nullptr;
	
	int         error_code;
	vhpiHandleT   miter_handle;
	vhpiHandleT   mod_handle;
	
	miter_handle = vhpi_iterator(vhpiDesignUnits, parent);
	error_code = check_error();
	
	if(!error_code && miter_handle)
	{
		mod_handle = vhpi_scan(miter_handle);
		error_code = check_error();
		
		while(mod_handle)
		{
			component_full_id = util::remove(util::remove(vhpi_get_str(vhpiFullNameP, mod_handle), '\\'), ' ');
			error_code = check_error();
			
			component_id = util::remove(util::remove(vhpi_get_str(vhpiNameP, mod_handle), '\\'), ' ');
			error_code = check_error();
			
			component_type = vhpi_get_str(vhpiDefNameP, mod_handle);
			error_code = check_error();
			
			//csak ezt kellene berakni adapter-be			
			if(!error_code)
			{
				if(parent != nullptr)
				{

					parent_full_id = util::remove(util::remove(vhpi_get_str(vhpiFullNameP, parent), '\\'), ' ');
					error_code = check_error();
												
					logic::vhdl::component_t* parent_ptr = dynamic_cast<logic::vhdl::component_t*>(logic_engine_ptr->get_component(parent_full_id));
					
					if (nullptr == parent_ptr)
					{
						logic_engine_ptr->error("create_components(): '" + parent_full_id + "' module is not found or invalid type");
					}
					else
					{
						new logic::vhdl::component_t(logic_engine_ptr, component_full_id, component_id, component_type, parent_ptr);
					}
				}
				else
				{
					new logic::vhdl::component_t(logic_engine_ptr, component_full_id, component_id, component_type, nullptr);
				}
			}
			else
			{
				logic_engine_ptr->error("create_components(): error occured");
			}
			
			/** rekurziv melysegi bejaras **/
			create_components(mod_handle);

			vhpi_release_handle(mod_handle);

			/** lepunk a kovetkezo modulra ezen a szinten **/
			mod_handle = vhpi_scan(miter_handle);
			error_code = check_error();
		}
	}
}

/**
 * get_module_hierarchy()
 * adapter-be bepakolja a teljes modul hierarchiat
**/
void initialize_logitherm(const vhpiCbDataT* data)
{
	try
	{
		if (nullptr == manager_ptr) manager_ptr = logitherm::manager_t::get_manager();
		if(nullptr == logic_engine_ptr) logic_engine_ptr = dynamic_cast<logic::vhdl::adapter_t*>(manager_ptr->set_logic_engine(util::logic_engine_t::vhdl));

		create_components(nullptr);
		logic_engine_ptr->print_tree(std::cerr);
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$initialize_logitherm(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$initialize_logitherm(): unkown error occured\n"), vhpiError);
		return;
	}
    return;
}

void start_timer(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$start_timer(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$start_timer(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}
		manager_ptr->start_timer();

	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$start_timer(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$start_timer(): unkown error occured\n"), vhpiError);
		return;
	}
    return;
}

void get_elapsed_time(const vhpiCbDataT* data)
{
	try
	{
		if(nullptr == manager_ptr)
		{
			std::cerr << "$get_elapsed_time(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'" << std::endl;
			vhpi_assert(const_cast<char*>("$get_elapsed_time(): LogiTherm manager is nullptr, LogiTherm must be initialized first with '$initialize_logitherm()'\n"), vhpiError);
			return;
		}
		std::pair<unit::time_t, unit::time_t> elapsed_time = manager_ptr->get_elapsed_time();
		vhpi_printf(("Wall time = " + std::to_string(static_cast<double>(elapsed_time.first)) + ", CPU time = " + std::to_string(static_cast<double>(elapsed_time.second)) + "\n" ).c_str());
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg), vhpiError);
		return;
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		vhpi_assert(const_cast<char*>(error_msg.c_str()), vhpiError);
		return;
	}
	catch(...)
	{
		std::cerr << "$get_elapsed_time(): unkown error occured" << std::endl;
		vhpi_assert(const_cast<char*>("$get_elapsed_time(): unkown error occured\n"), vhpiError);
		return;
	}
    return;
}

extern "C"
{
	/**
	 * regisztralja a initialize_logitherm fv-t
	**/
	void register_vhpi_functions(void)
	{
		vhpiForeignDataT	foreign_data;
	    vhpiHandleT			foreignf_handle;

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$add_dynamic_dissipation_trace");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = add_dynamic_dissipation_trace;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$add_static_dissipation_trace");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = add_static_dissipation_trace;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$add_activity_trace");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = add_activity_trace;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$add_avg_temperature_trace");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = add_avg_temperature_trace;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$add_max_temperature_trace");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = add_max_temperature_trace;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$add_min_temperature_trace");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = add_min_temperature_trace;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    int flags = 0;
		vhpiCbDataT cb_data;
		vhpiHandleT cb_handle;
		cb_data.reason = vhpiCbEndOfReset;
		cb_data.cb_rtn = restart_simulation;
		cb_handle = vhpi_register_cb(&cb_data, flags);
		vhpi_release_handle(cb_handle);

		foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$set_thermal_engine");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = set_thermal_engine;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$print_dissipation_map");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = print_dissipation_map;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$print_temperature_map");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = print_temperature_map;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$print_temperature_map_in_svg");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = print_temperature_map_in_svg;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$add_display_component");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = add_display_component;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$set_log_file_path");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = set_log_file_path;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$set_notification_level");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = set_notification_level;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$set_timestep");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = set_timestep;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$read_liberty_file");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = read_liberty_file;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$initialize_logitherm");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = initialize_logitherm;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$start_timer");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = start_timer;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);

	    foreign_data.kind        = vhpiFuncF;
	    foreign_data.libraryName = const_cast<char*>("liblogitherm.so"); // ezt elegansabban kellene megoldani...
	    foreign_data.modelName = const_cast<char*>("$get_elapsed_time");
	    foreign_data.elabf = nullptr;
	    foreign_data.execf = get_elapsed_time;
	    foreignf_handle = vhpi_register_foreignf( &foreign_data );
	    vhpi_release_handle(foreignf_handle);
	}

	/*****************************************************************************
	 *
	 * Required structure for initializing VPI routines.
	 *
	 *****************************************************************************/

	// void (*vhdl_startup_routines[])() = {
	//     register_vhpi_functions,
	//     nullptr
	// };

}

namespace logic
{
	namespace vhdl
	{
		adapter_t::adapter_t(logitherm::manager_t* manager, util::log_t* log)
		:
			logic::adapter_t(manager, log, "logic::vhdl::adapter_t", util::logic_engine_t::vhdl, manager->get_timestep())
		{}
		
		adapter_t::~adapter_t()
		{
			debug("logic::vhdl::adapter_t::~adapter_t()");
			delete_components_tree();
		}				
	}
}
