#ifndef _VHDL_ACTIVITY_TRACE_H_
#define _VHDL_ACTIVITY_TRACE_H_

#include "logic/base/activity_trace_t.hpp"
// #include <vpi_user.h>

namespace logic
{
	namespace vhdl
	{
		
		class activity_trace_t	:	public logic::activity_trace_t
		{
			private:
				unsigned long long time;
			
			public:
				activity_trace_t(const std::string& path, const std::string& prefix, const std::string& id);
				
				void increment(unsigned long long current_time);
				
		};
	}
}
#endif //_VERILOG_ACTIVITY_TRACE_H_
