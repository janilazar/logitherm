#ifndef _VHDL_ACTIVITY_H_
#define _VHDL_ACTIVITY_H_

#include "logic/base/activity_t.hpp"
// #include <vpi_user.h>

namespace logic
{
	namespace vhdl
	{
		class activity_t	:	public logic::activity_t
		{
			private:
				unsigned long long time;
								
			public:
				/**
				 * default konstruktor
				**/ 
				activity_t() = delete;
				
				/**
				 * ez a konstruktor kell a push_back-nek
				 * (sc_dissipator_module-ban az activities vector egy ilyen tipust tartalmaz)
				**/ 
				activity_t(const std::string& id, const std::function<unit::energy_t()> &f);
				
				~activity_t();
				
				void activity(unsigned long long current_time);
		};
	}
}

#endif //_VHDL_ACTIVITY_H_
