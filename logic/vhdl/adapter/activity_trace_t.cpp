#include "logic/vhdl/adapter/activity_trace_t.hpp"


namespace logic
{
	namespace vhdl
	{
		activity_trace_t::activity_trace_t(const std::string& path, const std::string& prefix, const std::string& id)
		:
			logic::activity_trace_t(path, prefix, id),
			time(0ull)
		{}
		
		void activity_trace_t::increment(unsigned long long current_time)
		{
			if(current_time > time)
			{
				logic::activity_trace_t::increment();
				time = current_time;
			}
		}
	}
}
