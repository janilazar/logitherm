#include "logic/vhdl/adapter/activity_t.hpp"
#include "manager/manager_t.hpp"

namespace logic
{
	namespace vhdl
	{
		activity_t::activity_t(const std::string& id, const std::function<unit::energy_t()> &f)
		:
			logic::activity_t(id,f),
			time(0ull)
		{}
		
		activity_t::~activity_t()
		{}
		
		void activity_t::activity(unsigned long long current_time)
		{
			if(current_time > time)
			{
				logic::activity_t::activity();
				time = current_time;
			}
		}
		
	}
}
