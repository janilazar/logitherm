#include "logic/base/adapter_t.hpp"
#include "manager/manager_t.hpp"
#include "util/log_t.hpp"


#include <sys/types.h>
#include <unistd.h>

namespace logic
{
	void adapter_t::debug(const char* msg) const
	{
		log->debug("'" + id + "' " + msg);
	}
	
	void adapter_t::debug(const std::string& msg) const
	{
		log->debug("'" + id + "' " + msg);
	}
	
	void adapter_t::warning(const char* msg) const
	{
		log->warning("'" + id + "' " + msg);
	}
	
	void adapter_t::warning(const std::string& msg) const
	{
		log->warning("'" + id + "' " + msg);
	}
	
	void adapter_t::error(const char* msg) const
	{
		log->error("'" + id + "' " + msg);
	}
	
	void adapter_t::error(const std::string& msg) const
	{
		log->error("'" + id + "' " + msg);
	}
	
	adapter_t::adapter_t(logitherm::manager_t* manager, util::log_t* log, const std::string& id, util::logic_engine_t type, const util::timestep_t& timestep)
	:
		id(id),
		type(type),
		manager(manager),
		log(log),
		timestep(timestep)
	{}
		
	adapter_t::~adapter_t()
	{}
	
	void adapter_t::delete_components_tree()
	{
		for(auto &it: components_tree)
		{
			it.second->delete_component();
			it.second->~component_t();
		}
	}
	
	/**
	 * megkeresi az adott logic komponenst a components_tree-ben
	 * a logic_component search_for_component fv-t hasznalja fel rekurzivan
	**/ 
	bool adapter_t::search_for_component(const std::string& name) const
	{
		return (nullptr != get_component(name));
	}
	
	component_t* adapter_t::get_component(const std::string& name) const
	{	

		// std::cout << "THREAD ID=" << getpid() << std::endl;
		// std::cout << "get component " << name << std::endl;
		component_t* component = nullptr;
		// std::cout << "component nullptr" << std::endl;
		// if(name == "instruction_buffer_0") components_tree.find("instruction_buffer_0");

		// std::cout << "utana??" << std::endl;

		auto component_tree_iterator = components_tree.find(name); //itt hasal el... mi a tokom van???
		
		// std::cout << "component_tree_iterator" << std::endl;
		if( component_tree_iterator != components_tree.end())
		{
			// std::cout << "if ide" << std::endl;
			component = component_tree_iterator->second;
		}
		else
		{
			// std::cout << "else oda" << std::endl;
			for(auto &it: components_tree)
			{
				component = it.second->get_component(name);
				if (nullptr != component) break;
			}			
		}
		return component;
	}

	/**
	 * egy logikai komponenst ad hozza a components_tree-hez
	 * vegig kell iteralnia minden szinten a component_treen, hogy megkeresse a parent-et
	 * hibaellenorzes: component mar egyszer szerepel a tree-ben (nev az azonosito)
	 * hibaellenorzes: a parent nem szerepel a tree-ben
	**/ 
	void adapter_t::add_component(logic::component_t* component)
	{
		if(search_for_component(component->id))
		{
			warning("logic::adapter_t::add_component(): component '" + component->id + "' is already in the 'components_tree'");
		}
		else
		{
			if(component->parent != nullptr)
			{
				if(false == search_for_component(component->parent->id))
				{
					warning("logic::adapter_t::add_component(): could not find the parent '" + component->parent->id + "' of the component '" + component->id + "'");
				}
				else
				{
					component->parent->add_child(component);
					// std::cout << component->id << " added" <<std::endl;
				}
			}
			else
			{
				components_tree.insert(std::make_pair(component->id,component));
				// std::cout << component->id << " added" <<std::endl;
			}
		}
	}
	
	void adapter_t::timestep_changed()
	{}
			
	std::ostream& adapter_t::print_tree(std::ostream& os)
	{
		for(auto &it: components_tree) it.second->print_component(os);
		return os;
	}
} //namespace logic
