#ifndef _ACTIVITY_TRACE_
#define _ACTIVITY_TRACE_

#include "util/trace_t.hpp"

namespace logic
{
	class activity_trace_t	:	public util::trace_t
	{
		private:
			// component_t* component;
			size_t counter;
					
		public:
			/**
			 * path -> file eleresi utvonala
			 * prefix -> trace nevenek eleje, jellemzoen a modul neve
			 * id -> trace azonositoja
			**/ 
			activity_trace_t(const std::string& path, const std::string& id, const std::string& postfix);

			activity_trace_t(const std::string& path, const std::string& id, const std::string& postfix, const std::string& filename);
			
			void initialize(std::ostream& os) override;
			
			void trace(std::ostream& os) override;
			
			void reset() override;
			
			void finalize(std::ostream& os) override;
			
			void increment();	
	};
	
}

#endif //_ACTIVITY_TRACE_
