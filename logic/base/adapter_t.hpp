#ifndef _LOGIC_BASE_
#define _LOGIC_BASE_

#include <set>
#include <string>
#include <iostream>

#include "logic/base/component_t.hpp"
#include "unit/all_units.hpp"
#include "util/timestep_t.hpp"

namespace logitherm
{
	class manager_t;
}

namespace util
{
	class log_t;
}

namespace logic
{
	class adapter_t
	{
		public:
			const std::string id;
			const util::logic_engine_t type;

		private:
			
			friend class logic::component_t;

			/**
			 * az osszes logikai komponenst taroloja
			 * a hierarchia megfelel a leiras (jelenleg: SystemC) hierarchiajanak)
			**/ 
			std::unordered_map<std::string,logic::component_t*> components_tree;
			
			/**
			 * egy logikai komponenst ad hozza a logic_components_tree-hez
			 * vegig kell iteralnia minden szinten a component_treen, hogy megkeresse a parent-et
			 * hibaellenorzes: component mar egyszer szerepel a tree-ben (nev az azonosito)
			 * hibaellenorzes: a parent nem szerepel a tree-ben
			**/ 
			void add_component(logic::component_t* component);
			
		public:
			/**
			 * injected dependency
			**/
			logitherm::manager_t* const manager;
			util::log_t* const log;

			/**
			 * co-szimulacio idolepteke
			**/ 
			const util::timestep_t& timestep;
			
		public:
			/**
			 * megkeresi az adott logikai komponenst a logic_components_tree-ben
			 * a logic_component search_for_component fv-t hasznalja fel rekurzivan
			**/ 
			bool search_for_component(const std::string& name) const;
			
			logic::component_t* get_component(const std::string& name) const;
			
			adapter_t(logitherm::manager_t* manager, util::log_t* log, const std::string& id, util::logic_engine_t type, const util::timestep_t& timestep);
			
			virtual ~adapter_t();
			
			void delete_components_tree();
			
			/**
			 * ez a fuggveny allitja be a co-szimulacio idolepteket
			**/
			virtual void timestep_changed();
			
			std::ostream& print_tree(std::ostream& os);


			void debug(const char* msg) const;
			void debug(const std::string& msg) const;
			void warning(const char* msg) const;
			void warning(const std::string& msg) const;
			void error(const char*) const;
			void error(const std::string& msg) const;
	};
} /** namespace logic **/
#endif //_LOGIC_BASE_
