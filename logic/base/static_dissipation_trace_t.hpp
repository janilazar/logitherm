#ifndef _STATIC_DISSIPATION_TRACE_
#define _STATIC_DISSIPATION_TRACE_

#include "util/trace_t.hpp"
#include "util/timestep_t.hpp"

namespace logic
{
	
	class component_t;
	
	class static_dissipation_trace_t	:	public util::trace_t
	{
		private:
			component_t* component;
			const util::timestep_t& timestep;
			
		public:
			
			static_dissipation_trace_t(const std::string& path, const std::string& postfix, logic::component_t* component);
			
			
			void initialize(std::ostream& os) override;
			
			void trace(std::ostream& os) override;
			
			void reset() override;
			
			void finalize(std::ostream& os) override;
	};
}

#endif //_STATIC_DISSIPATION_TRACE_
