#ifndef _LOGIC_COMPONENT_
#define _LOGIC_COMPONENT_

#include <string>
#include <unordered_map>
#include <iostream>
#include <functional>

#include "logic/base/activity_t.hpp"

#include "util/tracer_t.hpp"

#include "unit/all_units.hpp"
//#include "unit/temperature_t.hpp"

namespace logitherm
{
	class manager_t;
}

namespace util
{
	class log_t;
}

namespace logic
{
	class adapter_t;
	class activity_trace_t;
	class dynamic_dissipation_trace_t;
	class static_dissipation_trace_t;
	/**
	 * logikai komponens egy modulnak felel meg systemc-ben, verilogban, vhdl-ben
	 * logikai komponensek hierarchiaba rendezodnek, egy komponensben lehet tobb komponens is,
	 * de egy komponens csak egy masikhoz tartozhat (egy parent, tobb child komponens lehet)
	 * a tenyleges logikai motorok logic componentjenek ebbol az osztalybol kellene leszarmaznia
	**/ 
	class component_t
	{
		public:
		
			/**
			 * komponens neve, egyertelmuen azonositja a komponenst
			**/ 
			const std::string id;
			
			/**
			 * a parentre mutato pointer
			 * ha nincs parent, akkor nullptr
			**/ 
			component_t* const parent;

			/**
			 * injected dependency
			**/
			//::logitherm::manager_t* const manager;
			logic::adapter_t* const adapter;
			util::log_t* const log;

		public:
			/**
			 * a komponens homerseklete
			**/ 
			unit::temperature_t temperature;
		
		protected:
			/**
			 * children-ekre mutato pointer
			 * osszehasonlitas a nev string alapjan
			**/
			std::unordered_map<std::string, component_t*> children;
			
			/**
			 * activity-ket tarol
			 * elso egy egyedi azonosito, masodik az activity
			**/ 
			std::unordered_map<std::string, logic::activity_t*> activities; //itt mi a fenenek kell kiirni a ::logic-ot??
		
			/**
			 * a megfigyelendo adatok rogziteseert felelos valtozo
			**/ 
			util::tracer_t tracer;
			
		public:
		
			component_t() = delete;
		
			/**
			 * konstruktor, ami a nev es a parent mutato alapjan inicializalja a komponenst.
			 * minden esetben kellenek ezek az informaciok egy logic_component letrehozasahoz
			**/ 
			component_t(logic::adapter_t* adapter, const std::string& id, component_t* const parent);
			
			/**
			 * virtualis destruktor
			**/ 
			virtual ~component_t();	
			 
			/**
			 * hozzaad egy uj child-ot a children-hez
			 * hibaellenorzes: adott komponens mar megtalalhato a childrenben
			**/
			void add_child(component_t* component);
			
			/**
			 * torol minden elemet a childrenben
			**/ 
			virtual void delete_children();
			
			/**
			 * torli az activities-t
			**/ 
			virtual void delete_activities();
			
			/**
			 * meghivja a component torlo fv-eit
			**/ 
			virtual void delete_component();
			
			/**
			 * beallitja a tracer valtozot
			 * leszarmazott tracer_t eseten override-olni kell ezt is
			**/ 
			virtual void initialize_tracer();			
			
			/**
			 * a component statikus disszipacioja
			**/ 
			virtual unit::power_t component_static_dissipation() const;
			unit::power_t static_dissipation() const;
			
			
			/**
			 * a component dinamikus disszipacioja
			**/ 
			virtual unit::power_t component_dynamic_dissipation() const;
			unit::power_t dynamic_dissipation() const;
			
			/**
			 * logic_component-ben es a childrenjeiben keressuk a component-et
			 * BFS szeru bejaras: ha nincs components-ben, akkor vegigmegyunk components-en,
			 * es mindegyik children-jeben rakeresunk 
			**/ 
			bool search_for_component(const std::string& name) const;

			component_t* get_component(const std::string& name) const;
			
			/**
			 * visszater a component fogyasztasaval egy szimulacios ciklusban
			 * ezt a fuggvenyt meg kell valositania az ebbol leszarmazo component-nek
			 * ami mar szimulacios motor specifikus
			**/ 
			unit::power_t get_component_dissipation() const;
			
			/**
			 * visszater a component es az osszes leszarmazott fogyasztasanak osszegevel
			**/ 
			unit::power_t get_dissipation() const; 
			
			void add_activity(activity_t*);
			
			
			void reset();
			
			/**
			 * kapott ostream-re kirakja a component es leszarmazottak nevet
			**/ 
			virtual std::ostream& print_component(std::ostream& os, std::string indent = "");


			void debug(const char* msg) const;
			void debug(const std::string& msg) const;
			void warning(const char* msg) const;
			void warning(const std::string& msg) const;
			void error(const char*) const;
			void error(const std::string& msg) const;

			void trace_component();
			
			activity_trace_t* add_activity_trace(const std::string& path, const std::string& id, const std::string& postfix = "");
			static_dissipation_trace_t* add_static_dissipation_trace(const std::string& path, const std::string& postfix = "");
			dynamic_dissipation_trace_t* add_dynamic_dissipation_trace(const std::string& path, const std::string& postfix = "");
			
	};
	

	/**
	 * osszehasonlito functor az std::set-hez
	 * az osszehasonlitas a nev alapjan tortenik
	**/
	struct components_equal
	{	 
		bool operator() (const logic::component_t* lhs, const logic::component_t* rhs) const
		{
			return lhs->id  ==  rhs->id;
		}
		
	};
} /** namespace logic **/	

namespace std
{
	template<>
	struct hash<logic::component_t*>
	{	 
		size_t operator() (const logic::component_t* component) const
		{
			if(component == nullptr) throw("hash<logic::component_t*>operator(): operand is nullptr");
			return hash<string>()(component->id);
		}
		
	};
} // namespace std
	
#endif //_LOGIC_COMPONENT_
