#include "logic/base/static_dissipation_trace_t.hpp"
#include "logic/base/component_t.hpp"
#include "logic/base/adapter_t.hpp"
#include "util/string_manipulation.hpp"

namespace logic
{	
	static_dissipation_trace_t::static_dissipation_trace_t(const std::string& path, const std::string& postfix, logic::component_t* component)
	:
		util::trace_t(path, component->id, "static_dissipation", postfix),
		component(component),
		timestep(component->adapter->timestep)
	{}


	void static_dissipation_trace_t::initialize(std::ostream& os)
	{
		os << "timestamp, "<< id << "_" << type;
	}

	void static_dissipation_trace_t::trace(std::ostream& os)
	{
		os << util::to_string(timestep.current_time()) << ", " << component->static_dissipation();
	}

	void static_dissipation_trace_t::reset()
	{}
	
	void static_dissipation_trace_t::finalize(std::ostream& os)
	{}
}
