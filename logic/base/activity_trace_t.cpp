#include "logic/base/activity_trace_t.hpp"
#include "logic/base/component_t.hpp"

namespace logic
{
	activity_trace_t::activity_trace_t(const std::string& path, const std::string& id, const std::string& postfix)
	:
		util::trace_t(path, id, "activity", postfix),
		counter(0)
	{}

	activity_trace_t::activity_trace_t(const std::string& path, const std::string& id, const std::string& postfix, const std::string& filename)
	:
		util::trace_t(path, id, "activity", postfix, filename),
		counter(0)
	{}

	void activity_trace_t::initialize(std::ostream& os)
	{
		os << id << "_" << type;
	}

	void activity_trace_t::trace(std::ostream& os)
	{
		os << counter; 
	}

	void activity_trace_t::reset()
	{
		counter = 0;
	}
	
	void activity_trace_t::finalize(std::ostream& os)
	{}

	void activity_trace_t::increment()
	{
		++counter;
	}
}
