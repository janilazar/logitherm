#include "logic/base/activity_t.hpp"
//#include "manager/manager_t.hpp"

namespace logic
{
	activity_t::activity_t(const std::string& id, const std::function<unit::energy_t()> &f)
	:
		energy(0.0_J),
		energy_function(f),
		id(id)
	{}
	
	activity_t::~activity_t()
	{}
	
	/** 
	 * ezt kell meghivni a ciklus legvegen
	 * logic_component start_new_cycle() fv-eben van meghivva
	**/
	void activity_t::reset()
	{
		energy = 0.0_J;
	}
	
	
	/**
	 * ezt a fuggvenyt hivja meg esemeny hatasara a logikai motor, ennek kell beallitania az energy valtozot
	 * a leszarmazott activity-nek ezt implementalnia kell
	**/
	void activity_t::activity()
	{
		energy += energy_function();
	}
	
	/**
	 * ezt a fv-t hivja meg a logic_component get_component_dissipation() fv-e
	**/ 
	unit::energy_t activity_t::activity_energy()
	{
		return energy;
	}
}
