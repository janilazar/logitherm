#ifndef _LOGIC_ACTIVITY_
#define _LOGIC_ACTIVITY_

#include <functional>
#include <utility>
#include <string>
#include <unordered_map>

#include "unit/all_units.hpp"

namespace logitherm
{
	/** forward decl **/
	class manager_t;
}

namespace logic
{
	class activity_t
	{
		protected:
			
			/**
			 * injected dependency
			**/ 
			//::logitherm::manager_t* const manager;
			
			/**
			 * egy ciklusban disszipalt energia
			**/ 
			unit::energy_t energy;
			
			
			/**
			 * ehhez az aktivitashoz rendelt disszipaciot kiszamito fv
			**/ 
			std::function<unit::energy_t()> energy_function;
						
		public:
			
			const std::string id;			
			
			activity_t() = delete;
			
			activity_t(const std::string& id, const std::function<unit::energy_t()> &f);
			
			virtual ~activity_t();
			
			/** 
			 * ezt kell meghivni a ciklus legvegen
			 * logic_component start_new_cycle() fv-eben van meghivva
			**/
			virtual void reset();
			
			
			/**
			 * ezt a fuggvenyt hivja meg esemeny hatasara a logikai motor, ennek kell beallitania az energy valtozot
			 * a leszarmazott activity-nek ezt implementalnia kell
			**/
			virtual void activity();
			
			/**
			 * ezt a fv-t hivja meg a logic_component get_component_dissipation() fv-e
			**/ 
			unit::energy_t activity_energy();
	};
}

#endif //_LOGIC_ACTIVITY_
