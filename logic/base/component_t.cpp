#include "logic/base/component_t.hpp"
#include "logic/base/adapter_t.hpp"
#include "manager/manager_t.hpp"

#include "logic/base/activity_trace_t.hpp"
#include "logic/base/static_dissipation_trace_t.hpp"
#include "logic/base/dynamic_dissipation_trace_t.hpp"

namespace logic
{
	void component_t::debug(const char* msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr!\n DEBUG MSG '" + msg + "'");
		log->debug("'" + id + "' " + msg);
	}
	
	void component_t::debug(const std::string& msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr!\n DEBUG MSG '" + msg + "'");
		log->debug("'" + id + "' " + msg);
	}
	
	void component_t::warning(const char* msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr!\n WARNING MSG '" + msg + "'");
		log->warning("'" + id + "' " + msg);
	}
	
	void component_t::warning(const std::string& msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr!\n WARNING MSG '" + msg + "'");
		log->warning("'" + id + "' " + msg);
	}
	
	void component_t::error(const char* msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr!\n ERROR MSG '" + msg + "'");
		log->error("'" + id + "' " + msg);
	}
	
	void component_t::error(const std::string& msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr!\n ERROR MSG '" + msg + "'");
		log->error("'" + id + "' " + msg);
	}
	
	/**
	 * konstruktor, ami a nev es a parent mutato alapjan inicializalja a komponenst.
	 * minden esetben kellenek ezek az informaciok egy logic_component letrehozasahoz
	 * kapasbol hozzaadjuk a logic_component_tree-hez
	**/ 
	component_t::component_t(adapter_t* adapter, const std::string& id, component_t* const parent)
	:
		id(id), // verilog miatt megis csak ilyenre csinalom
		parent(parent),
		adapter(adapter),
		log((adapter != nullptr) ? adapter->log : nullptr),
		//manager((adapter != nullptr) ? adapter->manager : nullptr),
		temperature(300_K),
		tracer("logic_component", id, this->log)
	{

		if(nullptr == adapter)
		{
			std::cerr << "WARNING '"<< id <<"' component_t(): logic adapter is nullptr" << std::endl;
		}
		else
		{
			adapter->add_component(this);
		}
	}
	
	component_t::~component_t()
	{}
	 
	/**
	 * hozzaad egy uj child-ot a children-hez
	 * hozzaadott child parent-jet beallitja this-re
	 * hibaellenorzes: adott komponens mar megtalalhato a childrenben
	**/
	void component_t::add_child(component_t* component)
	{
		if(children.find(component->id) != children.end())
		{
			warning("logic::component_t::add_child(): '" + component->id + "' logic component is already among the 'children'");
		}
		else
		{
			children.insert(std::make_pair(component->id,component));
		}
	}
	
	
	/**
	 * torol minden elemet a childrenben
	**/ 
	void component_t::delete_children()
	{
		for(auto &it: children)
		{
			it.second->delete_component();
			it.second->~component_t(); // i made dis
		}
	}
	
	/**
	 * torli az activities-t
	**/ 
	void component_t::delete_activities()
	{
		for(auto &it: activities)
		{
			delete it.second;
		}
	}
	
	/**
	 * meghivja a component torlo fv-eit
	**/ 
	void component_t::delete_component()
	{
		debug("logic::component_t::delete_component()");
		delete_children();
		delete_activities();
		// ~component_t(); 
	}

	
	void component_t::initialize_tracer()
	{}
	
	unit::power_t component_t::component_static_dissipation() const
	{
		return 0_W;
	}
	
	unit::power_t component_t::static_dissipation() const
	{
		unit::power_t dissipation = component_static_dissipation();
		
		// if(std::isnan(static_cast<double>(dissipation))) 
		// {
		// 	std::cout << "#1 static_dissipation(): " << "static_power is nan, " << dissipation << std::endl;
		// }

		for (auto &it: children) dissipation += it.second->static_dissipation();

		// if(std::isnan(static_cast<double>(dissipation))) 
		// {
		// 	std::cout << "#2 static_dissipation(): " << "static_power is nan, " << dissipation << std::endl;
		// }

		return dissipation;
	}
	
	unit::power_t component_t::component_dynamic_dissipation() const
	{
		unit::power_t dissipation = 0.0_W;
		
		unit::time_t timestep = adapter->timestep.current_timestep();
		for (auto &it: activities) dissipation += it.second->activity_energy()/timestep;
		return dissipation;
	}
	
	unit::power_t component_t::dynamic_dissipation() const
	{
		unit::power_t dissipation = component_dynamic_dissipation();
		for (auto &it: children) dissipation += it.second->dynamic_dissipation();
		return dissipation;
	}
	
	/**
	 * logic_component-ben es a childrenjeiben keressuk a component-et
	 * BFS szeru bejaras: ha nincs children-ben, akkor azokra a children-re ujra meghivjuk a fv-t
	**/ 
	bool component_t::search_for_component(const std::string& name) const
	{
		return (nullptr != get_component(name));
	}
	
	component_t* component_t::get_component(const std::string& name) const
	{
		component_t* component = nullptr;
		if(children.find(name) != children.end())
		{
			component = children.find(name)->second;
		}
		else
		{
			for (auto &it: children)
			{
				component = it.second->get_component(name);
				if(nullptr != component) break;
			}
		}
		return component;
	}
	
	unit::power_t component_t::get_component_dissipation() const
	{
		unit::power_t dissipation = component_static_dissipation();
		dissipation += component_dynamic_dissipation();
		
		return dissipation;
	}
	
	unit::power_t component_t::get_dissipation() const
	{
		unit::power_t static_power = static_dissipation();
		unit::power_t dynamic_power = dynamic_dissipation();

		unit::power_t dissipation = static_power + dynamic_power;

		// if(std::isnan(static_cast<double>(static_power))) 
		// {
		// 	std::cout << " get_dissipation(): " << "static_power is nan" << std::endl;
		// }

		// if(std::isnan(static_cast<double>(dynamic_power))) 
		// {
		// 	std::cout << " get_dissipation(): " << "dynamic_power is nan" << std::endl;
		// }

		// if(std::isnan(static_cast<double>(dissipation))) //is_NaN
		// {
			
		// 	std::cout << id << " static=" << static_power << " dynamic=" << dynamic_power;
		// 	std::cout << " sum=" << static_cast<double>(static_power + dynamic_power) << " total=" << dissipation << std::endl;
		// }
		return dissipation;
		// return static_dissipation() + dynamic_dissipation();
	}
	
	void component_t::add_activity(activity_t* activity)
	{
		if(activities.find(activity->id) != activities.end())
		{
			warning("logic::component_t::add_activity(): activity '" + activity->id +"' is already among the 'activities'");
		}
		else
		{
			debug("logic::component_t::add_activity(): activity '" + activity->id + "' added");
			activities.insert(std::make_pair(activity->id, activity));
		}
	}	
	
	void component_t::reset()
	{
		for(auto &it: activities) it.second->reset();
		for (auto &it: children) it.second->reset();
	}
	
	/**
	 * kapott ostream-re kirakja a component es leszarmazottak nevet
	**/ 
	std::ostream& component_t::print_component(std::ostream& os, std::string indent)
	{
		os << indent << id << std::endl;
		if(!children.empty())
		{
			indent += "  ";
			for(auto &it: children)
			{
				it.second->print_component(os, indent);
			}
		}
		return os;
	}
	
	void component_t::trace_component()
	{
		if(adapter == nullptr) error("'" + id + "' adapter is nullptr");
		adapter->manager->trace_component(&this->tracer);
	}

	activity_trace_t* component_t::add_activity_trace(const std::string& path, const std::string& activity_id, const std::string& postfix)
	{
		activity_trace_t* ptr = new activity_trace_t(path, this->id + "_" + activity_id, postfix);
		tracer.add_trace(ptr);
		trace_component();
		return ptr;
	}
	
	static_dissipation_trace_t* component_t::add_static_dissipation_trace(const std::string& path, const std::string& postfix)
	{
		static_dissipation_trace_t* ptr = new static_dissipation_trace_t(path, postfix, this);
		tracer.add_trace(ptr);
		trace_component();
		return ptr;
	}
	
	dynamic_dissipation_trace_t* component_t::add_dynamic_dissipation_trace(const std::string& path, const std::string& postfix)
	{
		dynamic_dissipation_trace_t* ptr = new dynamic_dissipation_trace_t(path, postfix, this);
		tracer.add_trace(ptr);
		trace_component();
		return ptr;
	}
	
} //namespace logic
