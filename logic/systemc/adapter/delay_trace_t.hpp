#ifndef _SC_DELAY_TRACE_
#define _SC_DELAY_TRACE_

#include "util/trace_t.hpp"
#include "logic/systemc/adapter/delay_t.hpp"

namespace logic
{
	namespace systemc
	{
		class delay_trace_t	:	public util::trace_t
		{
			private:
				delay_t* signal;
				
			public:
				const std::string id;
				
			public:
				
				//kell meg egy konstruktor, ami file-t is beallitja
				
				
				delay_trace_t(const std::string& path, const std::string& file, const std::string& postfix, logic::systemc::delay_t* signal);
								
				void initialize(std::ostream& os) override;
				
				void trace(std::ostream& os) override;
				
				void reset() override;
				
				void finalize(std::ostream& os) override;
		};
	} //namespace systemc
} //namespace logic

#endif //_SC_DELAY_TRACE_
