#include "logic/systemc/adapter/delay_trace_t.hpp"

namespace logic
{
	namespace systemc
	{

		delay_trace_t::delay_trace_t(const std::string& path, const std::string& file, const std::string& postfix, logic::systemc::delay_t* signal)
		:
			util::trace_t(path, signal->name(), "delay", postfix, file),
			signal(signal),
			id(signal->name())
		{}


		void delay_trace_t::initialize(std::ostream& os)
		{
			os << id << "_" << type;
		}

		void delay_trace_t::trace(std::ostream& os)
		{
			os << signal->get_delay();
		}

		void delay_trace_t::reset()
		{}
		
		void delay_trace_t::finalize(std::ostream& os)
		{}
		
	} //namespace systemc
} //namespace logic
