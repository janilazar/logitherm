#ifndef _LOGIC_ADAPTER_
#define _LOGIC_ADAPTER_

#include <set>
#include <string>
#include <iostream>

#include "logic/base/adapter_t.hpp"
#include "logic/systemc/digital/src/sysc/kernel/sc_status.h"
#include "logic/systemc/digital/src/sysc/kernel/sc_object.h"

namespace logic
{
	namespace systemc
	{
		class trigger_module_t;

		class adapter_t	:	public logic::adapter_t,
							public sc_core::sc_object
		{
			private:
				trigger_module_t* progress_notifier;
				trigger_module_t* trigger_co_simulation;
				
				bool do_refresh_temperatures;
				
			
			public:				
				adapter_t() = delete;

				adapter_t(logitherm::manager_t* manager, util::log_t* log);
				
				~adapter_t();
				
				/** sc_core::sc_object-bol oroklunk, annak van ilyen fv-e, ami bejegyezheto callback-kent**/
				void simulation_phase_callback() override;
				
				void timestep_changed() override;
		};
	} // namespace systemc
} // namespace logic

#endif //_LOGIC_ADAPTER_
