#include "logic/systemc/adapter/adapter_t.hpp"
#include "manager/manager_t.hpp"
#include "logic/systemc/adapter/trigger_module_t.hpp"
#include "logic/systemc/digital/src/sysc/kernel/sc_time.h"
#include "logic/systemc/digital/src/sysc/kernel/sc_simcontext.h"

namespace logic
{
	namespace systemc
	{
		adapter_t::adapter_t(logitherm::manager_t* manager, util::log_t* log)
		:
			logic::adapter_t(manager, log, "logic::systemc::adapter_t", util::logic_engine_t::systemc, manager->get_timestep()),
			sc_core::sc_object(id.c_str()),
			progress_notifier(new trigger_module_t("notifier",[&,this]()->void {std::cout << sc_core::sc_time_stamp() << std::endl; progress_notifier->notify(sc_core::sc_time(1, sc_core::SC_MS));})),
			trigger_co_simulation(new trigger_module_t("trigger",[&,this]()->void {do_refresh_temperatures=true; trigger_co_simulation->notify(sc_core::sc_time(static_cast<double>(timestep.next_timestep()*1e6), sc_core::SC_US));})),
			do_refresh_temperatures(false)
		{
			progress_notifier->notify(sc_core::sc_time(1, sc_core::SC_MS));

			// trigger_co_simulation.notify(sc_core::sc_time(static_cast<double>(timestep.next_timestep()), sc_core::SC_SEC));
			// register_simulation_phase_callback(sc_core::SC_BEFORE_TIMESTEP);
		}
		
		adapter_t::~adapter_t()
		{
			delete progress_notifier;
			delete trigger_co_simulation;
		}

		void adapter_t::simulation_phase_callback()
		{
			if(true == do_refresh_temperatures)
			{
				if(nullptr == manager)
				{
					std::cerr << "'" << id << "' logic::systemc::adapter_t::simulation_phase_callback(): manager is nullptr" << std::endl;
					throw("'" + id + "' logic::systemc::adapter_t::simulation_phase_callback(): manager is nullptr");
				}
				manager->run_simulation_cycle();
				do_refresh_temperatures = false;
			}
			
			
		}
				
		//TODO itt javitani kell!!4negy
		void adapter_t::timestep_changed()
		{
			trigger_co_simulation->notify(sc_core::sc_time(static_cast<double>(timestep.next_timestep()*1e6), sc_core::SC_US));
			register_simulation_phase_callback(sc_core::SC_BEFORE_TIMESTEP);
		}
	}
}
