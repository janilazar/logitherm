#ifndef _DELAYED_SIGNAL_H_
#define _DELAYED_SIGNAL_H_

#include "logic/systemc/digital/src/sysc/communication/sc_port.h"
#include "logic/systemc/digital/src/sysc/communication/sc_signal_ifs.h"
#include "logic/systemc/digital/src/sysc/communication/sc_signal_ports.h"
#include "logic/systemc/digital/src/sysc/communication/sc_signal.h"
#include "logic/systemc/digital/src/sysc/communication/sc_writer_policy.h"
#include "logic/systemc/digital/src/sysc/kernel/sc_event.h"
#include "logic/systemc/digital/src/sysc/kernel/sc_process.h"

#include "logic/systemc/adapter/delay_t.hpp"

#include "logic/systemc/digital/src/sysc/kernel/sc_simcontext.h"
#include "logic/systemc/digital/src/sysc/tracing/sc_trace.h"
#include <typeinfo>

#include <queue>
#include <functional>
// for ... debug purpose
#include <iostream>

// ----------------------------------------------------------------------------
//  CLASS : sc_delayed_signal<T>
//
//  The sc_delayed_signal<T> primitive channel class.
// ----------------------------------------------------------------------------
namespace logic{
	namespace systemc{		
		
#if ! defined( SC_DISABLE_VIRTUAL_BIND )
#  define SC_VIRTUAL_ virtual
#else
#  define SC_VIRTUAL_ /* non-virtual */
#endif
		
		
		
		
		
		template< class T, ::sc_core::sc_writer_policy POL /* = SC_ONE_WRITER */ >
		class sc_delayed_signal	:	public    ::sc_core::sc_signal_inout_if<T>,
									public    delay_t,
									protected ::sc_core::sc_writer_policy_check<POL>
		{
			protected:
				typedef ::sc_core::sc_signal_inout_if<T>       	if_type;
				typedef sc_delayed_signal<T,POL> 				this_type;
				typedef ::sc_core::sc_writer_policy_check<POL> 	policy_type;

			public: // constructors and destructor:
			
				SC_HAS_PROCESS(sc_delayed_signal);
				
				sc_delayed_signal()
				: delay_t([&,this]()->void{m_value = m_queue; m_out = m_queue;}),
				  m_value(::sc_core::sc_gen_unique_name("delayed_signal_value")),
				  m_queue(),
				  m_out(::sc_core::sc_gen_unique_name("out"))
				{}

				explicit sc_delayed_signal( const char* name_)
				: delay_t( name_, [&,this]()->void{m_value = m_queue; m_out = m_queue;}),
				  m_value(std::string(std::string(name_) + "delayed_signal_value").c_str()), 
				  m_queue(),
				  m_out(std::string(std::string(name_) + "out").c_str())
				{}


				virtual ~sc_delayed_signal(){}


				// interface methods
				virtual void register_port( ::sc_core::sc_port_base&, const char* );

				virtual ::sc_core::sc_writer_policy get_writer_policy() const
				{
					return POL;
				}

				// get the default event
				virtual const ::sc_core::sc_event& default_event() const
				{ 
					return m_value.default_event();
				}


				// get the value changed event
				virtual const ::sc_core::sc_event& value_changed_event() const
				{
					return m_value.value_changed_event();
				}

				// read the current value
				virtual const T& read() const
				{
					return m_value.read();
				}

				// get a reference to the current value (for tracing)
				virtual const T& get_data_ref() const
				{
					m_value.get_data_ref();
				}


				// was there an event?
				virtual bool event() const
				{
					return m_value.event();
				}
				
				// write the new value
				virtual void write( const T& );

				void initialize(const T& init_value){
					m_value.write(init_value);//na milesz??
					m_out.initialize(init_value);
				}


				// other methods
				operator const T& () const
				{
					return read();
				}

				this_type& operator = ( const T& a )
				{
					write( a );
					return *this;
				}

				this_type& operator = ( const ::sc_core::sc_signal_in_if<T>& a )
				{
					write( a.read() );
					return *this;
				}

				this_type& operator = ( const this_type& a )
				{
					write( a.read() );
					return *this;
				}

				const T& get_new_value() const
				{
					return m_value.get_new_value();
					//return m_new_val;
				}

				void operator () ( if_type& interface_ )
				{
					m_out.bind( interface_ );
				}

				void operator () ( ::sc_core::sc_inout<T>& parent_ )
				{
					m_out.bind( parent_ );
				}
				
				void trace( ::sc_core::sc_trace_file* tf ) const
				{ 
					::sc_core::sc_deprecated_trace();
				#		ifdef DEBUG_SYSTEMC
						::sc_core::sc_trace( tf, read(), name() ); 
				#       else
							if ( tf ) {}
				#	    endif
				}


				virtual void print( ::std::ostream& = ::std::cout ) const;
				virtual void dump( ::std::ostream& = ::std::cout ) const;

				virtual const char* kind() const
				{
					return "sc_delayed_signal";
				}

			private:
				//void action() override;

			protected:

				virtual void update();
				void do_update();
			
			public:
				::sc_core::sc_signal<T,POL>   		new_value;

			protected:
				::sc_core::sc_signal<T,POL>   		m_value;
				//std::queue<T>	   					m_queue;
				::sc_core::sc_signal<T,POL>			m_queue;
			public:
				sc_core::sc_out<T>					m_out;
				
				
			private:

				// disabled
				sc_delayed_signal( const this_type& );
		};
		
		template< class T, ::sc_core::sc_writer_policy POL >
		inline void sc_delayed_signal<T,POL>::register_port( ::sc_core::sc_port_base& port_, const char* if_typename_ )
		{
			bool is_output = std::string( if_typename_ ) == typeid(if_type).name();
			if( !policy_type::check_port( this, &port_, is_output ) )
			   ((void)0); // fallback? error has been suppressed ...
		}

		// write the new value
		// ezt modositom !!!!negy
		//template< class T, ::sc_core::sc_writer_policy POL >
		//inline void sc_delayed_signal<T,POL>::action()
		//{
		//	//::sc_core::sc_time current_time = ::sc_core::sc_time_stamp();
		//	//m_value = m_queue.front();
		//	m_value = m_queue;
		//	m_out = m_queue;
		//	//m_queue.pop();			
		//}


		// write the new value
		// ezt modositom !!!!negy
		template< class T, ::sc_core::sc_writer_policy POL >
		inline void sc_delayed_signal<T,POL>::write( const T& value_ )
		{
			//std::cout << name() << " zero_delay " << this->zero_delay() << std::endl;
			new_value = value_;
			if(this->zero_delay())
			{
				m_value = value_;
				m_out = value_;
			}
			else
			{
				m_queue = value_;
				this->notify(get_delay());
			}
		}


		template< class T, ::sc_core::sc_writer_policy POL >
		inline void sc_delayed_signal<T,POL>::print( ::std::ostream& os ) const
		{
			m_value.print(os);
		}

		template< class T, ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<T,POL>::dump( ::std::ostream& os ) const
		{
			m_value.dump(os);
		}
		
		
		template< class T, ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<T,POL>::update()
		{
			m_value.update();
		}

		template< class T, ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<T,POL>::do_update()
		{
			m_value.do_update();
		}
		
		
		template< ::sc_core::sc_writer_policy POL >
		class sc_delayed_signal<bool,POL>	:	public    ::sc_core::sc_signal_inout_if<bool>,
												public    delay_t,
												protected ::sc_core::sc_writer_policy_check<POL>
		{
			protected:
				typedef ::sc_core::sc_signal_inout_if<bool>    	if_type;
				typedef sc_delayed_signal<bool,POL> 			this_type;
				typedef ::sc_core::sc_writer_policy_check<POL> 	policy_type;

			public: // constructors and destructor:

				SC_HAS_PROCESS(sc_delayed_signal);
				
				sc_delayed_signal()
				: delay_t(::sc_core::sc_gen_unique_name("delayed_signal_value"), [&,this]()->void{m_value = m_queue; m_out = m_queue;}),
				  m_value(::sc_core::sc_gen_unique_name("delayed_signal_value")),
				  m_queue(),
				  m_out(::sc_core::sc_gen_unique_name("out"))
				{}

				explicit sc_delayed_signal( const char* name_)
				: delay_t( name_, [&,this]()->void{m_value = m_queue; m_out = m_queue;}),
				  m_value(std::string(std::string(name_) + "delayed_signal_value").c_str()),
				  m_queue(),
				  m_out(std::string(std::string(name_) + "out").c_str())
				{}
				
				explicit sc_delayed_signal( const char* name_, bool initial_value_)
				: delay_t( name_, [&,this]()->void{m_value = m_queue; m_out = m_queue;}),
				  m_value(std::string(std::string(name_) + "delayed_signal_value").c_str(), initial_value_),
				  m_queue(),
				  m_out(std::string(std::string(name_) + "out").c_str())
				{}

				virtual ~sc_delayed_signal();

		
				// interface methods

				virtual void register_port( ::sc_core::sc_port_base&, const char* );

				virtual ::sc_core::sc_writer_policy get_writer_policy() const
				{
					return POL;
				}

				// get the default event
				virtual const ::sc_core::sc_event& default_event() const
				{
					return m_value.default_event();
				}

				// get the value changed event
				virtual const ::sc_core::sc_event& value_changed_event() const;

				// get the positive edge event
				virtual const ::sc_core::sc_event& posedge_event() const;

				// get the negative edge event
				virtual const ::sc_core::sc_event& negedge_event() const;


				// read the current value
				virtual const bool& read() const
				{
					return m_value.read();
				}

				// get a reference to the current value (for tracing)
				virtual const bool& get_data_ref() const
				{
					return m_value.get_data_ref();
				}


				// was there a value changed event?
				virtual bool event() const
				{
					return m_value.event();
				}

				// was there a positive edge event?
				virtual bool posedge() const
				{
					return m_value.posedge();
				}

				// was there a negative edge event?
				virtual bool negedge() const
				{
					return m_value.negedge();
				}

				// write the new value
				virtual void write( const bool& );

				void initialize(const bool& init_value)
				{
					m_value.write(init_value);//na milesz??
					m_out.initialize(init_value);
				}


				// other methods

				operator const bool& () const
				{
					return m_value.read();
				}


				this_type& operator = ( const bool& a )
				{ 
					std::cout << "operator = " << a << std::endl;
					write( a );
					return *this;
				}

				this_type& operator = ( const ::sc_core::sc_signal_in_if<bool>& a )
				{
					std::cout << "operator = "<< a.read() << std::endl;
					write( a.read() );
					return *this;
				}

				this_type& operator = ( const this_type& a )
				{
					std::cout << "operator = "<< a << std::endl;
					write( a.read() );
					return *this;
				}				
				
				const bool& get_new_value() const
				{
					return m_value.get_new_value();
				}


				void operator () ( if_type& interface_ )
				{
					m_out.bind( interface_ );
				}
				

				void operator () ( ::sc_core::sc_inout<bool>& parent_ )
				{
					m_out.bind( parent_ );
				}
				
				void trace( ::sc_core::sc_trace_file* tf ) const
				{
					::sc_core::sc_deprecated_trace();
				#       ifdef DEBUG_SYSTEMC
						::sc_core::sc_trace( tf, read(), name() ); 
				#       else
							if ( tf ) {}
				#       endif
				}


				virtual void print( ::std::ostream& = ::std::cout ) const;
				virtual void dump( ::std::ostream& = ::std::cout ) const;

				virtual const char* kind() const
				{
					return "sc_delayed_signal";
				}

			protected:
				
				// ez az uj write fv!!
				//void action() override;
				
				virtual void update();
						void do_update();

				virtual bool is_clock() const
				{
					return false;
				}
				
			public:
				::sc_core::sc_signal<bool,POL>   	new_value;

			protected:
				::sc_core::sc_signal<bool,POL>   	m_value;
				//std::queue<bool>	   				m_queue;
				::sc_core::sc_signal<bool,POL>		m_queue;
			public:
				::sc_core::sc_out<bool>						m_out;
				
			private:

				// reset creation
				virtual ::sc_core::sc_reset* is_reset() const;

				// disabled
				sc_delayed_signal( const this_type& );
		};
		
		
		
		template< ::sc_core::sc_writer_policy POL >
		class sc_delayed_signal<sc_dt::sc_logic,POL>	:	public    ::sc_core::sc_signal_inout_if<sc_dt::sc_logic>, 
															public    delay_t,
															protected ::sc_core::sc_writer_policy_check<POL>
		{
			protected:
				typedef ::sc_core::sc_signal_inout_if<sc_dt::sc_logic> 	if_type;
				typedef sc_delayed_signal<sc_dt::sc_logic,POL>			this_type;
				typedef ::sc_core::sc_writer_policy_check<POL>         	policy_type;

			public: // constructors and destructor:
				
				SC_HAS_PROCESS(sc_delayed_signal);
				
				sc_delayed_signal()
				: delay_t( ::sc_core::sc_gen_unique_name( "delayed_signal" ), [&,this]()->void{m_value = m_queue; m_out = m_queue;}),
				  m_value(::sc_core::sc_gen_unique_name("delayed_signal_value")),
				  m_queue(),
				  m_out(::sc_core::sc_gen_unique_name("out"))
				{}

				explicit sc_delayed_signal( const char* name_)
				: delay_t( name_, [&,this]()->void{m_value = m_queue; m_out = m_queue;}),
				  m_value(std::string(std::string(name_) + "delayed_signal_value").c_str()), 
				  m_queue(),
				  m_out(std::string(std::string(name_) + "out").c_str())
				{}
				
				sc_delayed_signal( const char* name_, sc_dt::sc_logic initial_value_)
				: delay_t( name_ , [&,this]()->void{m_value = m_queue; m_out = m_queue;}),
				  m_value(std::string(std::string(name_) + "delayed_signal_value").c_str(), initial_value_),
				  m_queue(),
				  m_out(std::string(std::string(name_) + "out").c_str())
				{}

				virtual ~sc_delayed_signal()
				{}


				// interface methods

				virtual void register_port( ::sc_core::sc_port_base&, const char* );

				virtual ::sc_core::sc_writer_policy get_writer_policy() const
				{
					return POL;
				}

				// get the default event
				virtual const ::sc_core::sc_event& default_event() const
				{
					return m_value.default_event();
				}

				// get the value changed event
				virtual const ::sc_core::sc_event& value_changed_event() const;

				// get the positive edge event
				virtual const ::sc_core::sc_event& posedge_event() const;

				// get the negative edge event
				virtual const ::sc_core::sc_event& negedge_event() const;


				// read the current value
				virtual const sc_dt::sc_logic& read() const
				{
					return m_value.read();
				}

				// get a reference to the current value (for tracing)
				virtual const sc_dt::sc_logic& get_data_ref() const
				{
					return m_value.get_data_ref();
				}


				// was there an event?
				virtual bool event() const
				{
					return m_value.event();
				}

				// was there a positive edge event?
				virtual bool posedge() const
				{
					return m_value.posedge();
				}

				// was there a negative edge event?
				virtual bool negedge() const
				{
					return m_value.negedge();
				}


				// write the new value
				virtual void write( const sc_dt::sc_logic& );

				void initialize(const sc_dt::sc_logic& init_value)
				{
					m_value.write(init_value);//na milesz??
					m_out.initialize(init_value);
				}

				// other methods

				operator const sc_dt::sc_logic& () const
				{
					return m_value.read();
				}


				this_type& operator = ( const sc_dt::sc_logic& a )
				{
					write( a );
					return *this;
				}

				this_type& operator = ( const ::sc_core::sc_signal_in_if<sc_dt::sc_logic>& a )
				{
					write( a.read() );
					return *this;
				}

				this_type& operator = (const this_type& a)
				{
					write( a.read() ); return *this;
				}


				const sc_dt::sc_logic& get_new_value() const
				{
					return m_value.get_new_value();
				}				

				void operator () ( if_type& interface_ )
				{
					m_out.bind( interface_ );
				}
				
				void operator () ( ::sc_core::sc_inout<sc_dt::sc_logic>& parent_ )
				{
					m_out.bind( parent_ );
				}
				
				
				void trace( ::sc_core::sc_trace_file* tf ) const
				{
					::sc_core::sc_deprecated_trace();
				#       ifdef DEBUG_SYSTEMC
						::sc_core::sc_trace( tf, read(), name() ); 
				#       else
							if ( tf ) {}
				#       endif
				}

				virtual void print( ::std::ostream& = ::std::cout ) const;
				virtual void dump( ::std::ostream& = ::std::cout ) const;

				virtual const char* kind() const
				{
					return "sc_delayed_signal";
				}

			protected:
				
				// ez az uj write fv!!
				//void action() override;
				
				virtual void update();
						void do_update();

			public:
				::sc_core::sc_signal<sc_dt::sc_logic,POL>   new_value;
				
			protected:
				::sc_core::sc_signal<sc_dt::sc_logic,POL>	m_value;
				//std::queue<sc_dt::sc_logic>					m_queue;
				::sc_core::sc_signal<sc_dt::sc_logic,POL>	m_queue;
			public:
				sc_core::sc_out<sc_dt::sc_logic>			m_out;
				
			private:

				// disabled
				sc_delayed_signal( const this_type& );
		};
		
		
		template< typename T, ::sc_core::sc_writer_policy POL >
		inline
		::std::ostream&
		operator << ( ::std::ostream& os, const sc_delayed_signal<T,POL>& a )
		{
			return ( os << a.read() );
		}
	} //logitherm
} //sc_core



#endif //_DELAYED_SIGNAL_H_
