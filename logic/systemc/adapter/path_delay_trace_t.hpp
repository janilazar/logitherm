#ifndef _SC_PATH_DELAY_TRACE_
#define _SC_PATH_DELAY_TRACE_

#include "util/trace_t.hpp"
#include "logic/systemc/adapter/delay_t.hpp"
#include <vector>

namespace util
{
	class timestep_t;
}

namespace logic
{
	namespace systemc
	{
		class adapter_t;

		class path_delay_trace_t	:	public util::trace_t
		{
			private:
				std::vector<delay_t*> critical_path;
				adapter_t* adapter;
				const util::timestep_t& timestep;

			public:
				const std::string id; //path neve
				
			public:
				
				path_delay_trace_t(const std::string& path, const std::string& path_id, const std::string& postfix, adapter_t* adapter);
				
				void initialize(std::ostream& os) override;
				
				void trace(std::ostream& os) override;
				
				void reset();
				
				void finalize(std::ostream& os) override;
				
				void add_delay(delay_t* signal);
		};
	} //namespace systemc
} //namespace logic

#endif //_SC_PATH_DELAY_TRACE_
