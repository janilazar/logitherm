#ifndef _EVENT_MODULE_H_
#define _EVENT_MODULE_H_

#include <functional>

#include "logic/systemc/digital/src/sysc/kernel/sc_module.h"
#include "logic/systemc/digital/src/sysc/kernel/sc_event.h"

namespace logic
{
	namespace systemc
	{
		
		class trigger_module_t : public sc_core::sc_module
		{
			private:
				sc_core::sc_event 	m_module_event;
				std::function<void()>	func;
				
			public:
				/**
				 * ez a makro egy typedef trigger_module_t valamit takar
				**/ 
				SC_HAS_PROCESS(trigger_module_t);
				
				/**
				 * default konstruktor
				**/ 
				trigger_module_t(const std::function<void()>& f, logic::systemc::adapter_t* adapter = nullptr)
				:
					sc_core::sc_module(adapter),
					func(f)
				{
					SC_METHOD(action);
					sensitive << m_module_event;
					dont_initialize();
				}
				
				/**
				 * nev alapjan inicializalo konstruktor
				**/ 
				trigger_module_t(const sc_core::sc_module_name& nm, const std::function<void()>& f, logic::systemc::adapter_t* adapter = nullptr) /* for those used to old style */
				:
					sc_core::sc_module( nm, adapter),
					func(f)
				{
					SC_METHOD(action);
					sensitive << m_module_event;
					dont_initialize();
				}
				
				/**
				 * kind fv override-olasa, szabvany szerint kotelezo
				**/ 
				const char* kind() const override
				{
					return "trigger_module_t";
				}
				
				/**
				 * miafasz
				**/ 
				void notify (double when, ::sc_core::sc_time_unit base)
				{
					m_module_event.notify(::sc_core::sc_time(when, base));
				}
				
				/**
				 * miafasz
				**/ 
				void notify (const ::sc_core::sc_time& when)
				{
					m_module_event.notify(when); //miert nem volt jo sima notify-al?				
				}
				
				/**
				 * ez a fv hivodik meg, ha esemeny tortenik
				 * leszarmazottban kotelezo implementalni
				**/
				void action()
				{
					func();
				}

		};

		//extern ::sc_core::sc_module* ::sc_core::sc_module_dynalloc(::sc_core::sc_module*);			//ez nem tudom, mire valo
		//#define SC_NEW(x)  ::sc_core::sc_module_dynalloc(new x);	//ez nem tudom, mire valo
		
	} //namespace logitherm
} //namespace sc_core

#endif // _EVENT_MODULE_H_

