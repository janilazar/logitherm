#include "logic/systemc/adapter/value_trace_t.hpp"
#include "logic/systemc/adapter/adapter_t.hpp"
#include "util/string_manipulation.hpp"
#include "util/timestep_t.hpp"

namespace logic
{
	namespace systemc
	{
				
		value_trace_t::value_trace_t(const std::string& path, const std::string& id, const std::string& postfix, sc_core::sc_signal<T>* signal, adapter_t* adapter)
		:
			util::trace_t(path, id, "value", postfix),
			interface(signal),
			adapter(adapter)
		{}

		value_trace_t::value_trace_t(const std::string& path, const std::string& id, const std::string& postfix, sca_tdf::sca_signal<T>* signal, adapter_t* adapter)
		:
			util::trace_t(path, id, "value", postfix),
			interface(signal),
			adapter(adapter)
		{}

		void value_trace_t::initialize(std::ostream& os)
		{
			os << "timestamp, " << id << "_" << type;
		}

		void value_trace_t::trace(std::ostream& os)
		{

			os << util::to_string(timestep.current_time()) << ", ";

			const sc_core::sc_signal<T>* sc_signal_ptr = dynamic_cast<sc_core::sc_signal<T>*>(interface);
			if (sc_signal_ptr != nullptr)
			{
				os << sc_signal_ptr->read();
			}
			else
			{
				const sca_tdf::sca_signal<T>* sca_tdf_signal_ptr = dynamic_cast<const sca_tdf::sca_signal<T>*>(interface);
				if(sca_tdf_signal_ptr != nullptr)
				{
					os << sca_tdf_signal_ptr->get_typed_trace_value();
				}
			}
		}

		void value_trace_t::reset()
		{}
		
		void value_trace_t::finalize(std::ostream& os)
		{}
	} //namespace systemc
} //namespace logic

#endif //_VALUE_TRACE_T_
