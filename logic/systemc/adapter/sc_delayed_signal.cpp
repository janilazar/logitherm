/*****************************************************************************

  The following code is derived, directly or indirectly, from the SystemC
  source code Copyright (c) 1996-2014 by all Contributors.
  All Rights reserved.

  The contents of this file are subject to the restrictions and limitations
  set forth in the SystemC Open Source License (the "License");
  You may not use this file except in compliance with such restrictions and
  limitations. You may obtain instructions on how to receive a copy of the
  License at http://www.accellera.org/. Software distributed by Contributors
  under the License is distributed on an "AS IS" basis, WITHOUT WARRANTY OF
  ANY KIND, either express or implied. See the License for the specific
  language governing rights and limitations under the License.

 *****************************************************************************/

/*****************************************************************************

  sc_signal.cpp -- The sc_signal<T> primitive channel class.

  Original Author: Martin Janssen, Synopsys, Inc., 2001-05-21

  CHANGE LOG IS AT THE END OF THE FILE
 *****************************************************************************/


#include "logic/systemc/digital/src/sysc/communication/sc_communication_ids.h"
#include "logic/systemc/digital/src/sysc/utils/sc_utils_ids.h"
#include "logic/systemc/adapter/sc_delayed_signal.hpp"
#include "logic/systemc/digital/src/sysc/datatypes/int/sc_signed.h"
#include "logic/systemc/digital/src/sysc/datatypes/int/sc_unsigned.h"
#include "logic/systemc/digital/src/sysc/datatypes/bit/sc_lv_base.h"
#include "logic/systemc/digital/src/sysc/kernel/sc_reset.h"

#include <sstream>
#include <iostream>

using sc_dt::sc_lv_base;
using sc_dt::sc_signed;
using sc_dt::sc_unsigned;
using sc_dt::int64;
using sc_dt::uint64;

namespace logic
{
	namespace systemc
	{

		template< ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<bool,POL>::register_port( ::sc_core::sc_port_base& port_, const char* if_typename_ )
		{
			bool is_output = std::string( if_typename_ ) == typeid(if_type).name();
			if( !policy_type::check_port( this, &port_, is_output ) )
			   ((void)0); // fallback? error has been suppressed ...
		}

		//template<::sc_core::sc_writer_policy POL >
		//void sc_delayed_signal<bool,POL>::action()
		//{
		//	//::sc_core::sc_time current_time = ::sc_core::sc_time_stamp();
		//	m_value = m_queue;
		//	m_out = m_queue;
		//	//m_queue.pop();	
		//	
		//}

		// write the new value

		template< ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<bool,POL>::write( const bool& value_ )
		{
			new_value = value_;
			//std::cout << name() << " zero_delay " << this->zero_delay() << std::endl;
			if(this->zero_delay())
			{
				m_value = value_;
				m_out = value_;
			}
			else
			{
				//m_queue.push(value_);
				m_queue = value_;
				this->notify(get_delay());
				
			}
		}

		template< ::sc_core::sc_writer_policy POL >
		inline void sc_delayed_signal<bool,POL>::print( ::std::ostream& os ) const
		{
			m_value.print(os);
		}

		template< ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<bool,POL>::dump( ::std::ostream& os ) const
		{
			m_value.dump(os);
		}


		template< ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<bool,POL>::update()
		{
			m_value.update();
		}

		template< ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<bool,POL>::do_update()
		{
			m_value.do_update();
		}

		// (edge) event methods

		template< ::sc_core::sc_writer_policy POL >
		const ::sc_core::sc_event&	sc_delayed_signal<bool,POL>::value_changed_event() const
		{
			return m_value.value_changed_event();
		}

		template< ::sc_core::sc_writer_policy POL >
		const ::sc_core::sc_event&	sc_delayed_signal<bool,POL>::posedge_event() const
		{
			return m_value.posedge_event();
		}

		template< ::sc_core::sc_writer_policy POL >
		const ::sc_core::sc_event&	sc_delayed_signal<bool,POL>::negedge_event() const
		{
			return m_value.negedge_event();
		}


		// reset support:

		template< ::sc_core::sc_writer_policy POL >
		::sc_core::sc_reset* sc_delayed_signal<bool,POL>::is_reset() const
		{
			return m_value.is_reset();
		}

		// destructor

		template< ::sc_core::sc_writer_policy POL >
		sc_delayed_signal<bool,POL>::~sc_delayed_signal()
		{}

		// IIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIIII
		
		
		//template<::sc_core::sc_writer_policy POL >
		//void sc_delayed_signal<sc_dt::sc_logic,POL>::action()
		//{
		//	//::sc_core::sc_time current_time = ::sc_core::sc_time_stamp();
		//	m_value = m_queue;
		//	m_out = m_queue;
		//}

		template< ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<sc_dt::sc_logic,POL>::register_port( ::sc_core::sc_port_base& port_,
													   const char* if_typename_ )
		{
			bool is_output = std::string( if_typename_ ) == typeid(if_type).name();
			if( !policy_type::check_port( this, &port_, is_output ) )
			   ((void)0); // fallback? error has been suppressed ...
		}


		// write the new value

		template< ::sc_core::sc_writer_policy POL >
		inline void sc_delayed_signal<sc_dt::sc_logic,POL>::write( const sc_dt::sc_logic& value_ )
		{
			
			new_value = value_;
			//std::cout << name() << " zero_delay " << this->zero_delay() << std::endl;
			if(this->zero_delay())
			{
				m_value = value_;
				m_out = value_;
			}
			else
			{
				m_queue = value_;
				this->notify(get_delay());
			}
		}

		template< ::sc_core::sc_writer_policy POL >
		inline void sc_delayed_signal<sc_dt::sc_logic,POL>::print( ::std::ostream& os ) const
		{
			m_value.print(os);
		}

		template< ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<sc_dt::sc_logic,POL>::dump( ::std::ostream& os ) const
		{
		   m_value.dump(os);
		}


		template< ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<sc_dt::sc_logic,POL>::update()
		{
			m_value.update();
		}

		template< ::sc_core::sc_writer_policy POL >
		void sc_delayed_signal<sc_dt::sc_logic,POL>::do_update()
		{
		   m_value.do_update();
		}

		// (edge) event methods

		template< ::sc_core::sc_writer_policy POL >
		const ::sc_core::sc_event& sc_delayed_signal<sc_dt::sc_logic,POL>::value_changed_event() const
		{
			return m_value.value_changed_event();
		}

		template< ::sc_core::sc_writer_policy POL >
		const ::sc_core::sc_event& sc_delayed_signal<sc_dt::sc_logic,POL>::posedge_event() const
		{
			return m_value.posedge_event();
		}

		template< ::sc_core::sc_writer_policy POL >
		const ::sc_core::sc_event& sc_delayed_signal<sc_dt::sc_logic,POL>::negedge_event() const
		{
			return m_value.negedge_event();
		}


		// template instantiations for writer policies

		template class sc_delayed_signal<bool,::sc_core::SC_ONE_WRITER>;
		template class sc_delayed_signal<bool,::sc_core::SC_MANY_WRITERS>;
		template class sc_delayed_signal<bool,::sc_core::SC_UNCHECKED_WRITERS>;

		template class sc_delayed_signal<sc_dt::sc_logic,::sc_core::SC_ONE_WRITER>;
		template class sc_delayed_signal<sc_dt::sc_logic,::sc_core::SC_MANY_WRITERS>;
		template class sc_delayed_signal<sc_dt::sc_logic,::sc_core::SC_UNCHECKED_WRITERS>;
	} // namespace logitherm
} // namespace sc_core

