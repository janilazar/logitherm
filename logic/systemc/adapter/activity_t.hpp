#ifndef _SYSTEMC_ACTIVITY_
#define _SYSTEMC_ACTIVITY_

#include <functional>
#include "logic/base/activity_t.hpp"
#include "logic/systemc/digital/src/systemc.h"
#include "manager/manager_t.hpp"

namespace logic
{
	namespace systemc
	{	
		class activity_t	:	public logic::activity_t,
								public sc_core::sc_process_host
		{
			private:
				sc_core::sc_time time;
				
			public:	
				
				/**
				 * default konstruktor
				**/ 
				activity_t() = delete;
				
				/**
				 * ez a konstruktor kell a push_back-nek
				 * (sc_dissipator_module-ban az activities vector egy ilyen tipust tartalmaz)
				**/ 
				activity_t(const std::string& id, const std::function<::unit::energy_t()> &f)
				:
					logic::activity_t(id,f),
					time(sc_core::SC_ZERO_TIME)
				{}
				
				void activity() override
				{

					// figyelem, itt hibat okozhat az, hogy glitch-re is szamolunk disszipaciot!
					// egy oda-vissza elvaltasnal nem kellene diszipaciot szamolni, de most szamitunk
					// mivel fel es lefutora kulon activity-t csinaltam, igy nem trivialis a megoldas...
					sc_core::sc_time current_time = sc_core::sc_time_stamp();
					if(current_time > time)
					{
						energy += energy_function();
						time = current_time;
					}
				}
				
				~activity_t(){}
		};
	} //namespace logitherm
} //namespace sc_core

#endif //_SYSTEMC_ACTIVITY_
