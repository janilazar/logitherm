#include "logic/systemc/adapter/path_delay_trace_t.hpp"
#include "logic/systemc/digital/src/sysc/kernel/sc_time.h"
#include "logic/systemc/adapter/adapter_t.hpp"
#include "util/string_manipulation.hpp"
#include "util/timestep_t.hpp"

namespace logic
{
	namespace systemc
	{

		path_delay_trace_t::path_delay_trace_t(const std::string& file_path, const std::string& path_id, const std::string& postfix, adapter_t* adapter)
		:
			util::trace_t(file_path, path_id, "path_delay", postfix),
			adapter(adapter),
			timestep(adapter->timestep),
			id(path_id)
		{}


		void path_delay_trace_t::initialize(std::ostream& os)
		{
			os << "timestamp, "<< id << "_" << type;
		}

		void path_delay_trace_t::trace(std::ostream& os)
		{
			// double total_delay = 0.0;
			sc_core::sc_time total_delay = sc_core::SC_ZERO_TIME;
			for(auto &it: critical_path)
			{
				total_delay += it->get_delay();
			}
			// double miafasz = total_delay.to_seconds();
			// double miafasz = 1.23e4;
			// os << util::to_string(timestep.current_time()) << ", " << "miafaszomvanmar";
			os << util::to_string(timestep.current_time()) << ", " << util::to_string(total_delay.to_seconds());
		}

		void path_delay_trace_t::reset()
		{}
		
		void path_delay_trace_t::finalize(std::ostream& os)
		{}

		void path_delay_trace_t::add_delay(delay_t* signal)
		{
			bool found = false;
			for(auto &it: critical_path)
			{
				if(it->name() == signal->name())
				{
					found = true;
					break;
				}
			}
			if(found)
			{
				std::cerr << "Warning, signal '" << signal->name() << "' has been already added to '" << id << "'." << std::endl;
			}
			else
			{
				critical_path.push_back(signal);
			}
		}
		
	} //namespace systemc
} //namespace logic
