#ifndef _VALUE_TRACE_T_
#define _VALUE_TRACE_T_

#include "util/trace_t.hpp"
#include "logic/systemc/adapter/adapter_t.hpp"
#include "util/string_manipulation.hpp"
#include "util/timestep_t.hpp"
namespace sc_core
{
	template< class T, sc_writer_policy POL /* = SC_DEFAULT_WRITER_POLICY */ >
	class sc_signal;
}

namespace sca_tdf
{
	template<typename T>
	class sca_signal;
}

namespace logic
{
	namespace systemc
	{

		// class adapter_t;

		template<typename T>
		class value_trace_t	:	public util::trace_t
		{
			private:
				sc_core::sc_interface* interface;
				logic::adapter_t* adapter;
				const util::timestep_t& timestep;
				
			public:				
				value_trace_t(const std::string& path, const std::string& id, const std::string& postfix, sc_core::sc_signal<T>* signal, logic::adapter_t* adapter)
				:
					util::trace_t(path, id, "value", postfix),
					interface(signal),
					adapter(adapter),
					timestep(adapter->timestep)
				{}

				value_trace_t(const std::string& path, const std::string& id, const std::string& postfix, sca_tdf::sca_signal<T>* signal, logic::adapter_t* adapter)
				:
					util::trace_t(path, id, "value", postfix),
					interface(signal),
					adapter(adapter),
					timestep(adapter->timestep)
				{}

				void initialize(std::ostream& os)
				{
					os << "timestamp, " << id << "_" << type;
				}

				void trace(std::ostream& os)
				{

					os << util::to_string(timestep.current_time()) << ", ";

					const sc_core::sc_signal<T>* sc_signal_ptr = dynamic_cast<sc_core::sc_signal<T>*>(interface);
					if (sc_signal_ptr != nullptr)
					{
						os << sc_signal_ptr->read();
					}
					else
					{
						const sca_tdf::sca_signal<T>* sca_tdf_signal_ptr = dynamic_cast<const sca_tdf::sca_signal<T>*>(interface);
						if(sca_tdf_signal_ptr != nullptr)
						{
							os << sca_tdf_signal_ptr->get_typed_trace_value();
						}
					}
				}

				void reset()
				{}
				
				void finalize(std::ostream& os)
				{}
		};
	} //namespace systemc
} //namespace logic

#endif //_VALUE_TRACE_T_
