#ifndef _SC_DELAY_
#define _SC_DELAY_

#include "logic/systemc/adapter/trigger_module_t.hpp"
#include <functional>

namespace logic
{
	namespace systemc
	{
		class delay_t	:	public trigger_module_t
		{
			private:
				std::function<::sc_core::sc_time()>	delay_function; 
				bool								delay_function_set;
				
			private:
						
			public:
				delay_t(const std::function<void()>& f)
				:
					trigger_module_t(::sc_core::sc_gen_unique_name( "delay_t" ), f),
					delay_function_set(false)
				{}
				
				delay_t(const char* name_, const std::function<void()>& f)
				:
					trigger_module_t(name_, f),
					delay_function_set(false)
				{}
			
				void set_delay_function(const std::function<sc_core::sc_time()> &func)
				{
					delay_function = func;
					delay_function_set = true;
				}
				
				sc_core::sc_time get_delay()
				{
					if(delay_function_set) return delay_function();
					else return sc_core::SC_ZERO_TIME;
				}
				
				bool zero_delay()
				{
					return !delay_function_set;
				}
			
			
			
		};
	}
}

#endif //_SC_DELAY_
