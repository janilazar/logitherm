#ifndef _SNIPER_COMPONENT_
#define _SNIPER_COMPONENT_

#include "logic/base/component_t.hpp"

#include "unit/frequency_t.hpp"
#include "logic/sniper/adapter/frequency_trace_t.hpp"

namespace logitherm
{
	class maanger_t;
}

namespace logic
{
	class adapter_t;
}

namespace logic
{
	namespace sniper
	{
		class component_t	:	public logic::component_t
		{
			private:
				unit::power_t static_power;
				unit::power_t dynamic_power;
				unit::frequency_t current_frequency;
			
			public:
				component_t() = delete;
				component_t(logic::adapter_t* adapter, const std::string& id, component_t* parent);
				
				void set_static_dissipation(unit::power_t power);
				void set_dynamic_dissipation(unit::power_t power);
				void set_current_frequency(unit::frequency_t frequency);
				
				unit::power_t component_static_dissipation() const override;
				unit::power_t component_dynamic_dissipation() const override;
				unit::frequency_t component_frequency() const;

				frequency_trace_t* add_frequency_trace(const std::string& path, const std::string& postfix="");
		};
	}
}

#endif //_SNIPER_COMPONENT_
