#ifndef _SNIPER_ADAPTER_
#define _SNIPER_ADAPTER_

#include "logic/base/adapter_t.hpp"

namespace logic
{
	namespace sniper
	{
		class adapter_t	:	public	::logic::adapter_t
		{
			private:
				size_t num_cores;
			
			public:
				
				/**
				 * letrehozza a logic components tree-t
				**/ 
				void create_components(size_t num_cores); // <- ez hozza letre a logic component tree-t
				//void delete_components_tree();
				
				adapter_t() = delete;
				adapter_t(logitherm::manager_t* manager, util::log_t* log);
				~adapter_t();
			
		};
	}
}

//valahol itt kellene implementalni azokat a fv-eket, ami majd hivhato lesz pythonbol
//kell majd init-re,


#endif //_SNIPER_ADAPTER_
