#include "logic/sniper/adapter/component_t.hpp"
#include "logic/sniper/adapter/adapter_t.hpp"
#include "manager/manager_t.hpp"

namespace logic
{
	namespace sniper
	{
		
		component_t::component_t(logic::adapter_t* adapter, const std::string& id, component_t* parent)
		:
			logic::component_t(adapter, id, parent),
			static_power(0_W),
			dynamic_power(0_W),
			current_frequency(0_MHz)
		{}
		
		/**
		 * ezt a fuggvenyt a sniper python interfeszebol kell meghivni
		**/ 
		void component_t::set_static_dissipation(unit::power_t power)
		{
			if(std::isnan(static_cast<double>(power))) 
			{
				std::cout << "#2 set_static_dissipation(): " << "static_power is nan, " << power << std::endl;
			}

			static_power = power;
		}
		
		/**
		 * ezt a fuggvenyt a sniper python interfeszebol kell meghivni
		**/
		void component_t::set_dynamic_dissipation(unit::power_t power)
		{
			dynamic_power = power;
		}

		/**
		 * ezt a fuggvenyt a sniper python interfeszebol kell meghivni
		**/
		void component_t::set_current_frequency(unit::frequency_t frequency)
		{
			current_frequency = frequency;
		}
		
		unit::power_t component_t::component_static_dissipation() const
		{
			return static_power;
			// return 0_W;
		}
		
		unit::power_t component_t::component_dynamic_dissipation() const
		{
			return dynamic_power;
			// return 0_W;
		}

		unit::frequency_t component_t::component_frequency() const
		{
			return current_frequency;
		}

		frequency_trace_t* component_t::add_frequency_trace(const std::string& path, const std::string& postfix)
		{
			frequency_trace_t* ptr = new frequency_trace_t(path, postfix, this);
			tracer.add_trace(ptr);
			trace_component();
			return ptr;
		}
	}
}
