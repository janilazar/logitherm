#include "logic/base/adapter_t.hpp"
#include "logic/sniper/adapter/component_t.hpp"
#include "logic/sniper/adapter/frequency_trace_t.hpp"
#include "util/string_manipulation.hpp"

namespace logic
{	
	namespace sniper
	{
		frequency_trace_t::frequency_trace_t(const std::string& path, const std::string& postfix, component_t* component)
		:
			util::trace_t(path, component->id, "frequency", postfix),
			component(component),
			timestep(component->adapter->timestep)
		{}


		void frequency_trace_t::initialize(std::ostream& os)
		{
			os << "timestamp, "<< id << "_" << type;
		}

		void frequency_trace_t::trace(std::ostream& os)
		{
			os << util::to_string(timestep.current_time()) << ", " << component->component_frequency();
		}

		void frequency_trace_t::reset()
		{
			component->reset();
		}
		
		void frequency_trace_t::finalize(std::ostream& os)
		{}
	}
}
