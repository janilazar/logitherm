#include "logic/sniper/adapter/adapter_t.hpp"
#include "logic/sniper/adapter/component_t.hpp"

#include "logic/sniper/adapter/adapter_t.hpp"
#include "logic/sniper/adapter/component_t.hpp"
#include "thermal/sunred/adapter/layout/component_t.hpp"
//#include "thermal/sloth/adapter/adapter_t.hpp"
#include "manager/manager_t.hpp"
#include "util/log_t.hpp"

/* undef some macros to avoid redefined warnings */
#undef _POSIX_C_SOURCE
#undef _XOPEN_SOURCE
#include "Python.h"

namespace logic
{
	namespace sniper
	{
		
		void adapter_t::create_components(size_t cores)
		{
			std::cout << "miafaszvan" << std::endl;

			num_cores = cores;
			component_t* processor = new component_t(this,"processor",nullptr);
			for(size_t coreid = 0; coreid < num_cores; ++coreid)
			{
				component_t* core_ptr = new component_t(this, "core_" + std::to_string(coreid),processor);
				
				component_t* instruction_fetch_unit_ptr = new component_t(this, "instruction_fetch_unit_" + std::to_string(coreid), core_ptr);
				new component_t(this, "instruction_buffer_" + std::to_string(coreid), instruction_fetch_unit_ptr);
				new component_t(this, "instruction_cache_" + std::to_string(coreid), instruction_fetch_unit_ptr);
				
				component_t*  branch_predictor_ptr = new component_t(this, "branch_predictor_" + std::to_string(coreid), core_ptr);
				new component_t(this, "branch_target_buffer_" + std::to_string(coreid), branch_predictor_ptr);
				new component_t(this, "chooser_" + std::to_string(coreid), branch_predictor_ptr);
				new component_t(this, "global_predictor_" + std::to_string(coreid), branch_predictor_ptr);
				new component_t(this, "l1_local_predictor_" + std::to_string(coreid), branch_predictor_ptr);
				new component_t(this, "l2_local_predictor_" + std::to_string(coreid), branch_predictor_ptr);
				new component_t(this, "ras_" + std::to_string(coreid), branch_predictor_ptr);

				component_t* paging_ptr = new component_t(this, "paging_" + std::to_string(coreid), core_ptr); //paging??
				new component_t(this, "dtlb_" + std::to_string(coreid), paging_ptr);
				new component_t(this, "itlb_" + std::to_string(coreid), paging_ptr);

				new component_t(this, "l2_" + std::to_string(coreid), core_ptr);

				component_t* instruction_decode_ptr = new component_t(this, "instruction_decode_" + std::to_string(coreid), core_ptr);
				new component_t(this, "instruction_decoder_" + std::to_string(coreid), instruction_decode_ptr);
				new component_t(this, "fp_front_end_rat_" + std::to_string(coreid), instruction_decode_ptr);
				new component_t(this, "free_list_" + std::to_string(coreid), instruction_decode_ptr);
				new component_t(this, "int_front_end_rat_" + std::to_string(coreid), instruction_decode_ptr);

				component_t* memory_order_execution_ptr = new component_t(this, "memory_order_execution_" + std::to_string(coreid), core_ptr);
				new component_t(this, "loadq_" + std::to_string(coreid), memory_order_execution_ptr); //mem order&exec??
				new component_t(this, "storeq_" + std::to_string(coreid), memory_order_execution_ptr);

				new component_t(this, "l1_data_cache_" + std::to_string(coreid), core_ptr);
				
				component_t* instruction_scheduler_ptr = new component_t(this, "instruction_scheduler_" + std::to_string(coreid), core_ptr);
				new component_t(this, "fp_instruction_window_" + std::to_string(coreid), instruction_scheduler_ptr);
				new component_t(this, "instruction_window_" + std::to_string(coreid), instruction_scheduler_ptr);
				new component_t(this, "rob_" + std::to_string(coreid), instruction_scheduler_ptr);
				
				
				
				
				component_t* execution_unit_ptr = new component_t(this, "execution_unit_" + std::to_string(coreid), core_ptr);
				new component_t(this, "complex_alus_" + std::to_string(coreid), execution_unit_ptr);
				new component_t(this, "floating_point_units_" + std::to_string(coreid), execution_unit_ptr);
				new component_t(this, "integer_alus_" +  std::to_string(coreid), execution_unit_ptr);
				component_t* register_files_ptr = new component_t(this, "register_files_" + std::to_string(coreid), execution_unit_ptr);
				new component_t(this, "floating_point_rf_" + std::to_string(coreid), register_files_ptr);
				new component_t(this, "integer_rf_" + std::to_string(coreid), register_files_ptr);
				new component_t(this, "results_broadcast_bus_" + std::to_string(coreid), execution_unit_ptr);
				
				
			}
			
			new component_t(this, "l3", processor);
			new component_t(this, "dram", processor);

			std::cout << "miafaszvan" << std::endl;
		}
	
		adapter_t::adapter_t(logitherm::manager_t* manager, util::log_t* log)
		:
			logic::adapter_t(manager, log, "logic::sniper::adapter_t", util::logic_engine_t::sniper, manager->get_timestep())
		{
			std::cout <<"adapter() konstruktor" << std::endl;
		}
		
		adapter_t::~adapter_t()
		{
			delete_components_tree();
		}
	} //namespace sniper
} //namespace logic

static logitherm::manager_t* manager_ptr = nullptr;
static thermal::adapter_t* thermal_engine_ptr = nullptr;
static util::thermal_engine_t thermal_engine_type;
static logic::sniper::adapter_t* logic_engine_ptr = nullptr;

/**
 * beallitja a logikai komponenseket, layout komponenseket, es az idolepteket
 * argumentumkent meg kell kapnia az idolepteket
**/ 
static PyObject* initialize_logitherm(PyObject * self, PyObject *args)
{
	try
	{
		if(nullptr == manager_ptr) manager_ptr = logitherm::manager_t::get_manager();
		if(nullptr == logic_engine_ptr) logic_engine_ptr = dynamic_cast<logic::sniper::adapter_t*>(manager_ptr->set_logic_engine(util::logic_engine_t::sniper));

		int cores;
		if (!PyArg_ParseTuple(args, "i", &cores)) throw("initialize_logitherm(): parsing arguments failed");

		size_t num_cores = cores;
		
		/* build the structure of the logic components */
		logic_engine_ptr->create_components(num_cores);		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "init_framework(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * letrehoz egy min_temperature_trace-
**/ 
static PyObject* add_min_temperature_trace(PyObject * self, PyObject *args)
{
	try
	{
		if(thermal_engine_ptr == nullptr) throw("add_min_temperature_trace(): thermal engine is nullptr, it must be initialized first with 'set_thermal_engine()'");
		
		layout::adapter_t* layout_ptr = thermal_engine_ptr->get_layout();

		if(layout_ptr == nullptr) throw("add_min_temperature_trace(): layout is nullptr, layout must be initialized first");


		const char* py_id;
		const char* py_path;
		
		if (!PyArg_ParseTuple(args, "ss", &py_id, &py_path)) throw("add_min_temperature_trace(): parsing arguments failed");
		
		std::string id = py_id; // kell ez ide??
		std::string path = py_path;

		/* find the component */
		if(layout_ptr->search_for_component(id))
		{
			layout_ptr->get_component(id)->add_min_temperature_trace(path);
		}
		else
		{
			layout_ptr->warning("add_min_temperature_trace(): layout::component_t '" + id + "' was not found");
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "add_min_temperature_trace(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * letrehoz egy min_temperature_trace-
**/ 
static PyObject* add_max_temperature_trace(PyObject * self, PyObject *args)
{
	try
	{
		if(thermal_engine_ptr == nullptr) throw("add_max_temperature_trace(): thermal engine is nullptr, it must be initialized first with 'set_thermal_engine()'");
		
		layout::adapter_t* layout_ptr = thermal_engine_ptr->get_layout();

		if(layout_ptr == nullptr) throw("add_max_temperature_trace(): layout is nullptr, layout must be initialized first");


		const char* py_id;
		const char* py_path;
		
		if (!PyArg_ParseTuple(args, "ss", &py_id, &py_path)) throw("add_max_temperature_trace(): parsing arguments failed");
		
		std::string id = py_id; // kell ez ide??
		std::string path = py_path;

		/* find the component */
		if(layout_ptr->search_for_component(id))
		{
			layout_ptr->get_component(id)->add_max_temperature_trace(path);
		}
		else
		{
			layout_ptr->warning("add_max_temperature_trace(): layout::component_t '" + id + "' was not found");
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "add_max_temperature_trace(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * letrehoz egy min_temperature_trace-
**/ 
static PyObject* add_avg_temperature_trace(PyObject * self, PyObject *args)
{
	try
	{
		if(thermal_engine_ptr == nullptr) throw("add_avg_temperature_trace(): thermal engine is nullptr, it must be initialized first with 'set_thermal_engine()'");
		
		layout::adapter_t* layout_ptr = thermal_engine_ptr->get_layout();

		if(layout_ptr == nullptr) throw("add_avg_temperature_trace(): layout is nullptr, layout must be initialized first");


		const char* py_id;
		const char* py_path;
		
		if (!PyArg_ParseTuple(args, "ss", &py_id, &py_path)) throw("add_avg_temperature_trace(): parsing arguments failed");
		
		std::string id = py_id; // kell ez ide??
		std::string path = py_path;

		/* find the component */
		if(layout_ptr->search_for_component(id))
		{
			layout_ptr->get_component(id)->add_avg_temperature_trace(path);
		}
		else
		{
			layout_ptr->warning("add_avg_temperature_trace(): layout::component_t '" + id + "' was not found");
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "add_avg_temperature_trace(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * letrehoz egy max_temperature_trace-
**/ 
static PyObject* add_dynamic_dissipation_trace(PyObject * self, PyObject *args)
{
	try
	{
		if(logic_engine_ptr == nullptr) throw("add_dynamic_dissipation_trace(): logic engine is nullptr, LogiTherm must be initialized first with 'initialize_logitherm()'");
		
		const char* py_id;
		const char* py_path;
		
		if (!PyArg_ParseTuple(args, "ss", &py_id, &py_path)) throw("add_dynamic_dissipation_trace(): parsing arguments failed");
		
		std::string id = py_id;
		std::string path = py_path;

		/* find the component */
		if(logic_engine_ptr->search_for_component(id))
		{
			logic_engine_ptr->get_component(id)->add_dynamic_dissipation_trace(path);
		}
		else
		{
			logic_engine_ptr->warning("add_dynamic_dissipation_trace(): logic::component_t '" + id + "' was not found");
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "add_dynamic_dissipation_trace(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * letrehoz egy max_temperature_trace-t
**/ 
static PyObject* add_static_dissipation_trace(PyObject * self, PyObject *args)
{
	try
	{
		if(logic_engine_ptr == nullptr) throw("add_static_dissipation_trace(): logic engine is nullptr, LogiTherm must be initialized first with 'initialize_logitherm()'");
		
		const char* py_id;
		const char* py_path;
		
		if (!PyArg_ParseTuple(args, "ss", &py_id, &py_path)) throw("add_static_dissipation_trace(): parsing arguments failed");
		
		std::string id = py_id;
		std::string path = py_path;

		/* find the component */
		if(logic_engine_ptr->search_for_component(id))
		{
			logic_engine_ptr->get_component(id)->add_static_dissipation_trace(path);
		}
		else
		{
			logic_engine_ptr->warning("add_static_dissipation_trace(): logic::component_t '" + id + "' was not found");
		}
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "add_static_dissipation_trace(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * letrehoz egy frequency_temperature_trace-t
**/ 
static PyObject* add_frequency_trace(PyObject * self, PyObject *args)
{
	try
	{
		if(logic_engine_ptr == nullptr) throw("add_frequency_trace(): logic engine is nullptr, LogiTherm must be initialized first with 'initialize_logitherm()'");
		
		const char* py_id;
		const char* py_path;
		
		if (!PyArg_ParseTuple(args, "ss", &py_id, &py_path)) throw("add_frequency_trace(): parsing arguments failed");
		
		std::string id = py_id;
		std::string path = py_path;

		/* find the component */
		auto* component_ptr = dynamic_cast<logic::sniper::component_t*>(logic_engine_ptr->get_component(id));
		if (nullptr == component_ptr) throw ("add_frequency_trace(): component '" + id + "' was not found");

		component_ptr->add_frequency_trace(path);
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "add_static_dissipation_trace(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * letrehoz egy max_temperature_trace-
**/ 
static PyObject* set_thermal_engine(PyObject * self, PyObject *args)
{
	try
	{
		if(nullptr == manager_ptr) throw("set_thermal_engine(): LogiTherm manager is nullptr, it must be initialized first with 'initialize_logitherm()'");
		
		const char* py_type;
		const char* py_path;
		const char* py_file;
		
		if (!PyArg_ParseTuple(args, "sss", &py_type, &py_path, &py_file)) throw("set_thermal_engine(): parsing arguments failed");
		
		std::string type = py_type;
		std::string path = py_path;
		std::string file = py_file;

		if(type == "sunred")
		{
			/* initializing the thermal engine to thermal::sunred::adapter_t */
			thermal_engine_type = util::thermal_engine_t::sunred;
			thermal_engine_ptr = manager_ptr->set_thermal_engine(thermal_engine_type);
		}
		else if(type == "sloth")
		{
			/* initializing the thermal engine to thermal::sunred::adapter_t */
			thermal_engine_type = util::thermal_engine_t::sloth;
			thermal_engine_ptr = manager_ptr->set_thermal_engine(thermal_engine_type);
		}
		else if(type == "3d-ice")
		{
			/* initializing the thermal engine to thermal::sunred::adapter_t */
			thermal_engine_type = util::thermal_engine_t::threedice;
			thermal_engine_ptr = manager_ptr->set_thermal_engine(thermal_engine_type);
		}
		else
		{
			throw("$set_thermal_engine(): thermal engine type can not be recognized");
		}

		/** termikus motor inicializalasa fajlbol **/
		thermal_engine_ptr->read_files(path, file);
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "set_thermal_engine(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * statikus disszipaciot allitja be a keretrendszerben
**/ 
static PyObject* set_static_dissipation(PyObject * self, PyObject *args)
{
	try
	{
		if(logic_engine_ptr == nullptr) throw("logic engine is nullptr, LogiTherm must be initialized first with 'initialize_logitherm()'");

		const char* py_id;
		double dissipation;
		
		if (!PyArg_ParseTuple(args, "sd", &py_id, &dissipation)) throw("set_static_dissipation(): parsing argument failed.");
		
		std::string id = py_id;

		/* find the component */
		auto* component_ptr = dynamic_cast<logic::sniper::component_t*>(logic_engine_ptr->get_component(id));
		if (nullptr == component_ptr)
		{
			throw ("set_static_dissipation(): component '" + id + "' was not found");
		}

		// std::cout << "miafasz van3" << std::endl;
		if(std::isnan(dissipation)) 
		{
			std::cout << "#1 set_static_dissipation(): " << "static_power is nan, " << dissipation << std::endl;
		}

		component_ptr->set_static_dissipation(static_cast<unit::power_t>(dissipation));
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "Unkown exception occured @ set_static_dissipation()" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * dinamikus disszipaciot allitja be a keretrendszerben
**/ 
static PyObject* set_dynamic_dissipation(PyObject * self, PyObject *args)
{
	try
	{
		if(logic_engine_ptr == nullptr) throw("logic engine is nullptr, LogiTherm must be initialized first with 'initialize_logitherm()'");
		
		const char* py_id;
		double dissipation;
		
		if (!PyArg_ParseTuple(args, "sd", &py_id, &dissipation)) throw("set_dynamic_dissipation(): parsing argument failed.");

		std::string id = py_id;

		/* find the component */
		auto* component_ptr = dynamic_cast<logic::sniper::component_t*>(logic_engine_ptr->get_component(id));
		if (nullptr == component_ptr) throw ("set_dynamic_dissipation(): component '" + id + "' was not found");

		component_ptr->set_dynamic_dissipation(static_cast<unit::power_t>(dissipation));
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "set_dynamic_dissipation(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * dinamikus disszipaciot allitja be a keretrendszerben
**/ 
static PyObject* set_frequency(PyObject * self, PyObject *args)
{
	try
	{
		if(logic_engine_ptr == nullptr) throw("logic engine is nullptr, LogiTherm must be initialized first with 'initialize_logitherm()'");
		
		const char* py_id;
		double frequency;
		
		if (!PyArg_ParseTuple(args, "sd", &py_id, &frequency)) throw("set_frequency(): parsing argument failed.");

		std::string id = py_id;

		/* find the component */
		auto* component_ptr = dynamic_cast<logic::sniper::component_t*>(logic_engine_ptr->get_component(id));
		if (nullptr == component_ptr) throw ("set_frequency(): component '" + id + "' was not found");

		component_ptr->set_current_frequency(static_cast<unit::frequency_t>(frequency));
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "set_frequency(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * asdf
**/ 
static PyObject* get_temperature(PyObject * self, PyObject *args)
{
	try
	{
		if(logic_engine_ptr == nullptr) throw("logic engine is nullptr, LogiTherm must be initialized first with 'initialize_logitherm()'");
		
		const char* py_id;
		
		if (!PyArg_ParseTuple(args, "s", &py_id)) throw("get_component_temperature(): parsing argument failed.");
		
		std::string id = py_id;
		
		/* find the component */
		auto* component_ptr = logic_engine_ptr->get_component(id);
		if (nullptr == component_ptr) throw ("get_component_temperature(): component '" + id + "' was not found");
		
		unit::temperature_t temperature = component_ptr->temperature;
		
		PyObject* pytemp = PyFloat_FromDouble(static_cast<double>(temperature));
		return pytemp;
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "get_component_temperature(): unkown error ocurred" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * asdf
**/ 
static PyObject* print_temperature_map_in_svg(PyObject * self, PyObject *args)
{
	try
	{
		if(manager_ptr == nullptr) throw("print_temperature_map_in_svg(): LogiTherm manager is nullptr, it must be initialized first with 'initialize_logitherm()'");
		if(thermal_engine_ptr == nullptr) throw("print_temperature_map_in_svg(): thermal engine is nullptr, it must be initialized first with 'set_thermal_engine()'");
		
		const char* py_file;
		
		if (!PyArg_ParseTuple(args, "s", &py_file))
		{
			throw("print_temperature_map_in_svg(): parsing argument failed");
		}
		std::string file = py_file;
		std::ofstream of(file);
		
		/* print the svg file  */
		if(of.is_open())
		{
			manager_ptr->print_temperature_map_in_svg(of);
			of.close();
		}
		else
		{
			throw("print_temperature_map_in_svg(): could not open '" + file + "'");
		}
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "Unkown exception occured @ print_temperature_map_in_svg()" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * dissipator component-et ad hozza a manager-ben talalhato dissipator_component unordered_map-be
**/ 
static PyObject* add_display_component(PyObject * self, PyObject *args)
{
	try
	{
		if(manager_ptr == nullptr) throw("manager was not initialized");

		const char* py_id;
		
		if (!PyArg_ParseTuple(args, "s", &py_id))
		{
			throw("add_display_compponent(): parsing argument failed.");
		}
		std::string id = py_id;
		
		/* execute one simulation cycle */
		manager_ptr->add_display_component(id);
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "add_display_component(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * dissipator component-et ad hozza a manager-ben talalhato dissipator_component unordered_map-be
**/ 
static PyObject* add_dissipator_component(PyObject * self, PyObject *args)
{
	try
	{
		if(manager_ptr == nullptr) throw("manager was not initialized");

		const char* py_id;
		
		if (!PyArg_ParseTuple(args, "s", &py_id))
		{
			throw("add_dissipator_component(): parsing argument failed.");
		}
		std::string id = py_id;
		
		/* execute one simulation cycle */
		manager_ptr->add_dissipator_component(id);
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "add_dissipator_component(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * beallitja a log utvonalat
**/ 
static PyObject* set_log_file_path(PyObject * self, PyObject *args)
{
	try
	{
		if(manager_ptr == nullptr) throw("set_log_file_path(): LogiTherm manager is nullptr, LogiTherm must be initialized first with 'initialize_logitherm()'");

		const char* py_path;
		
		if (!PyArg_ParseTuple(args, "s", &py_path))
		{
			throw("set_log_file_path(): parsing argument failed");
		}
		
		std::string path = py_path;

		manager_ptr->set_log_file_path(path);
		
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "set_log_file_path(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * a sniper-nek ezt a fv-t kell meghivnia a beallitott idokozonkent
**/ 
static PyObject* run_simulation_cycle(PyObject * self, PyObject *args)
{
	try
	{
		if(manager_ptr == nullptr) throw("run_simulation_cycle(): LogiTherm manager is nullptr, it must be initialized first with 'initialize_logitherm()'");

		manager_ptr->run_simulation_cycle();
		std::ofstream ofs("/mnt/storage/egyetem/phd/tema/projects/sniper/output/dissipation.map");
		thermal_engine_ptr->print_dissipation_map(ofs);
		ofs.close();
		
		manager_ptr->debug("run_simulation_cycle()");
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "run_simulaton_cycle(): unkown exception occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * cleanup, sniper a sim vegen hivja meg
**/ 
static PyObject* set_timestep(PyObject * self, PyObject *args)
{
	try
	{
		if(manager_ptr == nullptr) throw("set_timestep(): LogiTherm manager is nullptr, it must be initialized first with 'initialize_logitherm()'");

		double step;

		if (!PyArg_ParseTuple(args, "d", &step)) throw("set_timestep(): parsing argument failed");


		unit::time_t timestep = 1_ns * step; //ha jol emlekszem nanosec-ben kellett megadni a lepest

		manager_ptr->set_timestep(timestep);
		
		// manager_ptr->~manager_t(); //ez mi a tokomet keresett itt???
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "set_timestep(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}


/**
 * cleanup, sniper a sim vegen hivja meg
**/ 
static PyObject* cleanup(PyObject * self, PyObject *args)
{
	try
	{
		if(manager_ptr == nullptr) throw("manager was not initialized");
		if(thermal_engine_ptr != nullptr)
		{
			layout::adapter_t* layout_ptr = thermal_engine_ptr->get_layout();
			if(nullptr != layout_ptr) layout_ptr->delete_components_tree();
		}
		
		manager_ptr->~manager_t();
	}
	catch(const char* error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(const std::string& error_msg)
	{
		std::cerr << error_msg << std::endl;
		return nullptr; /* error occured, notify python */
	}
	catch(...)
	{
		std::cerr << "cleanup(): unkown error occured" << std::endl;
		return nullptr; /* error occured, notify python */
	}
	Py_RETURN_NONE;
}

/**
 * Second, register this function within a module’s symbol table
 * (all Python functions live in a module, even if they’re actually C functions!)
**/
static PyMethodDef wrappermethods[] = {
 { "initialize_logitherm", initialize_logitherm, METH_VARARGS, "initialize the framework, sets the logic adapater to logic::sniper::adapter_t"},
 { "add_min_temperature_trace", add_min_temperature_trace, METH_VARARGS, "traces the maximum temperature of a layout component"},
 { "add_max_temperature_trace", add_max_temperature_trace, METH_VARARGS, "traces the minimum temperature of a layout component"},
 { "add_avg_temperature_trace", add_avg_temperature_trace, METH_VARARGS, "traces the average temperature of a layout component"},
 { "add_dynamic_dissipation_trace", add_dynamic_dissipation_trace, METH_VARARGS, "traces the dynamic dissipation of a logic component"},
 { "add_static_dissipation_trace", add_static_dissipation_trace, METH_VARARGS, "traces the static dissipation of a logic component"},
 { "add_frequency_trace", add_frequency_trace, METH_VARARGS, "traces the frequency of a logic component"},
 { "set_thermal_engine", set_thermal_engine, METH_VARARGS, "sets the thermal engine"},
 { "set_static_dissipation", set_static_dissipation, METH_VARARGS, "sets the static dissipation of the component"},
 { "set_dynamic_dissipation", set_dynamic_dissipation, METH_VARARGS, "sets the dynamic dissipation of the component"},
 { "set_frequency", set_frequency, METH_VARARGS, "sets the frequency of the component"},
 { "get_temperature", get_temperature, METH_VARARGS, "returns the temperature of the specified component"},
 { "print_temperature_map_in_svg", print_temperature_map_in_svg, METH_VARARGS, "prints the temperature map in an svg file"},
 { "add_display_component", add_display_component, METH_VARARGS, "adds a component to the svg thermal map"},
 { "add_dissipator_component", add_dissipator_component, METH_VARARGS, "adds a component to the dissipator map"},
 { "set_log_file_path", set_log_file_path, METH_VARARGS, "sets the path where the logs during logi-thermal simulation are saved"},
 { "run_simulation_cycle", run_simulation_cycle, METH_VARARGS, "runs a single logi-thermal simulation cycle"},
 { "set_timestep", set_timestep, METH_VARARGS, "sets the timestep of the logi-thermal simulation"},
 { "cleanup", cleanup, METH_VARARGS, "destroys logitherm data structure"},
 { NULL, NULL, 0, NULL }
};

/*
 * SystemC miatt kell, kulonben panaszkodna undefined reference-re
*/
extern "C"
{
	int sc_main(int argc, char* argv[])
	{
		std::cerr << "LogiTherm initialized to Sniper sim" << std::endl;
		return 0;
	}
}
/**
 * Third, write an init function for the module (all extension modules require an init function).
**/ 
extern "C"
{
	void initliblogitherm(void)
	{
	  Py_InitModule("liblogitherm", wrappermethods);
	}
}
