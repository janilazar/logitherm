#ifndef _FREQUENCY_TRACE_
#define _FREQUENCY_TRACE_

#include "util/trace_t.hpp"
#include "util/timestep_t.hpp"

namespace logic
{
	namespace sniper
	{
		class component_t;
		
		class frequency_trace_t	:	public util::trace_t
		{
			private:
				component_t* component;
				const util::timestep_t& timestep;
							
			public:
				
				frequency_trace_t(const std::string& path, const std::string& postfix, component_t* component);
				
				
				void initialize(std::ostream& os) override;
				
				void trace(std::ostream& os) override;
				
				void reset() override;
				
				void finalize(std::ostream& os) override;
		};
	}
}

#endif //_FREQUENCY_TRACE_
