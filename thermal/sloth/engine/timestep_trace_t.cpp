#include "thermal/sloth/engine/timestep_trace_t.hpp"
#include "thermal/sloth/engine/engine_t.hpp"
#include "thermal/sloth/adapter/adapter_t.hpp"
// #include "logic/base/component_t.hpp"
// #include "logic/base/adapter_t.hpp"
#include "util/string_manipulation.hpp"

namespace thermal
{	
	namespace sloth
	{
		timestep_trace_t::timestep_trace_t(const std::string& path, const std::string& postfix, thermal::sloth::engine_t* engine)
		:
			util::trace_t(path, "timestep", "adaptive_timestep", postfix),
			engine(engine)
			// timestep(component->adapter->timestep)
		{}


		void timestep_trace_t::initialize(std::ostream& os)
		{
			os << "timestamp, timestep" << type;
		}

		void timestep_trace_t::trace(std::ostream& os)
		{
			os << util::to_string(engine->adapter->timestep.current_time()) << "," << util::to_string(engine->get_timestep());
		}

		void timestep_trace_t::reset()
		{
			// component->reset();
		}
		
		void timestep_trace_t::finalize(std::ostream& os)
		{}
	}
}
