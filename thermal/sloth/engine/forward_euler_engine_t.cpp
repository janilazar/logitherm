#include "thermal/sloth/engine/forward_euler_engine_t.hpp"

#include "thermal/sloth/adapter/layout/layers_information_t.hpp"
#include "manager/manager_t.hpp"

#include "Eigen/Eigen"
#include "GenEigsSolver.h"
#include "MatOp/SparseGenMatProd.h"

namespace thermal
{
	namespace sloth
	{
		template<utype_float_t float_enum>
		std::tuple<double,double> forward_euler_engine_t<float_enum>::calculate_boundary(side_t side, size_t x, size_t y, size_t z)
		{
			std::tuple<double,double,double> Gcell = get_material_conductivity(x,y,z);
			
			double G;
			if (side == side_t::top || side == side_t::bottom) G = 2.0*std::get<2>(Gcell);
			if (side == side_t::east || side == side_t::west) G = 2.0*std::get<0>(Gcell);
			if (side == side_t::south || side == side_t::north) G = 2.0*std::get<1>(Gcell);
			
			boundary_t boundary;
			try
			{
				boundary = std::get<0>(boundary_conditions.at(side));
			}
			catch(...)
			{
				adapter->error("boundary condition was not set at '" + adapter->get_side(side) + "' side");
			}
			
			if(boundary == boundary_t::adiabatic)
			{
				return std::make_tuple(0.0,0.0);
			}
			
			if(boundary == boundary_t::temperature)
			{
				return std::make_tuple(G,G*std::get<1>(boundary_conditions.at(side)));
			}
			
			if(boundary == boundary_t::current)
			{
				return std::make_tuple(0.0,std::get<1>(boundary_conditions.at(side)));
			}
			
			if(boundary == boundary_t::conductive)
			{
				double thickness = static_cast<double>(layout_structure.get_layer_thickness(z));
				double A;
				if (side == side_t::top || side == side_t::bottom) A = x_size*y_size;
				if (side == side_t::east || side == side_t::west) A = y_size*thickness;
				if (side == side_t::south || side == side_t::north) A = x_size*thickness;
				double h = std::get<1>(boundary_conditions.at(side));
				double Ght = h*A;
				double Gtotal = Ght*G / (Ght + G);
				return std::make_tuple(Gtotal,0.0); //ambient_temperature*Gtotal);
			}
			
			adapter->error("calculate_boundary(): unkown boundary condition type");
			return std::make_tuple(0.0,0.0);
		}
		
		template<utype_float_t float_enum>
		std::tuple<double,double,double> forward_euler_engine_t<float_enum>::get_material_conductivity(size_t x, size_t y, size_t z)
		{
			double lambda = layout_structure.get_layer_material(z).thermal_conductivity;
			double thickness = static_cast<double>(layout_structure.get_layer_thickness(z));
			
			double Gx = lambda*y_size*thickness/(x_size);
			double Gy = lambda*x_size*thickness/(y_size);
			double Gz = lambda*x_size*y_size/(thickness);
			return std::make_tuple(Gx,Gy,Gz);
		}

		template<utype_float_t float_enum>
		typename forward_euler_engine_t<float_enum>::float_type forward_euler_engine_t<float_enum>::predict_max_temperature_change()
		{
			return 1.0;
		}
		

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::init_Gminus_vectors()
		{
			// #pragma omp parallel for
			for(size_t index = 0; index < nodes; ++index)
			{
				size_t x = index % x_pitch;
				size_t y = (index/x_pitch) % y_pitch;
				size_t z = (index/(x_pitch*y_pitch));

				std::tuple<double,double,double> Gt = get_material_conductivity(x,y,z);
				double Gx = std::get<0>(Gt); //x iranyu konduktancia
				double Gy = std::get<1>(Gt); //y iranyu konduktancia
				double Gz = std::get<2>(Gt); //z iranyu konduktancia
				double Gmd = 0.0;			//foatloban levo konduktancia, fugg attol, hogy hany szomszedos node van
				
				if(x > 0)
				{
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x-1 + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_values.push_back(Gx);
					Gmd -= Gx;
				}
				else
				{
					//west boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::west,x,y,z));
				}
				
				if(y > 0)
				{
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x + (y-1)*x_pitch + z*x_pitch*y_pitch);
					Gminus_values.push_back(Gy);
					Gmd -= Gy;
				}
				else
				{
					//north boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::north,x,y,z));
				}
				
				if(z > 0)
				{
					//ide tenni a elozo reteg Gz-jet is
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x + y*x_pitch + (z-1)*x_pitch*y_pitch);
					
					double Gz_this = 2.0*Gz;
					double Gz_prev = 2.0*std::get<2>(get_material_conductivity(x,y,z-1));
					double Gtotal = (Gz_this*Gz_prev)/(Gz_this+Gz_prev);

					Gminus_values.push_back(static_cast<float_type>(Gtotal));
					Gmd -= Gtotal;
				}
				else
				{
					//top boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::top,x,y,z));
					
				}
				
				if(x < x_pitch-1)
				{
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x+1 + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_values.push_back(Gx);
					Gmd -= Gx;
				}
				else
				{
					//east boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::east,x,y,z));
				}
				
				if(y < y_pitch-1)
				{
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x + (y+1)*x_pitch + z*x_pitch*y_pitch);
					Gminus_values.push_back(Gy);
					Gmd -= Gy;
				}
				else
				{
					//south boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::south,x,y,z));
				}
				
				if(z < z_pitch-1)
				{
					//ide tenni kovi reteg gz-jet
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x + y*x_pitch + (z+1)*x_pitch*y_pitch);

					double Gz_next = 2.0*std::get<2>(get_material_conductivity(x,y,z+1));
					double Gz_this = 2.0*Gz;
					double Gtotal = (Gz_next*Gz_this)/(Gz_next+Gz_this); //a gebasz itt van
					
					Gminus_values.push_back(static_cast<float_type>(Gtotal));
					Gmd -= Gtotal;
				}
				else
				{
					//bottom boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::bottom,x,y,z));
				}
				
				//foatlo irasa
				Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
				Gminus_cols.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
				Gminus_values.push_back(Gmd);
			}
		}


		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::init_Cth_vectors()
		{
			// #pragma omp parallel for
			for(size_t index = 0; index < nodes; ++index)
			{
				size_t x = index % x_pitch;
				size_t y = (index/x_pitch) % y_pitch;
				size_t z = (index/(x_pitch*y_pitch));
				double thickness = static_cast<double>(layout_structure.get_layer_thickness(z));

				//foatloban hozza kell adni a hokapacitasbol eredo konduktanciat is
				Cth_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
				Cth_cols.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
				
				Cth_values.push_back(layout_structure.get_layer_material(z).thermal_capacity*x_size*y_size*thickness);
				reciprocal_Cth_values.push_back(1.0/(layout_structure.get_layer_material(z).thermal_capacity*x_size*y_size*thickness));
			}
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble_mx_Gfwd()
		{
			mx_Gfwd.Assemble(&Gminus_rows[0],&Gminus_cols[0],&Gminus_values[0],static_cast<int>(Gminus_values.size()),"Gfwd",nodes, nodes);
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble_mx_Gminus()
		{
			mx_Gminus.Assemble(&Gminus_rows[0],&Gminus_cols[0],&Gminus_values[0],static_cast<int>(Gminus_values.size()),"Gminus",nodes, nodes);
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble_mx_Cth()
		{
			mx_Cth.Assemble(&Cth_rows[0],&Cth_cols[0],&Cth_values[0],static_cast<int>(Cth_values.size()),"Cth",nodes, nodes); //reciprocal_Cth_values volt
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble_mx_GCth()
		{
			mx_GCth.Assemble(&Cth_rows[0],&Cth_cols[0],&Cth_values[0],static_cast<int>(Cth_values.size()),"GCth",nodes, nodes); //reciprocal_Cth_values volt
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble_vec_new_temperature_map()
		{
			vec_new_temperature_map.Allocate("new_temperature_map",nodes);
			vec_new_temperature_map.Zeros(); //SetValues(ambient_temperature);
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble_vec_temperature_map_accelerator()
		{
			if(paralution::_paralution_available_accelerator() && use_accelerator)
			{
				vec_temperature_map_accelerator.Allocate("temperature_map_accelerator",nodes);
				vec_temperature_map_accelerator.Zeros(); //SetValues(ambient_temperature);
			}
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble_vec_dissipation_map_accelerator()
		{
			if(paralution::_paralution_available_accelerator() && use_accelerator)
			{
				vec_dissipation_map_accelerator.Allocate("dissipation_map_accelerator",nodes);
				vec_dissipation_map_accelerator.Zeros();
			}
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble_vec_Pboundary()
		{
			vec_Pboundary.Allocate("P_boundary",nodes);
			vec_Pboundary.Zeros();

			// #pragma omp parallel for
			for(size_t index = 0; index < nodes; ++index)
			{
				size_t x = index % x_pitch;
				size_t y = (index/x_pitch) % y_pitch;
				size_t z = (index/(x_pitch*y_pitch));
				
				if(x == 0)
				{
					//west boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::west,x,y,z));
				}
				
				if(y == 0)
				{
					//north boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::north,x,y,z));
				}
				
				if(z == 0)
				{
					//top boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::top,x,y,z));
				}
				
				if(x == x_pitch-1)
				{
					//east boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::east,x,y,z));
				}
				
				if(y == y_pitch-1)
				{
					//south boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::south,x,y,z));
				}
				
				if(z == z_pitch-1)
				{
					//bottom boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::bottom,x,y,z));
				}
			}
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble_vec_reciprocal_Cth()
		{
			vec_reciprocal_Cth.Allocate("reciprocal_C",nodes);
			
			// #pragma omp parallel for
			for(size_t index = 0; index < nodes; ++index)
			{
				size_t x = index % x_pitch;
				size_t y = (index/x_pitch) % y_pitch;
				size_t z = (index/(x_pitch*y_pitch));

				double thickness = static_cast<double>(layout_structure.get_layer_thickness(z));
				vec_reciprocal_Cth[x + y*x_pitch + z*x_pitch*y_pitch] = 1.0/(layout_structure.get_layer_material(z).thermal_capacity*x_size*y_size*thickness);
			}

		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble_vec_dTdt()
		{
			vec_dTdt.Allocate("dT/dt",nodes);
			vec_dTdt.Zeros();
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::assemble()
		{
				init_Gminus_vectors();
				init_Cth_vectors();

				assemble_mx_Gfwd();
				assemble_mx_Gminus();
				assemble_mx_Cth();
				assemble_mx_GCth();

				assemble_vec_new_temperature_map();
				assemble_vec_temperature_map_accelerator();
				assemble_vec_dissipation_map_accelerator();
				assemble_vec_Pboundary();
				assemble_vec_reciprocal_Cth();
				assemble_vec_dTdt();

		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::move_to_accelerator()
		{
			if(paralution::_paralution_available_accelerator() && use_accelerator)
			{
				mx_Gminus.MoveToAccelerator();
				mx_Gfwd.MoveToAccelerator();
				mx_Cth.MoveToAccelerator();
				mx_GCth.MoveToAccelerator();

				vec_new_temperature_map.MoveToAccelerator();
				vec_temperature_map_accelerator.MoveToAccelerator();
				vec_dissipation_map_accelerator.MoveToAccelerator();
				vec_Pboundary.MoveToAccelerator();
				vec_reciprocal_Cth.MoveToAccelerator();
				vec_dTdt.MoveToAccelerator();
			}	
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::reset_mx_GCth()
		{
			mx_GCth.CopyFrom(mx_Cth);
			mx_GCth.Scale(1.0/timestep);
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::reset_mx_Gfwd()
		{
			mx_Gfwd.CopyFrom(mx_Gminus);
			mx_Gfwd.MatrixAdd(mx_GCth);
		}


		template<utype_float_t float_enum>
		bool forward_euler_engine_t<float_enum>::over_temperature_threshold()
		{
			float_type max_dTdt;
			max_dTdt = predict_max_temperature_change();
			double delta_time = static_cast<double>(cycle_counter) * timestep;
			return (max_dTdt*delta_time > temperature_threshold);
		}


		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::solve()
		{
			if(paralution::_paralution_available_accelerator() && use_accelerator)
			{
				vec_dissipation_map_accelerator.CopyFrom(dissipation_map);
				
				mx_Gfwd.Apply(vec_temperature_map_accelerator, &vec_new_temperature_map);
				vec_new_temperature_map.ScaleAdd2(timestep, vec_dissipation_map_accelerator, timestep, vec_Pboundary, timestep);
				vec_new_temperature_map.PointWiseMult(vec_reciprocal_Cth);

				vec_temperature_map_accelerator.CopyFrom(vec_new_temperature_map);
			}
			else
			{
				mx_Gfwd.Apply(temperature_map, &vec_new_temperature_map);
				vec_new_temperature_map.ScaleAdd2(timestep, dissipation_map, timestep, vec_Pboundary, timestep);
				vec_new_temperature_map.PointWiseMult(vec_reciprocal_Cth);
			}

			temperature_map.CopyFrom(vec_new_temperature_map);
			dissipation_map.Zeros();
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::calculate_temperatures(double timestep)
		{
			this->timestep = timestep;
			
			reset_mx_GCth();
			reset_mx_Gfwd();

			solve();			
		}
		
		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::calculate_temperatures()
		{
			if(use_adaptive_timestep)
			{
				cycle_counter++;
				dissipation_map.Scale(1.0/static_cast<double>(cycle_counter));
				if(over_temperature_threshold() || (cycle_counter == max_empty_cycle))
				{	
					double delta_time = static_cast<double>(cycle_counter) * timestep;
					cycle_counter = 0;
					calculate_temperatures(delta_time);
				}
				else
				{
					dissipation_map.Scale(static_cast<double>(cycle_counter));
				}
			}
			else
			{
				solve();
			}
		}
		
		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::save_conductivity_matrix(const std::string& file)
		{
			paralution::LocalMatrix<float_type> mx_reciprocal_Cth;
			mx_reciprocal_Cth.Assemble(&Cth_rows[0],&Cth_cols[0],&reciprocal_Cth_values[0],static_cast<int>(reciprocal_Cth_values.size()),"reciprocal_Cth",nodes, nodes);

			paralution::LocalMatrix<float_type> mx_G;
			mx_G.Assemble(&Gminus_rows[0],&Gminus_cols[0],&Gminus_values[0],static_cast<int>(Gminus_values.size()),"G",nodes, nodes);

			mx_G.MatrixMult(mx_reciprocal_Cth,mx_Gminus);

			mx_G.WriteFileMTX(file);
			mx_G.Clear();
			mx_reciprocal_Cth.Clear();
		}

		template<utype_float_t float_enum>
		double forward_euler_engine_t<float_enum>::get_timestep()
		{
			return timestep;
		}

		template<utype_float_t float_enum>
		double forward_euler_engine_t<float_enum>::get_stability_limit()
		{
			return stability_limit;
		}

		template<utype_float_t float_enum>
		void forward_euler_engine_t<float_enum>::check_stability()
		{
		    paralution::LocalMatrix<float_type> mx_reciprocal_Cth;
			mx_reciprocal_Cth.Assemble(&Cth_rows[0],&Cth_cols[0],&reciprocal_Cth_values[0],static_cast<int>(reciprocal_Cth_values.size()),"reciprocal_Cth",nodes, nodes);

			paralution::LocalMatrix<float_type> mx_G;
			mx_G.Assemble(&Gminus_rows[0],&Gminus_cols[0],&Gminus_values[0],static_cast<int>(Gminus_values.size()),"G",nodes, nodes);

			mx_G.MatrixMult(mx_reciprocal_Cth,mx_Gminus);

			int* rows = new int[mx_G.get_nnz()];
			int* cols = new int[mx_G.get_nnz()];
			float_type* vals = new float_type[mx_G.get_nnz()];

		    mx_G.ConvertToCOO();
		    mx_G.CopyToCOO(rows, cols, vals);

		    std::vector<Eigen::Triplet<float_type>> stability_matrix_triplets;
			stability_matrix_triplets.reserve(mx_G.get_nnz());

		    for(size_t it = 0; it < mx_G.get_nnz(); ++it)
			{
				stability_matrix_triplets.push_back(Eigen::Triplet<float_type>(rows[it], cols[it], vals[it])); // itt le kellene osztani Cth-val valahogy...
			}

			Eigen::SparseMatrix<float_type> stability_matrix(nodes,nodes);
			stability_matrix.setFromTriplets(stability_matrix_triplets.begin(), stability_matrix_triplets.end());

			Spectra::SparseGenMatProd<float_type> op(stability_matrix);

			size_t number_of_eigenvalues = 1;
			Spectra::GenEigsSolver<float_type, Spectra::LARGEST_MAGN, Spectra::SparseGenMatProd<float_type>> eigs(&op, number_of_eigenvalues, std::min(nodes, std::max(2*number_of_eigenvalues+1, std::size_t{20})));

			// Initialize and compute
		    eigs.init();
		    int nconv = eigs.compute();

		    // Retrieve results
		    Eigen::Matrix<std::complex<float_type>, Eigen::Dynamic, 1> evalues;
		   
		    if(eigs.info() == Spectra::SUCCESSFUL) evalues = eigs.eigenvalues();
		    else error("check_stability(): failed to evaluate eigenvalues");

		    stability_limit = 2.0/std::abs(evalues(0));

			if(stability_limit < timestep) warning("check_stability(): possible stability issue, choose smaller (<" + std::to_string(stability_limit) + ") timestep");

			mx_G.Clear();
			mx_reciprocal_Cth.Clear();
			delete[] cols;
			delete[] rows;
			delete[] vals;
		}

		template<utype_float_t float_enum>
		forward_euler_engine_t<float_enum>::forward_euler_engine_t(thermal::sloth::adapter_t* adapter,
						   layout::sloth::adapter_t& layout_structure,
						   std::unordered_map<side_t,std::tuple<boundary_t, double>>& boundary_conditions,
						   paralution::LocalVector<float_type>& dissipation_map,
						   paralution::LocalVector<float_type>& temperature_map,
						   double ambient_temperature,
						   double timestep,
						   bool use_adaptive_timestep,
						   double temperature_threshold,
						   const precision_t& precision,
						   bool use_accelerator)
		:
			engine_t(adapter),
			layout_structure(layout_structure),
			boundary_conditions(boundary_conditions),
			x_pitch(layout_structure.get_pitch()[0]),
			y_pitch(layout_structure.get_pitch()[1]),
			z_pitch(layout_structure.get_pitch()[2]),
			x_size(layout_structure.get_cell_size()[0]),
			y_size(layout_structure.get_cell_size()[1]),
			nodes(x_pitch*y_pitch*z_pitch),
			stability_limit(0.0),
			timestep(timestep),
			use_adaptive_timestep(use_adaptive_timestep),
			cycle_counter(0),
			max_empty_cycle(1000),
			temperature_threshold(temperature_threshold),
			// temperature_tolerance(precision.temperature_tolerance),
			absolute_tolerance(1e-10),
			relative_tolerance(1e-3),
			dissipation_map(dissipation_map),
			temperature_map(temperature_map),
			ambient_temperature(ambient_temperature),
			use_accelerator(use_accelerator)
		{
			assemble();
			
			reset_mx_GCth();
			reset_mx_Gfwd();

			// itt lehetne egy ellenorzest csinalni, hogy a timestep hogyan viszonyul a stabilitashoz
			// triplet -> egy nnz bejegyzes mx-ban -> Gminus/Cth mx alapjan meg lehetne csinalni...

			check_stability();

			

			move_to_accelerator();
		}
		
		template<utype_float_t float_enum>
		forward_euler_engine_t<float_enum>::~forward_euler_engine_t()
		{

			mx_Gfwd.Clear();
			mx_Gminus.Clear();
			mx_Cth.Clear();
			mx_GCth.Clear();

			vec_new_temperature_map.Clear();
			vec_temperature_map_accelerator.Clear();
			vec_dissipation_map_accelerator.Clear();
			vec_Pboundary.Clear();
			vec_reciprocal_Cth.Clear();
			vec_dTdt.Clear();
		}
				
	}
}

/*
 * explicit template instantiation
*/
template class thermal::sloth::forward_euler_engine_t<static_cast<thermal::sloth::utype_float_t>(thermal::sloth::float_t::snl)>;
template class thermal::sloth::forward_euler_engine_t<static_cast<thermal::sloth::utype_float_t>(thermal::sloth::float_t::dbl)>;