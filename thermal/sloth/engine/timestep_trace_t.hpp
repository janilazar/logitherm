#ifndef _DYNAMIC_DISSIPATION_TRACE_
#define _DYNAMIC_DISSIPATION_TRACE_

#include "util/trace_t.hpp"
#include "util/timestep_t.hpp"

namespace thermal
{
	namespace sloth
	{
	class engine_t;
	
		class timestep_trace_t	:	public util::trace_t
		{
			private:
				engine_t* engine;

				// const util::timestep_t& timestep;
							
			public:
				
				timestep_trace_t(const std::string& path, const std::string& postfix, thermal::sloth::engine_t* engine);
				
				
				void initialize(std::ostream& os) override;
				
				void trace(std::ostream& os) override;
				
				void reset() override;
				
				void finalize(std::ostream& os) override;
		};
	}
}

#endif //_DYNAMIC_DISSIPATION_TRACE_
