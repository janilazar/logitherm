#ifndef _SLOTH_ENGINE_
#define _SLOTH_ENGINE_

#include <tuple>
#include <vector>

#include "util/tracer_t.hpp"
// #include "unit/all_units.hpp"
// #include "dependency/paralution/paralution.hpp"
// #include "thermal/sloth/adapter/adapter_t.hpp"
// #include "thermal/sloth/adapter/precision_t.hpp"

// namespace logitherm
// {
// 	class manager_t;
// }

namespace thermal
{
	namespace sloth
	{
		// forward decl.
		class timestep_trace_t;
		class log_t;
		class adapter_t;

		// template<utype_euler_t euler_enum, utype_float_t float_enum>
		class engine_t
		{

			friend class timestep_trace_t;

			public:
				const std::string id;

			private:
				util::log_t* log;

			protected:
				/**
				 * a megfigyelendo adatok rogziteseert felelos valtozo
				**/ 
				util::tracer_t tracer;
				thermal::sloth::adapter_t* adapter;

			public:
				engine_t() = delete;
				engine_t(thermal::sloth::adapter_t* adapter);

				~engine_t();
				
				/** egyenlet megoldasa, ket path**/
				virtual void calculate_temperatures() = 0;

				/** ha logaritmikus az idoleptek **/
				virtual void calculate_temperatures(double timestep) = 0;

				virtual void save_conductivity_matrix(const std::string& file) = 0;

				timestep_trace_t* add_timestep_trace(const std::string& path, const std::string& postfix);
				
				virtual double get_timestep() = 0;
				virtual double get_stability_limit() = 0;

				void debug(const char* msg) const;
				void debug(const std::string& msg) const;
				void warning(const char* msg) const;
				void warning(const std::string& msg) const;
				void error(const char*) const;
				void error(const std::string& msg) const;
		};
	}
}

#endif //_SLOTH_ENGINE_
