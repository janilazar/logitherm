#include "thermal/sloth/engine/engine_t.hpp"
#include "thermal/sloth/engine/timestep_trace_t.hpp"
#include "thermal/sloth/adapter/adapter_t.hpp"
#include "manager/manager_t.hpp"

// #include "thermal/sloth/adapter/layout/layers_information_t.hpp"
#include "util/log_t.hpp"

namespace thermal
{
	namespace sloth
	{

		engine_t::engine_t(thermal::sloth::adapter_t* adapter)
		:
			adapter(adapter),
			log((adapter != nullptr) ? adapter->log : nullptr),
			id("SloTh"),
			tracer("thermal_engine", this->id, this->log)
		{}

		engine_t::~engine_t()
		{}

		timestep_trace_t* engine_t::add_timestep_trace(const std::string& path, const std::string& postfix)
		{
			timestep_trace_t* ptr = new timestep_trace_t(path, postfix, this);
			tracer.add_trace(ptr);
			if(adapter == nullptr) error("'"+ id + "' adapter is nullptr");
			adapter->manager->trace_component(&this->tracer);
			return ptr;
		}

		void engine_t::debug(const char* msg) const
		{
			if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr!\nDEBUG MSG '" + msg + "'");
			log->debug("'" + id + "' " + msg);
		}
		
		void engine_t::debug(const std::string& msg) const
		{
			if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr!\nDEBUG MSG '" + msg + "'");
			log->debug("'" + id + "' " + msg);
		}
		
		void engine_t::warning(const char* msg) const
		{
			if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr\nWARNING MSG '" + msg + "'");
			log->warning("'" + id + "' " + msg);
		}
		
		void engine_t::warning(const std::string& msg) const
		{
			if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr\nWARNING MSG '" + msg + "'");
			log->warning("'" + id + "' " + msg);
		}
		
		void engine_t::error(const char* msg) const
		{
			if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr\nERROR MSG '" + msg + "'");
			log->error("'" + id + "' " + msg);
		}
		
		void engine_t::error(const std::string& msg) const
		{
			if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr\nERROR MSG '" + msg + "'");
			log->error("'" + id + "' " + msg);
		}				
	}
}

/*
 * explicit template instantiation
*/
// template class thermal::sloth::forward_euler_engine_t<float>;
// template class thermal::sloth::forward_euler_engine_t<double>;