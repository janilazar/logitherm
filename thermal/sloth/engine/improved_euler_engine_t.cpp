#include "thermal/sloth/engine/improved_euler_engine_t.hpp"

#include "thermal/sloth/adapter/layout/layers_information_t.hpp"
#include "manager/manager_t.hpp"

#include <algorithm>

namespace thermal
{
	namespace sloth
	{
		template<utype_float_t float_enum>
		std::tuple<double,double> improved_euler_engine_t<float_enum>::calculate_boundary(side_t side, size_t x, size_t y, size_t z)
		{
			std::tuple<double,double,double> Gcell = get_material_conductivity(x,y,z);
			
			double G;
			if (side == side_t::top || side == side_t::bottom) G = 2.0*std::get<2>(Gcell);
			if (side == side_t::east || side == side_t::west) G = 2.0*std::get<0>(Gcell);
			if (side == side_t::south || side == side_t::north) G = 2.0*std::get<1>(Gcell);
			
			boundary_t boundary;
			try
			{
				boundary = std::get<0>(boundary_conditions.at(side));
			}
			catch(...)
			{
				adapter->error("boundary condition was not set at '" + adapter->get_side(side) + "' side");
			}
			
			if(boundary == boundary_t::adiabatic)
			{
				return std::make_tuple(0.0,0.0);
			}
			
			if(boundary == boundary_t::temperature)
			{
				return std::make_tuple(G,G*std::get<1>(boundary_conditions.at(side)));
			}
			
			if(boundary == boundary_t::current)
			{
				return std::make_tuple(0.0,std::get<1>(boundary_conditions.at(side)));
			}
			
			if(boundary == boundary_t::conductive)
			{
				double thickness = static_cast<double>(layout_structure.get_layer_thickness(z));
				double A;
				if (side == side_t::top || side == side_t::bottom) A = x_size*y_size;
				if (side == side_t::east || side == side_t::west) A = y_size*thickness;
				if (side == side_t::south || side == side_t::north) A = x_size*thickness;
				double h = std::get<1>(boundary_conditions.at(side));
				double Ght = h*A;
				double Gtotal = Ght*G / (Ght + G);
				return std::make_tuple(Gtotal,0.0); //ambient_temperature*Gtotal);	
			}
			
			adapter->error("calculate_boundary(): unkown boundary condition type");
			return std::make_tuple(0.0,0.0);
		}
		
		template<utype_float_t float_enum>
		std::tuple<double,double,double> improved_euler_engine_t<float_enum>::get_material_conductivity(size_t x, size_t y, size_t z)
		{
			double lambda = layout_structure.get_layer_material(z).thermal_conductivity;
			double thickness = static_cast<double>(layout_structure.get_layer_thickness(z));
			
			double Gx = lambda*y_size*thickness/(x_size);
			double Gy = lambda*x_size*thickness/(y_size);
			double Gz = lambda*x_size*y_size/(thickness);
			return std::make_tuple(Gx,Gy,Gz);
		}

		template<utype_float_t float_enum>
		typename improved_euler_engine_t<float_enum>::float_type improved_euler_engine_t<float_enum>::predict_max_temperature_change()
		{
			float_type max = 1.0f;
			if(paralution::_paralution_available_accelerator() && use_accelerator)
			{
				// TODO
				// Gdc.Apply(Tnode, &dTdt);
				// dTdt.ScaleAdd2(1.0,Pbound, -1.0, Pnode, -1.0);
			}
			else
			{
				// Gdc.Apply(temperature_map, &dTdt);
				// dTdt.ScaleAdd2(1.0,Pbound, -1.0, dissipation_map, -1.0);
			}
			// dTdt.PointWiseMult(reciprocal_Cnode);
			// dTdt.Amax(max);

			return max;
		}
		

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::init_Gminus_vectors()
		{
			// #pragma omp parallel for
			for(size_t index = 0; index < nodes; ++index)
			{
				size_t x = index % x_pitch;
				size_t y = (index/x_pitch) % y_pitch;
				size_t z = (index/(x_pitch*y_pitch));

				std::tuple<double,double,double> Gt = get_material_conductivity(x,y,z);
				double Gx = std::get<0>(Gt); //x iranyu konduktancia
				double Gy = std::get<1>(Gt); //y iranyu konduktancia
				double Gz = std::get<2>(Gt); //z iranyu konduktancia
				double Gmd = 0.0;			//foatloban levo konduktancia, fugg attol, hogy hany szomszedos node van
				
				if(x > 0)
				{
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x-1 + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_values.push_back(Gx);
					Gmd -= Gx;
				}
				else
				{
					//west boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::west,x,y,z));
				}
				
				if(y > 0)
				{
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x + (y-1)*x_pitch + z*x_pitch*y_pitch);
					Gminus_values.push_back(Gy);
					Gmd -= Gy;
				}
				else
				{
					//north boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::north,x,y,z));
				}
				
				if(z > 0)
				{
					//ide tenni a elozo reteg Gz-jet is
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x + y*x_pitch + (z-1)*x_pitch*y_pitch);
					double Gz_this = 2.0*Gz;
					double Gz_prev = 2.0*std::get<2>(get_material_conductivity(x,y,z-1));
					double Gtotal = Gz_this*Gz_prev/(Gz_this+Gz_prev);
					Gminus_values.push_back(Gtotal);
					Gmd -= Gtotal;
				}
				else
				{
					//top boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::top,x,y,z));
					
				}
				
				if(x < x_pitch-1)
				{
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x+1 + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_values.push_back(Gx);
					Gmd -= Gx;
				}
				else
				{
					//east boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::east,x,y,z));
				}
				
				if(y < y_pitch-1)
				{
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x + (y+1)*x_pitch + z*x_pitch*y_pitch);
					Gminus_values.push_back(Gy);
					Gmd -= Gy;
				}
				else
				{
					//south boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::south,x,y,z));
				}
				
				if(z < z_pitch-1)
				{
					//ide tenni kovi reteg gz-jet
					Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
					Gminus_cols.push_back(x + y*x_pitch + (z+1)*x_pitch*y_pitch);
					double Gz_this = 2.0*Gz;
					double Gz_next = 2.0*std::get<2>(get_material_conductivity(x,y,z+1));
					double Gtotal = Gz_this*Gz_next/(Gz_this+Gz_next);
					Gminus_values.push_back(Gtotal);
					Gmd -= Gtotal;
				}
				else
				{
					//bottom boundary
					Gmd -= std::get<0>(calculate_boundary(side_t::bottom,x,y,z));
				}
				
				//foatlo irasa
				Gminus_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
				Gminus_cols.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
				Gminus_values.push_back(Gmd);
			}

			Gxyzmin = std::abs(*std::min_element(Gminus_values.cbegin(), Gminus_values.cend(), [](float_type a, float_type b)->bool{return std::abs(a) < std::abs(b);}));
		}


		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::init_Cth_vectors()
		{
			Cthmin = std::numeric_limits<double>::max();

			// #pragma omp parallel for
			for(size_t index = 0; index < nodes; ++index)
			{
				size_t x = index % x_pitch;
				size_t y = (index/x_pitch) % y_pitch;
				size_t z = (index/(x_pitch*y_pitch));
				double thickness = static_cast<double>(layout_structure.get_layer_thickness(z));

				//foatloban hozza kell adni a hokapacitasbol eredo konduktanciat is
				Cth_rows.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
				Cth_cols.push_back(x + y*x_pitch + z*x_pitch*y_pitch);
				
				float_type Cth_value = layout_structure.get_layer_material(z).thermal_capacity*x_size*y_size*thickness;
				Cth_values.push_back(Cth_value);
				// if(Cthmin > Cth_value) Cthmin = Cth_value;
			}

			Cthmin = *std::min_element(Cth_values.cbegin(), Cth_values.cend(), [](float_type a, float_type b)->bool{return std::abs(a) < std::abs(b);});

			// absolute_tolerance = Cthmin * temperature_tolerance / (2.0 * timestep);
			// std::cout << "absolute_tolerance=" << absolute_tolerance << std::endl;
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_mx_Gplus()
		{
			mx_Gplus.Assemble(&Gminus_rows[0],&Gminus_cols[0],&Gminus_values[0],static_cast<int>(Gminus_values.size()),"Gplus",nodes, nodes);
			mx_Gplus.Scale(-1.0);
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_mx_Gminus()
		{
			mx_Gminus.Assemble(&Gminus_rows[0],&Gminus_cols[0],&Gminus_values[0],static_cast<int>(Gminus_values.size()),"Gminus",nodes, nodes);
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_mx_Gfwd()
		{
			mx_Gfwd.Assemble(&Gminus_rows[0],&Gminus_cols[0],&Gminus_values[0],static_cast<int>(Gminus_values.size()),"Gfwd",nodes, nodes);
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_mx_Gbwd()
		{
			mx_Gbwd.Assemble(&Gminus_rows[0],&Gminus_cols[0],&Gminus_values[0],static_cast<int>(Gminus_values.size()),"Gbwd",nodes, nodes);
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_mx_Cth()
		{
			mx_Cth.Assemble(&Cth_rows[0],&Cth_cols[0],&Cth_values[0],static_cast<int>(Cth_values.size()),"Cth",nodes, nodes);
		}
		
		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_mx_GCth()
		{
			mx_GCth.Assemble(&Cth_rows[0],&Cth_cols[0],&Cth_values[0],static_cast<int>(Cth_values.size()),"GCth",nodes, nodes);

		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_vec_Psum()
		{
			vec_Psum.Allocate("Psum",nodes);
			vec_Psum.Zeros(); //SetValues(ambient_temperature);
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_vec_temperature_map_accelerator()
		{
			if(paralution::_paralution_available_accelerator() && use_accelerator)
			{
				vec_temperature_map_accelerator.Allocate("temperature_map_accelerator",nodes);
				vec_temperature_map_accelerator.Zeros(); //SetValues(ambient_temperature);
			}
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_vec_dissipation_map_accelerator()
		{
			if(paralution::_paralution_available_accelerator() && use_accelerator)
			{
				vec_dissipation_map_accelerator.Allocate("dissipation_map_accelerator",nodes);
				vec_dissipation_map_accelerator.Zeros();
			}
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_vec_Pboundary()
		{
			vec_Pboundary.Allocate("Pboundary",nodes);
			vec_Pboundary.Zeros();

			// #pragma omp parallel for
			for(size_t index = 0; index < nodes; ++index)
			{
				size_t x = index % x_pitch;
				size_t y = (index/x_pitch) % y_pitch;
				size_t z = (index/(x_pitch*y_pitch));
				
				if(x == 0)
				{
					//west boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::west,x,y,z));
				}
				
				if(y == 0)
				{
					//north boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::north,x,y,z));
				}
				
				if(z == 0)
				{
					//top boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::top,x,y,z));
				}
				
				if(x == x_pitch-1)
				{
					//east boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::east,x,y,z));
				}
				
				if(y == y_pitch-1)
				{
					//south boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::south,x,y,z));
				}
				
				if(z == z_pitch-1)
				{
					//bottom boundary
					vec_Pboundary[x + y*x_pitch + z*x_pitch*y_pitch] += std::get<1>(calculate_boundary(side_t::bottom,x,y,z));
				}
			}
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_vec_dTdt()
		{
			vec_dTdt.Allocate("dT/dt",nodes);
			vec_dTdt.Zeros();
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble_solver()
		{
			//parameterekkel kellene meg jatszani
			// precision    # of digits
			// float		~7.2
			// double		~15.9

			// numeric_limits<float_enum>::epsilon()
			// ehelyett valami numeric_limits-beli cuccost kene beirni, ne legyen ilyen buzi statikusan beirva a pontossag
			solver.Init(absolute_tolerance, relative_tolerance, 1e4, 1000); // MAGIC //
			// solver.Init(1e-22, 1e-20, 1e4, 1000); // MAGIC //
			// solver.Init(std::numeric_limits<float>::epsilon(), std::numeric_limits<float>::epsilon(), 1e4, 1000); // MAGIC //
			// solver.Init(std::numeric_limits<float>::min()*100.0, std::numeric_limits<float>::min()*100.0, 1e4, 1000); // MAGIC // ennel elszall a residual, hiaba csokken folyamatosan, egyszercsak vegtelen lesz flot eseten
			solver.SetResidualNorm(3);
			solver.SetOperator(mx_Gbwd);
			solver.SetPreconditioner(preconditioner);
			solver.Build();
			solver.Verbose(0);  //set the level of verbose output of the solver (0-no output, 2-detailed output, including residual and iteration information)
			
			
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::assemble()
		{
				init_Gminus_vectors();
				init_Cth_vectors();

				// TODO itt lehetne mar meghatarozni EPS_abs-ot
				if(precision.absolute_tolerance_specified == true)
				{
					absolute_tolerance = static_cast<double>(precision.absolute_tolerance);
				}
				else
				{
					if(use_adaptive_timestep == true) absolute_tolerance = std::min(Gxyzmin, 2.0*Cthmin/(max_empty_cycle*timestep)) * static_cast<double>(precision.temperature_tolerance);
					else absolute_tolerance = std::min(Gxyzmin, 2.0*Cthmin/timestep) * static_cast<double>(precision.temperature_tolerance);
				}
				std::cout << "absolute_tolerance=" << absolute_tolerance << std::endl;
				
				assemble_mx_Gminus();
				assemble_mx_Gplus();
				assemble_mx_Gfwd();
				assemble_mx_Gbwd();
				assemble_mx_Cth();
				assemble_mx_GCth();

				assemble_vec_Psum();
				assemble_vec_temperature_map_accelerator();
				assemble_vec_dissipation_map_accelerator();
				assemble_vec_Pboundary();
				// assemble_vec_reciprocal_Cth();
				assemble_vec_dTdt();
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::move_to_accelerator()
		{
			if(paralution::_paralution_available_accelerator() && use_accelerator)
			{
				mx_Gminus.MoveToAccelerator();
				mx_Gplus.MoveToAccelerator();
				mx_Gfwd.MoveToAccelerator();
				mx_Gbwd.MoveToAccelerator();
				mx_Cth.MoveToAccelerator();
				mx_GCth.MoveToAccelerator();

				vec_Psum.MoveToAccelerator();
				vec_temperature_map_accelerator.MoveToAccelerator();
				vec_dissipation_map_accelerator.MoveToAccelerator();
				vec_Pboundary.MoveToAccelerator();
				vec_dTdt.MoveToAccelerator();
				solver.MoveToAccelerator();
			}	
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::reset_mx_GCth(double ts)
		{
			mx_GCth.Zeros();
			mx_GCth.MatrixAdd(mx_Cth);
			mx_GCth.Scale(2.0/ts);
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::reset_mx_Gfwd()
		{
			mx_Gfwd.CopyFrom(mx_Gminus);
			mx_Gfwd.MatrixAdd(mx_GCth);
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::reset_mx_Gbwd()
		{
			mx_Gbwd.CopyFrom(mx_Gplus);
			mx_Gbwd.MatrixAdd(mx_GCth);
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::reset_solver()
		{
			// solver.ReBuildNumeric(); //ez mintha nem kellene???
			solver.Verbose(0); //ezt ujra be kell allitani??
		}

		template<utype_float_t float_enum>
		bool improved_euler_engine_t<float_enum>::over_temperature_threshold()
		{
			float_type max_dTdt;
			max_dTdt = predict_max_temperature_change();
			double delta_time = static_cast<double>(cycle_counter) * timestep;
			return (max_dTdt*delta_time > temperature_threshold);
		}


		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::solve()
		{
			if(paralution::_paralution_available_accelerator() && use_accelerator)
			{				
				vec_dissipation_map_accelerator.CopyFrom(dissipation_map);
				
				mx_Gfwd.Apply(vec_temperature_map_accelerator, &vec_Psum);
				vec_Psum.ScaleAdd2(1.0, vec_dissipation_map_accelerator, 2.0, vec_Pboundary, 2.0);
				solver.Solve(vec_Psum,&vec_temperature_map_accelerator);
				
				temperature_map.CopyFrom(vec_temperature_map_accelerator);
			}
			else
			{
				mx_Gfwd.Apply(temperature_map, &vec_Psum);
				vec_Psum.ScaleAdd2(1.0, dissipation_map, 2.0, vec_Pboundary, 2.0);
				solver.Solve(vec_Psum,&temperature_map);
			}
			dissipation_map.Zeros();
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::calculate_temperatures(double ts)
		{			
			reset_mx_GCth(ts);
			reset_mx_Gfwd();
			reset_mx_Gbwd();

			reset_solver();
			
			if(paralution::_paralution_available_accelerator() && use_accelerator)
			{
				vec_dissipation_map_accelerator.CopyFrom(dissipation_map);
				
				mx_GCth.Apply(vec_temperature_map_accelerator, &vec_Psum);
				vec_Psum.ScaleAdd2(1.0, vec_Pboundary, 2.0, vec_dissipation_map_accelerator, 2.0/static_cast<double>(cycle_counter));
				solver.Solve(vec_Psum, &vec_temperature_map_accelerator);
				
				temperature_map.CopyFrom(vec_temperature_map_accelerator);
			}
			else
			{
				mx_GCth.Apply(temperature_map, &vec_Psum);
				vec_Psum.ScaleAdd2(1.0, vec_Pboundary, 2.0, dissipation_map, 2.0/static_cast<double>(cycle_counter));
				solver.Solve(vec_Psum, &temperature_map);
			}
			dissipation_map.Zeros();			
		}
		
		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::calculate_temperatures()
		{
			if(use_adaptive_timestep)
			{
				cycle_counter++;
				dissipation_map.Scale(1.0/static_cast<double>(cycle_counter));
				if(over_temperature_threshold() || (cycle_counter == max_empty_cycle))
				{	
					double delta_time = static_cast<double>(cycle_counter) * timestep;
					cycle_counter = 0;
					calculate_temperatures(delta_time);
				}
				else
				{
					dissipation_map.Scale(static_cast<double>(cycle_counter));
				}
			}
			else
			{
				solve();
			}
		}

		template<utype_float_t float_enum>
		void improved_euler_engine_t<float_enum>::save_conductivity_matrix(const std::string& file)
		{
			// TODO
		}

		template<utype_float_t float_enum>
		double improved_euler_engine_t<float_enum>::get_timestep()
		{
			return timestep * static_cast<double>(cycle_counter+1);
		}

		template<utype_float_t float_enum>
		double improved_euler_engine_t<float_enum>::get_stability_limit()
		{
			return timestep * static_cast<double>(cycle_counter+1);
		}

		template<utype_float_t float_enum>
		improved_euler_engine_t<float_enum>::improved_euler_engine_t(thermal::sloth::adapter_t* adapter,
						   layout::sloth::adapter_t& layout_structure,
						   std::unordered_map<side_t,std::tuple<boundary_t, double>>& boundary_conditions,
						   paralution::LocalVector<float_type>& dissipation_map,
						   paralution::LocalVector<float_type>& temperature_map,
						   double ambient_temperature,
						   double timestep,
						   bool use_adaptive_timestep,
						   double temperature_threshold,
						   const precision_t& precision,
						   bool use_accelerator)
		:
			engine_t(adapter),
			layout_structure(layout_structure),
			boundary_conditions(boundary_conditions),
			x_pitch(layout_structure.get_pitch()[0]),
			y_pitch(layout_structure.get_pitch()[1]),
			z_pitch(layout_structure.get_pitch()[2]),
			x_size(layout_structure.get_cell_size()[0]),
			y_size(layout_structure.get_cell_size()[1]),
			nodes(x_pitch*y_pitch*z_pitch),
			precision(precision),
			timestep(timestep),
			use_adaptive_timestep(use_adaptive_timestep),
			cycle_counter(0),
			max_empty_cycle(1000),
			temperature_threshold(temperature_threshold),
			// temperature_tolerance(static_cast<double>(precision.temperature_tolerance)),
			Cthmin(0.0),
			Gxyzmin(0.0),
			absolute_tolerance(1e-10),
			relative_tolerance(1e-3),
			dissipation_map(dissipation_map),
			temperature_map(temperature_map),
			ambient_temperature(ambient_temperature),
			use_accelerator(use_accelerator)
		{
			assemble();
			
			reset_mx_GCth(timestep);
			reset_mx_Gfwd();
			reset_mx_Gbwd();

			assemble_solver();

			move_to_accelerator();
		}
		
		template<utype_float_t float_enum>
		improved_euler_engine_t<float_enum>::~improved_euler_engine_t()
		{
			// adapter->debug("~improved_euler_engine_t()");

			// delete lo_res_model;
			solver.Clear();
			preconditioner.Clear();

			mx_Gplus.Clear();
			mx_Gminus.Clear();
			mx_Gfwd.Clear();
			mx_Gbwd.Clear();
			mx_GCth.Clear();
			mx_Cth.Clear();
			
			vec_temperature_map_accelerator.Clear();
			vec_dissipation_map_accelerator.Clear();
			vec_Pboundary.Clear();
			vec_Psum.Clear();
			vec_dTdt.Clear();
			// reciprocal_Cnode.Clear();
			// dTdt.Clear();
		}
				
	}
}

/*
 * explicit template instantiation
*/
template class thermal::sloth::improved_euler_engine_t<static_cast<thermal::sloth::utype_float_t>(thermal::sloth::float_t::snl)>;
template class thermal::sloth::improved_euler_engine_t<static_cast<thermal::sloth::utype_float_t>(thermal::sloth::float_t::dbl)>;