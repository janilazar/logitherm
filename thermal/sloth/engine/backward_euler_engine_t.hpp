#ifndef _SLOTH_BACKWARD_EULER_ENGINE_T_
#define _SLOTH_BACKWARD_EULER_ENGINE_T_

#include <tuple>
#include <vector>
#include <type_traits>

#include "dependency/paralution/paralution.hpp"
#include "thermal/sloth/adapter/adapter_t.hpp"
#include "thermal/sloth/adapter/precision_t.hpp"
#include "thermal/sloth/engine/engine_t.hpp"

namespace logitherm
{
	class manager_t;
}

namespace thermal
{
	namespace sloth
	{
		template<utype_float_t float_enum>
		class backward_euler_engine_t	:	public engine_t
		{
			public:
				using boundary_t = thermal::sloth::boundary_t;
				using side_t = thermal::sloth::side_t;

				typedef typename std::conditional<(float_enum == static_cast<utype_float_t>(float_t::snl)), float, double>::type float_type;
				
			private:				
				// node-ok szama x,y,z koordinata tengely menten
				const size_t x_pitch, y_pitch, z_pitch, nodes;
				
				// egy cella merete
				const double x_size, y_size;
								
				// mx_Gplus konduktancia matrix inicializalashoz szukseges valtozok
				std::vector<int> Gplus_rows, Gplus_cols;
				std::vector<float_type> Gplus_values;

				// mx_GCth  konduktancia mx es Cnode inicializalasahoz szukeses valtozok
				std::vector<int> Cth_rows, Cth_cols;
				std::vector<float_type> Cth_values;
				
				const layout::sloth::adapter_t& layout_structure;
				std::unordered_map<side_t,std::tuple<boundary_t, double>>& boundary_conditions;
				
				const precision_t& precision;

				double timestep;
				// double temperature_tolerance;
				double Cthmin;
				double Gxyzmin;
				double absolute_tolerance;
				double relative_tolerance;
				
				paralution::LocalVector<float_type>& temperature_map; /** temperature map, az adat az adapter-ben van lefoglalva **/
				paralution::LocalVector<float_type>& dissipation_map; /** dissipation map, az adat az adapter-ben van lefoglalva **/

				
				double ambient_temperature;

				bool use_adaptive_timestep;

				size_t cycle_counter;
				size_t max_empty_cycle;
				double temperature_threshold;

				const bool use_accelerator;

				// std::ostream* boundary_current_stream;
				// std::ostream* capacity_current_stream;
				// std::ostream* dissipation_stream;

				// a teljes rendszer konduktancia mx-a mx_Gbwd*Tnode = Pnode
				paralution::LocalMatrix<float_type> mx_Gbwd;
				
				// DC komponenseket tartalmazo konduktancia mx, kapacok helyettesitokepe nincs benne
				paralution::LocalMatrix<float_type> mx_Gplus; /** konduktancia mx, amiben nincs benne a kapacok helyettesitokepe **/
				
				// konduktancia mx, kapacok helyettesitokepe
				paralution::LocalMatrix<float_type> mx_GCth;

				// konduktancia mx, kapacok helyettesitokepe
				paralution::LocalMatrix<float_type> mx_Cth;
				
				paralution::LocalVector<float_type> vec_Psum;

				// a node-ok homersekletet tartalmazo vektor, ACCELERATOR ONLY
				paralution::LocalVector<float_type> vec_temperature_map_accelerator;
				
				// a node-ok teljes disszipaciojat tartalmazo vektor, ACCELERATOR ONLY
				paralution::LocalVector<float_type> vec_dissipation_map_accelerator;
				
				// a peremfeltetelekbol adodo aram
				paralution::LocalVector<float_type> vec_Pboundary;
				
				// a kapacitasok aramgeneratora a helyettesitokepben
				// paralution::LocalVector<float_type> Pcap;
				
				// az egyes nodeok hokapacitasa
				paralution::LocalVector<float_type> vec_Cth;
				paralution::LocalVector<float_type> vec_reciprocal_Cth;

				// homerseklet derivalt az adaptiv idoleptekhez
				paralution::LocalVector<float_type> vec_dTdt;

				// paralution::LocalVector<float_type> Ppredictor; /** dissipation map, az adat az adapter-ben van lefoglalva **/
				
				// SOLVER
				paralution::CG<paralution::LocalMatrix<float_type>,paralution::LocalVector<float_type>, float_type> solver;
				
				// RPECONDITIONER
				paralution::TNS<paralution::LocalMatrix<float_type>, paralution::LocalVector<float_type>, float_type> preconditioner;
				// paralution::MultiColoredILU<paralution::LocalMatrix<float_type>, paralution::LocalVector<float_type>, float_type> preconditioner;

			private:
				
				std::tuple<double,double> calculate_boundary(side_t side, size_t x, size_t y, size_t z);

				/** x,y,z iranyu konduktanciakat hatarozza meg a meretek, anyagparameterek alapjan **/
				std::tuple<double,double,double> get_material_conductivity(size_t x, size_t y, size_t z);
				
				/** kapacitas helyettesitokepeben levo konduktanciat hatarozza meg **/
				double get_thermal_capacity_conductivity(size_t x, size_t y, size_t z);

				// inicializalja a dc_cols es dc_values vektorokat
				void init_Gplus_vectors();

				// inicializalja a cap_cols es cal_values vektorokat
				void init_Cth_vectors();

				// letrehozza a teljes strukturat leiro mx-ot
				void assemble_mx_Gbwd();

				// letrehozza a mx_Gplus matrixot
				void assemble_mx_Gplus();

				// letrehozza a mx_GCth matrixot
				void assemble_mx_Cth();

				// letrehozza a mx_GCth matrixot
				void assemble_mx_GCth();

				void assemble_vec_Psum();

				// letrehozza a Tnode vektort
				void assemble_vec_temperature_map_accelerator();

				// letrehozza a Pnode vektort
				void assemble_vec_dissipation_map_accelerator();

				// letrehozza a Pbound vektort, acceleratorra viszi ha kell
				void assemble_vec_Pboundary();

				// letrehozza a Cnode vektort, acceleratorra viszi ha kell
				void assemble_vec_Cth_vec_reciprocal_Cth();
				
				// letrehozza a 
				void assemble_vec_dTdt();

				void assemble_solver();

				// meghivja az osszes assemble fv-t
				void assemble();

				void move_to_accelerator();

				// timestepnek megfelelo mx_Gbwd mx-ot allit ossze mx_Gplus es mx_GCth alapjan
				void reset_mx_Gbwd();

				// timestepnek es Cnode-nak megfelelo mx_GCth-et allit be
				void reset_mx_GCth(double ts);

				void reset_solver();

				bool over_temperature_threshold();

				float_type predict_max_temperature_change();

				void solve();
				void solve(double ts);

				void print_boundary_current();
				void print_capacity_current();
				void print_dissipation();
				
			public:
				/** default konstruktor letiltva **/
				backward_euler_engine_t() = delete;
				
				/** konstuktor, x,y,z felbontas, xy meret, reteg vastagsagok, anyagok parameterei **/
				backward_euler_engine_t(thermal::sloth::adapter_t* adapter,
						 layout::sloth::adapter_t& layout_structure,
						 std::unordered_map<side_t,std::tuple<boundary_t, double>>& boundary_conditions,
						 paralution::LocalVector<float_type>& dissipation_map,
						 paralution::LocalVector<float_type>& temperature_map,
						 double ambient_temperature,
						 double timestep,
						 bool use_adaptive_timestep,
						 double temperature_threshold,
						 const precision_t& precision,
						 bool use_accelerator);
				
				/** egyenlet megoldasa, ket path**/
				void calculate_temperatures();

				/** ha logaritmikus az idoleptek **/
				void calculate_temperatures(double timestep);

				void save_conductivity_matrix(const std::string& file);

				double get_timestep() override;
				double get_stability_limit() override;

				~backward_euler_engine_t();
			
		};
	}
}

#endif //_SLOTH_BACKWARD_EULER_ENGINE_T_
