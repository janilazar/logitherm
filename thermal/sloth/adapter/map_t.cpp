#include <string>
#include <tuple>
#include <algorithm>
#include "thermal/sloth/adapter/map_t.hpp"
#include "dependency/paralution/paralution.hpp"
#include "thermal/sloth/adapter/adapter_t.hpp"

namespace thermal
{
	namespace sloth
	{

		map_t::map_t(thermal::sloth::adapter_t* thermal_adapter, const precision_t& precision)
		:
			nodes(0),
			allocated(false),
			thermal_adapter(thermal_adapter),
			precision(precision)
		{}

		map_t::map_t(const std::string& name, size_t nodes, thermal::sloth::adapter_t* thermal_adapter, const precision_t& precision)
		:
			nodes(nodes),
			allocated(true),
			thermal_adapter(thermal_adapter),
			precision(precision)
		{
			switch(precision.float_type)
			{
				case float_t::snl:
					map_fl.Allocate(name, nodes);
					break;
				case float_t::dbl:
					map_db.Allocate(name, nodes);
					break;
			}
		}

		map_t::~map_t()
		{
			switch(precision.float_type)
			{
				case float_t::snl:
					map_fl.Clear();
					break;
				case float_t::dbl:
					map_db.Clear();
					break;
			}
		}

		bool map_t::is_allocated() const
		{
			return allocated;
		}

		void map_t::allocate(const std::string& name, size_t nodes)
		{
			if(allocated == true) thermal_adapter->error("map_t::allocate(): map is already allocated");
			else
			{
				allocated = true;
				// this->precision = precision;
				this->nodes = nodes;
				switch(precision.float_type)
				{
					case float_t::snl:
						map_fl.Allocate(name, nodes);
						break;
					case float_t::dbl:
						map_db.Allocate(name, nodes);
						break;
				}
			}
		}

		void map_t::set_values(double value)
		{
			if(allocated == false) thermal_adapter->error("map_t::set_values(): map must be allocated first");
			switch(precision.float_type)
			{
				case float_t::snl:
					map_fl.SetValues(value);
					break;
				case float_t::dbl:
					map_db.SetValues(value);
					break;
			}
		}

		void map_t::set_zeros()
		{
			if(allocated == false) thermal_adapter->error("map_t::set_zeros(): map must be allocated first");
			switch(precision.float_type)
			{
				case float_t::snl:
					map_fl.Zeros();
					break;
				case float_t::dbl:
					map_db.Zeros();
					break;
			}
		}

		void map_t::set_value(size_t index, double value)
		{
			if(allocated == false) thermal_adapter->error("map_t::set_value(): map must be allocated first");
			switch(precision.float_type)
			{
				case float_t::snl:
					map_fl[index] = static_cast<float>(value);
					break;
				case float_t::dbl:
					map_db[index] = value;
					break;
			}
		}

		double map_t::get_value(size_t index) const
		{
			if(allocated == false) thermal_adapter->error("map_t::get_value(): map must be allocated first");
			switch(precision.float_type)
			{
				case float_t::snl:
					return static_cast<double>(map_fl[index]);
				case float_t::dbl:
					return map_db[index];
			}
			thermal_adapter->error("map_t::get_value(): unkown precision");
			return 0.0;
		}

		std::tuple<double, double> map_t::min_max(size_t begin, size_t end) const
		{
			if(allocated == false) thermal_adapter->error("map_t::min_max(): map must be allocated first");
			switch(precision.float_type)
			{
				case float_t::snl:
				{
					auto minmax_it = std::minmax_element(&(map_fl[0]) + begin, &(map_fl[0]) + end);
					return std::tuple<double, double>(*minmax_it.first, *minmax_it.second);
				}
				case float_t::dbl:
				{
					auto minmax_it = std::minmax_element(&(map_db[0]) + begin, &(map_db[0]) + end);
					return std::tuple<double, double>(*minmax_it.first, *minmax_it.second);
				}
			}
			thermal_adapter->error("map_t::min_max(): unkown precision");
			return std::make_tuple(0.0,0.0);
		}

	}
}