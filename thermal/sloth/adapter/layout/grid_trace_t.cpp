#include "thermal/sloth/adapter/layer_dissipation_trace_t.hpp"
#include "thermal/sloth/adapter/adapter_t.hpp"
#include "thermal/sloth/adapter/layout/component_t.hpp"
#include "manager/manager_t.hpp"

namespace layout
{
	namespace sloth
	{
		template<float_type>
		layer_dissipation_trace_t::layer_dissipation_trace_t(const std::string& path, const std::string& layer_id, const std::string& id, const std::string& posfix, thermal::sloth::adapter_t* thermal_adapter)
		:
			util::trace_t(path, layer_id, id, postfix),
			counter(0),
			layer_id(layer_id),
			thermal_adapter(thermal_adapter),
			data(data)
			// timestep(thermal_adapter->timestep)
		{
			if("" == id) thermal_adapter->error("layer_dissipation_trace_t(): invalid trace id");
		}

		template<float_type>
		void layer_dissipation_trace_t::initialize(std::ostream& os)
		{			
			// the_layout = dynamic_cast<layout::sunred::adapter_t*>(logitherm::manager_t::get_manager()->get_thermal_adapter()->get_layout());
			// if(the_layout == nullptr) throw("Invalid layout type.");
			
			// thermal_adapter = dynamic_cast<thermal::sunred::adapter_t*>(logitherm::manager_t::get_manager()->get_thermal_adapter());
			// if(nullptr == thermal_adapter) throw("Invalid thermal engine type.");
			
			
			unsigned char size_t_size = sizeof(size_t);
			size_t frame_number = 0;
			if(thermal_adapter->get_layout() == nullptr) thermal_adapter->error("layer_dissipation_trace_t::initialize(): layout is nullptr");

			size_t width = thermal_adapter->get_layout()->get_pitch()[0];
			size_t height = thermal_adapter->get_layout()->get_pitch()[1];
			
			/** print header **/
			os.write(reinterpret_cast<const char*>(&size_t_size), sizeof(size_t_size));
			os.write(reinterpret_cast<const char*>(&frame_number),size_t_size);
			os.write(reinterpret_cast<const char*>(&width),size_t_size);
			os.write(reinterpret_cast<const char*>(&height),size_t_size);
			//ide bele kell irni a felbontast
			//std::fstream s(my_file_path); // use option std::ios_base::binary if necessary s.seekp(position_of_data_to_overwrite, std::ios_base::beg); s.write(my_data, size_of_data_to_overwrite);
		}

		template<float_type>
		void layer_dissipation_trace_t::trace(std::ostream& os)
		{
			//std::cerr << "temperature_dist_trace_t" << std::endl;
			
			//std::string file = path + prefix + "." + type + "_" + std::to_string(counter) + ".map"; 
			os.write(reinterpret_cast<const char*>(&counter),sizeof(size_t));
			++counter;
			// get_layer_index()*width*height 
			// size_t from = thermal_adapter->get_layer_index()*width*height;
			// size_t length = width*height;
			// thermal_adapter->get_layer_temperature(os, layer_id);
			// thermal::sloth::adapter_t* thermal_adapter = dynamic_cast<thermal::sloth::adapter_t*>(adapter->adapter);
			thermal_adapter->print_dissipation_map(os, layer_id);

			// for(size_t it = 0; it < length; ++it)
			// {
			// 	double val = data[from + it];
			// 	os.write(reinterpret_cast<const char*>(&val), sizeof(double));
			// }
		}

		template<float_type>
		void layer_dissipation_trace_t::reset()
		{}
		
		template<float_type>
		void layer_dissipation_trace_t::finalize(std::ostream& os)
		{
			os.seekp(std::ios::beg+sizeof(unsigned char));
			os.write(reinterpret_cast<const char*>(&counter),sizeof(size_t));
			//std::cerr << "temp_dist finalize" << std::endl;
		}
	} // namespace sloth 
} // namespace thermal

/*
 * explicit template instantiation
*/
template class layout::sloth::layer_dissipation_trace_t<float>;
template class layout::sloth::layer_dissipation_trace_t<double>;