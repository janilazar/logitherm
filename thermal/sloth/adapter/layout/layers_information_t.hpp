#ifndef _SLOTH_LAYERS_INFORMATION_
#define _SLOTH_LAYERS_INFORMATION_


#include <string>
#include <unordered_map>

#include "thermal/sloth/adapter/material_t.hpp"
#include "thermal/sloth/adapter/adapter_t.hpp"
#include "util/log_t.hpp"
#include "unit/all_units.hpp"

namespace layout
{
	namespace sloth
	{
		class layers_information_t
		{
			private:
				/** injection dependency **/
				layout::adapter_t* const adapter;
				util::log_t* const log;
				
				/** layer id, index **/
				std::unordered_map<std::string, size_t> layers;
				
				/** layer thickness in the index order **/
				std::vector<::unit::length_t> thickness;

				/** layer id, index**/
				std::unordered_map<std::string, size_t> active_layers;

				unit::length_t total_thickness;
				
				/** layer-ek anyagai **/
				std::vector<::thermal::sloth::material_t> materials;
			
			public:
				layers_information_t() = delete;

				layers_information_t(layout::adapter_t* adapter, util::log_t* log)
				:
					adapter(adapter),
					log(log),
					total_thickness(0_um)
				{}

				layers_information_t(const layers_information_t& other)
				:
					adapter(other.adapter),
					log(other.log),
					layers(other.layers),
					thickness(other.thickness),
					active_layers(other.active_layers),
					total_thickness(other.total_thickness),
					materials(other.materials)
				{}
				
				/**
				 * ez a fuggveny hozzaad egy layert a hierarchiahoz
				 * ellenorzes: a kapott azonosito egyedi, meg nincs eltarolva
				**/ 
				void add_layer(const std::string& id, const ::thermal::sloth::material_t& material,const ::unit::length_t& thickness)
				{
					if(layers.find(id) != layers.end())
					{
						log->warning("layout::sloth::layers_information_t::add_layer(): layer '" + id + "' is already in the layout layer's hierarchy");
					}
					else
					{
						layers.insert(std::make_pair(id,layers.size()));
						this->thickness.push_back(thickness);
						total_thickness += thickness;
						materials.push_back(material);
					}
				}
				
				bool find_layer(const std::string& id) const
				{
					if(layers.find(id) == layers.end()) return false;
					else return true;
				}
				
				size_t get_layer_index(const std::string& id) const
				{
					if(layers.find(id) == layers.end()) log->error("layout::sloth::layers_information_t::get_layer_index(): layer '" + id + "' is not found");
					return layers.find(id)->second;
				}
				
				void set_layer_active(const std::string& id)
				{
					if(layers.find(id) == layers.end())
					{
						log->warning("layout::sloth::set_layer_active(): layer '" + id + "' is not found");
					}
					else
					{
						active_layers.insert(std::make_pair(id, layers.find(id)->second));
					}
				}
				
				const std::unordered_map<std::string, size_t>& get_active_layers()
				{
					return active_layers;
				}
				
				unit::length_t get_layer_thickness(const std::string& id) const
				{
					if(layers.find(id) == layers.end()) log->error("layout::sloth::layers_information_t::get_layer_thickness(): layer '" + id + "' is not found");
					return thickness.at(layers.find(id)->second);
				}
				
				unit::length_t get_layer_thickness(size_t index) const
				{
					if(thickness.size() < index + 1) log->error("layout::sloth::layers_information_t::get_layer_thickness(): invalid layer index.");
					return thickness.at(index);
				}
				
				const thermal::sloth::material_t& get_layer_material(const std::string& id) const
				{
					if(layers.find(id) == layers.end()) log->error("layout::sloth::layers_information_t::get_layer_material(): layer '" + id + "' is not found");
					return materials.at(layers.find(id)->second);
				}
				
				const thermal::sloth::material_t& get_layer_material(size_t index) const
				{
					if(materials.size() < index + 1) log->error("layout::sloth::layers_information_t::get_layer_material(): invalid layer index");
					return materials.at(index);
				}
				
				size_t get_size() const
				{
					return layers.size();
				}

				unit::length_t get_thickness() const
				{
					return total_thickness;
				}
		};
	} //namespace sunred
} //namespace layout
#endif //_LAYOUT_SUNRED_LAYERS_INFORMATION_
