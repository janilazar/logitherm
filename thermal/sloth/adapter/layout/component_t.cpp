#include "thermal/sloth/adapter/layout/component_t.hpp"
#include "manager/manager_t.hpp" //nem lesz para, mert csak a cpp-re van meghivva a dolog->ilyenkor nincs utkozes (remelem)

#include "thermal/sloth/adapter/layout/adapter_t.hpp"
#include "thermal/base/layout/layer_temperature_trace_t.hpp"

#include "thermal/sloth/adapter/layout/layer_boundary_current_trace_t.hpp"
#include "thermal/sloth/adapter/layout/layer_capacity_current_trace_t.hpp"
#include "thermal/sloth/adapter/layout/layer_dissipation_trace_t.hpp"

#include <iostream>

namespace layout
{
	namespace sloth
	{
		/**
		 * konstruktor, ami a nev, relativ pozicio, meret es a alyer szama alapjan hoz letre egy layout component-et
		**/ 
		component_t::component_t(const std::string& id, const std::string& layer_id, xy_length_t position, xy_length_t sides,  component_t* const parent, layout::sloth::adapter_t* adapter)
		:	
			layout::component_t(adapter, id, parent, position, sides),
			layer_id(layer_id)
		{

			if(!adapter->find_layer(layer_id))
			{
				error("layout::sloth::component_t(): could not find the layer '" + layer_id + "' for component '" + id + "' in the layout structure");
			}
			
			adapter->layers.set_layer_active(layer_id);

			if(!adapter->component_fits(this))
			{
				warning("add_component(): layout component '" + id + "' does not fit in the layout");
			}

			xy_length_t absolute_position = get_absolute_position();
			xy_length_t cell_size = adapter->cell_size;
			
			pitch_position = xy_pitch_t(static_cast<size_t>(absolute_position[0]/(cell_size[0])),static_cast<size_t>(absolute_position[1]/(cell_size[1])));
			
			xy_length_t side = get_side_lengths();

			size_t pitch_size_x = static_cast<size_t>((absolute_position[0] + side[0])/cell_size[0]) - pitch_position[0] + 1; //itt kellene egyet hozzaadni, hogy belepjen a for ciklusokba, es az jol mukodjon
			size_t pitch_size_y = static_cast<size_t>((absolute_position[1] + side[1])/cell_size[1]) - pitch_position[1] + 1; //itt kellene egyet hozzaadni, hogy belepjen a for ciklusokba, es az jol mukodjon

			pitch_size = xy_pitch_t(pitch_size_x, pitch_size_y);
			
			size_t pitch_center_x = static_cast<size_t>((absolute_position[0] + side[0])/(cell_size[0]*2.0));
			size_t pitch_center_y = static_cast<size_t>((absolute_position[1] + side[1])/(cell_size[1]*2.0));
			pitch_center = xy_pitch_t(pitch_center_x,pitch_center_y);

			size_t x_start = pitch_position[0];
			size_t x_end = x_start + pitch_size[0];

			size_t y_start = pitch_position[1];
			size_t y_end = y_start + pitch_size[1];

			area_array = new unit::area_t[pitch_size[0]*pitch_size[1]];

			size_t index = 0;
			for(size_t y_index = y_start; y_index < y_end; ++y_index)
			{
				for(size_t x_index = x_start; x_index < x_end; ++x_index)
				{

					unit::length_t x_size = cell_size[0];
					unit::length_t y_size = cell_size[1];

					unit::length_t cell_x_pos = cell_size[0]*static_cast<double>(x_index);
					unit::length_t cell_y_pos = cell_size[1]*static_cast<double>(y_index);


					//TODO ez itt nem fasza, mert ha a vege ugyanabba a cellaba esik, mint az eleje, akkor alig ad hozza disszipaciot **/

					/** ki kell szamitani, hogy a component teruletenek mekkora resze esik az adott cellaba **/
					if(cell_x_pos < absolute_position[0]) x_size = x_size - (absolute_position[0] - cell_x_pos); //bal szel
					if (absolute_position[0] + side[0] < cell_x_pos + cell_size[0]) x_size = x_size - (cell_x_pos + cell_size[0] - (absolute_position[0] + side[0])); //jobb szel
					
					if(cell_y_pos < absolute_position[1]) y_size = y_size - (absolute_position[1] - cell_y_pos); //felso szel
					if (absolute_position[1] + side[1] < cell_y_pos + cell_size[1]) y_size = y_size - (cell_y_pos + cell_size[1] - (absolute_position[1] + side[1])); //also szel

					area_array[index++] = x_size * y_size;
				}
			}
		}
		
		component_t::~component_t()
		{
			delete[] area_array;
		}

		layer_temperature_trace_t* component_t::add_layer_temperature_trace(const std::string& path, const std::string& postfix)
		{
			layer_temperature_trace_t* ptr = new layer_temperature_trace_t(path, layer_id, postfix, adapter->adapter);
			tracer.add_trace(ptr, std::ios::out | std::ios::binary);
			trace_component();
			return ptr;
		}

		layer_boundary_current_trace_t* component_t::add_layer_boundary_current_trace(const std::string& path, const std::string& postfix)
		{
			// itt kene valahogy szolni enginenek v adapternek, hogy foglalja le a buffert

			thermal::sloth::adapter_t* thermal_adapter = dynamic_cast<thermal::sloth::adapter_t*>(adapter->adapter);
			if(thermal_adapter == nullptr) error("add_layer_boundary_current_trace(): thermal adapter is nullptr or invalid type");
			
			thermal_adapter->reserve_layer_boundary_current_buffer();
			layer_boundary_current_trace_t* ptr = new layer_boundary_current_trace_t(path, layer_id, postfix, thermal_adapter, this);
			tracer.add_trace(ptr, std::ios::out | std::ios::binary);
			trace_component();
			return nullptr;
		}

		layer_capacity_current_trace_t* component_t::add_layer_capacity_current_trace(const std::string& path, const std::string& postfix)
		{
			// itt kene valahogy szolni enginenek v adapternek, hogy foglalja le a buffert
			thermal::sloth::adapter_t* thermal_adapter = dynamic_cast<thermal::sloth::adapter_t*>(adapter->adapter);
			if(thermal_adapter == nullptr) error("add_layer_capacity_current_trace(): thermal adapter is nullptr or invalid type");
			
			thermal_adapter->reserve_layer_capacity_current_buffer();
			layer_capacity_current_trace_t* ptr = new layer_capacity_current_trace_t(path, layer_id, postfix, thermal_adapter, this);
			tracer.add_trace(ptr, std::ios::out | std::ios::binary);
			trace_component();
			return ptr;
		}
		
		layer_dissipation_trace_t* component_t::add_layer_dissipation_trace(const std::string& path, const std::string& postfix)
		{
			// itt kene valahogy szolni enginenek v adapternek, hogy foglalja le a buffert
			thermal::sloth::adapter_t* thermal_adapter = dynamic_cast<thermal::sloth::adapter_t*>(adapter->adapter);
			if(thermal_adapter == nullptr) error("add_layer_dissipation_trace(): thermal adapter is nullptr or invalid type");

			thermal_adapter->reserve_layer_dissipation_buffer();
			layer_dissipation_trace_t* ptr = new layer_dissipation_trace_t(path, layer_id, postfix, thermal_adapter, this);
			tracer.add_trace(ptr, std::ios::out | std::ios::binary);
			trace_component();
			return ptr;
		}

		/**
		 * kapott ostream-re kirakja a component es leszarmazottak nevet
		**/ 
		std::ostream& component_t::print_component(std::ostream& os, std::string indent)
		{
			os << indent << id << " position=" << get_absolute_position() << " size=" << get_side_lengths() << std::endl;
			if(!children.empty())
			{
				indent += "  "; //hulye vagyok, rekurziv bejarasnal kell melyseginformacio
				for(auto &it: children)
				{
					layout::sloth::component_t* child_ptr = dynamic_cast<::layout::sloth::component_t*>(it.second);
					if(nullptr == child_ptr)
					{
						error("layout::slothcomponent_t::print_component(): layout component '" + it.second->id + "' has invalid type.");
					}
					child_ptr->print_component(os, indent);
				}
			}
			return os;
		}
	} //namespace sloth
} //namespace layout

