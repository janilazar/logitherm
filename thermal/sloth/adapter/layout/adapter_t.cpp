#include "thermal/sloth/adapter/layout/adapter_t.hpp"
#include "util/log_t.hpp"

namespace layout
{
	namespace sloth
	{
		
		/**
		 * ellenorzi, hogy a component elfer-e a layout-on
		 * true-val ter vissza, ha elfer
		 * hibaellenorzes: component nincs benne a layout_component_ts_tree-ben
		**/
		bool adapter_t::component_fits(const component_t* component)
		{
			if(!search_for_component(component->id))
			{
				warning("component_fits(): layout component '" + component->id + "' is not in the tree");
				return false;
			}
			else
			{
				/** coordinates<> operator< hasznalom **/
				return (component->get_absolute_position() + component->get_side_lengths()) < layout_size;
			}
		}
		
		/**
		 * visszater a layout x, y, es z iranyu meretevel
		**/ 
		xyz_pitch_t adapter_t::get_pitch()
		{
			return xyz_pitch_t(resolution[0], resolution[1], layers.get_size());
		}

		/**
		 * visszater a layout x, y, es z iranyu meretevel
		**/ 
		xyz_length_t adapter_t::get_length()
		{
			return xyz_length_t(layout_size[0], layout_size[1], layers.get_thickness());
		}
		
		void adapter_t::add_layer(const std::string& layer_name, const ::thermal::sloth::material_t& material, const ::unit::length_t& thickness)
		{
			layers.add_layer(layer_name, material, thickness);
		}

		bool adapter_t::find_layer(const std::string& layer_id)
		{
			return layers.find_layer(layer_id);		
		}
		
		size_t adapter_t::get_layer_index(const std::string& layer_id)
		{
			return layers.get_layer_index(layer_id);		
		}
		
		unit::length_t adapter_t::get_layer_thickness(const std::string& layer_id) const
		{
			return layers.get_layer_thickness(layer_id);		
		}
		
		unit::length_t adapter_t::get_layer_thickness(size_t index) const
		{
			return layers.get_layer_thickness(index);		
		}
		
		const thermal::sloth::material_t& adapter_t::get_layer_material(const std::string& id) const
		{
			return layers.get_layer_material(id);
		}
		
		const thermal::sloth::material_t& adapter_t::get_layer_material(size_t index) const
		{
			return layers.get_layer_material(index);
		}

		void adapter_t::set_layer_active(const std::string& layer_id)
		{
			layers.set_layer_active(layer_id);
		}

		const std::unordered_map<std::string, size_t>& adapter_t::get_active_layers()
		{
			return layers.get_active_layers();
		}
		
		const xy_length_t& adapter_t::get_cell_size()
		{
			return cell_size;
		}
		
		/**
		 * meret alapjan inicializalodik a layout, nem lehet ezutan modositani a meretet
		**/ 
		adapter_t::adapter_t(thermal::adapter_t* adapter, util::log_t* log, const xy_length_t& size, const xy_pitch_t& resolution)
		:
			layout::adapter_t(adapter, log, "layout::sloth::adapter_t"),
			layout_size(size),
			resolution(resolution),
			cell_size(xy_length_t(size[0]/resolution[0], size[1]/resolution[1])),
			layers(this, log)
		{}

		adapter_t::adapter_t(const adapter_t& other, thermal::adapter_t* adapter, util::log_t* log)
		:
			layout::adapter_t(other, adapter, log, "layout::sloth::adapter_t"),
			layout_size(other.layout_size),
			resolution(other.resolution),
			cell_size(other.cell_size),
			layers(other.layers)
		{}

		adapter_t::~adapter_t()
		{}
		
	} //namespace sloth	
} //namespace layout
