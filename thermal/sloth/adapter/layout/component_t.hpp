#ifndef _LAYOUT_SLOTH_COMPONENT_
#define _LAYOUT_SLOTH_COMPONENT_

#include "thermal/base/layout/component_t.hpp"
//#include "thermal/sloth/adapter/layout/component_information_t.hpp"
#include "unit/all_units.hpp"

namespace layout
{
	class layer_temperature_trace_t;
	
	namespace sloth
	{
		class adapter_t;
		
		class layer_boundary_current_trace_t;
		class layer_capacity_current_trace_t;
		class layer_dissipation_trace_t;

		class component_t	:	public layout::component_t//,
								//public ::layout::sloth::component_information_t
		{
			public:
				const std::string layer_id;

				xy_pitch_t pitch_position;
				xy_pitch_t pitch_size;
				xy_pitch_t pitch_center;

				unit::area_t* area_array;

			public:
				component_t() = delete;
				
				/**
				 * konstruktor, ami a nev, relativ pozicio, meret es a layer szama alapjan hoz letre egy layout component-et
				**/ 
				component_t(const std::string& id, const std::string& layer_id, xy_length_t position, xy_length_t sides, component_t* const parent, layout::sloth::adapter_t* adapter = nullptr);

				~component_t();

				layer_temperature_trace_t* add_layer_temperature_trace(const std::string& path, const std::string& postfix = "") override;
				
				layer_boundary_current_trace_t* add_layer_boundary_current_trace(const std::string& path, const std::string& postfix = "");
				layer_capacity_current_trace_t* add_layer_capacity_current_trace(const std::string& path, const std::string& postfix = "");
				layer_dissipation_trace_t* 		add_layer_dissipation_trace(const std::string& path, const std::string& postfix = "");
					

				/**
				 * visszaadja az adott komponens abszolut poziciojat a layout-on (xy pozicio)
				**/	
				//xy_length_t get_absolute_position() const;

				/**
				 * vizszintes tengelyre tukrozi a layout_component-et
				 * ez vonatkozik az osszes benne levo layout_component-re is
				**/
				//void reflect_horizontal();

				/**
				 * fuggoleges tengelyre tukrozi a layout_component-et
				 * ez vonatkozik az osszes benne levo layout_component-re is
				**/
				//void reflect_vertical();
			
				/**
				 * kiszamitja a meretet a layout_component-nek
				 * ehhez vegigmegy az osszes child-on, es azoknak is kiszamitja
				 * ha children ures, akkor a size-zal ter vissza
				 * warning message: ha egy child kilog a layout-bol
				**/
				//xy_length_t calculate_size();

				// unit::area_t calculate_area();
			
				/**
				 * kapott ostream-re kirakja a component es leszarmazottak nevet
				**/ 
				std::ostream& print_component(std::ostream& os, std::string indent = "") override;
			
		};
		
	} //namespace sunred
} //namespace layout

#endif //_LAYOUT_SUNRED_COMPONENT_
