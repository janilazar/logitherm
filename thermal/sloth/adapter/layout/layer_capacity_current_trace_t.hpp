#ifndef _LAYER_CAPACITY_CURRENT_TRACE_T_
#define _LAYER_CAPACITY_CURRENT_TRACE_T_

// #include "thermal/base/adapter/adapter_t.hpp"
// #include "thermal/base/adapter/layout/adapter_t.hpp"
#include "util/trace_t.hpp"
// #include "util/timestep_t.hpp"
// #include "dependency/paralution/paralution.hpp"


namespace thermal
{
	namespace sloth
	{
		class adapter_t;
	}
}

namespace layout
{
	namespace sloth
	{
		class component_t;

		class layer_capacity_current_trace_t	:	public util::trace_t
		{
			private:
				size_t counter;
				const std::string layer_id;
				thermal::sloth::adapter_t* thermal_adapter;
				layout::sloth::component_t* layout_component;
				// const util::timestep_t& timestep;
				// paralution::LocalVector<float_type>& data;
				
			public:
				/**
				 * path -> file eleresi utvonala
				 * prefix -> trace nevenek eleje, jellemzoen a modul neve
				 * id -> trace azonositoja
				**/ 
				layer_capacity_current_trace_t(const std::string& path, const std::string& layer_id, const std::string& postfix, thermal::sloth::adapter_t* thermal_adapter, layout::sloth::component_t* component);
				
				void initialize(std::ostream& os) override;
				
				void trace(std::ostream& os) override;
				
				void reset() override;
				
				void finalize(std::ostream& os) override;
				
		};
	}
} //namespace thermal

#endif //_LAYER_CAPACITY_CURRENT_TRACE_T_
