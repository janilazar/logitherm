#include "thermal/sloth/adapter/adapter_t.hpp"
#include "thermal/sloth/adapter/map_t.hpp"
#include "manager/manager_t.hpp"
#include "thermal/sloth/engine/engine_t.hpp"
#include "thermal/sloth/engine/forward_euler_engine_t.hpp"
#include "thermal/sloth/engine/backward_euler_engine_t.hpp"
#include "thermal/sloth/engine/improved_euler_engine_t.hpp"
#include "util/log_t.hpp"

#include "file_io/sloth_sunred/adapter_t.hpp"

namespace thermal
{
	namespace sloth
	{	
		// precision_t map_t::precision = precision_t::dbl;

		//const char* side_t_string [] = {"east", "south", "west", "north", "top", "bottom"};

		size_t adapter_t::instance_counter = 0;
		
		std::unordered_map<side_t, std::string> adapter_t::side_t_2_string = {{side_t::east, "east"},
																		{side_t::south, "south"},
																		{side_t::west, "west"},
																		{side_t::north, "north"},
																		{side_t::top, "top"},
																		{side_t::bottom,"bottom"}};
				
		std::unordered_map<std::string, side_t> adapter_t::string_2_side_t = {{"east",side_t::east},
																		{"south", side_t::south},
																		{"west", side_t::west},
																		{"north", side_t::north},
																		{"top", side_t::top},
																		{"bottom", side_t::bottom}};
		
		std::unordered_map<std::string, boundary_t> adapter_t::string_2_boundary_t = {{"temperature", boundary_t::temperature},
																				{"current", boundary_t::current},
																				{"adiabatic", boundary_t::adiabatic},
																				{"conductive", boundary_t::conductive}};

		void adapter_t::create_layout(const xy_length_t& size, const xy_pitch_t& pitch)
		{
			layout_structure = new layout::sloth::adapter_t(this, log, size, pitch);
			auto* sloth_layout = dynamic_cast<layout::sloth::adapter_t*>(layout_structure);
			
			// layout_size = xy_length_t(sloth_layout->get_length()[0],sloth_layout->get_length()[1]);
			// cell_size = xy_length_t(layout_size[0]/pitch[0],layout_size[1]/pitch[1]);
		}
			
		void adapter_t::create_structure()
		{
			layout::sloth::adapter_t* layout_ptr = dynamic_cast<layout::sloth::adapter_t*>(layout_structure);
			if(nullptr == layout_ptr) error("create_structure(): Invalid layout format or layout has not been created.");
			
			x_pitch = layout_ptr->get_pitch()[0];
			y_pitch = layout_ptr->get_pitch()[1];
			z_pitch = layout_ptr->get_pitch()[2];

			if((x_pitch == 0) || (y_pitch == 0)) error("create_structure(): layout size must be greater then 0,0.");
			if (z_pitch == 0) error("create_structure(): there must be at least 1 layer");
			
			nodes = x_pitch * y_pitch * z_pitch;
			
			// Creating SUNRED boundary conditions
			if (boundary_conditions.find(side_t::west) == boundary_conditions.end()) boundary_conditions.insert(std::make_pair(side_t::west, std::make_tuple(boundary_t::adiabatic, 0.0)));
			if (boundary_conditions.find(side_t::east) == boundary_conditions.end()) boundary_conditions.insert(std::make_pair(side_t::east, std::make_tuple(boundary_t::adiabatic, 0.0)));
			if (boundary_conditions.find(side_t::south) == boundary_conditions.end()) boundary_conditions.insert(std::make_pair(side_t::south, std::make_tuple(boundary_t::adiabatic, 0.0)));
			if (boundary_conditions.find(side_t::north) == boundary_conditions.end()) boundary_conditions.insert(std::make_pair(side_t::north, std::make_tuple(boundary_t::adiabatic, 0.0)));
			if (boundary_conditions.find(side_t::top) == boundary_conditions.end()) boundary_conditions.insert(std::make_pair(side_t::top, std::make_tuple(boundary_t::adiabatic, 0.0)));
			if (boundary_conditions.find(side_t::bottom) == boundary_conditions.end()) boundary_conditions.insert(std::make_pair(side_t::bottom, std::make_tuple(boundary_t::adiabatic, 0.0)));
			
			temperature_map.allocate("temperature map on host", nodes);
			temperature_map.set_zeros(); // set_values(static_cast<double>(ambient_temperature)); -> atterek delta homersekletre...
			dissipation_map.allocate("dissipation map on host", nodes);
			dissipation_map.set_zeros();

			switch(precision.numerical_method)
			{
				// using float_type = (precision.float_type == float_t::snl ? (float) : (precision.float_type == float_t::dbl ? double : void));

				// const utype_float_t alma = static_cast<utype_float_t>(precision.float_type);

				case euler_t::forward:
				{
					switch(precision.float_type)
					{	
						case float_t::snl:
							engine = new forward_euler_engine_t<static_cast<utype_float_t>(float_t::snl)>(this,*layout_ptr, boundary_conditions, dissipation_map.map_fl, temperature_map.map_fl,
														// layer_boundary_current_buffer.map_fl, layer_capacity_current_buffer.map_fl, layer_dissipation_buffer.map_fl,
														static_cast<double>(ambient_temperature), static_cast<double>(timestep.current_timestep()), enable_adaptive_timestep,
														static_cast<double>(temperature_threshold), precision, use_accelerator);
							break;
						
						case float_t::dbl:
							engine = new forward_euler_engine_t<static_cast<utype_float_t>(float_t::dbl)>(this,*layout_ptr, boundary_conditions, dissipation_map.map_db, temperature_map.map_db,
														// layer_boundary_current_buffer.map_db, layer_capacity_current_buffer.map_db, layer_dissipation_buffer.map_db,
														static_cast<double>(ambient_temperature), static_cast<double>(timestep.current_timestep()), enable_adaptive_timestep,
														static_cast<double>(temperature_threshold), precision, use_accelerator);
							break;
					}	
					break;
				}
						
				case euler_t::backward:
				{
					switch(precision.float_type)
					{
						case float_t::snl:
							engine = new backward_euler_engine_t<static_cast<utype_float_t>(float_t::snl)>(this,*layout_ptr, boundary_conditions, dissipation_map.map_fl, temperature_map.map_fl,
														// layer_boundary_current_buffer.map_fl, layer_capacity_current_buffer.map_fl, layer_dissipation_buffer.map_fl,
														static_cast<double>(ambient_temperature), static_cast<double>(timestep.current_timestep()), enable_adaptive_timestep,
														static_cast<double>(temperature_threshold), precision, use_accelerator);
							break;
						
						case float_t::dbl:
							engine = new backward_euler_engine_t<static_cast<utype_float_t>(float_t::dbl)>(this,*layout_ptr, boundary_conditions, dissipation_map.map_db, temperature_map.map_db,
														// layer_boundary_current_buffer.map_db, layer_capacity_current_buffer.map_db, layer_dissipation_buffer.map_db,
														static_cast<double>(ambient_temperature), static_cast<double>(timestep.current_timestep()), enable_adaptive_timestep,
														static_cast<double>(temperature_threshold), precision, use_accelerator);
							break;
					}
					break;
				}

				case euler_t::improved:
				{
					switch(precision.float_type)
					{
						case float_t::snl:
							engine = new improved_euler_engine_t<static_cast<utype_float_t>(float_t::snl)>(this,*layout_ptr, boundary_conditions, dissipation_map.map_fl, temperature_map.map_fl,
														// layer_boundary_current_buffer.map_fl, layer_capacity_current_buffer.map_fl, layer_dissipation_buffer.map_fl,
														static_cast<double>(ambient_temperature), static_cast<double>(timestep.current_timestep()), enable_adaptive_timestep,
														static_cast<double>(temperature_threshold), precision, use_accelerator);
							break;
						
						case float_t::dbl:
							engine = new improved_euler_engine_t<static_cast<utype_float_t>(float_t::dbl)>(this,*layout_ptr, boundary_conditions, dissipation_map.map_db, temperature_map.map_db,
														// layer_boundary_current_buffer.map_db, layer_capacity_current_buffer.map_db, layer_dissipation_buffer.map_db,
														static_cast<double>(ambient_temperature), static_cast<double>(timestep.current_timestep()), enable_adaptive_timestep,
														static_cast<double>(temperature_threshold), precision, use_accelerator);
							break;
					}
					break;
				}
					
				default:
					error("create_structure(): unkown integration type");
			}
		}
		
		/**
		 * ennek a fuggvenynek kell kiszamitania az idolepesben az uj homerseklet adatokat
		 * ebbol az osztalybol leszarmazo termikus motor adapter osztalynak ezt a fuggvenyt
		 * meg kell valositania
		**/ 
		void adapter_t::calculate_temperatures()
		{

			if(timestep.is_linear())
			{
				engine->calculate_temperatures();
			}
			else
			{
				if(enable_adaptive_timestep) error("calculate_temperatures(): adaptive timestep is only supported in linear timestep mode.");

				engine->calculate_temperatures(static_cast<double>(timestep.current_timestep()));
			}
		}
		
		/*
		 * pontossagot allithatjuk vele
		 * create_layout elott kell beallitani
		*/
		void adapter_t::set_precision(float_t float_type, euler_t numerical_method, unit::temperature_t temperature_tolerance)
		{
			if(!initialized())
			{
				this->precision.set_float_type(float_type);
				this->precision.set_numerical_method(numerical_method);
				this->precision.set_temperature_tolerance(temperature_tolerance);
			}
			else warning("set_precision(): changing precision is not possible after initialization");
		}

		void adapter_t::set_precision(float_t float_type, euler_t numerical_method, unit::power_t absolute_tolerance)
		{
			if(!initialized())
			{
				this->precision.set_float_type(float_type);
				this->precision.set_numerical_method(numerical_method);
				this->precision.set_absolute_tolerance(absolute_tolerance);
			}
			else warning("set_precision(): changing precision is not possible after initialization");
		}


		/*
		 * hasznaljuk-e az acceleratort (ha elerheto)
		*/
		void adapter_t::use_available_accalerator(bool use_accelerator)
		{
			if(!initialized()) this->use_accelerator = use_accelerator;
			else warning("use_available_accalerator(): changing backend is not possible after initialization");

			
		}

		/*
		 * hasznaljuk-e az acceleratort (ha elerheto)
		*/
		void adapter_t::use_adaptive_timestep(bool enable_adaptive_timestep, unit::temperature_t temperature_threshold)
		{
			if(!initialized())
			{
				this->enable_adaptive_timestep = enable_adaptive_timestep;
				this->temperature_threshold = temperature_threshold;
			}
			else warning("use_available_accalerator(): changing adaptive timestep is not possible after initialization");


			
		}

		/**
		 * hozzaad egy uj materialt a termikus motorhoz
		 * a leszarmazott termikus motornak ezt implementalnia kell
		 * ellenorzes: material nev egyedi
		**/ 
		void adapter_t::add_material(std::string const &name, double thermal_conductivity, double thermal_capacity)
		{
			if(materials_container.find(name) != materials_container.end())
			{
				warning("add_material(): material '" + name + "' already in the materials_container");
			}
			else
			{
				materials_container.insert(std::make_pair(name, material_t(name, thermal_conductivity,thermal_capacity)));
			}
		}
		
		void adapter_t::add_material(const material_t& material)
		{
			if(materials_container.find(material.id) != materials_container.end())
			{
				warning("add_material(): material '" + material.id + "' is already in the materials_container");
			}
			else
			{
				materials_container.insert(std::make_pair(material.id, material));
			}
		}

		side_t adapter_t::get_side(const std::string& side)
		{
			if(string_2_side_t.find(side) == string_2_side_t.end()) error("get_side(): unkown side '" + side + "'");
			return string_2_side_t.at(side);
		}

		const std::string& adapter_t::get_side(side_t side)
		{
			return side_t_2_string.at(side);
		}

		boundary_t adapter_t::get_boundary(const std::string& boundary)
		{
			if(string_2_boundary_t.find(boundary) == string_2_boundary_t.end()) error("get_boundary(): unkown boundary '" + boundary + "'");
			return string_2_boundary_t.at(boundary);
		}
		
		void adapter_t::set_ambient_temperature(const unit::temperature_t& temperature)
		{
			ambient_temperature = temperature;
		}

		void adapter_t::set_pixel_size(unit::length_t size)
		{
			pixel_size = size;
		}
		
		void adapter_t::add_layer(const std::string& layer_name, const std::string& material_name, const unit::length_t& thickness)
		{
			layout::sloth::adapter_t* layout_ptr = dynamic_cast<::layout::sloth::adapter_t*>(layout_structure);
			if(nullptr == layout_ptr) error("add_layer(): invalid layout format or layout has not been created.");
			if(materials_container.find(material_name) == materials_container.end()) error("add_layer(): unkown material type '" + material_name + "'");
			layout_ptr->add_layer(layer_name, materials_container.at(material_name), thickness);
		}
		
		void adapter_t::add_layer(const std::string& layer_name, const material_t& material, const unit::length_t& thickness)
		{
			layout::sloth::adapter_t* layout_ptr = dynamic_cast<::layout::sloth::adapter_t*>(layout_structure);
			if(nullptr == layout_ptr) error("add_layer(): invalid layout format or layout has not been created.");
			if(materials_container.find(material.id) == materials_container.end()) add_material(material);
			layout_ptr->add_layer(layer_name, material, thickness);
		}
		
		void adapter_t::add_boundary_condition(side_t side, boundary_t type, double value)
		{
			if(boundary_conditions.find(side) != boundary_conditions.end())
			{
				warning("add_boundary_condition(): boundary condition to '" + get_side(side) + "' is already set");
			}
			else
			{
				boundary_conditions.insert(std::make_pair(side,std::make_tuple(type, value)));
			}
		}

		void adapter_t::save_conductivity_matrix(const std::string& path, const std::string& file)
		{	
			if(initialized())
			{
				engine->save_conductivity_matrix(path+file);

				// switch(precision.float_type)
				// {
				// 	case float_t::snl:
				// 		engine_fl->save_conductivity_matrix(path+file);
				// 		break;
				// 	case float_t::dbl:
				// 		engine_db->save_conductivity_matrix(path+file);
				// 		break;
				// 	default:
				// 		error("save_conductivity_matrix(): unkown float precision");
				// }
			}
			else
			{
				warning("save_conductivity_matrix(): structure was not initialized");
			}
		}

		timestep_trace_t* adapter_t::add_timestep_trace(const std::string& path, const std::string& postfix)
		{
			if(nullptr == engine) error("add_timestep_trace(): engine was not been initialized");

			return engine->add_timestep_trace(path, postfix);
		}

		unit::time_t adapter_t::get_stability_limit()
		{
			return static_cast<unit::time_t>(engine->get_stability_limit());
		}

		size_t adapter_t::initialize_paralution()
		{
			paralution::init_paralution();
			return ++instance_counter;
		}

		size_t adapter_t::stop_paralution()
		{
			if(instance_counter == 1)
			{
				paralution::stop_paralution();
			}

			return --instance_counter;
		}
		
		adapter_t::adapter_t(logitherm::manager_t* manager, util::log_t* log, const std::string& id, const util::timestep_t& timestep)
		:
			thermal::adapter_t(manager, log, id, util::thermal_engine_t::sloth, timestep),
			paralution_initialized(initialize_paralution()),
			use_accelerator(false),
			enable_adaptive_timestep(false),
			temperature_threshold(0.1_K),
			// precision(precision_t::dbl),
			temperature_map(this, precision),
			dissipation_map(this, precision),
			layer_boundary_current_buffer(this, precision),
			layer_capacity_current_buffer(this, precision),
			layer_dissipation_buffer(this, precision),
			engine(nullptr),
			// engine_db(nullptr),
			pixel_size(0.5_um),
			ambient_temperature(300_K)
		{
			debug("adapter_t()");

			// paralution::init_paralution();
		}

		adapter_t::adapter_t(const adapter_t& other, const std::string& id, const util::timestep_t& timestep)
		:
			thermal::adapter_t(other.manager, other.log, id, util::thermal_engine_t::sloth, timestep),
			materials_container(other.materials_container),
			floorplans(other.floorplans),
			boundary_conditions(other.boundary_conditions),
			paralution_initialized(other.paralution_initialized),
			use_accelerator(other.use_accelerator),
			enable_adaptive_timestep(false),
			temperature_threshold(0.1_K),
			precision(other.precision),
			temperature_map(this, this->precision),
			dissipation_map(this, this->precision),
			layer_boundary_current_buffer(this, this->precision),
			layer_capacity_current_buffer(this, this->precision),
			layer_dissipation_buffer(this, this->precision),
			engine(nullptr),
			// engine_db(nullptr),
			pixel_size(other.pixel_size),
			ambient_temperature(other.ambient_temperature)
		{
			// create_layout_structure
			layout::sloth::adapter_t* layout_ptr = dynamic_cast<layout::sloth::adapter_t*>(other.layout_structure);
			if(nullptr == layout_ptr) error("initialization from other thermal engine failed: layout could not be initialized");
 		

 			// create_layout()
 		// 	layout_structure = new layout::sloth::adapter_t(this, log, size, pitch);
			// auto* sloth_layout = dynamic_cast<layout::sloth::adapter_t*>(layout_structure);
			
			// layout_size = xy_length_t(sloth_layout->get_length()[0],sloth_layout->get_length()[1]);
			// cell_size = xy_length_t(layout_size[0]/pitch[0],layout_size[1]/pitch[1]);
			
			layout_structure = new layout::sloth::adapter_t(*layout_ptr, this, this->log);

			if(other.initialized() == false) error("adapter_t(other): other adapter was not initialized");

			initialize();
		}

		adapter_t::~adapter_t()
		{
			debug("~adapter_t()");

			// paralution::stop_paralution(); //nem lehet, hogy ezt kene legutoljara tenni??
			delete engine;
			// delete engine_db;

			// delete temperature_map;
			// delete dissipation_map;

			// delete layer_boundary_current_buffer;
			// delete layer_capacity_current_buffer;
			// delete layer_dissipation_buffer;
			stop_paralution();
		}
		
		std::ostream& adapter_t::print_temperature_map_in_svg(std::ostream& os, std::unordered_set<layout::component_t*, std::hash<layout::component_t*>, layout::components_equal>& components)
		{
			if(temperature_map.is_allocated())
			{
						
				struct color {
					unsigned red;
					unsigned green;
					unsigned blue;

					color(unsigned red = 0, unsigned green = 0, unsigned blue = 0) : red(red), green(green), blue(blue) {}

					std::string to_hex() const {
						std::stringstream stream;

						stream << std::setfill('0');
						stream << std::hex << std::setw(2) << red << std::setw(2) << green << std::setw(2) << blue;
						return stream.str();
					}
				};

				//ez egy nem tul elegans megoldas, be kellene vezetni valamilyen unit-okat (um, mm stb, ugyanez wattra es sec-re is)
				layout::sloth::adapter_t* layout_ptr = dynamic_cast<layout::sloth::adapter_t*>(layout_structure);
				
				xy_length_t cell_size = layout_ptr->get_cell_size();
				size_t picture_grid_x = static_cast<size_t>(cell_size[0]/pixel_size); //14; // horizontal size of one grid in pixels static_cast<size_t>(std::get<0>(get_cell_size())*2e6);
				size_t picture_grid_y = static_cast<size_t>(cell_size[1]/pixel_size); //52; // vertical size of one grid in pixels static_cast<size_t>(std::get<0>(get_cell_size())*2e6);

				xy_length_t actual_pixel_size(cell_size[0]/picture_grid_x, cell_size[1]/picture_grid_y);
				

				for(auto const& active_layer: layout_ptr->get_active_layers())
				{
					auto min_max = get_temperature_range(active_layer.first); //megcsinalni
					double temperature_range = std::get<1>(min_max) - std::get<0>(min_max);

					unsigned const max_colour = 255;

					size_t font_size = std::min(picture_grid_x, picture_grid_y) * 0.6;
					size_t label_font_size = 2.5 * font_size;
					
					size_t longest_name = 0;
					size_t component_number = components.size();

					for (auto const &a_component : components) 
					{
						size_t size = a_component->id.size();
						if (size > longest_name) longest_name = size;
					}

					auto number_of_digits = [](size_t value){size_t digits = 1; while ((value /= 10) != 0) ++digits; return digits;};
					size_t largest_id_size = number_of_digits(components.size());

					size_t picture_width = ((x_pitch + 1.0) * picture_grid_x + (longest_name + largest_id_size + 3.0) * label_font_size * 0.8);
					size_t picture_height = std::max((z_pitch * y_pitch * picture_grid_y + (z_pitch - 1) * picture_grid_y), (2 * component_number) * label_font_size);

					os << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
					os << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">" << std::endl;
					os << "<svg width=\"" << picture_width << "\" height=\"" << picture_height << "\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">" << std::endl;

					//Creating the thermal map layer
					os << "\t<g id=\"thermal_map\">" << std::endl;
					
					// TODO csak az aktiv layert rajzoljuk ki, erre kellene valamilyen megoldas...
					for (size_t i = x_pitch*y_pitch*active_layer.second; i < x_pitch*y_pitch*(active_layer.second+1); ++i)
					{
						double temperature = temperature_map.get_value(i) + static_cast<double>(ambient_temperature);
						double value = (temperature - std::get<0>(min_max)) / temperature_range;

						size_t position_x = i % x_pitch;
						size_t position_y = i / x_pitch + (i / x_pitch / y_pitch); // The second part creates a picture_grid_y high gap between layers.

						unsigned red = ((value < 0.3) ? 0.0 : (1.0 / 0.7) * (value - 0.3)) * max_colour;
						unsigned green = (0.5 * value * value * value * value) * max_colour;
						unsigned blue = (0.5 * (1 - value * value * value)) * max_colour;
						color the_color(red, green, blue);

						os << "\t\t<rect x=\"" << (position_x * picture_grid_x) << "\" y=\"" << (position_y * picture_grid_y) << "\" width=\"" << picture_grid_x << "\" height=\"" << picture_grid_y << "\" style=\"fill:#" << the_color.to_hex() << ";stroke-width:0;\"/>" << std::endl;
					}

					os << "\t</g>" << std::endl;

					//Creating the label_shapes_layer
					os << "\t<g id=\"label_shapes_layer\">" << std::endl;

					//	Adding the bounding boxes of the components
					for (auto const &a_component : components)
					{
						auto component = dynamic_cast<layout::sloth::component_t*>(a_component);
						if (!component)
						{
							error("print_temperature_map_in_svg(): layout component '" + a_component->id + "' has not valid type.");
						}

						for(auto &shape: component->get_shapes())
						{
							size_t const size_x = static_cast<size_t>(shape->get_side_lengths()[0]/actual_pixel_size[0]);//std::get<0>(layout->get_size());
							size_t const size_y = static_cast<size_t>(shape->get_side_lengths()[1]/actual_pixel_size[1]);//std::get<1>(layout->get_size());

							size_t const layer = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(component->layer_id);

							size_t const pixel_x = static_cast<size_t>(shape->get_absolute_position()[0]/actual_pixel_size[0]);//topleft_x * picture_grid_x; 
							size_t const pixel_y = static_cast<size_t>(shape->get_absolute_position()[1]/actual_pixel_size[1])+ active_layer.second * (y_pitch + 1) * picture_grid_y;//(topleft_y + layer * (y_pitch + 1)) * picture_grid_y;
							size_t const width = size_x; //* picture_grid_x;
							size_t const height = size_y;// * picture_grid_y;

							os << "\t\t<rect x=\"" << pixel_x << "\" y=\"" << pixel_y << "\" width=\"" << width << "\" height=\"" << height << "\" style=\"fill:#434343;fill-opacity:0.25;stroke:#cccccc;stroke-width:1;stroke-opacity:0.6\"/>" << std::endl;
						}
					}

					os << "\t</g>" << std::endl;

					//Creating the label_text_layer
					os << "\t<g id=\"label_text_layer\">" << std::endl;

					//	Adding the number of components on their bounding boxes
					size_t component_id = 0;

					for (auto const &a_component : components)
					{
						for(auto &component: a_component->get_shapes())
						{
							auto shape = dynamic_cast<layout::sloth::component_t*>(component);
							if (!shape)
							{
								error("print_temperature_map_in_svg(): layout component '" + a_component->id + "' has not valid type.");
							}

							size_t const topleft_x = static_cast<size_t>(shape->get_absolute_position()[0]/actual_pixel_size[0]);
							size_t const topleft_y = static_cast<size_t>(shape->get_absolute_position()[1]/actual_pixel_size[1]);

							size_t const size_x = static_cast<size_t>(shape->get_side_lengths()[0]/actual_pixel_size[0]);//std::get<0>(layout->get_size());
							size_t const size_y = static_cast<size_t>(shape->get_side_lengths()[1]/actual_pixel_size[1]);

							size_t const layer = dynamic_cast<::layout::sloth::adapter_t*>(layout_structure)->get_layer_index(shape->layer_id);

							size_t const font_size_coefficient = std::min(shape->pitch_size[0], shape->pitch_size[1]);
							size_t const local_font_size = font_size * font_size_coefficient;

							size_t const pixel_x = topleft_x + 0.5 * size_x - (number_of_digits(component_id) / 4.0 * local_font_size); 
							size_t const pixel_y = topleft_y + active_layer.second * (y_pitch + 1) * picture_grid_y + 0.5 * size_y  + (local_font_size / 4.0);


							os << "\t\t<text x=\"" << pixel_x << "\" y=\"" << pixel_y << "\" style=\"font-size:" << local_font_size << ";fill:#ffffff;fill-opacity:0.7;\">" << component_id << "</text>" << std::endl;
						}
						++component_id;
					}

					//	Adding the component names as labels on the right side
					component_id = 0;
					for (auto const &a_component : components) {
						os << "\t\t<text x=\"" << ((x_pitch + 1) * picture_grid_x) << "\" y=\"" << ((2 * component_id + 1) * label_font_size) << "\" style=\"font-size:" << label_font_size << ";fill:#323232;\">" << 
							component_id << ": " << a_component->id << "</text>" << std::endl;
						++component_id;
					}

					os << "\t</g>" << std::endl;

					os << "</svg>" << std::endl;
				}
			}
			return os;
		}
		
		std::ostream& adapter_t::print_temperature_map(std::ostream& os)
		{
			if(temperature_map.is_allocated())
			{
				for(size_t k = 0; k < z_pitch; ++k)
				{
					os << "---------- layer [" << k << "] ----------" << std::endl;
					for(size_t j = 0; j < y_pitch; ++j)
					{
						for(size_t i = 0; i < x_pitch; ++i)
						{
							os << temperature_map.get_value(i + j * x_pitch + k * x_pitch * y_pitch) + static_cast<double>(ambient_temperature) << " ";
						}
						os << std::endl;
					}		
				}
			}
			return os;
		}
		
		
		/**
		 * ez a fuggveny kinyeri a termikus motor altal kiszamitott homerseklet eloszlasbol az egyes modulok homersekletet
		**/
		void adapter_t::refresh_component_temperature(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipator_components)
		{
			for(auto &it: dissipator_components)
			{
				/** valtozok, amikkel atlathatobb a homersekleti mintavetel pozicioja **/
				layout::sloth::component_t* layout_component = dynamic_cast<layout::sloth::component_t*>(it.second);
				if (!layout_component)
				{
					error("refresh_component_temperature(): layout component '" + it.second->id + "' has not valid type.");
				}
				
				size_t x = layout_component->pitch_center[0];//static_cast<size_t>((component_x_position + component_center_x)/std::get<0>(cell_size));
				size_t y = layout_component->pitch_center[1];//static_cast<size_t>((component_y_position + component_center_y)/std::get<1>(cell_size));
				size_t z = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(layout_component->layer_id);
				
				/** a modul homersekletenek beallitasa **/
				it.first->temperature = static_cast<unit::temperature_t>(temperature_map.get_value(x + y * x_pitch + z * x_pitch * y_pitch)) + ambient_temperature;
			}
		}

		/**
		 * ezek a fuggvenyek kinyerik a komponens homersekletet (min, max, avg, center)
		**/
		unit::temperature_t adapter_t::get_min_temperature(layout::component_t* component)
		{
			layout::sloth::component_t* ptr = dynamic_cast<layout::sloth::component_t*>(component);
			if(ptr == nullptr) error("get_min_temperature(): layout component '" + ptr->id + "' has invalid type");

			size_t x_start = ptr->pitch_position[0];
			size_t x_end = x_start + ptr->pitch_size[0];

			size_t y_start = ptr->pitch_position[1];
			size_t y_end = y_start + ptr->pitch_size[1];

			size_t z_index = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(ptr->layer_id);
			
			double min = temperature_map.get_value(x_start + y_start * x_pitch + z_index * x_pitch * y_pitch);
			
			for(size_t x_index = x_start; x_index < x_end; ++x_index)
			{
				for(size_t y_index = y_start; y_index < y_end; ++y_index)
				{
					if(min > temperature_map.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch)) min = temperature_map.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch);
				}
			}
			return static_cast<unit::temperature_t>(min)+ambient_temperature;;
		}
		
		unit::temperature_t adapter_t::get_max_temperature(layout::component_t* component)
		{
			layout::sloth::component_t* ptr = dynamic_cast<layout::sloth::component_t*>(component);
			if(ptr == nullptr) error("get_max_temperature(): layout component '" + ptr->id + "' has invalid type");

			size_t x_start = ptr->pitch_position[0];
			size_t x_end = x_start + ptr->pitch_size[0];

			size_t y_start = ptr->pitch_position[1];
			size_t y_end = y_start + ptr->pitch_size[1];

			size_t z_index = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(ptr->layer_id);
			
			double max = temperature_map.get_value(x_start + y_start * x_pitch + z_index * x_pitch * y_pitch);
			
			for(size_t x_index = x_start; x_index < x_end; ++x_index)
			{
				for(size_t y_index = y_start; y_index < y_end; ++y_index)
				{
					if(max < temperature_map.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch)) max = temperature_map.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch);
				}
			}
			return static_cast<unit::temperature_t>(max)+ambient_temperature;
		}
		
		unit::temperature_t adapter_t::get_avg_temperature(::layout::component_t* component)
		{
			layout::sloth::component_t* ptr = dynamic_cast<::layout::sloth::component_t*>(component);
			if(ptr == nullptr) error("get_avg_temperature(): layout component '" + ptr->id + "' has invalid type");

			size_t x_start = ptr->pitch_position[0];
			size_t x_end = x_start + ptr->pitch_size[0];

			size_t y_start = ptr->pitch_position[1];
			size_t y_end = y_start + ptr->pitch_size[1];

			size_t z_index = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(ptr->layer_id);
			
			double avg = 0.0;
			
			size_t cell_count = 0;
			for(size_t x_index = x_start; x_index < x_end; ++x_index)
			{
				for(size_t y_index = y_start; y_index < y_end; ++y_index)
				{
					++cell_count;
					avg += temperature_map.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch);
				}
			}
			avg = avg / static_cast<double>(cell_count);
			return static_cast<unit::temperature_t>(avg)+ambient_temperature;;
		}

		std::ostream& adapter_t::get_layer_temperature(std::ostream& os, const std::string& layer_id)
		{
			size_t z_index = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(layer_id);
			
			
			for(size_t x_index = 0; x_index < x_pitch; ++x_index)
			{
				for(size_t y_index = 0; y_index <  y_pitch; ++y_index)
				{	
					float temperature = temperature_map.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch) + static_cast<double>(ambient_temperature);
					os.write(reinterpret_cast<const char*>(&temperature), sizeof(float));
				}
			}
			return os;
		}
		
		/**
		 * ezek a fuggvenyek kinyerik a komponens homersekletet (min, max, avg, center)
		**/
		
		std::ostream& adapter_t::print_component_temperature(std::ostream& os, layout::sloth::component_t* component)
		{
			size_t x_start = component->pitch_position[0];
			size_t x_end = x_start + component->pitch_size[0];

			size_t y_start = component->pitch_position[1];
			size_t y_end = y_start + component->pitch_size[1];

			size_t z_index = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(component->layer_id);
			
			
			for(size_t x_index = x_start; x_index < x_end; ++x_index)
			{
				for(size_t y_index = y_start; y_index <  y_end; ++y_index)
				{	
					float temperature = temperature_map.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch) + static_cast<double>(ambient_temperature);
					os.write(reinterpret_cast<const char*>(&temperature), sizeof(float));
				}
			}
			return os;
		}
	
		std::tuple<double, double> adapter_t::get_temperature_range(const std::string& layer_id) const
		{
			if(!temperature_map.is_allocated())
			{
				error("get_temperature_range(): temperature map has not been initialized.");
			}

			size_t layer_index = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(layer_id);
			std::tuple<double, double> minmax = temperature_map.min_max(x_pitch*y_pitch*layer_index, x_pitch*y_pitch*(layer_index+1));
			std::get<0>(minmax) += static_cast<double>(ambient_temperature);
			std::get<1>(minmax) += static_cast<double>(ambient_temperature);
			return minmax;
		}
		
		std::ostream& adapter_t::print_dissipation_map(std::ostream& os)
		{
			if(dissipation_map.is_allocated())
			{
				for(size_t k = 0; k < z_pitch; ++k)
				{
					os << "---------- layer [" << k << "] ----------" << std::endl;
					for(size_t j = 0; j < y_pitch; ++j)
					{
						for(size_t i = 0; i < x_pitch; ++i)
						{
							os << dissipation_map.get_value(i + j * x_pitch + k * x_pitch * y_pitch) << " ";
						}
						os << std::endl;
					}		
				}
			}
			return os;
		}
		
		
		/**
		 * ez a fuggveny egy szimulacios lepesnel a logitherm_proxy adataibol kiszamitja, hogy milyen pozicioban mennyit disszipalt a chip
		 * ezt a fuggvenyt a thermal_base refresh_temperatures fv-e hivja meg
		**/ 
		void adapter_t::refresh_dissipation_map(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipator_components)
		{
			/** kinullazzuk a distribution tombot **/
			// dissipation_map.set_zeros();
			
			/** vegigiteralunk a dissipatorok tombjen **/
			for(auto &it: dissipator_components)
			{
				/** fogyasztas meghatarozasa **/
				double dissipation = static_cast<double>(it.first->get_dissipation());
								
				/** terulet meghatarozasa **/
				double total_area = static_cast<double>(it.second->calculate_area());

				double power_density = dissipation/total_area;

				for(auto &shape: it.second->get_shapes())
				{
					layout::sloth::component_t* layout_component = dynamic_cast<layout::sloth::component_t*>(shape);
					if (!layout_component)
					{
						error("refresh_dissipation_map(): layout component '" + it.second->id + "' has not valid type.");
					}
				
					// unit::length_t x_pos = layout_component->get_absolute_position()[0];
					// unit::length_t y_pos = layout_component->get_absolute_position()[1];	

					/** valtozok, amikkel egyszerubbe valnak az egymasba agyazott ciklusok **/
					size_t x_start = layout_component->pitch_position[0];
					size_t x_end = x_start + layout_component->pitch_size[0];

					size_t y_start = layout_component->pitch_position[1];
					size_t y_end = y_start + layout_component->pitch_size[1];

					layout::sloth::adapter_t* layout_ptr = dynamic_cast<::layout::sloth::adapter_t*>(layout_structure);
					if(layout_ptr == nullptr ) error("refresh_dissipation_map(): layout adapter  has not valid type.");

					size_t z_index = layout_ptr->get_layer_index(layout_component->layer_id);
					
					for(size_t y_index = y_start; y_index < y_end; ++y_index)
					{
						for(size_t x_index = x_start; x_index < x_end; ++x_index)
						{
//							::unit::length_t x_size(cell_size[0]);
//							::unit::length_t y_size(cell_size[1]);
//
//							::unit::length_t cell_x_pos(cell_size[0]*static_cast<double>(x_index));
//							::unit::length_t cell_y_pos(cell_size[1]*static_cast<double>(y_index));
//
//							/** ki kell szamitani, hogy a component teruletenek mekkora resze esik az adott cellaba **/
//							if(cell_x_pos < x_pos) x_size = cell_size[0] - (x_pos - cell_x_pos); //bal szel
//							else if (x_pos + layout_component->get_side_lengths()[0] < cell_x_pos + cell_size[0]) x_size = x_pos + layout_component->get_side_lengths()[0] - cell_x_pos; //jobb szel
//							else x_size = cell_size[0];
//							
//							if(cell_y_pos < y_pos) y_size = cell_size[1] - (y_pos - cell_y_pos); //felso szel
//							else if (y_pos + layout_component->get_side_lengths()[1] < cell_y_pos + cell_size[1]) y_size = y_pos + layout_component->get_side_lengths()[1] - cell_y_pos; //also szel
//							else y_size = cell_size[1];

							/** disszipacio novelese  dissipation/area ertekkel (egyenletes disszipaciosuruseg a teruleten) **/
							size_t x = x_index-x_start;
							size_t y = y_index-y_start;

							unit::area_t area = layout_component->area_array[x + y * layout_component->pitch_size[0]];
							double new_dissipation = dissipation_map.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch) + static_cast<double>(area*power_density);
							dissipation_map.set_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch, new_dissipation);
						}
					}				
					
				}
			}			
		}

		std::ostream& adapter_t::get_component_boundary_current(std::ostream& os, layout::sloth::component_t* component)
		{
			size_t z_index = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(component->layer_id);
			size_t x_start = component->pitch_position[0];
			size_t x_end = component->pitch_position[0] + component->pitch_size[0];
			size_t y_start = component->pitch_position[1];
			size_t y_end = component->pitch_position[1] + component->pitch_size[1];

			for(size_t x_index = x_start; x_index < x_end; ++x_index)
			{
				for(size_t y_index = y_start; y_index <  y_end; ++y_index)
				{	
					float value = layer_boundary_current_buffer.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch);
					os.write(reinterpret_cast<const char*>(&value), sizeof(float));
				}
			}
			return os;
		}

		std::ostream& adapter_t::get_component_capacity_current(std::ostream& os, layout::sloth::component_t* component)
		{
			size_t z_index = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(component->layer_id);
			size_t x_start = component->pitch_position[0];
			size_t x_end = component->pitch_position[0] + component->pitch_size[0];
			size_t y_start = component->pitch_position[1];
			size_t y_end = component->pitch_position[1] + component->pitch_size[1];

			for(size_t x_index = x_start; x_index < x_end; ++x_index)
			{
				for(size_t y_index = y_start; y_index <  y_end; ++y_index)
				{	
					float value = layer_capacity_current_buffer.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch);
					os.write(reinterpret_cast<const char*>(&value), sizeof(float));
				}
			}
			return os;
		}

		std::ostream& adapter_t::get_component_dissipation(std::ostream& os, layout::sloth::component_t* component)
		{
			size_t z_index = dynamic_cast<layout::sloth::adapter_t*>(layout_structure)->get_layer_index(component->layer_id);
			size_t x_start = component->pitch_position[0];
			size_t x_end = component->pitch_position[0] + component->pitch_size[0];
			size_t y_start = component->pitch_position[1];
			size_t y_end = component->pitch_position[1] + component->pitch_size[1];

			for(size_t x_index = x_start; x_index < x_end; ++x_index)
			{
				for(size_t y_index = y_start; y_index <  y_end; ++y_index)
				{	
					float value = layer_dissipation_buffer.get_value(x_index + y_index * x_pitch + z_index * x_pitch * y_pitch);
					os.write(reinterpret_cast<const char*>(&value), sizeof(float));
				}
			}
			return os;
		}

		void adapter_t::reserve_layer_boundary_current_buffer()
		{
			if(initialized()) layer_boundary_current_buffer.allocate("layer boundary current buffer", nodes);
			else error("reserve_layer_boundary_current_buffer(): structure must be initialized first");
		}

		void adapter_t::reserve_layer_capacity_current_buffer()
		{
			if(initialized()) layer_capacity_current_buffer.allocate("layer capacity current buffer", nodes);
			else error("reserve_layer_capacity_current_buffer(): structure must be initialized first");
		}

		void adapter_t::reserve_layer_dissipation_buffer()
		{
			if(initialized()) layer_dissipation_buffer.allocate("layer dissipation buffer", nodes);
			else error("reserve_layer_dissipation_buffer(): structure must be initialized first");
		}

		// void adapter_t::refresh_detailed_dissipation_map(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipator_components)
		
		void adapter_t::read_files(const std::string& path, const std::string& initfile)
		{
			file_io::sloth::adapter_t parser(this, log, path, initfile);
			parser.read_files();
		}
		
	} //namespace sloth
	
} //namespace thermal
