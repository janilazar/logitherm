#ifndef _SLOTH_MATERIAL_
#define _SLOTH_MATERIAL_


#include <string>

namespace thermal
{
	namespace sloth
	{
		class material_t
		{
			public:
			
				/**
				 * nev, ami egyertelmuen azonositja az anyagot
				**/ 
				const std::string id;
				
				/**
				 * hovezetes
				**/ 
				const float thermal_conductivity;
				
				/**
				 * hokapacitas
				**/ 
				const float thermal_capacity;
				
			public:	
				material_t() = delete;
				material_t(const std::string& nm, float thermal_conductivity, float thermal_capacity)
				:
					id(nm),
					thermal_conductivity(thermal_conductivity),
					thermal_capacity(thermal_capacity)
				{}
		};
		
		/**
		 * osszehasonlito functor az std::set-hez
		 * az osszehasonlitas azonosito alapjan tortenik
		**/
		struct material_equal
		{//itt rh-st at kellene irni std::string-re??
			bool operator() (const material_t& lhs, const material_t& rhs) const
			{
				return lhs.id == rhs.id;
			}
			
		};
		
	} //namespace sunred
} //namespace thermal

namespace std
{
	template<>
	struct hash<::thermal::sloth::material_t>
	{	 
		size_t operator() (const ::thermal::sloth::material_t& material) const
		{
			return hash<string>()(material.id);
		}
		
	};
}
#endif //_MATERIAL_
