#ifndef _MAP_T_
#define _MAP_T_

#include <string>
#include <tuple>
#include <algorithm>
#include "dependency/paralution/paralution.hpp"
#include "thermal/sloth/adapter/precision_t.hpp"

namespace thermal
{
	namespace sloth
	{
		class adapter_t;

		class map_t
		{
			private:
				size_t nodes;
				bool allocated;
				thermal::sloth::adapter_t* thermal_adapter;
				const precision_t& precision;

			public:
				paralution::LocalVector<float> map_fl;
				paralution::LocalVector<double> map_db;

			public:

				map_t() = delete;

				map_t(thermal::sloth::adapter_t* thermal_adapter, const precision_t& precision);

				map_t(const std::string& name, size_t nodes, thermal::sloth::adapter_t* thermal_adapter, const precision_t& precision);

				~map_t();

				bool is_allocated() const;

				void allocate(const std::string& name, size_t nodes);

				void set_values(double value);

				void set_zeros();

				void set_value(size_t index, double value);

				double get_value(size_t index) const;

				std::tuple<double, double> min_max(size_t begin, size_t end) const;

		};
	}
}

#endif //_MAP_T_