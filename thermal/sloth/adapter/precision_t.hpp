#ifndef _PRECISION_T_
#define _PRECISION_T_

#include <unit/temperature_t.hpp>
#include <unit/power_t.hpp>

namespace thermal
{
	namespace sloth
	{
		enum class float_t : size_t {snl, dbl};
		typedef std::underlying_type<float_t>::type utype_float_t;

		enum class euler_t : size_t {backward, forward, improved};
		typedef std::underlying_type<float_t>::type utype_euler_t;

		enum class solver_t	: size_t{paralution, viennacl};
		typedef std::underlying_type<float_t>::type utype_solver_t;

		class precision_t
		{
			public:
				float_t float_type;
				euler_t numerical_method;
				unit::temperature_t temperature_tolerance;
				unit::power_t absolute_tolerance;
				bool absolute_tolerance_specified;

			public:
				// precision_t() = delete;

				precision_t(float_t _float_type_ = float_t::dbl, euler_t _numerical_method_ = euler_t::backward, unit::temperature_t _temperature_tolerance_ = 1_mK)
				:
					float_type(_float_type_),
					numerical_method(numerical_method),
					temperature_tolerance(_temperature_tolerance_),
					absolute_tolerance(0_W),
					absolute_tolerance_specified(false)
				{}

				void set_temperature_tolerance(unit::temperature_t _temperature_tolerance_)
				{
					absolute_tolerance_specified = false;
					temperature_tolerance = _temperature_tolerance_;
				}

				void set_absolute_tolerance(unit::power_t _absolute_tolerance_)
				{
					absolute_tolerance_specified = true;
					absolute_tolerance = _absolute_tolerance_;
				}


				void set_float_type(float_t _float_type_)
				{
					float_type = _float_type_;
				}

				void set_numerical_method(euler_t _numerical_method_)
				{
					numerical_method = _numerical_method_;
				}
		};
	}
}

#endif //_PRECISION_T_