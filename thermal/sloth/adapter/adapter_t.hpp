#ifndef _SLOTH_ADAPTER_
#define _SLOTH_ADAPTER_

#include <string>
#include <unordered_map>
#include <algorithm>
#include <iomanip>

#include "thermal/base/adapter_t.hpp"
#include "thermal/sloth/adapter/layout/adapter_t.hpp"
#include "thermal/sloth/adapter/map_t.hpp"
#include "thermal/sloth/adapter/material_t.hpp"
#include "thermal/sloth/adapter/precision_t.hpp"
#include "thermal/sloth/engine/engine_t.hpp"

#include "unit/all_units.hpp"

// #include "dependency/paralution/paralution.hpp"

/** nehany helper cucc az adapter-hez **/
namespace thermal
{
	namespace sloth
	{
		enum class boundary_t{temperature, current, adiabatic, conductive};
		enum class side_t{east, south, west, north, top, bottom};
	}
}

/** hashelheto legyen a fentebbi side_t **/
namespace std
{
	template<>
	struct hash<thermal::sloth::side_t>
	{
		size_t operator() (thermal::sloth::side_t side) const
		{
			return static_cast<size_t>(side);
		}
		
	};

	template<>
	struct hash<thermal::sloth::boundary_t>
	{
		size_t operator() (thermal::sloth::boundary_t boundary) const
		{
			return static_cast<std::underlying_type<thermal::sloth::boundary_t>::type>(boundary);
		}
		
	};
}

namespace util
{
	class log_t;
}

namespace thermal
{
	namespace sloth
	{
		class timestep_trace_t;
		
		class adapter_t : public thermal::adapter_t
		{
			public:
				using xy_length_t = layout::xy_length_t;
				using xy_pitch_t = layout::xy_pitch_t;
				
			private:
				


				/** az osszes anyag tulajdonsagait tarolo kontener **/
				std::unordered_map<std::string,material_t> materials_container;
				
				
				/** key==floorplan neve, tartalom== floorplan_file **/
				std::unordered_map<std::string, std::string> floorplans;
				
				std::unordered_map<side_t,std::tuple<boundary_t, double>, std::hash<side_t>> boundary_conditions;

				// ez szamlalja, hogy hany peldany van ebbol a termikus szarbol (csak egyszer hivjuk meg a stop_paralutiont)
				static size_t instance_counter;

				static std::unordered_map<side_t, std::string> side_t_2_string;				
				static std::unordered_map<std::string, side_t> string_2_side_t;
				static std::unordered_map<std::string, boundary_t> string_2_boundary_t;

				// xy_length_t layout_size;
				// xy_length_t cell_size;
				unit::length_t pixel_size;
				
				size_t x_pitch;
				size_t y_pitch;
				size_t z_pitch;
				size_t nodes;

				// double absolute_tolerance, relative_tolerance;
				
				unit::temperature_t ambient_temperature;
				
				bool paralution_initialized;
				bool use_accelerator;

				/** csak lin timestep eseten lehet **/
				bool enable_adaptive_timestep;
				unit::temperature_t temperature_threshold;

				precision_t precision;
				// euler_t integration;
				
				map_t temperature_map;
				map_t dissipation_map;

				map_t layer_boundary_current_buffer;
				map_t layer_capacity_current_buffer;
				map_t layer_dissipation_buffer;
				
				/**
				 * ez a termikus solver
				**/
				engine_t* engine;

			private:
				/**
				 * ez fuggveny a szimulalando (3D) IC felepitese alapjan a termikus motor igenyei
				 * szerint lefoglalja a szukseges eroforrasokat, beallitja a the_layout, the_dissipation_map
				 * es a the_temperature_map pointereket
				**/
				void create_structure() override;
				
				/**
				 * ez a fuggveny egy szimulacios lepesnel a logitherm_proxy adataibol kiszamitja, hogy milyen pozicioban mennyit disszipalt a chip
				**/ 
				void refresh_dissipation_map(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>&) override;

				/**
				 * ez a fuggveny kinyeri a termikus motor altal kiszamitott homerseklet eloszlasbol az egyes modulok homersekletet
				**/
				void refresh_component_temperature(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>&) override;

				size_t initialize_paralution();
				size_t stop_paralution();
				
			public:
				adapter_t() = delete;
				adapter_t(logitherm::manager_t* manager, util::log_t* log, const std::string& id, const util::timestep_t& timestep);
				adapter_t(const adapter_t& other, const std::string& id, const util::timestep_t& timestep);

				~adapter_t();
				
				/**
				 * ennek a fuggvenynek kell kiszamitania az idolepesben az uj homerseklet adatokat
				**/ 
				void calculate_temperatures() override;
				
				/*
				 * pontossagot allithatjuk vele
				 * create_layout elott kell beallitani
				*/
				void set_precision(float_t precision, euler_t integration = euler_t::backward, unit::temperature_t temperature_tolerance = 1_mK);

				void set_precision(float_t float_type, euler_t integration_type, unit::power_t absolute_tolerance);

				/*
				 * hasznaljuk-e az acceleratort (ha elerheto)
				*/
				void use_available_accalerator(bool accelerator);

				/*
				 * hasznaljuk-e adaptiv idolepteket
				*/
				void use_adaptive_timestep(bool enable_adaptive_timestep, unit::temperature_t temperature_threshold);

				void create_layout(const xy_length_t&, const xy_pitch_t&);
				
				/**
				 * hozzaad egy uj materialt a termikus motorhoz
				 * a leszarmazott termikus motornak ezt implementalnia kell
				 * ellenorzes: material nev egyedi
				**/ 
				void add_material(std::string const &name, double thermal_conductivity, double thermal_capacity);
				void add_material(const material_t& material);

				//visszater side_t tipussal boundary string alapjan
				//hibat dob, ha nem tudja a stringet megfeleltetni side_t-nek
				side_t get_side(const std::string& side);

				//visszater stringgel side_t alapjan, hibak kiirasahoz hasznalatos
				const std::string& get_side(side_t side);

				//visszater a string alapjan a boundary-vel
				//hibat dob, ha ismeretlen a boundary
				boundary_t get_boundary(const std::string& boundary);
				
				void set_ambient_temperature(const unit::temperature_t& temperature);

				void set_pixel_size(unit::length_t size);
				
				void add_layer(const std::string& layer_name, const std::string& material_name, const unit::length_t& thickness);
				void add_layer(const std::string& layer_name, const material_t& material, const unit::length_t& thickness);
				
				//void set_cell_size(const std::tuple<::unit::length_t,::unit::length_t>& cell_size);
				
				
				void add_boundary_condition(side_t, boundary_t, double value);
				

				// ezzel a fv-el elmenthetjuk a G mx-ot a termikus motorbol
				void save_conductivity_matrix(const std::string& path, const std::string& file);


				timestep_trace_t* add_timestep_trace(const std::string& path, const std::string& postfix = "");
				
				/** ==timestep, ha nem elorelepo euler, egyebket a szamolt ertek **/
				unit::time_t get_stability_limit();
				
				

				/**
				 * ez a fuggveny kinyeri a komponensek homersekletet (min, max, avg, center)
				**/
				unit::temperature_t get_min_temperature(layout::component_t* component) override;
				unit::temperature_t get_max_temperature(layout::component_t* component) override;
				unit::temperature_t get_avg_temperature(layout::component_t* component) override;

				std::ostream& get_layer_temperature(std::ostream& os, const std::string& layer_id) override;

				std::ostream& print_component_temperature(std::ostream& os, layout::sloth::component_t* component);		
				
				std::ostream& print_temperature_map_in_svg(std::ostream& os, std::unordered_set<layout::component_t*, std::hash<layout::component_t*>, layout::components_equal>& components) override;
				
				std::ostream& print_temperature_map(std::ostream& os) override;
				
				std::tuple<double, double> get_temperature_range(const std::string& layer_id) const;
				
				std::ostream& print_dissipation_map(std::ostream& os) override;

				void reserve_layer_boundary_current_buffer();
				void reserve_layer_capacity_current_buffer();
				void reserve_layer_dissipation_buffer();
				
				std::ostream& get_component_boundary_current(std::ostream& os, layout::sloth::component_t* component);
				std::ostream& get_component_capacity_current(std::ostream& os, layout::sloth::component_t* component);
				std::ostream& get_component_dissipation(std::ostream& os, layout::sloth::component_t* component);
				
				void read_files(const std::string& path, const std::string& initfile) override;
			
		};
	}
}
#endif // _SLOTH_ADAPTER_

