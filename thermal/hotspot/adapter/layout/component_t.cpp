#include "thermal/hotspot/adapter/layout/component_t.hpp"
#include "manager/manager_t.hpp"

#include "thermal/base/layout/layer_temperature_trace_t.hpp"

namespace layout
{
	namespace hotspot
	{
		//itt megadni poziciot meretet is. adapter-ben pedig megcsinalni, hoyg minden egyes IC elementet hozzaadjunk component-kent
		component_t::component_t(layout::adapter_t* adapter, const std::string& id, layout::xy_length_t position, layout::xy_length_t sides, size_t layer_index, size_t unit_index, size_t offset)
		:
			layout::component_t(adapter, id, nullptr, position, sides),
			layer_index(layer_index),
			unit_index(unit_index),
			offset(offset)
		{}

		layer_temperature_trace_t* component_t::add_layer_temperature_trace(const std::string& path, const std::string& postfix)
		{
			layer_temperature_trace_t* ptr = new layer_temperature_trace_t(path, std::to_string(layer_index), postfix, adapter->adapter);
			tracer.add_trace(ptr, std::ios::out | std::ios::binary);
			trace_component();
			return ptr;
		}
		
		/**
		 * kapott ostream-re kirakja a component es leszarmazottak nevet
		**/ 
		std::ostream& component_t::print_component(std::ostream& os, std::string indent)
		{
			//size, pposition???
			os << indent << id << std::endl;
			if(!children.empty())
			{
				indent += "  ";
				for(auto &it: children)
				{
					it.second->print_component(os, indent);
				}
			}
			return os;
		}
	} //namespace hotspot
}//namespace layout
