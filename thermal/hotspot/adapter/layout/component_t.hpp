#ifndef _LAYOUT_HOTSPOT_COMPONENT_
#define _LAYOUT_HOTSPOT_COMPONENT_

#include "thermal/base/layout/component_t.hpp"
// #include "thermal/3dice/engine/include/floorplan_element.h"
// #include "thermal/3dice/engine/include/ic_element.h"

namespace layout
{
	class layer_temperature_trace_t;
	
	namespace hotspot
	{
		class component_t	:	public layout::component_t
		{
			public:
				const size_t layer_index;
				const size_t unit_index;
				const size_t offset;
				//Stackelement* ???? pointer arra a stackelementre, ami tartalmazza a floorplanelementet
				
			public:
				component_t() = delete;
				component_t(layout::adapter_t* adapter, const std::string& id, layout::xy_length_t position, layout::xy_length_t sides, size_t layer_index, size_t unit_index, size_t offset);

				layer_temperature_trace_t* add_layer_temperature_trace(const std::string& path, const std::string& postfix = "") override;

				//ez vajon kell a dynamic_cast-hoz???
				std::ostream& print_component(std::ostream& os, std::string indent = "") override;
		};
	} //namespace hotspot
} //namespace layout
#endif //_LAYOUT_HOTSPOT_COMPONENT_
