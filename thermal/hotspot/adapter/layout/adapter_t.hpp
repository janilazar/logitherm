#ifndef _LAYOUT_HOTSPOT_ADAPTER_
#define _LAYOUT_HOTSPOT_ADAPTER_

#include "thermal/base/layout/adapter_t.hpp"

namespace layout
{
	namespace hotspot
	{
		class adapter_t:  public layout::adapter_t
		{
			private:
				xyz_pitch_t pitch;

			public:
				adapter_t() = delete;
				adapter_t(thermal::adapter_t* adapter, util::log_t* log, layout::xyz_pitch_t pitch);
				~adapter_t();

				layout::xyz_pitch_t get_pitch() override;
				layout::xyz_length_t get_length() override;
		};
	}
}
#endif //_LAYOUT_HOTSPOT_ADAPTER_
