#include "thermal/hotspot/adapter/adapter_t.hpp"
#include "manager/manager_t.hpp"
#include "util/log_t.hpp"
// #include "thermal/3dice/engine/include/stack_file_parser.h"

#include "thermal/hotspot/adapter/layout/adapter_t.hpp"
#include "thermal/hotspot/adapter/layout/component_t.hpp"

extern "C"
{
	#include "thermal/hotspot/engine/temperature.h"
	#include "thermal/hotspot/engine/temperature_grid.h"
	#include "thermal/hotspot/engine/flp.h"
	#include "thermal/hotspot/engine/util.h"
}

#include <memory>
#include <functional>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <cstring>

namespace thermal
{
	namespace hotspot
	{
		adapter_t::adapter_t(logitherm::manager_t* manager, util::log_t* log)
		:
			thermal::adapter_t(manager, log, "thermal::hotspot::adapter_t",util::thermal_engine_t::hotspot, manager->get_timestep()),
			layout_size(0_um, 0_um),
			cell_size(0_um, 0_um),
			pixel_size(0.5_um)
		{}
		
		adapter_t::~adapter_t()
		{
			delete_RC_model(hotspot_model);
			free_dvector(temperature_vector);
			free_dvector(power_vector);
			delete hotspot_config;
		}

		// void adapter_t::init_thermal_engine()
		// {
		// 	// stack_description_init(&stkd);
		// 	// thermal_data_init(&tdata);
		// 	// analysis_init(&analysis);
		// 	// output_init(&output);
		// }

		void adapter_t::read_files(const std::string& path, const std::string& configfile)
		{
			hotspot_config = new thermal_config_t;
			*hotspot_config = default_thermal_config();
			std::ifstream thermal_config_file(path+configfile);
			if(!thermal_config_file.is_open()) error("read_files(): unable to open thermal config file '" + path + configfile + "'");

			std::string parameter, value;
			std::unordered_map<std::string, std::string> table;
			while(thermal_config_file >> parameter >> value)
			{
				if(table.find(parameter) != table.end()) warning("read_files(): overwriting '" + parameter + "' parameter's value '" + table.at(parameter) + "'' to '" + value + "'");
				table.insert(std::make_pair(parameter, value));
			}

			if(!thermal_config_file.eof()) error("read_files(): " + path + configfile + "' has invalid format");
			
			str_pair* config_table = new str_pair[table.size()];
			size_t index = 0;
			for(auto &it: table)
			{
				strcpy(config_table[index].name, it.first.c_str());
				strcpy(config_table[index].value, it.second.c_str());
				++index;
			}
			
			thermal_config_add_from_strs(hotspot_config, config_table, index);
			if(std::string(hotspot_config->model_type) != std::string(GRID_MODEL_STR)) error("read_files(): only 'grid' model type is supported");
			if(std::string(hotspot_config->grid_layer_file) == std::string(NULLFILE)) error("read_files(): 'grid_layer_file' was not set");
			
			create_layout();

			temperature_vector = hotspot_vector(hotspot_model);
			power_vector = hotspot_vector(hotspot_model);

			/* set up initial instantaneous temperatures */
			if (std::string(hotspot_model->config->init_file) !=  std::string(NULLFILE))
			{
				error("read_files(): init_file is not supported");
				// if (!hotspot_model->config->dtm_used)	/* initial T = steady T for no DTM	*/
					// read_temp(hotspot_model, temperature_vector, hotspot_model->config->init_file, FALSE);
				// else	/* initial T = clipped steady T with DTM	*/
					// read_temp(hotspot_model, temperature_vector, hotspot_model->config->init_file, TRUE);
			}
			else
			{	/* no input file - use init_temp as the common temperature	*/
				set_temp(hotspot_model, temperature_vector, hotspot_model->config->init_temp);
			}

			//last_trans->cuboidon, inicializalni init_temp alapjan
			// for(size_t x = 0; x < x_pitch; ++x)
			// {
			// 	for(size_t y = 0; y < y_pitch; ++y)
			// 	{
 			//		for(size_t z = 0; z < z_pitch; ++z)
 			//		{
 			//			hotspot_model->grid->last_trans->cuboid[z][x][y] = hotspot_model->config->init_temp;
 			//		}
			// 	}
			// }

			first_call = true;
		}

		void adapter_t::create_layout()
		{
			hotspot_model = alloc_RC_model(hotspot_config, nullptr, 0); //detailed_3D off

			x_pitch = hotspot_config->grid_cols;
			y_pitch = hotspot_config->grid_rows;
			z_pitch = hotspot_model->grid->n_layers;
			nodes = x_pitch * y_pitch * z_pitch;

			layout_structure = new layout::hotspot::adapter_t(this, log, layout::xyz_pitch_t(x_pitch, y_pitch, z_pitch));
			
			populate_R_model(hotspot_model, nullptr);
			populate_C_model(hotspot_model, nullptr);

			

			if(hotspot_model->grid == nullptr) error("create_layout(): 'grid' is nullptr");

			layout_size = xy_length_t(hotspot_model->grid->width, hotspot_model->grid->height);
			// layout_size = xy_length_t(stkd.Dimensions->Chip.Length*1e-6l, stkd.Dimensions->Chip.Width*1e-6l);
			cell_size = xy_length_t(hotspot_model->grid->width/x_pitch, hotspot_model->grid->height/y_pitch);
			
			add_dissipator_components();
			
		}
		
		void adapter_t::add_dissipator_components()
		{
			// size_t base = 0;
			//vegigmegyunk a layer-eken, ha disszipalnak, megnezzuk a layer flp-jet ahol a units-okon vegigmegyunk es hozzaadjuk oket a layout-hoz
			if(hotspot_model->grid == nullptr) error("add_dissipator_components(): 'grid' is nullptr");

			for(size_t layer_index = 0, base = 0; layer_index < hotspot_model->grid->n_layers; ++layer_index)
			{
				if(hotspot_model->grid->layers[layer_index].has_power > 0)
				{
					flp_t* layer_flp = hotspot_model->grid->layers[layer_index].flp;
					if(layer_flp == nullptr) error("add_dissipator_components(): 'flp' of layer no. '" + std::to_string(layer_index) + "' is nullptr");

					for(size_t unit_index = 0; unit_index < layer_flp->n_units; ++unit_index)
					{
						/** ezzel automatikusan hozza is adodik a layout hierarchiahoz a cucc **/
						std::string name = layer_flp->units[unit_index].name;
						xy_length_t position(layer_flp->units[unit_index].leftx, layer_flp->units[unit_index].bottomy);
						xy_length_t sides(layer_flp->units[unit_index].width, layer_flp->units[unit_index].height);
						
						size_t index = get_blk_index(layer_flp, const_cast<char*>(name.c_str()));
						
						layout::hotspot::component_t* component_ptr = new layout::hotspot::component_t(layout_structure, name, position, sides, layer_index, unit_index, base+index);

						manager->add_dissipator_component(name);
					}
					base += hotspot_model->grid->layers[layer_index].flp->n_units;
				}
			}
		}
	
		/**
		 * dummy fv, mert kell implementalni
		**/ 
		void adapter_t::create_structure()
		{}
	 
		/**
		 * ez a fuggveny a logic component-ek 
		**/ 
		void adapter_t::refresh_dissipation_map(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipators)
		{
			for(size_t i=0, base=0; i < hotspot_model->grid->n_layers; i++)
			{
				if(hotspot_model->grid->layers[i].has_power)
					for(size_t j=0; j < hotspot_model->grid->layers[i].flp->n_units; j++) power_vector[base+j] = 0;
				base += hotspot_model->grid->layers[i].flp->n_units;
			}

			for(auto &dissipator: dissipators)
			{
				layout::hotspot::component_t* layout_component = dynamic_cast<layout::hotspot::component_t*>(dissipator.second);
				power_vector[layout_component->offset] = static_cast<double>(dissipator.first->get_dissipation());
			}
		}
		
		/**
		 * ez a fuggveny egy szimulacios lepesnel a logitherm_manager adataibol kiszamitja, hogy milyen pozicioban mennyit disszipalt a chip
		**/ 
		void adapter_t::calculate_temperatures()
		{
			if(first_call)
			{
				compute_temp(hotspot_model, power_vector, temperature_vector, static_cast<double>(timestep.current_timestep()));
				first_call = false;
			}
			else
			{
				compute_temp(hotspot_model, power_vector, nullptr, static_cast<double>(timestep.current_timestep()));
			}
		}

		unit::temperature_t adapter_t::get_min_temperature(layout::component_t* component)
		{
			layout::hotspot::component_t* ptr = dynamic_cast<layout::hotspot::component_t*>(component);
			size_t layer_index = ptr->layer_index;
			size_t unit_index = ptr->unit_index;

			double min;

			size_t i1 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].i1;
          	size_t j1 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].j1;
          	size_t i2 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].i2;
          	size_t j2 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].j2;

			min = hotspot_model->grid->last_trans->cuboid[layer_index][i1][j1];
			for(size_t i=i1; i < i2; i++)
			{
				for(size_t j=j1; j < j2; j++)
				{
					if (hotspot_model->grid->last_trans->cuboid[layer_index][i][j] < min) min = hotspot_model->grid->last_trans->cuboid[layer_index][i][j];
				}
			}
			return unit::temperature_t(min);
		}
		
		unit::temperature_t adapter_t::get_max_temperature(layout::component_t* component)
		{
			layout::hotspot::component_t* ptr = dynamic_cast<layout::hotspot::component_t*>(component);
			size_t layer_index = ptr->layer_index;
			size_t unit_index = ptr->unit_index;

			double max;

			size_t i1 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].i1;
          	size_t j1 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].j1;
          	size_t i2 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].i2;
          	size_t j2 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].j2;

			max = hotspot_model->grid->last_trans->cuboid[layer_index][i1][j1];
			for(size_t i=i1; i < i2; i++)
			{
				for(size_t j=j1; j < j2; j++)
				{
					if (hotspot_model->grid->last_trans->cuboid[layer_index][i][j] > max) max = hotspot_model->grid->last_trans->cuboid[layer_index][i][j];
				}
			}
			return unit::temperature_t(max);
		}

		unit::temperature_t adapter_t::get_avg_temperature(layout::component_t* component)
		{
			layout::hotspot::component_t* ptr = dynamic_cast<layout::hotspot::component_t*>(component);
			size_t layer_index = ptr->layer_index;
			size_t unit_index = ptr->unit_index;

			double avg;

			size_t i1 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].i1;
          	size_t j1 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].j1;
          	size_t i2 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].i2;
          	size_t j2 = hotspot_model->grid->layers[layer_index].g2bmap[unit_index].j2;
          	size_t count = 0;

			for(size_t i=i1; i < i2; i++)
			{
				for(size_t j=j1; j < j2; j++)
				{
					avg += hotspot_model->grid->last_trans->cuboid[layer_index][i][j];
					count++;
				}
			}
			return unit::temperature_t(avg/count);
		}

		std::ostream& adapter_t::get_layer_temperature(std::ostream& os, const std::string& layer_id)
		{
			size_t layer_index = std::stoi(layer_id);

			for(size_t j = 0; j < y_pitch; ++j)
			{
				for(size_t i = 0; i < x_pitch ; ++i)
				{
					float temperature;
					temperature = hotspot_model->grid->last_trans->cuboid[layer_index][i][j];
					os.write(reinterpret_cast<const char*>(&temperature), sizeof(float));
				}
			}
			return os;
		}
		
		/**
		 * ez a fuggveny kinyeri a termikus motor altal kiszamitott homerseklet eloszlasbol az egyes modulok homersekletet
		**/
		void adapter_t::refresh_component_temperature(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipators)
		{						
			for(auto &dissipator: dissipators)
			{
				layout::hotspot::component_t* ptr = dynamic_cast<layout::hotspot::component_t*>(dissipator.second);
				dissipator.first->temperature = static_cast<unit::temperature_t>(temperature_vector[ptr->offset]);
			}
		}
		
		std::ostream& adapter_t::print_temperature_map(std::ostream& os)
		{
			for(size_t k = 0; k < z_pitch; ++k)
			{
				os << "---------- layer [" << k << "] ----------" << std::endl;
				for(size_t j = 0; j < y_pitch; ++j)
				{
					for(size_t i = 0; i < x_pitch; ++i)
					{
						os << hotspot_model->grid->last_trans->cuboid[k][i][j] << " ";
					}
					os << std::endl;
				}		
			}
			return os;
		}
		
		std::ostream& adapter_t::print_dissipation_map(std::ostream& os)
		{
			grid_model_vector_t *p = new_grid_model_vector(hotspot_model->grid);
  			xlate_vector_b2g(hotspot_model->grid, power_vector, p, V_POWER);
			for(size_t k = 0; k < z_pitch; ++k)
			{
				os << "---------- layer [" << k << "] ----------" << std::endl;
				for(size_t j = 0; j < y_pitch; ++j)
				{
					for(size_t i = 0; i < x_pitch; ++i)
					{
						os << p->cuboid[k][i][j] << " ";
					}
					os << std::endl;
				}		
			}
			return os;
		}

		std::ostream& adapter_t::print_temperature_map_in_svg(std::ostream& os, std::unordered_set<layout::component_t*, std::hash<layout::component_t*>, layout::components_equal>& components)
		{						
			// struct color {
			// 	unsigned red;
			// 	unsigned green;
			// 	unsigned blue;

			// 	color(unsigned red = 0, unsigned green = 0, unsigned blue = 0) : red(red), green(green), blue(blue) {}

			// 	std::string to_hex() const {
			// 		std::stringstream stream;

			// 		stream << std::setfill('0');
			// 		stream << std::hex << std::setw(2) << red << std::setw(2) << green << std::setw(2) << blue;
			// 		return stream.str();
			// 	}
			// };

			// //ez egy nem tul elegans megoldas, be kellene vezetni valamilyen unit-okat (um, mm stb, ugyanez wattra es sec-re is)
			// size_t picture_grid_x =  static_cast<size_t>(cell_size[0]/pixel_size); // horizontal size of one grid in pixels
			// size_t picture_grid_y = static_cast<size_t>(cell_size[1]/pixel_size); // vertical size of one grid in pixels

			// xy_length_t actual_pixel_size(cell_size[0]/picture_grid_x, cell_size[1]/picture_grid_y);

			// auto min_max = get_temperature_range(); //megcsinalni
			// double temperature_range = std::get<1>(min_max) - std::get<0>(min_max);

			// unsigned const max_colour = 255;

			// size_t font_size = std::min(picture_grid_x, picture_grid_y) * 0.6;
			// size_t label_font_size = 2.5 * font_size;
			
			// size_t longest_name = 0;
			// size_t component_number = components.size();

			// for (auto const &a_component : components) 
			// {
			// 	size_t size = a_component->id.size();
			// 	if (size > longest_name) longest_name = size;
			// }

			// auto number_of_digits = [](size_t value){size_t digits = 1; while ((value /= 10) != 0) ++digits; return digits;};
			// size_t largest_id_size = number_of_digits(components.size());

			// //itt szamoljuk ki a kep meretet, le kell kerni a layout meretet, es onnan ki lehet szamolni
			// size_t picture_width = (static_cast<size_t>(layout_size[0]/pixel_size) + (longest_name + largest_id_size + 3.0) * label_font_size * 0.8);
			// size_t picture_height = std::max((z_pitch * static_cast<size_t>(layout_size[1]/pixel_size) + (z_pitch - 1) * picture_grid_y), (2 * component_number) * label_font_size);

			// os << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
			// os << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">" << std::endl;
			// os << "<svg width=\"" << picture_width << "\" height=\"" << picture_height << "\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">" << std::endl;

			// //Creating the thermal map layer
			// os << "\t<g id=\"thermal_map\">" << std::endl;
			
			// for (size_t i = 0; i < nodes; ++i)
			// {
			// 	//itt minden cellara ki kellene szamolni a picture_grid_x, picture_grid_y-t
			// 	double temperature = tdata.Temperatures[i]; //remelem ez igy jo lesz
			// 	double value = (temperature - std::get<0>(min_max)) / temperature_range;

			// 	size_t position_x = i % x_pitch;
			// 	size_t position_y = i / x_pitch + (i / x_pitch / y_pitch); // The second part creates a picture_grid_y high gap between layers.

			// 	unsigned red = ((value < 0.3) ? 0.0 : (1.0 / 0.7) * (value - 0.3)) * max_colour;
			// 	unsigned green = (0.5 * value * value * value * value) * max_colour;
			// 	unsigned blue = (0.5 * (1 - value * value * value)) * max_colour;
			// 	color the_color(red, green, blue);

			// 	::unit::length_t cell_x(get_cell_length(stkd.Dimensions, i % x_pitch)*1e-6l);
			// 	::unit::length_t cell_y(get_cell_width(stkd.Dimensions, (i / x_pitch) % y_pitch)*1e-6l);

			// 	size_t cell_pix_x = static_cast<size_t>(cell_x/pixel_size);
			// 	size_t cell_pix_y = static_cast<size_t>(cell_y/pixel_size);

			// 	os << "\t\t<rect x=\"" << (position_x * cell_pix_x) << "\" y=\"" << (position_y * cell_pix_y) << "\" width=\"" << cell_pix_x << "\" height=\"" << cell_pix_y << "\" style=\"fill:#" << the_color.to_hex() << ";stroke-width:0;\"/>" << std::endl;
			// }

			// os << "\t</g>" << std::endl;
			
			// //Creating the label_shapes_layer
			// os << "\t<g id=\"label_shapes_layer\">" << std::endl;

			// //	Adding the bounding boxes of the components
			// for (auto const &a_component : components)
			// {
			// 	auto component = dynamic_cast<::layout::hotspot::component_t*>(a_component);
			// 	if (!component)
			// 	{
			// 		error("print_temperature_map_in_svg(): layout component '" + a_component->id + "' has not valid type.");
			// 	}

			// 	for(auto &shape: component->get_shapes())
			// 	{
			// 		size_t const size_x = static_cast<size_t>(shape->get_side_lengths()[0]/actual_pixel_size[0]);//std::get<0>(layout->get_size());
			// 		size_t const size_y = static_cast<size_t>(shape->get_side_lengths()[1]/actual_pixel_size[1]);//std::get<1>(layout->get_size());

			// 		size_t const layer = component->layer_index;

			// 		size_t const pixel_x = static_cast<size_t>(shape->get_absolute_position()[0]/actual_pixel_size[0]);//topleft_x * picture_grid_x; 
			// 		size_t const pixel_y = static_cast<size_t>(shape->get_absolute_position()[1]/actual_pixel_size[1])+ layer * (y_pitch + 1) * picture_grid_y;//(topleft_y + layer * (y_pitch + 1)) * picture_grid_y;
			// 		size_t const width = size_x; //* picture_grid_x;
			// 		size_t const height = size_y;// * picture_grid_y;

			// 		os << "\t\t<rect x=\"" << pixel_x << "\" y=\"" << pixel_y << "\" width=\"" << width << "\" height=\"" << height << "\" style=\"fill:#434343;fill-opacity:0.25;stroke:#cccccc;stroke-width:1;stroke-opacity:0.6\"/>" << std::endl;
			// 	}
			// }

			// os << "\t</g>" << std::endl;

			/*
			//Creating the label_text_layer
			os << "\t<g id=\"label_text_layer\">" << std::endl;

			//	Adding the number of components on their bounding boxes
			size_t component_id = 0;

			for (auto const &a_component : components)
			{
				for(auto &component: a_component->get_shapes())
				{
					auto shape = dynamic_cast<::layout::hotspot::component_t*>(component);
					if (!shape)
					{
						error("print_temperature_map_in_svg(): layout component '" + component->id + "' has not valid type.");
					}

					size_t const topleft_x = static_cast<size_t>(shape->get_absolute_position()[0]/actual_pixel_size[0]);
					size_t const topleft_y = static_cast<size_t>(shape->get_absolute_position()[1]/actual_pixel_size[1]);

					size_t const size_x = static_cast<size_t>(shape->get_side_lengths()[0]/actual_pixel_size[0]);//std::get<0>(layout->get_size());
					size_t const size_y = static_cast<size_t>(shape->get_side_lengths()[1]/actual_pixel_size[1]);

					size_t const layer = shape->layer_index;

					size_t const font_size_coefficient = std::min(shape->pitch_size[0], shape->pitch_size[1]);
					size_t const local_font_size = font_size * font_size_coefficient;

					size_t const pixel_x = topleft_x + 0.5 * size_x - (number_of_digits(component_id) / 4.0 * local_font_size); 
					size_t const pixel_y = topleft_y + layer * (y_pitch + 1) * picture_grid_y + 0.5 * size_y  + (local_font_size / 4.0);


					os << "\t\t<text x=\"" << pixel_x << "\" y=\"" << pixel_y << "\" style=\"font-size:" << local_font_size << ";fill:#ffffff;fill-opacity:0.7;\">" << component_id << "</text>" << std::endl;
				}
				++component_id;
			}

			//	Adding the component names as labels on the right side
			component_id = 0;
			for (auto const &a_component : components) {
				os << "\t\t<text x=\"" << ((x_pitch + 1) * picture_grid_x) << "\" y=\"" << ((2 * component_id + 1) * label_font_size) << "\" style=\"font-size:" << label_font_size << ";fill:#323232;\">" << 
					component_id << ": " << a_component->id << "</text>" << std::endl;
				++component_id;
			}

			os << "\t</g>" << std::endl;
			*/
			// os << "</svg>" << std::endl;

			return os;

		}

		std::tuple<double, double> adapter_t::get_temperature_range()
		{
			// auto minmax_iterators = std::minmax_element(tdata.Temperatures, tdata.Temperatures + nodes);
			return std::make_tuple(0.0, 0.0);
		}
		
		
	} //namespace hotspot	
} //namespace thermal
