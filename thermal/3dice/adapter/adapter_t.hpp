#ifndef _THREED_ICE_ADAPTER_
#define _THREED_ICE_ADAPTER_


//3D ICE include-olasa

#include "thermal/base/adapter_t.hpp"
#include "thermal/3dice/adapter/layout/adapter_t.hpp"
#include "thermal/3dice/adapter/layout/component_t.hpp"
#include "thermal/base/layout/coordinates_t.hpp"

#include "thermal/3dice/engine/include/stack_description.h"
#include "thermal/3dice/engine/include/thermal_data.h"
#include "thermal/3dice/engine/include/output.h"
#include "thermal/3dice/engine/include/analysis.h"
#include "thermal/3dice/engine/include/types.h"

namespace thermal
{
	namespace threed_ice
	{
		class adapter_t : public thermal::adapter_t
		{
			private:
				using xy_length_t = layout::xy_length_t; //kell ez??

				StackDescription_t	stkd;
				Analysis_t			analysis;
				Output_t			output;
				ThermalData_t		tdata;

				size_t x_pitch;
				size_t y_pitch;
				size_t z_pitch;
				size_t nodes;

				xy_length_t layout_size;
				xy_length_t cell_size; //0,0 cella merete

				unit::length_t pixel_size;

			private:

				std::tuple<double, double> get_temperature_range();
			
				void add_dissipator_components();
			
				void init_thermal_engine();

				void create_layout();

				/**
				 * ez majd a 3d-ice megfelelo fv-eit hivja meg, illetve le kell foglalnia a dissipation_map es temperature_map valtozokat is
				**/ 
				void create_structure() override;
			 
				/**
				 * ez a fuggveny a logic component-ek 
				**/ 
				void refresh_dissipation_map(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipators) override;
				
				/**
				 * ez a fuggveny kinyeri a termikus motor altal kiszamitott homerseklet eloszlasbol az egyes modulok homersekletet
				**/
				void refresh_component_temperature(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipators) override;
			 
			public:
				adapter_t() = delete;
				adapter_t(logitherm::manager_t* manager, util::log_t* log);
				
				~adapter_t();

				void read_files(const std::string& path, const std::string& stackfile) override;

				//std::ostream& write_floorplan_file(std::ostream& os);
				/**
				 * ez a fuggveny egy szimulacios lepesnel a logitherm_manager adataibol kiszamitja, hogy milyen pozicioban mennyit disszipalt a chip
				**/ 
				void calculate_temperatures() override;
				
				
				unit::temperature_t get_min_temperature(layout::component_t* component) override;
				unit::temperature_t get_max_temperature(layout::component_t* component) override;
				unit::temperature_t get_avg_temperature(layout::component_t* component) override;
				std::ostream& get_layer_temperature(std::ostream& os, const std::string& layer_id) override;
				
				/**
				 * layout -> a file-ban a sorrend fontos, ugyanolyan sorrendben varja a disszipaciot is
				**/
			
				std::ostream& print_temperature_map(std::ostream& os) override;
				
				std::ostream& print_dissipation_map(std::ostream& os) override;
				
				std::ostream& print_temperature_map_in_svg(std::ostream& os, std::unordered_set<layout::component_t*, std::hash<layout::component_t*>, layout::components_equal>&) override;


			
		};
	} // namespace threed_ice
} //namespace thermal
#endif //_THREED_ICE_ADAPTER_
