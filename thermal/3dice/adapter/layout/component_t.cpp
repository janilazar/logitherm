#include "thermal/3dice/adapter/layout/component_t.hpp"
#include "manager/manager_t.hpp"

#include "thermal/base/layout/layer_temperature_trace_t.hpp"

namespace layout
{
	namespace threed_ice
	{
		//itt megadni poziciot meretet is. adapter-ben pedig megcsinalni, hoyg minden egyes IC elementet hozzaadjunk component-kent
		component_t::component_t(layout::adapter_t* adapter, const std::string& id, FloorplanElement_t* fe, size_t layer_index)
		:
			layout::component_t(adapter, id, nullptr, xy_length_t(0_um, 0_um), xy_length_t(0_um, 0_um)),
			floorplan_element(fe),
			ic_element(nullptr),
			layer_index(layer_index)
		{}

		layer_temperature_trace_t* component_t::add_layer_temperature_trace(const std::string& path, const std::string& postfix)
		{
			layer_temperature_trace_t* ptr = new layer_temperature_trace_t(path, std::to_string(layer_index), postfix, adapter->adapter);
			tracer.add_trace(ptr, std::ios::out | std::ios::binary);
			trace_component();
			return ptr;
		}
		
		//ITT MODOSITANI KELLEN E, HOGY AZ IC ELEMENT-EK legyenek
		//itt megadni poziciot meretet is. adapter-ben pedig megcsinalni, hoyg minden egyes IC elementet hozzaadjunk component-kent
		// component_t::component_t(::layout::adapter_t* adapter, const std::string& id, ICElement_t* ice, ::layout::threed_ice::component_t* parent)
		// :
		// 	layout::component_t(adapter, id, parent, xy_length_t(ice->SW_X, ice->SW_Y), xy_length_t(ice->Length, ice->Width)), //emiatt kell length-re sima double konstruktor is...
		// 	floorplan_element(parent->floorplan_element),
		// 	ic_element(ice),
		// 	layer_index(parent->layer_index)
		// {
		// 	//if(nullptr == floorplan_element) error("component_t(): floorplan element is nullptr");
		// }
		
		//std::ostream& component_t::print_temperature_names(std::ostream& os)
		//{
		//	os << this->id << "_min_temperature, ";
		//	os << this->id << "_max_temperature, ";
		//	os << this->id << "_avg_temperature, ";
		//	os << std::endl;
		//	return os;
		//}
		
		//std::ostream& component_t::print_temperature_values(std::ostream& os)
		//{
		//	::thermal::threed_ice::adapter_t* thermal_engine_ptr = dynamic_cast<::thermal::threed_ice::adapter_t*>(::logitherm::manager_t::get_manager().get_thermal_engine());
		//	if(thermal_engine_ptr == nullptr) throw("Thermal engine has invalid type.");
		//	thermal_engine_ptr->print_component_temperature(os, this);
		//	os << std::endl;
		//	return os;
		//}
		
		/**
		 * kapott ostream-re kirakja a component es leszarmazottak nevet
		**/ 
		std::ostream& component_t::print_component(std::ostream& os, std::string indent)
		{
			//size, pposition???
			os << indent << id << std::endl;
			if(!children.empty())
			{
				indent += "  ";
				for(auto &it: children)
				{
					it.second->print_component(os, indent);
				}
			}
			return os;
		}
	} //namespace threed_ice
}//namespace layout
