#include "thermal/3dice/adapter/layout/adapter_t.hpp"

namespace layout
{
	namespace threed_ice
	{
		adapter_t::adapter_t(thermal::adapter_t* adapter, util::log_t* log, layout::xyz_pitch_t pitch)
		:
			layout::adapter_t(adapter, log, "layout::threed_ice::adapter_t"),
			pitch(pitch)
		{}
		
		adapter_t::~adapter_t()
		{
			delete_components_tree();
		}

		xyz_pitch_t adapter_t::get_pitch()
		{
			return pitch;
		}

		xyz_length_t adapter_t::get_length()
		{
			warning("get_length(): this function has not been implemented yet");
			return xyz_length_t(0_m, 0_m, 0_m);
		}
	}
}
