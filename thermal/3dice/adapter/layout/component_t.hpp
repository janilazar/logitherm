#ifndef _LAYOUT_THREED_ICE_COMPONENT_
#define _LAYOUT_THREED_ICE_COMPONENT_

#include "thermal/base/layout/component_t.hpp"
#include "thermal/3dice/engine/include/floorplan_element.h"
#include "thermal/3dice/engine/include/ic_element.h"

namespace layout
{
	class layer_temperature_trace_t;
	
	namespace threed_ice
	{
		class component_t	:	public layout::component_t
		{
			public:
				FloorplanElement_t* const floorplan_element;
				ICElement_t* const ic_element;
				const size_t layer_index;
				//Stackelement* ???? pointer arra a stackelementre, ami tartalmazza a floorplanelementet
				
			public:
				component_t() = delete;
				component_t(layout::adapter_t* adapter, const std::string& id, FloorplanElement_t* fe, size_t layer_index);
				// component_t(layout::adapter_t* adapter, const std::string& id, ICElement_t* ice, layout::threed_ice::component_t* parent);

				layer_temperature_trace_t* add_layer_temperature_trace(const std::string& path, const std::string& postfix = "") override;
				
				
				//std::ostream& print_temperature_names(std::ostream& os) override;
				
				//std::ostream& print_temperature_values(std::ostream& os) override;
				
				//ez vajon kell a dynamic_cast-hoz???
				std::ostream& print_component(std::ostream& os, std::string indent = "") override;
		};
	} //namespace threed_ice
} //namespace layout
#endif //_LAYOUT_THREED_ICE_COMPONENT_
