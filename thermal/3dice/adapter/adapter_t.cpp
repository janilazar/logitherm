#include "thermal/3dice/adapter/adapter_t.hpp"
#include "manager/manager_t.hpp"
#include "util/log_t.hpp"
#include "thermal/3dice/engine/include/stack_file_parser.h"

#include "thermal/3dice/adapter/layout/adapter_t.hpp"
#include "thermal/3dice/adapter/layout/component_t.hpp"

#include <memory>
#include <functional>
#include <sstream>
#include <iomanip>
#include <algorithm>

namespace thermal
{
	namespace threed_ice
	{
		adapter_t::adapter_t(logitherm::manager_t* manager, util::log_t* log)
		:
			thermal::adapter_t(manager, log, "thermal::threed_ice::adapter_t", util::thermal_engine_t::threedice, manager->get_timestep()),
			layout_size(0_um, 0_um),
			cell_size(0_um, 0_um),
			pixel_size(0.5_um)
		{}
		
		adapter_t::~adapter_t()
		{
			thermal_data_destroy(&tdata);
			output_destroy(&output);
			stack_description_destroy(&stkd);
			analysis_destroy(&analysis);

			//delete layout_structure; //ososztalyban is deletezzuk, ez a baj!!!4negy
		}

		void adapter_t::init_thermal_engine()
		{
			stack_description_init(&stkd);
			thermal_data_init(&tdata);
			analysis_init(&analysis);
			output_init(&output);
		}

		void adapter_t::read_files(const std::string& path, const std::string& stackfile)
		{
			init_thermal_engine();
			Error_t error_code = parse_stack_description_file(const_cast<char*>((path+stackfile).c_str()), &stkd, &analysis, &output);
			if (error_code != TDICE_SUCCESS)
			{
				error("create_structure(): parsing stack description file failed");
			}
			create_layout();
		}

		void adapter_t::create_layout()
		{
			x_pitch = stkd.Dimensions->Grid.NColumns;
			y_pitch = stkd.Dimensions->Grid.NRows;
			z_pitch = stkd.Dimensions->Grid.NLayers;
			nodes = x_pitch * y_pitch * z_pitch;
			layout_structure = new layout::threed_ice::adapter_t(this, log, layout::xyz_pitch_t(x_pitch, y_pitch, z_pitch));
			thermal_data_build(&tdata, &stkd.StackElements, stkd.Dimensions, &analysis);
			
			// TODO ez itt biztosan jo? tenyleg a chip_length az x koordinata?, chip_width pedig az y?
			layout_size = xy_length_t(stkd.Dimensions->Chip.Length*1e-6l, stkd.Dimensions->Chip.Width*1e-6l);
			cell_size = xy_length_t(get_cell_length(stkd.Dimensions, 0)*1e-6l, get_cell_width(stkd.Dimensions, 0)*1e-6l);
			
			add_dissipator_components();
			
		}
		
		void adapter_t::add_dissipator_components()
		{

			PowerGrid_t* pgrid = &tdata.PowerGrid;
			
			Quantity_t layer ;

			/**
			 * ez a ciklus ugy jarja be a stack-et ahogy az insert_power_values() fv is
			**/
			for (layer = 0u ; layer != pgrid->NLayers ; layer++)
			{
				switch (pgrid->LayersProfile [layer])
				{
					case TDICE_LAYER_SOURCE:
					case TDICE_LAYER_SOURCE_CONNECTED_TO_AMBIENT:
					{
						Floorplan_t* current_floorplan = pgrid->FloorplansProfile[layer];
						/**
						 * ez a ciklus vegigmeny a current_floorplan elementjein, es mindegyik element nevet berakta a floorplan_element_list-be
						**/ 
						for(FloorplanElementListNode_t* current_node_element = floorplan_element_list_begin(&current_floorplan->ElementsList); current_node_element != nullptr; current_node_element = floorplan_element_list_next(current_node_element))
						{
							FloorplanElement_t *current_element = floorplan_element_list_data(current_node_element);
							
							/** ezzel automatikusan hozza is adodik a layout hierarchiahoz a cucc **/
							layout::threed_ice::component_t* component_ptr = new layout::threed_ice::component_t(layout_structure, current_element->Id, current_element, static_cast<size_t>(layer));
							
							size_t ic_element_count = 0;
							for(ICElementListNode_t* current_ic_node_element = ic_element_list_begin(&current_element->ICElements); current_ic_node_element != nullptr; current_ic_node_element = ic_element_list_next(current_ic_node_element))
							{
								//ITT INKABB CSAK SHAPE-T KELLENE HOZZAADNI AZ ELOZO COMPONENT_PTR-HEZ, NEM UJ COMPONENTET CSINALNI
								ICElement_t* current_ic_element = ic_element_list_data(current_ic_node_element);
								layout::xy_length_t reference_position(current_ic_element->SW_X, current_ic_element->SW_Y);
								layout::xy_length_t absolute_position = component_ptr->get_absolute_position()+reference_position;
								layout::xy_length_t sides(current_ic_element->Length, current_ic_element->Width);
								layout::shape_t* shape = new layout::shape_t(reference_position, absolute_position, sides);
								component_ptr->add_shape(shape);
							}
							/**
							 * igy minden floorplan element hozza lesz adva a dissipator-okhoz
							**/ 
							manager->add_dissipator_component(current_element->Id);
						}									
						break;
					}
					default:
						break;
				}
			}
		}
	
		/**
		 * dummy fv, mert kell implementalni
		**/ 
		void adapter_t::create_structure()
		{}
	 
		/**
		 * ez a fuggveny a logic component-ek 
		**/ 
		void adapter_t::refresh_dissipation_map(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipators)
		{
			//ami meg szar lehet az az, hogy a floorplan element power_queue-ja nincs inicializalva
			for(auto &it: dissipators)
			{
				put_into_powers_queue(dynamic_cast<layout::threed_ice::component_t*>(it.second)->floorplan_element->PowerValues, static_cast<double>(it.first->get_dissipation()));
			}
		}
		
		/**
		 * ez a fuggveny egy szimulacios lepesnel a logitherm_manager adataibol kiszamitja, hogy milyen pozicioban mennyit disszipalt a chip
		**/ 
		void adapter_t::calculate_temperatures()
		{
			SimResult_t sim_result = emulate_slot(&tdata, stkd.Dimensions, &analysis); // szerintem ez lesz a nyero -> increments the simulation time until the simulation time of a single power value finishes.
			if(TDICE_WRONG_CONFIG == sim_result)
			{
				error("calculate_temperatures(): the emulation function does not match the configuration");
			}
			if(TDICE_SOLVER_ERROR == sim_result)
			{
				error("calculate_temperatures(): the solver routine failed");
			}
		}

		unit::temperature_t adapter_t::get_min_temperature(layout::component_t* component)
		{
			layout::threed_ice::component_t* ptr = dynamic_cast<layout::threed_ice::component_t*>(component);
			if(ptr == nullptr) error("get_min_temperature(): layout component '" + ptr->id + "' has invalid type");

			size_t layer_size = stkd.Dimensions->Grid.NColumns*stkd.Dimensions->Grid.NRows;
			return static_cast<unit::temperature_t>(get_min_temperature_floorplan_element(ptr->floorplan_element, stkd.Dimensions, tdata.Temperatures + ptr->layer_index*layer_size));
		}
		
		unit::temperature_t adapter_t::get_max_temperature(layout::component_t* component)
		{
			layout::threed_ice::component_t* ptr = dynamic_cast<layout::threed_ice::component_t*>(component);
			if(ptr == nullptr) error("get_max_temperature(): layout component '" + ptr->id + "' has invalid type");
			
			size_t layer_size = stkd.Dimensions->Grid.NColumns*stkd.Dimensions->Grid.NRows;
			return static_cast<unit::temperature_t>(get_max_temperature_floorplan_element(ptr->floorplan_element, stkd.Dimensions, tdata.Temperatures + ptr->layer_index*layer_size));
		}

		unit::temperature_t adapter_t::get_avg_temperature(layout::component_t* component)
		{
			layout::threed_ice::component_t* ptr = dynamic_cast<layout::threed_ice::component_t*>(component);
			if(ptr == nullptr) error("get_avg_temperature(): layout component '" + ptr->id + "' has invalid type");
			
			size_t layer_size = stkd.Dimensions->Grid.NColumns*stkd.Dimensions->Grid.NRows;
			return static_cast<unit::temperature_t>(get_avg_temperature_floorplan_element(ptr->floorplan_element, stkd.Dimensions, tdata.Temperatures + ptr->layer_index*layer_size));
		}

		std::ostream& adapter_t::get_layer_temperature(std::ostream& os, const std::string& layer_id)
		{
			size_t layer_index = std::stoi(layer_id);
			double* temperature_map = tdata.Temperatures;

			if(nullptr != temperature_map)
			{
				for(size_t j = 0; j < y_pitch; ++j)
				{
					for(size_t i = 0; i < x_pitch ; ++i)
					{
						float temperature;
						temperature = get_cell_temperature(&tdata, stkd.Dimensions, layer_index, y_pitch-1-j, i);
						os.write(reinterpret_cast<const char*>(&temperature), sizeof(float));
					}
				}		
			}
			return os;
		}
		
		/**
		 * ez a fuggveny kinyeri a termikus motor altal kiszamitott homerseklet eloszlasbol az egyes modulok homersekletet
		**/
		void adapter_t::refresh_component_temperature(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipators)
		{
			generate_output(&output, stkd.Dimensions, tdata.Temperatures, tdata.PowerGrid.Sources, get_simulated_time(&analysis), TDICE_OUTPUT_INSTANT_SLOT);
			
			size_t layer_size = stkd.Dimensions->Grid.NColumns*stkd.Dimensions->Grid.NRows;

			for(auto &it: dissipators)
			{
				layout::threed_ice::component_t* layout_component = dynamic_cast<layout::threed_ice::component_t*>(it.second);
				if(layout_component == nullptr)
				{
					error("refresh_component_temperature(): layout component '" + it.second->id + "' has invalid type.");
				}

				it.first->temperature = static_cast<unit::temperature_t>(get_avg_temperature_floorplan_element(layout_component->floorplan_element, stkd.Dimensions, tdata.Temperatures+layout_component->layer_index*layer_size));
			}	
			
		}
		
		std::ostream& adapter_t::print_temperature_map(std::ostream& os)
		{
			double* temperature_map = tdata.Temperatures;
			if(nullptr != temperature_map)
			{
				for(size_t k = 0; k < z_pitch; ++k)
				{
					os << "---------- layer [" << k << "] ----------" << std::endl;
					for(size_t j = 0; j < y_pitch; ++j)
					{
						for(size_t i = 0; i < x_pitch; ++i)
						{
							os << get_cell_temperature(&tdata, stkd.Dimensions, k, j, i) << " ";
						}
						os << std::endl;
					}		
				}
			}
			return os;
		}
		
		std::ostream& adapter_t::print_dissipation_map(std::ostream& os)
		{
			double* dissipation_map = tdata.PowerGrid.Sources;

			if(nullptr != dissipation_map)
			{
				for(size_t k = 0; k < z_pitch; ++k)
				{
					os << "---------- layer [" << k << "] ----------" << std::endl;
					for(size_t j = 0; j < y_pitch; ++j)
					{
						for(size_t i = 0; i < x_pitch; ++i)
						{
							os << dissipation_map[i + j * x_pitch + k * x_pitch * y_pitch] << " ";
						}
						os << std::endl;
					}		
				}
			}
			return os;
		}

		std::ostream& adapter_t::print_temperature_map_in_svg(std::ostream& os, std::unordered_set<layout::component_t*, std::hash<layout::component_t*>, layout::components_equal>& components)
		{						
			struct color {
				unsigned red;
				unsigned green;
				unsigned blue;

				color(unsigned red = 0, unsigned green = 0, unsigned blue = 0) : red(red), green(green), blue(blue) {}

				std::string to_hex() const {
					std::stringstream stream;

					stream << std::setfill('0');
					stream << std::hex << std::setw(2) << red << std::setw(2) << green << std::setw(2) << blue;
					return stream.str();
				}
			};

			//ez egy nem tul elegans megoldas, be kellene vezetni valamilyen unit-okat (um, mm stb, ugyanez wattra es sec-re is)
			size_t picture_grid_x =  static_cast<size_t>(cell_size[0]/pixel_size); // horizontal size of one grid in pixels
			size_t picture_grid_y = static_cast<size_t>(cell_size[1]/pixel_size); // vertical size of one grid in pixels

			xy_length_t actual_pixel_size(cell_size[0]/picture_grid_x, cell_size[1]/picture_grid_y);

			auto min_max = get_temperature_range(); //megcsinalni
			double temperature_range = std::get<1>(min_max) - std::get<0>(min_max);

			unsigned const max_colour = 255;

			size_t font_size = std::min(picture_grid_x, picture_grid_y) * 0.6;
			size_t label_font_size = 2.5 * font_size;
			
			size_t longest_name = 0;
			size_t component_number = components.size();

			for (auto const &a_component : components) 
			{
				size_t size = a_component->id.size();
				if (size > longest_name) longest_name = size;
			}

			auto number_of_digits = [](size_t value){size_t digits = 1; while ((value /= 10) != 0) ++digits; return digits;};
			size_t largest_id_size = number_of_digits(components.size());

			//itt szamoljuk ki a kep meretet, le kell kerni a layout meretet, es onnan ki lehet szamolni
			size_t picture_width = (static_cast<size_t>(layout_size[0]/pixel_size) + (longest_name + largest_id_size + 3.0) * label_font_size * 0.8);
			size_t picture_height = std::max((z_pitch * static_cast<size_t>(layout_size[1]/pixel_size) + (z_pitch - 1) * picture_grid_y), (2 * component_number) * label_font_size);

			os << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
			os << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">" << std::endl;
			os << "<svg width=\"" << picture_width << "\" height=\"" << picture_height << "\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">" << std::endl;

			//Creating the thermal map layer
			os << "\t<g id=\"thermal_map\">" << std::endl;
			
			for (size_t i = 0; i < nodes; ++i)
			{
				//itt minden cellara ki kellene szamolni a picture_grid_x, picture_grid_y-t
				double temperature = tdata.Temperatures[i]; //remelem ez igy jo lesz
				double value = (temperature - std::get<0>(min_max)) / temperature_range;

				size_t position_x = i % x_pitch;
				size_t position_y = i / x_pitch + (i / x_pitch / y_pitch); // The second part creates a picture_grid_y high gap between layers.

				unsigned red = ((value < 0.3) ? 0.0 : (1.0 / 0.7) * (value - 0.3)) * max_colour;
				unsigned green = (0.5 * value * value * value * value) * max_colour;
				unsigned blue = (0.5 * (1 - value * value * value)) * max_colour;
				color the_color(red, green, blue);

				::unit::length_t cell_x(get_cell_length(stkd.Dimensions, i % x_pitch)*1e-6l);
				::unit::length_t cell_y(get_cell_width(stkd.Dimensions, (i / x_pitch) % y_pitch)*1e-6l);

				size_t cell_pix_x = static_cast<size_t>(cell_x/pixel_size);
				size_t cell_pix_y = static_cast<size_t>(cell_y/pixel_size);

				os << "\t\t<rect x=\"" << (position_x * cell_pix_x) << "\" y=\"" << (position_y * cell_pix_y) << "\" width=\"" << cell_pix_x << "\" height=\"" << cell_pix_y << "\" style=\"fill:#" << the_color.to_hex() << ";stroke-width:0;\"/>" << std::endl;
			}

			os << "\t</g>" << std::endl;
			
			//Creating the label_shapes_layer
			os << "\t<g id=\"label_shapes_layer\">" << std::endl;

			//	Adding the bounding boxes of the components
			for (auto const &a_component : components)
			{
				auto component = dynamic_cast<::layout::threed_ice::component_t*>(a_component);
				if (!component)
				{
					error("print_temperature_map_in_svg(): layout component '" + a_component->id + "' has not valid type.");
				}

				for(auto &shape: component->get_shapes())
				{
					size_t const size_x = static_cast<size_t>(shape->get_side_lengths()[0]/actual_pixel_size[0]);//std::get<0>(layout->get_size());
					size_t const size_y = static_cast<size_t>(shape->get_side_lengths()[1]/actual_pixel_size[1]);//std::get<1>(layout->get_size());

					size_t const layer = component->layer_index;

					size_t const pixel_x = static_cast<size_t>(shape->get_absolute_position()[0]/actual_pixel_size[0]);//topleft_x * picture_grid_x; 
					size_t const pixel_y = static_cast<size_t>(shape->get_absolute_position()[1]/actual_pixel_size[1])+ layer * (y_pitch + 1) * picture_grid_y;//(topleft_y + layer * (y_pitch + 1)) * picture_grid_y;
					size_t const width = size_x; //* picture_grid_x;
					size_t const height = size_y;// * picture_grid_y;

					os << "\t\t<rect x=\"" << pixel_x << "\" y=\"" << pixel_y << "\" width=\"" << width << "\" height=\"" << height << "\" style=\"fill:#434343;fill-opacity:0.25;stroke:#cccccc;stroke-width:1;stroke-opacity:0.6\"/>" << std::endl;
				}
			}

			os << "\t</g>" << std::endl;

			/*
			//Creating the label_text_layer
			os << "\t<g id=\"label_text_layer\">" << std::endl;

			//	Adding the number of components on their bounding boxes
			size_t component_id = 0;

			for (auto const &a_component : components)
			{
				for(auto &component: a_component->get_shapes())
				{
					auto shape = dynamic_cast<::layout::threed_ice::component_t*>(component);
					if (!shape)
					{
						error("print_temperature_map_in_svg(): layout component '" + component->id + "' has not valid type.");
					}

					size_t const topleft_x = static_cast<size_t>(shape->get_absolute_position()[0]/actual_pixel_size[0]);
					size_t const topleft_y = static_cast<size_t>(shape->get_absolute_position()[1]/actual_pixel_size[1]);

					size_t const size_x = static_cast<size_t>(shape->get_side_lengths()[0]/actual_pixel_size[0]);//std::get<0>(layout->get_size());
					size_t const size_y = static_cast<size_t>(shape->get_side_lengths()[1]/actual_pixel_size[1]);

					size_t const layer = shape->layer_index;

					size_t const font_size_coefficient = std::min(shape->pitch_size[0], shape->pitch_size[1]);
					size_t const local_font_size = font_size * font_size_coefficient;

					size_t const pixel_x = topleft_x + 0.5 * size_x - (number_of_digits(component_id) / 4.0 * local_font_size); 
					size_t const pixel_y = topleft_y + layer * (y_pitch + 1) * picture_grid_y + 0.5 * size_y  + (local_font_size / 4.0);


					os << "\t\t<text x=\"" << pixel_x << "\" y=\"" << pixel_y << "\" style=\"font-size:" << local_font_size << ";fill:#ffffff;fill-opacity:0.7;\">" << component_id << "</text>" << std::endl;
				}
				++component_id;
			}

			//	Adding the component names as labels on the right side
			component_id = 0;
			for (auto const &a_component : components) {
				os << "\t\t<text x=\"" << ((x_pitch + 1) * picture_grid_x) << "\" y=\"" << ((2 * component_id + 1) * label_font_size) << "\" style=\"font-size:" << label_font_size << ";fill:#323232;\">" << 
					component_id << ": " << a_component->id << "</text>" << std::endl;
				++component_id;
			}

			os << "\t</g>" << std::endl;
			*/
			os << "</svg>" << std::endl;

			return os;

		}

		std::tuple<double, double> adapter_t::get_temperature_range()
		{
			auto minmax_iterators = std::minmax_element(tdata.Temperatures, tdata.Temperatures + nodes);
			return std::make_tuple(*minmax_iterators.first, *minmax_iterators.second);
		}
		
		
	} //namespace threed_ice	
} //namespace thermal
