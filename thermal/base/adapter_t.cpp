#include "thermal/base/adapter_t.hpp"
#include "manager/manager_t.hpp"
#include "util/log_t.hpp"

namespace thermal
{
	void adapter_t::debug(const char* msg) const
	{
		log->debug("'" + id + "' " + msg);
	}
	
	void adapter_t::debug(const std::string& msg) const
	{
		log->debug("'" + id + "' " + msg);
	}
	
	void adapter_t::warning(const char* msg) const
	{
		log->warning("'" + id + "' " + msg);
	}
	
	void adapter_t::warning(const std::string& msg) const
	{
		log->warning("'" + id + "' " + msg);
	}
	
	void adapter_t::error(const char* msg) const
	{
		log->error("'" + id + "' " + msg);
	}
	
	void adapter_t::error(const std::string& msg) const
	{
		log->error("'" + id + "' " + msg);
	}
	
	adapter_t::adapter_t(logitherm::manager_t* manager, util::log_t* log, const std::string& id, util::thermal_engine_t type, const util::timestep_t& timestep)
	:
		layout_structure(nullptr),
		id(id),
		type(type),
		structure_created(false),
		manager(manager),
		log(log),
		timestep(timestep)
	{}
	
	adapter_t::~adapter_t()
	{
		debug("~adapter_t()");
		delete layout_structure;
	}

	void adapter_t::initialize()
	{
		if(structure_created == false)
		{
			structure_created = true;
			create_structure();
		}
	}

	bool adapter_t::initialized() const
	{
		return structure_created;
	}
	
	void adapter_t::refresh_temperatures(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipator_components)
	{
		if(structure_created != true)
		{
			initialize();
			if(dissipator_components.size() == 0) warning("refresh_temperatures(): there are no dissipator components");
		}

		if(dissipator_components.size() > 0)
		{
			refresh_dissipation_map(dissipator_components);
			calculate_temperatures(); // -> itt is lehetne alkotni valamit...
			refresh_component_temperature(dissipator_components);
		}

	}
	
	std::ostream& adapter_t::print_temperature_map_in_svg(std::ostream& os, std::unordered_set<layout::component_t*, std::hash<layout::component_t*>, layout::components_equal>& display_components)
	{
		return os;
	}
	
	layout::adapter_t* adapter_t::get_layout()
	{
		if(nullptr == layout_structure) warning("get_layout(): layout structure was not allocated.");
		return layout_structure;
	}
	
	void adapter_t::timestep_changed()
	{}
}
