#ifndef _THERMAL_ADAPTER_
#define _THERMAL_ADAPTER_


#include <iostream>
#include <unordered_set>

#include "thermal/base/layout/adapter_t.hpp"
#include "logic/base/component_t.hpp"
#include "unit/all_units.hpp"
#include "util/timestep_t.hpp"

/** forward decl **/
namespace logitherm
{
	class manager_t;
}

namespace util
{
	class log_t;
}

/**
 * a logitermikus szimulaciohoz hasznalatos termikus motorok adapter osztalyanak
 * ebbol az absztrakt osztalybol kell leszarmaznia
**/ 
namespace thermal
{
	class adapter_t
	{
		private:
			friend class layout::adapter_t; //ez miert kell???

		public:
			const std::string id;
			const util::thermal_engine_t type;
			
		private:
			bool structure_created;
			
		protected:
			/**
			 * az absztrakt layout hierarchia
			**/
			layout::adapter_t* layout_structure; //unique_ptr??
			
		public:
			/**
			 * injected dependency
			**/
			logitherm::manager_t* const manager;
			util::log_t* const log;
		
			/**
			 * co-szimulacio idolepteke
			**/ 
			const util::timestep_t& timestep;
			
		private:			
			/**
			 * ez fuggveny a szimulalando (3D) IC felepitese alapjan a termikus motor igenyei
			 * szerint lefoglalja a szukseges eroforrasokat, beallitja a fenti 3 pointert
			**/ 
			virtual void create_structure() = 0;
			
			/**
			 * ez a fuggveny egy szimulacios lepesnel a logitherm_proxy adataibol kiszamitja, hogy milyen pozicioban mennyit disszipalt a chip
			**/ 
			virtual void refresh_dissipation_map(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>&) = 0;
			
			/**
			 * ez a fuggveny kinyeri a termikus motor altal kiszamitott homerseklet eloszlasbol az egyes modulok homersekletet
			**/
			virtual void refresh_component_temperature(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>&) = 0;
		
		public:

			adapter_t() = delete;
			adapter_t(const adapter_t& other) = delete;

			/**
			 * termikus motor default konstruktora, beallitja a dolgokat nullptr-re
			**/ 			
			adapter_t(logitherm::manager_t* manager, util::log_t* log, const std::string& id, util::thermal_engine_t type, const util::timestep_t& timestep);

			/**
			 * egy masik termikus motor alapjan inicializalodik
			**/
			// adapter_t(const adapter_t& other, logitherm::manager_t* manager, util::log_t* log, const std::string& id, const util::timestep_t& timestep);

			/**
			 * destruktor, felszabaditja a dinamikusan foglalt eroforrasokat
			**/ 
			virtual ~adapter_t();

			/**
			 * ez a fv hivja meg a private create_structure fv-t.
			**/
			void initialize();

			/**
			 * visszater true-val ha az initialize fv mar lefutott
			**/
			bool initialized() const;

			/**
			 * ennek a fuggvenynek kell kiszamitania az idolepesben az uj homerseklet adatokat
			 * ebbol az osztalybol leszarmazo termikus motor adapter osztalynak ezt a fuggvenyt
			 * meg kell valositania
			**/ 
			virtual void calculate_temperatures() = 0;
			
			/**
			 * ezt a fuggvenyt kell meghivni bizonyos idokozonkent a logikai motornak
			 * ez a fuggveny megnezi, hogy a struktura letre van-e hozva (ha nem, akkor meghivodik a create_structure() fv)
			 * ezutan frissiti a dissipation_map-et (kiszamitodik, hogy melyik modul mennyit disszipalt)
			 * majd meghivodik a calculate_temperature() fv, ami kiszamitja az uj homerseklet eloszlast
			 * ezutan visszairodnak a homerseklet adatok komponensekbe
			 * ellenorzes: layout mar inicializalva van
			**/ 
			void refresh_temperatures(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipator_components);
			
			virtual unit::temperature_t get_min_temperature(layout::component_t* component) = 0;
			virtual unit::temperature_t get_max_temperature(layout::component_t* component) = 0;
			virtual unit::temperature_t get_avg_temperature(layout::component_t* component) = 0;
			virtual std::ostream& get_layer_temperature(std::ostream& os, const std::string& layer_id) = 0;

			virtual std::ostream& print_temperature_map(std::ostream& os) = 0;
			virtual std::ostream& print_dissipation_map(std::ostream& os) = 0;
			virtual std::ostream& print_temperature_map_in_svg(std::ostream& os, std::unordered_set<layout::component_t*, std::hash<layout::component_t*>, layout::components_equal>& display_components);
			
			void debug(const char* msg) const;
			void debug(const std::string& msg) const;
			void warning(const char* msg) const;
			void warning(const std::string& msg) const;
			void error(const char*) const;
			void error(const std::string& msg) const;

			layout::adapter_t* get_layout();
			
			virtual void timestep_changed();
			
			virtual void read_files(const std::string&, const std::string&) = 0;
			
			
		
	};
} // namespace thermal

#endif //_THERMAL_ADAPTER_
