#ifndef _TEMPERATURE_DIST_TRACE_
#define _TEMPERATURE_DIST_TRACE_

// #include "thermal/base/adapter/adapter_t.hpp"
// #include "thermal/base/adapter/layout/adapter_t.hpp"
#include "util/trace_t.hpp"
#include "util/timestep_t.hpp"

namespace thermal
{
	class adapter_t;
}

namespace layout
{
	class layer_temperature_trace_t	:	public util::trace_t
	{
		private:
			size_t counter;
			const std::string layer_id;
			thermal::adapter_t* thermal_engine;
			const util::timestep_t& timestep;
			
		public:
			/**
			 * path -> file eleresi utvonala
			 * prefix -> trace nevenek eleje, jellemzoen a modul neve
			 * id -> trace azonositoja
			**/ 
			layer_temperature_trace_t(const std::string& path, const std::string& layer_id, const std::string& postfix, thermal::adapter_t* thermal_engine);
			
			void initialize(std::ostream& os) override;
			
			void trace(std::ostream& os) override;
			
			void reset() override;
			
			void finalize(std::ostream& os) override;
			
	};
} //namespace thermal

#endif //_TEMPERATURE_DIST_TRACE_
