#include "thermal/base/layout/adapter_t.hpp"
#include "thermal/base/adapter_t.hpp"
#include "manager/manager_t.hpp"
#include "util/log_t.hpp"

namespace layout
{
	void adapter_t::debug(const char* msg) const
	{
		log->debug("'" + id + "' " + msg);
	}
	
	void adapter_t::debug(const std::string& msg) const
	{
		log->debug("'" + id + "' " + msg);
	}
	
	void adapter_t::warning(const char* msg) const
	{
		log->warning("'" + id + "' " + msg);
	}
	
	void adapter_t::warning(const std::string& msg) const
	{
		log->warning("'" + id + "' " + msg);
	}
	
	void adapter_t::error(const char* msg) const
	{
		log->error("'" + id + "' " + msg);
	}
	
	void adapter_t::error(const std::string& msg) const
	{
		log->error("'" + id + "' " + msg);
	}
	
	adapter_t::adapter_t(thermal::adapter_t* adapter, util::log_t* log, const std::string& id)
	:
		id(id),
		adapter(adapter),
		manager(adapter->manager),
		log(log)
	{}

	adapter_t::adapter_t(const adapter_t& other, thermal::adapter_t* adapter, util::log_t* log, const std::string& id)
	:
		id(id),
		adapter(adapter),
		components_tree(other.components_tree),
		manager(adapter->manager),
		log(log)
	{}
	
	adapter_t::~adapter_t()
	{
		debug("~adapter_t()");
	}
	
	void adapter_t::delete_components_tree()
	{
		for(auto &it: components_tree)
		{
			it.second->delete_children();
			delete it.second;
		}
	}
	
	/**
	 * megkeresi az adott layout komponenst a layout_component_ts_tree-ben
	 * a layout_component_t search_for_component fv-t hasznalja fel rekurzivan
	**/ 
	bool adapter_t::search_for_component(const std::string& name) const
	{
		return (nullptr != get_component(name));
	}
	
	component_t* adapter_t::get_component(const std::string& name) const
	{
		component_t* component = nullptr;
		if(components_tree.find(name) != components_tree.end())
		{
			component = components_tree.find(name)->second;
		}
		else
		{
			for(auto &it: components_tree)
			{
				component = it.second->get_component(name);
				if (nullptr != component) break;
			}			
		}
		return component;
	}
	
	/**
	 * egy layout komponenst ad hozza a layout_component_ts_tree-hez
	 * vegig kell iteralnia minden szinten a component_treen, hogy megkeresse a parent-et
	 * hibaellenorzes: component mar egyszer szerepel a tree-ben (nev az azonosito)
	 * hibaellenorzes: a layer nem letezik, amihez hozza kellene adni
	 * hibaellenorzes: a parent nem szerepel a tree-ben
	**/ 
	void adapter_t::add_component(component_t* component)
	{
		if(search_for_component(component->id))
		{
			warning("layout::adapter_t::add_component(): component '" + component->id + "' is already in the 'components_tree'\n");
		}
		else
		{
		
			if(component->parent != nullptr)
			{
				if(!search_for_component(component->parent->id))
				{
					error("layout::adapter_t::add_component(): could not find the parent '" + component->parent->id + "' of the layout component '" + component->id + "'");
				}
				else
				{
					component->parent->add_child(component);
				}
			}
			else
			{
				components_tree.insert(std::make_pair(component->id,component));
			}
		}
	}

	/**
	*	szimulacio kezdete elott meg kell hivni
	*	vegigmegy a komponenseken es elvegzi a szukseges ellenorzeseket
	**/
//	void adapter_t::check_tree()
//	{
//		for(auto &it: components_tree) check_component(it.second);
//	}
//
//	void adapter_t::check_component(component_t* component)
//	{
//		for(auto &it: component->get_children()) check_component(it.second);
//	}
	
	xyz_pitch_t adapter_t::get_pitch()
	{
		return xyz_pitch_t(0,0,0);
	}

	xyz_length_t adapter_t::get_length()
	{
		return xyz_length_t(0_m, 0_m, 0_m);
	}

	std::ostream& adapter_t::print_tree(std::ostream& os)
	{
		for(auto &it: components_tree) it.second->print_component(os);
		return os;
	}		
} //namespace layout
