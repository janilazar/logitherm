#include "thermal/base/layout/max_temperature_trace_t.hpp"
#include "thermal/base/adapter_t.hpp"
#include "thermal/base/layout/component_t.hpp"
#include "manager/manager_t.hpp"
#include "util/string_manipulation.hpp"

namespace layout
{
	max_temperature_trace_t::max_temperature_trace_t(const std::string& path, const std::string& postfix, component_t* component)
	:
		util::trace_t(path, component->id, "max_temperature", postfix),
		component(component),
		layout_structure(component->adapter),
		thermal_engine(layout_structure->adapter),
		timestep(thermal_engine->timestep)
	{
		if("" == id) layout_structure->error("avg_temperature_trace_t(): invalid trace id");
	}

	void max_temperature_trace_t::initialize(std::ostream& os)
	{
		os << "timestamp, " << id << "_" << type;
	}

	void max_temperature_trace_t::trace(std::ostream& os)
	{
		os << util::to_string(timestep.current_time()) << ", " << thermal_engine->get_max_temperature(component); 
	}

	void max_temperature_trace_t::reset()
	{}
	
	void max_temperature_trace_t::finalize(std::ostream& os)
	{}

}
