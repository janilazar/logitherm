#ifndef _LAYOUT_BASE_
#define _LAYOUT_BASE_

#include "thermal/base/layout/component_t.hpp"

/** forward decl **/
namespace logitherm
{
	class manager_t;
}

namespace thermal
{
	class adapter_t;
}

namespace util
{
	class log_t;
}

namespace layout
{
	class adapter_t
	{
		public:
			const std::string id;
			
		private:
			friend class layout::component_t;

			/**
			 * az osszes layout komponens taroloja
			**/ 
			std::unordered_map<std::string,layout::component_t*> components_tree;
		
		public:
			/**
			 * injected dependency
			**/
			logitherm::manager_t* const manager;
			thermal::adapter_t* const adapter;
			util::log_t* const log;

		private:
			/**
			 * egy layout komponenst ad hozza a layout_components_tree-hez
			 * vegig kell iteralnia minden szinten a component_treen, hogy megkeresse a parent-et
			 * hibaellenorzes: component mar egyszer szerepel a tree-ben (nev az azonosito)
			 * hibaellenorzes: a parent nem szerepel a tree-ben
			**/ 
			void add_component(component_t*);
			
		public:

			adapter_t(const adapter_t& other) = delete;

			adapter_t(thermal::adapter_t* adapter, util::log_t* log, const std::string& id);
			adapter_t(const adapter_t& other, thermal::adapter_t* adapter, util::log_t* log, const std::string& id);
			
			virtual ~adapter_t();
			
			void delete_components_tree();
			
			/**
			 * megkeresi az adott logikai komponenst a logic_components_tree-ben
			 * a layout_component search_for_component fv-t hasznalja fel rekurzivan
			**/ 
			bool search_for_component(const std::string& name) const;
			component_t* get_component(const std::string& name) const;

			virtual xyz_pitch_t get_pitch();
			virtual xyz_length_t get_length();
			
			
			
			//void check_tree();
			//virtual void check_component(component_t*);
			
			virtual std::ostream& print_tree(std::ostream& os);

			void debug(const char* msg) const;
			void debug(const std::string& msg) const;
			void warning(const char* msg) const;
			void warning(const std::string& msg) const;
			void error(const char*) const;
			void error(const std::string& msg) const;
			
			
	};
	
}
#endif //_LAYOUT_BASE_
