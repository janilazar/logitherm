#ifndef _LAYOUT_SHAPE_
#define _LAYOUT_SHAPE_

#include "thermal/base/layout/coordinates_t.hpp"
#include "unit/length_t.hpp"
#include "unit/area_t.hpp"

namespace layout
{
	using xy_length_t	= coordinates_t<unit::length_t, 2>;
	class shape_t
	{
		private:
			xy_length_t reference_position;
			xy_length_t absolute_position;
			xy_length_t side_length;
			unit::area_t area;
		
		public:
			shape_t() = delete;
			
			shape_t(xy_length_t reference_position, xy_length_t absolute_position, xy_length_t sides)
			:
				reference_position(reference_position),
				absolute_position(absolute_position),
				side_length(sides),
				area(sides[0]*sides[1])
			{}

			virtual ~shape_t()
			{}

			//void set_absolute_position(xy_length_t position)
			//{
			//	absolute_position = position;
			//}

			xy_length_t get_absolute_position() const
			{
				return absolute_position;
			}
			
			xy_length_t get_reference_position() const
			{
				return reference_position;
			}

			xy_length_t get_side_lengths() const
			{
				return side_length;
			}

			unit::area_t get_area() const
			{
				return area;
			}
	};
}


#endif //_LAYOUT_SHAPE_