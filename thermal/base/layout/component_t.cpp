#include "thermal/base/layout/component_t.hpp"
#include "thermal/base/layout/adapter_t.hpp"
#include "thermal/base/adapter_t.hpp"
#include "manager/manager_t.hpp"
#include "util/log_t.hpp"

#include "thermal/base/layout/min_temperature_trace_t.hpp"
#include "thermal/base/layout/max_temperature_trace_t.hpp"
#include "thermal/base/layout/avg_temperature_trace_t.hpp"
#include "thermal/base/layout/layer_temperature_trace_t.hpp"

#include "tr3ster/adapter/time_constant_spectra_trace_t.hpp"

namespace layout
{
	
	void component_t::debug(const char* msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr!\nDEBUG MSG '" + msg + "'");
		log->debug("'" + id + "' " + msg);
	}
	
	void component_t::debug(const std::string& msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr!\nDEBUG MSG '" + msg + "'");
		log->debug("'" + id + "' " + msg);
	}
	
	void component_t::warning(const char* msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr\nWARNING MSG '" + msg + "'");
		log->warning("'" + id + "' " + msg);
	}
	
	void component_t::warning(const std::string& msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr\nWARNING MSG '" + msg + "'");
		log->warning("'" + id + "' " + msg);
	}
	
	void component_t::error(const char* msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr\nERROR MSG '" + msg + "'");
		log->error("'" + id + "' " + msg);
	}
	
	void component_t::error(const std::string& msg) const
	{
		if(log == nullptr) throw("ERROR '"+ id + "' log is nullptr\nERROR MSG '" + msg + "'");
		log->error("'" + id + "' " + msg);
	}

	/**
	 * konstruktor
	**/ 
	component_t::component_t(layout::adapter_t* adapter, const std::string& id, component_t* const parent,  xy_length_t position, xy_length_t sides)
	:
		shape_t(position, ((parent!=nullptr) ? parent->get_absolute_position()+position : position), sides),
		adapter(adapter),
		log((adapter != nullptr) ? adapter->log : nullptr),
		id(id),
		parent(parent),
		tracer("layout_component", this->id, this->log)
	{
		if(nullptr == adapter)
		{
			std::cerr << "WARNING '"<< id <<"' component_t(): layout adapter is nullptr" << std::endl;
		}
		else
		{
			adapter->add_component(this);
		}
	}
	
	component_t::~component_t()
	{}
	
	/**
	 * hozzaad egy uj child-ot a children-hez
	 * hibaellenorzes: adott komponens mar megtalalhato a childrenben
	**/
	void component_t::add_child(component_t* component)
	{
		if(children.find(component->id) != children.end())
		{
			warning("component_t::add_child(): layout component '" + component->id + "'  is already among the children");
		}
		else
		{
			children.insert(std::make_pair(component->id,component));
			shapes.push_back(component);
		}
	}
	
	/**
	 * torli a component_et es minden elemet a childrenben
	**/ 
	void component_t::delete_children()
	{
		for(auto &it: children)
		{
			delete it.second;
		}
	}

	void component_t::add_shape(shape_t* shape)
	{
		shapes.push_back(shape);
	}
	
	void component_t::delete_shapes()
	{
		for(auto &it: shapes)
		{
			delete it;
		}
	}

	
	void component_t::delete_component()
	{
		delete_children();
		delete_shapes();
	}
	
	/**
	 * layout_component-ben es a childrenjeiben keressuk a component-et
	 * BFS szeru bejaras: ha nincs components-ben, akkor vegigmegyunk components-en,
	 * es mindegyik children-jeben rakeresunk 
	**/ 
	bool component_t::search_for_component(const std::string& name) const
	{
		return (nullptr != get_component(name));
	}
	
	component_t* component_t::get_component(const std::string& name) const
	{
		component_t* component = nullptr;
		if(children.find(name) != children.end())
		{
			component = children.find(name)->second;
		}
		else
		{
			for (auto &it: children)
			{
				component = it.second->get_component(name);
				if(nullptr != component) break;
			}
		}
		return component;
	}

	const std::unordered_map<std::string,layout::component_t*>& component_t::get_children()
	{
		return children;
	}

	const std::vector<shape_t*>& component_t::get_shapes()
	{
		if(shapes.size() == 0)
		{
			shapes.push_back(this);
			warning("get_shapes(): shapes vector size was 0.");
		}
		return shapes;
	}


	const std::vector<shape_t*>& component_t::initialize_shapes_from_children()
	{
		if(!children.empty())
		{
			for(auto &child: children)
			{
				for(auto &shape: child.second->initialize_shapes_from_children())
				{
					shapes.push_back(shape);
				}
			}
		}
		// else
		// {
			return get_shapes();
		// }
	}

	unit::area_t component_t::calculate_area()
	{

		// if(shapes.size() > 0)
		// {
			unit::area_t total_area = 0_um2;
			for(auto shape: get_shapes())
			{
				total_area += shape->get_area();
			}
			// if(std::isnan(static_cast<double>(total_area)))
			// {
			// 	std::cout << id << " calculate_area =" << total_area << std::endl;
			// }
			return total_area;
		// }
		// else
		// {
		// 	// unit::area_t total_area = get_area();
		// 	// if(std::isnan(static_cast<double>(total_area)))
		// 	// {
		// 	// 	std::cout << id << " calculate_area =" << total_area << std::endl;
		// 	// }
		// 	return get_area();
		// }
		

		// if(std::isnan(static_cast<double>(total_area)))
		// {
		// 	std::cout << id << " total_area=" << total_area << std::endl;
		// }

		
		
		// if(std::isnan(static_cast<double>(total_area)))
		// {
		// 	std::cout << id << " total_area=" << total_area << std::endl;
		// }
		
	}
//	xy_length_t component_t::get_absolute_position() const
//	{
//		xy_length_t abs_pos = get_reference_position();
//		if (nullptr != parent)
//		{
//			if(nullptr == parent)
//			{
//				error("layout::component_t::get_absolute_position(): layout component '" + parent->id + "' has invalid type.");
//			}
//			
//			abs_pos += parent->get_reference_position();
//		}
//		return abs_pos;
//	}

		min_temperature_trace_t* component_t::add_min_temperature_trace(const std::string& path, const std::string& postfix)
		{
			min_temperature_trace_t* ptr = new min_temperature_trace_t(path, postfix, this);
			tracer.add_trace(ptr);
			trace_component();
			return ptr;
		}
		
		max_temperature_trace_t* component_t::add_max_temperature_trace(const std::string& path, const std::string& postfix)
		{
			max_temperature_trace_t* ptr = new max_temperature_trace_t(path, postfix, this);
			tracer.add_trace(ptr);
			trace_component();
			return ptr;
		}
		
		avg_temperature_trace_t* component_t::add_avg_temperature_trace(const std::string& path, const std::string& postfix)
		{
			avg_temperature_trace_t* ptr = new avg_temperature_trace_t(path, postfix, this);
			tracer.add_trace(ptr);
			trace_component();
			return ptr;
		}

		tr3ster::time_constant_spectra_trace_t* component_t::calculate_time_constant_spectra(const std::string& path, const std::string& postfix)
		{
			tr3ster::time_constant_spectra_trace_t* ptr = new tr3ster::time_constant_spectra_trace_t(path, postfix, this);
			tracer.add_trace(ptr);
			trace_component();
			return ptr;
		}
	
	/**
	 * kapott ostream-re kirakja a component es leszarmazottak nevet
	**/ 
	std::ostream& component_t::print_component(std::ostream& os, std::string indent)
	{
		os << indent << id << std::endl;
		if(!children.empty())
		{
			indent += "  ";
			for(auto &it: children)
			{
				it.second->print_component(os, indent);
			}
		}
		return os;
	}

	void component_t::trace_component()
	{
		if(adapter == nullptr) error("'"+ id + "' adapter is nullptr");
		adapter->manager->trace_component(&this->tracer);
	}
	
} //namespace layout

