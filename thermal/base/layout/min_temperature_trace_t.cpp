#include "thermal/base/layout/min_temperature_trace_t.hpp"
#include "thermal/base/adapter_t.hpp"
#include "thermal/base/layout/component_t.hpp"
#include "manager/manager_t.hpp"
#include "util/string_manipulation.hpp"

namespace layout
{
	min_temperature_trace_t::min_temperature_trace_t(const std::string& path, const std::string& postfix, component_t* component)
	:
		util::trace_t(path, component->id, "min_temperature", postfix),
		component(component),
		layout_structure(component->adapter),
		thermal_engine(layout_structure->adapter),
		timestep(thermal_engine->timestep)
	{
		if("" == id) layout_structure->error("avg_temperature_trace_t(): invalid trace id");
	}

	void min_temperature_trace_t::initialize(std::ostream& os)
	{
		os << "timestamp, " << id << "_" << type;
	}

	void min_temperature_trace_t::trace(std::ostream& os)
	{
		os << util::to_string(timestep.current_time()) << ", " << thermal_engine->get_min_temperature(component); 
	}

	void min_temperature_trace_t::reset()
	{}
	
	void min_temperature_trace_t::finalize(std::ostream& os)
	{}
}
