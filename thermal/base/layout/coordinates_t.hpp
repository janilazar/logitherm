#ifndef _LAYOUT_COORDINATES_
#define _LAYOUT_COORDINATES_

#include <iostream>

#include "unit/length_t.hpp"

namespace layout
{
		template<typename type, size_t dimension>
		class coordinates_t
		{
			private:
				
				type value[dimension];
				
			public:
				/**
				 * default konstruktor
				 * 0-ra inicializalja a koordinatakat
				 * feltetel: legyen konverzio 0 literalisrol type tipusra
				**/ 
				coordinates_t()
				{
					for(auto &it: value) it = static_cast<type>(0.0l);
				}
				
				/**
				 * konstruktor, ami inicializalo lista alapjan allitja be a tarolt tombot
				 * ellenorzes: a listanak ugyanolyan hosszunak kell lennie, mint amennyi dimenzios a coordinate
				 * !!MAGIC!!
				**/ 
				template<typename...T>
				coordinates_t(T...ts) : value{static_cast<type>(ts)...} /* MAGIC */
				{}
				
				/**
				 * operator+ a coordinate tipusra
				**/ 
				coordinates_t operator+ (const coordinates_t& rhs) const
				{
					coordinates_t result = rhs;
					for (size_t it = 0; it < dimension; ++it) result.value[it] += value[it];
					return result;
				}

				coordinates_t operator- (const coordinates_t& rhs) const
				{
					coordinates_t result = rhs;
					for (size_t it = 0; it < dimension; ++it) result.value[it] -= value[it];
					return result;
				}
				
				type& operator[] (size_t index)
				{
					if(index >= dimension) throw(std::string("operator[] is out of bound"));
					return value[index];
				}
				
				type operator[] (size_t index) const
				{
					if(index >= dimension) throw(std::string("operator[] is out of bound"));
					return value[index];
				}
				
				
				bool operator < (const coordinates_t& rhs) const
				{
					for(size_t it = 0; it < dimension; ++it)
					{
						if(value[it] >= rhs.value[it])
						{
							return false;
						}
					}
					return true;
				}
				
				friend std::ostream& operator<< (std::ostream& os, const coordinates_t<type,dimension>& obj) //ezzel miafasz baj van?
				{
					os << obj.value[0];
					for(size_t it=1; it<dimension; ++it) os << ", " << obj.value[it];
					return os;
				}
		};

		using xy_length_t = coordinates_t<unit::length_t, 2>;
		using xyz_length_t = coordinates_t<unit::length_t, 3>;
		using xy_pitch_t = coordinates_t<size_t, 2>;
		using xyz_pitch_t = coordinates_t<size_t, 3>;
} //namespace layout

#endif //_UTIL_COORDINATES_
