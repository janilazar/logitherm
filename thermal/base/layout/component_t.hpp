#ifndef _LAYOUT_COMPONENT_
#define _LAYOUT_COMPONENT_

#include <string>
#include <unordered_map>
#include <iostream>

#include "util/tracer_t.hpp"
#include "thermal/base/layout/shape_t.hpp"

/** forward decl **/
namespace logitherm
{
	class manager_t;
}

namespace util
{
	class log_t;
}

namespace tr3ster
{
	class time_constant_spectra_trace_t;
}

namespace layout
{	
	class adapter_t;
	class min_temperature_trace_t;
	class max_temperature_trace_t;
	class avg_temperature_trace_t;
	class layer_temperature_trace_t;
	/**
	 * a layout hierarchiat megvalosito osztaly
	 * egy layout_component egy komponens a layout-on
	 * egy layout_component-ben tovabbi layout_component-ek lehetnek
	**/ 	
	class component_t	:	public shape_t
	{
		public:
			/**
			 * injection dependency
			**/
			layout::adapter_t* const adapter;
			util::log_t* const log;
		
			const std::string id;
			
			/**
			 * a parentre mutato pointer
			 * ha nincs parent, akkor nullptr
			**/ 
			component_t* const parent;

			//const xy_length_t absolute_position;
						
		protected:
			/**
			 * a megfigyelendo adatok rogziteseert felelos valtozo
			**/ 
			util::tracer_t tracer;

			/**
			 * children-ekre mutato pointer
			 * osszehasonlitas a nev string alapjan
			**/
			std::unordered_map<std::string, layout::component_t*> children;

			/**
			 * a componentet alkoto alakzatok, amik a layout-on vannak
			 *
			*/
			std::vector<shape_t*> shapes; //ez itt miert nem csak siman shape???

		public:
			/**
			 * default konstruktor letiltva
			**/ 
			component_t() = delete;
			
			/**
			 * konstruktor, ami a nev, relativ pozicio, meret es a alyer szama alapjan hoz letre egy layout component-et
			**/ 
			component_t(layout::adapter_t* adapter, const std::string& id, component_t* parent, xy_length_t position, xy_length_t sides);
			
			/**
			 * virtualis destruktor
			**/ 
			virtual ~component_t();	
			
			/**
			 * hozzaad egy uj child-ot a children-hez
			 * hibaellenorzes: adott komponens mar megtalalhato a childrenben
			**/
			void add_child(component_t* component);

			/**
			 * torol minden elemet a childrenben
			**/ 
			void delete_children();


			void add_shape(shape_t* shape);
			
			/**
			 * torol minden elemet a shapes-ben
			**/
			void delete_shapes();

			void delete_component();

			const std::unordered_map<std::string,layout::component_t*>& get_children();

			const std::vector<shape_t*>& get_shapes();

			const std::vector<shape_t*>& initialize_shapes_from_children();

			unit::area_t calculate_area();

			/**
			 * visszaadja az adott komponens abszolut poziciojat a layout-on (xy pozicio)
			**/	
			//xy_length_t get_absolute_position() const;
			
			/**
			 * layout_component-ben es a childrenjeiben keressuk a component-et
			 * BFS szeru bejaras: ha nincs components-ben, akkor vegigmegyunk components-en,
			 * es mindegyik children-jeben rakeresunk 
			**/ 
			bool search_for_component(const std::string& name) const;
			
			component_t* get_component(const std::string& name) const;

			void trace_component();
			
			min_temperature_trace_t* add_min_temperature_trace(const std::string& path, const std::string& postfix = "");
			max_temperature_trace_t* add_max_temperature_trace(const std::string& path, const std::string& postfix = "");
			avg_temperature_trace_t* add_avg_temperature_trace(const std::string& path, const std::string& postfix = "");
			tr3ster::time_constant_spectra_trace_t* calculate_time_constant_spectra(const std::string& path, const std::string& postfix = "");
			virtual layer_temperature_trace_t* add_layer_temperature_trace(const std::string& path, const std::string& postfix = "") = 0;

			/**
			 * kapott ostream-re kirakja a component es leszarmazottak nevet
			**/ 
			virtual std::ostream& print_component(std::ostream& os, std::string indent = "");

			void debug(const char* msg) const;
			void debug(const std::string& msg) const;
			void warning(const char* msg) const;
			void warning(const std::string& msg) const;
			void error(const char*) const;
			void error(const std::string& msg) const;

	};
		
	/**
	 * osszehasonlito functor az std::set-hez
	 * az osszehasonlitas a nev alapjan tortenik
	**/
	struct components_equal
	{	 
		bool operator() (const component_t* lhs, const component_t* rhs) const
		{
			return lhs->id == rhs->id;
		}
		
	};
} // namespace layout

namespace std
{
	template<>
	struct hash<layout::component_t*>
	{	 
		size_t operator() (const layout::component_t* component) const
		{
			if(component == nullptr) throw("hash<logic::component_t*>operator(): operand is nullptr");
			return hash<string>()(component->id);
		}
		
	};
}
#endif // _LAYOUT_COMPONENT_
