#include "thermal/base/layout/layer_temperature_trace_t.hpp"
#include "thermal/base/adapter_t.hpp"
#include "thermal/base/layout/component_t.hpp"
#include "manager/manager_t.hpp"

namespace layout
{
	layer_temperature_trace_t::layer_temperature_trace_t(const std::string& path, const std::string& layer_id, const std::string& postfix, thermal::adapter_t* thermal_engine)
	:
		util::trace_t(path, layer_id, "temperature_distribution", postfix),
		counter(0),
		layer_id(layer_id),
		thermal_engine(thermal_engine),
		timestep(thermal_engine->timestep)
	{
		if("" == id) thermal_engine->error("layer_temperature_trace_t(): invalid trace id");
	}

	void layer_temperature_trace_t::initialize(std::ostream& os)
	{		
		unsigned char size_t_size = sizeof(size_t);
		size_t frame_number = 0;
		if(thermal_engine->get_layout() == nullptr) thermal_engine->error("layer_temperature_trace_t::initialize(): layout is nullptr");

		size_t width = thermal_engine->get_layout()->get_pitch()[0];
		size_t height = thermal_engine->get_layout()->get_pitch()[1];
		
		/** print header **/
		os.write(reinterpret_cast<const char*>(&size_t_size), sizeof(size_t_size));
		os.write(reinterpret_cast<const char*>(&frame_number),size_t_size);
		os.write(reinterpret_cast<const char*>(&width),size_t_size);
		os.write(reinterpret_cast<const char*>(&height),size_t_size);
	}

	void layer_temperature_trace_t::trace(std::ostream& os)
	{
		os.write(reinterpret_cast<const char*>(&counter),sizeof(size_t)); // ez muxik
		++counter;
		thermal_engine->get_layer_temperature(os, layer_id);
	}

	void layer_temperature_trace_t::reset()
	{}
	
	void layer_temperature_trace_t::finalize(std::ostream& os)
	{
		os.seekp(std::ios::beg+sizeof(unsigned char));
		os.write(reinterpret_cast<const char*>(&counter),sizeof(size_t));
	}
}
