#ifndef _MIN_TEMPERATURE_TRACE_
#define _MIN_TEMPERATURE_TRACE_

#include "util/trace_t.hpp"
#include "util/timestep_t.hpp"

namespace thermal
{
	class adapter_t;
}

namespace layout
{
	class component_t;
	class adapter_t;
	class min_temperature_trace_t	:	public ::util::trace_t
	{
		private:
			layout::component_t* component;
			layout::adapter_t* layout_structure;
			thermal::adapter_t* thermal_engine;
			const util::timestep_t& timestep; 
					
		public:
			/**
			 * path -> file eleresi utvonala
			 * prefix -> trace nevenek eleje, jellemzoen a modul neve
			 * id -> trace azonositoja
			**/ 
			min_temperature_trace_t(const std::string& path, const std::string& postfix, component_t* component);
			
			void initialize(std::ostream& os) override;
			
			void trace(std::ostream& os) override;
			
			void reset() override;	
			
			void finalize(std::ostream& os) override;
	};
} //namespace layout

#endif //_MIN_TEMPERATURE_TRACE_
