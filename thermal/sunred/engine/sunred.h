//#pragma GCC diagnostic push
//#pragma GCC diagnostic ignored "-Wdelete-non-virtual-dtor"
//#pragma GCC diagnostic ignored "-Wreorder"
//#pragma GCC diagnostic ignored "-Wunused-variable"
//#pragma GCC diagnostic ignored "-Wunused-but-set-variable"
//#pragma GCC diagnostic ignored "-Wswitch"
//#pragma GCC diagnostic ignored "-Wformat"
//#pragma GCC diagnostic ignored "-Wdeprecated"
//#pragma GCC diagnostic ignored "-Wunknown-pragmas"
//#pragma GCC diagnostic ignored "-Wcomment"
//#pragma GCC diagnostic ignored "-Wsign-compare"
//#pragma GCC diagnostic ignored "-Wparentheses"

#pragma GCC system_header

#include "PLString.h"
#include "PLThread.h"
#include "apa.h"
#include "bmp.h"
#include "dmatrix.h"
#include "drcella.h"
#include "drhalozat.h"
#include "gfunc.h"
#include "hiba.h"
#include "isspace.h"
#include "listaestomb.h"
#include "main.h"
#include "masina.h"
#include "tipusok.h"

//#pragma GCC diagnostic pop
