//***********************************************************************
// Vector SUNRED halozat class source
// Creation date:	2004. 05. 19.
// Creator:			Pohl L�szl�
// Owner:			Budapest University of Technology and Economics
//					Department of Electron Devices
// Modified date:	2004. 05. 19.
//***********************************************************************


//***********************************************************************
#ifndef VSUN_QCHALOZAT_SOURCE
#define	VSUN_QCHALOZAT_SOURCE
//***********************************************************************


//***********************************************************************
#include "tipusok.h"
#include "qchalozat.h"
//***********************************************************************
#define qrhalozat qchalozat
#define qr0halo qc0halo
#define qrsubstep qcsubstep
#define qrcella qccella
#define qrcellahalo qccellahalo
#define qszalparam qcszalparam
#define qnoderedThread qcnoderedThread
#define qnodered_0_szalak qcnodered_0_szalak
#define qforwsubs_0_szalak qcforwsubs_0_szalak
#define qbacksubs_0_szalak qcbacksubs_0_szalak
#undef VSUN_QHALOZAT_SOURCE
//***********************************************************************
#include "qrhalozat.cpp"
//***********************************************************************
#undef qrhalozat
#undef qr0halo
#undef qrsubstep
#undef qrcella
#undef qrcellahalo
#undef qszalparam
#undef qnoderedThread
#undef qnodered_0_szalak
#undef qforwsubs_0_szalak
#undef qbacksubs_0_szalak


//***********************************************************************
#endif
