//***********************************************************************
// Vector SUNRED halozat class header
// Creation date:	2004. 05. 19.
// Creator:			Pohl L�szl�
// Owner:			Budapest University of Technology and Economics
//					Department of Electron Devices
// Modified date:	2004. 06. 08.
//***********************************************************************


//***********************************************************************
#ifndef VSUN_QCHALOZAT_HEADER
#define	VSUN_QCHALOZAT_HEADER
//***********************************************************************

#define qrhalozat qchalozat
#define qr0halo qc0halo
#define qrsubstep qcsubstep
#define qrcella qccella
#define qrcellahalo qccellahalo
#undef VSUN_QHALOZAT_HEADER
//***********************************************************************
#include "qrhalozat.h"
//***********************************************************************
#undef qrhalozat
#undef qr0halo
#undef qrsubstep
#undef qrcella
#undef qrcellahalo


//***********************************************************************
#endif


