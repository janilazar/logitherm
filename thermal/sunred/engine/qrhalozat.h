//***********************************************************************
// Vector SUNRED halozat class header
// Creation date:	2004. 04. 03.
// Creator:			Pohl L�szl�
// Owner:			Budapest University of Technology and Economics
//					Department of Electron Devices
// Modified date:	2008. 07. 21.
//***********************************************************************


//***********************************************************************
#ifndef VSUN_QHALOZAT_HEADER
#define	VSUN_QHALOZAT_HEADER
//***********************************************************************


//***********************************************************************
#include "tipusok.h"
#include "qrcella.h"
//***********************************************************************


//***********************************************************************
class qrhalozat;	// val�s h�l�zat, az �sszes �sszevon�si l�p�st tartalmazza
class qrsubstep;	// val�s h�l�zat �sszevon�si l�p�se
//***********************************************************************


//***********************************************************************
class qrsubstep{
//***********************************************************************
	qrcella		**halo;
	u32			x_size,y_size,z_size;
	u32			n;//n=x_size*y_size*z_size;
	irany		ir;
	bool		noExt;//Ha nincsenek external node-ok, true (lastReduce �ll�tja)

	// ideiglenes f�jl

	PLString	TempFajlNeve,VTempFajlNeve;
	bool		mentett;
public:
	qrsubstep(){halo=0;x_size=y_size=z_size=n=0;ir=X_IRANY;mentett=false;}
	qrsubstep(cu32 x,cu32 y,cu32 z);
	~qrsubstep(){free();}

	u32 getx(){return x_size;}
	u32 gety(){return y_size;}
	u32 getz(){return z_size;}
    qrcella ** gethalo(){return halo;}
    void AllocV(){for(uns i=0;i<n;i++)if(halo[i]!=NULL)halo[i]->allocV();}
    void AllocM(){for(uns i=0;i<n;i++)if(halo[i]!=NULL)halo[i]->allocM();}
    void AllocY(){for(uns i=0;i<n;i++)if(halo[i]!=NULL)halo[i]->allocY();}

	void free();
	void freeY();

	void noderedTh(cu32 min,cu32 max);

	void nodered(qrsubstep & src,const irany I,const bool usetmp);//src-t I ir�nyban �sszevonja, a cella forwsubsot is megh�vja
	void nodered();//0. l�p�s redukci�ja
	void lastReduce(qrsubstep & src,const irany I,const bool usetmp);//az utols� redukci�s l�p�s
	void forwsubs(qrsubstep * src);// j vektorok sz�m�t�sa
	void lastForw(qrsubstep * src){// Az utols� forwsubs
		if(noExt){
			if(mentett)Load();
			(*halo)->forwLast();
			if(mentett){
				if(src)src->FreeM();
				FreeM();
			}
		}
		else{
            forwsubs(src);
            (**halo).extern_j_reduce();
        }
	}
	void backsubs(qrsubstep * src);// 
	void firstBack(qrsubstep * src,bool aramot){//Az utols� backsubs
		if(noExt){
			if(mentett){
				Load();
				if(src)src->Load();
			}
			(*halo)->backFirst();
			if(mentett)FreeM();
		}
		else{
            (**halo).extern_solve();
            (**halo).extern_backsubs(aramot);
            backsubs(src);
        }
	}

	void Save();
	void SaveV();
	void SaveVha(){if(mentett)SaveV();}
	void Store();
	void Load();
	void FreeM(){SaveV();for(u32 i=0;i<n;i++)halo[i]->free();}

	void zero_n(){n=0;} // A qrhalozat destruktora haszn�lja, hogy qrsubstep::free-ben ne t�r�lja a halo t�mb elemeit (mert azok csak pointerek a "nulladik" megfelelo elem�re), csak mag�t a halo t�bm�t
	void clear(){qrcella ** p=halo;for(u32 i=n;i!=0;i--){if(*p!=0)delete *p;*(p++)=0;}} // kit�rli a cell�kat, ha vannak

	//***********************************************************************
	qrcella * getpoi(cu32 x,cu32 y,cu32 z){
	// dokument�l�sn�l haszn�ljuk
	//***********************************************************************
		if(x>=x_size||y>=y_size||z>=z_size)return 0;
		return halo[z*x_size*y_size+y*x_size+x];
	}
	//***********************************************************************
	qrcella & getref(cu32 x,cu32 y,cu32 z){
	// cella felt�lt�sn�l haszn�ljuk
	//***********************************************************************
#ifdef vsundebugmode
		if(x>=x_size||y>=y_size||z>=z_size)throw hiba("","program error -> qrsubstep::getref() -> illegal index");
#endif
		return *halo[z*x_size*y_size+y*x_size+x];
	}
};
//***********************************************************************
	

//***********************************************************************
class qrhalozat{
//***********************************************************************
	qrsubstep	**lepesek;		// az egyes l�p�sekhez tartoz� h�l�zatok ([0]-ba a feladat::fill teszi az elemeket)

	u32			x_size,y_size,z_size;				// a lepesek h�l�zat kezd� m�rete
	u32			n;//n=x_size*y_size*z_size;			// a lepesek h�l�zat kezd� m�rete
	u32			x_step,y_step,z_step;				// adott ir�ny� l�p�sek sz�ma
	u32			n_step;								// n_step=x_step+y_step+z_step+1;
public:
	qrhalozat(cu32 xsize,cu32 ysize,cu32 zsize);
	~qrhalozat(){if(lepesek!=0){for(u32 i=0;i<n_step;i++)if(lepesek[i]!=0){delete lepesek[i];lepesek[i]=0;};delete [] lepesek;}}

	void nodered(const bool EnableLastStep,const bool usetmp); // a cella megh�vja forwsubsot is
	void forwsubs(const bool EnableLastStep);
	void backsubs(const bool EnableLastStep,bool aramot);
    qrsubstep *get0(){return lepesek[0];}
};
//***********************************************************************


//***********************************************************************
inline qrsubstep::qrsubstep(cu32 x,cu32 y,cu32 z){
//***********************************************************************
	x_size=x;
	y_size=y;
	z_size=z;
	n=x*y*z;
	halo=new qrcella*[n];
	if(!halo)throw hiba("","qrsubstep::qrsubstep -> alloc failed");
	qrcella ** p=halo;
	for(u32 i=n;i!=0;i--)*(p++)=0;
	ir=X_IRANY;
	mentett=false;
}


//***********************************************************************
inline void qrsubstep::free()
//***********************************************************************
{
	qrcella **p=halo;
	for(u32 i=n;i!=0;i--,p++)if(*p!=0){delete *p;*p=0;}
	delete [] halo;
	halo=0;
	if(TempFajlNeve!="")remove(TempFajlNeve.c_str());
	if(VTempFajlNeve!="")remove(VTempFajlNeve.c_str());
}


//***********************************************************************
inline void qrsubstep::freeY()
//***********************************************************************
{
	qrcella **p=halo;
	for(u32 i=n;i!=0;i--,p++)if(*p!=0)(*p)->freeY();
}


//######################################################################


//***********************************************************************
inline void qrhalozat::forwsubs(const bool EnableLastStep)
//***********************************************************************
{
	for(u32 i=0;i<n_step-1;i++){
//		status_update_IV((PLString("step ")+(i+1)+PLString(" of ")+n_step).c_str());
		lepesek[i]->forwsubs(i?lepesek[i-1]:NULL);
	}
//	status_update_IV((PLString("step ")+n_step+PLString(" of ")+n_step).c_str());
	if(EnableLastStep)lepesek[n_step-1]->lastForw(n_step>1?lepesek[n_step-2]:NULL);
	else lepesek[n_step-1]->forwsubs(n_step>1?lepesek[n_step-2]:NULL);
}


//***********************************************************************
inline void qrhalozat::backsubs(const bool EnableLastStep,bool aramot)
// Ha van external csom�pont, akkor annak a dolga, hogy u �rt�keket j�l
// be�ll�tsa ennek kezel�s�t nem �rtam meg.
//***********************************************************************
{
//	status_update_IV((PLString("step ")+1+PLString(" of ")+n_step).c_str());
	if(EnableLastStep)lepesek[n_step-1]->firstBack(n_step>1?lepesek[n_step-2]:NULL,aramot);
	else lepesek[n_step-1]->backsubs(n_step>1?lepesek[n_step-2]:NULL);
	for(u32 i=n_step-2;i!=0;i--){
//		status_update_IV((PLString("step ")+(n_step-i)+PLString(" of ")+n_step).c_str());
		lepesek[i]->backsubs(i?lepesek[i-1]:NULL);
	}
//	status_update_IV((PLString("step ")+n_step+PLString(" of ")+n_step).c_str());
	lepesek[0]->backsubs(NULL);//ub-ket �ll�tja el� (oc1==CENTER)
}


//***********************************************************************
#endif


