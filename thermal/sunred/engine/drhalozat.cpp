//***********************************************************************
// Vector SUNRED halozat class source
// Creation date:	2004. 04. 04.
// Creator:			Pohl L�szl�
//***********************************************************************


//***********************************************************************
#ifndef VSUN_DRHALOZAT_SOURCE
#define	VSUN_DRHALOZAT_SOURCE
//***********************************************************************


//***********************************************************************
#include "tipusok.h"
#include "drhalozat.h"
//***********************************************************************

#ifdef DRCELLA_DIND
drcella::dind * drcella::dblokk=NULL;
#endif // DRCELLA_DIND

/*
//***********************************************************************
struct szalparam{
//***********************************************************************
	u32 min;
	u32 max;
	drsubstep * p;
};


//***********************************************************************
unsigned __stdcall noderedThread(void* pArguments){
//***********************************************************************
	szalparam & par=*((szalparam*)pArguments);
	par.p->noderedTh(par.min,par.max);
	_endthreadex(0);
	return 0;
}
*/

//***********************************************************************
void drsubstep::noderedTh(cu32 min,cu32 max){
//***********************************************************************
	u32 elozo=0;
    drmatrix xa,xx;
//	status_update_V("  0%");
	for(u32 i=min;i<max;i++){
//		if(elozo!=(i*100)/n){
//			elozo=(i*100)/n;
//			status_update_V((PLString(" ")+(elozo)+PLString("%")).c_str());
//		}
		if(halo[i]!=0)halo[i]->nodered(xa,xx);
	}
//	status_update_V("100%");
}


//***********************************************************************
class dnodered_0_szalak:public PLThread{
//***********************************************************************
	drcella ** halo;
	cu32 kezd,veg;
public:
	dnodered_0_szalak(cu32 k,cu32 v,drcella ** hal):kezd(k),veg(v),halo(hal){}
	void run(){
        drmatrix xa,xx;
		for(u32 i=kezd;i<veg;i++)
			if(halo[i]!=0)halo[i]->nodered(xa,xx);

		pthread_mutex_lock(&Substep_Mutex);
//        printf("VSUN_Akt_Thread=%u, kezd=%u, veg=%u\n",VSUN_Akt_Thread,kezd,veg);
		if(VSUN_Akt_Thread)VSUN_Akt_Thread--;
		pthread_mutex_unlock(&Substep_Mutex);
	}
};


//***********************************************************************
void drsubstep::nodered(){
//***********************************************************************

	u32 elozo=0,ii=0,sn;
	VSUN_Akt_Thread=sn=(n<VSUN_CPU_Thread) ? n : VSUN_CPU_Thread;
	dnodered_0_szalak ** szaltomb=new dnodered_0_szalak*[sn];
	for(u32 i=0;i<sn;i++)szaltomb[i]=new dnodered_0_szalak(i*n/sn,(i+1)*n/sn,halo);
	parhuzamosanfut=true;
	for(u32 i=0;i<sn;i++)szaltomb[i]->start();
	for(u32 i=0;i<sn;i++)szaltomb[i]->wait();
	parhuzamosanfut=false;
	for(u32 i=0;i<sn;i++)delete szaltomb[i];
	delete [] szaltomb;

/*	for(u32 i=0;i<n;i++){
//		if(elozo!=(i*100)/n){
//			elozo=(i*100)/n;
//			status_update_V((PLString(" ")+(elozo)+PLString("%")).c_str());
//		}
//	getchar();
		if(halo[i]!=0)halo[i]->nodered();
	}
*/
//	status_update_V("100%");
/*
	szalparam t1={0,n/2,this},t2={n/2,n,this};
	HANDLE hThread1,hThread2;
	unsigned threadID1,threadID2;
	hThread1=(HANDLE)_beginthreadex(NULL,32768,&noderedThread,&t1,0,&threadID1);
	hThread2=(HANDLE)_beginthreadex(NULL,32768,&noderedThread,&t2,0,&threadID2);
	WaitForSingleObject(hThread2,INFINITE);
	WaitForSingleObject(hThread1,INFINITE);
	CloseHandle(hThread1);
	CloseHandle(hThread2);
*/
}


//***********************************************************************
void drsubstep::nodered(drsubstep & src,const irany I,const bool usetmp){
// A  megadott ir�nyban �sszevonja p�ros�val a cell�kat
// Ha usetmp==true, akkor tmp-be menti a m�trixokat
//***********************************************************************
//	status_update_V("  0%");
	free();// kor�bbi v�ltozat t�rl�se

	ir=I;// �sszevon�si ir�ny

	// �j m�retek sz�m�t�sa

	switch(I)
	{
		case X_IRANY:
		{
#ifdef vsundebugmode
			if(src.x_size<=1)throw hiba("","program error -> substep::nodered -> X_IRANY reduction -> src.x_size<=1 (%u<=1)",src.x_size);
#endif
			x_size=(src.x_size+1)/2;
			y_size=src.y_size;
			z_size=src.z_size;
		}
		break;
		case Y_IRANY:
		{
#ifdef vsundebugmode
			if(src.y_size<=1)throw hiba("","program error -> substep::nodered -> Y_IRANY reduction -> src.y_size<=1 (%u<=1)",src.y_size);
#endif
			x_size=src.x_size;
			y_size=(src.y_size+1)/2;
			z_size=src.z_size;
		}
		break;
		case Z_IRANY:
		{
#ifdef vsundebugmode
			if(src.z_size<=1)throw hiba("","program error -> substep::nodered -> Z_IRANY reduction -> src.z_size<=1 (%u<=1)",src.z_size);
#endif
			x_size=src.x_size;
			y_size=src.y_size;
			z_size=(src.z_size+1)/2;
		}
		break;
		default:throw hiba("","program error -> substep::nodered -> unknown irany");
	}
	n=x_size*y_size*z_size;
	if(n==0)throw hiba("","program error -> substep::nodered -> n==0");

	// pointert�mb lefoglal�sa

//	status_update_V("  5%");
	halo=new drcella * [n];
	if(halo==0)throw hiba("","substep::nodered -> alloc failed");
	drcella **pdest=halo;
	for(u32 j=n;j!=0;j--)*(pdest++)=0;// pointerek null�z�sa
	
	//cell�k l�trehoz�sa az elozo substep alapj�n
	
	pdest=halo;
	drcella **psrc1=src.halo;
	switch(I){
		case X_IRANY:{
			if((src.x_size&1)==0){
				for(u32 i1=0;i1<n;i1++,pdest++,psrc1+=2){//p�ros cellasz�m x ir�nyban
					if(psrc1[0]!=0||psrc1[1]!=0)*pdest=new drcella(psrc1[0],psrc1[1],EAST);
				}
			}
			else for(u32 i1=n/x_size;i1!=0;i1--,pdest++,psrc1++){//p�ratlan cellasz�m x ir�nyban, itt pdest++,psrc1++ a lenti if miatt szerepel
				for(u32 i2=x_size-1;i2!=0;i2--,pdest++,psrc1+=2){if(psrc1[0]!=0||psrc1[1]!=0)
					*pdest=new drcella(psrc1[0],psrc1[1],EAST);}
				if(psrc1[0]!=0)if((*pdest=new drcella(psrc1[0],0,EAST))==0)throw hiba("","substep::nodered -> alloc failed");//A sor utols� eleme egyed�l van
			}
		}
		break;
		case Y_IRANY:{
			drcella **psrc2=src.halo+x_size;//p�ratlan sorok
			if((src.y_size&1)==0){
				for(u32 i1=n/x_size;i1!=0;i1--,psrc1+=x_size,psrc2+=x_size)//p�ros cellasz�m y ir�nyban
					for(u32 i2=x_size;i2!=0;i2--,pdest++,psrc1++,psrc2++)if(*psrc1!=0||*psrc2!=0)
						if((*pdest=new drcella(*psrc1,*psrc2,NORTH))==0)throw hiba("","substep::nodered -> alloc failed");
			}
			else for(u32 i1=z_size;i1!=0;i1--){//p�ratlan cellasz�m y ir�nyban				
				for(u32 i2=y_size-1;i2!=0;i2--,psrc1+=x_size,psrc2+=x_size)
					for(u32 i3=x_size;i3!=0;i3--,pdest++,psrc1++,psrc2++)if(*psrc1!=0||*psrc2!=0)
						if((*pdest=new drcella(*psrc1,*psrc2,NORTH))==0)throw hiba("","substep::nodered -> alloc failed");
				for(u32 i3=x_size;i3!=0;i3--,pdest++,psrc1++,psrc2++)if(*psrc1!=0)//Az utols� sort �tm�solja
					if((*pdest=new drcella(*psrc1,0,NORTH))==0)throw hiba("","substep::nodered -> alloc failed");
			}
		}
		break;
		case Z_IRANY:{
			cu32 lap=x_size*y_size;
			drcella **psrc2=src.halo+lap;//p�ratlan s�kok
			for(u32 i1=src.z_size/2;i1!=0;i1--,psrc1+=lap,psrc2+=lap)
				for(u32 i2=lap;i2!=0;i2--,pdest++,psrc1++,psrc2++)if(*psrc1!=0||*psrc2!=0)
					if((*pdest=new drcella(*psrc1,*psrc2,TOP))==0)throw hiba("","substep::nodered -> alloc failed");
			if((src.z_size&1)!=0)//p�ratlan cellasz�m z ir�nyban
				for(u32 i2=lap;i2!=0;i2--,pdest++,psrc1++)if(*psrc1!=0)
					if((*pdest=new drcella(*psrc1,0,TOP))==0)throw hiba("","substep::nodered -> alloc failed");
		}
	}
	
	// mem�riafoglal�s a m�trixoknak �s vektoroknak

//	status_update_V("  7%");
    AllocV();//El�sz�r a vektorokat (ezeket k�s�bb nem szabad�tjuk fel)
	AllocM();//A seg�dm�trixokat (ezeket esetleg tmp f�jlba mentj�k)
	AllocY();//Az y m�trixot (ezeket t�r�lni fogjuk)

	// csom�pontredukci�

//	status_update_V(" 10%");

//ResetRunTimeClock();
	u32 elozo=0,ii=n/10,sn;
	VSUN_Akt_Thread=sn=(n<VSUN_CPU_Thread) ? n : VSUN_CPU_Thread;
	dnodered_0_szalak ** szaltomb=new dnodered_0_szalak*[sn];
	for(u32 i=0;i<sn;i++)szaltomb[i]=new dnodered_0_szalak(i*n/sn,(i+1)*n/sn,halo);
	parhuzamosanfut=true;
	for(u32 i=0;i<sn;i++)szaltomb[i]->start();
	for(u32 i=0;i<sn;i++)szaltomb[i]->wait();
	parhuzamosanfut=false;
	for(u32 i=0;i<sn;i++)delete szaltomb[i];
	delete [] szaltomb;
//printTime("red stop");
/*
	u32 elozo=0;
	for(pdest=halo,i4=0;i4<n;i4++){
		if(elozo!=(i4*80)/n){
			elozo=(i4*80)/n;
			status_update_V((PLString(" ")+(10+elozo)+PLString("%")).c_str());
		}
		if(pdest[i4]!=0)pdest[i4]->nodered();
	}
*/	
	// elozo y m�trixok felszabad�t�sa

//	status_update_V(" 90%");
	src.freeY();
	if(usetmp){
//		status_update_V(" 92%");
		src.Store();
	}
            //printf("\n****************************************************\n");
            //(**halo).get_d()->y.print();
            //getchar();
            //printf("\n****************************************************\n");
//	status_update_V("100%");
}


//***********************************************************************
void drsubstep::lastReduce(drsubstep & src,const irany I,const bool usetmp){
// Az utols� l�p�sben kell, hogy legyen cella mindk�t t�rf�lben
//***********************************************************************
	if(src.n!=2)throw hiba("","program error -> drsubstep::laststep -> src.n!=2 (n==%u)",src.n);
    if(src.halo[0]==NULL || src.halo[1]==NULL)throw hiba("drsubstep::lastReduce","Internal boundary fills the left or the right half of the fimulated field. Both side must have at least one material filled cell.");
	if(src.halo[0]->externals()==0&&src.halo[1]->externals()==0){
		noExt=true;
		Oldal o;
		if(I==X_IRANY)o=EAST;else if(I==Y_IRANY)o=NORTH;else o=TOP;
#ifdef vsundebugmode
		cu32 ciklus=src.halo[0]->getextended()?EXTENDED_SIDES:BASIC_SIDES;
		for(u32 iii=1;iii<ciklus;iii++)if((iii!=o&&src.halo[0]->getoldalnodeszam(iii)!=0)||((iii+1)!=o&&src.halo[1]->getoldalnodeszam(iii)!=0))
			throw hiba("","program error -> drsubstep::lastReduce -> nonreduced sidenode");
#endif
		free();// kor�bbi v�ltozat t�rl�se
		ir=I;// �sszevon�si ir�ny
		x_size=y_size=z_size=n=1;
		if((halo=new drcella*[1])==0)throw hiba("","substep::lastReduce -> alloc failed");
		if((*halo=new drcella(src.halo[0],src.halo[1],o))==0)throw hiba("","substep::lastReduce -> alloc failed");
		(*halo)->allocLast();
		parhuzamosanfut=true;
		(*halo)->reduceLast();
		parhuzamosanfut=false;
		src.freeY();
		if(usetmp){
			src.Store();
			Store();
		}
	}
	else{
		noExt=false;//de van external
		nodered(src,I,usetmp);
        (**halo).extern_y_reduce();
        (**halo).extern_j_reduce();
                //(**halo).get_d()->u.zero(-20.0);
                //for(uns i=0; i<(**halo).get_d()->external_azon.size(); i++)
                //    (**halo).get_d()->u.set(i,(**halo).get_d()->external_azon[i]);
                //(**halo).get_d()->u.print();
                //(**halo).get_d()->ub.print();
                //printf("\next db = %u\n",(**halo).get_d()->external_azon.size());
                //for(uns i=0; i<(**halo).get_d()->external_azon.size(); i++)
                //    printf("%u ",(**halo).get_d()->external_azon[i]);
                //printf("\n");
	}
}


//***********************************************************************
class dforwsubs_0_szalak:public PLThread{
//***********************************************************************
	drcella ** halo;
	cu32 kezd,veg;
public:
	dforwsubs_0_szalak(cu32 k,cu32 v,drcella ** hal):kezd(k),veg(v),halo(hal){}
	void run(){
		for(u32 i=kezd;i<veg;i++)
			if(halo[i]!=0)halo[i]->forwsubs();

		pthread_mutex_lock(&Substep_Mutex);
		if(VSUN_Akt_Thread)VSUN_Akt_Thread--;
		pthread_mutex_unlock(&Substep_Mutex);
	}
};


//***********************************************************************
void drsubstep::forwsubs(drsubstep * src){
// egyszer sem fut, mert a cella nodered megcsin�lja
//***********************************************************************
	drcella **p=halo;
	if(mentett)Load();

	u32 elozo=0,ii=n/10,sn;
	VSUN_Akt_Thread=sn=(n<VSUN_CPU_Thread) ? n : VSUN_CPU_Thread;
	dforwsubs_0_szalak ** szaltomb=new dforwsubs_0_szalak*[sn];
	for(u32 i=0;i<sn;i++)szaltomb[i]=new dforwsubs_0_szalak(i*n/sn,(i+1)*n/sn,halo);
	parhuzamosanfut=true;
	for(u32 i=0;i<sn;i++)szaltomb[i]->start();
	for(u32 i=0;i<sn;i++)szaltomb[i]->wait();
	parhuzamosanfut=false;
	for(u32 i=0;i<sn;i++)delete szaltomb[i];
	delete [] szaltomb;

//	for(u32 i=0;i<n;i++,p++)if(*p!=0)(*p)->forwsubs();
	if(mentett&&src)src->FreeM();
}


//***********************************************************************
class dbacksubs_0_szalak:public PLThread{
//***********************************************************************
	drcella ** halo;
	cu32 kezd,veg;
public:
	dbacksubs_0_szalak(cu32 k,cu32 v,drcella ** hal):kezd(k),veg(v),halo(hal){}
	void run(){
		for(u32 i=kezd;i<veg;i++)
			if(halo[i]!=0)halo[i]->backsubs();

		pthread_mutex_lock(&Substep_Mutex);
		if(VSUN_Akt_Thread)VSUN_Akt_Thread--;
		pthread_mutex_unlock(&Substep_Mutex);
	}
};


//***********************************************************************
void drsubstep::backsubs(drsubstep * src){
//***********************************************************************
	drcella **p=halo;
	if(mentett&&src)src->Load();

	u32 elozo=0,ii=n/10,sn;
	VSUN_Akt_Thread=sn=(n<VSUN_CPU_Thread) ? n : VSUN_CPU_Thread;
	dbacksubs_0_szalak ** szaltomb=new dbacksubs_0_szalak*[sn];
	for(u32 i=0;i<sn;i++)szaltomb[i]=new dbacksubs_0_szalak(i*n/sn,(i+1)*n/sn,halo);
	parhuzamosanfut=true;
	for(u32 i=0;i<sn;i++)szaltomb[i]->start();
	for(u32 i=0;i<sn;i++)szaltomb[i]->wait();
	parhuzamosanfut=false;
	for(u32 i=0;i<sn;i++)delete szaltomb[i];
	delete [] szaltomb;

//	for(u32 i=0;i<n;i++,p++)if(*p!=0)(*p)->backsubs();
	if(mentett&&src)FreeM();//a 0. szint�t nem t�rli
	if(mentett&&!src)SaveV();
}


//***********************************************************************
void drsubstep::Save(){
//***********************************************************************
	if(TempFajlNeve!="")remove(TempFajlNeve.c_str());	
	if(VTempFajlNeve!="")remove(VTempFajlNeve.c_str());
	TempFajlNeve=GetTmpName();							
	VTempFajlNeve=GetTmpName();
	FILE *fp=fopen(TempFajlNeve.c_str(),"wb");
	if(fp==0)throw hiba("","substep::Save -> cannot open file to write (%s)",TempFajlNeve.c_str());
	FILE *fpv=fopen(VTempFajlNeve.c_str(),"wb");
	if(fpv==0)throw hiba("","substep::Save -> cannot open file to write (%s)",VTempFajlNeve.c_str());
	for(u32 i=0;i<n;i++){halo[i]->save(fp);halo[i]->save_v(fpv);}
	fclose(fp);
	fclose(fpv);
	mentett=true;
}


//***********************************************************************
void drsubstep::SaveV(){
//***********************************************************************
	if(VTempFajlNeve!="")remove(VTempFajlNeve.c_str());
	VTempFajlNeve=GetTmpName();
	FILE *fpv=fopen(VTempFajlNeve.c_str(),"wb");
	if(fpv==0)throw hiba("","substep::Save -> cannot open file to write (%s)",VTempFajlNeve.c_str());
	for(u32 i=0;i<n;i++)halo[i]->save_v(fpv);
	fclose(fpv);
	mentett=true;
}


//***********************************************************************
void drsubstep::Store(){
//***********************************************************************
	if(TempFajlNeve!="")remove(TempFajlNeve.c_str());
	if(VTempFajlNeve!="")remove(VTempFajlNeve.c_str());
	TempFajlNeve=GetTmpName();
	VTempFajlNeve=GetTmpName();
	FILE *fp=fopen(TempFajlNeve.c_str(),"wb");
	if(fp==0)throw hiba("","substep::Store -> cannot open file to write (%s)",TempFajlNeve.c_str());
	FILE *fpv=fopen(VTempFajlNeve.c_str(),"wb");
	if(fpv==0)throw hiba("","substep::Store -> cannot open file to write (%s)",VTempFajlNeve.c_str());
	for(u32 i=0;i<n;i++)halo[i]->store(fp,fpv);
	fclose(fp);
	fclose(fpv);
	mentett=true;
}


//***********************************************************************
void drsubstep::Load(){
//***********************************************************************
	if(TempFajlNeve=="")throw hiba("","program error -> substep::Load -> TempFajlNeve==0");
	if(VTempFajlNeve=="")throw hiba("","program error -> substep::Load -> VTempFajlNeve==0");
	FILE *fp=fopen(TempFajlNeve.c_str(),"rb");
	if(fp==0)throw hiba("","substep::Load -> cannot open file to read (%s)",TempFajlNeve.c_str());
	FILE *fpv=fopen(VTempFajlNeve.c_str(),"rb");
	if(fpv==0)throw hiba("","substep::Load -> cannot open file to read (%s)",VTempFajlNeve.c_str());
	for(u32 i=0;i<n;i++)halo[i]->load(fp,fpv);
	fclose(fp);
	fclose(fpv);
}


//######################################################################


//***********************************************************************
drhalozat::drhalozat(cu32 xsize,cu32 ysize,cu32 zsize)
//***********************************************************************
{
	u32 i;

	// kezd�m�ret kisz�m�t�sa

	x_size=xsize;
	y_size=ysize;
	z_size=zsize;
	n=x_size*y_size*z_size;

	// a norm�l l�p�sek sz�m�nak kisz�m�t�sa
	
	for(x_step=0,i=x_size;i>1;i=(i+1)/2,x_step++);
	for(y_step=0,i=y_size;i>1;i=(i+1)/2,y_step++);
	for(z_step=0,i=z_size;i>1;i=(i+1)/2,z_step++);
	n_step=x_step+y_step+z_step+1;//A 0. step a kiindul� h�l�zat

	// a norm�l l�p�seket t�rol� objektumt�mb lefoglal�sa

	lepesek=new drsubstep*[n_step];
	if(lepesek==0)throw hiba("halozat::drhalozat","alloc failed");
	for(i=0;i<n_step;i++)lepesek[i]=NULL;

	// a 0. h�l�zat lefoglal�sa

	lepesek[0]=new drsubstep(x_size,y_size,z_size);
	if(!lepesek[0])throw hiba("drhalozat::drhalozat","alloc failed");
}


//***********************************************************************
void drhalozat::nodered(const bool EnableLastStep,const bool usetmp){
// Ha usetmp==true, akkor tmp f�jlba menti a m�trixokat
//***********************************************************************
//printTime("nodered start");
	if(lepesek[0]==0)throw hiba("","program error -> drhalozat::nodered -> missing lepesek[0] step");
  resetAnalLevel(0);
  setAnalStepDb(0,n_step);
  setAnalKeszStep(0,0,"nodered");
//	status_update_IV((PLString("step 1 of ")+n_step).c_str());
	lepesek[0]->nodered();
  setAnalKeszStep(0,1);
	cu32 steps=n_step-1;//a 0. l�p�s l�trhoz�sa nem a ciklusban t�rt�nik.
	u32 step=0;
//printTime("\nnodered 0","","",1);
	while(true){
		if(lepesek[step]->getx()>1){
			++step;
//			status_update_IV((PLString("step ")+(step+1)+PLString(" of ")+n_step).c_str());
			if(lepesek[step]!=0){delete lepesek[step];lepesek[step]=0;}
			lepesek[step]=new drsubstep;
			if(lepesek[step]==0)throw hiba("","halozat::nodered -> alloc failed");
			//redukci�
			if(step<steps)lepesek[step]->nodered(*(lepesek[step-1]),X_IRANY,usetmp);
			else{
				if(EnableLastStep)lepesek[step]->lastReduce(*lepesek[step-1],X_IRANY,usetmp);
				else lepesek[step]->nodered(*lepesek[step-1],X_IRANY,usetmp);
				break;
			}
  setAnalKeszStep(0,step+1);
//printTime("\nnodered X","","",1);
		}
		if(lepesek[step]->gety()>1){
			++step;
//			status_update_IV((PLString("step ")+(step+1)+PLString(" of ")+n_step).c_str());
			if(lepesek[step]!=0){delete lepesek[step];lepesek[step]=0;}
			lepesek[step]=new drsubstep;
			if(lepesek[step]==0)throw hiba("","halozat::nodered -> alloc failed");
			//redukci�
			if(step<steps)lepesek[step]->nodered(*lepesek[step-1],Y_IRANY,usetmp);
			else{
				if(EnableLastStep)lepesek[step]->lastReduce(*lepesek[step-1],Y_IRANY,usetmp);
				else lepesek[step]->nodered(*lepesek[step-1],Y_IRANY,usetmp);
				break;
			}
  setAnalKeszStep(0,step+1);
//printTime("\nnodered Y","","",1);
		}
		if(lepesek[step]->getz()>1){
			++step;
//			status_update_IV((PLString("step ")+(step+1)+PLString(" of ")+n_step).c_str());
			if(lepesek[step]!=0){delete lepesek[step];lepesek[step]=0;}
			lepesek[step]=new drsubstep;
			if(lepesek[step]==0)throw hiba("","halozat::nodered -> alloc failed");
			//redukci�
			if(step<steps)lepesek[step]->nodered(*lepesek[step-1],Z_IRANY,usetmp);
			else{
				if(EnableLastStep)lepesek[step]->lastReduce(*lepesek[step-1],Z_IRANY,usetmp);
				else lepesek[step]->nodered(*lepesek[step-1],Z_IRANY,usetmp);
				break;
			}
  setAnalKeszStep(0,step+1);
//printTime("\nnodered Z","","",1);
		}
		if(lepesek[step]->getx()<=1&&lepesek[step]->gety()<=1&&lepesek[step]->getz()<=1)
			throw hiba("","program error -> drhalozat::nodered -> vegtelen ciklus");
	}
  setAnalKeszStep(0,step+1);
}


//***********************************************************************
#endif
