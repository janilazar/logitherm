//***********************************************************************
// Vector SUNRED 2 matrix class header
// Creation date:	2008. 03. 14.
// Creator:			Pohl L�szl�
//***********************************************************************


//***********************************************************************
#ifndef VSUN2_DMATRIX_HEADER
#define	VSUN2_DMATRIX_HEADER
//***********************************************************************


//***********************************************************************
#include "tipusok.h"
#include "gfunc.h"
//***********************************************************************

//***********************************************************************
class ddinmem{ // NEM FIBERSAFE!!!
//***********************************************************************
	dbl * t[V_DIN_MAX_BLOKK]; // itt vannak a t�nyleges adatok
	u32 e[V_DIN_MAX_BLOKK]; // a blokkon h�ny eleme van bet�ltve
	u32 g[V_DIN_MAX_BLOKK]; // a blokkban most h�ny m�trix van (free-n�l cs�kken)
	u32 n,akt; // aktu�lis blokk
	void init(cu32 i){t[i]=new dbl[EGYMEGA];e[i]=0;g[i]=0;}
	void __free(cu32 i){delete[]t[i];t[i]=NULL;e[i]=0;}
	//***********************************************************************
	void setakt(cu32 db){
	//***********************************************************************
		for(;akt<n&&db+e[akt]>EGYMEGA;akt++);
		if(akt>=n)for(akt=0;akt<n&&db+e[akt]>EGYMEGA;akt++);
		if(akt>=n){
			n=akt+1;
			init(akt);
		}
		else if(t[akt]==NULL)init(akt);
	}
public:
	//***********************************************************************
	ddinmem():n(1),akt(0){init(0);}
	~ddinmem(){for(u32 i=0;i<n;i++)__free(i);}
	//***********************************************************************
	dbl * alloc(cu32 db,u32 & azonosito){
	//***********************************************************************
#ifdef vsundebugmode
		if(db>EGYMEGA)throw hiba("","ddinmem::alloc => db>EGYMEGA => %lu>%lu",db,EGYMEGA);
		if(n>=V_DIN_MAX_BLOKK-1)throw hiba("ddinmem::alloc","n>=V_DIN_MAX_BLOKK-1 => %lu>%lu",n,V_DIN_MAX_BLOKK-1);
#endif
		if(db==0){azonosito=V_DIN_MAX_BLOKK;return NULL;} // ilyenkor az azonos�t�t nem �ll�tjuk!!!
		if(db+e[akt]>EGYMEGA)setakt(db);// �res helyet keres�nk
		azonosito=akt;
		g[akt]++;
		cu32 temp=e[akt];
		e[akt]+=db;
		return t[akt]+temp;
	}
	//***********************************************************************
	void free(cu32 i){
	// az azonos�t� �ltal jelzett blokkban szabad�tunk fel
	// nem ellen�rzi, hogy t�bbsz�r szabad�tunk-e fel
	//***********************************************************************
		if(i==V_DIN_MAX_BLOKK)return;// NULL pointer volt
		if(!--g[i]){
			e[i]=0;
			if(i>0&&akt!=i){__free(i);akt=0;}
		}
	}
	//***********************************************************************
	void print(){
	//***********************************************************************
		printf("\nn=%lu\n",n);
		for(u32 i=0;i<n;i++)printf("blokk[%02lu]=%lu\n",i,g[i]);
	}
};

extern ddinmem * dvsun_memoria;
extern bool parhuzamosanfut;
extern uns db_mind,db_new;
//***********************************************************************
class dmBase{
//***********************************************************************
private:
	//***********************************************************************
	dmBase(const dmBase & src);
	//***********************************************************************
	void operator=(const dmBase & src);
	//***********************************************************************
protected:
	//***********************************************************************
	dbl * t;		// A t�mb
	u32 siz;		// A t�nyleges m�ret, row*col*dim*blokk
	u32 row,col;	// sor, oszlop
	u32 azonosito;  // dinmemhez
	//***********************************************************************
	void resize(cu32 newsiz){
	//***********************************************************************
		if(siz==newsiz)return;
        if(newsiz<siz&&siz<=8){siz=newsiz;return;}
		siz=newsiz;//db_mind++;
		if(azonosito==V_DIN_MAX_BLOKK+1)delete[]t;
		else dvsun_memoria->free(azonosito);
		if(parhuzamosanfut||newsiz>1024){
			t=new dbl[newsiz];//db_new++;
			azonosito=V_DIN_MAX_BLOKK+1;
		}
        else t=dvsun_memoria->alloc(newsiz,azonosito);
        //zero();
	}
	//***********************************************************************
	void set(cu32 n,cd v){
	//***********************************************************************
#ifdef vsundebugmode
		if(n>=siz)throw hiba("","dmBase::set => n>siz => %lu!=%lu",n,siz);
#endif
		t[n]=v;
	}
	//***********************************************************************
	dbl get(cu32 n)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(n>=siz)throw hiba("","dmBase::get => n>siz => %lu!=%lu",n,siz);
#endif
		return t[n];
	}
	//***********************************************************************
	virtual dbl get(cu32 r,cu32 c)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(r>=siz)throw hiba("","dmBase::get => r>siz => %lu!=%lu",r,siz);
#endif
		return t[r];
	}
public:
	//***********************************************************************
	void inc(cu32 n,cd v){
	//***********************************************************************
#ifdef vsundebugmode
		if(n>=siz)throw hiba("","dmBase::inc => n>siz => %lu!=%lu",n,siz);
#endif
		t[n]+=v;
	}
	//***********************************************************************
	dmBase():t(NULL),siz(0),row(0),col(0),azonosito(V_DIN_MAX_BLOKK+1){}
	//***********************************************************************
	~dmBase(){free();}
	//***********************************************************************
	void free(){
	//***********************************************************************
		if(azonosito==V_DIN_MAX_BLOKK+1)delete[]t;
		else {if(dvsun_memoria)dvsun_memoria->free(azonosito);azonosito=V_DIN_MAX_BLOKK+1;}
		t=NULL;siz=col=row=0;
	}
	//***********************************************************************
	virtual void resize(cu32 r,cu32 c)=0;
	//***********************************************************************
	u32 getsiz()const{return siz;}
	u32 getrow()const{return row;}
	u32 getcol()const{return col;}
	u32 getMemUsage(){return siz+sizeof(*this);}
	dbl * gett(){return t;}
	cd * gett()const{return t;}
	//***********************************************************************

	//***********************************************************************
	void copy(const dmBase & src){
	//***********************************************************************
		if(&src==this)return;
		resize(src.row,src.col);
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src.t[i];
	}

	//***********************************************************************
	void swap(dmBase & src){
	//***********************************************************************
		if(&src==this)return;
		dbl*vt=src.t;src.t=t;t=vt;
		u32 ut=src.siz;src.siz=siz;siz=ut;
		ut=src.row;src.row=row;row=ut;
		ut=src.col;src.col=col;col=ut;
	}
	//***********************************************************************
	void print(bool matrixkent=false)const{
	//***********************************************************************
        printf("siz=%u\n",siz);
		if(!matrixkent)for(u32 i=0;i<siz;i++)printf("%10g\t",double(t[i]));
        else for(uns i=0; i<row; i++,printf("\n"))for(uns j=0;j<col;j++)printf("%10g\t",double(get(i,j)));
		printf("\n");
	}
	//***********************************************************************
	void zero(cd value=0.0){
	//***********************************************************************
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=value;
	}
	//***********************************************************************
	bool is_zero(){
	//***********************************************************************
		cu32 n=siz;
        bool res=true;
		for(u32 i=0;i<n;i++)res = res && t[i]==0;
        return res;
	}
	//***********************************************************************
	void neg(){
	//***********************************************************************
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=-t[i];
	}
	//***********************************************************************
	void random(){
	//***********************************************************************
		cu32 n=siz;
//		srand((unsigned)time(NULL));
		for(u32 i=0;i<n;i++)t[i]=rand()+2;
	}
	//***********************************************************************
	void novekvo(){
	//***********************************************************************
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=i;
	}
	//***********************************************************************
	void add(const dmBase & src1,const dmBase & src2){
	//***********************************************************************
#ifdef vsundebugmode
		if(this==&src1||this==&src2)throw hiba("","dmBase::add => this==&src1||this==&src2");
		if(src1.siz!=src2.siz)throw hiba("","dmBase::add => src1.siz!=src2.siz => %lu!=%lu",src1.siz,src2.siz);
		if(src1.col!=src2.col)throw hiba("","dmBase::add => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
		if(src1.row!=src2.row)throw hiba("","dmBase::add => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
#endif
		resize(src1.row,src1.col);
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]+src2.t[i];
	}
	//***********************************************************************
	void addnr(const dmBase & src1,const dmBase & src2){
	// nem m�retezi �t, lehet azonos a forr�s �s a c�l
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=src2.siz)throw hiba("","dmBase::add => src1.siz!=src2.siz => %lu!=%lu",src1.siz,src2.siz);
		if(src1.col!=src2.col)throw hiba("","dmBase::add => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
		if(src1.row!=src2.row)throw hiba("","dmBase::add => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
		if(col!=src2.col)throw hiba("","dmBase::addnr => col!=src2.col => %lu!=%lu",col,src2.col);
		if(row!=src2.row)throw hiba("","dmBase::addnr => row!=src2.row => %lu!=%lu",row,src2.row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]+src2.t[i];
	}
	//***********************************************************************
	void pluszegyenlo(const dmBase & src1){
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=siz)throw hiba("","dmBase::pluszegyenlo => src1.siz!=siz => %lu!=%lu",src1.siz,siz);
		if(src1.col!=col)throw hiba("","dmBase::pluszegyenlo => src1.col!=col => %lu!=%lu",src1.col,col);
		if(src1.row!=row)throw hiba("","dmBase::pluszegyenlo => src1.row!=row => %lu!=%lu",src1.row,row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]+=src1.t[i];
	}
	//***********************************************************************
	void sub(const dmBase & src1,const dmBase & src2){
	//***********************************************************************
#ifdef vsundebugmode
		if(&src1==this||&src2==this)throw hiba("","dmBase::sub => &src1==this||&src2==this");
		if(src1.siz!=src2.siz)throw hiba("","dmBase::sub => src1.siz!=src2.siz => %lu!=%lu",src1.siz,src2.siz);
		if(src1.col!=src2.col)throw hiba("","dmBase::sub => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
		if(src1.row!=src2.row)throw hiba("","dmBase::sub => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
#endif
		resize(src1.row,src1.col);
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]-src2.t[i];
	}
	//***********************************************************************
	void subnr(const dmBase & src1,const dmBase & src2){
	// nem m�retezi �t, lehet azonos a forr�s �s a c�l
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=src2.siz)throw hiba("","dmBase::subnr => src1.siz!=src2.siz => %lu!=%lu",src1.siz,src2.siz);
		if(src1.col!=src2.col)throw hiba("","dmBase::subnr => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
		if(src1.row!=src2.row)throw hiba("","dmBase::subnr => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
		if(col!=src2.col)throw hiba("","dmBase::subnr => col!=src2.col => %lu!=%lu",col,src2.col);
		if(row!=src2.row)throw hiba("","dmBase::subnr => row!=src2.row => %lu!=%lu",row,src2.row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]-src2.t[i];
	}
	//***********************************************************************
	void addsubnr(const dmBase & src1,const dmBase & src2,const dmBase & src3){
	// nem m�retezi �t, lehet azonos a forr�s �s a c�l
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=src2.siz)throw hiba("","dmBase::subnr => src1.siz!=src2.siz => %lu!=%lu",src1.siz,src2.siz);
		if(src1.col!=src2.col)throw hiba("","dmBase::subnr => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
		if(src1.row!=src2.row)throw hiba("","dmBase::subnr => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
		if(src1.siz!=src3.siz)throw hiba("","dmBase::subnr => src1.siz!=src3.siz => %lu!=%lu",src1.siz,src3.siz);
		if(src1.col!=src3.col)throw hiba("","dmBase::subnr => src1.col!=src3.col => %lu!=%lu",src1.col,src3.col);
		if(src1.row!=src3.row)throw hiba("","dmBase::subnr => src1.row!=src3.row => %lu!=%lu",src1.row,src3.row);
		if(col!=src2.col)throw hiba("","dmBase::subnr => col!=src2.col => %lu!=%lu",col,src2.col);
		if(row!=src2.row)throw hiba("","dmBase::subnr => row!=src2.row => %lu!=%lu",row,src2.row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]+src2.t[i]-src3.t[i];
	}
	//***********************************************************************
	void minuszegyenlo(const dmBase & src1){
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=siz)throw hiba("","dmBase::minuszegyenlo => src1.siz!=siz => %lu!=%lu",src1.siz,siz);
		if(src1.col!=col)throw hiba("","dmBase::minuszegyenlo => src1.col!=col => %lu!=%lu",src1.col,col);
		if(src1.row!=row)throw hiba("","dmBase::minuszegyenlo => src1.row!=row => %lu!=%lu",src1.row,row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]-=src1.t[i];
	}
	//***********************************************************************
	void addaddsubadd(const dmBase & src1,const dmBase & src2,const dmBase & src3,const dmBase & src4){
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=src2.siz||src2.siz!=src3.siz||src3.siz!=src4.siz)
			throw hiba("","dmBase::addaddsubadd => src1.siz!=src2.siz!=src3.siz!=src4.siz => %lu!=%lu!=%lu!=%lu",src1.siz,src2.siz,src3.siz,src4.siz);
		if(src1.col!=src2.col||src2.col!=src3.col||src3.col!=src4.col)
			throw hiba("","dmBase::addaddsubadd => src1.col!=src2.col!=src3.col!=src4.col => %lu!=%lu!=%lu!=%lu",src1.col,src2.col,src3.col,src4.col);
		if(src1.row!=src2.row||src2.row!=src3.row||src3.row!=src4.row)
			throw hiba("","dmBase::addaddsubadd => src1.row!=src2.row!=src3.row!=src4.row => %lu!=%lu!=%lu!=%lu",src1.row,src2.row,src3.row,src4.row);
#endif
		resize(src1.row,src1.col);
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]+src2.t[i]-src3.t[i]+src4.t[i];
	}
	//***********************************************************************
	void addsubsubnr(const dmBase & src1,const dmBase & src2,const dmBase & src3){
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=src2.siz||src2.siz!=src3.siz||src3.siz!=siz)
			throw hiba("","dmBase::addsumsubnr => src1.siz!=src2.siz!=src3.siz!=siz => %lu!=%lu!=%lu!=%lu",src1.siz,src2.siz,src3.siz,siz);
		if(src1.col!=src2.col||src2.col!=src3.col||src3.col!=col)
			throw hiba("","dmBase::addsumsubnr => src1.col!=src2.col!=src3.col!=col => %lu!=%lu!=%lu!=%lu",src1.col,src2.col,src3.col,col);
		if(src1.row!=src2.row||src2.row!=src3.row||src3.row!=row)
			throw hiba("","dmBase::addsumsubnr => src1.row!=src2.row!=src3.row!=row => %lu!=%lu!=%lu!=%lu",src1.row,src2.row,src3.row,row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]-src2.t[i]-src3.t[i];
	}
	//***********************************************************************
	virtual void save(FILE *fp)const;
	virtual void savetext(const char * FileName)const;
	virtual void load(FILE *fp);
	//***********************************************************************
	void store(FILE *fp){
	// Lemezre menti �s t�rli a mem�ri�b�l
	//***********************************************************************
		save(fp);
		free();
	}
};

//###################################################################################################x
//###################################################################################################x
//###################################################################################################x

//***********************************************************************
class drmatrix;
class dsrmatrix;
class drvector;
//***********************************************************************

//***********************************************************************
class drvector:public dmBase{
//***********************************************************************
	friend class drmatrix;
	friend class dsrmatrix;
public:
	//***********************************************************************
	void resize(cu32 r,cu32 c){
	//***********************************************************************
#ifdef vsundebugmode
		if(r!=1&&c!=1)
			throw hiba("","drvector::resize => r!=1&&c!=1 => %lu,%lu", r,c);
#endif
		resize(r==1?c:r);
	}
	//***********************************************************************
	void resize(cu32 n){
	//***********************************************************************
#ifdef vsundebugmode2
		if(n==0)logprint("dsrmatrix::resize => n==0");
#endif
		row=1;
		col=n;
		dmBase::resize(n);
	}
	//***********************************************************************
	void set(cu32 i,cd v){t[i]=v;}
	dbl get(cu32 i)const{return t[i];}
	void inc(cu32 i,cd v){t[i]+=v;}
	void inc0(cd v){t[0]+=v;}
	//***********************************************************************
	void mul(const drmatrix & src1,const drvector & src2){tmul(src1,src2);}
	void mult(const drmatrix & src1,const drvector & src2){tmult(src1,src2);}
	void mul(const dsrmatrix & src1,const drvector & src2){tmul(src1,src2);}
	//***********************************************************************
	void tmul(const drmatrix & src1,const drvector & src2,const bool adde=false);
	void tmult(const drmatrix & src1,const drvector & src2,const bool adde=false);
	void tmul(const dsrmatrix & src1,const drvector & src2,const bool adde=false);
	void tmuladd(const drmatrix & src1,const drvector & src2);
	void tmuladd(const dsrmatrix & src1,const drvector & src2);
	void getsubvector(const drvector & src,cu32 n0,cu32 n);
	void setsubvector(const drvector & src,cu32 n0);
	void getsetsubvector(const drvector & src,cu32 from_n0,cu32 to_n0,cu32 n);
	void getsetaddsubvector(const drvector & src,cu32 from_n0,cu32 to_n0,cu32 n);
	//***********************************************************************
};


//***********************************************************************
class drmatrix:public dmBase{
//***********************************************************************
	friend class dsrmatrix;
	friend class drvector;
protected:
	//***********************************************************************
	void strasseninv();
	void strassentmul(const drmatrix & src1,const drmatrix & src2);//val�j�ban += !!!
	void strassentmul(const dsrmatrix & src1,const drmatrix & src2);//rBlokksz� konvert�lja//val�j�ban += !!!
	//***********************************************************************
	void getLine(dbl * dest,cu32 sor)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","drmatrix::getLine => sor>row => %lu>%lu",sor,row);
#endif
		cd*p=t+sor*col;
		for(u32 i=0;i<col;i++)dest[i]=p[i];
	}
	//***********************************************************************
	void getLineadd(dbl * dest,cu32 sor)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","drmatrix::getLineadd => sor>row => %lu>%lu",sor,row);
#endif
		cd*p=t+sor*col;
		for(u32 i=0;i<col;i++)dest[i]+=p[i];
	}
	//***********************************************************************
	void getLine(dbl * dest,cu32 sor,cu32 oszl,cu32 db)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","drmatrix::getLine => sor>row => %lu>%lu",sor,row);
		if(oszl+db>col)throw hiba("","drmatrix::getLine => oszl+db>col => %lu+%lu>%lu",oszl,db,col);
#endif
		cd*p=t+sor*col+oszl;
		for(u32 i=0;i<db;i++)dest[i]=p[i];
	}
	//***********************************************************************
	void getLineadd(dbl * dest,cu32 sor,cu32 oszl,cu32 db)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","drmatrix::getLineadd => sor>row => %lu>%lu",sor,row);
		if(oszl+db>col)throw hiba("","drmatrix::getLineadd => oszl+db>col => %lu+%lu>%lu",oszl,db,col);
#endif
		cd*p=t+sor*col+oszl;
		for(u32 i=0;i<db;i++)dest[i]+=p[i];
	}
public:
	//***********************************************************************
	void resize(cu32 r,cu32 c){
	//***********************************************************************
#ifdef vsundebugmode2
		if(r==0||c==0)logprint("drmatrix::resize => r==0||c==0 => %lu,%lu",r,c);
#endif
		row=r;
		col=c;
		dmBase::resize(r*c);
	}

	//***********************************************************************
	void egyseg(){
	//***********************************************************************
		cu32 n=col<row?col:row;
		zero();
		for(u32 i=0;i<n;i++)set(i,i,1);
	}

	//***********************************************************************
	void set(cu32 r,cu32 c,cd v){
	//***********************************************************************
#ifdef vsundebugmode
		if(r>=row)throw hiba("","drmatrix::set => r>=row => %lu!=%lu",r,row);
		if(c>=col)throw hiba("","drmatrix::set => c>=col => %lu!=%lu",c,col);
#endif
		t[r*col+c]=v;
	}

	//***********************************************************************
	dbl get(cu32 r,cu32 c)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(r>=row)throw hiba("","drmatrix::get => r>=row => %lu!=%lu",r,row);
		if(c>=col)throw hiba("","drmatrix::get => c>=col => %lu!=%lu",c,col);
#endif
		return t[r*col+c];
	}

	//***********************************************************************
	void inc(cu32 r,cu32 c,cd v){
	//***********************************************************************
#ifdef vsundebugmode
		if(r>=row)throw hiba("","drmatrix::inc => r>=row => %lu!=%lu",r,row);
		if(c>=col)throw hiba("","drmatrix::inc => c>=col => %lu!=%lu",c,col);
#endif
		t[r*col+c]+=v;
	}

	//***********************************************************************
	void inc00(cd v){t[0]+=v;}
	//***********************************************************************

	//***********************************************************************
	void transp(const drmatrix & src){
	//***********************************************************************
#ifdef vsundebugmode
		if(&src==this)throw hiba("","drmatrix::transp => &src==this");
#endif
		resize(src.col,src.row); // transzpon�l�shoz!
		for(u32 i=0;i<row;i++)for(u32 j=0;j<col;j++)set(i,j,src.get(j,i));
	}

	//***********************************************************************
	void tmul(const drmatrix & src1,const drmatrix & src2,const bool adde=false,u32 i_start=0,u32 i_stop=0){
		tmul_4(src1,src2,adde,i_start,i_stop);
	}
	void tmul_2(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop);
	void tmul_3(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop);
	void tmul_4(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop);
	void tmul_5(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop);
	void tmul_6(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop);
	void tmul_8(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop);
	void tmul(const dsrmatrix & src1,const drmatrix & src2,const bool adde=false,u32 i_start=0,u32 i_stop=0);
	void tmul(const drmatrix & src1,const dsrmatrix & src2,const bool adde=false);
	void tmul(const drmatrix & src1,const dsrmatrix & src2,drmatrix & masolat,const bool adde=false);
	void inv();
	void submatrixadd(const drmatrix & src1,cu32 y1,cu32 x1,const drmatrix & src2,cu32 y2,cu32 x2,cu32 y,cu32 x);
	void submatrixsub(const drmatrix & src1,cu32 y1,cu32 x1,const drmatrix & src2,cu32 y2,cu32 x2,cu32 y,cu32 x);
	void getsubmatrix(const drmatrix & src,cu32 y0,cu32 x0,cu32 y,cu32 x);
	void getsetsubmatrix(const dsrmatrix & src,cu32 y0_from,cu32 x0_from,cu32 y0_to,cu32 x0_to,cu32 y,cu32 x);
	void setsubmatrix(const drmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixadd(const drmatrix & src,cu32 y0,cu32 x0);
	void getsubmatrix(const dsrmatrix & src,cu32 y0,cu32 x0,cu32 y,cu32 x);
	void setsubmatrix(const dsrmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixadd(const dsrmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixneg(const drmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixnegadd(const drmatrix & src,cu32 y0,cu32 x0);
	//***********************************************************************

	//***********************************************************************
	void mul(const drmatrix & src1,const drmatrix & src2){
	//***********************************************************************
#ifdef vsundebugmode
		logprint("drmatrix::mul => nem hatekony");
#endif
		drmatrix r;
		r.transp(src2);
		tmul(src1,r);
	}
	//***********************************************************************
	void convert(const dsrmatrix & src);
	//***********************************************************************

};

//###################################################################################################x
//###################################################################################################x
//###################################################################################################x

//***********************************************************************
class dsrmatrix:public dmBase{
//***********************************************************************
	friend class drmatrix;
	friend class drvector;
	//***********************************************************************
	cu32 getcimunsafe(cu32 r,cu32 c)const{
	// Az r<=c biztos!
	//***********************************************************************
		return r*col+c-(r+1)*r/2;
	}
public:
	//***********************************************************************
	cu32 getcim(cu32 r,cu32 c)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(r>=row)throw hiba("","dsrmatrix::getcim => r>=row => %lu!=%lu",r,row);
		if(c>=col)throw hiba("","dsrmatrix::getcim => c>=col => %lu!=%lu",c,col);
#endif
		return (r<=c)?getcimunsafe(r,c):getcimunsafe(c,r);
	}
	//***********************************************************************
	void getLine(dbl * dest,cu32 sor)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","dsrmatrix::getLine => sor>row => %lu>%lu",sor,row);
#endif
		cd*p=t+sor;
		for(u32 i=0;i<sor;++i,p+=col-i,dest++)*dest=*p;
		for(u32 i=sor;i<col;i++,p++,dest++)*dest=*p;
	}
	//***********************************************************************
	void getLineadd(dbl * dest,cu32 sor)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","dsrmatrix::getLineadd => sor>row => %lu>%lu",sor,row);
#endif
		cd*p=t+sor;
		for(u32 i=0;i<sor;++i,p+=col-i,dest++)*dest+=*p;
		for(u32 i=sor;i<col;i++,p++,dest++)*dest+=*p;
	}
	//***********************************************************************
	void getLine(dbl * dest,cu32 sor,cu32 oszl,cu32 db)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","dsrmatrix::getLine => sor>row => %lu>%lu",sor,row);
		if(oszl+db>col)throw hiba("","dsrmatrix::getLine => oszl+db>col => %lu+%lu>%lu",oszl,db,col);
#endif
		if(oszl<sor){
			cd*p=t+getcimunsafe(oszl,sor);
			u32 n=0;
			for(u32 i=oszl;i<sor&&n<db;++i,p+=col-i,n++)dest[n]=*p;
			for(;n<db;p++,n++)dest[n]=*p;
		}
		else{
			cd*p=t+getcimunsafe(sor,oszl);
			for(u32 i=0;i<db;i++)dest[i]=p[i];
		}
	}
	//***********************************************************************
	void getLineadd(dbl * dest,cu32 sor,cu32 oszl,cu32 db)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","dsrmatrix::getLineadd => sor>row => %lu>%lu",sor,row);
		if(oszl+db>col)throw hiba("","dsrmatrix::getLineadd => oszl+db>col => %lu+%lu>%lu",oszl,db,col);
#endif
		if(oszl<sor){
			cd*p=t+getcimunsafe(oszl,sor);
			u32 n=0;
			for(u32 i=oszl;i<sor&&n<db;++i,p+=col-i,n++)dest[n]+=*p;
			for(;n<db;p++,n++)dest[n]+=*p;
		}
		else{
			cd*p=t+getcimunsafe(sor,oszl);
			for(u32 i=0;i<db;i++)dest[i]+=p[i];
		}
	}
	//***********************************************************************
	void setLine(cd*src,cu32 sor){
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","dsrmatrix::setLine => sor>row => %lu>%lu",sor,row);
#endif
		dbl*p=t+sor;
		for(u32 i=0;i<sor;++i,p+=col-i,src++)*p=*src;
		for(u32 i=sor;i<col;i++,p++,src++)*p=*src;
	}
	//***********************************************************************
	void setLine(cd*src,cu32 sor,cu32 oszl,cu32 db){
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","dsrmatrix::setLine => sor>row => %lu>%lu",sor,row);
		if(oszl+db>col)throw hiba("","dsrmatrix::setLine => oszl+db>col => %lu+%lu>%lu",oszl,db,col);
#endif
		if(oszl<sor){
			dbl*p=t+getcimunsafe(oszl,sor);
			u32 n=0;
			for(u32 i=oszl;i<sor&&n<db;++i,p+=col-i,n++)*p=src[n];
			for(;n<db;p++,n++)*p=src[n];
		}
		else{
			dbl*p=t+getcimunsafe(sor,oszl);
			for(u32 i=0;i<db;i++)p[i]=src[i];
		}
	}
	//***********************************************************************
	void resize(cu32 n){
	//***********************************************************************
#ifdef vsundebugmode2
		if(n==0)
            logprint("dsrmatrix::resize => n==0");
#endif
		row=col=n;
		dmBase::resize(n*(n+1)/2);
	}
	//***********************************************************************
	void resize(cu32 r,cu32 c){
	//***********************************************************************
#ifdef vsundebugmode
		if(r!=c)
			throw hiba("","dsrmatrix::resize => r!=c => %lu!=%lu", r,c);
#endif
		resize(r);
	}
	//***********************************************************************
	void egyseg(){
	//***********************************************************************
		cu32 n=col;
		zero();
		for(u32 i=0;i<n;i++)set(i,i,1);
	}
	//***********************************************************************
	void set(cu32 r,cu32 c,cd v){t[getcim(r,c)]=v;}
	//***********************************************************************
	dbl get(cu32 r,cu32 c)const{return t[getcim(r,c)];}
	//***********************************************************************
	void inc(cu32 r,cu32 c,cd v){t[getcim(r,c)]+=v;}
	//***********************************************************************
	void inc00(cd v){t[0]+=v;}
	//***********************************************************************
	void transp(const dsrmatrix & src){
	//***********************************************************************
#ifdef vsundebugmode
		if(&src==this)throw hiba("","dsrmatrix::transp => &src==this");
		logprint("dsrmatrix::transp => felelsleges muvelet");
#endif
		throw hiba("","dsrmatrix::transp => t�pusfelismer�ssel megcsin�lni!");
		copy(src);
	}
	//***********************************************************************
	void mul(const drmatrix & src1,const drmatrix & src2){
	//***********************************************************************
#ifdef vsundebugmode2
		logprint("dsrmatrix::mul => nem hatekony");
#endif
		drmatrix r;
		r.transp(src2);
		tmul(src1,r);
	}
	//***********************************************************************
	void mul_vigyazz(const dsrmatrix & src1,const dsrmatrix & src2){
	// �ltal�ban k�t symm szorzata NEM symm!!!
	//***********************************************************************
		tmul_vigyazz(src1,src2);
	}
	//***********************************************************************
	void inv(){
	//***********************************************************************
		ninv();
		neg();
	}
	//***********************************************************************
	void tmul(const drmatrix & src1,const drmatrix & src2,const bool adde=false,u32 i_start=0,u32 i_stop=0);
	void tmul_vigyazz(const dsrmatrix & src1,const dsrmatrix & src2,const bool adde=false,u32 i_start=0,u32 i_stop=0);// �ltal�ban k�t symm szorzata NEM symm!!!
	void ninv();
	void getsubmatrix(const dsrmatrix & src,cu32 n0,cu32 n);
	void getsetsubmatrix(const dsrmatrix & src,cu32 n0_from,cu32 n0_to,cu32 n);
	void getsetsubmatrix(const dsrmatrix & src,cu32 y0_from,cu32 x0_from,cu32 y0_to,cu32 x0_to,cu32 y,cu32 x);
	void getsetsubmatrixadd(const dsrmatrix & src,cu32 n0_from,cu32 n0_to,cu32 n);
	void setsubmatrix(const dsrmatrix & src,cu32 n0);
	void setsubmatrixadd(const dsrmatrix & src,cu32 n0);
	void setsubmatrix(const drmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixadd(const drmatrix & src,cu32 y0,cu32 x0);
	//***********************************************************************
protected:
	//***********************************************************************
	void strasseninv();
	void strassentmul(const drmatrix & src1,const drmatrix & src2);//val�j�ba += !!!
	void strassentmul(const dsrmatrix & src1,const dsrmatrix & src2);//val�j�ba += !!!
	//***********************************************************************
};

//***********************************************************************
class drmtmulszal:public PLThread{
//***********************************************************************
	static drmatrix rdummy;
	static dsrmatrix srdummy;
	const drmatrix &s1,&s2;
	const dsrmatrix &s3,&s4;
	drmatrix &d;
	dsrmatrix &d2;
	cu32 mode,i_start,i_stop;
public:
	drmtmulszal(const drmatrix &src1,const drmatrix &src2,drmatrix &dest,u32 i_start=0,u32 i_stop=0)
        :s1(src1),s2(src2),d(dest),mode(1),s3(srdummy),s4(srdummy),d2(srdummy),i_start(i_start),i_stop(i_stop){}

	drmtmulszal(const dsrmatrix &src1,const drmatrix &src2,drmatrix &dest,u32 i_start=0,u32 i_stop=0)
		:s3(src1),s2(src2),d(dest),mode(2),s1(rdummy),s4(srdummy),d2(srdummy),i_start(i_start),i_stop(i_stop){}

	drmtmulszal(const drmatrix &src1,const drmatrix &src2,dsrmatrix &dest,u32 i_start=0,u32 i_stop=0)
		:s1(src1),s2(src2),d2(dest),mode(3),s3(srdummy),s4(srdummy),d(rdummy),i_start(i_start),i_stop(i_stop){}

	drmtmulszal(const drmatrix &src1,const dsrmatrix &src2,drmatrix &dest,u32 i_start=0,u32 i_stop=0)
		:s1(src1),s3(src2),d(dest),mode(4),s2(rdummy),s4(srdummy),d2(srdummy),i_start(i_start),i_stop(i_stop){}

	drmtmulszal(const dsrmatrix &src1,const dsrmatrix &src2,dsrmatrix &dest,u32 i_start=0,u32 i_stop=0)
		:s3(src1),s4(src2),d2(dest),mode(5),s1(rdummy),s2(rdummy),d(rdummy),i_start(i_start),i_stop(i_stop){}


	void run(){
		switch(mode){
            case 1: d.tmul(s1,s2,false,i_start,i_stop); break;
			case 2: d.tmul(s3,s2,false,i_start,i_stop); break;
			case 3: d2.tmul(s1,s2,false,i_start,i_stop); break;
			case 4: d.tmul(s1,s3,false); break;
			case 5: d2.tmul_vigyazz(s3,s4,false,i_start,i_stop); break;
		}
	}
};


//***********************************************************************
class drminvszal:public PLThread{
//***********************************************************************
	static drmatrix rdummy;
	static dsrmatrix srdummy;
	drmatrix &d;
	dsrmatrix &d2;
	cu32 mode;
public:
	drminvszal(drmatrix &srcdest):d(srcdest),d2(srdummy),mode(1){}
	drminvszal(dsrmatrix &srcdest):d2(srcdest),d(rdummy),mode(2){}
	void run(){if(mode==1)d.inv();else d2.ninv();}
};

//***********************************************************************
class dcmatrix;
class dscmatrix;
//***********************************************************************
//***********************************************************************
class dcvector{
//***********************************************************************
	drvector re,im;
	friend class dcmatrix;
	friend class dscmatrix;
	void strasseninv();
public:
	void free(){re.free();im.free();}
	u32 getsiz()const{return re.getsiz();}
	u32 getrow()const{return re.getrow();}
	u32 getcol()const{return re.getcol();}
	u32 getMemUsage(){return re.getMemUsage()+im.getMemUsage()+sizeof(*this);;}
	void copy(const dcvector & src){re.copy(src.re);im.copy(src.im);}
	void swap(dcvector & src){re.swap(src.re);im.swap(src.im);}
	void print(bool matrixkent=false)const{re.print(matrixkent);im.print(matrixkent);}
	void zero(cd Re=0.0,cd Im=0.0){re.zero(Re);im.zero(Im);}
	void neg(){re.neg();im.neg();}
	void random(){re.random();im.random();}
	void novekvo(){re.novekvo();im.novekvo();}
	void add(const dcvector & src1,const dcvector & src2){re.add(src1.re,src2.re);im.add(src1.im,src2.im);}
	void addnr(const dcvector & src1,const dcvector & src2){re.addnr(src1.re,src2.re);im.addnr(src1.im,src2.im);}
	void pluszegyenlo(const dcvector & src1){re.pluszegyenlo(src1.re);im.pluszegyenlo(src1.im);}
	void sub(const dcvector & src1,const dcvector & src2){re.sub(src1.re,src2.re);im.sub(src1.im,src2.im);}
	void subnr(const dcvector & src1,const dcvector & src2){re.subnr(src1.re,src2.re);im.subnr(src1.im,src2.im);}
	void minuszegyenlo(const dcvector & src1){re.minuszegyenlo(src1.re);im.minuszegyenlo(src1.im);}
	void save(FILE *fp)const{re.save(fp);im.save(fp);}
	void savetext(const char * reFileName,const char * imFileName)const{re.savetext(reFileName);im.savetext(imFileName);}
	void load(FILE *fp){re.load(fp);im.load(fp);}
	void store(FILE *fp){re.store(fp);im.store(fp);}
	//***********************************************************************
	void resize(cu32 n){re.resize(n);im.resize(n);}
	void set(cu32 i,cdc v){re.set(i,v.re);im.set(i,v.im);}
	dcomplex get(cu32 i)const{return dcomplex(re.get(i),im.get(i));}
	void inc(cu32 i,cdc v){re.inc(i,v.re);im.inc(i,v.im);}
	void inc0(cdc v){re.inc0(v.re);im.inc0(v.im);}
	//***********************************************************************
	void mul(const dcmatrix & src1,const dcvector & src2){tmul(src1,src2);}
	void mult(const dcmatrix & src1,const dcvector & src2){tmult(src1,src2);}
	void mul(const dscmatrix & src1,const dcvector & src2){tmul(src1,src2);}
	void tmul(const dcmatrix & src1,const dcvector & src2,const bool adde=false);
	void tmult(const dcmatrix & src1,const dcvector & src2,const bool adde=false);
	void tmul(const dscmatrix & src1,const dcvector & src2,const bool adde=false);
	void tmuladd(const dcmatrix & src1,const dcvector & src2);
	void tmuladd(const dscmatrix & src1,const dcvector & src2);
	//***********************************************************************
	void getsubvector(const dcvector & src,cu32 n0,cu32 n){re.getsubvector(src.re,n0,n);im.getsubvector(src.im,n0,n);}
	void setsubvector(const dcvector & src,cu32 n0){re.setsubvector(src.re,n0);im.setsubvector(src.im,n0);}
	void getsetsubvector(const dcvector & src,cu32 from_n0,cu32 to_n0,cu32 n){re.getsetsubvector(src.re,from_n0,to_n0,n);im.getsetsubvector(src.im,from_n0,to_n0,n);}
	void getsetaddsubvector(const dcvector & src,cu32 from_n0,cu32 to_n0,cu32 n){re.getsetaddsubvector(src.re,from_n0,to_n0,n);im.getsetaddsubvector(src.im,from_n0,to_n0,n);}
};


//***********************************************************************
class dscmatrix{
//***********************************************************************
	dsrmatrix re,im;
	friend class dcmatrix;
	friend class dcvector;
	void strasseninv();
public:
	void free(){re.free();im.free();}
	void resize(cu32 r,cu32 c){re.resize(r,c);im.resize(r,c);}
	u32 getsiz()const{return re.getsiz();}
	u32 getrow()const{return re.getrow();}
	u32 getcol()const{return re.getcol();}
	u32 getMemUsage(){return re.getMemUsage()+im.getMemUsage()+sizeof(*this);;}
	void copy(const dscmatrix & src){re.copy(src.re);im.copy(src.im);}
	void swap(dscmatrix & src){re.swap(src.re);im.swap(src.im);}
	void print(bool matrixkent=false)const{re.print(matrixkent);im.print(matrixkent);}
	void zero(cd Re=0.0,cd Im=0.0){re.zero(Re);im.zero(Im);}
	void neg(){re.neg();im.neg();}
	void random(){re.random();im.random();}
	void novekvo(){re.novekvo();im.novekvo();}
	void add(const dscmatrix & src1,const dscmatrix & src2){re.add(src1.re,src2.re);im.add(src1.im,src2.im);}
	void addnr(const dscmatrix & src1,const dscmatrix & src2){re.addnr(src1.re,src2.re);im.addnr(src1.im,src2.im);}
	void pluszegyenlo(const dscmatrix & src1){re.pluszegyenlo(src1.re);im.pluszegyenlo(src1.im);}
	void sub(const dscmatrix & src1,const dscmatrix & src2){re.sub(src1.re,src2.re);im.sub(src1.im,src2.im);}
	void subnr(const dscmatrix & src1,const dscmatrix & src2){re.subnr(src1.re,src2.re);im.subnr(src1.im,src2.im);}
	void minuszegyenlo(const dscmatrix & src1){re.minuszegyenlo(src1.re);im.minuszegyenlo(src1.im);}
	void save(FILE *fp)const{re.save(fp);im.save(fp);}
	void savetext(const char * reFileName,const char * imFileName)const{re.savetext(reFileName);im.savetext(imFileName);}
	void load(FILE *fp){re.load(fp);im.load(fp);}
	void store(FILE *fp){re.store(fp);im.store(fp);}
	//***********************************************************************
	void resize(cu32 n){resize(n,n);}
	void egyseg(){re.egyseg();im.egyseg();}
	void set(cu32 r,cu32 c,cdc v){re.set(r,c,v.re);im.set(r,c,v.im);}
	dcomplex get(cu32 r,cu32 c)const{return dcomplex(re.get(r,c),im.get(r,c));}
	void inc(cu32 r,cu32 c,cdc v){re.inc(r,c,v.re);im.inc(r,c,v.im);}
	void inc00(cdc v){re.inc00(v.re);im.inc00(v.im);}
	void transp(const dscmatrix & src){re.transp(src.re);im.transp(src.im);}
	void tmul(const dcmatrix & src1,const dcmatrix & src2,const bool adde=false);
	void tmul_vigyazz(const dscmatrix & src1,const dscmatrix & src2,const bool adde=false);// �ltal�ban l�t symm szorzata NEM symm!!!
	void ninv();
	void mul(const dcmatrix & src1,const dcmatrix & src2);
	//***********************************************************************
	void mul_vigyazz(const dscmatrix & src1,const dscmatrix & src2){
	// �ltal�ban k�t symm szorzata NEM symm!!!
	//***********************************************************************
		tmul_vigyazz(src1,src2);
	}
	//***********************************************************************
	void inv(){
	//***********************************************************************
		ninv();
		neg();
	}
	void getsubmatrix(const dscmatrix & src,cu32 n0,cu32 n){
		re.getsubmatrix(src.re,n0,n);im.getsubmatrix(src.im,n0,n);
	}
	void getsetsubmatrix(const dscmatrix & src,cu32 n0_from,cu32 n0_to,cu32 n){
		re.getsetsubmatrix(src.re,n0_from,n0_to,n);im.getsetsubmatrix(src.im,n0_from,n0_to,n);
	}
	void getsetsubmatrix(const dscmatrix & src,cu32 y0_from,cu32 x0_from,cu32 y0_to,cu32 x0_to,cu32 y,cu32 x){
		re.getsetsubmatrix(src.re,y0_from,x0_from,y0_to,x0_to,y,x);im.getsetsubmatrix(src.im,y0_from,x0_from,y0_to,x0_to,y,x);
	}
	void getsetsubmatrixadd(const dscmatrix & src,cu32 n0_from,cu32 n0_to,cu32 n){
		re.getsetsubmatrixadd(src.re,n0_from,n0_to,n);im.getsetsubmatrixadd(src.im,n0_from,n0_to,n);
	}
	void setsubmatrix(const dscmatrix & src,cu32 n0){
		re.setsubmatrix(src.re,n0);im.setsubmatrix(src.im,n0);
	}
	void setsubmatrixadd(const dscmatrix & src,cu32 n0){
		re.setsubmatrixadd(src.re,n0);im.setsubmatrixadd(src.im,n0);
	}
	void setsubmatrix(const dcmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixadd(const dcmatrix & src,cu32 y0,cu32 x0);
};


//***********************************************************************
class dcmatrix{
//***********************************************************************
	friend class dscmatrix;
	friend class dcvector;
	drmatrix re,im;
	void strasseninv();
public:
	void free(){re.free();im.free();}
	void resize(cu32 r,cu32 c){re.resize(r,c);im.resize(r,c);}
	u32 getsiz()const{return re.getsiz();}
	u32 getrow()const{return re.getrow();}
	u32 getcol()const{return re.getcol();}
	u32 getMemUsage(){return re.getMemUsage()+im.getMemUsage()+sizeof(*this);;}
	void copy(const dcmatrix & src){re.copy(src.re);im.copy(src.im);}
	void swap(dcmatrix & src){re.swap(src.re);im.swap(src.im);}
	void print(bool matrixkent=false)const{re.print(matrixkent);im.print(matrixkent);}
	void zero(cd Re=0.0,cd Im=0.0){re.zero(Re);im.zero(Im);}
	void neg(){re.neg();im.neg();}
	void random(){re.random();im.random();}
	void novekvo(){re.novekvo();im.novekvo();}
	void add(const dcmatrix & src1,const dcmatrix & src2){re.add(src1.re,src2.re);im.add(src1.im,src2.im);}
	void addnr(const dcmatrix & src1,const dcmatrix & src2){re.addnr(src1.re,src2.re);im.addnr(src1.im,src2.im);}
	void pluszegyenlo(const dcmatrix & src1){re.pluszegyenlo(src1.re);im.pluszegyenlo(src1.im);}
	void sub(const dcmatrix & src1,const dcmatrix & src2){re.sub(src1.re,src2.re);im.sub(src1.im,src2.im);}
	void subnr(const dcmatrix & src1,const dcmatrix & src2){re.subnr(src1.re,src2.re);im.subnr(src1.im,src2.im);}
	void minuszegyenlo(const dcmatrix & src1){re.minuszegyenlo(src1.re);im.minuszegyenlo(src1.im);}
	void save(FILE *fp)const{re.save(fp);im.save(fp);}
	void savetext(const char * reFileName,const char * imFileName)const{re.savetext(reFileName);im.savetext(imFileName);}
	void load(FILE *fp){re.load(fp);im.load(fp);}
	void store(FILE *fp){re.store(fp);im.store(fp);}
	//***********************************************************************
	void egyseg(){re.egyseg();im.egyseg();}
	void set(cu32 r,cu32 c,cdc v){re.set(r,c,v.re);im.set(r,c,v.im);}
	dcomplex get(cu32 r,cu32 c)const{return dcomplex(re.get(r,c),im.get(r,c));}
	void inc(cu32 r,cu32 c,cdc v){re.inc(r,c,v.re);im.inc(r,c,v.im);}
	void inc00(cdc v){re.inc00(v.re);im.inc00(v.im);}
	void transp(const dcmatrix & src){re.transp(src.re);im.transp(src.im);}
	void convert(const dscmatrix & src){re.convert(src.re);im.convert(src.im);}
	void tmul(const dcmatrix & src1,const dcmatrix & src2,const bool adde=false);
	void tmul(const dscmatrix & src1,const dcmatrix & src2,const bool adde=false);
	void tmul(const dcmatrix & src1,const dscmatrix & src2,const bool adde=false);
	void inv();
	void submatrixadd(const dcmatrix & src1,cu32 y1,cu32 x1,const dcmatrix & src2,cu32 y2,cu32 x2,cu32 y,cu32 x){
		re.submatrixadd(src1.re,y1,x1,src2.re,y2,x2,y,x);im.submatrixadd(src1.im,y1,x1,src2.im,y2,x2,y,x);
	}
	void submatrixsub(const dcmatrix & src1,cu32 y1,cu32 x1,const dcmatrix & src2,cu32 y2,cu32 x2,cu32 y,cu32 x){
		re.submatrixsub(src1.re,y1,x1,src2.re,y2,x2,y,x);im.submatrixadd(src1.im,y1,x1,src2.im,y2,x2,y,x);
	}
	void getsubmatrix(const dcmatrix & src,cu32 y0,cu32 x0,cu32 y,cu32 x){
		re.getsubmatrix(src.re,y0,x0,y,x);im.getsubmatrix(src.im,y0,x0,y,x);
	}
	void getsetsubmatrix(const dscmatrix & src,cu32 y0_from,cu32 x0_from,cu32 y0_to,cu32 x0_to,cu32 y,cu32 x){
		re.getsetsubmatrix(src.re,y0_from,x0_from,y0_to,x0_to,y,x);im.getsetsubmatrix(src.im,y0_from,x0_from,y0_to,x0_to,y,x);
	}
	void setsubmatrix(const dcmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrix(src.re,y0,x0);im.setsubmatrix(src.im,y0,x0);
	}
	void setsubmatrixadd(const dcmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrixadd(src.re,y0,x0);im.setsubmatrixadd(src.im,y0,x0);
	}
	void getsubmatrix(const dscmatrix & src,cu32 y0,cu32 x0,cu32 y,cu32 x){
		re.getsubmatrix(src.re,y0,x0,y,x);im.getsubmatrix(src.im,y0,x0,y,x);
	}
	void setsubmatrix(const dscmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrix(src.re,y0,x0);im.setsubmatrix(src.im,y0,x0);
	}
	void setsubmatrixadd(const dscmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrixadd(src.re,y0,x0);im.setsubmatrixadd(src.im,y0,x0);
	}
	void setsubmatrixneg(const dcmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrixneg(src.re,y0,x0);im.setsubmatrixneg(src.im,y0,x0);
	}
	void setsubmatrixnegadd(const dcmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrixnegadd(src.re,y0,x0);im.setsubmatrixnegadd(src.im,y0,x0);
	}
	//***********************************************************************

	//***********************************************************************
	void mul(const dcmatrix & src1,const dcmatrix & src2){
	//***********************************************************************
#ifdef vsundebugmode
		logprint("dcmatrix::mul => nem hatekony");
#endif
		dcmatrix r;
		r.transp(src2);
		tmul(src1,r);
	}

};

//***********************************************************************
inline void dscmatrix::mul(const dcmatrix & src1,const dcmatrix & src2){
//***********************************************************************
#ifdef vsundebugmode2
	logprint("dscmatrix::mul => nem hatekony");
#endif
	dcmatrix r;
	r.transp(src2);
	tmul(src1,r);
}

//***********************************************************************
inline void dscmatrix::setsubmatrix(const dcmatrix & src,cu32 y0,cu32 x0){
//***********************************************************************
	re.setsubmatrix(src.re,y0,x0);im.setsubmatrix(src.im,y0,x0);
}


//***********************************************************************
inline void dscmatrix::setsubmatrixadd(const dcmatrix & src,cu32 y0,cu32 x0){
//***********************************************************************
	re.setsubmatrixadd(src.re,y0,x0);im.setsubmatrixadd(src.im,y0,x0);
}


//***********************************************************************
class dcmtmulszal:public PLThread{
//***********************************************************************
	static dcmatrix cdummy;
	static dscmatrix scdummy;
	const dcmatrix &s1,&s2;
	const dscmatrix &s3,&s4;
	dcmatrix &d;
	dscmatrix &d2;
	cu32 mode;
public:
	dcmtmulszal(const dcmatrix &src1,const dcmatrix &src2,dcmatrix &dest)
		:s1(src1),s2(src2),d(dest),mode(1),s3(scdummy),s4(scdummy),d2(scdummy){}

	dcmtmulszal(const dscmatrix &src1,const dcmatrix &src2,dcmatrix &dest)
		:s3(src1),s2(src2),d(dest),mode(2),s1(cdummy),s4(scdummy),d2(scdummy){}

	dcmtmulszal(const dcmatrix &src1,const dcmatrix &src2,dscmatrix &dest)
		:s1(src1),s2(src2),d2(dest),mode(3),s3(scdummy),s4(scdummy),d(cdummy){}

	dcmtmulszal(const dcmatrix &src1,const dscmatrix &src2,dcmatrix &dest)
		:s1(src1),s3(src2),d(dest),mode(4),s2(cdummy),s4(scdummy),d2(scdummy){}

	dcmtmulszal(const dscmatrix &src1,const dscmatrix &src2,dscmatrix &dest)
		:s3(src1),s4(src2),d2(dest),mode(5),s1(cdummy),s2(cdummy),d(cdummy){}


	void run(){
		switch(mode){
			case 1: d.tmul(s1,s2); break;
			case 2: d.tmul(s3,s2); break;
			case 3: d2.tmul(s1,s2); break;
			case 4: d.tmul(s1,s3); break;
			case 5: d2.tmul_vigyazz(s3,s4); break;
		}
	}
};


#endif
