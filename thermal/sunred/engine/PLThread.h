//***********************************************************************
// PLThread class header
// Creation date:	2008. 04. 02.
// Creator:			Pohl L�szl�
// Modified date:	2008. 04. 02.
//***********************************************************************


//***********************************************************************
#ifndef PLTHREAD_HEADER
#define	PLTHREAD_HEADER
//***********************************************************************
#include <pthread.h>

class PLThread{
    static unsigned akt_szalszam;
    static pthread_mutex_t akt_szalszam_mutex;
	pthread_t th;
	static void * parameter_fuggveny(void*v){
		if(!v){
			printf("\n\nError: NULL pointer in PLThread::parameter_fuggveny\n");
			exit(-1);
		}
		((PLThread*)v)->run();
		return 0;
	}
    static void inc_szalszam(){
		pthread_mutex_lock(&akt_szalszam_mutex);
		akt_szalszam++;
		pthread_mutex_unlock(&akt_szalszam_mutex);
    }
    static void dec_szalszam(){
		pthread_mutex_lock(&akt_szalszam_mutex);
		if(akt_szalszam>0)akt_szalszam--; // a logithermben ezt if(skt_szalszam>0)-ra cser�ltem, nem tudom, mi�rt.
		pthread_mutex_unlock(&akt_szalszam_mutex);
    }
public:
    static unsigned get_szalszam(){return akt_szalszam;}
	void start(){
        inc_szalszam();
		pthread_create(&th,NULL,parameter_fuggveny,this);
	}
	virtual void run(){printf("nem lehet\n");}
	void wait(){
        pthread_join(th,NULL);
        dec_szalszam();
    }
};

#endif