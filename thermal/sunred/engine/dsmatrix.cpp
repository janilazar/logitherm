//***********************************************************************
// Vector SUNRED 2 simmetrical matrix source
// Creation date:	2008. 04. 17.
// Creator:			Pohl L�szl�
//***********************************************************************


//***********************************************************************
#ifndef VSUN2_DSMATRIX_SOURCE
#define	VSUN2_DSMATRIX_SOURCE
//***********************************************************************


//***********************************************************************
#include "dmatrix.h"
//***********************************************************************


//***********************************************************************
// static defin�ci�k (nem haszn�lljuk ezeket a m�trixokat)
drmatrix drmtmulszal::rdummy;
dsrmatrix drmtmulszal::srdummy;
dcmatrix dcmtmulszal::cdummy;
dscmatrix dcmtmulszal::scdummy;
drmatrix drminvszal::rdummy;
dsrmatrix drminvszal::srdummy;
//***********************************************************************


//***********************************************************************
void drmatrix::convert(const dsrmatrix & src){
//***********************************************************************
	resize(src.row,src.col);
	for(u32 j=0;j<row;j++)src.getLine(t+col*j,j);
}


//***********************************************************************
void drmatrix::tmul(const drmatrix & src1,const dsrmatrix & src2,const bool adde){
// src2 egy transzpon�lt m�trix
//***********************************************************************
#ifdef vsundebugmode
	if(&src1==this)throw hiba("","drmatrix::tmul => &src1==this");
	if(src1.col!=src2.col)throw hiba("","drmatrix::tmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
#endif
#ifdef vsundebugmode2
//	logprint("drmatrix::tmul => drmatrix*dsrmatrix tobblet memoriaigeny");
#endif
    if(src2.siz==1){
        if(siz!=src1.siz)resize(src1.row,src1.col);
        if(adde){
            for(uns i=0;i<siz;i++)t[i] += src1.t[i] * src2.t[0];
        }
        else{
            for(uns i=0;i<siz;i++)t[i] = src1.t[i] * src2.t[0];
        }
    }
    else{
	    drmatrix masolat;
	    masolat.convert(src2);
	    tmul(src1,masolat,adde);
    }
}

//***********************************************************************
void drmatrix::tmul(const drmatrix & src1,const dsrmatrix & src2,drmatrix & masolat,const bool adde){
// src2 egy transzpon�lt m�trix
// Az el�z� fv-t�l abban k�l�nb�zik, hogy k�v�lr�l kapja az ideiglenes
// m�trix�t, �gy nem kell mindig �jat foglalni
//***********************************************************************
#ifdef vsundebugmode
	if(&src1==this)throw hiba("","drmatrix::tmul => &src1==this");
	if(src1.col!=src2.col)throw hiba("","drmatrix::tmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
#endif
#ifdef vsundebugmode2
//	logprint("drmatrix::tmul => drmatrix*dsrmatrix tobblet memoriaigeny");
#endif
    if(src2.siz==1){
        if(siz!=src1.siz)resize(src1.row,src1.col);
        if(adde){
            for(uns i=0;i<siz;i++)t[i] += src1.t[i] * src2.t[0];
        }
        else{
            for(uns i=0;i<siz;i++)t[i] = src1.t[i] * src2.t[0];
        }
    }
    else{
	    masolat.convert(src2);
	    tmul(src1,masolat,adde);
    }
}

//***********************************************************************
void drmatrix::tmul(const dsrmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop){
// src2 egy transzpon�lt m�trix
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","drmatrix::tmul => &src2==this");
	if(src1.col!=src2.col)throw hiba("","drmatrix::tmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if(adde){
		if(row!=src1.row)throw hiba("","drmatrix::tmul => row!=src1.row => %lu!=%lu",row,src1.row);
		if(col!=src2.row)throw hiba("","drmatrix::tmul => col!=src2.row => %lu!=%lu",col,src2.row);
	}
#endif
#ifdef PL_PROFILE
	GyujtStart();
#endif

    cd szorszam=dbl(src2.row)*src1.row*src1.col;

    if(!adde && i_stop==0){resize(src1.row,src2.row);zero();} // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if((mxOptimize!=mxMinSize) && (((src1.col|src2.row)&1)==0) && 
		(STRASSEN_MUL_N_LIMIT()<=szorszam)){
		strassentmul(src1,src2);
		return;
	}
    //if(i_stop==0 && szorszam>MUL_N_LIMIT && row>16){ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
    //    uns szalszam=(uns)(szorszam*2/MUL_N_LIMIT);
    //    szalszam = ( szalszam > row/8 ) ? row/8 : szalszam;
    //    szalszam = ( szalszam > VSUN_CPU_Thread ) ? VSUN_CPU_Thread : szalszam;
    //    if(szalszam>1){
    //        //
    //    }
    //}
	cu32 n=src1.col;
if(mxMulMode==mxMinSize){
	dbl * t0=t;
//	cd * t1p=src1.t+n*(n+1)/2*b;//n==src1.col,row==src1.row
	dbl dum[NEWKIVALTO],*t1=n>NEWKIVALTO?new dbl[n]:dum;
	for(u32 i=0;i<row;i++){
		cd * t2=src2.t;//n==src1.col,col==src1.row
		src1.getLine(t1,i);
		for(u32 j=0;j<col;j++,t0++,t2+=n){
			dbl sum=t1[0]*t2[0];
			for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
			*t0+=sum;
		}
	}
	if(n>NEWKIVALTO)delete [] t1;
}
else{
	cu32 ni=row,nj=col,nkx=src1.col;
	cu32 di=row%4,dj=col%4,dk=src1.col%4;
	cu32 hi=ni-di,hj=nj-dj,hk=n-dk;
	dbl dum[4*NEWKIVALTO],*t1=n>NEWKIVALTO?new dbl[4*n]:dum;
	dbl *d0=t,*d1=t+nj,*d2=t+2*nj,*d3=t+3*nj;
	cd *a0=t1,*a1=t1+n,*a2=t1+2*n,*a3=t1+3*n;
	for(u32 i=0;i<hi;i+=4){
		cd *b0=src2.t,*b1=src2.t+n,*b2=src2.t+2*n,*b3=src2.t+3*n;
		src1.getLine(t1,    i);
		src1.getLine(t1+n,  i+1);
		src1.getLine(t1+2*n,i+2);
		src1.getLine(t1+3*n,i+3);
		for(u32 j=0;j<hj;j+=4){
			for(u32 k=0;k<hk;k+=4,a0+=4,a1+=4,a2+=4,a3+=4,b0+=4,b1+=4,b2+=4,b3+=4){
				d0[0]+=a0[0]*b0[0]+a0[1]*b0[1]+a0[2]*b0[2]+a0[3]*b0[3];
				d0[1]+=a0[0]*b1[0]+a0[1]*b1[1]+a0[2]*b1[2]+a0[3]*b1[3];
				d0[2]+=a0[0]*b2[0]+a0[1]*b2[1]+a0[2]*b2[2]+a0[3]*b2[3];
				d0[3]+=a0[0]*b3[0]+a0[1]*b3[1]+a0[2]*b3[2]+a0[3]*b3[3];

				d1[0]+=a1[0]*b0[0]+a1[1]*b0[1]+a1[2]*b0[2]+a1[3]*b0[3];
				d1[1]+=a1[0]*b1[0]+a1[1]*b1[1]+a1[2]*b1[2]+a1[3]*b1[3];
				d1[2]+=a1[0]*b2[0]+a1[1]*b2[1]+a1[2]*b2[2]+a1[3]*b2[3];
				d1[3]+=a1[0]*b3[0]+a1[1]*b3[1]+a1[2]*b3[2]+a1[3]*b3[3];

				d2[0]+=a2[0]*b0[0]+a2[1]*b0[1]+a2[2]*b0[2]+a2[3]*b0[3];
				d2[1]+=a2[0]*b1[0]+a2[1]*b1[1]+a2[2]*b1[2]+a2[3]*b1[3];
				d2[2]+=a2[0]*b2[0]+a2[1]*b2[1]+a2[2]*b2[2]+a2[3]*b2[3];
				d2[3]+=a2[0]*b3[0]+a2[1]*b3[1]+a2[2]*b3[2]+a2[3]*b3[3];

				d3[0]+=a3[0]*b0[0]+a3[1]*b0[1]+a3[2]*b0[2]+a3[3]*b0[3];
				d3[1]+=a3[0]*b1[0]+a3[1]*b1[1]+a3[2]*b1[2]+a3[3]*b1[3];
				d3[2]+=a3[0]*b2[0]+a3[1]*b2[1]+a3[2]*b2[2]+a3[3]*b2[3];
				d3[3]+=a3[0]*b3[0]+a3[1]*b3[1]+a3[2]*b3[2]+a3[3]*b3[3];
			}
			for(u32 k=0;k<dk;k++){
				d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];	d0[2]+=a0[k]*b2[k];	d0[3]+=a0[k]*b3[k];
				d1[0]+=a1[k]*b0[k];	d1[1]+=a1[k]*b1[k];	d1[2]+=a1[k]*b2[k];	d1[3]+=a1[k]*b3[k];
				d2[0]+=a2[k]*b0[k];	d2[1]+=a2[k]*b1[k];	d2[2]+=a2[k]*b2[k];	d2[3]+=a2[k]*b3[k];
				d3[0]+=a3[k]*b0[k];	d3[1]+=a3[k]*b1[k];	d3[2]+=a3[k]*b2[k];	d3[3]+=a3[k]*b3[k];
			}
			d0+=4,d1+=4,d2+=4,d3+=4;
			a0-=hk,a1-=hk,a2-=hk,a3-=hk,b0+=dk+3*n,b1+=dk+3*n,b2+=dk+3*n,b3+=dk+3*n;
		}
		for(u32 j=0;j<dj;j++){
			for(u32 k=0;k<n;k++){
				d0[j]+=a0[k]*b0[j*n+k]; d1[j]+=a1[k]*b0[j*n+k]; d2[j]+=a2[k]*b0[j*n+k]; d3[j]+=a3[k]*b0[j*n+k];
			}
		}
		d0+=dj+3*nj; d1+=dj+3*nj; d2+=dj+3*nj; d3+=dj+3*nj;
	}
	const dbl *b0=src2.t;
	for(u32 i=0;i<di;i++){
		src1.getLine(t1,hi+i);//a0=t1
		for(u32 j=0;j<nj;j++){
			for(u32 k=0;k<n;k++)d0[i*nj+j]+=a0[k]*b0[j*n+k];
		}
	}
	if(n>128)delete [] t1;
}
#ifdef PL_PROFILE
	GyujtStop(0);
#endif
}



//***********************************************************************
void dsrmatrix::getsubmatrix(const dsrmatrix & src,cu32 n0,cu32 n){
// Mag�ba rakja src-b�l az x0,y0-n�l kezd�d� x,y m�ret� r�szm�trixot
//***********************************************************************
#ifdef vsundebugmode
	if(n0+n>src.col)throw hiba("","dsrmatrix::getsubmatrix => n0+n>src.col => %lu+%lu>%lu",n0,n,src.col);
#endif
	resize(n);
	dbl * t0=t;
	for(u32 i=0;i<n;t0+=n-i,i++)src.getLine(t0,n0+i,n0+i,n-i);
}


//***********************************************************************
void dsrmatrix::getsetsubmatrix(const dsrmatrix & src,cu32 n0_from,cu32 n0_to,cu32 n){
// Mag�ba rakja src-b�l n0_to-hoz az n0_from-n�l kezd�d� x,y m�ret� r�szm�trixot
//***********************************************************************
#ifdef vsundebugmode
	if(n0_from+n>src.col)throw hiba("","dsrmatrix::getsetsubmatrix => n0_from+n>src.col => %lu+%lu>%lu",n0_from,n,src.col);
	if(n0_to+n>col)throw hiba("","dsrmatrix::getsetsubmatrix => n0_to+n>col => %lu+%lu>%lu",n0_to,n,col);
#endif
	dbl * t0=t+getcimunsafe(n0_to,n0_to);
	cu32 cs=col-n0_to;
	for(u32 i=0;i<n;t0+=cs-i,i++)src.getLine(t0,n0_from+i,n0_from+i,n-i);
}


//***********************************************************************
void dsrmatrix::getsetsubmatrixadd(const dsrmatrix & src,cu32 n0_from,cu32 n0_to,cu32 n){
// Mag�ba rakja src-b�l n0_to-hoz az n0_from-n�l kezd�d� x,y m�ret� r�szm�trixot
//***********************************************************************
#ifdef vsundebugmode
	if(n0_from+n>src.col)throw hiba("","dsrmatrix::getsetsubmatrixadd => n0_from+n>src.col => %lu+%lu>%lu",n0_from,n,src.col);
	if(n0_to+n>col)throw hiba("","dsrmatrix::getsetsubmatrixadd => n0_to+n>col => %lu+%lu>%lu",n0_to,n,col);
#endif
	dbl * t0=t+getcimunsafe(n0_to,n0_to);
	cu32 cs=col-n0_to;
	for(u32 i=0;i<n;t0+=cs-i,i++)src.getLineadd(t0,n0_from+i,n0_from+i,n-i);
}


//***********************************************************************
void dsrmatrix::setsubmatrix(const dsrmatrix & src,cu32 n0){
// Mag�ba rakja src-b�l az x0,y0-n�l r�szm�trixot, felt�telezi, hogy src 1 blokk
//***********************************************************************
#ifdef vsundebugmode
	if(n0+src.col>col)throw hiba("","dsrmatrix::setsubmatrix => n0+src.col>col => %lu+%lu>%lu",n0,src.col,col);
#endif
	dbl * t0=t+getcimunsafe(n0,n0);
	cu32 y=src.row,n=col;
	for(u32 i=0;i<y;t0+=n-n0-i,i++)src.getLine(t0,i,i,y-i);//mivel src.row==src.col
}


//***********************************************************************
void dsrmatrix::setsubmatrixadd(const dsrmatrix & src,cu32 n0){
// Mag�ba rakja src-b�l az x0,y0-n�l r�szm�trixot, felt�telezi, hogy src 1 blokk
//***********************************************************************
#ifdef vsundebugmode
	if(n0+src.col>col)throw hiba("","dsrmatrix::setsubmatrixadd => n0+src.col>col => %lu+%lu>%lu",n0,src.col,col);
#endif
	dbl * t0=t+getcimunsafe(n0,n0);
	cu32 y=src.row,n=col;
	for(u32 i=0;i<y;t0+=n-n0-i,i++)src.getLineadd(t0,i,i,y-i);//mivel src.row==src.col
}


//***********************************************************************
void dsrmatrix::setsubmatrix(const drmatrix & src,cu32 y0,cu32 x0){
// Mag�ba rakja src-b�l az x0,y0-n�l r�szm�trixot, felt�telezi, hogy src 1 blokk
//***********************************************************************
#ifdef vsundebugmode
	if(y0+src.row>row)throw hiba("","dsrmatrix::setsubmatrix => y0+src.row>row => %lu+%lu>%lu",y0,src.row,row);
	if(y0+src.row>x0)throw hiba("","dsrmatrix::setsubmatrix => y0+src.row>x0 => %lu+%lu>%lu",y0,src.row,x0);
	if(x0+src.col>col)throw hiba("","dsrmatrix::setsubmatrix => x0+src.col>col => %lu+%lu>%lu",x0,src.col,col);
#endif
	dbl * t0=t+getcimunsafe(y0,x0);
	cu32 y=src.row,n=col;
	for(u32 i=0;i<y;++i,t0+=n-y0-i)src.getLine(t0,i);
}


//***********************************************************************
void dsrmatrix::setsubmatrixadd(const drmatrix & src,cu32 y0,cu32 x0){
// Mag�ba rakja src-b�l az x0,y0-n�l r�szm�trixot, felt�telezi, hogy src 1 blokk
//***********************************************************************
#ifdef vsundebugmode
	if(y0+src.row>row)throw hiba("","dsrmatrix::setsubmatrixadd => y0+src.row>row => %lu+%lu>%lu",y0,src.row,row);
	if(y0+src.row>x0)throw hiba("","dsrmatrix::setsubmatrixadd => y0+src.row>x0 => %lu+%lu>%lu",y0,src.row,x0);
	if(x0+src.col>col)throw hiba("","dsrmatrix::setsubmatrixadd => x0+src.col>col => %lu+%lu>%lu",x0,src.col,col);
#endif
	dbl * t0=t+getcimunsafe(y0,x0);
	cu32 y=src.row,n=col;
	for(u32 i=0;i<y;++i,t0+=n-y0-i)src.getLineadd(t0,i);
}


//***********************************************************************
void drmatrix::getsubmatrix(const dsrmatrix & src,cu32 y0,cu32 x0,cu32 y,cu32 x){
// Mag�ba rakja src-b�l az x0,y0-n�l kezd�d� x,y m�ret� r�szm�trixot
//***********************************************************************
#ifdef vsundebugmode
	if(x0+x>src.col)throw hiba("","drmatrix::getsubmatrix => x0+x>src.col => %lu+%lu>%lu",x0,x,src.col);
	if(y0+y>src.row)throw hiba("","drmatrix::getsubmatrix => y0+y>src.row => %lu+%lu>%lu",y0,y,src.row);
#endif
	resize(y,x);
	dbl * t0=t;
	for(u32 i=0;i<y;t0+=x,i++)src.getLine(t0,y0+i,x0,x);
}


//***********************************************************************
void dsrmatrix::getsetsubmatrix(const dsrmatrix & src,cu32 y0_from,cu32 x0_from,cu32 y0_to,cu32 x0_to,cu32 y,cu32 x){
// Mag�ba rakja src-b�l az x0,y0-n�l kezd�d� x,y m�ret� r�szm�trixot
//***********************************************************************
#ifdef vsundebugmode
	if(x0_from+x>src.col)throw hiba("","dsrmatrix::getsetsubmatrix => x0_from+x>src.col => %lu+%lu>%lu",x0_from,x,src.col);
	if(y0_from+y>src.row)throw hiba("","dsrmatrix::getsetsubmatrix => y0_from+y>src.row => %lu+%lu>%lu",y0_from,y,src.row);
	if(x0_to+x>col)throw hiba("","dsrmatrix::getsetsubmatrix => x0_to+x>col => %lu+%lu>%lu",x0_to,x,col);
	if(y0_to+y>row)throw hiba("","dsrmatrix::getsetsubmatrix => y0_to+y>row => %lu+%lu>%lu",y0_to,y,row);
#endif
	dbl * t0=t+getcimunsafe(y0_to,x0_to);
	cu32 cs=col-y0_to-1;
	for(u32 i=0;i<y;t0+=cs-i,i++)src.getLine(t0,y0_from+i,x0_from,x);
}


//***********************************************************************
void drmatrix::getsetsubmatrix(const dsrmatrix & src,cu32 y0_from,cu32 x0_from,cu32 y0_to,cu32 x0_to,cu32 y,cu32 x){
// Mag�ba rakja src-b�l az x0,y0-n�l kezd�d� x,y m�ret� r�szm�trixot
//***********************************************************************
#ifdef vsundebugmode
	if(x0_from+x>src.col)throw hiba("","drmatrix::getsetsubmatrix => x0_from+x>src.col => %lu+%lu>%lu",x0_from,x,src.col);
	if(y0_from+y>src.row)throw hiba("","drmatrix::getsetsubmatrix => y0_from+y>src.row => %lu+%lu>%lu",y0_from,y,src.row);
	if(x0_to+x>col)throw hiba("","drmatrix::getsetsubmatrix => x0_to+x>col => %lu+%lu>%lu",x0_to,x,col);
	if(y0_to+y>row)throw hiba("","drmatrix::getsetsubmatrix => y0_to+y>row => %lu+%lu>%lu",y0_to,y,row);
#endif
	dbl * t0=t+y0_to*col+x0_to;
	for(u32 i=0;i<y;t0+=col,i++)src.getLine(t0,y0_from+i,x0_from,x);
}


//***********************************************************************
void drmatrix::setsubmatrix(const dsrmatrix & src,cu32 y0,cu32 x0){
// Mag�ba rakja src-b�l az x0,y0-n�l r�szm�trixot, felt�telezi, hogy src 1 blokk
//***********************************************************************
#ifdef vsundebugmode
	if(x0+src.col>col)throw hiba("","drmatrix::setsubmatrix => x0+src.col>col => %lu+%lu>%lu",x0,src.col,col);
	if(y0+src.row>row)throw hiba("","drmatrix::setsubmatrix => y0+src.row>row => %lu+%lu>%lu",y0,src.row,row);
#endif
	dbl * t0=t+col*y0+x0;
	cu32 y=src.row,x=src.col,n=col;
	for(u32 i=0;i<y;t0+=n,i++)src.getLine(t0,i,0,x);
}


//***********************************************************************
void drmatrix::setsubmatrixadd(const dsrmatrix & src,cu32 y0,cu32 x0){
// Mag�ba rakja src-b�l az x0,y0-n�l r�szm�trixot, felt�telezi, hogy src 1 blokk
//***********************************************************************
#ifdef vsundebugmode
	if(x0+src.col>col)throw hiba("","drmatrix::setsubmatrix => x0+src.col>col => %lu+%lu>%lu",x0,src.col,col);
	if(y0+src.row>row)throw hiba("","drmatrix::setsubmatrix => y0+src.row>row => %lu+%lu>%lu",y0,src.row,row);
#endif
	dbl * t0=t+col*y0+x0;
	cu32 y=src.row,x=src.col,n=col;
	for(u32 i=0;i<y;t0+=n,i++)src.getLineadd(t0,i,0,x);
}


//***********************************************************************
void dsrmatrix::tmul(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop){
// src2 egy transzpon�lt m�trix
//***********************************************************************
#ifdef vsundebugmode
	if(src1.col!=src2.col)throw hiba("","dsrmatrix::tmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if(src1.row!=src2.row)throw hiba("","dsrmatrix::tmul => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
	if(adde){
		if(row!=src1.row)throw hiba("","dsrmatrix::tmul => row!=src1.row => %lu!=%lu",row,src1.row);
		if(col!=src2.row)throw hiba("","dsrmatrix::tmul => col!=src2.row => %lu!=%lu",col,src2.row);
	}
#endif
#ifdef PL_PROFILE
	GyujtStart();
#endif

    cd szorszam=dbl(src2.row)*(src1.row+1)/2*src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

    // mxOptimize-t �s p�ratlans�got figyelni a m�reten k�v�l!

	if(!adde && i_stop==0){resize(src1.row);zero();} // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if((mxOptimize!=mxMinSize) && (((src1.col|src1.row|src2.row)&1)==0) && 
		(STRASSEN_MUL_N_LIMIT()<=szorszam)){
		strassentmul(src1,src2);
		return;
	}
    if(i_stop==0 && szorszam>MUL_N_LIMIT() && row>16){ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        uns szalszam=(uns)(szorszam*4/MUL_N_LIMIT());
        szalszam = ( szalszam > row/8 ) ? row/8 : szalszam;
        szalszam = ( szalszam > VSUN_CPU_Thread*2 ) ? VSUN_CPU_Thread*2 : szalszam;
        if(szalszam>1){
            //printf("\n%u %u\n",(uns)(szorszam*4/MUL_N_LIMIT),szalszam);
            drmtmulszal ** szaltomb=new drmtmulszal*[szalszam];
            cu32 darab=(row/szalszam/4)*4;
            if(darab<2)throw hiba("dsrmatrix::tmul","darab<2");
            for(uns i=0; i<szalszam-1; i++){
                szaltomb[i]=new drmtmulszal(src1,src2,*this,i*darab,(i+1)*darab);
            }
			parhuzamosanfut=true;
            szaltomb[szalszam-1]=new drmtmulszal(src1,src2,*this,(szalszam-1)*darab,row);
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->start();
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->wait();
            for(uns i=0; i<szalszam; i++)
                delete szaltomb[i];
            delete [] szaltomb;
            return;
        }
    }
    if(i_stop==0)i_stop=row; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
if(mxMulMode==mxMinSize){
	cu32 n=src1.col;
	dbl * t0=t+i_start*col-i_start*(i_start-1)/2; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd * t1=src1.t+i_start*n;//n==src1.col,row==src1.row // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<i_stop;i++,t1+=n){ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd * t2=src2.t+i*n;//n==src1.col,col==src1.row
		for(u32 j=i;j<col;j++,t0++,t2+=n){
			dbl sum=t1[0]*t2[0];
			for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
			*t0+=sum;
		}
	}
}
else{
	cu32 ni=i_stop,nj=col,nk=src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 di=i_stop%4,dj=col%4,dk=src1.col%4; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 hi=ni-di,hj=nj-dj,hk=nk-dk;
    cu32 t_start=i_start*col-i_start*(i_start-1)/2; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	dbl *d0=t+t_start,*d1=t+t_start+(nj-i_start),*d2=t+t_start+2*(nj-i_start)-1,*d3=t+t_start+3*(nj-i_start)-3; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd *a0=src1.t+i_start*nk,*a1=src1.t+i_start*nk+nk,*a2=src1.t+i_start*nk+2*nk,*a3=src1.t+i_start*nk+3*nk; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<hi;i+=4){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd *b0=src2.t+nk*i,*b1=src2.t+nk+nk*i,*b2=src2.t+2*nk+nk*i,*b3=src2.t+3*nk+nk*i;
		d1-=1;d2-=2;d3-=3;	
		for(u32 k=0;k<hk;k+=4,a0+=4,a1+=4,a2+=4,a3+=4,b0+=4,b1+=4,b2+=4,b3+=4){
			d0[0]+=a0[0]*b0[0]+a0[1]*b0[1]+a0[2]*b0[2]+a0[3]*b0[3];
			d0[1]+=a0[0]*b1[0]+a0[1]*b1[1]+a0[2]*b1[2]+a0[3]*b1[3];
			d0[2]+=a0[0]*b2[0]+a0[1]*b2[1]+a0[2]*b2[2]+a0[3]*b2[3];
			d0[3]+=a0[0]*b3[0]+a0[1]*b3[1]+a0[2]*b3[2]+a0[3]*b3[3];

			d1[1]+=a1[0]*b1[0]+a1[1]*b1[1]+a1[2]*b1[2]+a1[3]*b1[3];
			d1[2]+=a1[0]*b2[0]+a1[1]*b2[1]+a1[2]*b2[2]+a1[3]*b2[3];
			d1[3]+=a1[0]*b3[0]+a1[1]*b3[1]+a1[2]*b3[2]+a1[3]*b3[3];

			d2[2]+=a2[0]*b2[0]+a2[1]*b2[1]+a2[2]*b2[2]+a2[3]*b2[3];
			d2[3]+=a2[0]*b3[0]+a2[1]*b3[1]+a2[2]*b3[2]+a2[3]*b3[3];

			d3[3]+=a3[0]*b3[0]+a3[1]*b3[1]+a3[2]*b3[2]+a3[3]*b3[3];
		}
		for(u32 k=0;k<dk;k++){
			d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];	d0[2]+=a0[k]*b2[k];	d0[3]+=a0[k]*b3[k];
								d1[1]+=a1[k]*b1[k];	d1[2]+=a1[k]*b2[k];	d1[3]+=a1[k]*b3[k];
													d2[2]+=a2[k]*b2[k];	d2[3]+=a2[k]*b3[k];
																		d3[3]+=a3[k]*b3[k];
		}
		d0+=4,d1+=4,d2+=4,d3+=4;
		a0-=hk,a1-=hk,a2-=hk,a3-=hk,b0+=dk+3*nk,b1+=dk+3*nk,b2+=dk+3*nk,b3+=dk+3*nk;
		for(u32 j=i+4;j<hj;j+=4){
			for(u32 k=0;k<hk;k+=4,a0+=4,a1+=4,a2+=4,a3+=4,b0+=4,b1+=4,b2+=4,b3+=4){
				d0[0]+=a0[0]*b0[0]+a0[1]*b0[1]+a0[2]*b0[2]+a0[3]*b0[3];
				d0[1]+=a0[0]*b1[0]+a0[1]*b1[1]+a0[2]*b1[2]+a0[3]*b1[3];
				d0[2]+=a0[0]*b2[0]+a0[1]*b2[1]+a0[2]*b2[2]+a0[3]*b2[3];
				d0[3]+=a0[0]*b3[0]+a0[1]*b3[1]+a0[2]*b3[2]+a0[3]*b3[3];

				d1[0]+=a1[0]*b0[0]+a1[1]*b0[1]+a1[2]*b0[2]+a1[3]*b0[3];
				d1[1]+=a1[0]*b1[0]+a1[1]*b1[1]+a1[2]*b1[2]+a1[3]*b1[3];
				d1[2]+=a1[0]*b2[0]+a1[1]*b2[1]+a1[2]*b2[2]+a1[3]*b2[3];
				d1[3]+=a1[0]*b3[0]+a1[1]*b3[1]+a1[2]*b3[2]+a1[3]*b3[3];

				d2[0]+=a2[0]*b0[0]+a2[1]*b0[1]+a2[2]*b0[2]+a2[3]*b0[3];
				d2[1]+=a2[0]*b1[0]+a2[1]*b1[1]+a2[2]*b1[2]+a2[3]*b1[3];
				d2[2]+=a2[0]*b2[0]+a2[1]*b2[1]+a2[2]*b2[2]+a2[3]*b2[3];
				d2[3]+=a2[0]*b3[0]+a2[1]*b3[1]+a2[2]*b3[2]+a2[3]*b3[3];

				d3[0]+=a3[0]*b0[0]+a3[1]*b0[1]+a3[2]*b0[2]+a3[3]*b0[3];
				d3[1]+=a3[0]*b1[0]+a3[1]*b1[1]+a3[2]*b1[2]+a3[3]*b1[3];
				d3[2]+=a3[0]*b2[0]+a3[1]*b2[1]+a3[2]*b2[2]+a3[3]*b2[3];
				d3[3]+=a3[0]*b3[0]+a3[1]*b3[1]+a3[2]*b3[2]+a3[3]*b3[3];
			}
			for(u32 k=0;k<dk;k++){
				d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];	d0[2]+=a0[k]*b2[k];	d0[3]+=a0[k]*b3[k];
				d1[0]+=a1[k]*b0[k];	d1[1]+=a1[k]*b1[k];	d1[2]+=a1[k]*b2[k];	d1[3]+=a1[k]*b3[k];
				d2[0]+=a2[k]*b0[k];	d2[1]+=a2[k]*b1[k];	d2[2]+=a2[k]*b2[k];	d2[3]+=a2[k]*b3[k];
				d3[0]+=a3[k]*b0[k];	d3[1]+=a3[k]*b1[k];	d3[2]+=a3[k]*b2[k];	d3[3]+=a3[k]*b3[k];
			}
			d0+=4,d1+=4,d2+=4,d3+=4;
			a0-=hk,a1-=hk,a2-=hk,a3-=hk,b0+=dk+3*nk,b1+=dk+3*nk,b2+=dk+3*nk,b3+=dk+3*nk;
		}
		for(u32 j=0;j<dj;j++){
			for(u32 k=0;k<nk;k++){
				d0[j]+=a0[k]*b0[j*nk+k]; d1[j]+=a1[k]*b0[j*nk+k]; d2[j]+=a2[k]*b0[j*nk+k]; d3[j]+=a3[k]*b0[j*nk+k];
			}
		}
		d0+=dj+3*nj-3*i-6; d1+=dj+3*nj-3*i-9; d2+=dj+3*nj-3*i-12; d3+=dj+3*nj-3*i-15;
		a0+=4*nk; a1+=4*nk; a2+=4*nk; a3+=4*nk;
	}
	const dbl *b0=src2.t+hj*nk;
	for(u32 i=0;i<di;i++)for(u32 j=i;j<dj;j++,d0++){
		for(u32 k=0;k<nk;k++)*d0+=a0[i*nk+k]*b0[j*nk+k];
	}
}
#ifdef PL_PROFILE
	GyujtStop(1);
#endif

}


//***********************************************************************
void dsrmatrix::tmul_vigyazz(const dsrmatrix & src1,const dsrmatrix & ssrc2,const bool adde,cu32 i_start,cu32 i_stop){
// �ltal�ban k�t symm szorzata NEM symm!!!
//***********************************************************************
#ifdef vsundebugmode
	if(&src1==this)throw hiba("","dsrmatrix::blokktmul => &src1==this");
	if(&ssrc2==this)throw hiba("","dsrmatrix::blokktmul => &src2==this");
	if(src1.col!=ssrc2.col)throw hiba("","dsrmatrix::blokktmul => src1.col!=ssrc2.col => %lu!=%lu",src1.col,ssrc2.col);
	if(adde){
		if(row!=src1.row)throw hiba("","dsrmatrix::tmul_vigyazz => row!=src1.row => %lu!=%lu",row,src1.row);
		if(col!=ssrc2.row)throw hiba("","dsrmatrix::tmul_vigyazz => col!=ssrc2.row => %lu!=%lu",col,ssrc2.row);
	}
#endif
#ifdef PL_PROFILE
	GyujtStart();
#endif

	// mxOptimize-t �s p�ratlans�got figyelni a m�reten k�v�l!

	if(!adde){resize(src1.row);zero();}
	if((mxOptimize!=mxMinSize) && (((src1.col|ssrc2.row)&1)==0) && 
		(STRASSEN_MUL_N_LIMIT()<=dbl(ssrc2.row)*(src1.row+1)/2*src1.col)){
		strassentmul(src1,ssrc2);
		return;
	}
	drmatrix src2;
	src2.convert(ssrc2);
if(mxMulMode==mxMinSize){
	cu32 n=src1.col;
	dbl * t0=t;
	dbl dum[NEWKIVALTO],*t1=n>NEWKIVALTO?new dbl[n]:dum;
	for(u32 i=0;i<row;i++){
		cd * t2=src2.t+i*n;//n==src1.col,col==src1.row
		src1.getLine(t1,i);
		for(u32 j=i;j<col;j++,t0++,t2+=n){
			dbl sum=t1[0]*t2[0];
			for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
			*t0+=sum;
		}
	}
	if(n>NEWKIVALTO)delete [] t1;
}

else{
	cu32 ni=row,nj=col,nk=src1.col;
	cu32 di=row%4,dj=col%4,dk=src1.col%4;
	cu32 hi=ni-di,hj=nj-dj,hk=nk-dk;
	dbl dum[4*NEWKIVALTO],*t1=nk>NEWKIVALTO?new dbl[4*nk]:dum;
	dbl *d0=t,*d1=t+nj,*d2=t+2*nj-1,*d3=t+3*nj-3;
	cd *a0=t1,*a1=t1+nk,*a2=t1+2*nk,*a3=t1+3*nk;
	for(u32 i=0;i<hi;i+=4){
		cd *b0=src2.t+nk*i,*b1=src2.t+nk+nk*i,*b2=src2.t+2*nk+nk*i,*b3=src2.t+3*nk+nk*i;
		src1.getLine(t1,     i);
		src1.getLine(t1+nk,  i+1);
		src1.getLine(t1+2*nk,i+2);
		src1.getLine(t1+3*nk,i+3);
		d1-=1;d2-=2;d3-=3;	
		for(u32 k=0;k<hk;k+=4,a0+=4,a1+=4,a2+=4,a3+=4,b0+=4,b1+=4,b2+=4,b3+=4){
			d0[0]+=a0[0]*b0[0]+a0[1]*b0[1]+a0[2]*b0[2]+a0[3]*b0[3];
			d0[1]+=a0[0]*b1[0]+a0[1]*b1[1]+a0[2]*b1[2]+a0[3]*b1[3];
			d0[2]+=a0[0]*b2[0]+a0[1]*b2[1]+a0[2]*b2[2]+a0[3]*b2[3];
			d0[3]+=a0[0]*b3[0]+a0[1]*b3[1]+a0[2]*b3[2]+a0[3]*b3[3];

			d1[1]+=a1[0]*b1[0]+a1[1]*b1[1]+a1[2]*b1[2]+a1[3]*b1[3];
			d1[2]+=a1[0]*b2[0]+a1[1]*b2[1]+a1[2]*b2[2]+a1[3]*b2[3];
			d1[3]+=a1[0]*b3[0]+a1[1]*b3[1]+a1[2]*b3[2]+a1[3]*b3[3];

			d2[2]+=a2[0]*b2[0]+a2[1]*b2[1]+a2[2]*b2[2]+a2[3]*b2[3];
			d2[3]+=a2[0]*b3[0]+a2[1]*b3[1]+a2[2]*b3[2]+a2[3]*b3[3];

			d3[3]+=a3[0]*b3[0]+a3[1]*b3[1]+a3[2]*b3[2]+a3[3]*b3[3];
		}
		for(u32 k=0;k<dk;k++){
			d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];	d0[2]+=a0[k]*b2[k];	d0[3]+=a0[k]*b3[k];
								d1[1]+=a1[k]*b1[k];	d1[2]+=a1[k]*b2[k];	d1[3]+=a1[k]*b3[k];
													d2[2]+=a2[k]*b2[k];	d2[3]+=a2[k]*b3[k];
																		d3[3]+=a3[k]*b3[k];
		}
		d0+=4,d1+=4,d2+=4,d3+=4;
		a0-=hk,a1-=hk,a2-=hk,a3-=hk,b0+=dk+3*nk,b1+=dk+3*nk,b2+=dk+3*nk,b3+=dk+3*nk;
		for(u32 j=i+4;j<hj;j+=4){
			for(u32 k=0;k<hk;k+=4,a0+=4,a1+=4,a2+=4,a3+=4,b0+=4,b1+=4,b2+=4,b3+=4){
				d0[0]+=a0[0]*b0[0]+a0[1]*b0[1]+a0[2]*b0[2]+a0[3]*b0[3];
				d0[1]+=a0[0]*b1[0]+a0[1]*b1[1]+a0[2]*b1[2]+a0[3]*b1[3];
				d0[2]+=a0[0]*b2[0]+a0[1]*b2[1]+a0[2]*b2[2]+a0[3]*b2[3];
				d0[3]+=a0[0]*b3[0]+a0[1]*b3[1]+a0[2]*b3[2]+a0[3]*b3[3];

				d1[0]+=a1[0]*b0[0]+a1[1]*b0[1]+a1[2]*b0[2]+a1[3]*b0[3];
				d1[1]+=a1[0]*b1[0]+a1[1]*b1[1]+a1[2]*b1[2]+a1[3]*b1[3];
				d1[2]+=a1[0]*b2[0]+a1[1]*b2[1]+a1[2]*b2[2]+a1[3]*b2[3];
				d1[3]+=a1[0]*b3[0]+a1[1]*b3[1]+a1[2]*b3[2]+a1[3]*b3[3];

				d2[0]+=a2[0]*b0[0]+a2[1]*b0[1]+a2[2]*b0[2]+a2[3]*b0[3];
				d2[1]+=a2[0]*b1[0]+a2[1]*b1[1]+a2[2]*b1[2]+a2[3]*b1[3];
				d2[2]+=a2[0]*b2[0]+a2[1]*b2[1]+a2[2]*b2[2]+a2[3]*b2[3];
				d2[3]+=a2[0]*b3[0]+a2[1]*b3[1]+a2[2]*b3[2]+a2[3]*b3[3];

				d3[0]+=a3[0]*b0[0]+a3[1]*b0[1]+a3[2]*b0[2]+a3[3]*b0[3];
				d3[1]+=a3[0]*b1[0]+a3[1]*b1[1]+a3[2]*b1[2]+a3[3]*b1[3];
				d3[2]+=a3[0]*b2[0]+a3[1]*b2[1]+a3[2]*b2[2]+a3[3]*b2[3];
				d3[3]+=a3[0]*b3[0]+a3[1]*b3[1]+a3[2]*b3[2]+a3[3]*b3[3];
			}
			for(u32 k=0;k<dk;k++){
				d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];	d0[2]+=a0[k]*b2[k];	d0[3]+=a0[k]*b3[k];
				d1[0]+=a1[k]*b0[k];	d1[1]+=a1[k]*b1[k];	d1[2]+=a1[k]*b2[k];	d1[3]+=a1[k]*b3[k];
				d2[0]+=a2[k]*b0[k];	d2[1]+=a2[k]*b1[k];	d2[2]+=a2[k]*b2[k];	d2[3]+=a2[k]*b3[k];
				d3[0]+=a3[k]*b0[k];	d3[1]+=a3[k]*b1[k];	d3[2]+=a3[k]*b2[k];	d3[3]+=a3[k]*b3[k];
			}
			d0+=4,d1+=4,d2+=4,d3+=4;
			a0-=hk,a1-=hk,a2-=hk,a3-=hk,b0+=dk+3*nk,b1+=dk+3*nk,b2+=dk+3*nk,b3+=dk+3*nk;
		}
		for(u32 j=0;j<dj;j++){
			for(u32 k=0;k<nk;k++){
				d0[j]+=a0[k]*b0[j*nk+k]; d1[j]+=a1[k]*b0[j*nk+k]; d2[j]+=a2[k]*b0[j*nk+k]; d3[j]+=a3[k]*b0[j*nk+k];
			}
		}
		d0+=dj+3*nj-3*i-6; d1+=dj+3*nj-3*i-9; d2+=dj+3*nj-3*i-12; d3+=dj+3*nj-3*i-15;
	}
	const dbl *b0=src2.t+hj*nk;
	for(u32 i=0;i<di;i++){
		src1.getLine(t1,hi+i);//a0=t1
		for(u32 j=i;j<dj;j++,d0++){
			for(u32 k=0;k<nk;k++)*d0+=a0[k]*b0[j*nk+k];
		}
	}
	if(nk>NEWKIVALTO)delete [] t1;
}
#ifdef PL_PROFILE
	GyujtStop(2);
#endif

}

//***********************************************************************
void drmatrix::strassentmul(const dsrmatrix & src1eredeti,const drmatrix & src2){
// val�j�ban += !!!
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","drmatrix::strassentmul => &src2==this");
	if(src1eredeti.col!=src2.col)throw hiba("","drmatrix::strassentmul => src1.col!=src2.col => %lu!=%lu",src1eredeti.col,src2.col);
	if((src1eredeti.col&1)||(src2.row&1))
		throw hiba("","drmatrix::strassentmul => src1.col=%lu, src1.row=%lu, src2.row=%lu => paratlan oldalhosz"
			  ,src1eredeti.col,src1eredeti.row,src2.row);
#endif
	parhuzamosanfut=true;
	drmatrix src1;
	src1.convert(src1eredeti); // csak egy blokkot vesz ki, ami az eredm�nyben a 0-�s lesz, ez�rt lent mindig 0-�s!
	switch(mxOptimize){
		case mxMaxSpeed :{ // 21 seg�dm�trix kell, ami 5,25 teljes eredm�nym�trix m�ret
				drmatrix a1122,b1122,a2122,b11,a11,b1222,a22,b1121,a1112,b22,a1121,b1112,a1222,b2122; // 14 db
				drmatrix q1,q2,q3,q4,q5,q6,q7; // 7 db

				a1122.submatrixadd(src1, 0, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				b1122.submatrixadd(src2, 0, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq1(a1122, b1122, q1);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a2122.submatrixadd(src1, src1.row/2, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				b11.getsubmatrix(src2, 0, 0, src2.row/2, src2.col/2);
				drmtmulszal mq2(a2122, b11, q2);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a11.getsubmatrix(src1, 0, 0, src1.row/2, src1.col/2);
				b1222.submatrixsub(src2, src2.row/2, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				drmtmulszal mq3(a11, b1222, q3);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a22.getsubmatrix(src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				b1121.submatrixsub(src2, 0, src2.col/2, src2, 0, 0, src2.row/2, src2.col/2);//csere
				drmtmulszal mq4(a22, b1121, q4);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a1112.submatrixadd(src1, 0, 0, src1, 0, src1.col/2, src1.row/2, src1.col/2);
				b22.getsubmatrix(src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq5(a1112, b22  , q5);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a1121.submatrixsub(src1, src1.row/2, 0, src1, 0, 0, src1.row/2, src1.col/2);
				b1112.submatrixadd(src2, 0, 0, src2, src2.row/2, 0, src2.row/2, src2.col/2);//csere
				drmtmulszal mq6(a1121, b1112, q6);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a1222.submatrixsub(src1, 0, src1.col/2, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);

				src1.free(); // ez a m�solat!!!

				b2122.submatrixadd(src2, 0, src2.col/2, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				drmtmulszal mq7(a1222, b2122, q7);	//ezekben csak 0-�s blokk van, mert submatrixok!

				mq1.start(); mq2.start(); mq3.start(); mq4.start(); mq5.start(); mq6.start(); mq7.start();
				mq1.wait();  mq2.wait();  mq3.wait();  mq4.wait();  mq5.wait();  mq6.wait();  mq7.wait();

				a1122.free(); b1122.free(); a2122.free(); b1222.free(); b1121.free(); a22.free();
				a1112.free(); a1121.free(); b1112.free(); a1222.free(); b2122.free(); b22.free(); b11.free();

				a11.addaddsubadd(q1,q4,q5,q7);
				setsubmatrixadd(a11,0,0);
				a11.add(q3,q5);
				setsubmatrixadd(a11,0,col/2);
				a11.add(q2,q4);
				setsubmatrixadd(a11,row/2,0);
				a11.addaddsubadd(q1,q3,q2,q6);
				setsubmatrixadd(a11,row/2,col/2);
			}
			break;
		case mxBalanced1 :{ // 12 seg�dm�trix kell, ami 3 teljes eredm�nym�trix m�ret
				drmatrix m1,m2,m3,m4,m5,m6; // 6 db
				drmatrix q1,q2,q3,q4,q5,q6; // 6 db

				m1.submatrixadd(src1, 0, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.submatrixadd(src2, 0, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq1(m1, m2, q1); //ezekben csak 0-�s blokk van, mert submatrixok!

				m3.getsubmatrix(src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m4.submatrixsub(src2, 0, src2.col/2, src2, 0, 0, src2.row/2, src2.col/2);//csere
				drmtmulszal mq4(m3, m4, q2);   //ezekben csak 0-�s blokk van, mert submatrixok!

				m5.submatrixadd(src1, 0, 0, src1, 0, src1.col/2, src1.row/2, src1.col/2);
				m6.getsubmatrix(src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq5(m5, m6, q3);   //ezekben csak 0-�s blokk van, mert submatrixok!

				q5.submatrixsub(src1, 0, src1.col/2, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				q6.submatrixadd(src2, 0, src2.col/2, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);// csere
				drmtmulszal mq7(q5, q6, q4); //ezekben csak 0-�s blokk van, mert submatrixok!

				mq1.start(); mq4.start(); mq5.start(); mq7.start();
				mq1.wait();  mq4.wait();  mq5.wait();  mq7.wait();

				m1.addaddsubadd(q1,q2,q3,q4);
				setsubmatrixadd(m1,0,0);

				m1.submatrixadd(src1, src1.row/2, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.getsubmatrix(src2, 0, 0, src2.row/2, src2.col/2);
				drmtmulszal mq2(m1, m2, q5);	//ezekben csak 0-�s blokk van, mert submatrixok!

				m3.getsubmatrix(src1, 0, 0, src1.row/2, src1.col/2);
				m4.submatrixsub(src2, src2.row/2, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				drmtmulszal mq3(m3, m4, q6);	//ezekben csak 0-�s blokk van, mert submatrixok!

				m5.submatrixsub(src1, src1.row/2, 0, src1, 0, 0, src1.row/2, src1.col/2);//a21,a11

				src1.free(); // ez a m�solat!!!

				m6.submatrixadd(src2, 0, 0, src2, src2.row/2, 0, src2.row/2, src2.col/2);//csere
				drmtmulszal mq6(m5, m6, q4);	//ezekben csak 0-�s blokk van, mert submatrixok!

				mq2.start(); mq3.start(); mq6.start();
				mq2.wait();  mq3.wait();  mq6.wait();

				m2.free(); m3.free(); m4.free(); m5.free(); m6.free();

				m1.add(q6,q3); // azaz Q3+Q5
				setsubmatrixadd(m1,0,col/2);
				m1.add(q5,q2); // azaz Q2+Q4
				setsubmatrixadd(m1,row/2,0);
				m1.addaddsubadd(q1,q6,q5,q4); // azaz Q1+Q3-Q2+Q6
				setsubmatrixadd(m1,row/2,col/2);
			}
			break;
		case mxBalanced2 :{ // 9 seg�dm�trix kell, ami 2,25 teljes eredm�nym�trix m�ret
				drmatrix m1,m2,m3,m4; // 4 db
				drmatrix q1,q2,q3,q4,q5; // 5 db
				
				m1.submatrixadd(src1, 0, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.submatrixadd(src2, 0, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq1(m1, m2, q1); //ezekben csak 0-�s blokk van, mert submatrixok!

				m3.getsubmatrix(src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m4.submatrixsub(src2, 0, src2.col/2, src2, 0, 0, src2.row/2, src2.col/2);//csere
				drmtmulszal mq4(m3, m4, q2);   //ezekben csak 0-�s blokk van, mert submatrixok!

				mq1.start(); mq4.start();
				mq1.wait();  mq4.wait();

				m1.submatrixadd(src1, 0, 0, src1, 0, src1.col/2, src1.row/2, src1.col/2);
				m2.getsubmatrix(src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq5(m1, m2, q3);   //ezekben csak 0-�s blokk van, mert submatrixok!

				m3.submatrixsub(src1, 0, src1.col/2, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m4.submatrixadd(src2, 0, src2.col/2, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				drmtmulszal mq7(m3, m4, q4); //ezekben csak 0-�s blokk van, mert submatrixok!

				mq5.start(); mq7.start();
				mq5.wait();  mq7.wait();

				m1.addaddsubadd(q1,q2,q3,q4);
				setsubmatrixadd(m1,0,0);

				m3.getsubmatrix(src1, 0, 0, src1.row/2, src1.col/2);
				m4.submatrixsub(src2, src2.row/2, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				drmtmulszal mq3(m3, m4, q4);	//ezekben csak 0-�s blokk van, mert submatrixok!

				m1.submatrixadd(src1, src1.row/2, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.getsubmatrix(src2, 0, 0, src2.row/2, src2.col/2);
				drmtmulszal mq2(m1, m2, q5);	//ezekben csak 0-�s blokk van, mert submatrixok!

				mq2.start(); mq3.start();
				mq2.wait();  mq3.wait();

				m3.free(); m4.free();

				m1.add(q4,q3); //Q3+Q5

				q3.free();
				
				setsubmatrixadd(m1,0,col/2);
				m1.add(q5,q2); // Q2+Q4
				setsubmatrixadd(m1,row/2,0);

				m1.submatrixsub(src1, src1.row/2, 0, src1, 0, 0, src1.row/2, src1.col/2);

				src1.free(); // ez a m�solat!!!

				m2.submatrixadd(src2, 0, 0, src2, src2.row/2, 0, src2.row/2, src2.col/2);//csere
				q2.tmul(m1,m2);

				m1.addaddsubadd(q1,q4,q5,q2); // azaz Q1+Q3-Q2+Q6
				setsubmatrixadd(m1,row/2,col/2);
			}
			break;
		case mxBalanced3 :{ // 6 seg�dm�trix kell, ami 1,5 teljes eredm�nym�trix m�ret
				drmatrix m1,m2; // 2 db
				drmatrix q1,q2,q3,q4; // 4 db
				
				m1.submatrixadd(src1, 0, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.submatrixadd(src2, 0, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq1(m1, m2, q1); //ezekben csak 0-�s blokk van, mert submatrixok!

				q3.getsubmatrix(src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				q4.submatrixsub(src2, 0, src2.col/2, src2, 0, 0, src2.row/2, src2.col/2);// csere
				drmtmulszal mq4(q3, q4, q2);   //ezekben csak 0-�s blokk van, mert submatrixok!

				mq1.start(); mq4.start();
				mq1.wait();  mq4.wait();

				m1.submatrixadd(src1, 0, 0, src1, 0, src1.col/2, src1.row/2, src1.col/2);
				m2.getsubmatrix(src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				q3.tmul(m1,m2);

				m1.submatrixsub(src1, 0, src1.col/2, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.submatrixadd(src2, 0, src2.col/2, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				q4.tmul(m1,m2);

				m1.addaddsubadd(q1,q2,q3,q4);
				setsubmatrixadd(m1,0,0);

				m1.getsubmatrix(src1, 0, 0, src1.row/2, src1.col/2);
				m2.submatrixsub(src2, src2.row/2, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				q4.tmul(m1,m2);

				m1.add(q4,q3); //Q3+Q5
				setsubmatrixadd(m1,0,col/2);

				m1.submatrixadd(src1, src1.row/2, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.getsubmatrix(src2, 0, 0, src2.row/2, src2.col/2);
				q3.tmul(m1,m2);

				m1.add(q3,q2); // Q2+Q4
				setsubmatrixadd(m1,row/2,0);

				m1.submatrixsub(src1, src1.row/2, 0, src1, 0, 0, src1.row/2, src1.col/2);

				src1.free(); // ez a m�solat!!!

				m2.submatrixadd(src2, 0, 0, src2, src2.row/2, 0, src2.row/2, src2.col/2);//csere
				q2.tmul(m1,m2);

				m1.addaddsubadd(q1,q4,q3,q2); // azaz Q1+Q3-Q2+Q6
				setsubmatrixadd(m1,row/2,col/2);
			}
			break;
		default:throw hiba("","drmatrix::strassentmul => mxOptimize==mxMinSize");
	}
}


//***********************************************************************
void dsrmatrix::strassentmul(const drmatrix & src1,const drmatrix & src2){
// val�j�ban += !!!
// Ez igaz�b�l nem strassen algoritmus, hanem az eredm�nym�trixot h�rom
// darabb�l rakjuk �ssze, hogy a negyediket ne kelljen kisz�molni
// [a1]�[b1]=[c11 c12], itt b transzpon�lt term�szetesen
// [a2] [b2] [    c22]
//***********************************************************************
#ifdef vsundebugmode
	if(src1.col!=src2.col)throw hiba("","dsrmatrix::strassentmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if(src1.row!=src2.row)throw hiba("","dsrmatrix::strassentmul => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
	if(src1.col&1)throw hiba("","dsrmatrix::strassentmul => src1.col=%lu, src1.row=%lu, src2.row=%lu => paratlan oldalhosz"
			  ,src1.col,src1.row,src2.row);
#endif
	parhuzamosanfut=true;
	switch(mxOptimize){
		case mxMaxSpeed :{//max helyfoglal�s k�tszeres
				drmatrix a1,a2,b1,b2,c12;
				dsrmatrix c11,c22;
				a1.getsubmatrix(src1,0         ,0,src1.row/2,src1.col);
				a2.getsubmatrix(src1,src1.row/2,0,src1.row/2,src1.col);
				b1.getsubmatrix(src2,0         ,0,src2.row/2,src2.col);
				b2.getsubmatrix(src2,src2.row/2,0,src2.row/2,src2.col);
				drmtmulszal fc11(a1,b1,c11);
				drmtmulszal fc12(a1,b2,c12);
				drmtmulszal fc22(a2,b2,c22);
				fc11.start(); fc12.start(); fc22.start();
				fc11.wait(); fc12.wait(); fc22.wait();
				a1.free(); a2.free(); b1.free(); b2.free();
				setsubmatrixadd(c11,0);
				c11.free();
				setsubmatrixadd(c12,0,col/2);
				c12.free();
				setsubmatrixadd(c22,row/2);
				c22.free();
			}
			break;
		case mxBalanced1 :
		case mxBalanced2 :{// max helyfoglal�s src k�tszeres, dest m�sf�lszeres
				drmatrix a1,a2,b1,b2,c12;
				dsrmatrix c11,c22;
				a1.getsubmatrix(src1,0         ,0,src1.row/2,src1.col);
				a2.getsubmatrix(src1,src1.row/2,0,src1.row/2,src1.col);
				b1.getsubmatrix(src2,0         ,0,src2.row/2,src2.col);
				b2.getsubmatrix(src2,src2.row/2,0,src2.row/2,src2.col);
				drmtmulszal fc11(a1,b1,c11);
				drmtmulszal fc22(a2,b2,c22);
				fc11.start(); fc22.start();
				fc11.wait(); fc22.wait();
				a2.free(); b1.free(); 
				setsubmatrixadd(c11,0);
				c11.free();
				setsubmatrixadd(c22,row/2);
				c22.free();

				c12.tmul(a1,b2);
				a1.free(); b2.free();
				setsubmatrixadd(c12,0,col/2);
				c12.free();
			}
			break;
		case mxBalanced3 :{// max helyfoglal�s src m�sf�lszeres, dest m�sf�lszeres
				drmatrix ai,bi,ci;
				dsrmatrix cii;
				
				ai.getsubmatrix(src1,0         ,0,src1.row/2,src1.col);
				bi.getsubmatrix(src2,0         ,0,src2.row/2,src2.col);//b1
				cii.tmul(ai,bi);
				setsubmatrixadd(cii,0);
				cii.free();
				
				bi.getsubmatrix(src2,src2.row/2,0,src2.row/2,src2.col);//b2
				ci.tmul(ai,bi);
				setsubmatrixadd(ci,0,col/2);
				ci.free();
				
				ai.getsubmatrix(src1,src1.row/2,0,src1.row/2,src1.col);//a2
				cii.tmul(ai,bi);
				setsubmatrixadd(cii,0);
				cii.free();
			}
			break;
		default:throw hiba("","drmatrix::strassentmul => mxOptimize==mxMinSize");
	}
}


//***********************************************************************
void dsrmatrix::strassentmul(const dsrmatrix & src1,const dsrmatrix & src2){
// val�j�ban += !!!
// Ez igaz�b�l nem strassen algoritmus, hanem az eredm�nym�trixot h�rom
// darabb�l rakjuk �ssze, hogy a negyediket ne kelljen kisz�molni
// [a1]�[b1]=[c11 c12], itt b transzpon�lt term�szetesen
// [a2] [b2] [    c22]
//***********************************************************************
#ifdef vsundebugmode
	if(src1.col!=src2.col)throw hiba("","dsrmatrix::strassentmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if(src1.row!=src2.row)throw hiba("","dsrmatrix::strassentmul => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
	if(src1.col&1)throw hiba("","dsrmatrix::strassentmul => src1.col=%lu, src1.row=%lu, src2.row=%lu => paratlan oldalhosz"
			  ,src1.col,src1.row,src2.row);
#endif
	parhuzamosanfut=true;
	switch(mxOptimize){
		case mxMaxSpeed :{//max helyfoglal�s k�tszeres
				drmatrix a1,a2,b1,b2,c12;
				dsrmatrix c11,c22;
				a1.getsubmatrix(src1,0         ,0,src1.row/2,src1.col);
				a2.getsubmatrix(src1,src1.row/2,0,src1.row/2,src1.col);
				b1.getsubmatrix(src2,0         ,0,src2.row/2,src2.col);
				b2.getsubmatrix(src2,src2.row/2,0,src2.row/2,src2.col);
				drmtmulszal fc11(a1,b1,c11);
				drmtmulszal fc12(a1,b2,c12);
				drmtmulszal fc22(a2,b2,c22);
				fc11.start(); fc12.start(); fc22.start();
				fc11.wait(); fc12.wait(); fc22.wait();
				a1.free(); a2.free(); b1.free(); b2.free();
				setsubmatrixadd(c11,0);
				c11.free();
				setsubmatrixadd(c12,0,col/2);
				c12.free();
				setsubmatrixadd(c22,row/2);
				c22.free();
			}
			break;
		case mxBalanced1 :
		case mxBalanced2 :{// max helyfoglal�s src k�tszeres, dest m�sf�lszeres
				drmatrix a1,a2,b1,b2,c12;
				dsrmatrix c11,c22;
				a1.getsubmatrix(src1,0         ,0,src1.row/2,src1.col);
				a2.getsubmatrix(src1,src1.row/2,0,src1.row/2,src1.col);
				b1.getsubmatrix(src2,0         ,0,src2.row/2,src2.col);
				b2.getsubmatrix(src2,src2.row/2,0,src2.row/2,src2.col);
				drmtmulszal fc11(a1,b1,c11);
				drmtmulszal fc22(a2,b2,c22);
				fc11.start(); fc22.start();
				fc11.wait(); fc22.wait();
				a2.free(); b1.free(); 
				setsubmatrixadd(c11,0);
				c11.free();
				setsubmatrixadd(c22,row/2);
				c22.free();

				c12.tmul(a1,b2);
				a1.free(); b2.free();
				setsubmatrixadd(c12,0,col/2);
				c12.free();
			}
			break;
		case mxBalanced3 :{// max helyfoglal�s src m�sf�lszeres, dest m�sf�lszeres
				drmatrix ai,bi,ci;
				dsrmatrix cii;
				
				ai.getsubmatrix(src1,0,0,src1.row/2,src1.col);//a1
				bi.getsubmatrix(src2,0,0,src2.row/2,src2.col);//b1
				cii.tmul(ai,bi);
				setsubmatrixadd(cii,0);
				cii.free();
				
				bi.getsubmatrix(src2,src2.row/2,0,src2.row/2,src2.col);//b2
				ci.tmul(ai,bi);
				setsubmatrixadd(ci,0,col/2);
				ci.free();
				
				ai.getsubmatrix(src1,src1.row/2,0,src1.row/2,src1.col);//a2
				cii.tmul(ai,bi);
				ai.free(); bi.free();
				setsubmatrixadd(cii,0);
				cii.free();
			}
			break;
		default:throw hiba("","drmatrix::strassentmul => mxOptimize==mxMinSize");
	}
}

//***********************************************************************
void dsrmatrix::ninv(){
//***********************************************************************
#ifdef vsundebugmode
		if(row!=col)throw hiba("","drmatrix::ninv => row!=col => %lu!=%lu",row,col);
#endif
#ifdef PL_PROFILE
	GyujtStart();
#endif
	cu32 S=row,SS=row*col;
	dbl * m=t;
	switch(S){
		case 0: return;
		case 1: m[0]=-1.0/m[0];return;
		case 2: {
					dbl * const p=m;
					cd det=-1.0/(p[0]*p[2]-p[1]*p[1]),ndet=-det,m3=p[0]*det;
					p[0]=p[2]*det;p[1]=p[1]*ndet;p[2]=m3;
					return;
				}
	}

#ifndef PL_PROFILE // ne fusson a strassen, ha profiler �zemm�dban vagyunk, mert az m�trixszorz�st h�v
	if((mxOptimize!=mxMinSize) && ((col&1)==0) && (STRASSEN_INV_N_LIMIT()<=dbl(row)*(row+1)*row)){
		strasseninv();
		return;
	}
#endif

	dbl dum[2*NEWKIVALTO],*a=m,*b0=S>NEWKIVALTO?new dbl[2*S]:dum,*bs=b0+S;
	u32 i,j,k,ii,jj,kk,ik;

	for(i=ii=0;i<S;ii+=S-i,i++){
		cd r=(fabs(a[ii])<1e-20) ? 1e+20 : 1.0/a[ii];
		for(k=0,ik=i;k<i;++k,ik+=S-k){	b0[k]=a[ik];	bs[k]=a[ik]*=r;}
										b0[k]=a[ik];	bs[k]=a[ik]=-r;
		for(k++,ik++;k<S;k++,ik++){		b0[k]=a[ik];	bs[k]=a[ik]*=r;}

		for(j=jj=0;j<i;jj+=S-j,j++){//j=0-t�l i-1-ig
			cd x=b0[j],x2=a[jj+i-j];
			for(k=j,kk=0;k<S;k++,kk++)a[jj+kk]-=x*bs[k];
			a[jj+i-j]=x2;
		}
		for(jj+=S-j,j++;j<S;jj+=S-j,j++){//j=i+1-t�l S-1-ig
			cd x=b0[j];
			for(k=j,kk=0;k<S;k++,kk++)a[jj+kk]-=x*bs[k];//k!=i mindig igaz, mert j=i+1-t�l indul a ciklus
		}
	}
	if(S>NEWKIVALTO)delete [] b0;
#ifdef PL_PROFILE
	GyujtStop(4);
#endif
}


//***********************************************************************
void dsrmatrix::strasseninv(){
// rem�lhet�leg ez is neg�l
//***********************************************************************
#ifdef vsundebugmode
	if(col&1)throw hiba("","dsrmatrix::strasseninv => col=%lu => paratlan oldalhossz",col);
#endif
	dsrmatrix s1,s4,s6;
	drmatrix m1,m2,m3;
	s1.getsubmatrix(*this,0,row/2);//a11
	s1.ninv(); //-R1
//printf("\n-R1=");s1.print();	
	m1.getsubmatrix(*this,0,col/2,row/2,col/2);//a12
	m2.transp(m1);
	m1.tmul(s1,m2);//-R3
//printf("\n-R3=");m1.print();
	m3.transp(m1);
	s4.tmul(m2,m3);//-R4
	m3.free();
//printf("\n-R4=");s4.print();	
	m2.free();
	s6.getsubmatrix(*this,row/2,row/2);//a22
	s6.addnr(s4,s6);//-R5
//printf("\n-R5=");s6.print();	
	s4.free();
	s6.ninv();//+R6
//printf("\nR6=");s6.print();	
	setsubmatrix(s6,row/2); // c22 be�r�sa
	m2.tmul(m1,s6);//-c21
//printf("\n-c12=");m2.print();	
	setsubmatrix(m2,0,col/2);// c12 be�r�sa
	s6.tmul(m1,m2);//+R7
//printf("\nR7=");s6.print();	
	m1.free();
	m2.free();
	s6.addnr(s1,s6);//-c11
	s1.free();
//printf("\n-c11=");s6.print();	
	setsubmatrix(s6,0);//c11 be�r�sa

}

//***********************************************************************
void dscmatrix::ninv(){
//***********************************************************************
	cu32 row=getrow(),col=getcol();
#ifdef vsundebugmode
		if(row!=col)throw hiba("","dscmatrix::ninv => row!=col => %lu!=%lu",row,col);
#endif
	cu32 S=row,SS=row*col;
	dbl * mr=re.gett();
	dbl * mi=im.gett();
	switch(S){
		case 0: return;
		case 1: separate(-1.0/dcomplex(mr[0],mi[0]),mr[0],mi[0]);return;
		case 2: {
					dbl * const pr=mr;
					dbl * const pi=mi;
					cdc det=-1.0/(dcomplex(pr[0],pi[0])*dcomplex(pr[2],pi[2])-dcomplex(pr[1],pi[1])*dcomplex(pr[1],pi[1]));
					cdc ndet=-det,m3=dcomplex(pr[0],pi[0])*det;
					separate(dcomplex(pr[2],pi[2])*det,pr[0],pi[0]);
					separate(dcomplex(pr[1],pi[1])*ndet,pr[1],pi[1]);
					separate(m3,pr[2],pi[2]);
					return;
				}
	}

	if((mxOptimize!=mxMinSize) && ((col&1)==0) && (STRASSEN_INV_N_LIMIT()<=dbl(row)*(row+1)*row)){
		strasseninv();
		return;
	}

	dbl dum[4*NEWKIVALTO],*ar=mr,*ai=mi,*br0=S>NEWKIVALTO?new dbl[4*S]:dum,*brs=br0+S,*bi0=br0+2*S,*bis=br0+3*S;
	u32 i,j,k,ii,jj,kk,ik;

	for(i=ii=0;i<S;ii+=S-i,i++){
		cdc r=(fabs(ar[ii])+fabs(ai[ii])<1e-20) ? 1e+20 : 1.0/dcomplex(ar[ii],ai[ii]);
		for(k=0,ik=i;k<i;++k,ik+=S-k){	
			br0[k]=ar[ik];	bi0[k]=ai[ik];	
			separate(dcomplex(ar[ik],ai[ik])*r,ar[ik],ai[ik]);	
			brs[k]=ar[ik];	bis[k]=ai[ik];
		}
		br0[k]=ar[ik];	bi0[k]=ai[ik];	separate(-r,ar[ik],ai[ik]);	brs[k]=ar[ik];	bis[k]=ai[ik];
		for(k++,ik++;k<S;k++,ik++){		
			br0[k]=ar[ik];	bi0[k]=ai[ik];	
			separate(dcomplex(ar[ik],ai[ik])*r,ar[ik],ai[ik]);	
			brs[k]=ar[ik];	bis[k]=ai[ik];
		}

		for(j=jj=0;j<i;jj+=S-j,j++){//j=0-t�l i-1-ig
			cdc x=dcomplex(br0[j],bi0[j]),x2=dcomplex(ar[jj+i-j],ai[jj+i-j]);
			for(k=j,kk=0;k<S;k++,kk++)
				separate(dcomplex(ar[jj+kk],ai[jj+kk])-x*dcomplex(brs[k],bis[k]),ar[jj+kk],ai[jj+kk]);
			separate(x2,ar[jj+i-j],ai[jj+i-j]);
		}
		for(jj+=S-j,j++;j<S;jj+=S-j,j++){//j=i+1-t�l S-1-ig
			cdc x=dcomplex(br0[j],bi0[j]);
			for(k=j,kk=0;k<S;k++,kk++)
				separate(dcomplex(ar[jj+kk],ai[jj+kk])-x*dcomplex(brs[k],bis[k]),ar[jj+kk],ai[jj+kk]);//k!=i mindig igaz, mert j=i+1-t�l indul a ciklus
		}
	}
	if(S>NEWKIVALTO)delete [] br0;
}


//***********************************************************************
void dscmatrix::strasseninv(){
// rem�lhet�leg ez is neg�l
//***********************************************************************
	cu32 row=getrow(),col=getcol();
#ifdef vsundebugmode
	if(col&1)throw hiba("","dscmatrix::strasseninv => col=%lu => paratlan oldalhossz",col);
#endif
	dscmatrix s1,s4,s6;
	dcmatrix m1,m2,m3;
	s1.getsubmatrix(*this,0,row/2);//a11
	s1.ninv(); //-R1

	m1.getsubmatrix(*this,0,col/2,row/2,col/2);//a12
	m2.transp(m1);
	m1.tmul(s1,m2);//-R3

	m3.transp(m1);
	s4.tmul(m2,m3);//-R4
	m3.free();

	m2.free();
	s6.getsubmatrix(*this,row/2,row/2);//a22
	s6.addnr(s4,s6);//-R5

	s4.free();
	s6.ninv();//+R6

	setsubmatrix(s6,row/2); // c22 be�r�sa
	m2.tmul(m1,s6);//-c21

	setsubmatrix(m2,0,col/2);// c12 be�r�sa
	s6.tmul(m1,m2);//+R7

	m1.free();
	m2.free();
	s6.addnr(s1,s6);//-c11
	s1.free();

	setsubmatrix(s6,0);//c11 be�r�sa
}

#endif
