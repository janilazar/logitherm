//***********************************************************************
// h�l�felt�lt� �s futtat� g�p source
// Creation date:  2009. 08. 07.
// Creator:        Pohl L�szl�
//***********************************************************************


//***********************************************************************
#include "masina.h"
#include "gfunc.h"
#include "drcella.h"
#include "dccella.h"
#include "qrcella.h"
#include "qccella.h"
#include "drhalozat.h"
#include "qrhalozat.h"
#include "dchalozat.h"
#include "qchalozat.h"
//***********************************************************************


//***********************************************************************
masina::~masina(){
//***********************************************************************
    del_all();
}


//***********************************************************************
void masina::del_all(){
//***********************************************************************
    for(uns i=0;i<t_dr.size();i++)delete t_dr[i];       t_dr.clear();
    for(uns i=0;i<t_dc.size();i++)delete t_dc[i];       t_dc.clear();
    for(uns i=0;i<t_qr.size();i++)delete t_qr[i];       t_qr.clear();
    for(uns i=0;i<t_qc.size();i++)delete t_qc[i];       t_qc.clear();
    for(uns i=0;i<t_drmap.size();i++)delete t_drmap[i]; t_drmap.clear();
    for(uns i=0;i<t_dcmap.size();i++)delete t_dcmap[i]; t_dcmap.clear();
}


//***********************************************************************
void masina::del_azon(){
//***********************************************************************
    if(!par.is_ac && !par.is_quad){if(t_dr.size()>par.azon){delete t_dr[par.azon]; t_dr[par.azon]=NULL;}}
    if( par.is_ac && !par.is_quad){if(t_dc.size()>par.azon){delete t_dc[par.azon]; t_dc[par.azon]=NULL;}}
    if(!par.is_ac &&  par.is_quad){if(t_qr.size()>par.azon){delete t_qr[par.azon]; t_qr[par.azon]=NULL;}}
    if( par.is_ac &&  par.is_quad){if(t_qc.size()>par.azon){delete t_qc[par.azon]; t_qc[par.azon]=NULL;}}
}


//***********************************************************************
void masina::foglal_halot(uns step){
//***********************************************************************
      setAnalKeszStep(0,step,"alloc grid");
    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    switch(melyik){
        case 0: foglal_halot_dr(); break;
        case 1: foglal_halot_dc(); break;
        case 2: foglal_halot_qr(); break;
        case 3: foglal_halot_qc(); break;
    }
}


//***********************************************************************
void coupled_model::set_externals(uns x0, uns y0, uns z0, drcella ** p, const model & mod)const{
// Az �sszes cell�ban be�ll�tja, hogy mely csom�pontok externalok,
// �s ezeknek mennyi az azonos�t�ja
//***********************************************************************
    for(uns i=0; i<compmod.tfootpr.size(); i++){
        for(uns iy=0; iy<compmod.tfootpr[i].h; iy++)
            for(uns ix=0; ix<compmod.tfootpr[i].w; ix++){

                // cellaindex megkeres�se

                uns x=0,y=0,z=0;
                dbl Ax,Ay;
                switch(oldal){
                    case WEST:
                    case EAST:
                        x = this->x ;
                        y = this->y + compmod.tfootpr[i].x + ix ;
                        z = this->z + compmod.tfootpr[i].y + iy ;
                        Ax = y<y0 ? mod.y_pit[y].get(nulla) : 0.0;
                        Ay = z<z0 ? mod.z_pit[z].get(mod.x_hossz[2*x]) : 0.0;
                        break;
                    case SOUTH:
                    case NORTH:
                        x = this->x + compmod.tfootpr[i].x + ix ;
                        y = this->y ;
                        z = this->z + compmod.tfootpr[i].y + iy ;
                        Ax = x<x0 ? mod.x_pit[x].get(nulla) : 0.0;
                        Ay = z<z0 ? mod.z_pit[z].get(mod.x_hossz[2*x]) : 0.0;
                        break;
                    case BOTTOM:
                    case TOP:
                        x = this->x + compmod.tfootpr[i].x + ix ;
                        y = this->y + compmod.tfootpr[i].y + iy ;
                        z = this->z;
                        Ax = x<x0 ? mod.x_pit[x].get(nulla) : 0.0;
                        Ay = y<y0 ? mod.y_pit[y].get(nulla) : 0.0;
                        break;
                    default:
                        throw hiba("coupled_model::set_externals","unknown side to external");
                }

                // external be�ll�t�sa

                if(x<x0 && y<y0 && z<z0){
                    uns n=x0*y0*z+x0*y+x;
                    if(p[n]!=NULL){
                        p[n]->get_d()->external_nulladik[oldal]=true;
                        p[n]->get_d()->texternal_azon.resize(BASIC_SIDES);
                        p[n]->get_d()->texternal_area.resize(BASIC_SIDES);
                        p[n]->get_d()->texternal_azon[oldal]=compmod.tfootpr[i].external_azon;
                        p[n]->get_d()->texternal_area[oldal]=Ax*Ay;
                    }
                }
            }
    }
}


//***********************************************************************
void coupled_model::set_externals(uns x0, uns y0, uns z0, dccella ** p, const model & mod)const{
// Az �sszes cell�ban be�ll�tja, hogy mely csom�pontok externalok,
// �s ezeknek mennyi az azonos�t�ja
//***********************************************************************
    for(uns i=0; i<compmod.tfootpr.size(); i++){
        for(uns iy=0; iy<compmod.tfootpr[i].h; iy++)
            for(uns ix=0; ix<compmod.tfootpr[i].w; ix++){

                // cellaindex megkeres�se

                uns x=0,y=0,z=0;
                dbl Ax,Ay;
                switch(oldal){
                    case WEST:
                    case EAST:
                        x = this->x ;
                        y = this->y + compmod.tfootpr[i].x + ix ;
                        z = this->z + compmod.tfootpr[i].y + iy ;
                        Ax = y<y0 ? mod.y_pit[y].get(nulla) : 0.0;
                        Ay = z<z0 ? mod.z_pit[z].get(mod.x_hossz[2*x]) : 0.0;
                        break;
                    case SOUTH:
                    case NORTH:
                        x = this->x + compmod.tfootpr[i].x + ix ;
                        y = this->y ;
                        z = this->z + compmod.tfootpr[i].y + iy ;
                        Ax = x<x0 ? mod.x_pit[x].get(nulla) : 0.0;
                        Ay = z<z0 ? mod.z_pit[z].get(mod.x_hossz[2*x]) : 0.0;
                        break;
                    case BOTTOM:
                    case TOP:
                        x = this->x + compmod.tfootpr[i].x + ix ;
                        y = this->y + compmod.tfootpr[i].y + iy ;
                        z = this->z;
                        Ax = x<x0 ? mod.x_pit[x].get(nulla) : 0.0;
                        Ay = y<y0 ? mod.y_pit[y].get(nulla) : 0.0;
                        break;
                    default:
                        throw hiba("coupled_model::set_externals","unknown side to external");
                }

                // external be�ll�t�sa

                if(x<x0 && y<y0 && z<z0){
                    uns n=x0*y0*z+x0*y+x;
                    if(p[n]!=NULL){
                        p[n]->get_d()->external_nulladik[oldal]=true;
                        p[n]->get_d()->texternal_azon.resize(BASIC_SIDES);
                        p[n]->get_d()->texternal_area.resize(BASIC_SIDES);
                        p[n]->get_d()->texternal_azon[oldal]=compmod.tfootpr[i].external_azon;
                        p[n]->get_d()->texternal_area[oldal]=Ax*Ay;
                    }
                }
            }
    }
}


//***********************************************************************
void coupled_model::set_externals(uns x0, uns y0, uns z0, qrcella ** p, const model & mod)const{
// Az �sszes cell�ban be�ll�tja, hogy mely csom�pontok externalok,
// �s ezeknek mennyi az azonos�t�ja
//***********************************************************************
    for(uns i=0; i<compmod.tfootpr.size(); i++){
        for(uns iy=0; iy<compmod.tfootpr[i].h; iy++)
            for(uns ix=0; ix<compmod.tfootpr[i].w; ix++){

                // cellaindex megkeres�se

                uns x=0,y=0,z=0;
                dbl Ax,Ay;
                switch(oldal){
                    case WEST:
                    case EAST:
                        x = this->x ;
                        y = this->y + compmod.tfootpr[i].x + ix ;
                        z = this->z + compmod.tfootpr[i].y + iy ;
                        Ax = y<y0 ? mod.y_pit[y].get(nulla) : 0.0;
                        Ay = z<z0 ? mod.z_pit[z].get(mod.x_hossz[2*x]) : 0.0;
                        break;
                    case SOUTH:
                    case NORTH:
                        x = this->x + compmod.tfootpr[i].x + ix ;
                        y = this->y ;
                        z = this->z + compmod.tfootpr[i].y + iy ;
                        Ax = x<x0 ? mod.x_pit[x].get(nulla) : 0.0;
                        Ay = z<z0 ? mod.z_pit[z].get(mod.x_hossz[2*x]) : 0.0;
                        break;
                    case BOTTOM:
                    case TOP:
                        x = this->x + compmod.tfootpr[i].x + ix ;
                        y = this->y + compmod.tfootpr[i].y + iy ;
                        z = this->z;
                        Ax = x<x0 ? mod.x_pit[x].get(nulla) : 0.0;
                        Ay = y<y0 ? mod.y_pit[y].get(nulla) : 0.0;
                        break;
                    default:
                        throw hiba("coupled_model::set_externals","unknown side to external");
                }

                // external be�ll�t�sa

                if(x<x0 && y<y0 && z<z0){
                    uns n=x0*y0*z+x0*y+x;
                    if(p[n]!=NULL){
                        p[n]->get_d()->external_nulladik[oldal]=true;
                        p[n]->get_d()->texternal_azon.resize(BASIC_SIDES);
                        p[n]->get_d()->texternal_area.resize(BASIC_SIDES);
                        p[n]->get_d()->texternal_azon[oldal]=compmod.tfootpr[i].external_azon;
                        p[n]->get_d()->texternal_area[oldal]=Ax*Ay;
                    }
                }
            }
    }
}


//***********************************************************************
void coupled_model::set_externals(uns x0, uns y0, uns z0, qccella ** p, const model & mod)const{
// Az �sszes cell�ban be�ll�tja, hogy mely csom�pontok externalok,
// �s ezeknek mennyi az azonos�t�ja
//***********************************************************************
    for(uns i=0; i<compmod.tfootpr.size(); i++){
        for(uns iy=0; iy<compmod.tfootpr[i].h; iy++)
            for(uns ix=0; ix<compmod.tfootpr[i].w; ix++){

                // cellaindex megkeres�se

                uns x=0,y=0,z=0;
                dbl Ax,Ay;
                switch(oldal){
                    case WEST:
                    case EAST:
                        x = this->x ;
                        y = this->y + compmod.tfootpr[i].x + ix ;
                        z = this->z + compmod.tfootpr[i].y + iy ;
                        Ax = y<y0 ? mod.y_pit[y].get(nulla) : 0.0;
                        Ay = z<z0 ? mod.z_pit[z].get(mod.x_hossz[2*x]) : 0.0;
                        break;
                    case SOUTH:
                    case NORTH:
                        x = this->x + compmod.tfootpr[i].x + ix ;
                        y = this->y ;
                        z = this->z + compmod.tfootpr[i].y + iy ;
                        Ax = x<x0 ? mod.x_pit[x].get(nulla) : 0.0;
                        Ay = z<z0 ? mod.z_pit[z].get(mod.x_hossz[2*x]) : 0.0;
                        break;
                    case BOTTOM:
                    case TOP:
                        x = this->x + compmod.tfootpr[i].x + ix ;
                        y = this->y + compmod.tfootpr[i].y + iy ;
                        z = this->z;
                        Ax = x<x0 ? mod.x_pit[x].get(nulla) : 0.0;
                        Ay = y<y0 ? mod.y_pit[y].get(nulla) : 0.0;
                        break;
                    default:
                        throw hiba("coupled_model::set_externals","unknown side to external");
                }

                // external be�ll�t�sa

                if(x<x0 && y<y0 && z<z0){
                    uns n=x0*y0*z+x0*y+x;
                    if(p[n]!=NULL){
                        p[n]->get_d()->external_nulladik[oldal]=true;
                        p[n]->get_d()->texternal_azon.resize(BASIC_SIDES);
                        p[n]->get_d()->texternal_area.resize(BASIC_SIDES);
                        p[n]->get_d()->texternal_azon[oldal]=compmod.tfootpr[i].external_azon;
                        p[n]->get_d()->texternal_area[oldal]=Ax*Ay;
                    }
                }
            }
    }
}


//***********************************************************************
void masina::foglal_halot_dr(){
//***********************************************************************
    drhalozat *pdr=NULL;
    drcella ** hpdr=NULL;
    cuns x0=par.sim->pmodel->x_res, y0=par.sim->pmodel->y_res, z0=par.sim->pmodel->z_res;
    
    // h�l�zat lefoglal�sa

    cuns n0=t_dr.size();
    if(par.azon>=n0){ 
        t_dr.resize(par.azon+1);  
        for(uns i=n0;i<t_dr.size();i++)t_dr[i]=NULL; 
    }
    delete t_dr[par.azon]; 
    t_dr[par.azon]=NULL;
    pdr=t_dr[par.azon]=new drhalozat(x0,y0,z0);
    if(t_dr[par.azon]==NULL)throw hiba("masina::foglal_halot","alloc failed");
    hpdr=pdr->get0()->gethalo();

    // AllocStep0cells
    // vecdim=1, extended=false

    uns N=0;
    const model & mod=*par.sim->pmodel;
    for(uns i=0;i<z0;i++)for(uns j=0;j<y0;j++)for(uns k=0;k<x0;k++,N++)if(mod.tcolor[mod.tbmp[i].getpixel_also(k,j)].tipus==SzinNormal){
        hpdr[N]=new drcella(false);
    }
    for(uns i=0; i<mod.tcoupled.size(); i++)
        mod.tcoupled[i].set_externals(x0,y0,z0,hpdr,mod);
    N=0;
    for(uns i=0;i<z0;i++)for(uns j=0;j<y0;j++)for(uns k=0;k<x0;k++,N++)if(mod.tcolor[mod.tbmp[i].getpixel_also(k,j)].tipus==SzinNormal){
        u16 * olda=hpdr[N]->get_oldalak();
        bool * is_external=hpdr[N]->get_d()->external_nulladik, is_perem;
        u16 n=olda[EXTERNAL]=0;
        n+=olda[WEST]   = ( is_perem = ( k==0    || mod.tcolor[mod.tbmp[i].getpixel_also(k-1,j)].tipus!=SzinNormal) ) && !is_external[WEST]   ? 0 : 1; is_external[WEST]   = is_external[WEST]   && is_perem;
        n+=olda[EAST]   = ( is_perem = ( k==x0-1 || mod.tcolor[mod.tbmp[i].getpixel_also(k+1,j)].tipus!=SzinNormal) ) && !is_external[EAST]   ? 0 : 1; is_external[EAST]   = is_external[EAST]   && is_perem;
        n+=olda[SOUTH]  = ( is_perem = ( j==0    || mod.tcolor[mod.tbmp[i].getpixel_also(k,j-1)].tipus!=SzinNormal) ) && !is_external[SOUTH]  ? 0 : 1; is_external[SOUTH]  = is_external[SOUTH]  && is_perem;
        n+=olda[NORTH]  = ( is_perem = ( j==y0-1 || mod.tcolor[mod.tbmp[i].getpixel_also(k,j+1)].tipus!=SzinNormal) ) && !is_external[NORTH]  ? 0 : 1; is_external[NORTH]  = is_external[NORTH]  && is_perem;
        n+=olda[BOTTOM] = ( is_perem = ( i==0    || mod.tcolor[mod.tbmp[i-1].getpixel_also(k,j)].tipus!=SzinNormal) ) && !is_external[BOTTOM] ? 0 : 1; is_external[BOTTOM] = is_external[BOTTOM] && is_perem;
        n+=olda[TOP]    = ( is_perem = ( i==z0-1 || mod.tcolor[mod.tbmp[i+1].getpixel_also(k,j)].tipus!=SzinNormal) ) && !is_external[TOP]    ? 0 : 1; is_external[TOP]    = is_external[TOP]    && is_perem;
        hpdr[N]->set_nulladik_kulso_oldalszam(n);
        olda[CENTER]=1;
    }

    // Alloc V0

    pdr->get0()->AllocV();
}


//***********************************************************************
void masina::foglal_halot_dc(){
//***********************************************************************
    dchalozat *pdc=NULL;
    dccella ** hpdc=NULL;
    cuns x0=par.sim->pmodel->x_res, y0=par.sim->pmodel->y_res, z0=par.sim->pmodel->z_res;
    
    // h�l�zat lefoglal�sa

    cuns n0=t_dc.size();
    if(par.azon>=n0){ 
        t_dc.resize(par.azon+1);  
        for(uns i=n0;i<t_dc.size();i++)t_dc[i]=NULL; 
    }
    delete t_dc[par.azon]; 
    t_dc[par.azon]=NULL;
    pdc=t_dc[par.azon]=new dchalozat(x0,y0,z0);
    if(t_dc[par.azon]==NULL)throw hiba("masina::foglal_halot","alloc failed");
    hpdc=pdc->get0()->gethalo();

    // AllocStep0cells
    // vecdim=1, extended=false

    uns N=0;
    const model & mod=*par.sim->pmodel;
    for(uns i=0;i<z0;i++)for(uns j=0;j<y0;j++)for(uns k=0;k<x0;k++,N++)if(mod.tcolor[mod.tbmp[i].getpixel_also(k,j)].tipus==SzinNormal){
        hpdc[N]=new dccella(false); 
    }
    for(uns i=0; i<mod.tcoupled.size(); i++)
        mod.tcoupled[i].set_externals(x0,y0,z0,hpdc,mod);
    N=0;
    for(uns i=0;i<z0;i++)for(uns j=0;j<y0;j++)for(uns k=0;k<x0;k++,N++)if(mod.tcolor[mod.tbmp[i].getpixel_also(k,j)].tipus==SzinNormal){
        u16 * olda=hpdc[N]->get_oldalak();
        bool * is_external=hpdc[N]->get_d()->external_nulladik, is_perem;
        u16 n=olda[EXTERNAL]=0;
        n+=olda[WEST]   = ( is_perem = ( k==0    || mod.tcolor[mod.tbmp[i].getpixel_also(k-1,j)].tipus!=SzinNormal) ) && !is_external[WEST]   ? 0 : 1; is_external[WEST]   = is_external[WEST]   && is_perem;
        n+=olda[EAST]   = ( is_perem = ( k==x0-1 || mod.tcolor[mod.tbmp[i].getpixel_also(k+1,j)].tipus!=SzinNormal) ) && !is_external[EAST]   ? 0 : 1; is_external[EAST]   = is_external[EAST]   && is_perem;
        n+=olda[SOUTH]  = ( is_perem = ( j==0    || mod.tcolor[mod.tbmp[i].getpixel_also(k,j-1)].tipus!=SzinNormal) ) && !is_external[SOUTH]  ? 0 : 1; is_external[SOUTH]  = is_external[SOUTH]  && is_perem;
        n+=olda[NORTH]  = ( is_perem = ( j==y0-1 || mod.tcolor[mod.tbmp[i].getpixel_also(k,j+1)].tipus!=SzinNormal) ) && !is_external[NORTH]  ? 0 : 1; is_external[NORTH]  = is_external[NORTH]  && is_perem;
        n+=olda[BOTTOM] = ( is_perem = ( i==0    || mod.tcolor[mod.tbmp[i-1].getpixel_also(k,j)].tipus!=SzinNormal) ) && !is_external[BOTTOM] ? 0 : 1; is_external[BOTTOM] = is_external[BOTTOM] && is_perem;
        n+=olda[TOP]    = ( is_perem = ( i==z0-1 || mod.tcolor[mod.tbmp[i+1].getpixel_also(k,j)].tipus!=SzinNormal) ) && !is_external[TOP]    ? 0 : 1; is_external[TOP]    = is_external[TOP]    && is_perem;
        hpdc[N]->set_nulladik_kulso_oldalszam(n);
        olda[CENTER]=1;
    }

    // Alloc V0

    pdc->get0()->AllocV();
}


//***********************************************************************
void masina::foglal_halot_qr(){
//***********************************************************************

    qrhalozat *pqr=NULL;
    qrcella ** hpqr=NULL;
    cuns x0=par.sim->pmodel->x_res, y0=par.sim->pmodel->y_res, z0=par.sim->pmodel->z_res;
    
    // h�l�zat lefoglal�sa

    cuns n0=t_qr.size();
    if(par.azon>=n0){ 
        t_qr.resize(par.azon+1);  
        for(uns i=n0;i<t_qr.size();i++)t_qr[i]=NULL; 
    }
    delete t_qr[par.azon]; 
    t_qr[par.azon]=NULL;
    pqr=t_qr[par.azon]=new qrhalozat(x0,y0,z0);
    if(t_qr[par.azon]==NULL)throw hiba("masina::foglal_halot","alloc failed");
    hpqr=pqr->get0()->gethalo();

    // AllocStep0cells
    // vecdim=1, extended=false

    uns N=0;
    const model & mod=*par.sim->pmodel;
    for(uns i=0;i<z0;i++)for(uns j=0;j<y0;j++)for(uns k=0;k<x0;k++,N++)if(mod.tcolor[mod.tbmp[i].getpixel_also(k,j)].tipus==SzinNormal){
        hpqr[N]=new qrcella(false); 
    }
    for(uns i=0; i<mod.tcoupled.size(); i++)
        mod.tcoupled[i].set_externals(x0,y0,z0,hpqr,mod);
    N=0;
    for(uns i=0;i<z0;i++)for(uns j=0;j<y0;j++)for(uns k=0;k<x0;k++,N++)if(mod.tcolor[mod.tbmp[i].getpixel_also(k,j)].tipus==SzinNormal){
        u16 * olda=hpqr[N]->get_oldalak();
        bool * is_external=hpqr[N]->get_d()->external_nulladik, is_perem;
        u16 n=olda[EXTERNAL]=0;
        n+=olda[WEST]   = ( is_perem = ( k==0    || mod.tcolor[mod.tbmp[i].getpixel_also(k-1,j)].tipus!=SzinNormal) ) && !is_external[WEST]   ? 0 : 1; is_external[WEST]   = is_external[WEST]   && is_perem;
        n+=olda[EAST]   = ( is_perem = ( k==x0-1 || mod.tcolor[mod.tbmp[i].getpixel_also(k+1,j)].tipus!=SzinNormal) ) && !is_external[EAST]   ? 0 : 1; is_external[EAST]   = is_external[EAST]   && is_perem;
        n+=olda[SOUTH]  = ( is_perem = ( j==0    || mod.tcolor[mod.tbmp[i].getpixel_also(k,j-1)].tipus!=SzinNormal) ) && !is_external[SOUTH]  ? 0 : 1; is_external[SOUTH]  = is_external[SOUTH]  && is_perem;
        n+=olda[NORTH]  = ( is_perem = ( j==y0-1 || mod.tcolor[mod.tbmp[i].getpixel_also(k,j+1)].tipus!=SzinNormal) ) && !is_external[NORTH]  ? 0 : 1; is_external[NORTH]  = is_external[NORTH]  && is_perem;
        n+=olda[BOTTOM] = ( is_perem = ( i==0    || mod.tcolor[mod.tbmp[i-1].getpixel_also(k,j)].tipus!=SzinNormal) ) && !is_external[BOTTOM] ? 0 : 1; is_external[BOTTOM] = is_external[BOTTOM] && is_perem;
        n+=olda[TOP]    = ( is_perem = ( i==z0-1 || mod.tcolor[mod.tbmp[i+1].getpixel_also(k,j)].tipus!=SzinNormal) ) && !is_external[TOP]    ? 0 : 1; is_external[TOP]    = is_external[TOP]    && is_perem;
        hpqr[N]->set_nulladik_kulso_oldalszam(n);
        olda[CENTER]=1;
    }

    // Alloc V0

    pqr->get0()->AllocV();
}


//***********************************************************************
void masina::foglal_halot_qc(){
//***********************************************************************

    qchalozat *pqc=NULL;
    qccella ** hpqc=NULL;
    cuns x0=par.sim->pmodel->x_res, y0=par.sim->pmodel->y_res, z0=par.sim->pmodel->z_res;
    
    // h�l�zat lefoglal�sa

    cuns n0=t_qc.size();
    if(par.azon>=n0){ 
        t_qc.resize(par.azon+1);  
        for(uns i=n0;i<t_qc.size();i++)t_qc[i]=NULL; 
    }
    delete t_qc[par.azon]; 
    t_qc[par.azon]=NULL;
    pqc=t_qc[par.azon]=new qchalozat(x0,y0,z0);
    if(t_qc[par.azon]==NULL)throw hiba("masina::foglal_halot","alloc failed");
    hpqc=pqc->get0()->gethalo();

    // AllocStep0cells
    // vecdim=1, extended=false

    uns N=0;
    const model & mod=*par.sim->pmodel;
    for(uns i=0;i<z0;i++)for(uns j=0;j<y0;j++)for(uns k=0;k<x0;k++,N++)if(mod.tcolor[mod.tbmp[i].getpixel_also(k,j)].tipus==SzinNormal){
        hpqc[N]=new qccella(false); 
    }
    for(uns i=0; i<mod.tcoupled.size(); i++)
        mod.tcoupled[i].set_externals(x0,y0,z0,hpqc,mod);
    N=0;
    for(uns i=0;i<z0;i++)for(uns j=0;j<y0;j++)for(uns k=0;k<x0;k++,N++)if(mod.tcolor[mod.tbmp[i].getpixel_also(k,j)].tipus==SzinNormal){
        u16 * olda=hpqc[N]->get_oldalak();
        bool * is_external=hpqc[N]->get_d()->external_nulladik, is_perem;
        u16 n=olda[EXTERNAL]=0;
        n+=olda[WEST]   = ( is_perem = ( k==0    || mod.tcolor[mod.tbmp[i].getpixel_also(k-1,j)].tipus!=SzinNormal) ) && !is_external[WEST]   ? 0 : 1; is_external[WEST]   = is_external[WEST]   && is_perem;
        n+=olda[EAST]   = ( is_perem = ( k==x0-1 || mod.tcolor[mod.tbmp[i].getpixel_also(k+1,j)].tipus!=SzinNormal) ) && !is_external[EAST]   ? 0 : 1; is_external[EAST]   = is_external[EAST]   && is_perem;
        n+=olda[SOUTH]  = ( is_perem = ( j==0    || mod.tcolor[mod.tbmp[i].getpixel_also(k,j-1)].tipus!=SzinNormal) ) && !is_external[SOUTH]  ? 0 : 1; is_external[SOUTH]  = is_external[SOUTH]  && is_perem;
        n+=olda[NORTH]  = ( is_perem = ( j==y0-1 || mod.tcolor[mod.tbmp[i].getpixel_also(k,j+1)].tipus!=SzinNormal) ) && !is_external[NORTH]  ? 0 : 1; is_external[NORTH]  = is_external[NORTH]  && is_perem;
        n+=olda[BOTTOM] = ( is_perem = ( i==0    || mod.tcolor[mod.tbmp[i-1].getpixel_also(k,j)].tipus!=SzinNormal) ) && !is_external[BOTTOM] ? 0 : 1; is_external[BOTTOM] = is_external[BOTTOM] && is_perem;
        n+=olda[TOP]    = ( is_perem = ( i==z0-1 || mod.tcolor[mod.tbmp[i+1].getpixel_also(k,j)].tipus!=SzinNormal) ) && !is_external[TOP]    ? 0 : 1; is_external[TOP]    = is_external[TOP]    && is_perem;
        hpqc[N]->set_nulladik_kulso_oldalszam(n);
        olda[CENTER]=1;
    }

    // Alloc V0

    pqc->get0()->AllocV();
}


//***********************************************************************
void masina::foglal_matrixot(uns step){
//***********************************************************************
      setAnalKeszStep(0,step,"alloc matrix");
    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    switch(melyik){
        case 0: t_dr[par.azon]->get0()->AllocM(); t_dr[par.azon]->get0()->AllocY(); break;
        case 1: t_dc[par.azon]->get0()->AllocM(); t_dc[par.azon]->get0()->AllocY(); break;
        case 2: t_qr[par.azon]->get0()->AllocM(); t_qr[par.azon]->get0()->AllocY(); break;
        case 3: t_qc[par.azon]->get0()->AllocM(); t_qc[par.azon]->get0()->AllocY(); break;
    }
}


//***********************************************************************
void masina::kezdo_UT(uns step,const char * s){
//***********************************************************************
        setAnalKeszStep(0,step,s);
    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    switch(melyik){
        case 0: kezdo_UT_dr(); break;
        case 1: kezdo_UT_dc(); break;
        case 2: kezdo_UT_qr(); break;
        case 3: kezdo_UT_qc(); break;
    }
}


//***********************************************************************
void masina::kezdo_UT_dr(){
//***********************************************************************
    drcella ** hpdr=t_dr[par.azon]->get0()->gethalo();
    const model & mod=*par.sim->pmodel;
    cuns max=mod.x_res*mod.y_res*mod.z_res;
    const bool is_el=par.tipus==FieldEl;
//    cuns fazon = is_el ? par.u_azon : par.T_azon;

    for(uns i=0;i<max;i++)if(hpdr[i]!=NULL){
        hpdr[i]->get_d()->u.zero();
//        hpdr[i]->get_d()->ub.set( 0, par.is_start_map ? t_drmap[fazon]->getconstref(i).t[EXTERNAL] : is_el ? par.sim->ambiE : par.sim->ambiT );
        hpdr[i]->get_d()->ub.zero(); // a lentebbi sz�mol� f�ggv�nyek mindig hozz�adj�k a k�rnyezeti h�m�rs�kletet a k�z�ppont fesz�hez, akkor ide mi�rt az el�z� sor ker�lne???
    }
}


//***********************************************************************
void masina::kezdo_UT_dc(){
//***********************************************************************
    dccella ** hpdc=t_dc[par.azon]->get0()->gethalo();
    const model & mod=*par.sim->pmodel;
    cuns max=mod.x_res*mod.y_res*mod.z_res;
    const bool is_el=par.tipus==FieldEl;
//    cuns fazon = is_el ? par.u_azon : par.T_azon;
    for(uns i=0;i<max;i++)if(hpdc[i]!=NULL){
        hpdc[i]->get_d()->u.zero();
//        hpdc[i]->get_d()->ub.set( 0, par.is_start_map ? t_dcmap[fazon]->getconstref(i).t[EXTERNAL] : is_el ? par.sim->ambiE : par.sim->ambiT );
        hpdc[i]->get_d()->ub.zero(); // a lentebbi sz�mol� f�ggv�nyek mindig hozz�adj�k a k�rnyezeti h�m�rs�kletet a k�z�ppont fesz�hez, akkor ide mi�rt az el�z� sor ker�lne???
    }
}


//***********************************************************************
void masina::kezdo_UT_qr(){
//***********************************************************************
    qrcella ** hpqr=t_qr[par.azon]->get0()->gethalo();
    const model & mod=*par.sim->pmodel;
    cuns max=mod.x_res*mod.y_res*mod.z_res;
    const bool is_el=par.tipus==FieldEl;
//    cuns fazon = is_el ? par.u_azon : par.T_azon;

    for(uns i=0;i<max;i++)if(hpqr[i]!=NULL){
        hpqr[i]->get_d()->u.zero();
//        hpqr[i]->get_d()->ub.set( 0, d2q(par.is_start_map ? t_drmap[fazon]->getconstref(i).t[EXTERNAL] : is_el ? par.sim->ambiE : par.sim->ambiT) );
        hpqr[i]->get_d()->ub.zero(); // a lentebbi sz�mol� f�ggv�nyek mindig hozz�adj�k a k�rnyezeti h�m�rs�kletet a k�z�ppont fesz�hez, akkor ide mi�rt az el�z� sor ker�lne???
    }
}


//***********************************************************************
void masina::kezdo_UT_qc(){
//***********************************************************************
    qccella ** hpqc=t_qc[par.azon]->get0()->gethalo();
    const model & mod=*par.sim->pmodel;
    cuns max=mod.x_res*mod.y_res*mod.z_res;
    const bool is_el=par.tipus==FieldEl;
//    cuns fazon = is_el ? par.u_azon : par.T_azon;

    for(uns i=0;i<max;i++)if(hpqc[i]!=NULL){
        hpqc[i]->get_d()->u.zero();
//        hpqc[i]->get_d()->ub.set( 0, dc2qc(par.is_start_map ? t_dcmap[fazon]->getconstref(i).t[EXTERNAL] : is_el ? par.sim->ambiE : par.sim->ambiT) );
        hpqc[i]->get_d()->ub.zero(); // a lentebbi sz�mol� f�ggv�nyek mindig hozz�adj�k a k�rnyezeti h�m�rs�kletet a k�z�ppont fesz�hez, akkor ide mi�rt az el�z� sor ker�lne???
    }
}


//***********************************************************************
void masina::fill_step0_adm(uns step){
//***********************************************************************
      setAnalKeszStep(0,step,"fill Y");
    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    switch(melyik){
        case 0: fill_step0_adm_dr(); break;
        case 1: fill_step0_adm_dc(); break;
        case 2: fill_step0_adm_qr(); break;
        case 3: fill_step0_adm_qc(); break;
    }
}

/*
//***********************************************************************
void masina::get_T_dr(const drcella & dc,dbl T[]){
// bemenete a kisz�molt u �s ub h�m�rs�klet vektor
// nem peremre be�rja a kisz�molt �rt�ket
// peremre INTERPOL�L !!!!!!!!!!!!!!!!
//***********************************************************************
    u32 index=0;
    const drcella::celladat & d=*dc.d;
    bool is_filled[7]={true,false,false,false,false,false,false};
    T[t_o[0]]=d.ub.get(0); // k�z�ppont h�m�rs�klete
    for(u32 iv=1; iv<7; iv++){
        T[t_o[iv]]=T[0]; // minden�tt a k�z�ppont h�m�rs�klete legyen!
        if(d.oldalak[t_o[iv]]!=0){ // nem perem oldal
            T[t_o[iv]]=d.u.get(index); 
            index+=d.oldalak[t_o[iv]];
            is_filled[iv]=true;
        }
    }
}
*/

// H�m�rs�kletet honnan vegye:
// - saj�t k�z�ppont: termikus szimul�ci� mindig
// - Tmap: elektromos, par.is_start_map=true
// - ambient: elektromos, par.is_start_map=false
// �ramot honnan vegye
// - dummy: ha nem elektromos a szimul�ci�, vagy par.is_current false
// - Imap: elektromos szim �s par.current
//***********************************************************************
void masina::fill_step0_adm_dr(){
// be�ll�tja a cell�k admittancia �rt�keit
//***********************************************************************
    
    // el�k�sz�t�s

    drcella ** hpdr=t_dr[par.azon]->get0()->gethalo();
    const simulation & sim=*par.sim;
    const model & mod=*par.sim->pmodel;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
    uns N=0;
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
    const bool is_transi=par.is_transi;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
    cd dt=par.dt;
    if(is_start_map && t_drmap.size() < par.old_T_azon){
        uns i = t_drmap.size();
        t_drmap.resize(par.old_T_azon+1);
        for(; i < t_drmap.size(); i++)
            t_drmap[i] = NULL;
    }
    const tomb3d<real_cell_res> * ptemp=is_start_map?t_drmap[par.T_azon]:NULL;
//    const tomb3d<real_cell_res> * p_old_temp=is_start_map?t_drmap[par.old_T_azon]:NULL; // hiszter�zises anyagn�l 2014.01.05.
    const tomb3d<real_cell_res> * pcurr=t_drmap.size()>par.i_azon?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!
    if (is_el) {
        if (Ge_map.size() != x0*y0*z0) {
            Ge_map.resize(x0*y0*z0);
            Ge_map.zero();
        }
        if (Ge_old_map.size() != x0*y0*z0) {
            Ge_old_map.resize(x0*y0*z0);
            Ge_old_map.zero();
        }
    }
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
    gcsd res;
    const excitation * texc=is_el ? sim.texcitE : sim.texcitT;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;
//bool most=true;
    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpdr[N]!=NULL){
                // inicializ�l�s

                u16 index=0; // az Y ill J adott oldalhoz tartoz� indexe, nincs EXTERNAl

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                drcella & aktcella=*hpdr[N];
                drcella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                // csak a k�z�ppont h�m�rs�klete lesz �j termikus t�rn�l, a k�ls�k a r�gi h�m�rs�kleten maradnak. Ez a thomson effektusn�l jelent egy kellemetlen ugr�st.
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map?ptemp->getconstref(N).t[iv]+ambiT:ambiT;
//if(most&&p_old_temp!=NULL)printf("\n*** %g ***\n",ptemp->getconstref(N).t[0]-p_old_temp->getconstref(N).t[0]);
//                most=false;
                gp.I=&I;
                d.y.zero();//nxazb-t nem kell null�zni, mert minden elem�t felt�ltj�k
                d.nzb.zero();

                // cell�n bel�li vezet�sek sz�m�t�sa
                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::fill_step0_adm_dr","get_gcsd perem");
                if (is_el)
                    Ge_map[N] = res.sum_gfull;

                // cell�n bel�li vezet�sek �s peremfelt�telek be�ll�t�sa

                if(d.oldalak[WEST]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,WEST,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[WEST] : replusz(A_x*b->ertek(x,y,z,WEST,is_el,mod,A_x,true),res.gfull[WEST]));
                }
                else{ cd G=res.gfull[WEST]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[WEST]; }
                if(d.oldalak[EAST]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,EAST,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[EAST] : replusz(A_x*b->ertek(x,y,z,EAST,is_el,mod,A_x,true),res.gfull[EAST]));
                }
                else{ cd G=res.gfull[EAST]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[EAST]; }
                if(d.oldalak[SOUTH]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,SOUTH,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[SOUTH] : replusz(A_y*b->ertek(x,y,z,SOUTH,is_el,mod,A_y,true),res.gfull[SOUTH]));
                }
                else{ cd G=res.gfull[SOUTH]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[SOUTH]; }
                if(d.oldalak[NORTH]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,NORTH,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[NORTH] : replusz(A_y*b->ertek(x,y,z,NORTH,is_el,mod,A_y,true),res.gfull[NORTH]));
                }
                else{ cd G=res.gfull[NORTH]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[NORTH]; }
                if(d.oldalak[BOTTOM]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,BOTTOM,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[BOTTOM] : replusz(A_z*b->ertek(x,y,z,BOTTOM,is_el,mod,A_z,true),res.gfull[BOTTOM]));
                }
                else{ cd G=res.gfull[BOTTOM]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[BOTTOM]; }
                if(d.oldalak[TOP]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,TOP,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[TOP] : replusz(A_z*b->ertek(x,y,z,TOP,is_el,mod,A_z,true),res.gfull[TOP]));
                }
                else{ cd G=res.gfull[TOP]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[TOP]; }
                if(index!=d.y.getrow())throw hiba("masina::fill_step0_adm_dr","index!=d.y.getrow() (%u!=%u)",index,d.y.getrow());

                // gerjeszt�s

                const excitation & exc = texc[cella_szin];
                if(exc.is)switch(exc.tipus){
                    case GerjSemmi: break;
                    case GerjU: d.nzb.inc00(g_max); break;
                    case GerjI: break;
                    default: throw hiba("masina::fill_step0_adm_dr","Unknown exc.tipus");
                }

                // tranziens

                if(is_therm && is_transi && dt>nulla)d.nzb.inc00(res.c/dt);
            }
        }
    }
    fill_extern_adm_dr();
}


//***********************************************************************
void masina::fill_step0_adm_dc(){
// be�ll�tja a cell�k admittancia �rt�keit
//***********************************************************************
    
    // el�k�sz�t�s

    dccella ** hpdc=t_dc[par.azon]->get0()->gethalo();
    const simulation & sim=*par.sim;
    const model & mod=*par.sim->pmodel;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
    uns N=0;
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
    const tomb3d<real_cell_res> * ptemp=is_start_map?t_drmap[par.T_azon]:NULL;
    const tomb3d<real_cell_res> * pcurr=t_drmap.size()>par.i_azon?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
    gcsd res;
    const excitation * texc=is_el ? sim.texcitE : sim.texcitT;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;
    dcomplex s = par.is_timeconst ? par.s : dcomplex(nulla,2.0*M_PI*par.f);

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpdc[N]!=NULL){

                // inicializ�l�s

                u16 index=0; // az Y ill J adott oldalhoz tartoz� indexe, nincs EXTERNAl

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                dccella & aktcella=*hpdc[N];
                dccella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                d.y.zero();//nxazb-t nem kell null�zni, mert minden elem�t felt�ltj�k
                d.nzb.zero();

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::fill_step0_adm_dc","get_gcsd perem");

                // cell�n bel�li vezet�sek �s peremfelt�telek be�ll�t�sa

                if(d.oldalak[WEST]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,WEST,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[WEST] : replusz(A_x*b->ertek(x,y,z,WEST,is_el,mod,A_x,true),res.gfull[WEST]));
                }
                else{ cd G=res.gfull[WEST]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[WEST]; }
                if(d.oldalak[EAST]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,EAST,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[EAST] : replusz(A_x*b->ertek(x,y,z,EAST,is_el,mod,A_x,true),res.gfull[EAST]));
                }
                else{ cd G=res.gfull[EAST]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[EAST]; }
                if(d.oldalak[SOUTH]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,SOUTH,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[SOUTH] : replusz(A_y*b->ertek(x,y,z,SOUTH,is_el,mod,A_y,true),res.gfull[SOUTH]));
                }
                else{ cd G=res.gfull[SOUTH]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[SOUTH]; }
                if(d.oldalak[NORTH]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,NORTH,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[NORTH] : replusz(A_y*b->ertek(x,y,z,NORTH,is_el,mod,A_y,true),res.gfull[NORTH]));
                }
                else{ cd G=res.gfull[NORTH]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[NORTH]; }
                if(d.oldalak[BOTTOM]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,BOTTOM,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[BOTTOM] : replusz(A_z*b->ertek(x,y,z,BOTTOM,is_el,mod,A_z,true),res.gfull[BOTTOM]));
                }
                else{ cd G=res.gfull[BOTTOM]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[BOTTOM]; }
                if(d.oldalak[TOP]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,TOP,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(b->tipus==PeremU ? res.gfull[TOP] : replusz(A_z*b->ertek(x,y,z,TOP,is_el,mod,A_z,true),res.gfull[TOP]));
                }
                else{ cd G=res.gfull[TOP]; d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[TOP]; }
                if(index!=d.y.getrow())throw hiba("masina::fill_step0_adm_dc","index!=d.y.getrow() (%u!=%u)",index,d.y.getrow());

                // gerjeszt�s

                const excitation & exc = texc[cella_szin];
                if(exc.is)switch(exc.tipus){
                    case GerjSemmi: break;
                    case GerjU: d.nzb.inc00(g_max); break;
                    case GerjI: break;
                    default: throw hiba("masina::fill_step0_adm_dc","Unknown exc.tipus");
                }

                // kapacit�s

                if(is_therm)d.nzb.inc00(s*res.c);
            }
        }
    }
    fill_extern_adm_dc();
}


//***********************************************************************
void masina::fill_step0_adm_qr(){
// be�ll�tja a cell�k admittancia �rt�keit
//***********************************************************************
    
    // el�k�sz�t�s

    qrcella ** hpqr=t_qr[par.azon]->get0()->gethalo();
    const simulation & sim=*par.sim;
    const model & mod=*par.sim->pmodel;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
    uns N=0;
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
    const bool is_transi=par.is_transi;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
    cd dt=par.dt;
    if (is_start_map && t_drmap.size() < par.old_T_azon) {
        uns i = t_drmap.size();
        t_drmap.resize(par.old_T_azon + 1);
        for (; i < t_drmap.size(); i++)
            t_drmap[i] = NULL;
    }
    const tomb3d<real_cell_res> * ptemp=is_start_map?t_drmap[par.T_azon]:NULL;
    const tomb3d<real_cell_res> * p_old_temp=is_start_map?t_drmap[par.old_T_azon]:NULL; // hiszter�zises anyagn�l 2014.01.05.
    const tomb3d<real_cell_res> * pcurr=t_drmap.size()>par.i_azon?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
    gcsd res;
    const excitation * texc=is_el ? sim.texcitE : sim.texcitT;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpqr[N]!=NULL){

                // inicializ�l�s

                u16 index=0; // az Y ill J adott oldalhoz tartoz� indexe, nincs EXTERNAl

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                qrcella & aktcella=*hpqr[N];
                qrcella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                d.y.zero();//nxazb-t nem kell null�zni, mert minden elem�t felt�ltj�k
                d.nzb.zero();

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::fill_step0_adm_qr","get_gcsd perem");

                // cell�n bel�li vezet�sek �s peremfelt�telek be�ll�t�sa

                if(d.oldalak[WEST]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,WEST,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[WEST] : replusz(A_x*b->ertek(x,y,z,WEST,is_el,mod,A_x,true),res.gfull[WEST])));
                }
                else{ cq G=d2q(res.gfull[WEST]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[WEST]; }
                if(d.oldalak[EAST]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,EAST,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[EAST] : replusz(A_x*b->ertek(x,y,z,EAST,is_el,mod,A_x,true),res.gfull[EAST])));
                }
                else{ cq G=d2q(res.gfull[EAST]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[EAST]; }
                if(d.oldalak[SOUTH]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,SOUTH,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[SOUTH] : replusz(A_y*b->ertek(x,y,z,SOUTH,is_el,mod,A_y,true),res.gfull[SOUTH])));
                }
                else{ cq G=d2q(res.gfull[SOUTH]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[SOUTH]; }
                if(d.oldalak[NORTH]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,NORTH,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[NORTH] : replusz(A_y*b->ertek(x,y,z,NORTH,is_el,mod,A_y,true),res.gfull[NORTH])));
                }
                else{ cq G=d2q(res.gfull[NORTH]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[NORTH]; }
                if(d.oldalak[BOTTOM]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,BOTTOM,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[BOTTOM] : replusz(A_z*b->ertek(x,y,z,BOTTOM,is_el,mod,A_z,true),res.gfull[BOTTOM])));
                }
                else{ cq G=d2q(res.gfull[BOTTOM]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[BOTTOM]; }
                if(d.oldalak[TOP]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,TOP,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[TOP] : replusz(A_z*b->ertek(x,y,z,TOP,is_el,mod,A_z,true),res.gfull[TOP])));
                }
                else{ cq G=d2q(res.gfull[TOP]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[TOP]; }
                if(index!=d.y.getrow())throw hiba("masina::fill_step0_adm_qr","index!=d.y.getrow() (%u!=%u)",index,d.y.getrow());

                // gerjeszt�s

                const excitation & exc = texc[cella_szin];
                if(exc.is)switch(exc.tipus){
                    case GerjSemmi: break;
                    case GerjU: d.nzb.inc(0,0,qg_max); break;
                    case GerjI: break;
                    default: throw hiba("masina::fill_step0_adm_qr","Unknown exc.tipus");
                }

                // tranziens

                if(is_therm && is_transi && dt>nulla)d.nzb.inc00(d2q(res.c/dt));
            }
        }
    }
    fill_extern_adm_qr();
}


//***********************************************************************
void masina::fill_step0_adm_qc(){
// be�ll�tja a cell�k admittancia �rt�keit
//***********************************************************************
    
    // el�k�sz�t�s

    qccella ** hpqc=t_qc[par.azon]->get0()->gethalo();
    const simulation & sim=*par.sim;
    const model & mod=*par.sim->pmodel;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
    uns N=0;
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
    const tomb3d<real_cell_res> * ptemp=is_start_map?t_drmap[par.T_azon]:NULL;
    const tomb3d<real_cell_res> * pcurr=t_drmap.size()>par.i_azon?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
    gcsd res;
    const excitation * texc=is_el ? sim.texcitE : sim.texcitT;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;
    dcomplex s = par.is_timeconst ? par.s : dcomplex(nulla,2.0*M_PI*par.f);

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpqc[N]!=NULL){

                // inicializ�l�s

                u16 index=0; // az Y ill J adott oldalhoz tartoz� indexe, nincs EXTERNAl

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                qccella & aktcella=*hpqc[N];
                qccella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                d.y.zero();//nxazb-t nem kell null�zni, mert minden elem�t felt�ltj�k
                d.nzb.zero();

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::fill_step0_adm_qc","get_gcsd perem");

                // cell�n bel�li vezet�sek �s peremfelt�telek be�ll�t�sa

                if(d.oldalak[WEST]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,WEST,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[WEST] : replusz(A_x*b->ertek(x,y,z,WEST,is_el,mod,A_x,true),res.gfull[WEST])));
                }
                else{ cq G=d2q(res.gfull[WEST]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[WEST]; }
                if(d.oldalak[EAST]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,EAST,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[EAST] : replusz(A_x*b->ertek(x,y,z,EAST,is_el,mod,A_x,true),res.gfull[EAST])));
                }
                else{ cq G=d2q(res.gfull[EAST]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[EAST]; }
                if(d.oldalak[SOUTH]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,SOUTH,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[SOUTH] : replusz(A_y*b->ertek(x,y,z,SOUTH,is_el,mod,A_y,true),res.gfull[SOUTH])));
                }
                else{ cq G=d2q(res.gfull[SOUTH]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[SOUTH]; }
                if(d.oldalak[NORTH]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,NORTH,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[NORTH] : replusz(A_y*b->ertek(x,y,z,NORTH,is_el,mod,A_y,true),res.gfull[NORTH])));
                }
                else{ cq G=d2q(res.gfull[NORTH]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[NORTH]; }
                if(d.oldalak[BOTTOM]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,BOTTOM,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[BOTTOM] : replusz(A_z*b->ertek(x,y,z,BOTTOM,is_el,mod,A_z,true),res.gfull[BOTTOM])));
                }
                else{ cq G=d2q(res.gfull[BOTTOM]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[BOTTOM]; }
                if(d.oldalak[TOP]==0){ // perem oldal
                    const boundary * b=sim.get_perem(x,y,z,TOP,is_el);
                    if(b->tipus!=PeremOpen)d.nzb.inc00(d2q(b->tipus==PeremU ? res.gfull[TOP] : replusz(A_z*b->ertek(x,y,z,TOP,is_el,mod,A_z,true),res.gfull[TOP])));
                }
                else{ cq G=d2q(res.gfull[TOP]); d.y.set(index,index,G); d.nxazb.set(index,0,-G); d.nzb.inc00(G); index+=d.oldalak[TOP]; }
                if(index!=d.y.getrow())throw hiba("masina::fill_step0_adm_qc","index!=d.y.getrow() (%u!=%u)",index,d.y.getrow());

                // gerjeszt�s

                const excitation & exc = texc[cella_szin];
                if(exc.is)switch(exc.tipus){
                    case GerjSemmi: break;
                    case GerjU: d.nzb.inc00(qg_max); break;
                    case GerjI: break;
                    default: throw hiba("masina::fill_step0_adm_qc","Unknown exc.tipus");
                }

                // kapacit�s

                if(is_therm)d.nzb.inc00(dc2qc(s*res.c));
            }
        }
    }
    fill_extern_adm_qc();
}


//***********************************************************************
void masina::fill_step0_curr(uns step,const double * p_map){
//***********************************************************************
      setAnalKeszStep(0,step,"fill J");
    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    switch(melyik){
        case 0: fill_step0_curr_dr(p_map); break;
        case 1: fill_step0_curr_dc(); break;
        case 2: fill_step0_curr_qr(); break;
        case 3: fill_step0_curr_qc(); break;
    }
}


//***********************************************************************
inline dbl disszipacio(dbl p0,dbl d,dbl f,dbl r){
// f: sug�rzott teljes�tm�ny, r: sug�rz�si t�nyez�
    cd alap=p0*d;
    cd sug=f*r;//if(f!=0.0)printf("f=%g, r=%g\n",f,r);
    cd sum=( sug > alap * 0.9 ) ? alap*0.1 : alap-sug;
    return sum;
}
//***********************************************************************
inline dbl disszipacio(dbl u,dbl g,dbl d,dbl f,dbl r){
    return disszipacio(u*u*g,d,f,r);
}
//***********************************************************************

//***********************************************************************
inline dbl disszipacio(dcomplex u,dbl g,dbl d,dbl f,dbl r){
    return disszipacio(u.re*u.re*g,d,f,r); // �s ez mennyire j�?
}
//***********************************************************************

//***********************************************************************
hizotomb dissztomb,dissztomb_A,dissztomb_tenyleges;
dbl fulldissz=nulla;
const Oldal t_o[7]={EXTERNAL,WEST,EAST,SOUTH,NORTH,BOTTOM,TOP};
extern u32 global_akt_iter;
//***********************************************************************

//***********************************************************************
void masina::fill_step0_curr_dr(const double * p_map){
// be�ll�tja a cell�k inhomog�n �ram �rt�keit
// ha p_map nem NULL, akkor x0 � y0 � z0 m�ret�
//***********************************************************************

    // el�k�sz�t�s

    drcella ** hpdr=t_dr[par.azon]->get0()->gethalo();
    const simulation & sim=*par.sim;
    const model & mod=*par.sim->pmodel;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
    uns N=0;
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
    const bool is_transi=par.is_transi;
    const bool is_el_th_Seebeck=par.is_el_th_Seebeck;
    const bool is_th_el_Seebeck=par.is_th_el_Seebeck;
    const bool is_el_th_diss=par.is_el_th_diss;
//    const bool is_th_el_Seebeck=par.is_th_el_Seebeck;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
    cd dt=par.dt;
    cd fold=is_el?nulla:sim.ambiT;

    if (is_start_map && t_drmap.size() < par.old_T_azon) {
        uns i = t_drmap.size();
        t_drmap.resize(par.old_T_azon + 1);
        for (; i < t_drmap.size(); i++)
            t_drmap[i] = NULL;
    }

    const tomb3d<real_cell_res> * ptemp=is_start_map?t_drmap[par.T_azon]:NULL; // Seebeckn�l k�telez� start_map
//    const tomb3d<real_cell_res> * p_old_temp=is_start_map?t_drmap[par.old_T_azon]:NULL; // hiszter�zises anyagn�l 2014.01.05.
    const tomb3d<real_cell_res> * pcurr=t_drmap.size()>par.i_azon?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
    gcsd res,eres;
    const excitation * texc=is_el ? sim.texcitE : sim.texcitT;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;
    dissztomb.expand(x0, y0, z0);
    dissztomb_tenyleges.expand(x0, y0, z0);
    dissztomb_A.expand(x0, y0, z0);
    fulldissz=nulla;

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
//            const double * p_sor = p_map==NULL ? NULL : ( p_map + x0 * y );
            const double * p_sor = p_map == NULL ? NULL : (p_map + x0 * y + x0 * y0 * z); // �t�rva 2014.06.20.
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpdr[N]!=NULL){

                // inicializ�l�s

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                cd t_m[7]={0.0,A_x,A_x,A_y,A_y,A_z,A_z}; // EXTERNAL,WEST,EAST,..
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                drcella & aktcella=*hpdr[N];
                drcella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                //dbl Tc=is_therm?d.ub.get(0)+ambiT:is_start_map?ptemp->getconstref(N).t[EXTERNAL]+ambiT:ambiT;
                //dbl Tc=is_start_map?ptemp->getconstref(N).t[EXTERNAL]+ambiT:ambiT; // Az el�z� sort erre cser�ltem, mert ott ub.get(0) mindig 0.
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                d.j.zero();
                d.jb.zero();

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::fill_step0_curr_dr","get_gcsd perem");

                // peremfelt�telek be�ll�t�sa

                for(u32 iv=1;iv<7;iv++){
                    if(d.oldalak[t_o[iv]]==0){
                        const boundary * b=sim.get_perem(x,y,z,t_o[iv],is_el);
                        cd aktA=(t_o[iv]==WEST||t_o[iv]==EAST)?A_x:(t_o[iv]==SOUTH||t_o[iv]==NORTH)?A_y:A_z;
                        if(b->tipus==PeremU){ cd u=b->ertek(x,y,z,t_o[iv],is_el,mod,aktA,false)-fold; if(u!=nulla)d.jb.inc0(-u*res.gfull[t_o[iv]]); }
                        else if (b->tipus==PeremRU){ cd u=b->ertek2()-fold; if(u!=nulla)d.jb.inc0(-u*replusz(aktA*b->ertek(x,y,z,t_o[iv],is_el,mod,aktA,false),res.gfull[t_o[iv]])); }
                    }
                }
                
                // logiterm

                if (p_map != NULL) {
                    if (d.oldalak[TOP] == 0) {
                        const boundary * b = sim.get_perem(x, y, z, TOP, is_el);
                        switch (b->tipus) {
                            case PeremOpen: d.jb.inc0(-p_sor[x]);
                                break;
                            case PeremRU:
                            case PeremR:    d.jb.inc0(-p_sor[x] * res.gfull[TOP] / (res.gfull[TOP] + A_z*b->ertek(x, y, z, TOP, is_el, mod, A_z, true)));
                                break;
/*                            case PeremRU: {
                                    dbl g_perem = A_z*b->ertek(x, y, z, TOP, is_el, mod, A_z, true);
                                    d.jb.inc0(-((b->ertek2() - fold)*g_perem + p_sor[x]) * res.gfull[TOP] / (res.gfull[TOP] + g_perem));
                                }
                                break;
*/                            case PeremU: // nincs
                                break;
                        }
                    }
                    else // nem perem, hanem bels� csom�pontra kapcsol�dik
                        d.j.set(d.j.getsiz() - 1, -p_sor[x]); // Az utols� a j t�mbben a TOP node
                }

/*
                if( p_map != NULL && z == z0-1 ){
                    const boundary * b=sim.get_perem(x,y,z,TOP,is_el);
                    switch(b->tipus){
                        case PeremOpen: d.jb.inc0( -p_sor[x] );
                            break;
                        case PeremR:    d.jb.inc0(-p_sor[x] * res.gfull[TOP] / (res.gfull[TOP] + A_z*b->ertek(x, y, z, TOP, is_el, mod, A_z, true)));
                            break;
                        case PeremRU:{
                                dbl g_perem = A_z*b->ertek(x, y, z, TOP, is_el, mod, A_z, true);
                                d.jb.inc0( -((b->ertek2() - fold)*g_perem + p_sor[x]) * res.gfull[TOP] / (res.gfull[TOP] + g_perem));
                            }
                            break;
                        case PeremU: // nincs
                            break;
                    }                    
                }
                else if (p_map != NULL) { // nem perem, hanem bels� csom�pontra kapcsol�dik
                    d.j.set(d.j.getsiz() - 1, -p_sor[x]); // Az utols� a j t�mbben a TOP node
                }
*/

                // gerjeszt�s

                const excitation & exc = texc[cella_szin];
                if(exc.is)switch(exc.tipus){
                    case GerjSemmi: break;
                    case GerjU: d.jb.inc0(-(exc.ertek-fold)*g_max); break;
                    case GerjI: {
                            dbl ertek=exc.ertek;
                            //if(global_akt_iter>0 && global_akt_iter<100){
                            //    //double M=pow(1000.0,1.0/99.0);
                            //    //ertek=ertek/1000.0*pow(M,double(global_akt_iter));
                            //    //ertek=ertek/100.0*global_akt_iter;
                            //}
                            cd aram=-ertek*res.V/res.Vszin/12.0;
                            //for(uns iv=0; iv<d.j.getsiz(); iv++)d.j.inc(iv,aram); // �ram fele a peremen
                            //d.jb.inc0((12.0-d.j.getsiz())*aram); 
                            d.jb.inc0(12.0*aram); // minden �ram k�z�pen
                        }
                        break;
                    default: throw hiba("masina::fill_step0_curr_dr","Unknown exc.tipus");
                }

                // tranziens

                if(is_therm && is_transi && dt>nulla){
//                    d.jb.inc0(-res.c*(d.ub.get(0)-fold)/dt);// Sz�kelyn�l -= volt, itt tal�n + kell?
                    d.jb.inc0(-res.c*(gp.Temp[EXTERNAL]-ambiT)/dt);// A k�z�ppont h�m�rs�klete kell, tilos az �tlagot haszn�lni!
                    //printf("res.c=%g, d.ub.get(0)=%g, fold=%g, dt=%g\n",res.c,gp.Temp[0],ambiT,dt);
                }

                // z�bekk!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                // H�m�rs�kletek

                dbl Hom[7]; // EXTERNAL, WEST, EAST,...
                for(u32 iv=0; iv<7; iv++) Hom[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;

                //Seebeck-effektus

                if(is_el && is_th_el_Seebeck){
                    // Ha nem perem, J[i]-hez Iseebeck[i] ad�dik, Jb-hez pedig -Iseebeck[i]
                    // Ha perem:
                    // - szakad�sn�l nem ad�dik hozz� �ram
                    // - fesz�lts�gn�l -Iseebeck[i] ad�dik Jb-hez
                    // - vezet�sn�l -Iseebeck[i]*Gperem/(G[i]+Gperem) ad�dik Jb-hez

                    // Seebeck-�ramok

                    dbl Js[7]; // EXTERNAL, WEST, EAST,...
                    dbl TC=Hom[EXTERNAL];

                    for(u32 iv=1; iv<7; iv++) // Val�ban Gfull-lal kell sz�molni, ha a pn �tmeneten nem esik Seebeck fesz.
                        Js[iv] = -res.s[iv] * res.gfull[iv] * (Hom[iv]-TC);

                    // j vektorba rak�s

                    u16 index = 0; // a J adott oldalhoz tartoz� indexe, nincs EXTERNAl
                    for(u32 iv=1; iv<7; iv++){
                         if(d.oldalak[iv]==0){
                            cd aktA=(t_o[iv]==WEST||t_o[iv]==EAST)?A_x:(t_o[iv]==SOUTH||t_o[iv]==NORTH)?A_y:A_z;
                            const boundary * b=sim.get_perem(x,y,z,t_o[iv],is_el);
                            switch(b->tipus){
                                case PeremOpen:  break; // nem ad�dik hozz� semmi (nem folyik �ram a szakad�sb�l)
                                case PeremU: d.jb.inc0( -Js[t_o[iv]] );  break;
                                case PeremR:
                                case PeremRU:{
                                        dbl g_perem = t_m[t_o[iv]] * b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, false);
                                        d.jb.inc0(-Js[t_o[iv]] * g_perem / (g_perem + res.gfull[t_o[iv]]));
                                    }
                                    break;
                            }
                        }
                        else{ d.j.inc(index,Js[t_o[iv]]);     d.jb.inc0(-Js[t_o[iv]]);       index+=d.oldalak[t_o[iv]]; } // j.set-r�l j.inc-re �t�rva a pontosabb disszip�ci�/�rammodellez�s miatt
                   }
                }
                
                if(is_therm&&(is_el_th_diss||is_el_th_Seebeck)){ // kell a gfull �s a dfull meg S
                    const real_cell_res & Iel=pcurr->getconstref(N);
                    gp.ter=0;
                    gp.I=is_current ? &Iel : &I_dummy;
                    mod.get_gcsd(gp,eres,&mod.tseged[N]);
                    gp.I=&I; // a k�vetkez� ciklusmenet megint termikus, vissza kell �rni, amit elrontunk!
                    gp.ter=1;

                    // seebeck �ramok �s feszek

                    dbl Us[7]; // EXTERNAL, WEST, EAST,...
                    for(u32 iv=0; iv<7; iv++)Us[iv]=0.0;
                    if(is_th_el_Seebeck){
                        dbl TC=Hom[EXTERNAL];
                        for(u32 iv=1; iv<7; iv++) 
                            Us[iv] = eres.s[iv] * eres.gfull[iv] * (Hom[iv]-TC) / eres.gfull[iv]; // 2012.jan.02. Mi�rt szorzom �s osztom ugyanazzal????
                    }

                    // disszip�ci�

                    if(is_el_th_diss){
                        dbl ddd,ddsum=0.0;
                        for(u32 iv=1; iv<7; iv++){
                            ddd = sim.is_no_joule ? 0.0 : disszipacio(Iel.t[t_o[iv]]*Iel.t[t_o[iv]]/eres.gfull[t_o[iv]], eres.dfull[t_o[iv]],eres.f_rad[t_o[iv]], eres.fr[t_o[iv]]);
                            d.jb.inc0(-ddd);
                            ddsum+=ddd;
                        }
                        dissztomb.set(ddsum/(x_pit*y_pit*z_pit),x,y,z);
                        dissztomb_tenyleges.set(ddsum, x, y, z);
                        dissztomb_A.set(ddsum / (x_pit*y_pit), x, y, z);
                        fulldissz+=ddsum;
                    }

                    // Peltier-Thomson 

                    if(is_therm&&is_el_th_Seebeck){

                        // Peltier-Thomson-�ramok

                        dbl Jp[7]; // EXTERNAL, WEST, EAST,...
                        Jp[EXTERNAL] = 0.0;
                        if(sim.is_peltier_center && !sim.is_no_peltier){ // k�z�ppontra ker�l a peltier-h�
                            dbl S_Szomszed;

                            S_Szomszed = x > 0 ? mod.tcolor[mod.tbmp[z].getpixel_also(x-1,y)].pmat->get_S().get_x(Hom[WEST],gp.is_lin,&mod.tseged[N]) : nulla;
                            Jp[EXTERNAL] += 0.5 * (Hom[WEST]+absT) * Iel.t[WEST] * ( eres.s_valodi[WEST] - S_Szomszed );
                            
                            S_Szomszed = x < x0-1 ? mod.tcolor[mod.tbmp[z].getpixel_also(x+1,y)].pmat->get_S().get_x(Hom[EAST],gp.is_lin,&mod.tseged[N]) : nulla;
                            Jp[EXTERNAL] += 0.5 * (Hom[EAST]+absT) * Iel.t[EAST] * ( eres.s_valodi[EAST] - S_Szomszed );

                            S_Szomszed = y > 0 ? mod.tcolor[mod.tbmp[z].getpixel_also(x,y-1)].pmat->get_S().get_x(Hom[SOUTH],gp.is_lin,&mod.tseged[N]) : nulla;
                            Jp[EXTERNAL] += 0.5 * (Hom[SOUTH]+absT) * Iel.t[SOUTH] * ( eres.s_valodi[SOUTH] - S_Szomszed );

                            S_Szomszed = y < y0-1 ? mod.tcolor[mod.tbmp[z].getpixel_also(x,y+1)].pmat->get_S().get_x(Hom[NORTH],gp.is_lin,&mod.tseged[N]) : nulla;
                            Jp[EXTERNAL] += 0.5 * (Hom[NORTH]+absT) * Iel.t[NORTH] * ( eres.s_valodi[NORTH] - S_Szomszed );
                            
                            S_Szomszed = z > 0 ? mod.tcolor[mod.tbmp[z-1].getpixel_also(x,y)].pmat->get_S().get_x(Hom[BOTTOM],gp.is_lin,&mod.tseged[N]) : nulla;
                            Jp[EXTERNAL] += 0.5 * (Hom[BOTTOM]+absT) * Iel.t[BOTTOM] * ( eres.s_valodi[BOTTOM] - S_Szomszed );
                            
                            S_Szomszed = z < z0-1 ? mod.tcolor[mod.tbmp[z+1].getpixel_also(x,y)].pmat->get_S().get_x(Hom[TOP],gp.is_lin,&mod.tseged[N]) : nulla;
                            Jp[EXTERNAL] += 0.5 * (Hom[TOP]+absT) * Iel.t[TOP] * ( eres.s_valodi[TOP] - S_Szomszed );
                        }
                        else{
                            for(u32 iv=1; iv<7; iv++){ // Peltier-h�
                                Jp[iv] = sim.is_no_peltier ? 0.0 : (Hom[iv]+absT)*Iel.t[iv]*eres.s_valodi[iv];
                            }
                        }

                        for(u32 iv=1; iv<7; iv++) // Thomson-h�
                            Jp[EXTERNAL] += sim.is_no_thomson ? 0.0 : 0.5*(Hom[EXTERNAL]+Hom[iv]+2.0*absT)*Iel.t[iv]*(eres.s_valodi[EXTERNAL]-eres.s_valodi[iv]);

                        // j vektorba rak�s

                        u16 index = 0; // a J adott oldalhoz tartoz� indexe, nincs EXTERNAl
                        d.jb.inc0(Jp[EXTERNAL]);
                        for(u32 iv=1; iv<7; iv++){
                                if(d.oldalak[iv]==0){
                                    cd aktA=(t_o[iv]==WEST||t_o[iv]==EAST)?A_x:(t_o[iv]==SOUTH||t_o[iv]==NORTH)?A_y:A_z;
                                    const boundary * b=sim.get_perem(x,y,z,t_o[iv],is_el);
                                    switch(b->tipus){
                                        case PeremOpen:   d.jb.inc0(-Jp[iv]); break;
                                        case PeremU:      break;
                                        case PeremR:
                                        case PeremRU:
                                            d.jb.inc0(-Jp[t_o[iv]] * res.gfull[t_o[iv]] / (t_m[t_o[iv]] * b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, false) + res.gfull[t_o[iv]]));  break;
                                    }
                            }
                            else{ d.j.inc(index,Jp[t_o[iv]]);       index+=d.oldalak[t_o[iv]]; } // j.set-r�l j.inc-re �t�rva a pontosabb disszip�ci�/�rammodellez�s miatt
                        }
                    }
                }
            }
        }
    }
    fill_extern_curr_dr();
}


//***********************************************************************
void masina::fill_step0_curr_dc(){
// be�ll�tja a cell�k inhomog�n �ram �rt�keit
//***********************************************************************
    
    // el�k�sz�t�s

    dccella ** hpdc=t_dc[par.azon]->get0()->gethalo();
    const simulation & sim=*par.sim;
    const model & mod=*par.sim->pmodel;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
    uns N=0;
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
//    const bool is_transi=par.is_transi;
    const bool is_el_th_Seebeck=par.is_el_th_Seebeck;
    const bool is_el_th_diss=par.is_el_th_diss;
//    const bool is_th_el_Seebeck=par.is_th_el_Seebeck;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
//    cd dt=par.dt;
//    cd fold=is_el?sim.ambiE:sim.ambiT;
    const tomb3d<real_cell_res> * ptemp=is_start_map?t_drmap[par.T_azon]:NULL; // Seebeckn�l k�telez� start_map
    const tomb3d<real_cell_res> * pcurr=t_drmap.size()>par.i_azon?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
    gcsd res,eres;
    const excitation * texc=is_el ? sim.texcitE : sim.texcitT;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpdc[N]!=NULL){

                // inicializ�l�s

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                dccella & aktcella=*hpdc[N];
                dccella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                d.j.zero();
                d.jb.zero();

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::fill_step0_curr_dr","get_gcsd perem");

                // peremfelt�telek be�ll�t�sa

                for (u32 iv = 1; iv<7; iv++){
                    if (d.oldalak[t_o[iv]] == 0){
                        const boundary * b = sim.get_perem(x, y, z, t_o[iv], is_el);
                        cd aktA = (t_o[iv] == WEST || t_o[iv] == EAST) ? A_x : (t_o[iv] == SOUTH || t_o[iv] == NORTH) ? A_y : A_z;
                        if (b->tipus == PeremU){ cd u = b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, false); if (u != nulla)d.jb.inc0(-u*res.gfull[t_o[iv]]); }
                        // A PeremRU U-ja mindig DC
                    }
                }

                // gerjeszt�s

                const excitation & exc = texc[cella_szin];
                if(exc.is)switch(exc.tipus){
                    case GerjSemmi: break;
                    case GerjU: d.jb.inc0(-(exc.ertek)*g_max); break;
                    case GerjI: d.jb.inc0(-exc.ertek*res.V/res.Vszin); break;
                    default: throw hiba("masina::fill_step0_curr_dr","Unknown exc.tipus");
                }

                // z�bekk!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                if(is_therm&&(is_el_th_diss||is_el_th_Seebeck)){ // kell a gfull �s a dfull meg S
                    gp.ter=0;
                    gp.I=is_current ? &pcurr->getconstref(N) : &I_dummy;
                    mod.get_gcsd(gp,eres,&mod.tseged[N]);
                    gp.I=&I; // a k�vetkez� ciklusmenet megint termikus, vissza kell �rni, amit elrontunk!
                    gp.ter=1;
                    if(is_el_th_diss){
                        const dcomplex_cell_res & u_map=t_dcmap[par.u_azon]->getref(N);
                        cdc u_center=u_map.t[EXTERNAL]; // vajon hogy k�ne sz�molni a disszip�ci�t? �s hogy k�ne figyelembe venni a Peltier-Seebeck-Thomson hat�sokat?
                        d.jb.inc0(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[WEST]  -u_center),eres.gfull[WEST],  eres.dfull[WEST], eres.f_rad[WEST], eres.fr[WEST]));
                        d.jb.inc0(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[EAST]  -u_center),eres.gfull[EAST],  eres.dfull[EAST], eres.f_rad[EAST], eres.fr[EAST]));
                        d.jb.inc0(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[SOUTH] -u_center),eres.gfull[SOUTH], eres.dfull[SOUTH], eres.f_rad[SOUTH], eres.fr[SOUTH]));
                        d.jb.inc0(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[NORTH] -u_center),eres.gfull[NORTH], eres.dfull[NORTH], eres.f_rad[NORTH], eres.fr[NORTH]));
                        d.jb.inc0(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[BOTTOM]-u_center),eres.gfull[BOTTOM],eres.dfull[BOTTOM], eres.f_rad[BOTTOM], eres.fr[BOTTOM]));
                        d.jb.inc0(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[TOP]   -u_center),eres.gfull[TOP],   eres.dfull[TOP], eres.f_rad[TOP], eres.fr[TOP]));
                    }
                }
//printf("jb: ");d.jb.print();
            }
        }
    }
    fill_extern_curr_dc();
}


//***********************************************************************
void masina::fill_step0_curr_qr(){
// be�ll�tja a cell�k inhomog�n �ram �rt�keit
//***********************************************************************
    
    // el�k�sz�t�s

    qrcella ** hpqr=t_qr[par.azon]->get0()->gethalo();
    const simulation & sim=*par.sim;
    const model & mod=*par.sim->pmodel;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
    uns N=0;
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
    const bool is_transi=par.is_transi;
    const bool is_el_th_Seebeck=par.is_el_th_Seebeck;
    const bool is_el_th_diss=par.is_el_th_diss;
    const bool is_th_el_Seebeck=par.is_th_el_Seebeck;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
    cd dt=par.dt;
    cd fold=is_el?nulla:sim.ambiT;

    if (is_start_map && t_drmap.size() < par.old_T_azon) {
        uns i = t_drmap.size();
        t_drmap.resize(par.old_T_azon + 1);
        for (; i < t_drmap.size(); i++)
            t_drmap[i] = NULL;
    }

    const tomb3d<real_cell_res> * ptemp=is_start_map?t_drmap[par.T_azon]:NULL; // Seebeckn�l k�telez� start_map
    //const tomb3d<real_cell_res> * p_old_temp=is_start_map?t_drmap[par.old_T_azon]:NULL; // hiszter�zises anyagn�l 2014.01.05.
    const tomb3d<real_cell_res> * pcurr=t_drmap.size()>par.i_azon?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
    gcsd res,eres;
    const excitation * texc=is_el ? sim.texcitE : sim.texcitT;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;
    dissztomb.expand(x0, y0, z0);
    dissztomb_tenyleges.expand(x0, y0, z0);
    dissztomb_A.expand(x0, y0, z0);
    fulldissz=nulla;

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpqr[N]!=NULL){

                // inicializ�l�s

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                cd t_m[7]={0.0,A_x,A_x,A_y,A_y,A_z,A_z}; // EXTERNAL,WEST,EAST,..
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                qrcella & aktcella=*hpqr[N];
                qrcella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                d.j.zero();
                d.jb.zero();

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::fill_step0_curr_dr","get_gcsd perem");

                // peremfelt�telek be�ll�t�sa

                for (u32 iv = 1; iv<7; iv++){
                    if (d.oldalak[t_o[iv]] == 0){
                        const boundary * b = sim.get_perem(x, y, z, t_o[iv], is_el);
                        cd aktA = (t_o[iv] == WEST || t_o[iv] == EAST) ? A_x : (t_o[iv] == SOUTH || t_o[iv] == NORTH) ? A_y : A_z;
                        if (b->tipus == PeremU){ cd u = b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, false) - fold; if (u != nulla)d.jb.inc0(d2q(-u*res.gfull[t_o[iv]])); }
                        else if (b->tipus == PeremRU){ cd u = b->ertek2() - fold; if (u != nulla)d.jb.inc0(d2q(-u*replusz(aktA*b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, false), res.gfull[t_o[iv]]))); }
                    }
                }

                // gerjeszt�s

                const excitation & exc = texc[cella_szin];
                if(exc.is)switch(exc.tipus){
                    case GerjSemmi: break;
                    case GerjU: d.jb.inc0(d2q(-(exc.ertek-fold)*g_max)); break;
                    //case GerjI: d.jb.inc0(d2q(-exc.ertek*res.V/res.Vszin)); break;
                    case GerjI: {
                            dbl ertek=exc.ertek;
                            cd aram=-ertek*res.V/res.Vszin/12.0;
                            //for(uns iv=0; iv<d.j.getsiz(); iv++)d.j.inc(iv,d2q(aram)); // �ram fele a peremen
                            //d.jb.inc0(d2q((12.0-d.j.getsiz())*aram)); 
                            d.jb.inc0(d2q(12.0*aram)); // minden �ram k�z�pen
                        }
                        break;
                    default: throw hiba("masina::fill_step0_curr_dr","Unknown exc.tipus");
                }

                // tranziens

                if(is_therm && is_transi && dt>nulla)
                    d.jb.inc0(d2q(-res.c*(gp.Temp[EXTERNAL]-ambiT)/dt));// Sz�kelyn�l -= volt, itt tal�n + kell?

                // z�bekk!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                // H�m�rs�kletek

                dbl Hom[7]; // EXTERNAL, WEST, EAST,...
                for(u32 iv=0; iv<7; iv++) Hom[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;

                //Seebeck-effektus

                if(is_el && is_th_el_Seebeck){
                    // Ha nem perem, J[i]-hez Iseebeck[i] ad�dik, Jb-hez pedig -Iseebeck[i]
                    // Ha perem:
                    // - szakad�sn�l nem ad�dik hozz� �ram
                    // - fesz�lts�gn�l -Iseebeck[i] ad�dik Jb-hez
                    // - vezet�sn�l -Iseebeck[i]*Gperem/(G[i]+Gperem) ad�dik Jb-hez

                    // Seebeck-�ramok

                    dbl Js[7]; // EXTERNAL, WEST, EAST,...
                    dbl TC=Hom[EXTERNAL];

                    for(u32 iv=1; iv<7; iv++) Js[iv] = -res.s[iv] * res.gfull[iv] * (Hom[iv]-TC);

                    // j vektorba rak�s

                    u16 index = 0; // a J adott oldalhoz tartoz� indexe, nincs EXTERNAl
                    for(u32 iv=1; iv<7; iv++){
                         if(d.oldalak[iv]==0){
                            cd aktA=(t_o[iv]==WEST||t_o[iv]==EAST)?A_x:(t_o[iv]==SOUTH||t_o[iv]==NORTH)?A_y:A_z;
                            const boundary * b=sim.get_perem(x,y,z,t_o[iv],is_el);
                            switch(b->tipus){
                                case PeremOpen:  break; // nem ad�dik hozz� semmi
                                case PeremU: d.jb.inc0( d2q(-Js[t_o[iv]]) );  break;
                                case PeremR: 
                                case PeremRU: d.jb.inc0( d2q(-Js[t_o[iv]] * t_m[t_o[iv]] * b->ertek(x,y,z,t_o[iv],is_el,mod,aktA,false) / (  t_m[t_o[iv]] * b->ertek(x,y,z,t_o[iv],is_el,mod,aktA,false) + res.gfull[t_o[iv]] )) );  break;
                            }
                        }
                        else{ d.j.set(index,d2q(Js[t_o[iv]]));     d.jb.inc0(d2q(-Js[t_o[iv]]));       index+=d.oldalak[t_o[iv]]; }
                   }
                }

                if(is_therm&&(is_el_th_diss||is_el_th_Seebeck)){ // kell a gfull �s a dfull meg S
                    const real_cell_res & Iel=pcurr->getconstref(N);
                    gp.ter=0;
                    gp.I=is_current ? &Iel : &I_dummy;
                    mod.get_gcsd(gp,eres,&mod.tseged[N]);
                    gp.I=&I; // a k�vetkez� ciklusmenet megint termikus, vissza kell �rni, amit elrontunk!
                    gp.ter=1;

                    // seebeck feszek

                    dbl Us[7]; // EXTERNAL, WEST, EAST,...
                    for(u32 iv=0; iv<7; iv++)Us[iv]=0.0;
                    if(is_th_el_Seebeck){
                        dbl TC=Hom[EXTERNAL];
                        for(u32 iv=1; iv<7; iv++) 
                            Us[iv] = eres.s[iv] * eres.gfull[iv] * (Hom[iv]-TC) / eres.gfull[iv];
                    }

                    // disszip�ci�

                    if(is_el_th_diss){
                        //const real_cell_res & u_map=t_drmap[par.u_azon]->getref(N);
                        dbl ddd=0.0, ddsum=0.0;

                        for(u32 iv=1; iv<7; iv++){
                            d.jb.inc0(d2q(-(ddd = sim.is_no_joule ? 0.0 : disszipacio(Iel.t[iv]*Iel.t[iv]/eres.gfull[t_o[iv]], eres.dfull[t_o[iv]],eres.f_rad[t_o[iv]], eres.fr[t_o[iv]]))));
                            ddsum += ddd;
                        }
                        dissztomb.set(ddsum/(x_pit*y_pit*z_pit),x,y,z);
                        dissztomb_tenyleges.set(ddsum, x, y, z);
                        dissztomb_A.set(ddsum / (x_pit*y_pit), x, y, z);
                        fulldissz += ddsum;
                    }


                    // Peltier-Thomson 

                    if(is_therm&&is_el_th_Seebeck){

                        // Peltier-Thomson-�ramok

                        dbl Jp[7]; // EXTERNAL, WEST, EAST,...
                        for(u32 iv=1; iv<7; iv++){ // Peltier-h�
                            Jp[iv] = sim.is_no_peltier ? 0.0 : (Hom[iv]+absT)*Iel.t[iv]*eres.s_valodi[iv];
                        }

                        Jp[EXTERNAL] = 0.0;
                        for(u32 iv=1; iv<7; iv++) // Thomson-h�
                            Jp[EXTERNAL] += sim.is_no_thomson ? 0.0 : 0.5*(Hom[EXTERNAL]+Hom[iv]+2.0*absT)*Iel.t[iv]*(eres.s_valodi[EXTERNAL]-eres.s_valodi[iv]);

                        // j vektorba rak�s

                        u16 index = 0; // a J adott oldalhoz tartoz� indexe, nincs EXTERNAl
                        d.jb.inc0(d2q(Jp[EXTERNAL]));
                        for(u32 iv=1; iv<7; iv++){
                                if(d.oldalak[iv]==0){
                                    cd aktA=(t_o[iv]==WEST||t_o[iv]==EAST)?A_x:(t_o[iv]==SOUTH||t_o[iv]==NORTH)?A_y:A_z;
                                    const boundary * b=sim.get_perem(x,y,z,t_o[iv],is_el);
                                    switch(b->tipus){
                                        case PeremOpen:   d.jb.inc0(d2q(-Jp[iv])); break;
                                        case PeremU:      break;
                                        case PeremR:
                                        case PeremRU: d.jb.inc0( d2q(-Jp[t_o[iv]] * res.gfull[t_o[iv]] / (  t_m[t_o[iv]] * b->ertek(x,y,z,t_o[iv],is_el,mod,aktA,false) + res.gfull[t_o[iv]] )) );  break;
                                    }
                            }
                            else{ d.j.inc(index,d2q(Jp[t_o[iv]]));       index+=d.oldalak[t_o[iv]]; } // j.set-r�l j.inc-re �t�rva a pontosabb disszip�ci�/�rammodellez�s miatt
                        }
                    }

                }
            }
        }
    }
    fill_extern_curr_qr();
}


//***********************************************************************
void masina::fill_step0_curr_qc(){
// be�ll�tja a cell�k inhomog�n �ram �rt�keit
//***********************************************************************
    
    // el�k�sz�t�s

    qccella ** hpqc=t_qc[par.azon]->get0()->gethalo();
    const simulation & sim=*par.sim;
    const model & mod=*par.sim->pmodel;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
    uns N=0;
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
//    const bool is_transi=par.is_transi;
    const bool is_el_th_Seebeck=par.is_el_th_Seebeck;
    const bool is_el_th_diss=par.is_el_th_diss;
//    const bool is_th_el_Seebeck=par.is_th_el_Seebeck;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
//    cd dt=par.dt;
//    cd fold=is_el?sim.ambiE:sim.ambiT;
    const tomb3d<real_cell_res> * ptemp=is_start_map?t_drmap[par.T_azon]:NULL; // Seebeckn�l k�telez� start_map
    const tomb3d<real_cell_res> * pcurr=t_drmap.size()>par.i_azon?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
    gcsd res,eres;
    const excitation * texc=is_el ? sim.texcitE : sim.texcitT;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpqc[N]!=NULL){

                // inicializ�l�s

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                qccella & aktcella=*hpqc[N];
                qccella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                d.j.zero();
                d.jb.zero();

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::fill_step0_curr_dr","get_gcsd perem");

                // peremfelt�telek be�ll�t�sa

                for (u32 iv = 1; iv<7; iv++){
                    if (d.oldalak[t_o[iv]] == 0){
                        const boundary * b = sim.get_perem(x, y, z, t_o[iv], is_el);
                        cd aktA = (t_o[iv] == WEST || t_o[iv] == EAST) ? A_x : (t_o[iv] == SOUTH || t_o[iv] == NORTH) ? A_y : A_z;
                        if (b->tipus == PeremU){ cd u = b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, false); if (u != nulla)d.jb.inc0(d2q(-u*res.gfull[t_o[iv]])); }
                        // A PeremRU U-ja mindig DC
                    }
                }

                // gerjeszt�s

                const excitation & exc = texc[cella_szin];
                if(exc.is)switch(exc.tipus){
                    case GerjSemmi: break;
                    case GerjU: d.jb.inc0(d2q(-(exc.ertek)*g_max)); break;
                    case GerjI: d.jb.inc0(d2q(-exc.ertek*res.V/res.Vszin)); break;
                    default: throw hiba("masina::fill_step0_curr_dr","Unknown exc.tipus");
                }

                // z�bekk!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

                if(is_therm&&(is_el_th_diss||is_el_th_Seebeck)){ // kell a gfull �s a dfull meg S
                    gp.ter=0;
                    gp.I=is_current ? &pcurr->getconstref(N) : &I_dummy;
                    mod.get_gcsd(gp,eres,&mod.tseged[N]);
                    gp.I=&I; // a k�vetkez� ciklusmenet megint termikus, vissza kell �rni, amit elrontunk!
                    gp.ter=1;
                    if(is_el_th_diss){
                        const dcomplex_cell_res & u_map=t_dcmap[par.u_azon]->getref(N);
                        cdc u_center=u_map.t[EXTERNAL];
                        d.jb.inc0(d2q(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[WEST]  -u_center),eres.gfull[WEST],  eres.dfull[WEST], eres.f_rad[WEST], eres.fr[WEST])));
                        d.jb.inc0(d2q(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[EAST]  -u_center),eres.gfull[EAST],  eres.dfull[EAST], eres.f_rad[EAST], eres.fr[EAST])));
                        d.jb.inc0(d2q(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[SOUTH] -u_center),eres.gfull[SOUTH], eres.dfull[SOUTH], eres.f_rad[SOUTH], eres.fr[SOUTH])));
                        d.jb.inc0(d2q(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[NORTH] -u_center),eres.gfull[NORTH], eres.dfull[NORTH], eres.f_rad[NORTH], eres.fr[NORTH])));
                        d.jb.inc0(d2q(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[BOTTOM]-u_center),eres.gfull[BOTTOM],eres.dfull[BOTTOM], eres.f_rad[BOTTOM], eres.fr[BOTTOM])));
                        d.jb.inc0(d2q(sim.is_no_joule ? 0.0 : -disszipacio((u_map.t[TOP]   -u_center),eres.gfull[TOP],   eres.dfull[TOP], eres.f_rad[TOP], eres.fr[TOP])));
                    }
                }
            }
        }
    }
    fill_extern_curr_qc();
}


//***********************************************************************
void masina::fill_extern_adm_dr(){
// az external csom�pontok dr_y_compmod m�trix�t t�lti fel
//***********************************************************************
    const model & mod=*par.sim->pmodel;

    if(mod.tcoupled.size()==0)return;

    uns n=0;
    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            if(mod.tcoupled[i].compmod.texternal_azon[j] > n)
                n = mod.tcoupled[i].compmod.texternal_azon[j];
    dr_y_compmod.resize(n);

    // felt�lt�s

    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            for(uns k=j; k<mod.tcoupled[i].compmod.texternal_azon.size(); k++)
                dr_y_compmod.set(mod.tcoupled[i].compmod.texternal_azon[j]-1,mod.tcoupled[i].compmod.texternal_azon[k]-1,mod.tcoupled[i].compmod.y->get(j,k));
}


//***********************************************************************
void masina::fill_extern_curr_dr(){
// az external csom�pontok dr_j_compmod m�trix�t t�lti fel
//***********************************************************************
    const model & mod=*par.sim->pmodel;

    if(mod.tcoupled.size()==0)return;

    uns n=dr_y_compmod.getcol();
    dr_j_compmod.resize(n);

    // felt�lt�s

    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            dr_j_compmod.set(mod.tcoupled[i].compmod.texternal_azon[j]-1,mod.tcoupled[i].compmod.j->get(j));
}


//***********************************************************************
void masina::fill_extern_adm_dc(){
// az external csom�pontok dc_y_compmod m�trix�t t�lti fel
//***********************************************************************
    const model & mod=*par.sim->pmodel;

    if(mod.tcoupled.size()==0)return;

    uns n=0;
    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            if(mod.tcoupled[i].compmod.texternal_azon[j] > n)
                n = mod.tcoupled[i].compmod.texternal_azon[j];
    dc_y_compmod.resize(n);

    // felt�lt�s

    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            for(uns k=j; k<mod.tcoupled[i].compmod.texternal_azon.size(); k++)
                dc_y_compmod.set(mod.tcoupled[i].compmod.texternal_azon[j]-1,mod.tcoupled[i].compmod.texternal_azon[k]-1,mod.tcoupled[i].compmod.y->get(j,k));
}


//***********************************************************************
void masina::fill_extern_curr_dc(){
// az external csom�pontok dc_j_compmod m�trix�t t�lti fel
//***********************************************************************
    const model & mod=*par.sim->pmodel;

    if(mod.tcoupled.size()==0)return;

    uns n=dc_y_compmod.getcol();
    dc_j_compmod.resize(n);

    // felt�lt�s

    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            dc_j_compmod.set(mod.tcoupled[i].compmod.texternal_azon[j]-1,mod.tcoupled[i].compmod.j->get(j));
}


//***********************************************************************
void masina::fill_extern_adm_qr(){
// az external csom�pontok qr_y_compmod m�trix�t t�lti fel
//***********************************************************************
    const model & mod=*par.sim->pmodel;

    if(mod.tcoupled.size()==0)return;

    uns n=0;
    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            if(mod.tcoupled[i].compmod.texternal_azon[j] > n)
                n = mod.tcoupled[i].compmod.texternal_azon[j];
    qr_y_compmod.resize(n);

    // felt�lt�s

    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            for(uns k=j; k<mod.tcoupled[i].compmod.texternal_azon.size(); k++)
                qr_y_compmod.set(mod.tcoupled[i].compmod.texternal_azon[j]-1,mod.tcoupled[i].compmod.texternal_azon[k]-1,d2q(mod.tcoupled[i].compmod.y->get(j,k)));
}


//***********************************************************************
void masina::fill_extern_curr_qr(){
// az external csom�pontok qr_j_compmod m�trix�t t�lti fel
//***********************************************************************
    const model & mod=*par.sim->pmodel;

    if(mod.tcoupled.size()==0)return;

    uns n=qr_y_compmod.getcol();
    qr_j_compmod.resize(n);

    // felt�lt�s

    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            qr_j_compmod.set(mod.tcoupled[i].compmod.texternal_azon[j]-1,d2q(mod.tcoupled[i].compmod.j->get(j)));
}


//***********************************************************************
void masina::fill_extern_adm_qc(){
// az external csom�pontok qc_y_compmod m�trix�t t�lti fel
//***********************************************************************
    const model & mod=*par.sim->pmodel;

    if(mod.tcoupled.size()==0)return;

    uns n=0;
    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            if(mod.tcoupled[i].compmod.texternal_azon[j] > n)
                n = mod.tcoupled[i].compmod.texternal_azon[j];
    qc_y_compmod.resize(n);

    // felt�lt�s

    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            for(uns k=j; k<mod.tcoupled[i].compmod.texternal_azon.size(); k++)
                qc_y_compmod.set(mod.tcoupled[i].compmod.texternal_azon[j]-1,mod.tcoupled[i].compmod.texternal_azon[k]-1,d2q(mod.tcoupled[i].compmod.y->get(j,k)));
}


//***********************************************************************
void masina::fill_extern_curr_qc(){
// az external csom�pontok qc_j_compmod m�trix�t t�lti fel
//***********************************************************************
    const model & mod=*par.sim->pmodel;

    if(mod.tcoupled.size()==0)return;

    uns n=qc_y_compmod.getcol();
    qc_j_compmod.resize(n);

    // felt�lt�s

    for(uns i=0; i<mod.tcoupled.size(); i++)
        for(uns j=0; j<mod.tcoupled[i].compmod.texternal_azon.size(); j++)
            qc_j_compmod.set(mod.tcoupled[i].compmod.texternal_azon[j]-1,d2q(mod.tcoupled[i].compmod.j->get(j)));
}


//***********************************************************************
void masina::nodered(bool is_always_strassen_mul){
//***********************************************************************
    STRASSEN_MUL_LIMIT=STRASSEN_MUL_DEFAULT;
    if(par.tipus==FieldEl&&!is_always_strassen_mul)
        STRASSEN_MUL_LIMIT=50000;
    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    switch(melyik){
        case 0: t_dr[par.azon]->nodered(true,par.sim->hdd!=0); break;
        case 1: t_dc[par.azon]->nodered(true,par.sim->hdd!=0); break;
        case 2: t_qr[par.azon]->nodered(true,par.sim->hdd!=0); break;
        case 3: t_qc[par.azon]->nodered(true,par.sim->hdd!=0); break;
    }
}


//***********************************************************************
void masina::forwsubs(uns step){
//***********************************************************************
      setAnalKeszStep(0,step,"backsubs");
    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    switch(melyik){
        case 0: t_dr[par.azon]->forwsubs(true); break;
        case 1: t_dc[par.azon]->forwsubs(true); break;
        case 2: t_qr[par.azon]->forwsubs(true); break;
        case 3: t_qc[par.azon]->forwsubs(true); break;
    }
}


//***********************************************************************
void masina::backsubs(uns step,bool aramot){
//***********************************************************************
      setAnalKeszStep(0,step,"backsubs");
    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    switch(melyik){
        case 0: t_dr[par.azon]->backsubs(true,aramot); break;
        case 1: t_dc[par.azon]->backsubs(true,aramot); break;
        case 2: t_qr[par.azon]->backsubs(true,aramot); break;
        case 3: t_qc[par.azon]->backsubs(true,aramot); break;
    }
}


//***********************************************************************
void masina::get_csakcenter_matrix(uns mx_azon){
//***********************************************************************
    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    tomb3d<real_cell_res> * pad=NULL;
    tomb3d<dcomplex_cell_res> * pac=NULL;
    const model & mod=*par.sim->pmodel;
    cuns N=mod.x_res*mod.y_res*mod.z_res;
    if(par.is_ac){
        if(t_dcmap.size()<=mx_azon){
            uns sts=t_dcmap.size();
            t_dcmap.resize(mx_azon+1);
            for(uns i=sts;i<=mx_azon;i++)t_dcmap[i]=NULL;
        }
        delete t_dcmap[mx_azon];
        pac=t_dcmap[mx_azon]=new tomb3d<dcomplex_cell_res>;
        pac->resize(mod.x_res,mod.y_res,mod.z_res);
    }
    else{
        if(t_drmap.size()<=mx_azon){
            uns sts=t_drmap.size();
            t_drmap.resize(mx_azon+1);
            for(uns i=sts;i<=mx_azon;i++)t_drmap[i]=NULL;
        }
        delete t_drmap[mx_azon];
        pad=t_drmap[mx_azon]=new tomb3d<real_cell_res>;
        pad->resize(mod.x_res,mod.y_res,mod.z_res);
    }
    switch(melyik){
        case 0:{
            drcella ** hpdr=t_dr[par.azon]->get0()->gethalo();
            for(uns i=0;i<N;i++)
                pad->getref(i).t[EXTERNAL] = (hpdr[i]==NULL) ? nulla : hpdr[i]->d->ub.get(0);
        }
        break;
        case 1:{
            dccella ** hpdc=t_dc[par.azon]->get0()->gethalo();
            for(uns i=0;i<N;i++)
                pac->getref(i).t[EXTERNAL] = (hpdc[i]==NULL) ? nulla : hpdc[i]->d->ub.get(0);
        }
        case 2:{
            qrcella ** hpqr=t_qr[par.azon]->get0()->gethalo();
            for(uns i=0;i<N;i++)
                pad->getref(i).t[EXTERNAL] = (hpqr[i]==NULL) ? nulla : q2d(hpqr[i]->d->ub.get(0));
       }
        case 3:{
            qccella ** hpqc=t_qc[par.azon]->get0()->gethalo();
            for(uns i=0;i<N;i++)
                pac->getref(i).t[EXTERNAL] = (hpqc[i]==NULL) ? nulla : qc2dc(hpqc[i]->d->ub.get(0));
       }
    }
}


//***********************************************************************
inline dbl oszto(dbl G1,dbl G2){return G1/(G1+G2);} // G1/G1+G2 volt!
//***********************************************************************


//***********************************************************************
void masina::get_all_matrix(uns u_mx_azon,bool is_i,uns i_mx_azon){
//***********************************************************************
    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    switch(melyik){
        case 0: get_all_matrix_dr(u_mx_azon,is_i,i_mx_azon); break;
        case 1: get_all_matrix_dc(u_mx_azon,is_i,i_mx_azon); break;
        case 2: get_all_matrix_qr(u_mx_azon,is_i,i_mx_azon); break;
        case 3: get_all_matrix_qc(u_mx_azon,is_i,i_mx_azon); break;
    }
}


//***********************************************************************
inline dbl gyorsito(dbl x1,dbl x2,dbl x3,dbl szorzo){
//***********************************************************************
    if((x1-x2)*(x2-x3)>=nulla) return x3 + (x3-x2)*szorzo; // egym�s ut�n
    else                       return (x2+x3)*0.5;         // oda-vissza
}

//***********************************************************************
void masina::konvergens_U_T_szamito(cd szorzo){
//***********************************************************************
    static u32 ciklus=0;
    ciklus++;
    if(par.is_ac)throw hiba("masina::konvergens_U_T_szamito","nincs AC-ben megoldva");

    // helyfoglal�s a r�gi �rmaoknak

    cuns maxi=par.old_i_azon>par.older_i_azon?par.old_i_azon:par.older_i_azon;
    if(t_drmap.size()<=maxi){
        uns sts=t_drmap.size();
        t_drmap.resize(maxi+1);
        for(uns i=sts;i<=maxi;i++)t_drmap[i]=NULL;
        const model & mod=*par.sim->pmodel;
        cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
        if(t_drmap[par.old_i_azon]==NULL || t_drmap[par.old_i_azon]->x_size()!=x0 || t_drmap[par.old_i_azon]->y_size()!=y0 || t_drmap[par.old_i_azon]->z_size()!=z0){
            delete t_drmap[par.old_i_azon];
            t_drmap[par.old_i_azon]=new tomb3d<real_cell_res>;
            t_drmap[par.old_i_azon]->resize(x0,y0,z0);
        }
        if(t_drmap[par.older_i_azon]==NULL || t_drmap[par.older_i_azon]->x_size()!=x0 || t_drmap[par.older_i_azon]->y_size()!=y0 || t_drmap[par.older_i_azon]->z_size()!=z0){
            delete t_drmap[par.older_i_azon];
            t_drmap[par.older_i_azon]=new tomb3d<real_cell_res>;
            t_drmap[par.older_i_azon]->resize(x0,y0,z0);
        }
    }

    // mit sz�m�tsunk?

    const bool is_hom  = (par.old_T_azon!=par.older_T_azon)&&(t_drmap.size()>par.T_azon)&&(t_drmap.size()>par.old_T_azon)&&(t_drmap.size()>par.older_T_azon)&&(t_drmap[par.T_azon]!=NULL)&&(t_drmap[par.old_T_azon]!=NULL)&&(t_drmap[par.older_T_azon]!=NULL);
    const bool is_fesz = (par.old_u_azon!=par.older_u_azon)&&(t_drmap.size()>par.u_azon)&&(t_drmap.size()>par.old_u_azon)&&(t_drmap.size()>par.older_u_azon)&&(t_drmap[par.u_azon]!=NULL)&&(t_drmap[par.old_u_azon]!=NULL)&&(t_drmap[par.older_u_azon]!=NULL);
    const bool is_aram = (par.old_i_azon!=par.older_i_azon)&&(t_drmap.size()>par.i_azon)&&(t_drmap.size()>par.old_i_azon)&&(t_drmap.size()>par.older_i_azon)&&(t_drmap[par.i_azon]!=NULL)&&(t_drmap[par.old_i_azon]!=NULL)&&(t_drmap[par.older_i_azon]!=NULL);

    // sz�m�t�s

    if(ciklus%3==0){
        cu32 size=is_hom ? t_drmap[par.T_azon]->size() : t_drmap[par.u_azon]->size();
//        if(szorzo!=0.0)printf("\nconvergence factor = %g\n",szorzo);

        // h�m

        if(is_hom){
            for(u32 i=0;i<size;i++){
                real_cell_res & h1=t_drmap [ par.older_T_azon ] -> getref(i);
                real_cell_res & h2=t_drmap [ par.old_T_azon   ] -> getref(i);
                real_cell_res & h3=t_drmap [ par.T_azon       ] -> getref(i);
                for(u32 j=0;j<7;j++){
                    h3.t[j] = gyorsito(h1.t[j],h2.t[j],h3.t[j],szorzo);
                    //h3.t[j] = h3.t[j] >200.0 ? 200.0 : h3.t[j];
                    //h3.t[j] = h3.t[j] <60.0 ? 60.0 : h3.t[j];
                }
            }
        }

        // fesz

        if(is_fesz){
            for(u32 i=0;i<size;i++){
                real_cell_res & v1=t_drmap [ par.older_u_azon ] -> getref(i);
                real_cell_res & v2=t_drmap [ par.old_u_azon   ] -> getref(i);
                real_cell_res & v3=t_drmap [ par.u_azon       ] -> getref(i);
                for(u32 j=0;j<7;j++)
                    v3.t[j] = gyorsito(v1.t[j],v2.t[j],v3.t[j],szorzo);
            }
        }

        // �ram

        if(is_aram){
            for(u32 i=0;i<size;i++){
                real_cell_res & c1=t_drmap [ par.older_i_azon ] -> getref(i);
                real_cell_res & c2=t_drmap [ par.old_i_azon   ] -> getref(i);
                real_cell_res & c3=t_drmap [ par.i_azon       ] -> getref(i);
                for(u32 j=0;j<7;j++)
                    c3.t[j] = gyorsito(c1.t[j],c2.t[j],c3.t[j],szorzo);
            }
        }
    }
    else{
        if(is_aram){
            cu32 size=t_drmap[par.i_azon]->size();
            for(u32 i=0;i<size;i++){
                real_cell_res & c1=t_drmap [ par.older_i_azon ] -> getref(i);
                real_cell_res & c2=t_drmap [ par.old_i_azon   ] -> getref(i);
                real_cell_res & c3=t_drmap [ par.i_azon       ] -> getref(i);
                for(u32 j=0;j<7;j++){
                    c1.t[j] = c2.t[j];
                    c2.t[j] = c3.t[j];
                }
            }
        }
    }
}


//***********************************************************************
hizotomb radianciatomb, luminanciatomb;
// az elozo_full_I_semi t�rolja a f�lvezet�-�tmeneten �tfoly� �ram abszol�t �rt�k�nek 1/x-�t (hogy szorozni kelljen vele, ne osztani)
// az full_I_semi gy�jti az �ramokat
hizotomb full_I_semi, elozo_full_I_semi;
hizotomb areatomb; // egy adott pixelen mekkora f�lvezet�-�tmenet ter�let van
//***********************************************************************

//***********************************************************************
void masina::get_all_matrix_dr(uns u_mx_azon,bool is_i,uns i_mx_azon){
// A r�gi �ramot haszn�lja f�lvezet�shez, majd fel�l�rja!
//***********************************************************************
    
    // el�k�sz�t�s

//    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    tomb3d<real_cell_res> *pad=NULL,*padi=NULL;
    const model & mod=*par.sim->pmodel;
    simulation & sim=*par.sim;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
//    cuns max=mod.x_res*mod.y_res*mod.z_res;
    if(t_drmap.size()<=u_mx_azon){
        uns sts=t_drmap.size();
        t_drmap.resize(u_mx_azon+1);
        for(uns i=sts;i<=u_mx_azon;i++)t_drmap[i]=NULL;
    }

    // Ne t�r�lj�k le a r�gi h�m�rs�kleteket!

    tomb3d<real_cell_res> *pTemp=NULL;
    if(u_mx_azon==par.T_azon){
        pTemp=t_drmap[u_mx_azon];
        t_drmap[u_mx_azon]=NULL;
    }

    //

    delete t_drmap[u_mx_azon];
    pad=t_drmap[u_mx_azon]=new tomb3d<real_cell_res>;
    pad->resize(x0,y0,z0);
    if(is_i){
        if(t_drmap.size()<=i_mx_azon){
            uns sts=t_drmap.size();
            t_drmap.resize(i_mx_azon+1);
            for(uns i=sts;i<=i_mx_azon;i++)t_drmap[i]=NULL;
        }
        if(t_drmap[i_mx_azon]==NULL || t_drmap[i_mx_azon]->x_size()!=x0 || t_drmap[i_mx_azon]->y_size()!=y0 || t_drmap[i_mx_azon]->z_size()!=z0){
            delete t_drmap[i_mx_azon];
            padi=t_drmap[i_mx_azon]=new tomb3d<real_cell_res>;
            padi->resize(x0,y0,z0);
        }
        padi=t_drmap[i_mx_azon];
    }
    t_drI_temp.resize(x0,y0,z0); // a ki�r�shoz kisz�m�tjuk az �ramokat.
    t_drP_temp.resize(x0,y0,z0); // a ki�r�shoz kisz�m�tjuk az �ramokat.
    uns N=0;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
    const bool is_th_el_Seebeck=par.is_th_el_Seebeck;
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
    cd fold=is_el?nulla:sim.ambiT;
    gcsd res;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;
    drcella ** hpdr = t_dr[par.azon]->get0()->gethalo();
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    // 2017.01.17. Szerintem az el�z� h�m�rs�klet az, ahov� az old_T_azon mutat
//    const tomb3d<real_cell_res> * ptemp=is_start_map?(pTemp==NULL?t_drmap[par.T_azon]:pTemp):NULL; // mindig a j� h�m�rs�klettel sz�moljon!
    const tomb3d<real_cell_res> * ptemp = is_start_map ? (pTemp == NULL ? t_drmap[par.T_azon] : t_drmap[par.old_T_azon]) : NULL; // mindig a j� h�m�rs�klettel sz�moljon!
    const tomb3d<real_cell_res> * pcurr=is_el&&is_current?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!
    radianciatomb.expand (x0,y0,z0);
    luminanciatomb.expand(x0,y0,z0);
    areatomb.expand(x0,y0,z0);
    if(is_el)full_I_semi.free();
    if(!is_el){
        sim.bou_reset(sim.ambiT);
    }

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpdr[N]!=NULL){

                // inicializ�l�s

                u16 index=0; // az Y ill J adott oldalhoz tartoz� indexe, nincs EXTERNAl

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                cd t_m[7]={0.0,A_x,A_x,A_y,A_y,A_z,A_z}; // EXTERNAL,WEST,EAST,..
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                drcella & aktcella=*hpdr[N];
                drcella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                dbl * tu=pad->getref(N).t;
                dbl * ti=is_i ? padi->getref(N).t : NULL;
                dbl * ti_temp=is_i ? t_drI_temp.getref(N).t : t_drP_temp.getref(N).t; // �ramok
                cd u_center=tu[EXTERNAL]=d.ub.get(0);
//                cd u_center=tu[EXTERNAL]=(is_el||d.ub.get(0)>-150.0) ? d.ub.get(0) : -150.0;

//               if (x==0 && y==0 && z==4)
//                    tu[EXTERNAL] = 49.2 - 25.0;

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::get_all_matrix","get_gcsd perem");

                //Seebeck-effektus

                dbl Us[7],Is[7]; // EXTERNAL, WEST, EAST,...
                for(u32 iv=0; iv<7; iv++)Is[iv]=Us[iv]=0.0;
                if(is_el && is_th_el_Seebeck){

                    dbl Hom[7]; // EXTERNAL, WEST, EAST,...
                    for(u32 iv=0; iv<7; iv++) Hom[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;

                    // seebeck feszek

                    if(is_th_el_Seebeck){
                        dbl TC=Hom[EXTERNAL];
                        for(u32 iv=1; iv<7; iv++) 
                            Us[iv] = (Is[iv] = res.s[iv] * res.gfull[iv] * (Hom[iv]-TC)) / res.gfull[iv];
                    }
                }

                // fesz�lts�gek sz�m�t�sa
                for(u32 iv=1; iv<7; iv++){
                    if(d.oldalak[t_o[iv]]==0){ // perem oldal
                        const boundary * b=sim.get_perem(x,y,z,t_o[iv],is_el);
                        cd aktA=(t_o[iv]==WEST||t_o[iv]==EAST)?A_x:(t_o[iv]==SOUTH||t_o[iv]==NORTH)?A_y:A_z;
                        switch(b->tipus){
                            case PeremOpen: tu[t_o[iv]] = u_center + Us[t_o[iv]]; break;
                            case PeremU:    tu[t_o[iv]] = b->ertek(x,y,z,t_o[iv],is_el,mod,aktA,true) - fold; break;
                            case PeremR:{    
                                    cd a_Us = Us[t_o[iv]];
                                    cd a_gfull = res.gfull[t_o[iv]];
                                    cd a_A = t_m[iv];
                                    cd a_h = b->ertek(x,y,z,t_o[iv],is_el,mod,aktA,true);
                                    cd a_gperem = a_A * a_h;
                                    cd a_o = oszto(a_gfull, a_gperem);
                                    cd a_res = ( u_center + a_Us ) * a_o;
                                    tu[t_o[iv]] = a_res;
                                    //tu[t_o[iv]] = ( u_center + Us[t_o[iv]] ) * oszto( res.gfull[t_o[iv]], t_m[iv] * b->ertek(x,y,z,t_o[iv],is_el,mod,aktA,true) ); 
                                }
                                break;
                            case PeremRU:{
                                    cd a_Us = Us[t_o[iv]];
                                    cd a_gfull = res.gfull[t_o[iv]];
                                    cd a_A = t_m[iv];
                                    cd a_h = b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, true);
                                    cd a_gperem = a_A * a_h;
                                    cd a_o = oszto(a_gfull, a_gperem);
                                    cd a_o2 = oszto(a_gperem, a_gfull);
                                    cd a_res = (u_center + a_Us) * a_o + ( b->ertek2() - fold ) * a_o2;
                                    tu[t_o[iv]] = a_res;
                                }
                                break;
                        }
                    }else{tu[t_o[iv]]=d.u.get(index); index+=d.oldalak[t_o[iv]];} // ut�na n�velj�k, mert external+west==east
//                    if(!is_el)if(tu[t_o[iv]]<-150.0)tu[t_o[iv]]=-150.0;
                }
                if(index!=d.u.getsiz())throw hiba("masina::get_all_matrix","index!=d.u.getsiz() (%u!=%u)",index,d.u.getsiz());

                // �ramok sz�m�t�sa

                ti_temp[EXTERNAL]=nulla;
                for(u32 iv=1; iv<7; iv++)
                    ti_temp[t_o[iv]]  =res.gfull[t_o[iv]]  *(tu[t_o[iv]]  -u_center) - Is[t_o[iv]];
                if(is_i){
                    ti[EXTERNAL]=nulla;
                    for(u32 iv=1; iv<7; iv++){
                        ti[t_o[iv]]  =res.gfull[t_o[iv]]  *(tu[t_o[iv]]  -u_center) - Is[t_o[iv]];
                    }
                }
                if(is_el){
                    double rad = res.f_rad[WEST] + res.f_rad[EAST] + res.f_rad[SOUTH] + res.f_rad[NORTH] + res.f_rad[BOTTOM] + res.f_rad[TOP] ;
                    double lum = res.f_lum[WEST] + res.f_lum[EAST] + res.f_lum[SOUTH] + res.f_lum[NORTH] + res.f_lum[BOTTOM] + res.f_lum[TOP] ;
                    double area = res.f_area[WEST] + res.f_area[EAST] + res.f_area[SOUTH] + res.f_area[NORTH] + res.f_area[BOTTOM] + res.f_area[TOP] ;
                    radianciatomb.set(rad,x,y,z);
                    luminanciatomb.set(lum,x,y,z);
                    areatomb.set(area,x,y,z);
                }
            }
            else
                for (u32 iv = 0; iv<7; iv++)
                    pad->getref(N).t[iv] = 26.0; // A FIM-be ambient + 26 ker�l az internal boundaryra
        }
    }
    if(is_el){
        elozo_full_I_semi = full_I_semi;
        elozo_full_I_semi.neg();
    }
    if(!is_el){
        sim.bou_update(t_drmap[par.T_azon],&t_drP_temp);
    }
    delete pTemp; // ideiglenes h�m�rs�kleti t�rk�p t�rl�se
}


//***********************************************************************
void masina::get_all_matrix_dc(uns u_mx_azon,bool is_i,uns i_mx_azon){
//***********************************************************************
    
    // el�k�sz�t�s

//    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    tomb3d<dcomplex_cell_res> *pac=NULL,*paci=NULL;
    const model & mod=*par.sim->pmodel;
    const simulation & sim=*par.sim;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
//    cuns max=mod.x_res*mod.y_res*mod.z_res;
    if(t_dcmap.size()<=u_mx_azon){
        uns sts=t_dcmap.size();
        t_dcmap.resize(u_mx_azon+1);
        for(uns i=sts;i<=u_mx_azon;i++)t_dcmap[i]=NULL;
    }
    delete t_dcmap[u_mx_azon];
    pac=t_dcmap[u_mx_azon]=new tomb3d<dcomplex_cell_res>;
    pac->resize(x0,y0,z0);
    if(is_i){
        if(t_dcmap.size()<=i_mx_azon){
            uns sts=t_dcmap.size();
            t_dcmap.resize(i_mx_azon+1);
            for(uns i=sts;i<=i_mx_azon;i++)t_dcmap[i]=NULL;
        }
        delete t_dcmap[i_mx_azon];
        paci=t_dcmap[i_mx_azon]=new tomb3d<dcomplex_cell_res>;
        paci->resize(x0,y0,z0);
    }
    uns N=0;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
//    cd fold=is_el?sim.ambiE:sim.ambiT;
    gcsd res;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;
    dccella ** hpdc=t_dc[par.azon]->get0()->gethalo();
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    const tomb3d<real_cell_res> * ptemp=is_start_map?t_drmap[par.T_azon]:NULL;
    const tomb3d<real_cell_res> * pcurr=is_el&&is_current?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpdc[N]!=NULL){

                // inicializ�l�s

                u16 index=0; // az Y ill J adott oldalhoz tartoz� indexe, nincs EXTERNAl

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                gp.x = x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                dccella & aktcella=*hpdc[N];
                dccella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                dcomplex * tu=pac->getref(N).t;
                dcomplex * ti=is_i ? paci->getref(N).t : NULL;
                cdc u_center=tu[EXTERNAL]=d.ub.get(0);

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::get_all_matrix","get_gcsd perem");

                // fesz�lts�gek sz�m�t�sa

                for (u32 iv = 1; iv<7; iv++){
                    if (d.oldalak[t_o[iv]] == 0){ // perem oldal
                        const boundary * b = sim.get_perem(x, y, z, t_o[iv], is_el);
                        cd aktA = (t_o[iv] == WEST || t_o[iv] == EAST) ? A_x : (t_o[iv] == SOUTH || t_o[iv] == NORTH) ? A_y : A_z;
                        switch (b->tipus){
                            case PeremOpen: tu[t_o[iv]] = u_center; break;
                            case PeremU:    tu[t_o[iv]] = b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, true); break;
                            case PeremR:
                            case PeremRU:   tu[t_o[iv]] = u_center * oszto(res.gfull[t_o[iv]], aktA * b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, true)); break;
                        }
                    }
                    else{ tu[t_o[iv]] = d.u.get(index); index += d.oldalak[t_o[iv]]; } // ut�na n�velj�k, mert external+west==east
                    //                    if(!is_el)if(tu[t_o[iv]]<-150.0)tu[t_o[iv]]=-150.0;
                }
                if(index!=d.u.getsiz())throw hiba("masina::get_all_matrix","index!=d.u.getsiz() (%u!=%u)",index,d.u.getsiz());

                // �ramok sz�m�t�sa

                if(is_i){
                    ti[EXTERNAL]=nulla;
                    ti[WEST]  =res.gfull[WEST]  *(tu[WEST]  -u_center);
                    ti[EAST]  =res.gfull[EAST]  *(tu[EAST]  -u_center);
                    ti[SOUTH] =res.gfull[SOUTH] *(tu[SOUTH] -u_center);
                    ti[NORTH] =res.gfull[NORTH] *(tu[NORTH] -u_center);
                    ti[BOTTOM]=res.gfull[BOTTOM]*(tu[BOTTOM]-u_center);
                    ti[TOP]   =res.gfull[TOP]   *(tu[TOP]   -u_center);
                }
            }
        }
    }
}


//***********************************************************************
void masina::get_all_matrix_qr(uns u_mx_azon,bool is_i,uns i_mx_azon){
// A r�gi �ramot haszn�lja f�lvezet�shez, majd fel�l�rja!
//***********************************************************************
    
    // el�k�sz�t�s

//    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    tomb3d<real_cell_res> *pad=NULL,*padi=NULL;
    const model & mod=*par.sim->pmodel;
    simulation & sim=*par.sim;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
//    cuns max=mod.x_res*mod.y_res*mod.z_res;
    if(t_drmap.size()<=u_mx_azon){
        uns sts=t_drmap.size();
        t_drmap.resize(u_mx_azon+1);
        for(uns i=sts;i<=u_mx_azon;i++)t_drmap[i]=NULL;
    }

    // Ne t�r�lj�k le a r�gi h�m�rs�kleteket!

    tomb3d<real_cell_res> *pTemp=NULL;
    if(u_mx_azon==par.T_azon){
        pTemp=t_drmap[u_mx_azon];
        t_drmap[u_mx_azon]=NULL;
    }

    //

    delete t_drmap[u_mx_azon];
    pad=t_drmap[u_mx_azon]=new tomb3d<real_cell_res>;
    pad->resize(x0,y0,z0);
    if(is_i){
        if(t_drmap.size()<=i_mx_azon){
            uns sts=t_drmap.size();
            t_drmap.resize(i_mx_azon+1);
            for(uns i=sts;i<=i_mx_azon;i++)t_drmap[i]=NULL;
        }
        if(t_drmap[i_mx_azon]==NULL || t_drmap[i_mx_azon]->x_size()!=x0 || t_drmap[i_mx_azon]->y_size()!=y0 || t_drmap[i_mx_azon]->z_size()!=z0){
            delete t_drmap[i_mx_azon];
            padi=t_drmap[i_mx_azon]=new tomb3d<real_cell_res>;
            padi->resize(x0,y0,z0);
        }
        padi=t_drmap[i_mx_azon];
    }
    t_drI_temp.resize(x0,y0,z0); // a ki�r�shoz kisz�m�tjuk az �ramokat.
    t_drP_temp.resize(x0,y0,z0); // a ki�r�shoz kisz�m�tjuk az �ramokat.
    uns N=0;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
    const bool is_th_el_Seebeck=par.is_th_el_Seebeck;
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
    cd fold=is_el?nulla:sim.ambiT;
    gcsd res;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;
    qrcella ** hpqr=t_qr[par.azon]->get0()->gethalo();
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    // 2017.01.17. Szerintem az el�z� h�m�rs�klet az, ahov� az old_T_azon mutat
    //    const tomb3d<real_cell_res> * ptemp=is_start_map?(pTemp==NULL?t_drmap[par.T_azon]:pTemp):NULL; // mindig a j� h�m�rs�klettel sz�moljon!
    const tomb3d<real_cell_res> * ptemp = is_start_map ? (pTemp == NULL ? t_drmap[par.T_azon] : t_drmap[par.old_T_azon]) : NULL; // mindig a j� h�m�rs�klettel sz�moljon!
    const tomb3d<real_cell_res> * pcurr=is_el&&is_current?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!
    radianciatomb.expand (x0,y0,z0);
    luminanciatomb.expand(x0,y0,z0);
    areatomb.expand(x0,y0,z0);
    if(is_el)full_I_semi.free();
    if(!is_el){
        sim.bou_reset(sim.ambiT);
    }

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpqr[N]!=NULL){

                // inicializ�l�s

                u16 index=0; // az Y ill J adott oldalhoz tartoz� indexe, nincs EXTERNAl

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                cd t_m[7]={0.0,A_x,A_x,A_y,A_y,A_z,A_z}; // EXTERNAL,WEST,EAST,..
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                qrcella & aktcella=*hpqr[N];
                qrcella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                dbl * tu=pad->getref(N).t;
                dbl * ti=is_i ? padi->getref(N).t : NULL;
                dbl * ti_temp=is_i ? t_drI_temp.getref(N).t : t_drP_temp.getref(N).t; // �ramok
                cd u_center=tu[EXTERNAL]=q2d(d.ub.get(0));

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::get_all_matrix","get_gcsd perem");

                //Seebeck-effektus

                dbl Us[7],Is[7]; // EXTERNAL, WEST, EAST,...
                for(u32 iv=0; iv<7; iv++)Is[iv]=Us[iv]=0.0;
                if(is_el && is_th_el_Seebeck){

                    dbl Hom[7]; // EXTERNAL, WEST, EAST,...
                    for(u32 iv=0; iv<7; iv++) Hom[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;

                    // seebeck feszek

                    if(is_th_el_Seebeck){
                        dbl TC=Hom[EXTERNAL];
                        for(u32 iv=1; iv<7; iv++) 
                            Us[iv] = (Is[iv] = res.s[iv] * res.gfull[iv] * (Hom[iv]-TC)) / res.gfull[iv];
                    }
                }

                // fesz�lts�gek sz�m�t�sa

                for(u32 iv=1; iv<7; iv++){
                    if(d.oldalak[t_o[iv]]==0){ // perem oldal
                        const boundary * b=sim.get_perem(x,y,z,t_o[iv],is_el);
                        cd aktA=(t_o[iv]==WEST||t_o[iv]==EAST)?A_x:(t_o[iv]==SOUTH||t_o[iv]==NORTH)?A_y:A_z;
                        switch(b->tipus){
                            case PeremOpen: tu[t_o[iv]]=u_center+Us[t_o[iv]]; break;
                            case PeremU:    tu[t_o[iv]]=b->ertek(x,y,z,t_o[iv],is_el,mod,aktA,true)-fold; break;
                            case PeremR:    tu[t_o[iv]]=(u_center+Us[t_o[iv]])*oszto(res.gfull[t_o[iv]],t_m[iv]*b->ertek(x,y,z,t_o[iv],is_el,mod,aktA,true)); break;
                            case PeremRU:{
                                    cd a_Us = Us[t_o[iv]];
                                    cd a_gfull = res.gfull[t_o[iv]];
                                    cd a_A = t_m[iv];
                                    cd a_h = b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, true);
                                    cd a_gperem = a_A * a_h;
                                    cd a_o = oszto(a_gfull, a_gperem);
                                    cd a_o2 = oszto(a_gperem, a_gfull);
                                    cd a_res = (u_center + a_Us) * a_o + (b->ertek2() - fold) * a_o2;
                                    tu[t_o[iv]] = a_res;
                                }
                                break;
                        }
                    }else{tu[t_o[iv]]=q2d(d.u.get(index)); index+=d.oldalak[t_o[iv]];} // ut�na n�velj�k, mert external+west==east
                }
                if(index!=d.u.getsiz())throw hiba("masina::get_all_matrix","index!=d.u.getsiz() (%u!=%u)",index,d.u.getsiz());

                // �ramok sz�m�t�sa

                ti_temp[EXTERNAL]=nulla;
                for(u32 iv=1; iv<7; iv++)
                    ti_temp[t_o[iv]]  =res.gfull[t_o[iv]]  *(tu[t_o[iv]]  -u_center) - Is[t_o[iv]];
                if(is_i){
                    ti[EXTERNAL]=nulla;
                    for(u32 iv=1; iv<7; iv++)
                        ti[t_o[iv]]  =res.gfull[t_o[iv]]  *(tu[t_o[iv]]  -u_center) - Is[t_o[iv]];
                }
                if(is_el){
                    double rad = res.f_rad[WEST] + res.f_rad[EAST] + res.f_rad[SOUTH] + res.f_rad[NORTH] + res.f_rad[BOTTOM] + res.f_rad[TOP] ;
                    double lum = res.f_lum[WEST] + res.f_lum[EAST] + res.f_lum[SOUTH] + res.f_lum[NORTH] + res.f_lum[BOTTOM] + res.f_lum[TOP] ;
                    double area = res.f_area[WEST] + res.f_area[EAST] + res.f_area[SOUTH] + res.f_area[NORTH] + res.f_area[BOTTOM] + res.f_area[TOP] ;
                    radianciatomb.set(rad,x,y,z);
                    luminanciatomb.set(lum,x,y,z);
                    areatomb.set(area,x,y,z);
                }
            }
        }
    }
    if(is_el){
        elozo_full_I_semi = full_I_semi;
        elozo_full_I_semi.neg();
    }
    if(!is_el){
        sim.bou_update(t_drmap[par.T_azon],&t_drP_temp);
    }
    delete pTemp; // ideiglenes h�m�rs�kleti t�rk�p t�rl�se
}


//***********************************************************************
void masina::get_all_matrix_qc(uns u_mx_azon,bool is_i,uns i_mx_azon){
//***********************************************************************
    
    // el�k�sz�t�s

//    cuns melyik=uns(par.is_ac)+uns(par.is_quad)+uns(par.is_quad);
    tomb3d<dcomplex_cell_res> *pac=NULL,*paci=NULL;
    const model & mod=*par.sim->pmodel;
    const simulation & sim=*par.sim;
    cuns x0=mod.x_res, y0=mod.y_res, z0=mod.z_res;
//    cuns max=mod.x_res*mod.y_res*mod.z_res;
    if(t_dcmap.size()<=u_mx_azon){
        uns sts=t_dcmap.size();
        t_dcmap.resize(u_mx_azon+1);
        for(uns i=sts;i<=u_mx_azon;i++)t_dcmap[i]=NULL;
    }
    delete t_dcmap[u_mx_azon];
    pac=t_dcmap[u_mx_azon]=new tomb3d<dcomplex_cell_res>;
    pac->resize(x0,y0,z0);
    if(is_i){
        if(t_dcmap.size()<=i_mx_azon){
            uns sts=t_dcmap.size();
            t_dcmap.resize(i_mx_azon+1);
            for(uns i=sts;i<=i_mx_azon;i++)t_dcmap[i]=NULL;
        }
        delete t_dcmap[i_mx_azon];
        paci=t_dcmap[i_mx_azon]=new tomb3d<dcomplex_cell_res>;
        paci->resize(x0,y0,z0);
    }
    uns N=0;
    const bool is_therm=par.tipus==FieldTherm;
    const bool is_el=par.tipus==FieldEl;
    const bool is_start_map=par.is_start_map;
    const bool is_current=par.is_current;
    const bool is_lin=sim.is_lin;
    const bool is_no_semi=sim.is_no_semi;
    const bool is_semi_init=par.is_semi_init;
    cd ambiT=par.sim->ambiT;
    cuns ter=is_therm?1:0;
//    cd fold=is_el?sim.ambiE:sim.ambiT;
    gcsd res;
    gcsd_par gp;
    gp.is_lin=is_lin;
    gp.is_no_semi=is_no_semi;
    gp.ter=ter;
    gp.is_semi_init=is_semi_init;
    gp.ndc_init_I=sim.tanal[par.anal_index].ndc_I0;
    qccella ** hpqc=t_qc[par.azon]->get0()->gethalo();
    real_cell_res I_dummy;
    for(uns i=0;i<BASIC_SIDES;i++)I_dummy.t[i]=nulla;
    const tomb3d<real_cell_res> * ptemp=is_start_map?t_drmap[par.T_azon]:NULL;
    const tomb3d<real_cell_res> * pcurr=is_el&&is_current?t_drmap[par.i_azon]:NULL; // figyelem! drmap mindig!

    // felt�lt�s

    for(uns z=0;z<z0;z++){
        gp.z=z;
        for(uns y=0;y<y0;y++){
            cd y_pit=mod.y_pit[y].get(nulla);
            gp.y=y;
            gp.y_meret=y_pit;
            for(uns x=0;x<x0;x++,N++)if(hpqc[N]!=NULL){

                // inicializ�l�s

                u16 index=0; // az Y ill J adott oldalhoz tartoz� indexe, nincs EXTERNAl

                cd z_pit=mod.z_pit[z].get(mod.x_hossz[2*x]);
                gp.z_meret=z_pit;
                cd A_x=y_pit*z_pit;
                gp.Ax=A_x;
                cd x_pit=mod.x_pit[x].get(nulla), A_y=x_pit*z_pit, A_z=x_pit*y_pit;
                gp.x=x;
                gp.x_meret=x_pit;
                gp.Ay=A_y;
                gp.Az=A_z;
                qccella & aktcella=*hpqc[N];
                qccella::celladat & d=*aktcella.d;
                cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
                const color & cella_color=mod.tcolor[cella_szin];
                gp.cc=&cella_color;
                const real_cell_res & I=is_el&&is_current?pcurr->getconstref(N):I_dummy;
                for(u32 iv=0; iv<7; iv++) 
                    gp.Temp[iv] = is_start_map ? ptemp->getconstref(N).t[iv] + ambiT : ambiT;
                gp.I=&I;
                dcomplex * tu=pac->getref(N).t;
                dcomplex * ti=is_i ? paci->getref(N).t : NULL;
                cdc u_center=tu[EXTERNAL]=qc2dc(d.ub.get(0));

                // cell�n bel�li vezet�sek sz�m�t�sa

                if(!mod.get_gcsd(gp,res,&mod.tseged[N]))throw hiba("masina::get_all_matrix","get_gcsd perem");

                // fesz�lts�gek sz�m�t�sa

                for (u32 iv = 1; iv<7; iv++){
                    if (d.oldalak[t_o[iv]] == 0){ // perem oldal
                        const boundary * b = sim.get_perem(x, y, z, t_o[iv], is_el);
                        cd aktA = (t_o[iv] == WEST || t_o[iv] == EAST) ? A_x : (t_o[iv] == SOUTH || t_o[iv] == NORTH) ? A_y : A_z;
                        switch (b->tipus){
                        case PeremOpen: tu[t_o[iv]] = u_center; break;
                        case PeremU:    tu[t_o[iv]] = b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, true); break;
                        case PeremR:
                        case PeremRU:   tu[t_o[iv]] = u_center * oszto(res.gfull[t_o[iv]], aktA * b->ertek(x, y, z, t_o[iv], is_el, mod, aktA, true)); break;
                        }
                    }
                    else{ tu[t_o[iv]] = qc2dc(d.u.get(index)); index += d.oldalak[t_o[iv]]; } // ut�na n�velj�k, mert external+west==east
                }
                if(index!=d.u.getsiz())throw hiba("masina::get_all_matrix","index!=d.u.getsiz() (%u!=%u)",index,d.u.getsiz());

                // �ramok sz�m�t�sa

                if(is_i){
                    ti[EXTERNAL]=nulla;
                    ti[WEST]  =res.gfull[WEST]  *(tu[WEST]  -u_center);
                    ti[EAST]  =res.gfull[EAST]  *(tu[EAST]  -u_center);
                    ti[SOUTH] =res.gfull[SOUTH] *(tu[SOUTH] -u_center);
                    ti[NORTH] =res.gfull[NORTH] *(tu[NORTH] -u_center);
                    ti[BOTTOM]=res.gfull[BOTTOM]*(tu[BOTTOM]-u_center);
                    ti[TOP]   =res.gfull[TOP]   *(tu[TOP]   -u_center);
                }
            }
        }
    }
}


//***********************************************************************
inline uns lk2hatvany(uns ertek){
//***********************************************************************
    uns n=1,step=1u;
    for(;step<<=1;n++);
    for(u32 i=0;i<n;i++)if(ertek<=(1u<<i))return i;
    throw hiba("legkisebb2hatvanyamibebelefer()","impossibility");
}


#pragma pack(push)
#pragma pack(8)
//***********************************************************************
typedef struct { int kgrid; /* gridsize=2^kgrid       */
         int kresol;        /* field size =2^kresol   */
         int nlay;          /* # of the layers        */
         int analtype;      /* analysis type 0...4    */
         int nstep;         /* sequence No.           */
         double t;          /* time value [s]         */
         double dt;         /* arriving time-step     */
         double f;          /* frequency [Hz]         */
         double angle;      /* for timeconst [rad]    */
         int douim;         /* 0/1=normal/double im   */
         int sizeofdata;    /* double/complex (8/16)  */
         } resultheadtype;
//***********************************************************************
#pragma pack(pop)
//***********************************************************************

// WINFOSON
#ifdef _WIN32
    #include <direct.h>

//UNIX/POSIX
#else
    #include <sys/stat.h>
    
    //hogy ne kelljen a kodon belul vacakolni a mode_t-vel
    static int mkdir(const char *path)
    {
        return mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }

#endif

//***********************************************************************
inline dbl suruseg(dbl P,dbl A){
//***********************************************************************
    return A==0.0 ? 0.0 : P/A ;
}

//***********************************************************************
void backup_restore_file(const PLString & FajlNev, bool restore){
//***********************************************************************
    FILE * fp_in =  fopen(restore ? (FajlNev + ".backup").c_str() : FajlNev.c_str(), "rb");
    FILE * fp_out = fopen(restore ? FajlNev.c_str() : (FajlNev + ".backup").c_str(), "wb");
    if (fp_in == NULL || fp_out == NULL){
        if (fp_in)fclose(fp_in);
        if (fp_out)fclose(fp_out);
        return;
    }
    size_t n;
    do{
        unsigned char t[4096];
        n = fwrite(t, 1, n = fread(t, 1, 4096, fp_in), fp_out);
    } while (n == 4096);
    fclose(fp_in);
    fclose(fp_out);
}


//***********************************************************************
class AramKep {
//***********************************************************************
public:
    //***********************************************************************
    struct Szin{ // sz�n t�rol�sa
    //***********************************************************************
        unsigned sorszam;
        unsigned char r, g, b;
        char nev[25]; 
        Szin() :sorszam(0), r(0), g(0), b(0) {}
    };
    //***********************************************************************
    struct Cella {
    //***********************************************************************
        bool letezik,dummy[3];
        float w,e,s,n,b,t; // west, east, south, north, bottom, top �ramok
        float homerseklet;
        unsigned sorszam;
        Cella() :letezik(false), w(0.0), e(0.0), s(0.0), n(0.0), b(0.0), t(0.0), 
            homerseklet(0.0), sorszam(0) { dummy[0] = dummy[1] = dummy[2] = false; }
    };
    struct Koordinata { float x, y, z; };
    struct Cella_koordinata {
        Koordinata x0y0z0;
        Koordinata x1y0z0;
        Koordinata x0y1z0;
        Koordinata x1y1z0;
        Koordinata x0y0z1;
        Koordinata x1y0z1;
        Koordinata x0y1z1;
        Koordinata x1y1z1;
    };
    enum Tertipus {Elektromos, Termikus};
private:
    Tertipus tipus;
    unsigned x, y, z; //felbont�s
    float *xpitch, *ypitch, *zpitch; // oszt�sk�z�k m�terben
    unsigned n_paletta; // A sz�nek sz�ma
    Szin *paletta;
    Cella *aramok;
    char hibauzenet[256];
public:
    //***********************************************************************
    AramKep() :tipus(Termikus), x(0), y(0), z(0), xpitch(NULL), ypitch(NULL), zpitch(NULL), n_paletta(0), paletta(NULL), aramok(NULL) {}
    //***********************************************************************
    ~AramKep() { delete[]xpitch; delete[]ypitch; delete[]zpitch; delete[]paletta; delete[]aramok; }
    //***********************************************************************
    void setTerTipus(Tertipus tip) { tipus = tip; }
    //***********************************************************************
    Tertipus getTerTipus()const { return tipus; }
    //***********************************************************************
    void setFelbontas(unsigned X, unsigned Y, unsigned Z) {
    //***********************************************************************
        x = X; y = Y; z = Z;
        delete[]xpitch; delete[]ypitch; delete[]zpitch; delete[]aramok;
        xpitch = new float[x];
        ypitch = new float[y];
        zpitch = new float[z];
        aramok = new Cella[x*y*z];
    }
    //***********************************************************************
    unsigned getFelbontasX()const { return x; }
    //***********************************************************************
    unsigned getFelbontasY()const { return y; }
    //***********************************************************************
    unsigned getFelbontasZ()const { return z; }
    //***********************************************************************
    void setXPitch(unsigned index, float ertek) { xpitch[index] = ertek; }
    //***********************************************************************
    void setYPitch(unsigned index, float ertek) { ypitch[index] = ertek; }
    //***********************************************************************
    void setZPitch(unsigned index, float ertek) { zpitch[index] = ertek; }
    //***********************************************************************
    float getXPitch(unsigned index) const { return xpitch[index]; }
    //***********************************************************************
    float getYPitch(unsigned index) const { return ypitch[index]; }
    //***********************************************************************
    float getZPitch(unsigned index) const { return zpitch[index]; }
    //***********************************************************************
    void setPalettaMeret(unsigned n) { n_paletta = n; delete[]paletta; paletta = new Szin[n]; }
    //***********************************************************************
    unsigned getPalettaMeret()const { return n_paletta; }
    //***********************************************************************
    void setPalettaElem(unsigned index, unsigned sorszam, const char *nev, unsigned char r, unsigned char g, unsigned char b) {
    //***********************************************************************
        Szin &e = paletta[index];
        e.sorszam = sorszam; e.r = r; e.g = g; e.b = b;
        strncpy(e.nev, nev, 24);
        e.nev[24] = 0;
    }
    //***********************************************************************
    const Szin &getPalettaElem(unsigned index)const { return paletta[index]; }
    //***********************************************************************
    void setCella(unsigned X, unsigned Y, unsigned Z, float w, float e, 
        float s, float n, float b, float t, float homerseklet, unsigned sorszam) {
    //***********************************************************************
        Cella &c = aramok[Z*x*y + Y*x + X];
        c.letezik = true;
        c.w = w; c.e = e; c.s = s; c.n = n; c.b = b; c.t = t;
        c.homerseklet = homerseklet;
        c.sorszam = sorszam;
    }
    //***********************************************************************
    const Cella &getCella(unsigned X, unsigned Y, unsigned Z)const { return aramok[Z*x*y + Y*x + X]; }
    //***********************************************************************
    Cella_koordinata getCella_koordinata(unsigned X, unsigned Y, unsigned Z)const {
        Cella_koordinata c;
        c.x0y0z0.x = c.x0y0z1.x = c.x0y1z0.x = c.x0y1z1.x = (X == 0) ? 0.0f : xpitch[X - 1];
        c.x1y0z0.x = c.x1y0z1.x = c.x1y1z0.x = c.x1y1z1.x = xpitch[X];
        c.x0y0z0.y = c.x0y0z1.y = c.x1y0z0.y = c.x1y0z1.y = (Y == 0) ? 0.0f : ypitch[Y - 1];
        c.x0y1z0.y = c.x0y1z1.y = c.x1y1z0.y = c.x1y1z1.y = ypitch[Y];
        c.x0y0z0.z = c.x0y1z0.z = c.x1y0z0.z = c.x1y1z0.z = (Z == 0) ? 0.0f : zpitch[Z - 1];
        c.x0y0z1.z = c.x0y1z1.z = c.x1y0z1.z = c.x1y1z1.z = zpitch[Z];
        return c;
    }
    //***********************************************************************
    bool Save(const char *FajlNev)const {
    //***********************************************************************
        FILE *fp = fopen(FajlNev, "wb");
        if (fp == NULL) return false;
        
        if (fwrite("VSUNCUR", 1, 8, fp) != 8) return false;
        if (fwrite(&tipus, sizeof(Tertipus), 1, fp) != 1) return false;
        if (fwrite(&x, sizeof(unsigned), 1, fp) != 1) return false;
        if (fwrite(&y, sizeof(unsigned), 1, fp) != 1) return false;
        if (fwrite(&z, sizeof(unsigned), 1, fp) != 1) return false;
        if (fwrite(xpitch, sizeof(float), x, fp) != x) return false;
        if (fwrite(ypitch, sizeof(float), y, fp) != y) return false;
        if (fwrite(zpitch, sizeof(float), z, fp) != z) return false;
        if (fwrite(&n_paletta, sizeof(unsigned), 1, fp) != 1) return false;
        if (fwrite(paletta, sizeof(Szin), n_paletta, fp) != n_paletta) return false;
        if (fwrite(aramok, sizeof(Cella), x*y*z, fp) != x*y*z) return false;
        return true;
    }
    //***********************************************************************
    bool Load(const char *FajlNev) {
    //***********************************************************************
        char azon[8];
        strcpy(hibauzenet, "");
        FILE *fp = fopen(FajlNev, "rb");
        if (fp == NULL) { strcpy(hibauzenet, "Cannot open file: "); strcat(hibauzenet,FajlNev); return false; }
        if (fread(azon, 1, 8, fp) != 8) { strcpy(hibauzenet, "Cannot read VSUNCUR"); return false; }
        if (strcmp(azon, "VSUNCUR") != 0) { strcpy(hibauzenet, "VSUNCUR missing"); return false; }
        if (fread(&tipus, sizeof(Tertipus), 1, fp) != 1) { strcpy(hibauzenet, "Field type is missing"); return false; }
        x = y = z = n_paletta = 0;
        delete[]xpitch; delete[]ypitch; delete[]zpitch; delete[]paletta; delete[]aramok;
        xpitch = ypitch = zpitch = NULL; paletta = NULL; aramok = NULL;
        if (fread(&x, sizeof(unsigned), 1, fp) != 1) { strcpy(hibauzenet, "x read error"); return false; }
        if (fread(&y, sizeof(unsigned), 1, fp) != 1) { strcpy(hibauzenet, "y read error"); return false; }
        if (fread(&z, sizeof(unsigned), 1, fp) != 1) { strcpy(hibauzenet, "z read error"); return false; }
        xpitch = new float[x]; ypitch = new float[y]; zpitch = new float[z];
        if (fread(xpitch, sizeof(float), x, fp) != x) { strcpy(hibauzenet, "xpitch read error"); return false; }
        if (fread(ypitch, sizeof(float), y, fp) != y) { strcpy(hibauzenet, "xpitch read error"); return false; }
        if (fread(zpitch, sizeof(float), z, fp) != z) { strcpy(hibauzenet, "xpitch read error"); return false; }
        if (fread(&n_paletta, sizeof(unsigned), 1, fp) != 1) { strcpy(hibauzenet, "palette num read error"); return false; }
        paletta = new Szin[n_paletta];
        if (fread(paletta, sizeof(Szin), n_paletta, fp) != n_paletta) { strcpy(hibauzenet, "palette read error"); return false; }
        aramok = new Cella[x*y*z];
        if (fread(aramok, sizeof(Cella), x*y*z, fp) != x*y*z) { strcpy(hibauzenet, "Cannot read cells."); return false; }
        return true;
    }
    const char *getHiba()const { return hibauzenet; }
};


//***********************************************************************
void masina::set_aramkep_fej(AramKep &k)const{
//***********************************************************************
    const simulation & sim = *par.sim;
    const model & mod = *par.sim->pmodel;
    cuns x = mod.x_res, y = mod.y_res, z = mod.z_res;
    
    k.setTerTipus(par.tipus == FieldEl ? AramKep::Elektromos : AramKep::Termikus);
    k.setFelbontas(x, y, z);
    
    for (uns i = 0; i < x; i++)
        k.setXPitch(i, (float)mod.x_pit[i].get(nulla));
    for (uns i = 0; i < y; i++)
        k.setYPitch(i, (float)mod.y_pit[i].get(nulla));
    for (uns i = 0; i < z; i++)
        k.setZPitch(i, (float)mod.z_pit[i].get(nulla));
    
    uns colnum = 0;
    for (uns i = 0; i < colmax;i++)
        if (mod.tcolor[i].is && mod.tcolor[i].tipus==SzinNormal)
            colnum++;
    k.setPalettaMeret(colnum);
    
    colnum = 0;
    for (uns i = 0; i < colmax;i++)
        if (mod.tcolor[i].is && mod.tcolor[i].tipus == SzinNormal) {
            k.setPalettaElem(colnum, i, mod.tcolor[i].nev.c_str(), mod.tbmp[0].getpal(i).rgbRed, mod.tbmp[0].getpal(i).rgbGreen, mod.tbmp[0].getpal(i).rgbBlue);
            colnum++;
        }
}

//***********************************************************************
void masina::get_center_values(tomb<dbl>& cel) const{
//***********************************************************************
    const bool is_el = par.tipus == FieldEl;
    const tomb3d<real_cell_res> *ptemp = t_drmap[is_el ? par.u_azon : par.T_azon];
    const model & mod = *par.sim->pmodel;
    cuns x = mod.x_res, y = mod.y_res, z = mod.z_res;
    cuns N = x*y*z;
    cel.resize(N);
    for (uns i = 0; i < N; i++)
        cel[i] = ptemp->getconstref(i).t[EXTERNAL];
}

bool is_transi_step_as_dc = false;

//***********************************************************************
void masina::write_fim(const PLString & path,uns step,bool iternormal,uns analtype,bool restore){
// ha iternormal=true, akkor az iter�ci� el�rte a be�ll�tott limitet
//***********************************************************************
      setAnalKeszStep(0,step,"write fim");
    const simulation & sim=*par.sim;
    const model & mod=*par.sim->pmodel;
    cuns x=mod.x_res, y=mod.y_res, z=mod.z_res;

    uns wxy=(1u<<lk2hatvany(x)>1u<<lk2hatvany(y))?1u<<lk2hatvany(x):1u<<lk2hatvany(y);
    uns wz=1u<<lk2hatvany(z);

    if (sim.FIM_res_xy > wxy)
        wxy = sim.FIM_res_xy;
    if (sim.FIM_res_z > wz)
        wz = sim.FIM_res_z;

    cuns dx = sim.FIM_diff_x + x > wxy ? 0 : sim.FIM_diff_x;
    cuns dy = sim.FIM_diff_y + y > wxy ? 0 : sim.FIM_diff_y;
    cuns dz = sim.FIM_diff_z + z > wz  ? 0 : sim.FIM_diff_z;

    resultheadtype fej;
    memset(&fej,0,sizeof(fej));

    fej.kgrid=lk2hatvany(wxy*wz);
    fej.kresol=lk2hatvany(wxy);
    fej.nlay=wz;

    fej.analtype = is_transi_step_as_dc&&par.is_transi ? 0 : analtype;
    fej.sizeofdata=par.is_ac?sizeof(dcomplex):sizeof(dbl);

    fej.nstep = is_transi_step_as_dc&&par.is_transi ? 1 : par.anal_step;
    fej.t=par.t;
    fej.dt=par.dt;
    fej.f=par.is_ac? par.f : 0.0;
    fej.angle=0.0;
    fej.douim=0;

    // f�jl �r�sa

    FILE *fp;
    char t[20];
    const bool is_el=par.tipus==FieldEl;
    cd fold=is_el?nulla:sim.ambiT;

    PLString nev = path + ( is_el ? "electrical" : "thermal" );
    mkdir(nev.c_str());
    nev+="/";
    extern FILE *fpsok;
    
    if (is_transi_step_as_dc&&par.is_transi) {
        uns egesz = uns(par.t), tort;
        tort = uns((par.t - uns(par.t)) * 1000000000 + 0.01);
        sprintf(t, "0001_%06u_%09u", egesz, tort);
    }
    else
        sprintf(t, "%04u", fej.nstep);

    if(par.is_ac){
        const tomb3d<dcomplex_cell_res> * ptemp=t_dcmap[is_el?par.u_azon:par.T_azon];
        if(!sim.is_nofim){
            cuns db=2*wxy*wxy*wz+wxy*wxy;
            cuns Ldb=wxy*wxy;
            cuns Hdb=Ldb*wz;
            uns N=0;
            dcomplex * tor=new dcomplex[db];
            dcomplex * fim2=tor+Hdb;
            dcomplex * fim3=fim2+Hdb;

            for (uns i = 0;i < db;i++)tor[i] = nulla;
            for(uns i=0;i<z;i++){
                uns layelt=Ldb*(i+dz);
                for(u32 j=0;j<y;j++){
                    uns sorelt=wxy*(j+dy);
                    for(uns k=0;k<x;k++,N++){
                        tor[layelt+sorelt+k+dx] =ptemp->getconstref(N).t[EXTERNAL];
                        fim2[layelt+sorelt+k+dx]=ptemp->getconstref(N).t[BOTTOM];
                        fim3[sorelt+k+dx]       =ptemp->getconstref(N).t[TOP]; // csak az utols� r�tegn�l kellene
                    }
                }
            }

            PLString FajlNev=nev+"SRE"+t+(is_el?".FIMe":".FIM");
            if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            fwrite(&fej,sizeof(fej),1,fp);
            fwrite(tor,sizeof(dcomplex),Hdb+Hdb+Ldb,fp);
            fclose(fp);

            delete [] tor;
        }

        // Pr�bok

        bool pont=!sim.is_vesszo,vesszo=sim.is_vesszo;
        PLString FajlNev=nev+((analtype==2) ? "probe.bode" : (analtype==3) ? "probe.timeconst" : "probe.ac");
        if(par.anal_step>1){
            if((fp=fopen(FajlNev.c_str(),"at"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
        }
        else{
            if((fp=fopen(FajlNev.c_str(),"wt"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
        }

        // pitch

        PLString sor="# U/T x=\t0\t";
        dbl hossz=0.0;
        if(par.anal_step<=1){
            for(uns i=0; i<x; i++){
                sor = sor + mod.x_hossz[i*2] + "\t" + mod.x_hossz[i*2+1] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());

            sor="# I/P/DISS/RAD/LUM x=\t";
            hossz=0.0;
            for(uns i=0; i<x; hossz+=mod.x_pit[i++].get(nulla)){
                sor = sor + mod.x_hossz[i*2] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());

            sor="# U/T y=\t0\t";
            hossz=0.0;
            for(uns i=0; i<y; hossz+=mod.y_pit[i++].get(nulla)){
                sor = sor + mod.y_hossz[i*2] + "\t" + mod.y_hossz[i*2+1] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());

            sor="# I/P/DISS/RAD/LUM y=\t";
            hossz=0.0;
            for(uns i=0; i<y; hossz+=mod.y_pit[i++].get(nulla)){
                sor = sor + mod.y_hossz[i*2] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());

            sor="# U/T z=\t0\t";
            hossz=0.0;
            for(uns i=0; i<z; hossz+=mod.z_pit[i++].get(nulla)){
                sor = sor + mod.z_hossz[i*2] + "\t" + mod.z_hossz[i*2+1] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());

            sor="# I/P/DISS/RAD/LUM z=\t";
            hossz=0.0;
            for(uns i=0; i<z; hossz+=mod.z_pit[i++].get(nulla)){
                sor = sor + mod.z_hossz[i*2] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());
        }

        // most m�r t�nyleg a pr�bok

        if(analtype==2){
            sor=PLString("\nf[Hz]=\t")+par.f;
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            if(pont)fprintf(fpsok,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());
            if(vesszo)fprintf(fpsok,"%s\n",sor.c_str());
        }
        if(analtype==3){
            sor=PLString("\ntau[s]=\t")+(1.0/(2.0*M_PI*par.f));
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            if(pont)fprintf(fpsok,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());
            if(vesszo)fprintf(fpsok,"%s\n",sor.c_str());
        }
        if(is_el){
            for(uns i=0;i<sim.tproV.size();i++){
                cuns xt=sim.tproV[i].x, yt=sim.tproV[i].y, zt=sim.tproV[i].z;
                Oldal ot = (sim.tproV[i].oldal == CENTER) ? EXTERNAL : sim.tproV[i].oldal;
                if(analtype==3){
                    sor=sim.tproV[i].cimke+"=\t"+((ptemp->getconstref(xt,yt,zt).t[ot]+fold).im*log(10.0)/M_PI);
                }
                else{
                    sor=sim.tproV[i].cimke+"=\t"+(ptemp->getconstref(xt,yt,zt).t[ot]+fold).re+"\t"+(ptemp->getconstref(xt,yt,zt).t[ot]+fold).im;
                }
                if(pont)fprintf(fp,"%s\n",sor.c_str());
                if(pont)fprintf(fpsok,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
                if(vesszo)fprintf(fpsok,"%s\n",sor.c_str());
            }
        }
        else{
           for(uns i=0;i<sim.tproT.size();i++){
                cuns xt=sim.tproT[i].x, yt=sim.tproT[i].y, zt=sim.tproT[i].z;
                Oldal ot = (sim.tproT[i].oldal == CENTER) ? EXTERNAL : sim.tproT[i].oldal;
                if(analtype==3){
                    dcomplex temp=ptemp->getconstref(xt,yt,zt).t[ot];
                    sor=sim.tproT[i].cimke+"=\t"+((ptemp->getconstref(xt,yt,zt).t[ot]+fold).im*log(10.0)/M_PI);
                }
                else{
                    sor=sim.tproT[i].cimke+"=\t"+(ptemp->getconstref(xt,yt,zt).t[ot]+fold).re+"\t"+(ptemp->getconstref(xt,yt,zt).t[ot]+fold).im;
                }
                if(pont)fprintf(fp,"%s\n",sor.c_str());
                if(pont)fprintf(fpsok,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
                if(vesszo)fprintf(fpsok,"%s\n",sor.c_str());
            }
        }
        fclose(fp);
    }
    else{
        PLString FajlNev;
        const tomb3d<real_cell_res> *ptemp = t_drmap[ is_el ? par.u_azon : par.T_azon ];
        hizotomb aramtomb;
        uns N=0;
        AramKep ak;
        set_aramkep_fej(ak);
        if(!sim.is_nofim||sim.tproS.size()>0){
            cuns db=2*wxy*wxy*wz+wxy*wxy;
            cuns Ldb=wxy*wxy;
            cuns Hdb=Ldb*wz;
            cuns db_i=8*Hdb;
            dbl * tor=new dbl[db];
            dbl * fim2=tor+Hdb;
            dbl * fim3=fim2+Hdb;
            dbl * tikx=new dbl[db_i];
            dbl * tiky=tikx+Hdb;
            dbl * tikz=tiky+Hdb;
            dbl * tikc=tikz+Hdb;
            dbl * tikd=tikc+Hdb;
            dbl * tiax=tikd+Hdb;
            dbl * tiay=tiax+Hdb;
            dbl * tiaz=tiay+Hdb;
    //double dimbi=0.0,Ijunction=0.0;
            for (uns i = 0;i < db;i++)tor[i] = 50; //nulla; // 50 ker�l a nem kisz�m�tott t�rfogatr�szekre, l�sd m�g a 2610. sort
            for(uns i=0;i<z;i++){
                uns layelt=Ldb*(i+dz);
                for(u32 j=0;j<y;j++){
                    uns sorelt=wxy*(j+dy);
                    cd y_pit=mod.y_pit[j].get(nulla);
                    for(uns k=0;k<x;k++,N++){
                        cd z_pit=mod.z_pit[i].get(mod.x_hossz[2*k]);
                        cd x_pit=mod.x_pit[k].get(nulla);
                        tor[layelt+sorelt+k+dx] =ptemp->getconstref(N).t[EXTERNAL]+fold;
//if(tor[layelt+sorelt+k+dx]==0.0) tor[layelt+sorelt+k+dx]=5.2;
                        fim2[layelt+sorelt+k+dx]=ptemp->getconstref(N).t[BOTTOM]+fold;
                        fim3[sorelt+k+dx]       =ptemp->getconstref(N).t[TOP]+fold;  // csak az utols� r�tegn�l kellene
                        cd * t =is_el ? t_drI_temp.getconstref(N).t : t_drP_temp.getconstref(N).t;
                        // az �ramok sz�m�t�s�t nem verifik�ltam!
                        tiax[layelt+sorelt+k+dx]=fabs(tikx[layelt+sorelt+k+dx]=t[EAST]/(y_pit*z_pit));
                        tiay[layelt+sorelt+k+dx]=fabs(tiky[layelt+sorelt+k+dx]=t[NORTH]/(x_pit*z_pit));
                        tiaz[layelt+sorelt+k+dx]=fabs(tikz[layelt+sorelt+k+dx]=t[TOP]/(x_pit*y_pit));
                        //tiax[layelt+sorelt+k+dx]=fabs(tikx[layelt+sorelt+k+dx]=t[EAST]);
                        //tiay[layelt+sorelt+k+dx]=fabs(tiky[layelt+sorelt+k+dx]=t[NORTH]);
                        //tiaz[layelt+sorelt+k+dx]=fabs(tikz[layelt+sorelt+k+dx]=t[TOP]);
                        tikc[layelt+sorelt+k+dx]=sqrt(negyzet((t[EAST]-t[WEST])/(y_pit*z_pit))+negyzet((t[NORTH]-t[SOUTH])/(x_pit*z_pit))+negyzet((t[TOP]-t[BOTTOM])/(x_pit*y_pit)));
                        aramtomb.set(tikc[layelt+sorelt+k+dx],k,j,i);
                        tikd[layelt+sorelt+k+dx]=(t[WEST]+t[EAST]+t[SOUTH]+t[NORTH]+t[BOTTOM]+t[TOP])/(x_pit*y_pit*z_pit);

                        cuns szinszam = mod.tbmp[i].getpixel_also(k, j);
                        if(mod.tcolor[szinszam].is && mod.tcolor[szinszam].tipus==SzinNormal)
                            ak.setCella(k, j, i, (float)t[WEST], (float)t[EAST], (float)t[SOUTH], (float)t[NORTH], 
                                        (float)t[BOTTOM], (float)t[TOP], (float)(ptemp->getconstref(N).t[EXTERNAL] + fold), szinszam);
                    }
                }
            }
//if(is_el)printf("\n\nAram= %.10g\nFesz= %.10g\nIcella=%.10g\nIfull=%.10g\ndI=%.10g\n\n",
//   dimbi,ptemp->getconstref(56,32,1).t[EXTERNAL]+fold,t_drI_temp.getconstref(64,64,2).t[TOP],Ijunction,dimbi-Ijunction);
////if(is_el)printf("\n\nAram= %.10g\nFesz= %.10g\n\n",dimbi,ptemp->getconstref(56,32,1).t[EXTERNAL]+fold);
//if(is_el)printf("\n\nAram= %.10g\nFesz= %.10g\n\n",dimbi,ptemp->getconstref(14,8,1).t[EXTERNAL]+fold);
//if(is_el)printf("\n\nAram= %.10g\nFesz= %.10g\n\n",dimbi,ptemp->getconstref(21,23,1).t[EXTERNAL]+fold);

            if(!sim.is_nofim){

                // FIM

                FajlNev=nev+"SRE"+t+".FIM"; //path+"SRE"+t+(is_el?".FIMe":".FIM");
                if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
                fwrite(&fej,sizeof(fej),1,fp);
                fwrite(tor,sizeof(double),Hdb+Hdb+Ldb,fp);
                fclose(fp);

                // XIM

        //        FajlNev=path+"SRE"+t+(is_el?".XIMe":".XIM");
                if(analtype<2){
                    FajlNev=nev+"SRE"+t+".XIM";
                    if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
                    fwrite(&fej,sizeof(fej),1,fp);
                    fwrite(tikx,sizeof(double),Hdb,fp);
                    fclose(fp);

                    // YIM

            //        FajlNev=path+"SRE"+t+(is_el?".YIMe":".YIM");
                    FajlNev=nev+"SRE"+t+".YIM";
                    if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
                    fwrite(&fej,sizeof(fej),1,fp);
                    fwrite(tiky,sizeof(double),Hdb,fp);
                    fclose(fp);

                    // ZIM

            //        FajlNev=path+"SRE"+t+(is_el?".ZIMe":".ZIM");
                    FajlNev=nev+"SRE"+t+".ZIM";
                    if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
                    fwrite(&fej,sizeof(fej),1,fp);
                    fwrite(tikz,sizeof(double),Hdb,fp);
                    fclose(fp);

                    // CIM

            //        FajlNev=path+"SRE"+t+(is_el?".CIMe":".CIM");
                    FajlNev=nev+"SRE"+t+".CIM";
                    if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
                    fwrite(&fej,sizeof(fej),1,fp);
                    fwrite(tikc,sizeof(double),Hdb,fp);
                    fclose(fp);

                    // DIM

                    FajlNev=nev+"SRE"+t+".DIM";
            //        FajlNev=path+"SRE"+t+(is_el?".DIMe":".DIM");
                    if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
                    fwrite(&fej,sizeof(fej),1,fp);
                    fwrite(tikd,sizeof(double),Hdb,fp);
                    fclose(fp);

                    // CIMX

            //        FajlNev=path+"SRE"+t+(is_el?".CIMXe":".CIMX");
                    FajlNev=nev+"SRE"+t+".CIMX";
                    if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
                    fwrite(&fej,sizeof(fej),1,fp);
                    fwrite(tiax,sizeof(double),Hdb,fp);
                    fclose(fp);

                    // CIMY

            //        FajlNev=path+"SRE"+t+(is_el?".CIMYe":".CIMY");
                    FajlNev=nev+"SRE"+t+".CIMY";
                    if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
                    fwrite(&fej,sizeof(fej),1,fp);
                    fwrite(tiay,sizeof(double),Hdb,fp);
                    fclose(fp);

                    // CIMZ

            //        FajlNev=path+"SRE"+t+(is_el?".CIMZe":".CIMZ");
                    FajlNev=nev+"SRE"+t+".CIMZ";
                    if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
                    fwrite(&fej,sizeof(fej),1,fp);
                    fwrite(tiaz,sizeof(double),Hdb,fp);
                    fclose(fp);

                    // CUR

                    FajlNev = nev + "current" + t + ".cur";
                    if (!ak.Save(FajlNev.c_str()))
                        throw hiba("masina::writefim()", "file write error: %s", FajlNev.c_str());

                }
            }

            delete [] tor;
            delete [] tikx;
        }

        // F�lvezet�k ter�lete

        const color * tcolor=this->par.sim->pmodel->tcolor;
        double sum_semi_area = 0.0;
        for(unsigned ic=0;ic<colmax+1;ic++)
            if(tcolor[ic].is)
                for(unsigned jc=0; jc<tcolor[ic].tsemi.size(); jc++){
                    sum_semi_area += tcolor[ic].tsemi[jc].As;
//                    printf("\n���� %g %g\n",tcolor[ic].tsemi[jc].As,sum_semi_area);
                }

        // Pr�bok

        bool pont=!sim.is_vesszo,vesszo=sim.is_vesszo;
        FajlNev=nev+((analtype==4) ? "probe.trz" : "probe.dc");
        PLString SzimplaFajlNev = nev + ((analtype == 4) ? "szimpla.trz" : "szimpla.dc");
        backup_restore_file(FajlNev, restore);
        backup_restore_file(SzimplaFajlNev, restore);
        FILE *sfp;
        if(par.anal_step>1){
            if((fp=fopen(FajlNev.c_str(),"at"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            if((sfp=fopen(SzimplaFajlNev.c_str(),"at"))==0)throw hiba("masina::writefim()","cannot open %s",SzimplaFajlNev.c_str());
        }
        else{
            if((fp=fopen(FajlNev.c_str(),"wt"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            if((sfp=fopen(SzimplaFajlNev.c_str(),"wt"))==0)throw hiba("masina::writefim()","cannot open %s",SzimplaFajlNev.c_str());
        }

        // pitch

        PLString sor="# U/T x=\t0\t",sor2;
        dbl hossz=0.0;
        if(par.anal_step<=1){
            for(uns i=0; i<x; i++){
                sor = sor + mod.x_hossz[i*2] + "\t" + mod.x_hossz[i*2+1] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());

            sor="# I/P/DISS/RAD/LUM x=\t";
            hossz=0.0;
            for(uns i=0; i<x; hossz+=mod.x_pit[i++].get(nulla)){
                sor = sor + mod.x_hossz[i*2] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());

            sor="# U/T y=\t0\t";
            hossz=0.0;
            for(uns i=0; i<y; hossz+=mod.y_pit[i++].get(nulla)){
                sor = sor + mod.y_hossz[i*2] + "\t" + mod.y_hossz[i*2+1] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());

            sor="# I/P/DISS/RAD/LUM y=\t";
            hossz=0.0;
            for(uns i=0; i<y; hossz+=mod.y_pit[i++].get(nulla)){
                sor = sor + mod.y_hossz[i*2] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());

            sor="# U/T z=\t0\t";
            hossz=0.0;
            for(uns i=0; i<z; hossz+=mod.z_pit[i++].get(nulla)){
                sor = sor + mod.z_hossz[i*2] + "\t" + mod.z_hossz[i*2+1] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());

            sor="# I/P/DISS/RAD/LUM z=\t";
            hossz=0.0;
            for(uns i=0; i<z; hossz+=mod.z_pit[i++].get(nulla)){
                sor = sor + mod.z_hossz[i*2] + "\t";
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());
        }

        // most m�r t�nyleg a pr�bok

        if(is_el){
            double sum_rad = radianciatomb.sum();
            double sum_lum = luminanciatomb.sum();
            if(par.anal_step<=1){
                fprintf(fp,"# Radiant flux = %.10g W\n# Luminous flux = %.10g lm\n",sum_rad,sum_lum);
                fprintf(fp,"# dissipation = %.10g W\n",fulldissz);
                fprintf(fp,"# semiconductor area (real) = %g\n",sum_semi_area);
            }
            double max_area, max_p;
            radianciatomb.max_suruseg(areatomb,max_p,max_area);
            double cut_area = areatomb.sum_cut(radianciatomb,max_p/max_area*0.02);
            if(par.anal_step<=1){
                fprintf(fp,"# semiconductor area (significant) = %g\n",cut_area); 
            }
            if(analtype==4){
                sor=PLString("\nt[s]=\t")+par.t;
                if(pont)fprintf(fp,"%s\n",sor.c_str());
                if(pont)fprintf(fpsok,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
                if(vesszo)fprintf(fpsok,"%s\n",sor.c_str());
                
                // egyszer� f�jl
                
                sor=PLString("\n")+par.t;
                if(pont)fprintf(sfp,"%s",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(sfp,"%s",sor.c_str());
            }
            for(uns i=0;i<sim.tproV.size();i++){
                cuns xt=sim.tproV[i].x, yt=sim.tproV[i].y, zt=sim.tproV[i].z;
                Oldal ot = (sim.tproV[i].oldal == CENTER) ? EXTERNAL : sim.tproV[i].oldal;
                sor=sim.tproV[i].cimke+"=\t"+(ptemp->getconstref(xt,yt,zt).t[ot]+fold);
                if(pont)fprintf(fp,"%s\n",sor.c_str());
                if(pont)fprintf(fpsok,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
                if(vesszo)fprintf(fpsok,"%s\n",sor.c_str());
                
                // egyszer� f�jl
                
                sor=PLString("\t")+(ptemp->getconstref(xt,yt,zt).t[ot]+fold);
                if(pont)fprintf(sfp,"%s",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(sfp,"%s",sor.c_str());
            }
        }
        else{
            dbl sum_heat_flow=nulla;
            for(u32 i=1;i<7;i++)
                if(par.anal_step<=1){
                    if( sim.normalperem.th[i].conv.is_defined ){
                        cd A=sim.normalperem.th[i].A;
                        cd cHTC=sim.normalperem.th[i].cHTCA/A;
                        cd rHTC=sim.normalperem.th[i].rHTCA/A;
                        fprintf(fp,"# %s convective HTC = %g W/m2K",OldalNev[i],cHTC);
                        fprintf(fp,",     radiant HTC = %g W/m2K",rHTC);
                        fprintf(fp,",     full HTC = %g W/m2K",cHTC+rHTC);
                        fprintf(fp,",     average T = %g K",sim.normalperem.th[i].TA/A);
                        fprintf(fp,",     heat flow = %g W\n",sim.normalperem.th[i].P);
                        sum_heat_flow += sim.normalperem.th[i].P;
                    }
                }
            for(u32 j=0;j<sim.tinner.size();j++){
                for(u32 i=1;i<7;i++)
                    if(par.anal_step<=1){
                        if( sim.tinner[j].th[i].conv.is_defined && sim.tinner[j].th[i].P!=nulla){
                            cd A=sim.tinner[j].th[i].A;
                            cd cHTC=sim.tinner[j].th[i].cHTCA/A;
                            cd rHTC=sim.tinner[j].th[i].rHTCA/A;
                            fprintf(fp,"# color=%u %s convective HTC = %g W/m2K",sim.tinner[j].szin,OldalNev[i],cHTC);
                            fprintf(fp,",     radiant HTC = %g W/m2K",rHTC);
                            fprintf(fp,",     full HTC = %g W/m2K",cHTC+rHTC);
                            fprintf(fp,",     average T = %g K",sim.tinner[j].th[i].TA/A);
                            fprintf(fp,",     heat flow = %g W\n",sim.tinner[j].th[i].P);
                            sum_heat_flow += sim.tinner[j].th[i].P;
                        }
                    }
            }
            if(sum_heat_flow!=nulla)
                fprintf(fp,"# full heat flow = %g W\n",sum_heat_flow);
            if(analtype==4){
                sor=PLString("\nt[s]=\t")+par.t;
                if(pont)fprintf(fp,"%s\n",sor.c_str());
                if(pont)fprintf(fpsok,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
                if(vesszo)fprintf(fpsok,"%s\n",sor.c_str());
                
                // egyszer� f�jl
                
                sor=PLString("\n")+par.t;
                if(pont)fprintf(sfp,"%s",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(sfp,"%s",sor.c_str());
            }
           for(uns i=0;i<sim.tproT.size();i++){
                cuns xt=sim.tproT[i].x, yt=sim.tproT[i].y, zt=sim.tproT[i].z;
                Oldal ot = (sim.tproT[i].oldal == CENTER) ? EXTERNAL : sim.tproT[i].oldal;
                sor=sim.tproT[i].cimke+"=\t"+(ptemp->getconstref(xt,yt,zt).t[ot]+fold);
                if(pont)fprintf(fp,"%s\n",sor.c_str());
                if(pont)fprintf(fpsok,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
                if(vesszo)fprintf(fpsok,"%s\n",sor.c_str());
                
                // egyszer� f�jl
                
                sor=PLString("\t")+(ptemp->getconstref(xt,yt,zt).t[ot]+fold);
                if(pont)fprintf(sfp,"%s",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(sfp,"%s",sor.c_str());
            }
        }

        // section pr�bok

        for(uns i=0;i<sim.tproS.size();i++){
            sor=sim.tproS[i].cimke+(is_el?"_U[V]=\t":"_T[�C]=\t");
            cuns k1=sim.tproS[i].x, k2=sim.tproS[i].y;
            switch(sim.tproS[i].oldal){
                case WEST: // x section
                    sor=sor+(ptemp->getconstref(0,k1,k2).t[WEST]+fold)+"\t";
                    for(uns j=0;j<x;j++)
                        sor=sor+(ptemp->getconstref(j,k1,k2).t[EXTERNAL]+fold)+"\t"+(ptemp->getconstref(j,k1,k2).t[EAST]+fold)+"\t";
                    break;
                case SOUTH: // y section
                    sor=sor+(ptemp->getconstref(k1,0,k2).t[SOUTH]+fold)+"\t";
                    for(uns j=0;j<y;j++)
                        sor=sor+(ptemp->getconstref(k1,j,k2).t[EXTERNAL]+fold)+"\t"+(ptemp->getconstref(k1,j,k2).t[NORTH]+fold)+"\t";
                    break;
                case BOTTOM: // z section
                    sor=sor+(ptemp->getconstref(k1,k2,0).t[BOTTOM]+fold)+"\t";
                    for(uns j=0;j<z;j++)
                        sor=sor+(ptemp->getconstref(k1,k2,j).t[EXTERNAL]+fold)+"\t"+(ptemp->getconstref(k1,k2,j).t[TOP]+fold)+"\t";
                    break;
            }
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());
        }
        for(uns i=0;i<sim.tproS.size();i++){
            sor=sim.tproS[i].cimke+(is_el?"_I[A/m2]=\t":"_P[W/m2]=\t");
            cuns k1=sim.tproS[i].x, k2=sim.tproS[i].y;
            switch(sim.tproS[i].oldal){
                case WEST: // x section
                    for(uns j=0;j<x;j++)
                        sor=sor+(aramtomb.get(j,k1,k2))+"\t";
                    break;
                case SOUTH: // y section
                    for(uns j=0;j<y;j++)
                        sor=sor+(aramtomb.get(k1,j,k2))+"\t";
                    break;
                case BOTTOM: // z section
                    for(uns j=0;j<z;j++)
                        sor=sor+(aramtomb.get(k1,k2,j))+"\t";
                    break;
            }
            if(pont) fprintf(fp,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());
        }
        if(is_el){
            for(uns i=0;i<sim.tproS.size();i++){
                sor=sim.tproS[i].cimke+"_DISS[W/m3]=\t";
                cuns k1=sim.tproS[i].x, k2=sim.tproS[i].y;
                switch(sim.tproS[i].oldal){
                    case WEST: // x section
                        for(uns j=0;j<x;j++)
                            sor=sor+(dissztomb.get(j,k1,k2))+"\t";
                        break;
                    case SOUTH: // y section
                        for(uns j=0;j<y;j++)
                            sor=sor+(dissztomb.get(k1,j,k2))+"\t";
                        break;
                    case BOTTOM: // z section
                        for(uns j=0;j<z;j++)
                            sor=sor+(dissztomb.get(k1,k2,j))+"\t";
                        break;
                }
                if(pont) fprintf(fp,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
            }
            for(uns i=0;i<sim.tproS.size();i++){
                sor=sim.tproS[i].cimke+"_RAD_FI[W]=\t";
                cuns k1=sim.tproS[i].x, k2=sim.tproS[i].y;
                switch(sim.tproS[i].oldal){
                    case WEST: // x section
                        for(uns j=0;j<x;j++)
                            sor=sor+(radianciatomb.get(j,k1,k2))+"\t";
                        break;
                    case SOUTH: // y section
                        for(uns j=0;j<y;j++)
                            sor=sor+(radianciatomb.get(k1,j,k2))+"\t";
                        break;
                    case BOTTOM: // z section
                        for(uns j=0;j<z;j++)
                            sor=sor+(radianciatomb.get(k1,k2,j))+"\t";
                        break;
                }
                if(pont)fprintf(fp,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
            }
            for(uns i=0;i<sim.tproS.size();i++){
                sor=sim.tproS[i].cimke+"_RAD_L[W/m2sr]=\t";
                cuns k1=sim.tproS[i].x, k2=sim.tproS[i].y;
                switch(sim.tproS[i].oldal){
                    case WEST: // x section
                        for(uns j=0;j<x;j++)
                            sor=sor+suruseg(radianciatomb.get(j,k1,k2),(areatomb.get(j,k1,k2) * 4.0 * M_PI))+"\t";
                        break;
                    case SOUTH: // y section
                        for(uns j=0;j<y;j++)
                            sor=sor+suruseg(radianciatomb.get(k1,j,k2),(areatomb.get(k1,j,k2) * 4.0 * M_PI))+"\t";
                        break;
                    case BOTTOM: // z section
                        for(uns j=0;j<z;j++)
                            sor=sor+suruseg(radianciatomb.get(k1,k2,j),(areatomb.get(k1,k2,j) * 4.0 * M_PI))+"\t";
                        break;
                }
                if(pont)fprintf(fp,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
            }
            for(uns i=0;i<sim.tproS.size();i++){
                sor=sim.tproS[i].cimke+"_LUM_FI[lm=cd�sr]=\t";
                cuns k1=sim.tproS[i].x, k2=sim.tproS[i].y;
                switch(sim.tproS[i].oldal){
                    case WEST: // x section
                        for(uns j=0;j<x;j++)
                            sor=sor+(luminanciatomb.get(j,k1,k2))+"\t";
                        break;
                    case SOUTH: // y section
                        for(uns j=0;j<y;j++)
                            sor=sor+(luminanciatomb.get(k1,j,k2))+"\t";
                        break;
                    case BOTTOM: // z section
                        for(uns j=0;j<z;j++)
                            sor=sor+(luminanciatomb.get(k1,k2,j))+"\t";
                        break;
                }
                if(pont)fprintf(fp,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
            }
            for(uns i=0;i<sim.tproS.size();i++){
                sor=sim.tproS[i].cimke+"_LUM_L[cd/m2]=\t";
                cuns k1=sim.tproS[i].x, k2=sim.tproS[i].y;
                switch(sim.tproS[i].oldal){
                    case WEST: // x section
                        for(uns j=0;j<x;j++)
                            sor=sor+suruseg(luminanciatomb.get(j,k1,k2),(areatomb.get(j,k1,k2) * 4.0 * M_PI))+"\t";
                        break;
                    case SOUTH: // y section
                        for(uns j=0;j<y;j++)
                            sor=sor+suruseg(luminanciatomb.get(k1,j,k2),(areatomb.get(k1,j,k2) * 4.0 * M_PI))+"\t";
                        break;
                    case BOTTOM: // z section
                        for(uns j=0;j<z;j++)
                            sor=sor+suruseg(luminanciatomb.get(k1,k2,j),(areatomb.get(k1,k2,j) * 4.0 * M_PI))+"\t";
                        break;
                }
                if(pont)fprintf(fp,"%s\n",sor.c_str());
                sor.replace('.',',');
                if(vesszo)fprintf(fp,"%s\n",sor.c_str());
            }
        }

        // �ram pr�bok

        tomb3d<real_cell_res> & crt = is_el ? t_drI_temp : t_drP_temp;
        for(uns i=0;i<sim.tproC.size();i++){
            cuns xt=sim.tproC[i].x, yt=sim.tproC[i].y, zt=sim.tproC[i].z;
            cuns x2t=sim.tproC[i].x2, y2t=sim.tproC[i].y2, z2t=sim.tproC[i].z2;
            dbl it=0.0, ib=0.0, iw=0.0, ie=0.0, is=0.0, in=0.0;
            for(uns j=xt; j<=x2t; j++)
                for(uns k=yt; k<=y2t; k++){ 
                    ib+=crt.getconstref(j,k,zt).t[BOTTOM]; 
                    it+=crt.getconstref(j,k,z2t).t[TOP]; 
                }
            for(uns j=xt; j<=x2t; j++)
                for(uns k=zt; k<=z2t; k++){ 
                    is+=crt.getconstref(j,yt,k).t[SOUTH]; 
                    in+=crt.getconstref(j,y2t,k).t[NORTH]; 
                }
            for(uns j=yt; j<=y2t; j++)
                for(uns k=zt; k<=z2t; k++){ 
                    iw+=crt.getconstref(xt,j,k).t[WEST]; 
                    ie+=crt.getconstref(x2t,j,k).t[EAST]; 
                }
            dbl sumi=0.0;
            switch(sim.tproC[i].oldal){
                case NORTH: sumi=in; break;
                case SOUTH: sumi=is; break;
                case WEST: sumi=iw; break;
                case EAST: sumi=ie; break;
                case BOTTOM: sumi=ib; break;
                case TOP: sumi=it; break;
                case CENTER: sumi=in+is+iw+ie+ib+it; break;
            }
            sor=sim.tproC[i].cimke+"=\t"+sumi;
            if(pont)fprintf(fp,"%s\n",sor.c_str());
            if(pont)fprintf(fpsok,"%s\n",sor.c_str());
            sor.replace('.',',');
            if(vesszo)fprintf(fp,"%s\n",sor.c_str());
            if(vesszo)fprintf(fpsok,"%s\n",sor.c_str());
        }
        
        fclose(fp);
        fclose(sfp);

        // map probe

        for(uns i=0;i<sim.tproM.size();i++){
            FajlNev=nev+(is_el?"V_map_":"T_map_")+sim.tproM[i].cimke+".dc";
            if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());

            FajlNev=nev+(is_el?"I_map_":"P_map_")+sim.tproM[i].cimke+".dc";
            FILE *fp2=NULL;
            if((fp2=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            
            FajlNev=nev+(is_el?"L_map_":"__map_")+sim.tproM[i].cimke+".dc";
            FILE *fp3=NULL;
            if(is_el && (fp3=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            
            Oldal dal=sim.tproM[i].oldal;
            if(dal==CENTER)dal=EXTERNAL;
            switch(sim.tproM[i].x){
                case 0:{ // x ir�ny
                        cd xm=mod.x_pit[sim.tproM[i].y].get(nulla);
                        for(uns j=0;j<z;j++){
                            cd zm=mod.z_pit[j].get(mod.x_hossz[2*sim.tproM[i].y]);
                            sor="";
                            sor2="";
                            PLString sor3="";
                            for(uns k=0;k<y;k++){
                                sor = sor + (ptemp->getconstref(sim.tproM[i].y,k,j).t[dal]+fold) + "\t";
                                //fprintf(fp,"%.10g\t",ptemp->getconstref(sim.tproM[i].y,k,j).t[dal]+fold);
                                cd * t = is_el ? t_drI_temp.getconstref(sim.tproM[i].y,k,j).t : t_drP_temp.getconstref(sim.tproM[i].y,k,j).t;
                                cd ym=mod.y_pit[k].get(nulla);
                                cd felulet= (dal==WEST || dal==EAST)? ym*zm : (dal==NORTH || dal==SOUTH)? xm*zm : xm*ym; 
                                sor2 = sor2 + (t[dal]/felulet) + "\t";
                                //fprintf(fp2,"%.10g\t",t[dal]/felulet);
                                if(is_el)sor3 = sor3 + suruseg(luminanciatomb.get(sim.tproM[i].y,k,j),(areatomb.get(sim.tproM[i].y,k,j) * 4.0 * M_PI)) + "\t";
                            }
                            if(pont)fprintf(fp,"%s\n",sor.c_str());
                            sor.replace('.',',');
                            if(vesszo)fprintf(fp,"%s\n",sor.c_str());
                            if(pont)fprintf(fp2,"%s\n",sor2.c_str());
                            sor2.replace('.',',');
                            if(vesszo)fprintf(fp2,"%s\n",sor2.c_str());
                            //fprintf(fp,"\n");
                            //fprintf(fp2,"\n");
                            if(is_el && pont)fprintf(fp3,"%s\n",sor3.c_str());
                            if(is_el)sor3.replace('.',',');
                            if(is_el && vesszo)fprintf(fp3,"%s\n",sor3.c_str());
                        }
                    }
                    break;
                case 1:{ // y ir�ny
                        cd ym=mod.y_pit[sim.tproM[i].y].get(nulla);
                        for(uns j=0;j<z;j++){
                            sor="";
                            sor2="";
                            PLString sor3="";
                            for(uns k=0;k<x;k++){
                                cd zm=mod.z_pit[j].get(mod.x_hossz[2*k]);
                                sor = sor + (ptemp->getconstref(k,sim.tproM[i].y,j).t[dal]+fold) + "\t";
                                //fprintf(fp,"%.10g\t",ptemp->getconstref(k,sim.tproM[i].y,j).t[dal]+fold);
                                cd * t = is_el ? t_drI_temp.getconstref(k,sim.tproM[i].y,j).t : t_drP_temp.getconstref(k,sim.tproM[i].y,j).t;
                                cd xm=mod.x_pit[k].get(nulla);
                                cd felulet= (dal==WEST || dal==EAST)? ym*zm : (dal==NORTH || dal==SOUTH)? xm*zm : xm*ym; 
                                sor2 = sor2 + (t[dal]/felulet) + "\t";
                                //fprintf(fp2,"%.10g\t",t[dal]/felulet);
                                if(is_el)sor3 = sor3 + suruseg(luminanciatomb.get(k,sim.tproM[i].y,j),(areatomb.get(k,sim.tproM[i].y,j) * 4.0 * M_PI)) + "\t";
                            }
                            if(pont)fprintf(fp,"%s\n",sor.c_str());
                            sor.replace('.',',');
                            if(vesszo)fprintf(fp,"%s\n",sor.c_str());
                            if(pont)fprintf(fp2,"%s\n",sor2.c_str());
                            sor2.replace('.',',');
                            if(vesszo)fprintf(fp2,"%s\n",sor2.c_str());
                            //fprintf(fp,"\n");
                            //fprintf(fp2,"\n");
                            if(is_el && pont)fprintf(fp3,"%s\n",sor3.c_str());
                            if(is_el)sor3.replace('.',',');
                            if(is_el && vesszo)fprintf(fp3,"%s\n",sor3.c_str());
                        }
                    }
                    break;
                case 2:{ // z ir�ny
                        for(uns j=0;j<y;j++){
                            cd ym=mod.y_pit[j].get(nulla);
                            sor="";
                            sor2="";
                            PLString sor3="";
                            for(uns k=0;k<x;k++){
                                cd zm=mod.z_pit[sim.tproM[i].y].get(mod.x_hossz[2*k]);
                                sor = sor + (ptemp->getconstref(k,j,sim.tproM[i].y).t[dal]+fold) + "\t";
                                //fprintf(fp,"%.10g\t",ptemp->getconstref(k,j,sim.tproM[i].y).t[dal]+fold);
                                cd * t = is_el ? t_drI_temp.getconstref(k,j,sim.tproM[i].y).t : t_drP_temp.getconstref(k,j,sim.tproM[i].y).t;
                                cd xm=mod.x_pit[k].get(nulla);
                                cd felulet= (dal==WEST || dal==EAST)? ym*zm : (dal==NORTH || dal==SOUTH)? xm*zm : xm*ym; 
                                sor2 = sor2 + (t[dal]/felulet) + "\t";
                                //fprintf(fp2,"%.10g\t",t[dal]/felulet);
                                if(is_el)sor3 = sor3 + suruseg(luminanciatomb.get(k,j,sim.tproM[i].y),(areatomb.get(k,j,sim.tproM[i].y) * 4.0 * M_PI)) + "\t";
                            }
                            if(pont)fprintf(fp,"%s\n",sor.c_str());
                            sor.replace('.',',');
                            if(vesszo)fprintf(fp,"%s\n",sor.c_str());
                            if(pont)fprintf(fp2,"%s\n",sor2.c_str());
                            sor2.replace('.',',');
                            if(vesszo)fprintf(fp2,"%s\n",sor2.c_str());
                            //fprintf(fp,"\n");
                            //fprintf(fp2,"\n");
                            if(is_el && pont)fprintf(fp3,"%s\n",sor3.c_str());
                            if(is_el)sor3.replace('.',',');
                            if(is_el && vesszo)fprintf(fp3,"%s\n",sor3.c_str());
                        }
                    }
                    break;
                default: throw hiba("masina::writefim()","impossible map probe direction (%u)",sim.tproM[i].x);
            }
            fclose(fp);
            fclose(fp2);
            if(is_el)fclose(fp2);
        }

        N=0;
        if(par.is_fim_text){
            FajlNev=nev+"SRE"+t+".FIM.txt";
//            FajlNev=path+"SRE"+t+(is_el?".FIMe":".FIM")+".txt";
            if((fp=fopen(FajlNev.c_str(),"wt"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            fprintf(fp,"**************************************\nSUNRED map: center voltage/temperature\n  x   y   z  result\n**************************************\n");
            for(uns i=0;i<z;i++){
//                uns layelt=Ldb*i;
                for(u32 j=0;j<y;j++){
//                    uns sorelt=wxy*j;
                    for(uns k=0;k<x;k++,N++){
                        fprintf(fp,"%3u %3u %3u %16.8e\n",k,j,i,ptemp->getconstref(N).t[EXTERNAL]+fold);
                    }
                }
            }
            fclose(fp);
        }

if(!sim.is_nofim){
    if(is_el){
        if(analtype<2){
            FajlNev=nev+"SRE"+t+".DISSIM";
            if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            fwrite(&fej,sizeof(fej),1,fp);
            for(uns k=0; k<wz; k++)
                for(uns j=0; j<wxy; j++)
                    for(uns i=0; i<wxy; i++){
                        const double dtemp = (i < dx || j < dy || k < dz) ? 0.0 : dissztomb.get(i-dx, j-dy, k-dz);
                        fwrite(&dtemp,sizeof(double),1,fp);
                    }
            //dissztomb.free();
            fclose(fp);

            FajlNev = nev + "SRE" + t + ".DISSIM_TENYLEGES";
            if ((fp = fopen(FajlNev.c_str(), "wb")) == 0)throw hiba("masina::writefim()", "cannot open %s", FajlNev.c_str());
            fwrite(&fej, sizeof(fej), 1, fp);
            for (uns k = 0; k<wz; k++)
                for (uns j = 0; j<wxy; j++)
                    for (uns i = 0; i<wxy; i++){
                        const double dtemp = (i < dx || j < dy || k < dz) ? 0.0 : dissztomb_tenyleges.get(i-dx, j-dy, k-dz);
                        fwrite(&dtemp, sizeof(double), 1, fp);
                    }
            fclose(fp);

            FajlNev = nev + "SRE" + t + ".DISSIM_A";
            if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            fwrite(&fej,sizeof(fej),1,fp);
            for(uns k=0; k<wz; k++)
                for(uns j=0; j<wxy; j++)
                    for(uns i=0; i<wxy; i++){
                        // const double ddt = ( k==2 && wz > 2 ) ? dissztomb_A.get(i,j,1) + dissztomb_A.get(i,j,3) : 0.0 ;
                        double dtemp = (i < dx || j < dy || k < dz) ? 0.0 : dissztomb_A.get(i-dx, j-dy, k-dz); // +ddt;
                        dtemp = fabs(dtemp) > 1e-10 ? dtemp : 0.0; 
                        fwrite(&dtemp,sizeof(double),1,fp);
                    }
            //dissztomb.free();
            fclose(fp);

            if( mod.A_semi_full != nulla ){
                FajlNev=nev+"SRE"+t+".RAD_FI";
                if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
                fwrite(&fej,sizeof(fej),1,fp);
                for(uns k=0; k<wz; k++)
                    for(uns j=0; j<wxy; j++)
                        for(uns i=0; i<wxy; i++){
                            const double dtemp= (i < dx || j < dy || k < dz) ? 0.0 : radianciatomb.get(i-dx,j-dy,k-dz);
                            fwrite(&dtemp,sizeof(double),1,fp);
                        }
                //radianciatomb.free(); // a probe-n�l ki�rjuk a sum-ot, ez�rt a free csak k�s�bb (pl. most) lehet
                fclose(fp);
            }
        }

        if( mod.A_semi_full != nulla ){
            FajlNev=nev+"SRE"+t+".RAD_L";
            if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            fwrite(&fej,sizeof(fej),1,fp);
            for(uns k=0; k<wz; k++)
                for(uns j=0; j<wxy; j++)
                    for(uns i=0; i<wxy; i++){
                        const double A = (i < dx || j < dy || k < dz) ? 0.0 : areatomb.get(i-dx,j-dy,k-dz) * 4.0 * M_PI;
                        const double dtemp = suruseg((i < dx || j < dy || k < dz) ? 0.0 : radianciatomb.get(i-dx,j-dy,k-dz),A);
                        fwrite(&dtemp,sizeof(double),1,fp);
                    }
            //radianciatomb.free(); // a probe-n�l ki�rjuk a sum-ot, ez�rt a free csak k�s�bb (pl. most) lehet
            fclose(fp);
        }

        if(analtype<2 &&  mod.A_semi_full != nulla){
            FajlNev=nev+"SRE"+t+".LUM_FI";
            if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            fwrite(&fej,sizeof(fej),1,fp);
            for(uns k=0; k<wz; k++)
                for(uns j=0; j<wxy; j++)
                    for(uns i=0; i<wxy; i++){
                        const double dtemp= (i < dx || j < dy || k < dz) ? 0.0 : luminanciatomb.get(i-dx,j-dy,k-dz);
                        fwrite(&dtemp,sizeof(double),1,fp);
                    }
            //luminanciatomb.free(); // a probe-n�l ki�rjuk a sum-ot, ez�rt a free csak k�s�bb (pl. most) lehet
            fclose(fp);
        }

        if( mod.A_semi_full != nulla ){
            FajlNev=nev+"SRE"+t+".LUM_L";
            if((fp=fopen(FajlNev.c_str(),"wb"))==0)throw hiba("masina::writefim()","cannot open %s",FajlNev.c_str());
            fwrite(&fej,sizeof(fej),1,fp);
            for(uns k=0; k<wz; k++)
                for(uns j=0; j<wxy; j++)
                    for(uns i=0; i<wxy; i++){
                        const double A = (i < dx || j < dy || k < dz) ? 0.0 : areatomb.get(i-dx,j-dy,k-dz) * 4.0 * M_PI;
                        const double dtemp = suruseg((i < dx || j < dy || k < dz) ? 0.0 : luminanciatomb.get(i-dx,j-dy,k-dz),A);
                        fwrite(&dtemp,sizeof(double),1,fp);
                    }
            //luminanciatomb.free(); // a probe-n�l ki�rjuk a sum-ot, ez�rt a free csak k�s�bb (pl. most) lehet
            fclose(fp);
        }

    }
}
    }
    PLString FajlNev=path+"!iteration_limit_exceeded!";
    if(iternormal)remove(FajlNev.c_str());
    else fclose(fopen(FajlNev.c_str(),"wb"));
    //fclose(fpsok);
}
//

//***********************************************************************
bool masina::check_error(uns drmap_azon_1,uns drmap_azon_2,dbl maxerror){
//***********************************************************************
//    if(t_drmap.size()<=drmap_azon_1||t_drmap.size()<=drmap_azon_2)return false;
    const tomb3d<real_cell_res> * t1=t_drmap[drmap_azon_1];
    const tomb3d<real_cell_res> * t2=t_drmap[drmap_azon_2];
    cuns max=t1->size();
    if(max!=t2->size())throw hiba("masina::check_error","t1->size()!=t2->size() (%u!=%u)",max,t2->size());
    cd xhiba=maxerror; // sz�kely szerint *0.5
    dbl oszto=1e-20;
    for(uns i=0;i<max;i++){
        cd u2=fabs(t2->getconstref(i).t[EXTERNAL]);
        if(u2>oszto)oszto=u2;
    }
    dbl aktmaxhiba = nulla;
    static dbl last3maxhiba[6] = { 1,1,1,1,1,1 };
    for(uns i=0;i<max;i++){
        cd u1=t1->getconstref(i).t[EXTERNAL];
        cd u2=t2->getconstref(i).t[EXTERNAL];
        cd akthiba = fabs((u1 - u2) / oszto);
        if (akthiba > aktmaxhiba)
            aktmaxhiba = akthiba;
        if (akthiba > xhiba) {
            last3maxhiba[0] = last3maxhiba[1];
            last3maxhiba[1] = last3maxhiba[2];
            last3maxhiba[2] = last3maxhiba[3];
            last3maxhiba[3] = last3maxhiba[4];
            last3maxhiba[4] = last3maxhiba[5];
            last3maxhiba[5] = aktmaxhiba;
            return false;
        }
    }
    last3maxhiba[0] = last3maxhiba[1];
    last3maxhiba[1] = last3maxhiba[2];
    last3maxhiba[2] = last3maxhiba[3];
    last3maxhiba[3] = last3maxhiba[4];
    last3maxhiba[4] = last3maxhiba[5];
    last3maxhiba[5] = aktmaxhiba;
    return ( last3maxhiba[0] + last3maxhiba[1] + last3maxhiba[2] + last3maxhiba[3] + last3maxhiba[4] + last3maxhiba[5] ) < xhiba * 3;
}


//***********************************************************************
dbl masina::get_max_error(uns drmap_azon_1,uns drmap_azon_2){
//***********************************************************************
//    if(t_drmap.size()<=drmap_azon_1||t_drmap.size()<=drmap_azon_2)return 1.0;
    const tomb3d<real_cell_res> * t1=t_drmap[drmap_azon_1];
    const tomb3d<real_cell_res> * t2=t_drmap[drmap_azon_2];
    cuns max=t1->size();
    if(max!=t2->size())throw hiba("masina::check_error","t1->size()!=t2->size() (%u!=%u)",max,t2->size());
    dbl maxerror=nulla,u1max=nulla,u2max=nulla;
    uns imax=0;
    dbl oszto=1e-20;
    for(uns i=0;i<max;i++){
        cd u2=fabs(t2->getconstref(i).t[EXTERNAL]);
        if(u2>oszto)oszto=u2;
    }
    if(oszto==1e-20)return 1.0;
    for(uns i=0;i<max;i++){
        cd u1=t1->getconstref(i).t[EXTERNAL];
        cd u2=t2->getconstref(i).t[EXTERNAL];
        cd xhiba=fabs((u1-u2)/oszto);
        if(xhiba>maxerror){
            maxerror=xhiba;
            u1max=u1;
            u2max=u2;
            imax=i;
        }
    }
//    if(maxerror>0.01)printf("\nN=%u, u1=%g, u2=%g, hiba=%g\n",imax,u1max,u2max,maxerror);
    return maxerror;
}


