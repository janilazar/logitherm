//***********************************************************************
// Vector SUNRED 2 ccella class header
// Creation date:	2004. 07. 03.
// Creator:			Pohl L�szl�
// Owner:			Budapest University of Technology and Economics
//					Department of Electron Devices
// Modified date:	2008. 05. 03.
//***********************************************************************


//***********************************************************************
#ifndef VSUN2_QCCELLA_HEADER
#define	VSUN2_QCCELLA_HEADER
//***********************************************************************


//***********************************************************************
#define qrcella qccella
#define quad_float qcomplex
#define qsrmatrix qscmatrix
#define qrmatrix qcmatrix
#define qrvector qcvector
#define qrcellahalo qccellahalo
#define qr_y_extern qc_y_extern
#define qr_j_extern qc_j_extern
#define qr_u_extcom qc_u_extcom
#define qr_i_extcom qc_i_extcom
#define qr_y_compmod qc_y_compmod
#define qr_j_compmod qc_j_compmod
//***********************************************************************
#undef VSUN2_QRCELLA_HEADER
//***********************************************************************
#include "qrcella.h"
//***********************************************************************
#undef qr_y_extern
#undef qr_j_extern
#undef qr_i_extcom
#undef qr_u_extcom
#undef qr_y_compmod
#undef qr_j_compmod
#undef qrcella
#undef quad_float
#undef qrmatrix
#undef qsrmatrix
#undef qrvector
#undef rcellahalo
//***********************************************************************


#endif
