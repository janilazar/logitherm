//***********************************************************************
// Vector SUNRED 2 ccella class header
// Creation date:	2009. 08. 16.
// Creator:			Pohl L�szl�
//***********************************************************************


//***********************************************************************
#ifndef VSUN2_DCCELLA_HEADER
#define	VSUN2_DCCELLA_HEADER
//***********************************************************************


//***********************************************************************
#include "tipusok.h"
#include "dmatrix.h"
//***********************************************************************


//***********************************************************************
class dccella;
//***********************************************************************

#ifndef DCCELLA_DIND
#define DCCELLA_DIND
#endif

//***********************************************************************
class dccella{
    friend class masina;
	struct celladat{
		dccella *C1,*C2,*C; // Elt�roljuk a bemen� cell�kat is, ha csak egy van (a m�sik 0), akkor C is kap �rt�ket
        bool external_nulladik[BASIC_SIDES]; // bool[BASIC_SIDES], amewlyik true, az external, csak a 0. szinten haszn�ljuk
		bool extended;	// Ha vegyes parci�lis deriv�ltakat is tartalmaz a modell
        tomb<uns> texternal_azon; // tomb<uns>, mindegyik external csom�ponthoz viszi mag�val az azonos�t�j�t, ami nem lehet 0
        tomb<dbl> texternal_area; // tomb<dbl>, az external fel�lete, viszi mag�val
		u8 oc1,oc2;	// A bemen� cell�k �sszevont oldalai, konstruktor csin�lja
		u16 nulladik_kulso_oldalszam;	// a feladat::allocStep0cellsDC �ll�tja be a 0. szint� cell�k l�trehoz�sakor, az oldalak t�mb szumm�ja-center, magasabb szinten nem haszn�ljuk
		u16 oldalak[EXTENDED_SIDES]; // EXTERNAL, WEST, EAST, SOUTH, NORTH, BOTTOM, TOP, haszn�ld a kontansokat!, ide kapocs*vecdim darabot kell �rni!!
        u16 oldalak2[BASIC_SIDES]; // u16, csak a 0. szinten hasznb�ljuk, external csom�pontok megl�te eset�n
		dcvector j;		// az inhomog�n �ram vektor,   a forwsubshoz kell, forwsubs csin�lja
		dcvector jb;	// a reducej �ltal termelt jb, a backsubshoz kell, forwsubs csin�lja
		dcvector u;		// a kapcsok fesz�lts�ge, a fels�bb sznt� backsubs gener�lja
		dcvector ub;	// a bels� kapcsok fesz�lts�ge, a saj�t backsubs gener�lja
		dscmatrix y;	// az admittancia m�trix,              a noderedhez  kell, nodered csin�lja
		dscmatrix nzb;	// -yb^-1, amelyb�l a cella l�trej�tt, a backsubshoz kell, nodered csin�lja
		dcmatrix nxazb;	// -xa*zb, amelyb�l a cella l�trej�tt, a forwsubshoz kell, nodered csin�lja
        celladat(){for(uns i=0; i<BASIC_SIDES; i++)external_nulladik[i]=false;}
		//***********************************************************************
		void free(){
		//***********************************************************************
			j.free();
			jb.free();
			u.free();
			ub.free();
			y.free();
			nzb.free();
			nxazb.free();
		}
		//***********************************************************************
		void save(FILE * fp){
		//***********************************************************************
			u32 teszt='L';
			if(fwrite(&teszt,	sizeof(u32),	1,fp)!=1)throw hiba("dccella::save","teszt write failed");
			if(fwrite(&C1,		sizeof(dccella*),1,fp)!=1)throw hiba("dccella::save","C1 write failed");
			if(fwrite(&C2,		sizeof(dccella*),1,fp)!=1)throw hiba("dccella::save","C1 write failed");
			if(fwrite(&C,		sizeof(dccella*),1,fp)!=1)throw hiba("dccella::save","C write failed");
			if(fwrite(&oc1,		sizeof(u8),		1,fp)!=1)throw hiba("dccella::save","oc1 write failed");
			if(fwrite(&oc2,		sizeof(u8),		1,fp)!=1)throw hiba("dccella::save","oc2 write failed");
			if(fwrite(oldalak,	sizeof(u16),	EXTENDED_SIDES,fp)!=EXTENDED_SIDES)throw hiba("dccella::save","oldalak write failed");
            if(fwrite(oldalak2,	sizeof(u16),	BASIC_SIDES,fp)!=BASIC_SIDES)throw hiba("dccella::save","oldalak2 write failed");
			if(fwrite(&nulladik_kulso_oldalszam,sizeof(u16),1,fp)!=1)throw hiba("dccella::save","nulladik_kulso_oldalszam write failed");
			y.save(fp);
			nzb.save(fp);
			nxazb.save(fp);
            texternal_azon.save(fp);
            texternal_area.save(fp);
			if(fwrite(&extended,sizeof(bool),	1,fp)!=1)throw hiba("dccella::save","extended write failed");
			if(fwrite(&external_nulladik,sizeof(bool), BASIC_SIDES,fp)!=1)throw hiba("dccella::save","external_nulladik write failed");
		}
		//***********************************************************************
		void save_v(FILE * fp){
		//***********************************************************************
			u32 teszt='L';
			if(fwrite(&teszt,	sizeof(u32),	1,fp)!=1)throw hiba("dccella::save","teszt write failed");
			j.save(fp);
			jb.save(fp);
			u.save(fp);
			ub.save(fp);
		}
		//***********************************************************************
		void load(FILE * fp,FILE * fpv){
		//***********************************************************************
			u32 teszt;
			if(fread(&teszt,	sizeof(u32),	1,fp)!=1)throw hiba("dccella::load","teszt read failed");
			if(teszt!='L')throw hiba("dccella::load","not a dccella to read");
			if(fread(&C1,		sizeof(dccella*),1,fp)!=1)throw hiba("dccella::load","C1 read failed");
			if(fread(&C2,		sizeof(dccella*),1,fp)!=1)throw hiba("dccella::load","C1 read failed");
			if(fread(&C,		sizeof(dccella*),1,fp)!=1)throw hiba("dccella::load","C read failed");
			if(fread(&oc1,		sizeof(u8),		1,fp)!=1)throw hiba("dccella::load","oc1 read failed");
			if(fread(&oc2,		sizeof(u8),		1,fp)!=1)throw hiba("dccella::load","oc2 read failed");
			if(fread(oldalak,	sizeof(u16),	EXTENDED_SIDES,fp)!=EXTENDED_SIDES)throw hiba("dccella::load","oldalak read failed");
			if(fread(oldalak2,	sizeof(u16),	BASIC_SIDES,fp)!=BASIC_SIDES)throw hiba("dccella::load","oldalak2 read failed");
			if(fread(&nulladik_kulso_oldalszam,sizeof(u16),1,fp)!=1)throw hiba("dccella::load","nulladik_kulso_oldalszam read failed");
			y.load(fp);
			nzb.load(fp);
			nxazb.load(fp);
            texternal_azon.load(fp);
            texternal_area.load(fp);
			if(fread(&extended,sizeof(bool),	1,fp)!=1)throw hiba("dccella::load","extended read failed");
			if(fread(&external_nulladik,sizeof(bool), BASIC_SIDES,fp)!=1)throw hiba("dccella::load","external_nulladik read failed");
			if(fread(&teszt,	sizeof(u32),	1,fpv)!=1)throw hiba("dccella::load","teszt read failed from fpv");
			if(teszt!='L')throw hiba("dccella::load","not a dccella to read from fpv");
			j.load(fpv);
			jb.load(fpv);
			u.load(fpv);
			ub.load(fpv);
		}
	};
#ifdef DCCELLA_DIND
	//***********************************************************************
	class dind{ // NEM FIBERSAFE!!!
	//***********************************************************************
		celladat * t[V_DIN_MAX_BLOKK]; // itt vannak a t�nyleges adatok
		u32 e[V_DIN_MAX_BLOKK]; // a blokkon h�ny eleme van bet�ltve
		u32 g[V_DIN_MAX_BLOKK]; // a blokkban most h�ny m�trix van (free-n�l cs�kken)
		u32 n,akt; // aktu�lis blokk
		void init(cu32 i){t[i]=new celladat[DINDDB];e[i]=0;g[i]=0;}
		void __free(cu32 i){delete[]t[i];t[i]=NULL;e[i]=0;}
		//***********************************************************************
		void setakt(){
		//***********************************************************************
			for(;akt<n&&e[akt]>=DINDDB;akt++);
			if(akt>=n)for(akt=0;akt<n&&e[akt]>=DINDDB;akt++);
			if(akt>=n){
				n=akt+1;
				init(akt);
			}
			else if(t[akt]==NULL)init(akt);
		}
	public:
		//***********************************************************************
		dind():n(1),akt(0){init(0);}
		~dind(){for(u32 i=0;i<n;i++)__free(i);}
		//***********************************************************************
		celladat * alloc(u32 & azonosito){
		//***********************************************************************
	#ifdef vsundebugmode
			if(n>=V_DIN_MAX_BLOKK-1)throw hiba("","dind::alloc => n>=V_DIN_MAX_BLOKK-1 => %lu>%lu",n,V_DIN_MAX_BLOKK-1);
	#endif
			if(e[akt]>=DINDDB)setakt();// �res helyet keres�nk
			azonosito=akt;
			g[akt]++;
			return t[akt]+e[akt]++;
		}
		//***********************************************************************
		void free(u32 & i){ 
		// az azonos�t� �ltal jelzett blokkban szabad�tunk fel
		// nem ellen�rzi, hogy t�bbsz�r szabad�tunk-e fel
		//***********************************************************************
			if(i==V_DIN_MAX_BLOKK)return;// NULL pointer volt
			if(!--g[i]){
				e[i]=0;
				if(i>0&&akt!=i){__free(i);akt=0;}
			}
			i=V_DIN_MAX_BLOKK;
		}
		//***********************************************************************
		void print(){
		//***********************************************************************
			printf("\nn=%lu\n",n);
			for(u32 i=0;i<n;i++)printf("blokk[%02lu]=%lu\n",i,g[i]);
		}
	};
#endif // DCCELLA_DIND
	celladat * d;
	u32 azon;
#ifdef DCCELLA_DIND
	static dind * dblokk;
#endif // DCCELLA_DIND
public:
	// kell majd egy konstruktor az alapcell�knak
	dccella(dccella * c1,dccella * c2,const Oldal side1);
#ifdef DCCELLA_DIND
	~dccella(){if(parhuzamosanfut) { printf("dccella::~dccella => parhuzamosanfut"); exit(1); }if(d)d->free();dblokk->free(azon);/*delete d;*/}
#else
	~dccella(){if(parhuzamosanfut)throw hiba("","dccella::~dccella => parhuzamosanfut");delete d;}
#endif // DCCELLA_DIND
	void save(FILE * fp){if(d)d->save(fp);else throw hiba("dccella::save","d==NULL");}
	void save_v(FILE * fp){if(d)d->save_v(fp);else throw hiba("","dccella::save_v -> d==NULL");}
#ifdef DCCELLA_DIND
	void store(FILE * fp,FILE * fpv){if(parhuzamosanfut)throw hiba("","dccella::store => parhuzamosanfut");save(fp);save_v(fpv);if(d)d->free();dblokk->free(azon);/*delete d;*/ d=NULL;}
#else
	void store(FILE * fp,FILE * fpv){if(parhuzamosanfut)throw hiba("","dccella::store => parhuzamosanfut");save(fp);save_v(fpv);delete d; d=NULL;}
#endif // DCCELLA_DIND
#ifdef DCCELLA_DIND
	void load(FILE * fp,FILE * fpv){
		if(parhuzamosanfut)throw hiba("","dccella::load => parhuzamosanfut");
		if(d)d->free();dblokk->free(azon);/*delete d;*/
		d=dblokk->alloc(azon);//new celladat;
		d->load(fp,fpv);
	}
#else
	void load(FILE * fp,FILE * fpv){
		if(parhuzamosanfut)throw hiba("","dccella::load => parhuzamosanfut");
		delete d;
		d=new celladat;
		d->load(fp,fpv);
	}
#endif // DCCELLA_DIND
	dccella(bool extended);	// ha a 0. szint� cell�t hozzuk l�tre

	void allocY();// Elore lefoglaljuk az adott l�p�shez tartoz� Y m�trixot, hogy amikor f�lszabad�tjuk oket, a mem�ria nem lesz fragment�lt
	void allocM();// Elore lefoglaljuk az adott l�p�shez tartoz� t�bbi m�trixot, hogy amikor f�lszabad�tjuk oket, a mem�ria nem lesz fragment�lt
	void allocV();// Lefoglalja az adott l�p�shez tartoz� vektorokat
	void allocLast();// Utols� l�p�sben, ha nincsenek external node-ok
	void freeY(){d->y.free();} // Egyszerre t�r�lj�k
#ifdef DCCELLA_DIND
//	void freeM(){if(parhuzamosanfut)throw hiba("","dccella::freeM => parhuzamosanfut");if(d)d->free();dblokk->free(azon);/*delete d;*/ d=NULL;/*freeY();d->nzb.free();d->nxazb.free();*/} // Egyszerre t�r�lj�k
	void free(){if(parhuzamosanfut)throw hiba("","dccella::free => parhuzamosanfut");if(d)d->free();dblokk->free(azon);/*delete d;*/ d=NULL;/*freeM();d->j.free();d->jb.free();*/} // Egyszerre t�r�lj�k
#else
//	void freeM(){if(parhuzamosanfut)throw hiba("","dccella::freeM => parhuzamosanfut");delete d; d=NULL;/*freeY();d->nzb.free();d->nxazb.free();*/} // Egyszerre t�r�lj�k
	void free(){if(parhuzamosanfut)throw hiba("","dccella::free => parhuzamosanfut");delete d; d=NULL;/*freeM();d->j.free();d->jb.free();*/} // Egyszerre t�r�lj�k
#endif // DCCELLA_DIND
	void saveM(FILE *fp);// Csak a m�trixokat
	void loadM(FILE *fp);// Csak a m�trixokat

	void calcI(dcvector & I,const dcvector & U)const;//I=y*U+j
	void calcU(dcvector & U,const dcvector & I)const;//U=inv(y)*(I-j)
	
	void segedtombok(const dscmatrix ** honnan,u32 * ind1,u32 * ind2,u32 * index,u32 * kapocsszam,u32 * indy);
    void reorder_externals_before_nodered_0();
    void reorder_externals_before_forwsubs_0();
    void reorder_externals_after_backsubs_0();
    void extern_y_reduce();
    void extern_j_reduce();
    void extern_solve();
    void extern_backsubs(bool aramot);
	void nodered(bool isforws=true); // forwsubsot is megh�vja
	void reduceLast(){d->nzb.add(d->C1->d->y,d->C2->d->y);d->nzb.ninv();forwLast();}// y, nxazb, nzbxb marad 0
	void forwsubs();
	void forwLast(){
		d->jb.add(d->C1->d->j,d->C2->d->j);}
	void backsubs();//ub=nzbxb*u+nzb*jb, C1->u, C2->u
	void backFirst(){
		d->ub.tmul(d->nzb,d->jb);
		d->C1->d->u.copy(d->ub);
		d->C2->d->u.copy(d->ub);}// j �s u 0 m�ret�

	u32 externals(){return d->oldalak[EXTERNAL];}
	u32 getoldalnodeszam(cu32 i){return d->oldalak[i];}
	bool getextended(){return d->extended;}
	dccella * getcella(){return this;}
    u16 * get_oldalak(){return d->oldalak;}
    void set_nulladik_kulso_oldalszam(u16 db){d->nulladik_kulso_oldalszam=db;}
    celladat * get_d(){return d;}

/*	friend void copy(dccella & dest,const dccella & src);

	//***********************************************************************
*/
};

//***********************************************************************
inline dccella::dccella(dccella * c1,dccella * c2,const Oldal side1){
//***********************************************************************
#ifdef vsundebugmode
		if(side1!=EAST&&side1!=NORTH&&side1!=TOP)
			throw hiba("","Program error -> dccella::dccella -> side1!=EAST&&side1!=NORTH&&side1!=TOP");
		if(c1==0&&c2==0)throw hiba("","Program error -> dccella::dccella -> c1==0&&c2==0");
#endif
	if(parhuzamosanfut)throw hiba("","dccella::dccella => parhuzamosanfut");
#ifdef DCCELLA_DIND
	if(!dblokk)dblokk=new dind;
	d=dblokk->alloc(azon);//new celladat;
#else
	d=new celladat;
#endif // DCCELLA_DIND

	// oc1,oc2

	d->oc1=side1;
	d->oc2=(side1==EAST)?WEST:((side1==NORTH)?SOUTH:BOTTOM);
    d->nulladik_kulso_oldalszam=0;
	
	//C,C1,C2
	
	d->C=(c1==NULL)?c2:((c2==0)?c1:NULL);

	if(d->C!=NULL){//csak 1 bemeno cella van
		d->C1=NULL;d->C2=NULL;//Ha valahol hib�san hivatkozok ezekre, akkor elsz�lljon a program (jobb a biztos hiba, mint az intermittens...)
		//oldalak
		d->extended=d->C->d->extended;
		if(d->extended)for(u32 i3=0;i3<EXTENDED_SIDES;i3++)d->oldalak[i3]=d->C->d->oldalak[i3];
		else for(u32 i3=0;i3<BASIC_SIDES;i3++)d->oldalak[i3]=d->C->d->oldalak[i3];
		d->oldalak[CENTER]=0;
	}
	else
	{
#ifdef vsundebugmode
		cu8 side2=d->oc2;
		if(c1->d->oldalak[side1]!=c2->d->oldalak[side2])
			throw hiba("","Program error -> dccella::dccella -> c1->d->oldalak[side1]!=c2->d->oldalak[side2] (%u!=%u)"
					,u32(c1->d->oldalak[side1]),u32(c2->d->oldalak[side2]));
		if(c1->d->extended!=c2->d->extended)throw hiba("","program error -> dccella::dccella -> c1->d->extended!=c2->d->extended");
#endif
		d->C1=c1;d->C2=c2;
		d->extended=d->C1->d->extended;
		//oldalak
		u16 *ol0=d->oldalak,*ol1=d->C1->d->oldalak,*ol2=d->C2->d->oldalak;
		d->oldalak[EXTERNAL]=d->C1->d->oldalak[EXTERNAL]+d->C2->d->oldalak[EXTERNAL];
		if(d->extended)switch(d->oc1){
			case EAST:{
					ol0[WEST]=ol1[WEST]; ol0[EAST]=ol2[EAST];
					ol0[SOUTH]=ol1[SOUTH]+ol2[WS]+ol1[ES]+ol2[SOUTH];
					ol0[NORTH]=ol1[NORTH]+ol1[EN]+ol2[WN]+ol2[NORTH]; 
					ol0[BOTTOM]=ol1[BOTTOM]+ol2[WB]+ol1[EB]+ol2[BOTTOM];
					ol0[TOP]=ol1[TOP]+ol1[ET]+ol2[WT]+ol2[TOP];
					ol0[WS]=ol1[WS]; ol0[WN]=ol1[WN]; ol0[WB]=ol1[WB]; ol0[WT]=ol1[WT];
					ol0[ES]=ol2[ES]; ol0[EN]=ol2[EN]; ol0[EB]=ol2[EB]; ol0[ET]=ol2[ET];
					ol0[SB]=ol1[SB]+ol2[SB]; ol0[ST]=ol1[ST]+ol2[ST];
					ol0[NB]=ol1[NB]+ol2[NB]; ol0[NT]=ol1[NT]+ol2[NT];
				}
				break;
			case NORTH:{
					ol0[WEST]=ol1[WEST]+ol2[WS]+ol1[WN]+ol2[WEST];
					ol0[EAST]=ol1[EAST]+ol1[EN]+ol2[ES]+ol2[EAST];
					ol0[SOUTH]=ol1[SOUTH]; ol0[NORTH]=ol2[NORTH];
					ol0[BOTTOM]=ol1[BOTTOM]+ol2[SB]+ol1[NB]+ol2[BOTTOM];
					ol0[TOP]=ol1[TOP]+ol1[NT]+ol2[ST]+ol2[TOP];
					ol0[WS]=ol1[WS]; ol0[WN]=ol2[WN]; ol0[WB]=ol1[WB]+ol2[WB];
					ol0[WT]=ol1[WT]+ol2[WT]; ol0[ES]=ol1[ES]; ol0[EN]=ol2[EN];
					ol0[EB]=ol1[EB]+ol2[EB]; ol0[ET]=ol1[ET]+ol2[ET];
					ol0[SB]=ol1[SB]; ol0[ST]=ol1[ST]; ol0[NB]=ol2[NB];
					ol0[NT]=ol2[NT];
				}
				break;
			case TOP:{
					ol0[WEST]=ol1[WEST]+ol2[WB]+ol1[WT]+ol2[WEST];
					ol0[EAST]=ol1[EAST]+ol1[ET]+ol2[EB]+ol2[EAST];
					ol0[SOUTH]=ol1[SOUTH]+ol2[SB]+ol1[ST]+ol2[SOUTH];
					ol0[NORTH]=ol1[NORTH]+ol1[NT]+ol2[NB]+ol2[NORTH];
					ol0[BOTTOM]=ol1[BOTTOM]; ol0[TOP]=ol2[TOP]; ol0[WS]=ol1[WS]+ol2[WS];
					ol0[WN]=ol1[WN]+ol2[WN]; ol0[WB]=ol1[WB]; ol0[WT]=ol2[WT];
					ol0[ES]=ol1[ES]+ol2[ES]; ol0[EN]=ol1[EN]+ol2[EN]; ol0[EB]=ol1[EB];
					ol0[ET]=ol2[ET]; ol0[SB]=ol1[SB]; ol0[ST]=ol2[ST]; ol0[NB]=ol1[NB];
					ol0[NT]=ol2[NT];
				}
				break;
			default:throw hiba("","program error -> dccella::nodered -> illegal side");
		}
		else switch(d->oc1){
			case EAST:{
					ol0[WEST]=ol1[WEST]; 
					ol0[EAST]=ol2[EAST];
					ol0[SOUTH]=ol1[SOUTH]+ol2[SOUTH];
					ol0[NORTH]=ol1[NORTH]+ol2[NORTH]; 
					ol0[BOTTOM]=ol1[BOTTOM]+ol2[BOTTOM];
					ol0[TOP]=ol1[TOP]+ol2[TOP];
				}
				break;
			case NORTH:{
					ol0[WEST]=ol1[WEST]+ol2[WEST];
					ol0[EAST]=ol1[EAST]+ol2[EAST];
					ol0[SOUTH]=ol1[SOUTH]; 
					ol0[NORTH]=ol2[NORTH];
					ol0[BOTTOM]=ol1[BOTTOM]+ol2[BOTTOM];
					ol0[TOP]=ol1[TOP]+ol2[TOP];
				}
				break;
			case TOP:{
					ol0[WEST]=ol1[WEST]+ol2[WEST];
					ol0[EAST]=ol1[EAST]+ol2[EAST];
					ol0[SOUTH]=ol1[SOUTH]+ol2[SOUTH];
					ol0[NORTH]=ol1[NORTH]+ol2[NORTH];
					ol0[BOTTOM]=ol1[BOTTOM]; 
					ol0[TOP]=ol2[TOP];
				}
				break;
			default:throw hiba("","program error -> dccella::nodered -> illegal side");
		}
		d->oldalak[CENTER]=0;
	}
}


//***********************************************************************
inline dccella::dccella(bool extended){
//***********************************************************************
	if(parhuzamosanfut)throw hiba("","dccella::dccella => parhuzamosanfut");
#ifdef DCCELLA_DIND
	if(!dblokk)dblokk=new dind;
	d=dblokk->alloc(azon);//new celladat;
#else
	d=new celladat;
#endif // DCCELLA_DIND

	// oc1,oc2

	d->oc1=CENTER;
	d->oc2=CENTER;
    d->nulladik_kulso_oldalszam=0;
	
	//C,C1,C2
	
	d->C=d->C1=d->C2=0;

	// symm,extended (t�rf�gg�!)
/*
	switch(start->tipus){
		case FieldTherm:	symm=true; break;
		case FieldElTherm_1:symm=false; break;
		case FieldElTherm_2:symm=isElthermModel_2_Symm; break;
		case FieldMech_1:	symm=true; break;
		case FieldMech_2:	symm=true; break;
		case FieldThMech:	symm=true; break;
		default:throw hiba("","program error -> unknown field type in cella::cella");
	}
*/	d->extended=extended;//(start->tipus==FieldMech_1||start->tipus==FieldThMech)?true:false;//feladat::allocStep0cells f�ggv�nyekben is van ilyen!!!

	// oldalak felt�lt�se

	if(d->extended)for(u32 i3=0;i3<EXTENDED_SIDES;i3++)d->oldalak[i3]=0;//i3=1 volt, nem tudom, mi�rt
	else for(u32 i3=0;i3<BASIC_SIDES;i3++)d->oldalak[i3]=0;
}


//***********************************************************************
inline void dccella::allocY(){
// Elore lefoglaljuk az Y m�trixokat, mert a k�vetkezo l�p�s kisz�m�t�sa
// ut�n az Y m�trixot t�r�lj�k, a t�bbire viszont sz�ks�g lesz k�sobb
//***********************************************************************
	if(d->oc1==CENTER){
		cu32 u=d->nulladik_kulso_oldalszam;
		d->y.resize(u);
		return;
	}
	if(d->C==0){// 2 bemeno cella van
		cu32 kozos=d->C1->d->oldalak[d->oc1],size=(d->C1->d->y.getcol())+(d->C2->d->y.getcol())-(kozos+kozos);
		d->y.resize(size);
	}
	else d->y.resize(d->C->d->y.getrow());
}


//***********************************************************************
inline void dccella::allocM(){
// Elore lefoglaljuk a m�trixokat, mert ha lemezre mentj�k oket, akkor ne
// t�redezzen fel a mem�ria
//***********************************************************************
	if(d->oc1==CENTER){
		cu32 u1=d->oldalak[CENTER],u2=d->nulladik_kulso_oldalszam;
		d->nzb.resize(u1);
		d->nxazb.resize(u2,u1);
		return;
	}
	if(d->C==0){// 2 bemeno cella van
		cu32 kozos=d->C1->d->oldalak[d->oc1],size=(d->C1->d->y.getcol())+(d->C2->d->y.getcol())-(kozos+kozos);
		d->nzb.resize(kozos);
		d->nxazb.resize(size,kozos);
	}
	else{
		d->nzb.resize(d->C->d->nzb.getrow());
		d->nxazb.resize(d->C->d->nxazb.getrow(),d->C->d->nxazb.getcol());
	}
}


//***********************************************************************
inline void dccella::allocV(){
//***********************************************************************
	if(d->oc1==CENTER){	//	Nulladik cella gy�rt�sa
		cu32 u1=d->oldalak[CENTER],u2=d->nulladik_kulso_oldalszam;
		d->j.resize(u2);
		d->jb.resize(u1);
		d->u.resize(u2);
		d->ub.resize(u1);
		return;
	}
	if(d->C==0){// 2 bemeno cella van// nem teljesen korrekt, mert j-b�l k�ne a m�reteket meghat�rozni
		u32 kozos=d->C1->d->oldalak[d->oc1],size=(d->C1->d->y.getcol())+(d->C2->d->y.getcol())-(kozos+kozos);
		d->j.resize(size);
		d->jb.resize(kozos);
		d->u.resize(size);
		d->ub.resize(kozos);
	}
	else{
		d->j.resize(d->C->d->j.getsiz());
		d->jb.resize(d->C->d->jb.getsiz());
		d->u.resize(d->C->d->u.getsiz());
		d->ub.resize(d->C->d->ub.getsiz());
	}
}


//***********************************************************************
inline void dccella::allocLast(){
//***********************************************************************
#ifdef vsundebugmode
	if(d->oc1==CENTER)throw hiba("","program error -> dccella::allocLast -> illegal call");
#endif
	if(d->C==0){// 2 bemeno cella van
		u32 kozos=d->C1->d->oldalak[d->oc1],size=(d->C1->d->y.getcol())+(d->C2->d->y.getcol())-(kozos*2);
#ifdef vsundebugmode
		if(size!=0)throw hiba("","program error -> dccella::allocLast -> size!=0");
#endif
//		d->j.resize(size);//0 m�ret�
		d->jb.resize(kozos);
//		d->u.resize(size);//0 m�ret�
		d->ub.resize(kozos);
		d->nzb.resize(kozos);
		//nxazb,nzbxb,y alapb�l 0, �s annyi is marad
	}
	else{// mindk�t t�rf�lben kell lennie valaminek az utols� l�p�sben
		throw hiba("","program error -> dccella::alloclast -> C!=0");
	}
}


//***********************************************************************
inline void dccella::saveM(FILE *fp)
//***********************************************************************
{
#ifdef junajtid
	d->y.save(fp);
#endif
	d->nzb.save(fp);
	d->nxazb.save(fp);
}


//***********************************************************************
inline void dccella::loadM(FILE *fp)
//***********************************************************************
{
#ifdef junajtid
	d->y.load(fp);
#endif
	d->nzb.load(fp);
	d->nxazb.load(fp);
}


//***********************************************************************
inline void dccella::calcI(dcvector & I,const dcvector & U)const{
//I=y*U+j
//***********************************************************************
	I.mul(d->y,U);
	I.addnr(I,d->j);
}


//***********************************************************************
inline void dccella::calcU(dcvector & U,const dcvector & I)const{
//U=inv(y)*(I-j)
//***********************************************************************
	dscmatrix z;
	dcvector ij;

	z.copy(d->y);

	ij.sub(d->j,I);
	z.ninv();
	U.mul(z,ij);
}

//***********************************************************************
inline void dccella::segedtombok(const dscmatrix ** honnan,u32 * ind1,u32 * ind2,u32 * index,u32 * kapocsszam,u32 * indy){
//seg�dm�trixok felt�lt�se (konstruktorban is van hasonl�, csak egy�tt v�ltoztathat�!)
//***********************************************************************
	cu16 *o1=d->C1->d->oldalak,*o2=d->C2->d->oldalak;
	const dscmatrix *y1=&(d->C1->d->y),*y2=&(d->C2->d->y);
	ind1[0]=ind2[0]=0;
	if(d->extended){
		for(u32 i=1;i<EXTENDED_SIDES;i++)ind1[i]=ind1[i-1]+o1[i-1];
		for(u32 i=1;i<EXTENDED_SIDES;i++)ind2[i]=ind2[i-1]+o2[i-1];
		switch(d->oc1){
			case EAST:{
					honnan[ 0]=y1;index[ 0]=ind1[EXTERNAL];	kapocsszam[ 0]=o1[EXTERNAL];
					honnan[ 1]=y2;index[ 1]=ind2[EXTERNAL];	kapocsszam[ 1]=o2[EXTERNAL];
					honnan[ 2]=y1;index[ 2]=ind1[WEST];		kapocsszam[ 2]=o1[WEST];
					honnan[ 3]=y2;index[ 3]=ind2[EAST];		kapocsszam[ 3]=o2[EAST];
					honnan[ 4]=y1;index[ 4]=ind1[SOUTH];	kapocsszam[ 4]=o1[SOUTH];
					honnan[ 5]=y2;index[ 5]=ind2[WS];		kapocsszam[ 5]=o2[WS];
					honnan[ 6]=y1;index[ 6]=ind1[ES];		kapocsszam[ 6]=o1[ES];
					honnan[ 7]=y2;index[ 7]=ind2[SOUTH];	kapocsszam[ 7]=o2[SOUTH];
					honnan[ 8]=y1;index[ 8]=ind1[NORTH];	kapocsszam[ 8]=o1[NORTH];
					honnan[ 9]=y1;index[ 9]=ind1[EN];		kapocsszam[ 9]=o1[EN];
					honnan[10]=y2;index[10]=ind2[WN];		kapocsszam[10]=o2[WN];
					honnan[11]=y2;index[11]=ind2[NORTH];	kapocsszam[11]=o2[NORTH];
					honnan[12]=y1;index[12]=ind1[BOTTOM];	kapocsszam[12]=o1[BOTTOM];
					honnan[13]=y2;index[13]=ind2[WB];		kapocsszam[13]=o2[WB];
					honnan[14]=y1;index[14]=ind1[EB];		kapocsszam[14]=o1[EB];
					honnan[15]=y2;index[15]=ind2[BOTTOM];	kapocsszam[15]=o2[BOTTOM];
					honnan[16]=y1;index[16]=ind1[TOP];		kapocsszam[16]=o1[TOP];
					honnan[17]=y1;index[17]=ind1[ET];		kapocsszam[17]=o1[ET];
					honnan[18]=y2;index[18]=ind2[WT];		kapocsszam[18]=o2[WT];
					honnan[19]=y2;index[19]=ind2[TOP];		kapocsszam[19]=o2[TOP];
					honnan[20]=y1;index[20]=ind1[WS];		kapocsszam[20]=o1[WS];
					honnan[21]=y1;index[21]=ind1[WN];		kapocsszam[21]=o1[WN];
					honnan[22]=y1;index[22]=ind1[WB];		kapocsszam[22]=o1[WB];
					honnan[23]=y1;index[23]=ind1[WT];		kapocsszam[23]=o1[WT];
					honnan[24]=y2;index[24]=ind2[ES];		kapocsszam[24]=o2[ES];
					honnan[25]=y2;index[25]=ind2[EN];		kapocsszam[25]=o2[EN];
					honnan[26]=y2;index[26]=ind2[EB];		kapocsszam[26]=o2[EB];
					honnan[27]=y2;index[27]=ind2[ET];		kapocsszam[27]=o2[ET];
					honnan[28]=y1;index[28]=ind1[SB];		kapocsszam[28]=o1[SB];
					honnan[29]=y2;index[29]=ind2[SB];		kapocsszam[29]=o2[SB];
					honnan[30]=y1;index[30]=ind1[ST];		kapocsszam[30]=o1[ST];
					honnan[31]=y2;index[31]=ind2[ST];		kapocsszam[31]=o2[ST];
					honnan[32]=y1;index[32]=ind1[NB];		kapocsszam[32]=o1[NB];
					honnan[33]=y2;index[33]=ind2[NB];		kapocsszam[33]=o2[NB];
					honnan[34]=y1;index[34]=ind1[NT];		kapocsszam[34]=o1[NT];
					honnan[35]=y2;index[35]=ind2[NT];		kapocsszam[35]=o2[NT];
				}
				break;
			case NORTH:{
					honnan[ 0]=y1;index[ 0]=ind1[EXTERNAL];	kapocsszam[ 0]=o1[EXTERNAL];
					honnan[ 1]=y2;index[ 1]=ind2[EXTERNAL];	kapocsszam[ 1]=o2[EXTERNAL];
					honnan[ 2]=y1;index[ 2]=ind1[WEST];		kapocsszam[ 2]=o1[WEST];
					honnan[ 3]=y2;index[ 3]=ind2[WS];		kapocsszam[ 3]=o2[WS];
					honnan[ 4]=y1;index[ 4]=ind1[WN];		kapocsszam[ 4]=o1[WN];
					honnan[ 5]=y2;index[ 5]=ind2[WEST];		kapocsszam[ 5]=o2[WEST];
					honnan[ 6]=y1;index[ 6]=ind1[EAST];		kapocsszam[ 6]=o1[EAST];
					honnan[ 7]=y1;index[ 7]=ind1[EN];		kapocsszam[ 7]=o1[EN];
					honnan[ 8]=y2;index[ 8]=ind2[ES];		kapocsszam[ 8]=o2[ES];
					honnan[ 9]=y2;index[ 9]=ind2[EAST];		kapocsszam[ 9]=o2[EAST];
					honnan[10]=y1;index[10]=ind1[SOUTH];	kapocsszam[10]=o1[SOUTH];
					honnan[11]=y2;index[11]=ind2[NORTH];	kapocsszam[11]=o2[NORTH];
					honnan[12]=y1;index[12]=ind1[BOTTOM];	kapocsszam[12]=o1[BOTTOM];
					honnan[13]=y2;index[13]=ind2[SB];		kapocsszam[13]=o2[SB];
					honnan[14]=y1;index[14]=ind1[NB];		kapocsszam[14]=o1[NB];
					honnan[15]=y2;index[15]=ind2[BOTTOM];	kapocsszam[15]=o2[BOTTOM];
					honnan[16]=y1;index[16]=ind1[TOP];		kapocsszam[16]=o1[TOP];
					honnan[17]=y1;index[17]=ind1[NT];		kapocsszam[17]=o1[NT];
					honnan[18]=y2;index[18]=ind2[ST];		kapocsszam[18]=o2[ST];
					honnan[19]=y2;index[19]=ind2[TOP];		kapocsszam[19]=o2[TOP];
					honnan[20]=y1;index[20]=ind1[WS];		kapocsszam[20]=o1[WS];
					honnan[21]=y2;index[21]=ind2[WN];		kapocsszam[21]=o2[WN];
					honnan[22]=y1;index[22]=ind1[WB];		kapocsszam[22]=o1[WB];
					honnan[23]=y2;index[23]=ind2[WB];		kapocsszam[23]=o2[WB];
					honnan[24]=y1;index[24]=ind1[WT];		kapocsszam[24]=o1[WT];
					honnan[25]=y2;index[25]=ind2[WT];		kapocsszam[25]=o2[WT];
					honnan[26]=y1;index[26]=ind1[ES];		kapocsszam[26]=o1[ES];
					honnan[27]=y2;index[27]=ind2[EN];		kapocsszam[27]=o2[EN];
					honnan[28]=y1;index[28]=ind1[EB];		kapocsszam[28]=o1[EB];
					honnan[29]=y2;index[29]=ind2[EB];		kapocsszam[29]=o2[EB];
					honnan[30]=y1;index[30]=ind1[ET];		kapocsszam[30]=o1[ET];
					honnan[31]=y2;index[31]=ind2[ET];		kapocsszam[31]=o2[ET];
					honnan[32]=y1;index[32]=ind1[SB];		kapocsszam[32]=o1[SB];
					honnan[33]=y1;index[33]=ind1[ST];		kapocsszam[33]=o1[ST];
					honnan[34]=y2;index[34]=ind2[NB];		kapocsszam[34]=o2[NB];
					honnan[35]=y2;index[35]=ind2[NT];		kapocsszam[35]=o2[NT];
				}
				break;
			case TOP:{
					honnan[ 0]=y1;index[ 0]=ind1[EXTERNAL];	kapocsszam[ 0]=o1[EXTERNAL];
					honnan[ 1]=y2;index[ 1]=ind2[EXTERNAL];	kapocsszam[ 1]=o2[EXTERNAL];
					honnan[ 2]=y1;index[ 2]=ind1[WEST];		kapocsszam[ 2]=o1[WEST];
					honnan[ 3]=y2;index[ 3]=ind2[WB];		kapocsszam[ 3]=o2[WB];
					honnan[ 4]=y1;index[ 4]=ind1[WT];		kapocsszam[ 4]=o1[WT];
					honnan[ 5]=y2;index[ 5]=ind2[WEST];		kapocsszam[ 5]=o2[WEST];
					honnan[ 6]=y1;index[ 6]=ind1[EAST];		kapocsszam[ 6]=o1[EAST];
					honnan[ 7]=y1;index[ 7]=ind1[ET];		kapocsszam[ 7]=o1[ET];
					honnan[ 8]=y2;index[ 8]=ind2[EB];		kapocsszam[ 8]=o2[EB];
					honnan[ 9]=y2;index[ 9]=ind2[EAST];		kapocsszam[ 9]=o2[EAST];
					honnan[10]=y1;index[10]=ind1[SOUTH];	kapocsszam[10]=o1[SOUTH];
					honnan[11]=y2;index[11]=ind2[SB];		kapocsszam[11]=o2[SB];
					honnan[12]=y1;index[12]=ind1[ST];		kapocsszam[12]=o1[ST];
					honnan[13]=y2;index[13]=ind2[SOUTH];	kapocsszam[13]=o2[SOUTH];
					honnan[14]=y1;index[14]=ind1[NORTH];	kapocsszam[14]=o1[NORTH];
					honnan[15]=y1;index[15]=ind1[NT];		kapocsszam[15]=o1[NT];
					honnan[16]=y2;index[16]=ind2[NB];		kapocsszam[16]=o2[NB];
					honnan[17]=y2;index[17]=ind2[NORTH];	kapocsszam[17]=o2[NORTH];
					honnan[18]=y1;index[18]=ind1[BOTTOM];	kapocsszam[18]=o1[BOTTOM];
					honnan[19]=y2;index[19]=ind2[TOP];		kapocsszam[19]=o2[TOP];
					honnan[20]=y1;index[20]=ind1[WS];		kapocsszam[20]=o1[WS];
					honnan[21]=y2;index[21]=ind2[WS];		kapocsszam[21]=o2[WS];
					honnan[22]=y1;index[22]=ind1[WN];		kapocsszam[22]=o1[WN];
					honnan[23]=y2;index[23]=ind2[WN];		kapocsszam[23]=o2[WN];
					honnan[24]=y1;index[24]=ind1[WB];		kapocsszam[24]=o1[WB];
					honnan[25]=y2;index[25]=ind2[WT];		kapocsszam[25]=o2[WT];
					honnan[26]=y1;index[26]=ind1[ES];		kapocsszam[26]=o1[ES];
					honnan[27]=y2;index[27]=ind2[ES];		kapocsszam[27]=o2[ES];
					honnan[28]=y1;index[28]=ind1[EN];		kapocsszam[28]=o1[EN];
					honnan[29]=y2;index[29]=ind2[EN];		kapocsszam[29]=o2[EN];
					honnan[30]=y1;index[30]=ind1[EB];		kapocsszam[30]=o1[EB];
					honnan[31]=y2;index[31]=ind2[ET];		kapocsszam[31]=o2[ET];
					honnan[32]=y1;index[32]=ind1[SB];		kapocsszam[32]=o1[SB];
					honnan[33]=y2;index[33]=ind2[ST];		kapocsszam[33]=o2[ST];
					honnan[34]=y1;index[34]=ind1[NB];		kapocsszam[34]=o1[NB];
					honnan[35]=y2;index[35]=ind2[NT];		kapocsszam[35]=o2[NT];
				}
				break;
			default:throw hiba("","program error -> dccella::segedtombok -> illegal side");
		}

		indy[0]=0;
		for(u32 i=1;i<36;i++)indy[i]=indy[i-1]+kapocsszam[i-1];
	}
	else{ // A CENTER-t nem sz�molja, mert csak a nulladikhoz kell
		for(u32 i=1;i<BASIC_SIDES;i++)ind1[i]=ind1[i-1]+o1[i-1];
		for(u32 i=1;i<BASIC_SIDES;i++)ind2[i]=ind2[i-1]+o2[i-1];
		switch(d->oc1){
			case EAST:{
					honnan[ 0]=y1;index[ 0]=ind1[EXTERNAL];	kapocsszam[ 0]=o1[EXTERNAL];
					honnan[ 1]=y2;index[ 1]=ind2[EXTERNAL];	kapocsszam[ 1]=o2[EXTERNAL];
					honnan[ 2]=y1;index[ 2]=ind1[WEST];		kapocsszam[ 2]=o1[WEST];
					honnan[ 3]=y2;index[ 3]=ind2[EAST];		kapocsszam[ 3]=o2[EAST];
					honnan[ 4]=y1;index[ 4]=ind1[SOUTH];	kapocsszam[ 4]=o1[SOUTH];
					honnan[ 5]=y2;index[ 5]=ind2[SOUTH];	kapocsszam[ 5]=o2[SOUTH];
					honnan[ 6]=y1;index[ 6]=ind1[NORTH];	kapocsszam[ 6]=o1[NORTH];
					honnan[ 7]=y2;index[ 7]=ind2[NORTH];	kapocsszam[ 7]=o2[NORTH];
					honnan[ 8]=y1;index[ 8]=ind1[BOTTOM];	kapocsszam[ 8]=o1[BOTTOM];
					honnan[ 9]=y2;index[ 9]=ind2[BOTTOM];	kapocsszam[ 9]=o2[BOTTOM];
					honnan[10]=y1;index[10]=ind1[TOP];		kapocsszam[10]=o1[TOP];
					honnan[11]=y2;index[11]=ind2[TOP];		kapocsszam[11]=o2[TOP];
				}
				break;
			case NORTH:{
					honnan[ 0]=y1;index[ 0]=ind1[EXTERNAL];	kapocsszam[ 0]=o1[EXTERNAL];
					honnan[ 1]=y2;index[ 1]=ind2[EXTERNAL];	kapocsszam[ 1]=o2[EXTERNAL];
					honnan[ 2]=y1;index[ 2]=ind1[WEST];		kapocsszam[ 2]=o1[WEST];
					honnan[ 3]=y2;index[ 3]=ind2[WEST];		kapocsszam[ 3]=o2[WEST];
					honnan[ 4]=y1;index[ 4]=ind1[EAST];		kapocsszam[ 4]=o1[EAST];
					honnan[ 5]=y2;index[ 5]=ind2[EAST];		kapocsszam[ 5]=o2[EAST];
					honnan[ 6]=y1;index[ 6]=ind1[SOUTH];	kapocsszam[ 6]=o1[SOUTH];
					honnan[ 7]=y2;index[ 7]=ind2[NORTH];	kapocsszam[ 7]=o2[NORTH];
					honnan[ 8]=y1;index[ 8]=ind1[BOTTOM];	kapocsszam[ 8]=o1[BOTTOM];
					honnan[ 9]=y2;index[ 9]=ind2[BOTTOM];	kapocsszam[ 9]=o2[BOTTOM];
					honnan[10]=y1;index[10]=ind1[TOP];		kapocsszam[10]=o1[TOP];
					honnan[11]=y2;index[11]=ind2[TOP];		kapocsszam[11]=o2[TOP];
				}
				break;
			case TOP:{
					honnan[ 0]=y1;index[ 0]=ind1[EXTERNAL];	kapocsszam[ 0]=o1[EXTERNAL];
					honnan[ 1]=y2;index[ 1]=ind2[EXTERNAL];	kapocsszam[ 1]=o2[EXTERNAL];
					honnan[ 2]=y1;index[ 2]=ind1[WEST];		kapocsszam[ 2]=o1[WEST];
					honnan[ 3]=y2;index[ 3]=ind2[WEST];		kapocsszam[ 3]=o2[WEST];
					honnan[ 4]=y1;index[ 4]=ind1[EAST];		kapocsszam[ 4]=o1[EAST];
					honnan[ 5]=y2;index[ 5]=ind2[EAST];		kapocsszam[ 5]=o2[EAST];
					honnan[ 6]=y1;index[ 6]=ind1[SOUTH];	kapocsszam[ 6]=o1[SOUTH];
					honnan[ 7]=y2;index[ 7]=ind2[SOUTH];	kapocsszam[ 7]=o2[SOUTH];
					honnan[ 8]=y1;index[ 8]=ind1[NORTH];	kapocsszam[ 8]=o1[NORTH];
					honnan[ 9]=y2;index[ 9]=ind2[NORTH];	kapocsszam[ 9]=o2[NORTH];
					honnan[10]=y1;index[10]=ind1[BOTTOM];	kapocsszam[10]=o1[BOTTOM];
					honnan[11]=y2;index[11]=ind2[TOP];		kapocsszam[11]=o2[TOP];
				}
				break;
			default:throw hiba("","program error -> dccella::segedtombok -> illegal side");
		}

		indy[0]=0;
		for(u32 i=1;i<12;i++)indy[i]=indy[i-1]+kapocsszam[i-1];
	}
}


//***********************************************************************
inline void dccella::reorder_externals_before_nodered_0(){
// Az texternal_azon-t is rendbe rakja, tov�bb� y-t �s nxazb-t
//***********************************************************************

    uns extern_db=0,k=0;
    for(uns i=0; i<BASIC_SIDES; i++)if(d->external_nulladik[i])extern_db++;
    if(extern_db==0){d->texternal_azon.clear();d->texternal_area.clear();return;}

    // texternal_azon

    k=0;
    for(uns i=0; i<BASIC_SIDES; i++)if(d->external_nulladik[i]){
        d->texternal_azon[k]=d->texternal_azon[i];
        d->texternal_area[k]=d->texternal_area[i];
        k++;
    }
    d->texternal_azon.resize(extern_db);
    d->texternal_area.resize(extern_db);

    // y �s nxazb

    dcmatrix y2;
    y2.resize(d->y.getrow(),d->y.getcol());

    // y oszlopai

    uns i, eredeti_oszlopa, uj_oszlopa=0, n1=y2.getcol();

    // externek m�sol�sa

    for( i=eredeti_oszlopa=0; i<BASIC_SIDES; eredeti_oszlopa+=d->oldalak[i++] )if(d->external_nulladik[i]){
        for(uns j=0; j<d->oldalak[i]; j++){
            for(uns k=0; k<n1; k++)y2.set(k,uj_oszlopa+j,d->y.get(k,eredeti_oszlopa+j));
            uj_oszlopa += d->oldalak[i];
        }
    }

    // nem externek m�sol�sa (�ssz k�l�nbs�g egy ! a k�t k�dr�szlet k�z�tt)

    for( i=eredeti_oszlopa=0; i<BASIC_SIDES; eredeti_oszlopa+=d->oldalak[i++] )if(!d->external_nulladik[i]){
        for(uns j=0; j<d->oldalak[i]; j++){
            for(uns k=0; k<n1; k++)y2.set(k,uj_oszlopa+j,d->y.get(k,eredeti_oszlopa+j));
            uj_oszlopa += d->oldalak[i];
        }
    }

    // y �s nxazb sorai m�sol�sa

    dcmatrix nxazb2;
    nxazb2.copy(d->nxazb);
    uns eredeti_sora, uj_sora=0, n2=nxazb2.getcol();
    n1 = y2.getcol();

    // externek m�sol�sa

    for( i=eredeti_sora=0; i<BASIC_SIDES; eredeti_sora+=d->oldalak[i++] )if(d->external_nulladik[i]){
        for(uns j=0; j<d->oldalak[i]; j++){
            for(uns k=0; k<n1; k++)d->    y.set(uj_sora+j,k,    y2.get(eredeti_sora+j,k));
            for(uns k=0; k<n2; k++)d->nxazb.set(uj_sora+j,k,nxazb2.get(eredeti_sora+j,k));
            uj_sora += d->oldalak[i];
        }
    }

    // nem externek m�sol�sa (�ssz k�l�nbs�g egy ! a k�t k�dr�szlet k�z�tt)

    for( i=eredeti_sora=0; i<BASIC_SIDES; eredeti_sora+=d->oldalak[i++] ) if(!d->external_nulladik[i]){
        for(uns j=0; j<d->oldalak[i]; j++){
            for(uns k=0; k<n1; k++)d->    y.set(uj_sora+j,k,    y2.get(eredeti_sora+j,k));
            for(uns k=0; k<n2; k++)d->nxazb.set(uj_sora+j,k,nxazb2.get(eredeti_sora+j,k));
            uj_sora += d->oldalak[i];
        }
    }
}


//***********************************************************************
inline void dccella::reorder_externals_before_forwsubs_0(){
// Az oldalak t�mb�t is rendbe rakja, tov�bb� j-t �s u-t
//***********************************************************************

    uns extern_db=0,k=0;
    for(uns i=0; i<BASIC_SIDES; i++)if(d->external_nulladik[i])extern_db++;
    if(extern_db==0)return;

    dcvector j2,u2;
    j2.copy(d->j);
    u2.copy(d->u);
    uns i, eredeti_sora,uj_sora=0;

    // externek m�sol�sa

    for( i=eredeti_sora=0; i<BASIC_SIDES; eredeti_sora+=d->oldalak[i++] ) if(d->external_nulladik[i]){
        for(uns j=0; j<d->oldalak[i]; j++){
            d->j.set(uj_sora+j,j2.get(eredeti_sora+j));
            d->u.set(uj_sora+j,u2.get(eredeti_sora+j));
            uj_sora += d->oldalak[i];
        }
    }

    // nem externek m�sol�sa (�ssz k�l�nbs�g egy ! a k�t k�dr�szlet k�z�tt)

    for( i=eredeti_sora=0; i<BASIC_SIDES; eredeti_sora+=d->oldalak[i++] ) if(!d->external_nulladik[i]){
        for(uns j=0; j<d->oldalak[i]; j++){
            d->j.set(uj_sora+j,j2.get(eredeti_sora+j));
            d->u.set(uj_sora+j,u2.get(eredeti_sora+j));
            uj_sora += d->oldalak[i];
        }
    }

    // oldalak t�mb ment�se �s �trendez�se

    if(d->oldalak[EXTERNAL]!=0)
        throw hiba("dccella::reorder_externals_before_forwsubs_0","d->oldalak[EXTERNAL]!=0");
    for(i=0; i<BASIC_SIDES; i++) 
        d->oldalak2[i] = d->oldalak[i];
    for(i=0; i<BASIC_SIDES; i++)if( i!=EXTERNAL && d->external_nulladik[i]){
        d->oldalak[EXTERNAL] += d->oldalak[i];
        d->oldalak[i] = 0;
    }
}


//***********************************************************************
inline void dccella::reorder_externals_after_backsubs_0(){
// Az oldalak t�mb�t is rendbe rakja, tov�bb� j-t �s u-t
//***********************************************************************

    uns extern_db=0,k=0;
    for(uns i=0; i<BASIC_SIDES; i++)if(d->external_nulladik[i])extern_db++;
    if(extern_db==0)return;

    dcvector j2,u2;
    j2.copy(d->j);
    u2.copy(d->u);
    uns i, eredeti_sora,uj_sora=0;

    // oldalak vissza�ll�t�sa

    for(i=0; i<BASIC_SIDES; i++)
        d->oldalak[i] = d->oldalak2[i];

    // externek m�sol�sa

    for( i=eredeti_sora=0; i<BASIC_SIDES; eredeti_sora+=d->oldalak[i++] ) if(d->external_nulladik[i]){
        for(uns j=0; j<d->oldalak[i]; j++){
            d->j.set(eredeti_sora+j,j2.get(uj_sora+j));
            d->u.set(eredeti_sora+j,u2.get(uj_sora+j));
            uj_sora += d->oldalak[i];
        }
    }

    // nem externek m�sol�sa (�ssz k�l�nbs�g egy ! a k�t k�dr�szlet k�z�tt)

    for( i=eredeti_sora=0; i<BASIC_SIDES; eredeti_sora+=d->oldalak[i++] ) if(!d->external_nulladik[i]){
        for(uns j=0; j<d->oldalak[i]; j++){
            d->j.set(eredeti_sora+j,j2.get(uj_sora+j));
            d->u.set(eredeti_sora+j,u2.get(uj_sora+j));
            uj_sora += d->oldalak[i];
        }
    }
}


//***********************************************************************
extern dscmatrix dc_y_extern;
extern dcvector  dc_j_extern;
extern dcvector  dc_u_extcom;
extern dcvector  dc_i_extcom;
extern dscmatrix dc_y_compmod;
extern dcvector  dc_j_compmod;
//***********************************************************************

//***********************************************************************
inline void dccella::extern_y_reduce(){
// felt�tel: egy cell�b�l �ll a h�l�zat, melynek kapcsai az external csp-k
// texternal_azon t�mb seg�ts�g�vel kisz�m�tja a kompakt modellhez sz�ks�ges
// reduk�lt dc_y_extern-t
//***********************************************************************

    // dc_y_extern n�ret�lnek ksz�m�t�sa

    uns n = 0;
    uns db = d->texternal_azon.size();
    for(uns i=0; i<db; i++)if(d->texternal_azon[i]>n)n=d->texternal_azon[i];
    dc_y_extern.resize(n); dc_y_extern.zero(); // null�zza is!

    // redukci�
    
    for(uns i=0; i<db; i++)for(uns j=i; j<db;j++){// j=i !!!!
        if(d->texternal_azon[i]==d->texternal_azon[j]){
            dc_y_extern.inc(d->texternal_azon[i]-1,d->texternal_azon[j]-1,(i==j?1.0:2.0)*d->y.get(i,j));
//            if(i!=j&&d->y.get(i,j)>=0)printf("%3u,%3u: y[%u,%u]+=%g = %g\n",i,j,d->texternal_azon[i]-1,d->texternal_azon[j]-1,d->y.get(i,j),dc_y_extern.get(d->texternal_azon[i]-1,d->texternal_azon[j]-1));
        } // az (i,j) �s (j,i) index� elemet is hozz� kell adni, ami egyforma, de (i,i)-t csak egyszer
        else{
            dc_y_extern.inc(d->texternal_azon[i]-1,d->texternal_azon[j]-1,d->y.get(i,j));
//            if(i!=j&&d->y.get(i,j)>=0)printf("%3u,%3u: y[%u,%u]+=%g = %g\n",i,j,d->texternal_azon[i]-1,d->texternal_azon[j]-1,d->y.get(i,j),dc_y_extern.get(d->texternal_azon[i]-1,d->texternal_azon[j]-1));
        }
    }
//    dc_y_extern.print(true);
}


//***********************************************************************
inline void dccella::extern_j_reduce(){
// felt�tel: egy cell�b�l �ll a h�l�zat, melynek kapcsai az external csp-k
// texternal_azon t�mb seg�ts�g�vel kisz�m�tja a kompakt modellhez sz�ks�ges
// reduk�lt dc_j_extern-t
//***********************************************************************

    // dc_j_extern n�ret�lnek ksz�m�t�sa

    uns n = 0;
    uns db = d->texternal_azon.size();
    for(uns i=0; i<db; i++)if(d->texternal_azon[i]>n)n=d->texternal_azon[i];
    dc_j_extern.resize(n); dc_j_extern.zero(); // null�zza is!

    // redukci�
    
    for(uns i=0; i<db; i++){
        dc_j_extern.inc(d->texternal_azon[i]-1,d->j.get(i));
    }
//    dc_j_extern.print();
}


//***********************************************************************
inline void dccella::extern_solve(){
// felt�tel: egy cell�b�l �ll a h�l�zat, melynek kapcsai az external csp-k
// rendelkez�sre �ll: dc_y_extern, dc_j_extern, dc_y_compmod, dc_j_compmod
// kisz�m�tja: dc_u_extcom-ot �s dc_i_extcom-ot
//***********************************************************************

    // dc_u_extcom n�ret�lnek ksz�m�t�sa

    uns n = 0;
    uns db = d->texternal_azon.size();
    for(uns i=0; i<db; i++)if(d->texternal_azon[i]>n)n=d->texternal_azon[i];
    dc_u_extcom.resize(n); dc_u_extcom.zero(); // null�zza is!
    dc_i_extcom.resize(n); dc_i_extcom.zero(); // null�zza is!
    if(dc_y_extern.getcol()!=n)
        throw hiba("dccella::extern_solve_u","r_y_extern.getcol()!=n (%u!=%u)",dc_y_extern.getcol(),n);
    if(dc_y_compmod.getcol()!=n)
        throw hiba("dccella::extern_solve_u","dc_y_compmod.getcol()!=n (%u!=%u)",dc_y_compmod.getcol(),n);

    // dc_u_extcom sz�m�t�sa

    dscmatrix nzb;
    nzb.resize(n);
    nzb.add(dc_y_extern,dc_y_compmod);
    nzb.ninv();

    dcvector jb;
    jb.resize(n);
    jb.add(dc_j_extern,dc_j_compmod);

    dc_u_extcom.tmul(nzb,jb);

    // dc_i_extcom sz�m�t�sa

    jb.mul(dc_y_extern,dc_u_extcom);
    dc_i_extcom.add(jb,dc_j_extern);
}


//***********************************************************************
inline void dccella::extern_backsubs(bool aramot){
// felt�tel: egy cell�b�l �ll a h�l�zat, melynek kapcsai az external csp-k
// dc_u_extcom-b�l be�ll�tja az externap csp-k u-j�t
//***********************************************************************

    uns db = d->texternal_azon.size();
    if(aramot){
        uns n = 0;
        for(uns i=0; i<db; i++)if(d->texternal_azon[i]>n)n=d->texternal_azon[i];

        tomb<dbl> area;
        area.resize(n);
        for(uns i=0; i<n; i++)area[i]=0.0;
        for(uns i=0; i<db; i++)area[d->texternal_azon[i]-1] += d->texternal_area[i];
        dcvector I;
        I.resize(db);
        for(uns i=0; i<db; i++){
            I.set(i,dc_i_extcom.get(d->texternal_azon[i]-1) * d->texternal_area[i]/area[d->texternal_azon[i]-1]);
        }
        calcU(d->u,I);
    }
    else{
        for(uns i=0; i<db; i++){
            d->u.set(i,dc_u_extcom.get(d->texternal_azon[i]-1));
        }
    }
}


//***********************************************************************
inline void dccella::nodered(bool isforws){
// C1,C2 => y, nzb, nxazb, nzbxb
// a haszn�lt if-ek fel�p�t�s�n�l kihaszn�ltuk, hogy a k�z�s oldal
// mindig a C1 i. �s a C2 i+1-edik oldala!
// forwsubsot is megh�vja
//***********************************************************************
	if(d->oc1==CENTER){	// nulladik szint� redukci� (a cell�k m�r sz�t vannak v�lasztva)
        reorder_externals_before_nodered_0();
        d->nzb.ninv();		// nzb tartalmazza kezdetben yb-t
		dcmatrix xx;
		xx.copy(d->nxazb);	// nxazb tartalmazza kezdetben xa-t
		d->nxazb.tmul(xx,d->nzb);
		d->y.tmul(d->nxazb,xx,true);//muladd
	}
	else if(d->C==0){// 2 bemeno cella van
			
		// el�sz�r az admittanciam�trixot gener�ljuk

		const dscmatrix *honnan[36],*y1=&(d->C1->d->y),*y2=&(d->C2->d->y);//A k�t �sszevont oldal �s a k�t center hi�nyzik a 40-b�l (2*EXTENDED_SIDES)
		u32 index[36],kapocsszam[36];
		cu16 *o1=d->C1->d->oldalak,*o2=d->C2->d->oldalak;
		u32 ind1[EXTENDED_SIDES],ind2[EXTENDED_SIDES],indy[36];

#ifdef vsundebugmode
		if(d->oc1!=d->oc2+1)throw hiba("","program error -> dccella::nodered -> oc1!=oc2+1 (%u!=%u)",d->oc1,d->oc2+1);//uj
		if(o1[d->oc1]!=o2[d->oc2])throw hiba("","program error -> dccella::nodered -> o1[d->oc1]!=o2[d->oc2] (%u!=%u)",o1[d->oc1],o2[d->oc2]);//uj
#endif

		cu32 ciklus=d->extended?36:12;
		segedtombok(honnan,ind1,ind2,index,kapocsszam,indy);

		// parsing

		// az admittanciam�trix (y) felt�lt�se

		d->y.zero();
		for(u32 i=0;i<ciklus;i++)if(kapocsszam[i]){
			d->y.getsetsubmatrix(*honnan[i],index[i],indy[i],kapocsszam[i]);
			for(u32 j=i+1;j<ciklus;j++)if(kapocsszam[j]&&honnan[i]==honnan[j]){// a t�bbi elem nemszimmetrikus
				d->y.getsetsubmatrix(*honnan[j],index[i],index[j],indy[i],indy[j],kapocsszam[i],kapocsszam[j]);
			}
		}

		// v�ltoz�k

		dcmatrix xa;
		xa.resize(d->nxazb.getrow(),d->nxazb.getcol());

		// xa

		for(u32 i=0;i<ciklus;i++)if(kapocsszam[i]){
			if(honnan[i]==y1)xa.getsetsubmatrix(*y1,index[i],ind1[d->oc1],indy[i],0,kapocsszam[i],o1[d->oc1]);
			else			 xa.getsetsubmatrix(*y2,index[i],ind2[d->oc2],indy[i],0,kapocsszam[i],o2[d->oc2]);
		}

		// yb

		d->nzb.getsubmatrix(*y1,ind1[d->oc1],o1[d->oc1]);
		d->nzb.getsetsubmatrixadd(*y2,ind2[d->oc2],0,o2[d->oc2]);

		// matematikai m�veletek

		d->nzb.ninv();
		d->nxazb.tmul(xa,d->nzb);
		d->y.tmul(d->nxazb,xa,true);//muladd,y=ya-x�zb�xt

        // texternal_azon, texternal_area

        d->texternal_azon=d->C1->d->texternal_azon;
        d->texternal_azon.add(d->C2->d->texternal_azon);
        d->texternal_area=d->C1->d->texternal_area;
        d->texternal_area.add(d->C2->d->texternal_area);
	}
	else{
		// C => y, nzb, nxazb, nzbxb, texternal_azon, texternal_area
		d->y.copy(d->C->d->y);
		d->nzb.copy(d->C->d->nzb);
		d->nxazb.copy(d->C->d->nxazb);
        d->texternal_azon=d->C->d->texternal_azon;
        d->texternal_area=d->C->d->texternal_area;
	}

	if(isforws)forwsubs();

}


//***********************************************************************
inline void dccella::forwsubs(){
// c1,c2 => jb,j
//***********************************************************************
	if(d->oc1==CENTER){	// nulladik szint� redukci� (a cell�k m�r sz�t vannak v�lasztva)
        reorder_externals_before_forwsubs_0();
		d->j.tmul(d->nxazb,d->jb,true);// �t�rva 2011.03.27.=>muladd, a seebeck-effektus miatt   //mul, nem muladd!!! kezdetben j 0 kell legyen, csak jb nem 0 (fels�bb szinteken m�r muladd!)
	}                                  // A logitermikus miatt is kell a muladd! (2014.06.20.)
	else if(d->C==0){// 2 bemeno cella van
		const dscmatrix *honnan[36],*y1=&(d->C1->d->y),*y2=&(d->C2->d->y);//A k�t �sszevont oldal �s a k�t center hi�nyzik a 40-b�l (2*EXTENDED_SIDES)
		u32 index[36],kapocsszam[36];
		cu16 *o1=d->C1->d->oldalak,*o2=d->C2->d->oldalak;
		u32 ind1[EXTENDED_SIDES],ind2[EXTENDED_SIDES],indy[36];

		cu32 ciklus=d->extended?36:12;
		segedtombok(honnan,ind1,ind2,index,kapocsszam,indy);

		// sz�ks�ges v�ltoz�k

		const dcvector &j1=d->C1->d->j,&j2=d->C2->d->j;

		// j

		for(u32 i=0;i<ciklus;i++)if(kapocsszam[i]){
			d->j.getsetsubvector(honnan[i]==y1?j1:j2,index[i],indy[i],kapocsszam[i]);
		}

		// jb

		d->jb.getsubvector(j1,ind1[d->oc1],o1[d->oc1]);
		d->jb.getsetaddsubvector(j2,ind2[d->oc2],0,o2[d->oc2]);
		d->j.tmul(d->nxazb,d->jb,true);//muladd
	}
	else
	{
		d->j.copy(d->C->d->j);
		d->jb.copy(d->C->d->jb);
	}
}


//***********************************************************************
inline void dccella::backsubs(){
//ub=nzbxb*u+nzb*jb
//Kisz�molja c1 �s c2 peremcsom�pontjainak fesz�lt�sg�t (vagyis az � UA-jukat)
//***********************************************************************

	d->ub.tmult(d->nxazb,d->u);
	d->ub.tmul(d->nzb,d->jb,true);//muladd

	if(d->oc1==CENTER){	// nulladik szint� redukci� (a cell�k m�r sz�t vannak v�lasztva)
        reorder_externals_after_backsubs_0();
	}
	else if(d->C==0){// 2 bemeno cella van
		const dscmatrix *honnan[36],*y1=&(d->C1->d->y),*y2=&(d->C2->d->y);//A k�t �sszevont oldal �s a k�t center hi�nyzik a 40-b�l (2*EXTENDED_SIDES)
		u32 index[36],kapocsszam[36];
		cu16 *o1=d->C1->d->oldalak,*o2=d->C2->d->oldalak;
		u32 ind1[EXTENDED_SIDES],ind2[EXTENDED_SIDES],indy[36];

		cu32 ciklus=d->extended?36:12;
		segedtombok(honnan,ind1,ind2,index,kapocsszam,indy);

		// sz�ks�ges v�ltoz�k

		dcvector &u1=d->C1->d->u,&u2=d->C2->d->u;

		// ua

		for(u32 i=0;i<ciklus;i++)if(kapocsszam[i]){
			if(honnan[i]==y1)	u1.getsetsubvector(d->u,indy[i],index[i],kapocsszam[i]);
			else				u2.getsetsubvector(d->u,indy[i],index[i],kapocsszam[i]);
		}

		// ub

		u1.setsubvector(d->ub,ind1[d->oc1]);
		u2.setsubvector(d->ub,ind2[d->oc2]);
	}
	else{
		d->C->d->u.copy(d->u);
		d->C->d->ub.copy(d->ub);//ezt ut�lag tettem be, nem tudom, hogy eredetileg mi�rt nem
	}
}


#endif
