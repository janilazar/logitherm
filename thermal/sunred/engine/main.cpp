//***********************************************************************
// main
// Creation date:  2009. 07. 11.
// Creator:        Pohl L�szl�
//***********************************************************************


//***********************************************************************
#include "main.h"
//***********************************************************************


//***********************************************************************
bool GlobalUseTmp=false; //ezent�l itt kell �ll�tani
cd STRASSEN_INV_DEFAULT=500;
cd STRASSEN_MUL_DEFAULT=500;
cd MUL_DEFAULT=500;
dbl STRASSEN_INV_LIMIT=STRASSEN_INV_DEFAULT;	// enn�l nagyobb, kett�vel oszthat� oldalhissz�s�g� m�trixokat strassen algoritmussal invert�l
dbl STRASSEN_MUL_LIMIT=STRASSEN_MUL_DEFAULT;	// enn�l nagyobb, kett�vel oszthat� oldalhissz�s�g� m�trixokat strassen algoritmussal szoroz
dbl MUL_LIMIT=MUL_DEFAULT;	                    // enn�l nagyobb, kett�vel oszthat� oldalhissz�s�g� m�trixokat t�bb sz�lon szoroz
//***********************************************************************


//***********************************************************************
char hibaUzenet[1024];
const char * getHiba(){return hibaUzenet;}
//***********************************************************************


apa *logitherm_apa = NULL;

void logitherm_init(unsigned X_res, unsigned Y_res, unsigned Z_res, double x, double y, double z, double Si_hovez, double Si_hokapac,
    PeremTipus Wt, double Wv, PeremTipus Et, double Ev, PeremTipus St, double Sv, PeremTipus Nt, double Nv,
    PeremTipus Bt, double Bv, PeremTipus Tt, double Tv, double ambient_temp, double time_step, bool linear, const double * p_map, double * T_map) {
    delete logitherm_apa;
    logitherm_apa = new apa(X_res, Y_res, Z_res, x, y, z, Si_hovez, Si_hokapac, Wt, Wv, Et, Ev, St, Sv, Nt, Nv, Bt, Bv, Tt, Tv, ambient_temp, time_step, linear, p_map, T_map);
}

void logitherm_step(const double * p_map, double * T_map) {
    logitherm_apa->logitherm_next_step(p_map, T_map);
}

void logitherm_destruct() {
    delete logitherm_apa;
}