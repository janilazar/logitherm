//***********************************************************************
// Vector SUNRED dcomplex class header
// Creation date:	2004. 03. 19.
// Creator:			Pohl L�szl�
// Owner:			freeware
//***********************************************************************


//***********************************************************************
#ifndef VSUN_DCOMPLEX_HEADER
#define	VSUN_DCOMPLEX_HEADER
//***********************************************************************


//***********************************************************************
#include "tipusok.h"

//***********************************************************************
class dcomplex {
public:
			double re, im;

			dcomplex():re(0),im(0){;}
			dcomplex(const double ire,const double iim=0):re(ire),im(iim){}

	dcomplex & operator+=(const double a){re+=a;return *this;}
	dcomplex & operator+=(const dcomplex a){re+=a.re;im+=a.im;return *this;}
	dcomplex & operator-=(const double a){re-=a;return *this;}
	dcomplex & operator-=(const dcomplex a){re-=a.re;im-=a.im;return *this;}
	dcomplex & operator*=(const double a){re*=a;im*=a;return *this;}
	dcomplex & operator*=(const dcomplex a);
	dcomplex & operator/=(const double a){re/=a;im/=a;return *this;}
	dcomplex & operator/=(const dcomplex a);
	dcomplex   operator+()const{return *this;}
	dcomplex   operator-()const{return dcomplex(-re, -im);}

	dcomplex & Transp(){return *this;}
	double	  det(){return (double)sqrt(re*re+im*im);}

inline friend dcomplex operator+(const dcomplex a,const dcomplex b){return dcomplex(a.re+b.re,a.im+b.im);}
inline friend dcomplex operator+(const double a,const dcomplex b){return dcomplex(a+b.re,b.im);}
inline friend dcomplex operator+(const dcomplex a,const double b){return dcomplex(a.re+b,a.im);}
inline friend dcomplex operator-(const dcomplex a,const dcomplex b){return dcomplex(a.re-b.re,a.im-b.im);}
inline friend dcomplex operator-(const double a,const dcomplex b){return dcomplex(a-b.re,-b.im);}
inline friend dcomplex operator-(const dcomplex a,const double b){return dcomplex(a.re-b,a.im);}
inline friend dcomplex operator*(const dcomplex a,const dcomplex b){return dcomplex(a.re*b.re-a.im*b.im,a.im*b.re+b.im*a.re);}

inline friend dcomplex operator*(const dcomplex a,const double b){return dcomplex(a.re*b,a.im*b);}
inline friend dcomplex operator*(const double a,const dcomplex b){return dcomplex(b.re*a,b.im*a);}
friend dcomplex operator/(const dcomplex a,const dcomplex b);
inline friend dcomplex operator/(const dcomplex a,const double b){return dcomplex(a.re/b,a.im/b);}
friend dcomplex operator/(const double a,const dcomplex b); 

inline friend int operator==(const dcomplex a,const dcomplex b){return (a.re==b.re)&&(a.im==b.im);}
inline friend int operator!=(const dcomplex a,const dcomplex b){return (a.re!=b.re)||(a.im!=b.im);}

inline friend double real(const dcomplex a){return a.re;}   
inline friend double imag(const dcomplex a){return a.im;}   
inline friend dcomplex conj(const dcomplex a){return dcomplex(a.re,-a.im);}  
inline friend double norm(const dcomplex a){return (double)sqrt(a.re*a.re+a.im*a.im);}  
inline friend double arg(const dcomplex a){return (double)atan2(a.im,a.re);} 
friend dcomplex sqrt(const dcomplex a);
inline friend dcomplex polar(const double a,const double b=0){return dcomplex(a*(double)cos(b),a*(double)sin(b));}

inline friend double det(const dcomplex & a){return (double)sqrt(a.re*a.re+a.im*a.im);}
inline friend double fabs(const dcomplex & a){return (double)sqrt(a.re*a.re+a.im*a.im);}
inline friend void separate(const dcomplex & a,double & r,double & i){r=a.re;i=a.im;}
};
//***********************************************************************


//***********************************************************************
inline dcomplex sqrt(const dcomplex a) 
//***********************************************************************
{
	const double ar=double(atan2(a.im,a.re)),no=double(sqrt(norm(a)));
	return dcomplex(no*double(cos(ar*0.5)),no*double(sin(ar*0.5)));
}


//***********************************************************************
inline dcomplex& dcomplex::operator*=(const dcomplex a)
//***********************************************************************
{
    double temp=re;

	re = re*a.re-im*a.im;
    im = im*a.re+temp*a.im;
    return *this;
}


//***********************************************************************
inline dcomplex& dcomplex::operator/=(const dcomplex a)
//***********************************************************************
{
    double temp1=a.im*a.im+a.re*a.re;
	double temp2=re;

	re = (re*a.re+im*a.im)/temp1;
    im = (im*a.re-re*a.im)/temp1;
    return *this;
}


//***********************************************************************
inline dcomplex operator/(const dcomplex a,const dcomplex b) 
//***********************************************************************
{
    double temp=b.re*b.re+b.im*b.im;

	return dcomplex((a.re*b.re+a.im*b.im)/temp,
		           (a.im*b.re-a.re*b.im)/temp);
}


//***********************************************************************
inline dcomplex operator/(const double a,const dcomplex b) 
//***********************************************************************
{
    double temp=b.re*b.re+b.im*b.im;

	return dcomplex(a*b.re/temp,-a*b.im/temp);
}

#endif
