//***********************************************************************
// Vector SUNRED qcomplex class header
// Creation date:	2004. 03. 19.
// Creator:			Pohl L�szl�
// Owner:			freeware
//***********************************************************************


//***********************************************************************
#ifndef VSUN_QCOMPLEX_HEADER
#define	VSUN_QCOMPLEX_HEADER
//***********************************************************************


//***********************************************************************
#include "tipusok.h"

//***********************************************************************
class qcomplex {
public:
			quad_float re, im;

			qcomplex():re(qnulla),im(qnulla){;}
			qcomplex(const quad_float ire,const quad_float iim=qnulla):re(ire),im(iim){}

	qcomplex & operator+=(const quad_float a){re+=a;return *this;}
	qcomplex & operator+=(const qcomplex a){re+=a.re;im+=a.im;return *this;}
	qcomplex & operator-=(const quad_float a){re-=a;return *this;}
	qcomplex & operator-=(const qcomplex a){re-=a.re;im-=a.im;return *this;}
	qcomplex & operator*=(const quad_float a){re*=a;im*=a;return *this;}
	qcomplex & operator*=(const qcomplex a);
	qcomplex & operator/=(const quad_float a){re/=a;im/=a;return *this;}
	qcomplex & operator/=(const qcomplex a);
	qcomplex   operator+()const{return *this;}
	qcomplex   operator-()const{return qcomplex(-re, -im);}

	qcomplex & Transp(){return *this;}
	quad_float	  det(){return (quad_float)sqrt(re*re+im*im);}

inline friend qcomplex operator+(const qcomplex a,const qcomplex b){return qcomplex(a.re+b.re,a.im+b.im);}
inline friend qcomplex operator+(const quad_float a,const qcomplex b){return qcomplex(a+b.re,b.im);}
inline friend qcomplex operator+(const qcomplex a,const quad_float b){return qcomplex(a.re+b,a.im);}
inline friend qcomplex operator-(const qcomplex a,const qcomplex b){return qcomplex(a.re-b.re,a.im-b.im);}
inline friend qcomplex operator-(const quad_float a,const qcomplex b){return qcomplex(a-b.re,-b.im);}
inline friend qcomplex operator-(const qcomplex a,const quad_float b){return qcomplex(a.re-b,a.im);}
inline friend qcomplex operator*(const qcomplex a,const qcomplex b){return qcomplex(a.re*b.re-a.im*b.im,a.im*b.re+b.im*a.re);}

inline friend qcomplex operator*(const qcomplex a,const quad_float b){return qcomplex(a.re*b,a.im*b);}
inline friend qcomplex operator*(const quad_float a,const qcomplex b){return qcomplex(b.re*a,b.im*a);}
friend qcomplex operator/(const qcomplex a,const qcomplex b);
inline friend qcomplex operator/(const qcomplex a,const quad_float b){return qcomplex(a.re/b,a.im/b);}
friend qcomplex operator/(const quad_float a,const qcomplex b); 

inline friend int operator==(const qcomplex a,const qcomplex b){return (a.re==b.re)&&(a.im==b.im);}
inline friend int operator!=(const qcomplex a,const qcomplex b){return (a.re!=b.re)||(a.im!=b.im);}

inline friend quad_float real(const qcomplex a){return a.re;}   
inline friend quad_float imag(const qcomplex a){return a.im;}   
inline friend qcomplex conj(const qcomplex a){return qcomplex(a.re,-a.im);}  
inline friend quad_float norm(const qcomplex a){return sqrt(a.re*a.re+a.im*a.im);}  
inline friend quad_float arg(const qcomplex a){return d2q(atan2(q2d(a.im),q2d(a.re)));} 
friend qcomplex sqrt(const qcomplex a);
inline friend qcomplex polar(const quad_float a,const quad_float b=qnulla){return qcomplex(a*cos(q2d(b)),a*sin(q2d(b)));}

inline friend quad_float det(const qcomplex & a){return (quad_float)sqrt(a.re*a.re+a.im*a.im);}
inline friend quad_float fabs(const qcomplex & a){return (quad_float)sqrt(a.re*a.re+a.im*a.im);}
inline friend void separate(const qcomplex & a,quad_float & r,quad_float & i){r=a.re;i=a.im;}
};
//***********************************************************************


//***********************************************************************
inline qcomplex sqrt(const qcomplex a) 
//***********************************************************************
{
	const quad_float ar=d2q(atan2(q2d(a.im),q2d(a.re))),no=sqrt(norm(a));
	return qcomplex(no*d2q(cos(q2d(ar)*0.5)),no*d2q(sin(q2d(ar)*0.5)));
}


//***********************************************************************
inline qcomplex& qcomplex::operator*=(const qcomplex a)
//***********************************************************************
{
    quad_float temp=re;

	re = re*a.re-im*a.im;
    im = im*a.re+temp*a.im;
    return *this;
}


//***********************************************************************
inline qcomplex& qcomplex::operator/=(const qcomplex a)
//***********************************************************************
{
    quad_float temp1=a.im*a.im+a.re*a.re;
	quad_float temp2=re;

	re = (re*a.re+im*a.im)/temp1;
    im = (im*a.re-re*a.im)/temp1;
    return *this;
}


//***********************************************************************
inline qcomplex operator/(const qcomplex a,const qcomplex b) 
//***********************************************************************
{
    quad_float temp=b.re*b.re+b.im*b.im;

	return qcomplex((a.re*b.re+a.im*b.im)/temp,
		           (a.im*b.re-a.re*b.im)/temp);
}


//***********************************************************************
inline qcomplex operator/(const quad_float a,const qcomplex b) 
//***********************************************************************
{
    quad_float temp=b.re*b.re+b.im*b.im;

	return qcomplex(a*b.re/temp,-a*b.im/temp);
}

#endif
