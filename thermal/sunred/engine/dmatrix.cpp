//***********************************************************************
// Vector SUNRED 2 matrix source
// Creation date:	2008. 04. 09.
// Creator:			Pohl L�szl�
//***********************************************************************


//***********************************************************************
#ifndef VSUN2_DMATRIX_SOURCE
#define	VSUN2_DMATRIX_SOURCE
//***********************************************************************


//***********************************************************************
#include "dmatrix.h"
//***********************************************************************


ddinmem * dvsun_memoria=new ddinmem;

//***********************************************************************
void dmBase::save(FILE *fp)const{
//***********************************************************************
	char Head[4];Head[0]='S';Head[1]='R';Head[2]='M';
	Head[3]=char(sizeof(dbl));
	if(fwrite(Head,sizeof(char),4,fp)!=4)throw hiba("","dmBase::save => fwrite(Head,sizeof(char),4,fp)!=4");
	if(fwrite(&siz,sizeof(siz),1,fp)!=1)throw hiba("","dmBase::save => fwrite(&siz,sizeof(siz),1,fp)!=1");
	if(fwrite(&row,sizeof(row),1,fp)!=1)throw hiba("","dmBase::save => fwrite(&row,sizeof(row),1,fp)!=1");
	if(fwrite(&col,sizeof(col),1,fp)!=1)throw hiba("","dmBase::save => fwrite(&col,sizeof(col),1,fp)!=1");
	if(siz)if(fwrite(t,sizeof(dbl),siz,fp)!=siz)throw hiba("","dmBase::save => fwrite(re,sizeof(dbl),siz,fp)!=siz");
}

//***********************************************************************
void dmBase::savetext(const char * FileName)const{
//***********************************************************************
	FILE * fp=fopen(FileName,"wt");
	if(fp==NULL)throw hiba("","dmBase::savetext => cannot open %s",FileName);
	fprintf(fp,"row=%lu\ncol=%lu\nsiz=%lu\n\n",row,col,siz);
	for(u32 i=0;i<siz;i++)fprintf(fp,"%+16.16f\n",t[i]);
	fclose(fp);
}


//***********************************************************************
void dmBase::load(FILE *fp){
//***********************************************************************
	char Head[5]="XXXX";
	u32 size,rowe,cole;
	if(fread(Head,sizeof(char),4,fp)!=4)throw hiba("","dmBase::load => cannot read matrix head");
	if(strncmp(Head,"SRM",3)){Head[3]=0;throw hiba("","dmBase::load => Incorrect header. SRM expected, %s found",Head);}
	if(Head[3]!=char(sizeof(dbl)))throw hiba("","dmBase::load => incorrect data type (dbl size=%u)",sizeof(dbl));
	if(fread(&size,sizeof(size),1,fp)!=1)throw hiba("","dmBase::load => cannot read matrix size");
	if(fread(&rowe,sizeof(rowe),1,fp)!=1)throw hiba("","dmBase::load => cannot read matrix row");
	if(fread(&cole,sizeof(cole),1,fp)!=1)throw hiba("","dmBase::load => cannot read matrix col");
	if(size){
		resize(rowe,cole);
		if(fread(t,sizeof(dbl),siz,fp)!=siz)throw hiba("","dmBase::load => cannot read matrix data");
	}
}


//***********************************************************************
void drmatrix::submatrixadd(const drmatrix & src1,cu32 y1,cu32 x1,const drmatrix & src2,cu32 y2,cu32 x2,cu32 y,cu32 x){
// src1-b�l x1,y1-t�l, src2-b�l x2,y2-t�l ad �ssze egy x,y m�ret� blokkot
//***********************************************************************
#ifdef vsundebugmode
	if(x1+x>src1.col)throw hiba("","drmatrix::submatrixadd => x1+x>src1.col => %lu+%lu>%lu",x1,x,src1.col);
	if(y1+y>src1.row)throw hiba("","drmatrix::submatrixadd => y1+y>src1.row => %lu+%lu>%lu",y1,y,src1.row);
	if(x2+x>src2.col)throw hiba("","drmatrix::submatrixadd => x2+x>src2.col => %lu+%lu>%lu",x2,x,src2.col);
	if(y2+y>src2.row)throw hiba("","drmatrix::submatrixadd => y2+y>src2.row => %lu+%lu>%lu",y2,y,src2.row);
#endif
	resize(y,x);
	u32 n=0;
	cu32 col1=src1.col,col2=src2.col;
	cd * t1=src1.t+col1*y1+x1;
	cd * t2=src2.t+col2*y2+x2;
	for(u32 i=0;i<y;i++,t1+=col1,t2+=col2)
		for(u32 j=0;j<x;j++,n++)t[n]=t1[j]+t2[j];
}


//***********************************************************************
void drmatrix::submatrixsub(const drmatrix & src1,cu32 y1,cu32 x1,const drmatrix & src2,cu32 y2,cu32 x2,cu32 y,cu32 x){
// src1-b�l x1,y1-t�l, src2-b�l x2,y2-t�l ad �ssze egy x,y m�ret� blokkot
//***********************************************************************
#ifdef vsundebugmode
	if(x1+x>src1.col)throw hiba("","drmatrix::submatrixsub => x1+x>src1.col => %lu+%lu>%lu",x1,x,src1.col);
	if(y1+y>src1.row)throw hiba("","drmatrix::submatrixsub => y1+y>src1.row => %lu+%lu>%lu",y1,y,src1.row);
	if(x2+x>src2.col)throw hiba("","drmatrix::submatrixsub => x2+x>src2.col => %lu+%lu>%lu",x2,x,src2.col);
	if(y2+y>src2.row)throw hiba("","drmatrix::submatrixsub => y2+y>src2.row => %lu+%lu>%lu",y2,y,src2.row);
#endif
	resize(y,x);
	u32 n=0;
	cu32 col1=src1.col,col2=src2.col;
	cd * t1=src1.t+col1*y1+x1;
	cd * t2=src2.t+col2*y2+x2;
	for(u32 i=0;i<y;i++,t1+=col1,t2+=col2)
		for(u32 j=0;j<x;j++,n++)t[n]=t1[j]-t2[j];
}


//***********************************************************************
void drmatrix::getsubmatrix(const drmatrix & src,cu32 y0,cu32 x0,cu32 y,cu32 x){
// Mag�ba rakja src-b�l az x0,y0-n�l kezd�d� x,y m�ret� r�szm�trixot
//***********************************************************************
#ifdef vsundebugmode
	if(x0+x>src.col)throw hiba("","drmatrix::getsubmatrix => x0+x>src.col => %lu+%lu>%lu",x0,x,src.col);
	if(y0+y>src.row)throw hiba("","drmatrix::getsubmatrix => y0+y>src.row => %lu+%lu>%lu",y0,y,src.row);
#endif
	resize(y,x);
	u32 n=0;
	cu32 col0=src.col;
	cd * t0=src.t+col0*y0+x0;
	for(u32 i=0;i<y;i++,t0+=col0)for(u32 j=0;j<x;j++,n++)t[n]=t0[j];
}


//***********************************************************************
void drvector::getsubvector(const drvector & src,cu32 n0,cu32 n){
// Mag�ba rakja src-b�l az n0-n�l kezd�d� n m�ret� r�szvektort
//***********************************************************************
#ifdef vsundebugmode
	if(n0+n>src.siz)throw hiba("","drvector::getsubvector => n0+n>src.siz => %lu+%lu>%lu",n0,n,src.siz);
#endif
	resize(n);
	for(u32 i=0;i<n;i++)t[i]=src.t[n0+i];
}


//***********************************************************************
void drvector::setsubvector(const drvector & src,cu32 n0){
// Mag�ba rakja src-b�l az n0-n�l r�szvektort, felt�telezi, hogy src 1 blokk
//***********************************************************************
#ifdef vsundebugmode
	if(n0+src.siz>siz)throw hiba("","drvector::setsubvector => n0+src.siz>siz => %lu+%lu>%lu",n0,src.siz,siz);
#endif
	cu32 n=src.siz;
	for(u32 i=0;i<n;i++)t[n0+i]=src.t[i];
}


//***********************************************************************
void drvector::getsetsubvector(const drvector & src,cu32 from_n0,cu32 to_n0,cu32 n){
// Mag�ba rakja to_n0-t�l src-b�l az from_n0-n�l kezd�d� n m�ret� r�szvektort
//***********************************************************************
#ifdef vsundebugmode
	if(from_n0+n>src.siz)throw hiba("","drvector::getsetsubvector => from_n0+n>src.siz => %lu+%lu>%lu",from_n0,n,src.siz);
	if(to_n0+n>siz)throw hiba("","drvector::getsetsubvector => to_n0+n>siz => %lu+%lu>%lu",to_n0,n,siz);
#endif
	for(u32 i=0;i<n;i++)t[to_n0+i]=src.t[from_n0+i];
}


//***********************************************************************
void drvector::getsetaddsubvector(const drvector & src,cu32 from_n0,cu32 to_n0,cu32 n){
// Mag�hoz adja to_n0-t�l src-b�l az from_n0-n�l kezd�d� n m�ret� r�szvektort
//***********************************************************************
#ifdef vsundebugmode
	if(from_n0+n>src.siz)throw hiba("","drvector::getsetaddsubvector => from_n0+n>src.siz => %lu+%lu>%lu",from_n0,n,src.siz);
	if(to_n0+n>siz)throw hiba("","drvector::getsetaddsubvector => to_n0+n>siz => %lu+%lu>%lu",to_n0,n,siz);
#endif
	for(u32 i=0;i<n;i++)t[to_n0+i]+=src.t[from_n0+i];
}


//***********************************************************************
void drmatrix::setsubmatrix(const drmatrix & src,cu32 y0,cu32 x0){
// Mag�ba rakja src-b�l az x0,y0-n�l r�szm�trixot, felt�telezi, hogy src 1 blokk
//***********************************************************************
#ifdef vsundebugmode
	if(x0+src.col>col)throw hiba("","drmatrix::setsubmatrix => x0+src.col>col => %lu+%lu>%lu",x0,src.col,col);
	if(y0+src.row>row)
		throw hiba("","drmatrix::setsubmatrix => y0+src.row>row => %lu+%lu>%lu",y0,src.row,row);
#endif
	u32 n=0;
	dbl * t0=t+col*y0+x0;
	cu32 x=src.col,y=src.row;
	for(u32 i=0;i<y;i++,t0+=col)for(u32 j=0;j<x;j++,n++)t0[j]=src.t[n];
}


//***********************************************************************
void drmatrix::setsubmatrixadd(const drmatrix & src,cu32 y0,cu32 x0){
// Mag�ba rakja src-b�l az x0,y0-n�l r�szm�trixot, felt�telezi, hogy src 1 blokk
//***********************************************************************
#ifdef vsundebugmode
	if(x0+src.col>col)throw hiba("","drmatrix::setsubmatrixadd => x0+src.col>col => %lu+%lu>%lu",x0,src.col,col);
	if(y0+src.row>row)throw hiba("","drmatrix::setsubmatrixadd => y0+src.row>row => %lu+%lu>%lu",y0,src.row,row);
#endif
	u32 n=0;
	dbl * t0=t+col*y0+x0;
	cu32 x=src.col,y=src.row;
	for(u32 i=0;i<y;i++,t0+=col)for(u32 j=0;j<x;j++,n++)t0[j]+=src.t[n];
}


//***********************************************************************
void drmatrix::setsubmatrixneg(const drmatrix & src,cu32 y0,cu32 x0){
// Mag�ba rakja src-b�l az x0,y0-n�l r�szm�trixot neg�lva
//***********************************************************************
#ifdef vsundebugmode
	if(x0+src.col>col)throw hiba("","drmatrix::setsubmatrix => x0+src.col>col => %lu+%lu>%lu",x0,src.col,col);
	if(y0+src.row>row)throw hiba("","drmatrix::setsubmatrix => y0+src.row>row => %lu+%lu>%lu",y0,src.row,row);
#endif
	u32 n=0;
	dbl * t0=t+col*y0+x0;
	cu32 x=src.col,y=src.row;
	for(u32 i=0;i<y;i++,t0+=col)for(u32 j=0;j<x;j++,n++)t0[j]=-src.t[n];
}


//***********************************************************************
void drmatrix::setsubmatrixnegadd(const drmatrix & src,cu32 y0,cu32 x0){
// Mag�ba rakja src-b�l az x0,y0-n�l r�szm�trixot neg�lva
//***********************************************************************
#ifdef vsundebugmode
	if(x0+src.col>col)throw hiba("","drmatrix::setsubmatrixnegadd => x0+src.col>col => %lu+%lu>%lu",x0,src.col,col);
	if(y0+src.row>row)throw hiba("","drmatrix::setsubmatrixnegadd => y0+src.row>row => %lu+%lu>%lu",y0,src.row,row);
#endif
	u32 n=0;
	dbl * t0=t+col*y0+x0;
	cu32 x=src.col,y=src.row;
	for(u32 i=0;i<y;i++,t0+=col)for(u32 j=0;j<x;j++,n++)t0[j]-=src.t[n];
}


//***********************************************************************
void drmatrix::strasseninv(){
//***********************************************************************
#ifdef vsundebugmode
	if(col!=row)throw hiba("","drmatrix::strasseninv => col!=row => %lu!=%lu",col,row);
	if(col&1)throw hiba("","drmatrix::strasseninv => col=%lu => paratlan oldalhosz",col);
#endif
	parhuzamosanfut=true;
	drmatrix r1,r2,r3,r4,r6,c12,c21,m1,m2; // 7 db, ebb�l egyszerre max 6 van haszn�latban (1,5 m�trix m�ret)
	r1.getsubmatrix(*this,0,0,row/2,col/2);//a11
	r1.inv();
//printf("\nR1=");r1.print();	
	c12.getsubmatrix(*this,0,col/2,row/2,col/2);//a12
	m1.transp(c12);
	c12.free();
	drmtmulszal mr3(r1,m1,r3);
	mr3.start();

	c21.getsubmatrix(*this,row/2,0,row/2,col/2);//a21
	m2.transp(r1);
	drmtmulszal mr2(c21,m2,r2);
	mr2.start();

	mr3.wait();
//printf("\nR3=");r3.print();	

	m1.transp(r3);
	drmtmulszal mr4(c21,m1,r4);//r4
	mr4.start();

	mr2.wait();
	mr4.wait();
//printf("\nR2=");r2.print();	
//printf("\nR4=");r4.print();	

	c21.free();
	m1.free();
	m2.free();

	r6.getsubmatrix(*this,row/2,col/2,row/2,col/2);//a22
	r6.subnr(r4,r6);
//printf("\nR5=");r6.print();	

	r4.free();

	r6.inv();
//printf("\nR6=");r6.print();	

	m1.transp(r2);
	r2.free();
	drmtmulszal mc21(r6,m1,c21);
	mc21.start();

	m2.transp(r6);
	drmtmulszal mc12(r3,m2,c12);
	mc12.start();

	setsubmatrixneg(r6,row/2,col/2); // c22 be�r�sa

	mc21.wait();
//printf("\nc21=");c21.print();	

	m1.transp(c21);
	drmtmulszal mr7(r3,m1,r2);// r2 lesz r7
	mr7.start();

	setsubmatrix(c21,row/2,0); // c21 be�r�sa

	mc12.wait();
//printf("\nc12=");c12.print();	

	m2.free();
	setsubmatrix(c12,0,col/2); // c12 be�r�sa

	mr7.wait();
//printf("\nR7=");r2.print();	

	c12.sub(r1,r2); // r1-r7
//printf("\nc11=");c12.print();	
	setsubmatrix(c12,0,0); // c11 be�r�sa
}


//***********************************************************************
void drmatrix::strassentmul(const drmatrix & src1,const drmatrix & src2){
// val�j�ban += !!!!
//***********************************************************************
#ifdef vsundebugmode
	if(&src1==this)throw hiba("","drmatrix::strassentmul => &src1==this");
	if(&src2==this)throw hiba("","drmatrix::strassentmul => &src2==this");
	if(src1.col!=src2.col)throw hiba("","drmatrix::strassentmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if((src1.col&1)||(src1.row&1)||(src2.row&1))
		throw hiba("","drmatrix::strassentmul => src1.col=%lu, src1.row=%lu, src2.row=%lu => paratlan oldalhosz"
			  ,src1.col,src1.row,src2.row);
#endif
	parhuzamosanfut=true;
	switch(mxOptimize){
		case mxMaxSpeed :{ // 21 seg�dm�trix kell, ami 5,25 teljes eredm�nym�trix m�ret
				drmatrix a1122,b1122,a2122,b11,a11,b1222,a22,b1121,a1112,b22,a1121,b1112,a1222,b2122; // 14 db
				drmatrix q1,q2,q3,q4,q5,q6,q7; // 7 db

				a1122.submatrixadd(src1, 0, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				b1122.submatrixadd(src2, 0, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq1(a1122, b1122, q1);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a2122.submatrixadd(src1, src1.row/2, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				b11.getsubmatrix(src2, 0, 0, src2.row/2, src2.col/2);
				drmtmulszal mq2(a2122, b11, q2);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a11.getsubmatrix(src1, 0, 0, src1.row/2, src1.col/2);
				b1222.submatrixsub(src2, src2.row/2, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				drmtmulszal mq3(a11, b1222, q3);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a22.getsubmatrix(src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				b1121.submatrixsub(src2, 0, src2.col/2, src2, 0, 0, src2.row/2, src2.col/2);//csere
				drmtmulszal mq4(a22, b1121, q4);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a1112.submatrixadd(src1, 0, 0, src1, 0, src1.col/2, src1.row/2, src1.col/2);
				b22.getsubmatrix(src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq5(a1112, b22, q5);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a1121.submatrixsub(src1, src1.row/2, 0, src1, 0, 0, src1.row/2, src1.col/2);
				b1112.submatrixadd(src2, 0, 0, src2, src2.row/2, 0, src2.row/2, src2.col/2);//csere
				drmtmulszal mq6(a1121, b1112, q6);	//ezekben csak 0-�s blokk van, mert submatrixok!

				a1222.submatrixsub(src1, 0, src1.col/2, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				b2122.submatrixadd(src2, 0, src2.col/2, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				drmtmulszal mq7(a1222, b2122, q7);	//ezekben csak 0-�s blokk van, mert submatrixok!

				mq1.start(); mq2.start(); mq3.start(); mq4.start(); mq5.start(); mq6.start(); mq7.start();
				mq1.wait();  mq2.wait();  mq3.wait();  mq4.wait();  mq5.wait();  mq6.wait();  mq7.wait();

				a1122.free(); b1122.free(); a2122.free(); b1222.free(); b1121.free(); a22.free();
				a1112.free(); a1121.free(); b1112.free(); a1222.free(); b2122.free(); b22.free(); b11.free();

				a11.addaddsubadd(q1,q4,q5,q7);
				setsubmatrixadd(a11,0,0);
				a11.add(q3,q5);
				setsubmatrixadd(a11,0,col/2);
				a11.add(q2,q4);
				setsubmatrixadd(a11,row/2,0);
				a11.addaddsubadd(q1,q3,q2,q6);
				setsubmatrixadd(a11,row/2,col/2);
			}
			break;
		case mxBalanced1 :{ // 12 seg�dm�trix kell, ami 3 teljes eredm�nym�trix m�ret
				drmatrix m1,m2,m3,m4,m5,m6; // 6 db
				drmatrix q1,q2,q3,q4,q5,q6; // 6 db

				m1.submatrixadd(src1, 0, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.submatrixadd(src2, 0, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq1(m1, m2, q1); //ezekben csak 0-�s blokk van, mert submatrixok!

				m3.getsubmatrix(src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m4.submatrixsub(src2, 0, src2.col/2, src2, 0, 0, src2.row/2, src2.col/2);//csere
				drmtmulszal mq4(m3, m4, q2);   //ezekben csak 0-�s blokk van, mert submatrixok!

				m5.submatrixadd(src1, 0, 0, src1, 0, src1.col/2, src1.row/2, src1.col/2);
				m6.getsubmatrix(src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq5(m5, m6, q3);   //ezekben csak 0-�s blokk van, mert submatrixok!

				q5.submatrixsub(src1, 0, src1.col/2, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				q6.submatrixadd(src2, 0, src2.col/2, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);// csere
				drmtmulszal mq7(q5, q6, q4); //ezekben csak 0-�s blokk van, mert submatrixok!

				mq1.start(); mq4.start(); mq5.start(); mq7.start();
				mq1.wait();  mq4.wait();  mq5.wait();  mq7.wait();

				m1.addaddsubadd(q1,q2,q3,q4);
				setsubmatrixadd(m1,0,0);

				m1.submatrixadd(src1, src1.row/2, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.getsubmatrix(src2, 0, 0, src2.row/2, src2.col/2);
				drmtmulszal mq2(m1, m2, q5);	//ezekben csak 0-�s blokk van, mert submatrixok!

				m3.getsubmatrix(src1, 0, 0, src1.row/2, src1.col/2);
				m4.submatrixsub(src2, src2.row/2, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				drmtmulszal mq3(m3, m4, q6);	//ezekben csak 0-�s blokk van, mert submatrixok!

				m5.submatrixsub(src1, src1.row/2, 0, src1, 0, 0, src1.row/2, src1.col/2);
				m6.submatrixadd(src2, 0, 0, src2, src2.row/2, 0, src2.row/2, src2.col/2);//csere
				drmtmulszal mq6(m5, m6, q4);	//ezekben csak 0-�s blokk van, mert submatrixok!

				mq2.start(); mq3.start(); mq6.start();
				mq2.wait();  mq3.wait();  mq6.wait();

				m2.free(); m3.free(); m4.free(); m5.free(); m6.free();

				m1.add(q6,q3); // azaz Q3+Q5
				setsubmatrixadd(m1,0,col/2);
				m1.add(q5,q2); // azaz Q2+Q4
				setsubmatrixadd(m1,row/2,0);
				m1.addaddsubadd(q1,q6,q5,q4); // azaz Q1+Q3-Q2+Q6
				setsubmatrixadd(m1,row/2,col/2);
			}
			break;
		case mxBalanced2 :{ // 9 seg�dm�trix kell, ami 2,25 teljes eredm�nym�trix m�ret
				drmatrix m1,m2,m3,m4; // 4 db
				drmatrix q1,q2,q3,q4,q5; // 5 db
				
				m1.submatrixadd(src1, 0, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.submatrixadd(src2, 0, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq1(m1, m2, q1); //ezekben csak 0-�s blokk van, mert submatrixok!

				m3.getsubmatrix(src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m4.submatrixsub(src2, 0, src2.col/2, src2, 0, 0, src2.row/2, src2.col/2);//csere
				drmtmulszal mq4(m3, m4, q2);   //ezekben csak 0-�s blokk van, mert submatrixok!

				mq1.start(); mq4.start();
				mq1.wait();  mq4.wait();

				m1.submatrixadd(src1, 0, 0, src1, 0, src1.col/2, src1.row/2, src1.col/2);
				m2.getsubmatrix(src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq5(m1, m2, q3);   //ezekben csak 0-�s blokk van, mert submatrixok!

				m3.submatrixsub(src1, 0, src1.col/2, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m4.submatrixadd(src2, 0, src2.col/2, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				drmtmulszal mq7(m3, m4, q4); //ezekben csak 0-�s blokk van, mert submatrixok!

				mq5.start(); mq7.start();
				mq5.wait();  mq7.wait();

				m1.addaddsubadd(q1,q2,q3,q4);
				setsubmatrixadd(m1,0,0);

				m3.getsubmatrix(src1, 0, 0, src1.row/2, src1.col/2);
				m4.submatrixsub(src2, src2.row/2, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				drmtmulszal mq3(m3, m4, q4);	//ezekben csak 0-�s blokk van, mert submatrixok!

				m1.submatrixadd(src1, src1.row/2, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.getsubmatrix(src2, 0, 0, src2.row/2, src2.col/2);
				drmtmulszal mq2(m1, m2, q5);	//ezekben csak 0-�s blokk van, mert submatrixok!

				mq2.start(); mq3.start();
				mq2.wait();  mq3.wait();

				m3.free(); m4.free();

				m1.add(q4,q3); //Q3+Q5

				q3.free();
				
				setsubmatrixadd(m1,0,col/2);
				m1.add(q5,q2); // Q2+Q4
				setsubmatrixadd(m1,row/2,0);

				m1.submatrixsub(src1, src1.row/2, 0, src1, 0, 0, src1.row/2, src1.col/2);
				m2.submatrixadd(src2, 0, 0, src2, src2.row/2, 0, src2.row/2, src2.col/2);//csere
				q2.tmul(m1,m2);

				m1.addaddsubadd(q1,q4,q5,q2); // azaz Q1+Q3-Q2+Q6
				setsubmatrixadd(m1,row/2,col/2);
			}
			break;
		case mxBalanced3 :{ // 6 seg�dm�trix kell, ami 1,5 teljes eredm�nym�trix m�ret
				drmatrix m1,m2; // 2 db
				drmatrix q1,q2,q3,q4; // 4 db
				
				m1.submatrixadd(src1, 0, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.submatrixadd(src2, 0, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				drmtmulszal mq1(m1, m2, q1); //ezekben csak 0-�s blokk van, mert submatrixok!

				q3.getsubmatrix(src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				q4.submatrixsub(src2, 0, src2.col/2, src2, 0, 0, src2.row/2, src2.col/2);// csere
				drmtmulszal mq4(q3, q4, q2);   //ezekben csak 0-�s blokk van, mert submatrixok!

				mq1.start(); mq4.start();
				mq1.wait();  mq4.wait();

				m1.submatrixadd(src1, 0, 0, src1, 0, src1.col/2, src1.row/2, src1.col/2);
				m2.getsubmatrix(src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);
				q3.tmul(m1,m2);

				m1.submatrixsub(src1, 0, src1.col/2, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.submatrixadd(src2, 0, src2.col/2, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				q4.tmul(m1,m2);

				m1.addaddsubadd(q1,q2,q3,q4);
				setsubmatrixadd(m1,0,0);

				m1.getsubmatrix(src1, 0, 0, src1.row/2, src1.col/2);
				m2.submatrixsub(src2, src2.row/2, 0, src2, src2.row/2, src2.col/2, src2.row/2, src2.col/2);//csere
				q4.tmul(m1,m2);

				m1.add(q4,q3); //Q3+Q5
				setsubmatrixadd(m1,0,col/2);

				m1.submatrixadd(src1, src1.row/2, 0, src1, src1.row/2, src1.col/2, src1.row/2, src1.col/2);
				m2.getsubmatrix(src2, 0, 0, src2.row/2, src2.col/2);
				q3.tmul(m1,m2);

				m1.add(q3,q2); // Q2+Q4
				setsubmatrixadd(m1,row/2,0);

				m1.submatrixsub(src1, src1.row/2, 0, src1, 0, 0, src1.row/2, src1.col/2);
				m2.submatrixadd(src2, 0, 0, src2, src2.row/2, 0, src2.row/2, src2.col/2);//csere
				q2.tmul(m1,m2);

				m1.addaddsubadd(q1,q4,q3,q2); // azaz Q1+Q3-Q2+Q6
				setsubmatrixadd(m1,row/2,col/2);
			}
			break;
		default:throw hiba("","drmatrix::strassentmul => mxOptimize==mxMinSize");
	}
}


//***********************************************************************
void drmatrix::inv(){
//***********************************************************************
#ifdef vsundebugmode
		if(row!=col)throw hiba("","drmatrix::blokkninv => row!=col => %lu!=%lu",row,col);
#endif
	cu32 S=row,SS=row*col;
	dbl * m=t;
	switch(S){
		case 0: return;
		case 1: m[0]=1.0/m[0];return;
		case 2: {
					dbl * const p=m;
					cd det=1.0/(p[0]*p[3]-p[1]*p[2]),ndet=-det,m3=p[0]*det;
					p[0]=p[3]*det;p[1]=p[1]*ndet;p[2]=p[2]*ndet;p[3]=m3;
					return;
				}
	}

	if((mxOptimize!=mxMinSize) && ((col&1)==0) && (STRASSEN_INV_N_LIMIT()<=dbl(row)*row*row)){
		strasseninv();
		return;
	}

	u32 i,j,I,k;
	i32 dum[2*NEWKIVALTO],*x=S>NEWKIVALTO?new i32[2*S]:dum,*y=x+S;
	dbl * const N=m;
	dbl *O;
	
	for(i=0;i<S;i++)x[i]=y[i]=-1;
	for(i=0,I=0,O=N;i<S;i++,I+=S,O+=S){
		// Pivot kiv�laszt�sa
		
		dbl diff=0.0;
		i32 V=-1;
		u32 j,J;
		for(j=0;j<S;j++)if(x[j]==-1){
			dbl temp=dbl(fabs(O[j]));
			if(temp>diff){diff=temp;V=j;}//v-edik oszlopot v�lasztjuk
		}

		if((V==-1)||(diff==0))throw hiba("","drmatrix::inv => singular matrix");
		cu32 V2=u32(V);
		x[V2]=i;y[i]=V;

		// Elemcsere

		dbl A=1/O[V2];

		for(J=0;J<I;J+=S){
			cd C=-N[J+V2]*A;
			dbl *p1=N+J,*p2=O;
			for(u32 kl=S;kl!=0;kl--)*(p1++)+=C*(*(p2++));
			N[J+V2]=C;
		}
		for(J+=S;J<SS;J+=S){
			cd C=-N[J+V2]*A;
			dbl *p1=N+J,*p2=O;
			for(u32 kl=S;kl!=0;kl--)*(p1++)+=C*(*(p2++));
			N[J+V2]=C;
		}
		for(j=0;j<S;j++)O[j]*=A;
		O[V2]=A;
	}

#ifdef vsundebugmode
	for(i=0;i<S;i++)if(x[i]<0)throw hiba("","drmatrix::inv => -1 in x[]");
	for(i=0;i<S;i++)if(y[i]<0)throw hiba("","drmatrix::inv => -1 in y[]");
#endif

	// Sorok �s oszlopok sorrendbe rak�sa

	for(i=0;i<S;i++){
		for(j=i;y[j]!=i;j++);
		if(i!=j){
			dbl *cSi=N+i*S,*cSj=N+j*S;
			for(k=0;k<S;k++){dbl temp=cSi[k];cSi[k]=cSj[k];cSj[k]=temp;}
			y[j]=y[i];
		}
	}
	for(i=0;i<S;i++){
		for(j=i;x[j]!=i;j++);
		if(i!=j){
			dbl *cSi=N+i,*cSj=N+j;
			for(k=0;k<S;k++,cSi+=S,cSj+=S){dbl temp=*cSi;*cSi=*cSj;*cSj=temp;}
			x[j]=x[i];
		}
	}
	
	if(S>NEWKIVALTO)delete [] x;
}


//***********************************************************************
void drvector::tmul(const drmatrix & src1,const drvector & src2,const bool adde){
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","drvector::tmul => &src2==this");
	if(src1.col!=src2.siz)throw hiba("","drvector::tmul => src1.col!=src2.siz => %lu!=%lu",src1.col,src2.siz);
	if(adde){
		if(siz!=src1.row)throw hiba("","drvector::tmul => siz!=src1.row => %lu!=%lu",siz,src1.row);
	}
#endif
	if(!adde){resize(src1.row);zero();}
	cu32 n=src1.col;
	dbl * t0=t;
	cd * t1=src1.t;
	cd * t2=src2.t;
	for(u32 i=0;i<siz;i++,t1+=n){
		dbl sum=0;
		for(u32 k=0;k<n;k++)sum+=t1[k]*t2[k];
		t0[i]+=sum;
	}
}


//***********************************************************************
void drvector::tmult(const drmatrix & src1,const drvector & src2,const bool adde){
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","drvector::tmul => &src2==this");
	if(src1.row!=src2.siz)throw hiba("","drvector::tmul => src1.row!=src2.siz => %lu!=%lu",src1.row,src2.siz);
	if(adde){
		if(siz!=src1.col)throw hiba("","drvector::tmul => siz!=src1.col => %lu!=%lu",siz,src1.col);
	}
#endif
	if(!adde){resize(src1.col);zero();}
	cu32 n=src1.col,nn=src1.row;
	dbl * t0=t;
	cd * t1=src1.t;
	cd * t2=src2.t;
	for(u32 i=0;i<nn;i++){
		for(u32 j=0;j<n;j++)t0[j]+=t1[i*n+j]*t2[i];
	}
}


//***********************************************************************
void drvector::tmuladd(const drmatrix & src1,const drvector & src2){
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","drvector::tmul => &src2==this");
	if(src1.col!=src2.siz)throw hiba("","drvector::tmuladd => src1.col!=src2.siz => %lu!=%lu",src1.col,src2.siz);
	if(siz!=src2.siz)throw hiba("","drvector::tmuladd => siz!=src2.siz => %lu!=%lu",siz,src2.siz);
#endif
	cu32 n=src1.col;
	dbl * t0=t;
	cd * t1=src1.t;
	cd * t2=src2.t;
	for(u32 i=0;i<siz;i++,t1+=n){
		dbl sum=t1[0]*t2[0];
		for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
		t0[i]+=sum;
	}
}


//***********************************************************************
void dcvector::tmul(const dcmatrix & src1,const dcvector & src2,const bool adde){
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","dcvector::tmul => &src2==this");
	if(src1.getcol()!=src2.getsiz())
		throw hiba("","dcvector::tmul => src1.getcol()!=src2.getsiz() => %lu!=%lu",src1.getcol(),src2.getsiz());
	if(adde){
		if(getsiz()!=src1.getrow())throw hiba("","dcvector::tmul => getsiz()!=src1.getrow() => %lu!=%lu",getsiz(),src1.getrow());
	}
#endif
	if(!adde){resize(src1.getrow());zero();}
	cu32 n=src1.getcol(),siz=re.getsiz();
	dbl * re0=re.gett();
	dbl * im0=im.gett();
	cd * re1=src1.re.gett();
	cd * im1=src1.im.gett();
	cd * re2=src2.re.gett();
	cd * im2=src2.im.gett();
	for(u32 i=0;i<siz;i++,re1+=n,im1+=n){
		dbl sumre=0,sumim=0;
		for(u32 k=0;k<n;k++){sumre+=re1[k]*re2[k]-im1[k]*im2[k];sumim+=re1[k]*im2[k]+im1[k]*re2[k];}
		re0[i]+=sumre;
		im0[i]+=sumim;
	}
}


//***********************************************************************
void dcvector::tmult(const dcmatrix & src1,const dcvector & src2,const bool adde){
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","dcvector::tmult => &src2==this");
	if(src1.getrow()!=src2.getsiz())
		throw hiba("","dcvector::tmult => src1.getrow()!=src2.getsiz() => %lu!=%lu",src1.getrow(),src2.getsiz());
	if(adde){
		if(getsiz()!=src1.getcol())throw hiba("","dcvector::tmult => getsiz()!=src1.getcol() => %lu!=%lu",getsiz(),src1.getcol());
	}
#endif
	if(!adde){resize(src1.getcol());zero();}
	cu32 n=src1.getcol(),siz=re.getsiz(),nn=src1.getrow();
	dbl * re0=re.gett();
	dbl * im0=im.gett();
	cd * re1=src1.re.gett();
	cd * im1=src1.im.gett();
	cd * re2=src2.re.gett();
	cd * im2=src2.im.gett();
	for(u32 i=0;i<nn;i++){
		for(u32 j=0;j<n;j++){
			re0[j]+=re1[i*n+j]*re2[i]-im1[i*n+j]*im2[i];
			im0[j]+=re1[i*n+j]*im2[i]+im1[i*n+j]*re2[i];
		}
	}
}


//***********************************************************************
void dcvector::tmuladd(const dcmatrix & src1,const dcvector & src2){
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","dcvector::tmul => &src2==this");
	if(src1.getcol()!=src2.getsiz())
		throw hiba("","dcvector::tmul => src1.getcol()!=src2.getsiz() => %lu!=%lu",src1.getcol(),src2.getsiz());
	if(getsiz()!=src2.getsiz())throw hiba("","dcvector::tmuladd => getsiz()!=src2.getsiz() => %lu!=%lu",getsiz(),src2.getsiz());
#endif
	cu32 n=src1.getcol(),siz=re.getsiz();
	dbl * re0=re.gett();
	dbl * im0=im.gett();
	cd * re1=src1.re.gett();
	cd * im1=src1.im.gett();
	cd * re2=src2.re.gett();
	cd * im2=src2.im.gett();
	for(u32 i=0;i<siz;i++,re1+=n,im1+=n){
		dbl sumre=re1[0]*re2[0]-im1[0]*im2[0],sumim=re1[0]*im2[0]+im1[0]*re2[0];
		for(u32 k=1;k<n;k++){sumre+=re1[k]*re2[k]-im1[k]*im2[k];sumim+=re1[k]*im2[k]+im1[k]*re2[k];}
		re0[i]+=sumre;
		im0[i]+=sumim;
	}
}


//***********************************************************************
void dcvector::tmul(const dscmatrix & src1,const dcvector & src2,const bool adde){
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","dcvector::tmul => &src2==this");
	if(src1.getcol()!=src2.getsiz())
		throw hiba("","dcvector::tmul => src1.getcol()!=src2.getsiz() => %lu!=%lu",src1.getcol(),src2.getsiz());
	if(adde){
		if(getsiz()!=src1.getrow())throw hiba("","dcvector::tmul => getsiz()!=src1.getrow() => %lu!=%lu",getsiz(),src1.getrow());
	}
#endif
	if(!adde){resize(src1.getrow());zero();}
	cu32 n=src1.getcol(),siz=re.getsiz();
	dbl * re0=re.gett();
	dbl * im0=im.gett();
	dbl dum[2*NEWKIVALTO],*re1=n>NEWKIVALTO?new dbl[2*n]:dum,*im1=re1+n;
	cd * re2=src2.re.gett();
	cd * im2=src2.im.gett();
	for(u32 i=0;i<siz;i++){
		src1.re.getLine(re1,i);
		src1.im.getLine(im1,i);
		dbl sumre=re1[0]*re2[0]-im1[0]*im2[0],sumim=re1[0]*im2[0]+im1[0]*re2[0];
		for(u32 k=1;k<n;k++){sumre+=re1[k]*re2[k]-im1[k]*im2[k];sumim+=re1[k]*im2[k]+im1[k]*re2[k];}
		re0[i]+=sumre;
		im0[i]+=sumim;
	}
	if(n>NEWKIVALTO)delete [] re1;
}


//***********************************************************************
void dcvector::tmuladd(const dscmatrix & src1,const dcvector & src2){
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","dcvector::tmul => &src2==this");
	if(src1.getcol()!=src2.getsiz())
		throw hiba("","dcvector::tmul => src1.getcol()!=src2.getsiz() => %lu!=%lu",src1.getcol(),src2.getsiz());
	if(getsiz()!=src2.getsiz())throw hiba("","dcvector::tmuladd => getsiz()!=src2.getsiz() => %lu!=%lu",getsiz(),src2.getsiz());
#endif
	cu32 n=src1.getcol(),siz=re.getsiz();
	dbl * re0=re.gett();
	dbl * im0=im.gett();
	dbl dum[2*NEWKIVALTO],*re1=n>NEWKIVALTO?new dbl[2*n]:dum,*im1=re1+n;
	cd * re2=src2.re.gett();
	cd * im2=src2.im.gett();
	for(u32 i=0;i<siz;i++){
		src1.re.getLine(re1,i);
		src1.im.getLine(im1,i);
		dbl sumre=re1[0]*re2[0]-im1[0]*im2[0],sumim=re1[0]*im2[0]+im1[0]*re2[0];
		for(u32 k=1;k<n;k++){sumre+=re1[k]*re2[k]-im1[k]*im2[k];sumim+=re1[k]*im2[k]+im1[k]*re2[k];}
		re0[i]+=sumre;
		im0[i]+=sumim;
	}
	if(n>NEWKIVALTO)delete [] re1;
}

//***********************************************************************
void drvector::tmul(const dsrmatrix & src1,const drvector & src2,const bool adde){
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","drvector::tmul => &src2==this");
	if(src1.col!=src2.siz)throw hiba("","drvector::tmul => src1.col!=src2.siz => %lu!=%lu",src1.col,src2.siz);
	if(adde){
		if(siz!=src1.row)throw hiba("","drvector::tmul => row!=src1.row => %lu!=%lu",row,src1.row);
	}
#endif
	if(!adde){resize(src1.row);zero();}
	cu32 n=src1.col;
	dbl * t0=t;
	dbl dum[NEWKIVALTO],*t1=n>NEWKIVALTO?new dbl[n]:dum;
	cd * t2=src2.t;
	for(u32 i=0;i<siz;i++){
		src1.getLine(t1,i);
		dbl sum=t1[0]*t2[0];
		for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
		t0[i]+=sum;
	}
	if(n>NEWKIVALTO)delete [] t1;
}


//***********************************************************************
void drvector::tmuladd(const dsrmatrix & src1,const drvector & src2){
//***********************************************************************
#ifdef vsundebugmode
	if(&src2==this)throw hiba("","drvector::tmul => &src2==this");
	if(src1.col!=src2.siz)throw hiba("","drvector::tmul => src1.col!=src2.siz => %lu!=%lu",src1.col,src2.siz);
	if(siz!=src2.siz)throw hiba("","drvector::tmuladd => siz!=src2.siz => %lu!=%lu",siz,src2.siz);
#endif
	cu32 n=src1.col;
	dbl * t0=t;
	dbl dum[NEWKIVALTO],*t1=n>NEWKIVALTO?new dbl[n]:dum;
	cd * t2=src2.t;
	for(u32 i=0;i<siz;i++){
		src1.getLine(t1,i);
		dbl sum=t1[0]*t2[0];
		for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
		t0[i]+=sum;
	}
	if(n>NEWKIVALTO)delete [] t1;
}


//***********************************************************************
void drmatrix::tmul_8(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop){
// src2 egy transzpon�lt m�trix
//***********************************************************************
#ifdef vsundebugmode
	if(&src1==this)throw hiba("","drmatrix::tmul => &src1==this");
	if(&src2==this)throw hiba("","drmatrix::tmul => &src2==this");
	if(src1.col!=src2.col)throw hiba("","drmatrix::tmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if(adde){
		if(row!=src1.row)throw hiba("","drmatrix::tmul => row!=src1.row => %lu!=%lu",row,src1.row);
		if(col!=src2.row)throw hiba("","drmatrix::tmul => col!=src2.row => %lu!=%lu",col,src2.row);
	}
#endif
#ifdef PL_PROFILE
	GyujtStart();
#endif

    cd szorszam=dbl(src2.row)*src1.row*src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	// mxOptimize-t �s p�ratlans�got figyelni a m�reten k�v�l!

	if(!adde && i_stop==0){resize(src1.row,src2.row);zero();} // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if((mxOptimize!=mxMinSize) && (((src1.col|src1.row|src2.row)&1)==0) && 
		(STRASSEN_MUL_N_LIMIT()<=szorszam)){
		strassentmul(src1,src2);
		return;
	}
    if(i_stop==0 && szorszam>MUL_N_LIMIT() && row>16){ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        uns szalszam=(uns)(szorszam*2/MUL_N_LIMIT());
        szalszam = ( szalszam > row/8 ) ? row/8 : szalszam;
        szalszam = ( szalszam > VSUN_CPU_Thread ) ? VSUN_CPU_Thread : szalszam;
        if(szalszam>1){
            //printf("\n%u %u\n",row,szalszam);
            drmtmulszal ** szaltomb=new drmtmulszal*[szalszam];
            cu32 darab=(row/szalszam/4)*4;
            if(darab<2)throw hiba("drmatrix::tmul","darab<2");
            for(uns i=0; i<szalszam-1; i++){
                szaltomb[i]=new drmtmulszal(src1,src2,*this,i*darab,(i+1)*darab);
            }
			parhuzamosanfut=true;
            szaltomb[szalszam-1]=new drmtmulszal(src1,src2,*this,(szalszam-1)*darab,row);
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->start();
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->wait();
            for(uns i=0; i<szalszam; i++)
                delete szaltomb[i];
            delete [] szaltomb;
            return;
        }
    }
    if(i_stop==0)i_stop=row; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 n=src1.col;
if(mxMulMode==mxMinSize){
	dbl * t0=t+i_start*col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd * t1=src1.t+i_start*n;//n==src1.col,row==src1.row // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<i_stop;i++,t1+=n){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd * t2=src2.t;//n==src1.col,col==src1.row
		for(u32 j=0;j<col;j++,t0++,t2+=n){
			dbl sum=t1[0]*t2[0];
			for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
			*t0+=sum;
		}
	}
}
else {
	cu32 ni=i_stop,nj=col,nk=src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 di=i_stop%8,dj=col%8,dk=src1.col%8; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 hi=ni-di,hj=nj-dj,hk=nk-dk;
	dbl *d0=t+i_start*col,     *d1=t+i_start*col+nj,  *d2=t+i_start*col+2*nj,*d3=t+i_start*col+3*nj; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	dbl *d4=t+i_start*col+4*nj,*d5=t+i_start*col+5*nj,*d6=t+i_start*col+6*nj,*d7=t+i_start*col+7*nj; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd *a0=src1.t+i_start*n,     *a1=src1.t+i_start*n+nk,  *a2=src1.t+i_start*n+2*nk,*a3=src1.t+i_start*n+3*nk; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd *a4=src1.t+i_start*n+4*nk,*a5=src1.t+i_start*n+5*nk,*a6=src1.t+i_start*n+6*nk,*a7=src1.t+i_start*n+7*nk; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<hi;i+=8){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd *b0=src2.t,*b1=src2.t+nk,*b2=src2.t+2*nk,*b3=src2.t+3*nk,*b4=src2.t+4*nk,*b5=src2.t+5*nk,*b6=src2.t+6*nk,*b7=src2.t+7*nk;
		for(u32 j=0;j<hj;j+=8){
			for(u32 k=0;k<hk;k+=8,a0+=8,a1+=8,a2+=8,a3+=8,a4+=8,a5+=8,a6+=8,a7+=8,b0+=8,b1+=8,b2+=8,b3+=8,b4+=8,b5+=8,b6+=8,b7+=8){
				d0[0]+=a0[0]*b0[0]+a0[1]*b0[1]+a0[2]*b0[2]+a0[3]*b0[3]+a0[4]*b0[4]+a0[5]*b0[5]+a0[6]*b0[6]+a0[7]*b0[7];
				d0[1]+=a0[0]*b1[0]+a0[1]*b1[1]+a0[2]*b1[2]+a0[3]*b1[3]+a0[4]*b1[4]+a0[5]*b1[5]+a0[6]*b1[6]+a0[7]*b1[7];
				d0[2]+=a0[0]*b2[0]+a0[1]*b2[1]+a0[2]*b2[2]+a0[3]*b2[3]+a0[4]*b2[4]+a0[5]*b2[5]+a0[6]*b2[6]+a0[7]*b2[7];
				d0[3]+=a0[0]*b3[0]+a0[1]*b3[1]+a0[2]*b3[2]+a0[3]*b3[3]+a0[4]*b3[4]+a0[5]*b3[5]+a0[6]*b3[6]+a0[7]*b3[7];
				d0[4]+=a0[0]*b4[0]+a0[1]*b4[1]+a0[2]*b4[2]+a0[3]*b4[3]+a0[4]*b4[4]+a0[5]*b4[5]+a0[6]*b4[6]+a0[7]*b4[7];
				d0[5]+=a0[0]*b5[0]+a0[1]*b5[1]+a0[2]*b5[2]+a0[3]*b5[3]+a0[4]*b5[4]+a0[5]*b5[5]+a0[6]*b5[6]+a0[7]*b5[7];
				d0[6]+=a0[0]*b6[0]+a0[1]*b6[1]+a0[2]*b6[2]+a0[3]*b6[3]+a0[4]*b6[4]+a0[5]*b6[5]+a0[6]*b6[6]+a0[7]*b6[7];
				d0[7]+=a0[0]*b7[0]+a0[1]*b7[1]+a0[2]*b7[2]+a0[3]*b7[3]+a0[4]*b7[4]+a0[5]*b7[5]+a0[6]*b7[6]+a0[7]*b7[7];

				d1[0]+=a1[0]*b0[0]+a1[1]*b0[1]+a1[2]*b0[2]+a1[3]*b0[3]+a1[4]*b0[4]+a1[5]*b0[5]+a1[6]*b0[6]+a1[7]*b0[7];
				d1[1]+=a1[0]*b1[0]+a1[1]*b1[1]+a1[2]*b1[2]+a1[3]*b1[3]+a1[4]*b1[4]+a1[5]*b1[5]+a1[6]*b1[6]+a1[7]*b1[7];
				d1[2]+=a1[0]*b2[0]+a1[1]*b2[1]+a1[2]*b2[2]+a1[3]*b2[3]+a1[4]*b2[4]+a1[5]*b2[5]+a1[6]*b2[6]+a1[7]*b2[7];
				d1[3]+=a1[0]*b3[0]+a1[1]*b3[1]+a1[2]*b3[2]+a1[3]*b3[3]+a1[4]*b3[4]+a1[5]*b3[5]+a1[6]*b3[6]+a1[7]*b3[7];
				d1[4]+=a1[0]*b4[0]+a1[1]*b4[1]+a1[2]*b4[2]+a1[3]*b4[3]+a1[4]*b4[4]+a1[5]*b4[5]+a1[6]*b4[6]+a1[7]*b4[7];
				d1[5]+=a1[0]*b5[0]+a1[1]*b5[1]+a1[2]*b5[2]+a1[3]*b5[3]+a1[4]*b5[4]+a1[5]*b5[5]+a1[6]*b5[6]+a1[7]*b5[7];
				d1[6]+=a1[0]*b6[0]+a1[1]*b6[1]+a1[2]*b6[2]+a1[3]*b6[3]+a1[4]*b6[4]+a1[5]*b6[5]+a1[6]*b6[6]+a1[7]*b6[7];
				d1[7]+=a1[0]*b7[0]+a1[1]*b7[1]+a1[2]*b7[2]+a1[3]*b7[3]+a1[4]*b7[4]+a1[5]*b7[5]+a1[6]*b7[6]+a1[7]*b7[7];

				d2[0]+=a2[0]*b0[0]+a2[1]*b0[1]+a2[2]*b0[2]+a2[3]*b0[3]+a2[4]*b0[4]+a2[5]*b0[5]+a2[6]*b0[6]+a2[7]*b0[7];
				d2[1]+=a2[0]*b1[0]+a2[1]*b1[1]+a2[2]*b1[2]+a2[3]*b1[3]+a2[4]*b1[4]+a2[5]*b1[5]+a2[6]*b1[6]+a2[7]*b1[7];
				d2[2]+=a2[0]*b2[0]+a2[1]*b2[1]+a2[2]*b2[2]+a2[3]*b2[3]+a2[4]*b2[4]+a2[5]*b2[5]+a2[6]*b2[6]+a2[7]*b2[7];
				d2[3]+=a2[0]*b3[0]+a2[1]*b3[1]+a2[2]*b3[2]+a2[3]*b3[3]+a2[4]*b3[4]+a2[5]*b3[5]+a2[6]*b3[6]+a2[7]*b3[7];
				d2[4]+=a2[0]*b4[0]+a2[1]*b4[1]+a2[2]*b4[2]+a2[3]*b4[3]+a2[4]*b4[4]+a2[5]*b4[5]+a2[6]*b4[6]+a2[7]*b4[7];
				d2[5]+=a2[0]*b5[0]+a2[1]*b5[1]+a2[2]*b5[2]+a2[3]*b5[3]+a2[4]*b5[4]+a2[5]*b5[5]+a2[6]*b5[6]+a2[7]*b5[7];
				d2[6]+=a2[0]*b6[0]+a2[1]*b6[1]+a2[2]*b6[2]+a2[3]*b6[3]+a2[4]*b6[4]+a2[5]*b6[5]+a2[6]*b6[6]+a2[7]*b6[7];
				d2[7]+=a2[0]*b7[0]+a2[1]*b7[1]+a2[2]*b7[2]+a2[3]*b7[3]+a2[4]*b7[4]+a2[5]*b7[5]+a2[6]*b7[6]+a2[7]*b7[7];

				d3[0]+=a3[0]*b0[0]+a3[1]*b0[1]+a3[2]*b0[2]+a3[3]*b0[3]+a3[4]*b0[4]+a3[5]*b0[5]+a3[6]*b0[6]+a3[7]*b0[7];
				d3[1]+=a3[0]*b1[0]+a3[1]*b1[1]+a3[2]*b1[2]+a3[3]*b1[3]+a3[4]*b1[4]+a3[5]*b1[5]+a3[6]*b1[6]+a3[7]*b1[7];
				d3[2]+=a3[0]*b2[0]+a3[1]*b2[1]+a3[2]*b2[2]+a3[3]*b2[3]+a3[4]*b2[4]+a3[5]*b2[5]+a3[6]*b2[6]+a3[7]*b2[7];
				d3[3]+=a3[0]*b3[0]+a3[1]*b3[1]+a3[2]*b3[2]+a3[3]*b3[3]+a3[4]*b3[4]+a3[5]*b3[5]+a3[6]*b3[6]+a3[7]*b3[7];
				d3[4]+=a3[0]*b4[0]+a3[1]*b4[1]+a3[2]*b4[2]+a3[3]*b4[3]+a3[4]*b4[4]+a3[5]*b4[5]+a3[6]*b4[6]+a3[7]*b4[7];
				d3[5]+=a3[0]*b5[0]+a3[1]*b5[1]+a3[2]*b5[2]+a3[3]*b5[3]+a3[4]*b5[4]+a3[5]*b5[5]+a3[6]*b5[6]+a3[7]*b5[7];
				d3[6]+=a3[0]*b6[0]+a3[1]*b6[1]+a3[2]*b6[2]+a3[3]*b6[3]+a3[4]*b6[4]+a3[5]*b6[5]+a3[6]*b6[6]+a3[7]*b6[7];
				d3[7]+=a3[0]*b7[0]+a3[1]*b7[1]+a3[2]*b7[2]+a3[3]*b7[3]+a3[4]*b7[4]+a3[5]*b7[5]+a3[6]*b7[6]+a3[7]*b7[7];

				d4[0]+=a4[0]*b0[0]+a4[1]*b0[1]+a4[2]*b0[2]+a4[3]*b0[3]+a4[4]*b0[4]+a4[5]*b0[5]+a4[6]*b0[6]+a4[7]*b0[7];
				d4[1]+=a4[0]*b1[0]+a4[1]*b1[1]+a4[2]*b1[2]+a4[3]*b1[3]+a4[4]*b1[4]+a4[5]*b1[5]+a4[6]*b1[6]+a4[7]*b1[7];
				d4[2]+=a4[0]*b2[0]+a4[1]*b2[1]+a4[2]*b2[2]+a4[3]*b2[3]+a4[4]*b2[4]+a4[5]*b2[5]+a4[6]*b2[6]+a4[7]*b2[7];
				d4[3]+=a4[0]*b3[0]+a4[1]*b3[1]+a4[2]*b3[2]+a4[3]*b3[3]+a4[4]*b3[4]+a4[5]*b3[5]+a4[6]*b3[6]+a4[7]*b3[7];
				d4[4]+=a4[0]*b4[0]+a4[1]*b4[1]+a4[2]*b4[2]+a4[3]*b4[3]+a4[4]*b4[4]+a4[5]*b4[5]+a4[6]*b4[6]+a4[7]*b4[7];
				d4[5]+=a4[0]*b5[0]+a4[1]*b5[1]+a4[2]*b5[2]+a4[3]*b5[3]+a4[4]*b5[4]+a4[5]*b5[5]+a4[6]*b5[6]+a4[7]*b5[7];
				d4[6]+=a4[0]*b6[0]+a4[1]*b6[1]+a4[2]*b6[2]+a4[3]*b6[3]+a4[4]*b6[4]+a4[5]*b6[5]+a4[6]*b6[6]+a4[7]*b6[7];
				d4[7]+=a4[0]*b7[0]+a4[1]*b7[1]+a4[2]*b7[2]+a4[3]*b7[3]+a4[4]*b7[4]+a4[5]*b7[5]+a4[6]*b7[6]+a4[7]*b7[7];

				d5[0]+=a5[0]*b0[0]+a5[1]*b0[1]+a5[2]*b0[2]+a5[3]*b0[3]+a5[4]*b0[4]+a5[5]*b0[5]+a5[6]*b0[6]+a5[7]*b0[7];
				d5[1]+=a5[0]*b1[0]+a5[1]*b1[1]+a5[2]*b1[2]+a5[3]*b1[3]+a5[4]*b1[4]+a5[5]*b1[5]+a5[6]*b1[6]+a5[7]*b1[7];
				d5[2]+=a5[0]*b2[0]+a5[1]*b2[1]+a5[2]*b2[2]+a5[3]*b2[3]+a5[4]*b2[4]+a5[5]*b2[5]+a5[6]*b2[6]+a5[7]*b2[7];
				d5[3]+=a5[0]*b3[0]+a5[1]*b3[1]+a5[2]*b3[2]+a5[3]*b3[3]+a5[4]*b3[4]+a5[5]*b3[5]+a5[6]*b3[6]+a5[7]*b3[7];
				d5[4]+=a5[0]*b4[0]+a5[1]*b4[1]+a5[2]*b4[2]+a5[3]*b4[3]+a5[4]*b4[4]+a5[5]*b4[5]+a5[6]*b4[6]+a5[7]*b4[7];
				d5[5]+=a5[0]*b5[0]+a5[1]*b5[1]+a5[2]*b5[2]+a5[3]*b5[3]+a5[4]*b5[4]+a5[5]*b5[5]+a5[6]*b5[6]+a5[7]*b5[7];
				d5[6]+=a5[0]*b6[0]+a5[1]*b6[1]+a5[2]*b6[2]+a5[3]*b6[3]+a5[4]*b6[4]+a5[5]*b6[5]+a5[6]*b6[6]+a5[7]*b6[7];
				d5[7]+=a5[0]*b7[0]+a5[1]*b7[1]+a5[2]*b7[2]+a5[3]*b7[3]+a5[4]*b7[4]+a5[5]*b7[5]+a5[6]*b7[6]+a5[7]*b7[7];

				d6[0]+=a6[0]*b0[0]+a6[1]*b0[1]+a6[2]*b0[2]+a6[3]*b0[3]+a6[4]*b0[4]+a6[5]*b0[5]+a6[6]*b0[6]+a6[7]*b0[7];
				d6[1]+=a6[0]*b1[0]+a6[1]*b1[1]+a6[2]*b1[2]+a6[3]*b1[3]+a6[4]*b1[4]+a6[5]*b1[5]+a6[6]*b1[6]+a6[7]*b1[7];
				d6[2]+=a6[0]*b2[0]+a6[1]*b2[1]+a6[2]*b2[2]+a6[3]*b2[3]+a6[4]*b2[4]+a6[5]*b2[5]+a6[6]*b2[6]+a6[7]*b2[7];
				d6[3]+=a6[0]*b3[0]+a6[1]*b3[1]+a6[2]*b3[2]+a6[3]*b3[3]+a6[4]*b3[4]+a6[5]*b3[5]+a6[6]*b3[6]+a6[7]*b3[7];
				d6[4]+=a6[0]*b4[0]+a6[1]*b4[1]+a6[2]*b4[2]+a6[3]*b4[3]+a6[4]*b4[4]+a6[5]*b4[5]+a6[6]*b4[6]+a6[7]*b4[7];
				d6[5]+=a6[0]*b5[0]+a6[1]*b5[1]+a6[2]*b5[2]+a6[3]*b5[3]+a6[4]*b5[4]+a6[5]*b5[5]+a6[6]*b5[6]+a6[7]*b5[7];
				d6[6]+=a6[0]*b6[0]+a6[1]*b6[1]+a6[2]*b6[2]+a6[3]*b6[3]+a6[4]*b6[4]+a6[5]*b6[5]+a6[6]*b6[6]+a6[7]*b6[7];
				d6[7]+=a6[0]*b7[0]+a6[1]*b7[1]+a6[2]*b7[2]+a6[3]*b7[3]+a6[4]*b7[4]+a6[5]*b7[5]+a6[6]*b7[6]+a6[7]*b7[7];

				d7[0]+=a7[0]*b0[0]+a7[1]*b0[1]+a7[2]*b0[2]+a7[3]*b0[3]+a7[4]*b0[4]+a7[5]*b0[5]+a7[6]*b0[6]+a7[7]*b0[7];
				d7[1]+=a7[0]*b1[0]+a7[1]*b1[1]+a7[2]*b1[2]+a7[3]*b1[3]+a7[4]*b1[4]+a7[5]*b1[5]+a7[6]*b1[6]+a7[7]*b1[7];
				d7[2]+=a7[0]*b2[0]+a7[1]*b2[1]+a7[2]*b2[2]+a7[3]*b2[3]+a7[4]*b2[4]+a7[5]*b2[5]+a7[6]*b2[6]+a7[7]*b2[7];
				d7[3]+=a7[0]*b3[0]+a7[1]*b3[1]+a7[2]*b3[2]+a7[3]*b3[3]+a7[4]*b3[4]+a7[5]*b3[5]+a7[6]*b3[6]+a7[7]*b3[7];
				d7[4]+=a7[0]*b4[0]+a7[1]*b4[1]+a7[2]*b4[2]+a7[3]*b4[3]+a7[4]*b4[4]+a7[5]*b4[5]+a7[6]*b4[6]+a7[7]*b4[7];
				d7[5]+=a7[0]*b5[0]+a7[1]*b5[1]+a7[2]*b5[2]+a7[3]*b5[3]+a7[4]*b5[4]+a7[5]*b5[5]+a7[6]*b5[6]+a7[7]*b5[7];
				d7[6]+=a7[0]*b6[0]+a7[1]*b6[1]+a7[2]*b6[2]+a7[3]*b6[3]+a7[4]*b6[4]+a7[5]*b6[5]+a7[6]*b6[6]+a7[7]*b6[7];
				d7[7]+=a7[0]*b7[0]+a7[1]*b7[1]+a7[2]*b7[2]+a7[3]*b7[3]+a7[4]*b7[4]+a7[5]*b7[5]+a7[6]*b7[6]+a7[7]*b7[7];
			}
			for(u32 k=0;k<dk;k++){
				d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];	d0[2]+=a0[k]*b2[k];	d0[3]+=a0[k]*b3[k]; d0[4]+=a0[k]*b4[k];	d0[5]+=a0[k]*b5[k];	d0[6]+=a0[k]*b6[k];	d0[7]+=a0[k]*b7[k]; 
				d1[0]+=a1[k]*b0[k];	d1[1]+=a1[k]*b1[k];	d1[2]+=a1[k]*b2[k];	d1[3]+=a1[k]*b3[k]; d1[4]+=a1[k]*b4[k];	d1[5]+=a1[k]*b5[k];	d1[6]+=a1[k]*b6[k];	d1[7]+=a1[k]*b7[k]; 
				d2[0]+=a2[k]*b0[k];	d2[1]+=a2[k]*b1[k];	d2[2]+=a2[k]*b2[k];	d2[3]+=a2[k]*b3[k]; d2[4]+=a2[k]*b4[k];	d2[5]+=a2[k]*b5[k];	d2[6]+=a2[k]*b6[k];	d2[7]+=a2[k]*b7[k]; 
				d3[0]+=a3[k]*b0[k];	d3[1]+=a3[k]*b1[k];	d3[2]+=a3[k]*b2[k];	d3[3]+=a3[k]*b3[k]; d3[4]+=a3[k]*b4[k];	d3[5]+=a3[k]*b5[k];	d3[6]+=a3[k]*b6[k];	d3[7]+=a3[k]*b7[k]; 
				d4[0]+=a4[k]*b0[k];	d4[1]+=a4[k]*b1[k];	d4[2]+=a4[k]*b2[k];	d4[3]+=a4[k]*b3[k]; d4[4]+=a4[k]*b4[k];	d4[5]+=a4[k]*b5[k];	d4[6]+=a4[k]*b6[k];	d4[7]+=a4[k]*b7[k]; 
				d5[0]+=a5[k]*b0[k];	d5[1]+=a5[k]*b1[k];	d5[2]+=a5[k]*b2[k];	d5[3]+=a5[k]*b3[k]; d5[4]+=a5[k]*b4[k];	d5[5]+=a5[k]*b5[k];	d5[6]+=a5[k]*b6[k];	d5[7]+=a5[k]*b7[k]; 
				d6[0]+=a6[k]*b0[k];	d6[1]+=a6[k]*b1[k];	d6[2]+=a6[k]*b2[k];	d6[3]+=a6[k]*b3[k]; d6[4]+=a6[k]*b4[k];	d6[5]+=a6[k]*b5[k];	d6[6]+=a6[k]*b6[k];	d6[7]+=a6[k]*b7[k]; 
				d7[0]+=a7[k]*b0[k];	d7[1]+=a7[k]*b1[k];	d7[2]+=a7[k]*b2[k];	d7[3]+=a7[k]*b3[k]; d7[4]+=a7[k]*b4[k];	d7[5]+=a7[k]*b5[k];	d7[6]+=a7[k]*b6[k];	d7[7]+=a7[k]*b7[k]; 
			}
			d0+=8,d1+=8,d2+=8,d3+=8,d4+=8,d5+=8,d6+=8,d7+=8;
			a0-=hk,a1-=hk,a2-=hk,a3-=hk,a4-=hk,a5-=hk,a6-=hk,a7-=hk;
			b0+=dk+7*nk,b1+=dk+7*nk,b2+=dk+7*nk,b3+=dk+7*nk,b4+=dk+7*nk,b5+=dk+7*nk,b6+=dk+7*nk,b7+=dk+7*nk;
		}
		for(u32 j=0;j<dj;j++){
			for(u32 k=0;k<nk;k++){
				d0[j]+=a0[k]*b0[j*nk+k]; d1[j]+=a1[k]*b0[j*nk+k]; d2[j]+=a2[k]*b0[j*nk+k]; d3[j]+=a3[k]*b0[j*nk+k];
				d4[j]+=a4[k]*b0[j*nk+k]; d5[j]+=a5[k]*b0[j*nk+k]; d6[j]+=a6[k]*b0[j*nk+k]; d7[j]+=a7[k]*b0[j*nk+k];
			}
		}
		d0+=dj+7*nj; d1+=dj+7*nj; d2+=dj+7*nj; d3+=dj+7*nj; d4+=dj+7*nj; d5+=dj+7*nj; d6+=dj+7*nj; d7+=dj+7*nj; 
		a0+=8*nk; a1+=8*nk; a2+=8*nk; a3+=8*nk; a4+=8*nk; a5+=8*nk; a6+=8*nk; a7+=8*nk; 
	}
	const dbl *b0=src2.t;
	for(u32 i=0;i<di;i++)for(u32 j=0;j<nj;j++){
		for(u32 k=0;k<nk;k++)d0[i*nj+j]+=a0[i*nk+k]*b0[j*nk+k];
	}
}
#ifdef PL_PROFILE
	GyujtStop(3);
#endif

}


//***********************************************************************
void drmatrix::tmul_6(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop){
// src2 egy transzpon�lt m�trix
//***********************************************************************
#ifdef vsundebugmode
	if(&src1==this)throw hiba("","drmatrix::tmul => &src1==this");
	if(&src2==this)throw hiba("","drmatrix::tmul => &src2==this");
	if(src1.col!=src2.col)throw hiba("","drmatrix::tmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if(adde){
		if(row!=src1.row)throw hiba("","drmatrix::tmul => row!=src1.row => %lu!=%lu",row,src1.row);
		if(col!=src2.row)throw hiba("","drmatrix::tmul => col!=src2.row => %lu!=%lu",col,src2.row);
	}
#endif
#ifdef PL_PROFILE
	GyujtStart();
#endif

    cd szorszam=dbl(src2.row)*src1.row*src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	// mxOptimize-t �s p�ratlans�got figyelni a m�reten k�v�l!

	if(!adde && i_stop==0){resize(src1.row,src2.row);zero();} // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if((mxOptimize!=mxMinSize) && (((src1.col|src1.row|src2.row)&1)==0) && 
		(STRASSEN_MUL_N_LIMIT()<=szorszam)){
		strassentmul(src1,src2);
		return;
	}
    if(i_stop==0 && szorszam>MUL_N_LIMIT() && row>16){ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        uns szalszam=(uns)(szorszam*2/MUL_N_LIMIT());
        szalszam = ( szalszam > row/8 ) ? row/8 : szalszam;
        szalszam = ( szalszam > VSUN_CPU_Thread ) ? VSUN_CPU_Thread : szalszam;
        if(szalszam>1){
            //printf("\n%u %u\n",row,szalszam);
            drmtmulszal ** szaltomb=new drmtmulszal*[szalszam];
            cu32 darab=(row/szalszam/4)*4;
            if(darab<2)throw hiba("drmatrix::tmul","darab<2");
            for(uns i=0; i<szalszam-1; i++){
                szaltomb[i]=new drmtmulszal(src1,src2,*this,i*darab,(i+1)*darab);
            }
			parhuzamosanfut=true;
            szaltomb[szalszam-1]=new drmtmulszal(src1,src2,*this,(szalszam-1)*darab,row);
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->start();
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->wait();
            for(uns i=0; i<szalszam; i++)
                delete szaltomb[i];
            delete [] szaltomb;
            return;
        }
    }
    if(i_stop==0)i_stop=row; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 n=src1.col;
if(mxMulMode==mxMinSize){
	dbl * t0=t+i_start*col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd * t1=src1.t+i_start*n;//n==src1.col,row==src1.row // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<i_stop;i++,t1+=n){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd * t2=src2.t;//n==src1.col,col==src1.row
		for(u32 j=0;j<col;j++,t0++,t2+=n){
			dbl sum=t1[0]*t2[0];
			for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
			*t0+=sum;
		}
	}
}
else {
	cu32 ni=i_stop,nj=col,nk=src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 di=i_stop%6,dj=col%6,dk=src1.col%6; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 hi=ni-di,hj=nj-dj,hk=nk-dk;
	dbl *d0=t+i_start*col,     *d1=t+i_start*col+nj,  *d2=t+i_start*col+2*nj,*d3=t+i_start*col+3*nj; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	dbl *d4=t+i_start*col+4*nj,*d5=t+i_start*col+5*nj; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd *a0=src1.t+i_start*n,     *a1=src1.t+i_start*n+nk,  *a2=src1.t+i_start*n+2*nk,*a3=src1.t+i_start*n+3*nk; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd *a4=src1.t+i_start*n+4*nk,*a5=src1.t+i_start*n+5*nk; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<hi;i+=6){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd *b0=src2.t,*b1=src2.t+nk,*b2=src2.t+2*nk,*b3=src2.t+3*nk,*b4=src2.t+4*nk,*b5=src2.t+5*nk;
		for(u32 j=0;j<hj;j+=6){
			for(u32 k=0;k<hk;k+=6,a0+=6,a1+=6,a2+=6,a3+=6,a4+=6,a5+=6,b0+=6,b1+=6,b2+=6,b3+=6,b4+=6,b5+=6){
				d0[0]+=a0[0]*b0[0]+a0[1]*b0[1]+a0[2]*b0[2]+a0[3]*b0[3]+a0[4]*b0[4]+a0[5]*b0[5];
				d0[1]+=a0[0]*b1[0]+a0[1]*b1[1]+a0[2]*b1[2]+a0[3]*b1[3]+a0[4]*b1[4]+a0[5]*b1[5];
				d0[2]+=a0[0]*b2[0]+a0[1]*b2[1]+a0[2]*b2[2]+a0[3]*b2[3]+a0[4]*b2[4]+a0[5]*b2[5];
				d0[3]+=a0[0]*b3[0]+a0[1]*b3[1]+a0[2]*b3[2]+a0[3]*b3[3]+a0[4]*b3[4]+a0[5]*b3[5];
				d0[4]+=a0[0]*b4[0]+a0[1]*b4[1]+a0[2]*b4[2]+a0[3]*b4[3]+a0[4]*b4[4]+a0[5]*b4[5];
				d0[5]+=a0[0]*b5[0]+a0[1]*b5[1]+a0[2]*b5[2]+a0[3]*b5[3]+a0[4]*b5[4]+a0[5]*b5[5];

				d1[0]+=a1[0]*b0[0]+a1[1]*b0[1]+a1[2]*b0[2]+a1[3]*b0[3]+a1[4]*b0[4]+a1[5]*b0[5];
				d1[1]+=a1[0]*b1[0]+a1[1]*b1[1]+a1[2]*b1[2]+a1[3]*b1[3]+a1[4]*b1[4]+a1[5]*b1[5];
				d1[2]+=a1[0]*b2[0]+a1[1]*b2[1]+a1[2]*b2[2]+a1[3]*b2[3]+a1[4]*b2[4]+a1[5]*b2[5];
				d1[3]+=a1[0]*b3[0]+a1[1]*b3[1]+a1[2]*b3[2]+a1[3]*b3[3]+a1[4]*b3[4]+a1[5]*b3[5];
				d1[4]+=a1[0]*b4[0]+a1[1]*b4[1]+a1[2]*b4[2]+a1[3]*b4[3]+a1[4]*b4[4]+a1[5]*b4[5];
				d1[5]+=a1[0]*b5[0]+a1[1]*b5[1]+a1[2]*b5[2]+a1[3]*b5[3]+a1[4]*b5[4]+a1[5]*b5[5];

				d2[0]+=a2[0]*b0[0]+a2[1]*b0[1]+a2[2]*b0[2]+a2[3]*b0[3]+a2[4]*b0[4]+a2[5]*b0[5];
				d2[1]+=a2[0]*b1[0]+a2[1]*b1[1]+a2[2]*b1[2]+a2[3]*b1[3]+a2[4]*b1[4]+a2[5]*b1[5];
				d2[2]+=a2[0]*b2[0]+a2[1]*b2[1]+a2[2]*b2[2]+a2[3]*b2[3]+a2[4]*b2[4]+a2[5]*b2[5];
				d2[3]+=a2[0]*b3[0]+a2[1]*b3[1]+a2[2]*b3[2]+a2[3]*b3[3]+a2[4]*b3[4]+a2[5]*b3[5];
				d2[4]+=a2[0]*b4[0]+a2[1]*b4[1]+a2[2]*b4[2]+a2[3]*b4[3]+a2[4]*b4[4]+a2[5]*b4[5];
				d2[5]+=a2[0]*b5[0]+a2[1]*b5[1]+a2[2]*b5[2]+a2[3]*b5[3]+a2[4]*b5[4]+a2[5]*b5[5];

				d3[0]+=a3[0]*b0[0]+a3[1]*b0[1]+a3[2]*b0[2]+a3[3]*b0[3]+a3[4]*b0[4]+a3[5]*b0[5];
				d3[1]+=a3[0]*b1[0]+a3[1]*b1[1]+a3[2]*b1[2]+a3[3]*b1[3]+a3[4]*b1[4]+a3[5]*b1[5];
				d3[2]+=a3[0]*b2[0]+a3[1]*b2[1]+a3[2]*b2[2]+a3[3]*b2[3]+a3[4]*b2[4]+a3[5]*b2[5];
				d3[3]+=a3[0]*b3[0]+a3[1]*b3[1]+a3[2]*b3[2]+a3[3]*b3[3]+a3[4]*b3[4]+a3[5]*b3[5];
				d3[4]+=a3[0]*b4[0]+a3[1]*b4[1]+a3[2]*b4[2]+a3[3]*b4[3]+a3[4]*b4[4]+a3[5]*b4[5];
				d3[5]+=a3[0]*b5[0]+a3[1]*b5[1]+a3[2]*b5[2]+a3[3]*b5[3]+a3[4]*b5[4]+a3[5]*b5[5];

				d4[0]+=a4[0]*b0[0]+a4[1]*b0[1]+a4[2]*b0[2]+a4[3]*b0[3]+a4[4]*b0[4]+a4[5]*b0[5];
				d4[1]+=a4[0]*b1[0]+a4[1]*b1[1]+a4[2]*b1[2]+a4[3]*b1[3]+a4[4]*b1[4]+a4[5]*b1[5];
				d4[2]+=a4[0]*b2[0]+a4[1]*b2[1]+a4[2]*b2[2]+a4[3]*b2[3]+a4[4]*b2[4]+a4[5]*b2[5];
				d4[3]+=a4[0]*b3[0]+a4[1]*b3[1]+a4[2]*b3[2]+a4[3]*b3[3]+a4[4]*b3[4]+a4[5]*b3[5];
				d4[4]+=a4[0]*b4[0]+a4[1]*b4[1]+a4[2]*b4[2]+a4[3]*b4[3]+a4[4]*b4[4]+a4[5]*b4[5];
				d4[5]+=a4[0]*b5[0]+a4[1]*b5[1]+a4[2]*b5[2]+a4[3]*b5[3]+a4[4]*b5[4]+a4[5]*b5[5];

				d5[0]+=a5[0]*b0[0]+a5[1]*b0[1]+a5[2]*b0[2]+a5[3]*b0[3]+a5[4]*b0[4]+a5[5]*b0[5];
				d5[1]+=a5[0]*b1[0]+a5[1]*b1[1]+a5[2]*b1[2]+a5[3]*b1[3]+a5[4]*b1[4]+a5[5]*b1[5];
				d5[2]+=a5[0]*b2[0]+a5[1]*b2[1]+a5[2]*b2[2]+a5[3]*b2[3]+a5[4]*b2[4]+a5[5]*b2[5];
				d5[3]+=a5[0]*b3[0]+a5[1]*b3[1]+a5[2]*b3[2]+a5[3]*b3[3]+a5[4]*b3[4]+a5[5]*b3[5];
				d5[4]+=a5[0]*b4[0]+a5[1]*b4[1]+a5[2]*b4[2]+a5[3]*b4[3]+a5[4]*b4[4]+a5[5]*b4[5];
				d5[5]+=a5[0]*b5[0]+a5[1]*b5[1]+a5[2]*b5[2]+a5[3]*b5[3]+a5[4]*b5[4]+a5[5]*b5[5];
			}
			for(u32 k=0;k<dk;k++){
				d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];	d0[2]+=a0[k]*b2[k];	d0[3]+=a0[k]*b3[k]; d0[4]+=a0[k]*b4[k];	d0[5]+=a0[k]*b5[k];
				d1[0]+=a1[k]*b0[k];	d1[1]+=a1[k]*b1[k];	d1[2]+=a1[k]*b2[k];	d1[3]+=a1[k]*b3[k]; d1[4]+=a1[k]*b4[k];	d1[5]+=a1[k]*b5[k];
				d2[0]+=a2[k]*b0[k];	d2[1]+=a2[k]*b1[k];	d2[2]+=a2[k]*b2[k];	d2[3]+=a2[k]*b3[k]; d2[4]+=a2[k]*b4[k];	d2[5]+=a2[k]*b5[k];
				d3[0]+=a3[k]*b0[k];	d3[1]+=a3[k]*b1[k];	d3[2]+=a3[k]*b2[k];	d3[3]+=a3[k]*b3[k]; d3[4]+=a3[k]*b4[k];	d3[5]+=a3[k]*b5[k];
				d4[0]+=a4[k]*b0[k];	d4[1]+=a4[k]*b1[k];	d4[2]+=a4[k]*b2[k];	d4[3]+=a4[k]*b3[k]; d4[4]+=a4[k]*b4[k];	d4[5]+=a4[k]*b5[k];
				d5[0]+=a5[k]*b0[k];	d5[1]+=a5[k]*b1[k];	d5[2]+=a5[k]*b2[k];	d5[3]+=a5[k]*b3[k]; d5[4]+=a5[k]*b4[k];	d5[5]+=a5[k]*b5[k];
			}
			d0+=6,d1+=6,d2+=6,d3+=6,d4+=6,d5+=6;
			a0-=hk,a1-=hk,a2-=hk,a3-=hk,a4-=hk,a5-=hk;
			b0+=dk+5*nk,b1+=dk+5*nk,b2+=dk+5*nk,b3+=dk+5*nk,b4+=dk+5*nk,b5+=dk+5*nk;
		}
		for(u32 j=0;j<dj;j++){
			for(u32 k=0;k<nk;k++){
				d0[j]+=a0[k]*b0[j*nk+k]; d1[j]+=a1[k]*b0[j*nk+k]; d2[j]+=a2[k]*b0[j*nk+k]; d3[j]+=a3[k]*b0[j*nk+k];
				d4[j]+=a4[k]*b0[j*nk+k]; d5[j]+=a5[k]*b0[j*nk+k];
			}
		}
		d0+=dj+5*nj; d1+=dj+5*nj; d2+=dj+5*nj; d3+=dj+5*nj; d4+=dj+5*nj; d5+=dj+5*nj;
		a0+=6*nk; a1+=6*nk; a2+=6*nk; a3+=6*nk; a4+=6*nk; a5+=6*nk;
	}
	const dbl *b0=src2.t;
	for(u32 i=0;i<di;i++)for(u32 j=0;j<nj;j++){
		for(u32 k=0;k<nk;k++)d0[i*nj+j]+=a0[i*nk+k]*b0[j*nk+k];
	}
}
#ifdef PL_PROFILE
	GyujtStop(3);
#endif

}


//***********************************************************************
void drmatrix::tmul_5(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop){
// src2 egy transzpon�lt m�trix
//***********************************************************************
#ifdef vsundebugmode
	if(&src1==this)throw hiba("","drmatrix::tmul => &src1==this");
	if(&src2==this)throw hiba("","drmatrix::tmul => &src2==this");
	if(src1.col!=src2.col)throw hiba("","drmatrix::tmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if(adde){
		if(row!=src1.row)throw hiba("","drmatrix::tmul => row!=src1.row => %lu!=%lu",row,src1.row);
		if(col!=src2.row)throw hiba("","drmatrix::tmul => col!=src2.row => %lu!=%lu",col,src2.row);
	}
#endif
#ifdef PL_PROFILE
	GyujtStart();
#endif

    cd szorszam=dbl(src2.row)*src1.row*src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	// mxOptimize-t �s p�ratlans�got figyelni a m�reten k�v�l!

	if(!adde && i_stop==0){resize(src1.row,src2.row);zero();} // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if((mxOptimize!=mxMinSize) && (((src1.col|src1.row|src2.row)&1)==0) && 
		(STRASSEN_MUL_N_LIMIT()<=szorszam)){
		strassentmul(src1,src2);
		return;
	}
    if(i_stop==0 && szorszam>MUL_N_LIMIT() && row>16){ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        uns szalszam=(uns)(szorszam*2/MUL_N_LIMIT());
        szalszam = ( szalszam > row/8 ) ? row/8 : szalszam;
        szalszam = ( szalszam > VSUN_CPU_Thread ) ? VSUN_CPU_Thread : szalszam;
        if(szalszam>1){
            //printf("\n%u %u\n",row,szalszam);
            drmtmulszal ** szaltomb=new drmtmulszal*[szalszam];
            cu32 darab=(row/szalszam/4)*4;
            if(darab<2)throw hiba("drmatrix::tmul","darab<2");
            for(uns i=0; i<szalszam-1; i++){
                szaltomb[i]=new drmtmulszal(src1,src2,*this,i*darab,(i+1)*darab);
            }
			parhuzamosanfut=true;
            szaltomb[szalszam-1]=new drmtmulszal(src1,src2,*this,(szalszam-1)*darab,row);
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->start();
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->wait();
            for(uns i=0; i<szalszam; i++)
                delete szaltomb[i];
            delete [] szaltomb;
            return;
        }
    }
    if(i_stop==0)i_stop=row; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 n=src1.col;
if(mxMulMode==mxMinSize){
	dbl * t0=t+i_start*col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd * t1=src1.t+i_start*n;//n==src1.col,row==src1.row // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<i_stop;i++,t1+=n){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd * t2=src2.t;//n==src1.col,col==src1.row
		for(u32 j=0;j<col;j++,t0++,t2+=n){
			dbl sum=t1[0]*t2[0];
			for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
			*t0+=sum;
		}
	}
}
else {
	cu32 ni=i_stop,nj=col,nk=src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 di=i_stop%5,dj=col%5,dk=src1.col%5; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 hi=ni-di,hj=nj-dj,hk=nk-dk;
	dbl *d0=t+i_start*col,     *d1=t+i_start*col+nj,  *d2=t+i_start*col+2*nj,*d3=t+i_start*col+3*nj,  *d4=t+i_start*col+4*nj; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd *a0=src1.t+i_start*n,     *a1=src1.t+i_start*n+nk,  *a2=src1.t+i_start*n+2*nk,*a3=src1.t+i_start*n+3*nk,  *a4=src1.t+i_start*n+4*nk; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<hi;i+=5){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd *b0=src2.t,*b1=src2.t+nk,*b2=src2.t+2*nk,*b3=src2.t+3*nk,*b4=src2.t+4*nk;
		for(u32 j=0;j<hj;j+=5){
			for(u32 k=0;k<hk;k+=5,a0+=5,a1+=5,a2+=5,a3+=5,a4+=5,b0+=5,b1+=5,b2+=5,b3+=5,b4+=5){
				d0[0]+=a0[0]*b0[0]+a0[1]*b0[1]+a0[2]*b0[2]+a0[3]*b0[3]+a0[4]*b0[4];
				d0[1]+=a0[0]*b1[0]+a0[1]*b1[1]+a0[2]*b1[2]+a0[3]*b1[3]+a0[4]*b1[4];
				d0[2]+=a0[0]*b2[0]+a0[1]*b2[1]+a0[2]*b2[2]+a0[3]*b2[3]+a0[4]*b2[4];
				d0[3]+=a0[0]*b3[0]+a0[1]*b3[1]+a0[2]*b3[2]+a0[3]*b3[3]+a0[4]*b3[4];
				d0[4]+=a0[0]*b4[0]+a0[1]*b4[1]+a0[2]*b4[2]+a0[3]*b4[3]+a0[4]*b4[4];

				d1[0]+=a1[0]*b0[0]+a1[1]*b0[1]+a1[2]*b0[2]+a1[3]*b0[3]+a1[4]*b0[4];
				d1[1]+=a1[0]*b1[0]+a1[1]*b1[1]+a1[2]*b1[2]+a1[3]*b1[3]+a1[4]*b1[4];
				d1[2]+=a1[0]*b2[0]+a1[1]*b2[1]+a1[2]*b2[2]+a1[3]*b2[3]+a1[4]*b2[4];
				d1[3]+=a1[0]*b3[0]+a1[1]*b3[1]+a1[2]*b3[2]+a1[3]*b3[3]+a1[4]*b3[4];
				d1[4]+=a1[0]*b4[0]+a1[1]*b4[1]+a1[2]*b4[2]+a1[3]*b4[3]+a1[4]*b4[4];

				d2[0]+=a2[0]*b0[0]+a2[1]*b0[1]+a2[2]*b0[2]+a2[3]*b0[3]+a2[4]*b0[4];
				d2[1]+=a2[0]*b1[0]+a2[1]*b1[1]+a2[2]*b1[2]+a2[3]*b1[3]+a2[4]*b1[4];
				d2[2]+=a2[0]*b2[0]+a2[1]*b2[1]+a2[2]*b2[2]+a2[3]*b2[3]+a2[4]*b2[4];
				d2[3]+=a2[0]*b3[0]+a2[1]*b3[1]+a2[2]*b3[2]+a2[3]*b3[3]+a2[4]*b3[4];
				d2[4]+=a2[0]*b4[0]+a2[1]*b4[1]+a2[2]*b4[2]+a2[3]*b4[3]+a2[4]*b4[4];

				d3[0]+=a3[0]*b0[0]+a3[1]*b0[1]+a3[2]*b0[2]+a3[3]*b0[3]+a3[4]*b0[4];
				d3[1]+=a3[0]*b1[0]+a3[1]*b1[1]+a3[2]*b1[2]+a3[3]*b1[3]+a3[4]*b1[4];
				d3[2]+=a3[0]*b2[0]+a3[1]*b2[1]+a3[2]*b2[2]+a3[3]*b2[3]+a3[4]*b2[4];
				d3[3]+=a3[0]*b3[0]+a3[1]*b3[1]+a3[2]*b3[2]+a3[3]*b3[3]+a3[4]*b3[4];
				d3[4]+=a3[0]*b4[0]+a3[1]*b4[1]+a3[2]*b4[2]+a3[3]*b4[3]+a3[4]*b4[4];

				d4[0]+=a4[0]*b0[0]+a4[1]*b0[1]+a4[2]*b0[2]+a4[3]*b0[3]+a4[4]*b0[4];
				d4[1]+=a4[0]*b1[0]+a4[1]*b1[1]+a4[2]*b1[2]+a4[3]*b1[3]+a4[4]*b1[4];
				d4[2]+=a4[0]*b2[0]+a4[1]*b2[1]+a4[2]*b2[2]+a4[3]*b2[3]+a4[4]*b2[4];
				d4[3]+=a4[0]*b3[0]+a4[1]*b3[1]+a4[2]*b3[2]+a4[3]*b3[3]+a4[4]*b3[4];
				d4[4]+=a4[0]*b4[0]+a4[1]*b4[1]+a4[2]*b4[2]+a4[3]*b4[3]+a4[4]*b4[4];
			}
			for(u32 k=0;k<dk;k++){
				d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];	d0[2]+=a0[k]*b2[k];	d0[3]+=a0[k]*b3[k]; d0[4]+=a0[k]*b4[k];
				d1[0]+=a1[k]*b0[k];	d1[1]+=a1[k]*b1[k];	d1[2]+=a1[k]*b2[k];	d1[3]+=a1[k]*b3[k]; d1[4]+=a1[k]*b4[k];
				d2[0]+=a2[k]*b0[k];	d2[1]+=a2[k]*b1[k];	d2[2]+=a2[k]*b2[k];	d2[3]+=a2[k]*b3[k]; d2[4]+=a2[k]*b4[k];
				d3[0]+=a3[k]*b0[k];	d3[1]+=a3[k]*b1[k];	d3[2]+=a3[k]*b2[k];	d3[3]+=a3[k]*b3[k]; d3[4]+=a3[k]*b4[k];
				d4[0]+=a4[k]*b0[k];	d4[1]+=a4[k]*b1[k];	d4[2]+=a4[k]*b2[k];	d4[3]+=a4[k]*b3[k]; d4[4]+=a4[k]*b4[k];
			}
			d0+=5,d1+=5,d2+=5,d3+=5,d4+=5;
			a0-=hk,a1-=hk,a2-=hk,a3-=hk,a4-=hk;
			b0+=dk+4*nk,b1+=dk+4*nk,b2+=dk+4*nk,b3+=dk+4*nk,b4+=dk+4*nk;
		}
		for(u32 j=0;j<dj;j++){
			for(u32 k=0;k<nk;k++){
				d0[j]+=a0[k]*b0[j*nk+k]; d1[j]+=a1[k]*b0[j*nk+k]; d2[j]+=a2[k]*b0[j*nk+k]; d3[j]+=a3[k]*b0[j*nk+k];
				d4[j]+=a4[k]*b0[j*nk+k];
			}
		}
		d0+=dj+4*nj; d1+=dj+4*nj; d2+=dj+4*nj; d3+=dj+4*nj; d4+=dj+4*nj;
		a0+=5*nk; a1+=5*nk; a2+=5*nk; a3+=5*nk; a4+=5*nk;
	}
	const dbl *b0=src2.t;
	for(u32 i=0;i<di;i++)for(u32 j=0;j<nj;j++){
		for(u32 k=0;k<nk;k++)d0[i*nj+j]+=a0[i*nk+k]*b0[j*nk+k];
	}
}
#ifdef PL_PROFILE
	GyujtStop(3);
#endif

}


//***********************************************************************
void drmatrix::tmul_4(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop){
// src2 egy transzpon�lt m�trix
//***********************************************************************
#ifdef vsundebugmode
	if(&src1==this)throw hiba("","drmatrix::tmul => &src1==this");
	if(&src2==this)throw hiba("","drmatrix::tmul => &src2==this");
	if(src1.col!=src2.col)throw hiba("","drmatrix::tmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if(adde){
		if(row!=src1.row)throw hiba("","drmatrix::tmul => row!=src1.row => %lu!=%lu",row,src1.row);
		if(col!=src2.row)throw hiba("","drmatrix::tmul => col!=src2.row => %lu!=%lu",col,src2.row);
	}
#endif
#ifdef PL_PROFILE
	GyujtStart();
#endif

    cd szorszam=dbl(src2.row)*src1.row*src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	// mxOptimize-t �s p�ratlans�got figyelni a m�reten k�v�l!

	if(!adde && i_stop==0){resize(src1.row,src2.row);zero();} // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if((mxOptimize!=mxMinSize) && (((src1.col|src1.row|src2.row)&1)==0) && 
		(STRASSEN_MUL_N_LIMIT()<=szorszam)){
		strassentmul(src1,src2);
		return;
	}
    if(i_stop==0 && szorszam>MUL_N_LIMIT() && row>16){ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        uns szalszam=(uns)(szorszam*2/MUL_N_LIMIT());
        szalszam = ( szalszam > row/8 ) ? row/8 : szalszam;
        szalszam = ( szalszam > VSUN_CPU_Thread ) ? VSUN_CPU_Thread : szalszam;
        if(szalszam>1){
            //printf("\n%u %u\n",row,szalszam);
            drmtmulszal ** szaltomb=new drmtmulszal*[szalszam];
            cu32 darab=(row/szalszam/4)*4;
            if(darab<2)throw hiba("drmatrix::tmul","darab<2");
            for(uns i=0; i<szalszam-1; i++){
                szaltomb[i]=new drmtmulszal(src1,src2,*this,i*darab,(i+1)*darab);
            }
			parhuzamosanfut=true;
            szaltomb[szalszam-1]=new drmtmulszal(src1,src2,*this,(szalszam-1)*darab,row);
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->start();
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->wait();
            for(uns i=0; i<szalszam; i++)
                delete szaltomb[i];
            delete [] szaltomb;
            return;
        }
    }
    if(i_stop==0)i_stop=row; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 n=src1.col;
if(mxMulMode==mxMinSize){
	dbl * t0=t+i_start*col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd * t1=src1.t+i_start*n;//n==src1.col,row==src1.row // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<i_stop;i++,t1+=n){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd * t2=src2.t;//n==src1.col,col==src1.row
		for(u32 j=0;j<col;j++,t0++,t2+=n){
			dbl sum=t1[0]*t2[0];
			for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
			*t0+=sum;
		}
	}
}
else {
	cu32 ni=i_stop,nj=col,nk=src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 di=i_stop%4,dj=col%4,dk=src1.col%4; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 hi=ni-di,hj=nj-dj,hk=nk-dk;
	dbl *d0=t+i_start*col,*d1=t+i_start*col+nj,*d2=t+i_start*col+2*nj,*d3=t+i_start*col+3*nj; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd *a0=src1.t+i_start*n,*a1=src1.t+i_start*n+nk,*a2=src1.t+i_start*n+2*nk,*a3=src1.t+i_start*n+3*nk; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<hi;i+=4){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd *b0=src2.t,*b1=src2.t+nk,*b2=src2.t+2*nk,*b3=src2.t+3*nk;
		for(u32 j=0;j<hj;j+=4){
			for(u32 k=0;k<hk;k+=4,a0+=4,a1+=4,a2+=4,a3+=4,b0+=4,b1+=4,b2+=4,b3+=4){
				d0[0]+=a0[0]*b0[0]+a0[1]*b0[1]+a0[2]*b0[2]+a0[3]*b0[3];
				d0[1]+=a0[0]*b1[0]+a0[1]*b1[1]+a0[2]*b1[2]+a0[3]*b1[3];
				d0[2]+=a0[0]*b2[0]+a0[1]*b2[1]+a0[2]*b2[2]+a0[3]*b2[3];
				d0[3]+=a0[0]*b3[0]+a0[1]*b3[1]+a0[2]*b3[2]+a0[3]*b3[3];

				d1[0]+=a1[0]*b0[0]+a1[1]*b0[1]+a1[2]*b0[2]+a1[3]*b0[3];
				d1[1]+=a1[0]*b1[0]+a1[1]*b1[1]+a1[2]*b1[2]+a1[3]*b1[3];
				d1[2]+=a1[0]*b2[0]+a1[1]*b2[1]+a1[2]*b2[2]+a1[3]*b2[3];
				d1[3]+=a1[0]*b3[0]+a1[1]*b3[1]+a1[2]*b3[2]+a1[3]*b3[3];

				d2[0]+=a2[0]*b0[0]+a2[1]*b0[1]+a2[2]*b0[2]+a2[3]*b0[3];
				d2[1]+=a2[0]*b1[0]+a2[1]*b1[1]+a2[2]*b1[2]+a2[3]*b1[3];
				d2[2]+=a2[0]*b2[0]+a2[1]*b2[1]+a2[2]*b2[2]+a2[3]*b2[3];
				d2[3]+=a2[0]*b3[0]+a2[1]*b3[1]+a2[2]*b3[2]+a2[3]*b3[3];

				d3[0]+=a3[0]*b0[0]+a3[1]*b0[1]+a3[2]*b0[2]+a3[3]*b0[3];
				d3[1]+=a3[0]*b1[0]+a3[1]*b1[1]+a3[2]*b1[2]+a3[3]*b1[3];
				d3[2]+=a3[0]*b2[0]+a3[1]*b2[1]+a3[2]*b2[2]+a3[3]*b2[3];
				d3[3]+=a3[0]*b3[0]+a3[1]*b3[1]+a3[2]*b3[2]+a3[3]*b3[3];
			}
			for(u32 k=0;k<dk;k++){
				d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];	d0[2]+=a0[k]*b2[k];	d0[3]+=a0[k]*b3[k];
				d1[0]+=a1[k]*b0[k];	d1[1]+=a1[k]*b1[k];	d1[2]+=a1[k]*b2[k];	d1[3]+=a1[k]*b3[k];
				d2[0]+=a2[k]*b0[k];	d2[1]+=a2[k]*b1[k];	d2[2]+=a2[k]*b2[k];	d2[3]+=a2[k]*b3[k];
				d3[0]+=a3[k]*b0[k];	d3[1]+=a3[k]*b1[k];	d3[2]+=a3[k]*b2[k];	d3[3]+=a3[k]*b3[k];
			}
			d0+=4,d1+=4,d2+=4,d3+=4;
			a0-=hk,a1-=hk,a2-=hk,a3-=hk,b0+=dk+3*nk,b1+=dk+3*nk,b2+=dk+3*nk,b3+=dk+3*nk;
		}
		for(u32 j=0;j<dj;j++){
			for(u32 k=0;k<nk;k++){
				d0[j]+=a0[k]*b0[j*nk+k]; d1[j]+=a1[k]*b0[j*nk+k]; d2[j]+=a2[k]*b0[j*nk+k]; d3[j]+=a3[k]*b0[j*nk+k];
			}
		}
		d0+=dj+3*nj; d1+=dj+3*nj; d2+=dj+3*nj; d3+=dj+3*nj;
		a0+=4*nk; a1+=4*nk; a2+=4*nk; a3+=4*nk;
	}
	const dbl *b0=src2.t;
	for(u32 i=0;i<di;i++)for(u32 j=0;j<nj;j++){
		for(u32 k=0;k<nk;k++)d0[i*nj+j]+=a0[i*nk+k]*b0[j*nk+k];
	}
}
#ifdef PL_PROFILE
	GyujtStop(3);
#endif

}


//***********************************************************************
void drmatrix::tmul_3(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop){
// src2 egy transzpon�lt m�trix
//***********************************************************************
#ifdef vsundebugmode
	if(&src1==this)throw hiba("","drmatrix::tmul => &src1==this");
	if(&src2==this)throw hiba("","drmatrix::tmul => &src2==this");
	if(src1.col!=src2.col)throw hiba("","drmatrix::tmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if(adde){
		if(row!=src1.row)throw hiba("","drmatrix::tmul => row!=src1.row => %lu!=%lu",row,src1.row);
		if(col!=src2.row)throw hiba("","drmatrix::tmul => col!=src2.row => %lu!=%lu",col,src2.row);
	}
#endif
#ifdef PL_PROFILE
	GyujtStart();
#endif

    cd szorszam=dbl(src2.row)*src1.row*src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	// mxOptimize-t �s p�ratlans�got figyelni a m�reten k�v�l!

	if(!adde && i_stop==0){resize(src1.row,src2.row);zero();} // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if((mxOptimize!=mxMinSize) && (((src1.col|src1.row|src2.row)&1)==0) && 
		(STRASSEN_MUL_N_LIMIT()<=szorszam)){
		strassentmul(src1,src2);
		return;
	}
    if(i_stop==0 && szorszam>MUL_N_LIMIT() && row>16){ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        uns szalszam=(uns)(szorszam*2/MUL_N_LIMIT());
        szalszam = ( szalszam > row/8 ) ? row/8 : szalszam;
        szalszam = ( szalszam > VSUN_CPU_Thread ) ? VSUN_CPU_Thread : szalszam;
        if(szalszam>1){
            //printf("\n%u %u\n",row,szalszam);
            drmtmulszal ** szaltomb=new drmtmulszal*[szalszam];
            cu32 darab=(row/szalszam/4)*4;
            if(darab<2)throw hiba("drmatrix::tmul","darab<2");
            for(uns i=0; i<szalszam-1; i++){
                szaltomb[i]=new drmtmulszal(src1,src2,*this,i*darab,(i+1)*darab);
            }
			parhuzamosanfut=true;
            szaltomb[szalszam-1]=new drmtmulszal(src1,src2,*this,(szalszam-1)*darab,row);
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->start();
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->wait();
            for(uns i=0; i<szalszam; i++)
                delete szaltomb[i];
            delete [] szaltomb;
            return;
        }
    }
    if(i_stop==0)i_stop=row; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 n=src1.col;
if(mxMulMode==mxMinSize){
	dbl * t0=t+i_start*col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd * t1=src1.t+i_start*n;//n==src1.col,row==src1.row // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<i_stop;i++,t1+=n){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd * t2=src2.t;//n==src1.col,col==src1.row
		for(u32 j=0;j<col;j++,t0++,t2+=n){
			dbl sum=t1[0]*t2[0];
			for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
			*t0+=sum;
		}
	}
}
else {
	cu32 ni=i_stop,nj=col,nk=src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 di=i_stop%3,dj=col%3,dk=src1.col%3; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 hi=ni-di,hj=nj-dj,hk=nk-dk;
	dbl *d0=t+i_start*col,*d1=t+i_start*col+nj,*d2=t+i_start*col+2*nj; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd *a0=src1.t+i_start*n,*a1=src1.t+i_start*n+nk,*a2=src1.t+i_start*n+2*nk; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<hi;i+=3){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd *b0=src2.t,*b1=src2.t+nk,*b2=src2.t+2*nk;
		for(u32 j=0;j<hj;j+=3){
			for(u32 k=0;k<hk;k+=3,a0+=3,a1+=3,a2+=3,b0+=3,b1+=3,b2+=3){
				d0[0]+=a0[0]*b0[0]+a0[1]*b0[1]+a0[2]*b0[2];
				d0[1]+=a0[0]*b1[0]+a0[1]*b1[1]+a0[2]*b1[2];
				d0[2]+=a0[0]*b2[0]+a0[1]*b2[1]+a0[2]*b2[2];

				d1[0]+=a1[0]*b0[0]+a1[1]*b0[1]+a1[2]*b0[2];
				d1[1]+=a1[0]*b1[0]+a1[1]*b1[1]+a1[2]*b1[2];
				d1[2]+=a1[0]*b2[0]+a1[1]*b2[1]+a1[2]*b2[2];

				d2[0]+=a2[0]*b0[0]+a2[1]*b0[1]+a2[2]*b0[2];
				d2[1]+=a2[0]*b1[0]+a2[1]*b1[1]+a2[2]*b1[2];
				d2[2]+=a2[0]*b2[0]+a2[1]*b2[1]+a2[2]*b2[2];
			}
			for(u32 k=0;k<dk;k++){
				d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];	d0[2]+=a0[k]*b2[k];
				d1[0]+=a1[k]*b0[k];	d1[1]+=a1[k]*b1[k];	d1[2]+=a1[k]*b2[k];
				d2[0]+=a2[k]*b0[k];	d2[1]+=a2[k]*b1[k];	d2[2]+=a2[k]*b2[k];
			}
			d0+=3,d1+=3,d2+=3;
			a0-=hk,a1-=hk,a2-=hk,b0+=dk+2*nk,b1+=dk+2*nk,b2+=dk+2*nk;
		}
		for(u32 j=0;j<dj;j++){
			for(u32 k=0;k<nk;k++){
				d0[j]+=a0[k]*b0[j*nk+k]; d1[j]+=a1[k]*b0[j*nk+k]; d2[j]+=a2[k]*b0[j*nk+k];
			}
		}
		d0+=dj+2*nj; d1+=dj+2*nj; d2+=dj+2*nj;
		a0+=3*nk; a1+=3*nk; a2+=3*nk;
	}
	const dbl *b0=src2.t;
	for(u32 i=0;i<di;i++)for(u32 j=0;j<nj;j++){
		for(u32 k=0;k<nk;k++)d0[i*nj+j]+=a0[i*nk+k]*b0[j*nk+k];
	}
}
#ifdef PL_PROFILE
	GyujtStop(3);
#endif

}


//***********************************************************************
void drmatrix::tmul_2(const drmatrix & src1,const drmatrix & src2,const bool adde,u32 i_start,u32 i_stop){
// src2 egy transzpon�lt m�trix
//***********************************************************************
#ifdef vsundebugmode
	if(&src1==this)throw hiba("","drmatrix::tmul => &src1==this");
	if(&src2==this)throw hiba("","drmatrix::tmul => &src2==this");
	if(src1.col!=src2.col)throw hiba("","drmatrix::tmul => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
	if(adde){
		if(row!=src1.row)throw hiba("","drmatrix::tmul => row!=src1.row => %lu!=%lu",row,src1.row);
		if(col!=src2.row)throw hiba("","drmatrix::tmul => col!=src2.row => %lu!=%lu",col,src2.row);
	}
#endif
#ifdef PL_PROFILE
	GyujtStart();
#endif

    cd szorszam=dbl(src2.row)*src1.row*src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

	// mxOptimize-t �s p�ratlans�got figyelni a m�reten k�v�l!

	if(!adde && i_stop==0){resize(src1.row,src2.row);zero();} // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	if((mxOptimize!=mxMinSize) && (((src1.col|src1.row|src2.row)&1)==0) && 
		(STRASSEN_MUL_N_LIMIT()<=szorszam)){
		strassentmul(src1,src2);
		return;
	}
    if(i_stop==0 && szorszam>MUL_N_LIMIT() && row>16){ // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        uns szalszam=(uns)(szorszam*2/MUL_N_LIMIT());
        szalszam = ( szalszam > row/8 ) ? row/8 : szalszam;
        szalszam = ( szalszam > VSUN_CPU_Thread ) ? VSUN_CPU_Thread : szalszam;
        if(szalszam>1){
            //printf("\n%u %u\n",row,szalszam);
            drmtmulszal ** szaltomb=new drmtmulszal*[szalszam];
            cu32 darab=(row/szalszam/4)*4;
            if(darab<2)throw hiba("drmatrix::tmul","darab<2");
            for(uns i=0; i<szalszam-1; i++){
                szaltomb[i]=new drmtmulszal(src1,src2,*this,i*darab,(i+1)*darab);
            }
			parhuzamosanfut=true;
            szaltomb[szalszam-1]=new drmtmulszal(src1,src2,*this,(szalszam-1)*darab,row);
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->start();
            for(uns i=0; i<szalszam; i++)
                szaltomb[i]->wait();
            for(uns i=0; i<szalszam; i++)
                delete szaltomb[i];
            delete [] szaltomb;
            return;
        }
    }
    if(i_stop==0)i_stop=row; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 n=src1.col;
if(mxMulMode==mxMinSize){
	dbl * t0=t+i_start*col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd * t1=src1.t+i_start*n;//n==src1.col,row==src1.row // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<i_stop;i++,t1+=n){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd * t2=src2.t;//n==src1.col,col==src1.row
		for(u32 j=0;j<col;j++,t0++,t2+=n){
			dbl sum=t1[0]*t2[0];
			for(u32 k=1;k<n;k++)sum+=t1[k]*t2[k];
			*t0+=sum;
		}
	}
}
else {
	cu32 ni=i_stop,nj=col,nk=src1.col; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 di=i_stop%2,dj=col%2,dk=src1.col%2; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cu32 hi=ni-di,hj=nj-dj,hk=nk-dk;
	dbl *d0=t+i_start*col,*d1=t+i_start*col+nj; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	cd *a0=src1.t+i_start*n,*a1=src1.t+i_start*n+nk; // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	for(u32 i=i_start;i<hi;i+=2){  // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		cd *b0=src2.t,*b1=src2.t+nk;
		for(u32 j=0;j<hj;j+=2){
			for(u32 k=0;k<hk;k+=2,a0+=2,a1+=2,b0+=2,b1+=2){
				d0[0]+=a0[0]*b0[0]+a0[1]*b0[1];
				d0[1]+=a0[0]*b1[0]+a0[1]*b1[1];

				d1[0]+=a1[0]*b0[0]+a1[1]*b0[1];
				d1[1]+=a1[0]*b1[0]+a1[1]*b1[1];
			}
			for(u32 k=0;k<dk;k++){
				d0[0]+=a0[k]*b0[k];	d0[1]+=a0[k]*b1[k];
				d1[0]+=a1[k]*b0[k];	d1[1]+=a1[k]*b1[k];
			}
			d0+=2,d1+=2;
			a0-=hk,a1-=hk,b0+=dk+nk,b1+=dk+nk;
		}
		for(u32 j=0;j<dj;j++){
			for(u32 k=0;k<nk;k++){
				d0[j]+=a0[k]*b0[j*nk+k]; d1[j]+=a1[k]*b0[j*nk+k];
			}
		}
		d0+=dj+nj; d1+=dj+nj;
		a0+=2*nk; a1+=2*nk;
	}
	const dbl *b0=src2.t;
	for(u32 i=0;i<di;i++)for(u32 j=0;j<nj;j++){
		for(u32 k=0;k<nk;k++)d0[i*nj+j]+=a0[i*nk+k]*b0[j*nk+k];
	}
}
#ifdef PL_PROFILE
	GyujtStop(3);
#endif

}


//***********************************************************************
void dcmatrix::tmul(const dcmatrix & src1,const dcmatrix & src2,const bool adde){
// (a + ib)(c + id) = (ac - bd) + i[(a + b)(c + d) - ac - bd]
//***********************************************************************
	parhuzamosanfut=true;
	if(STRASSEN_MUL_N_LIMIT()<=dbl(src2.getrow())*src1.getrow()*src1.getcol()*2&&mxOptimize!=mxMinSize){
		switch(mxOptimize){
			case mxMaxSpeed :
			case mxBalanced1 :{
					drmatrix m1,m2,m3,m4,m5;
					m1.add(src1.re,src1.im);
					m2.add(src2.re,src2.im);
					drmtmulszal sz0(m1,m2,adde?m5:im);
					drmtmulszal sz1(src1.re,src2.re,m3);
					drmtmulszal sz2(src1.im,src2.im,m4);
					sz0.start(); sz1.start(); sz2.start();
					sz0.wait();  sz1.wait();  sz2.wait();
					if(adde){im.addnr(im,m5);m5.free();}
					im.addsubsubnr(im,m3,m4);
					if(adde){re.addsubnr(re,m3,m4);}
					else re.sub(m3,m4);
				}
				break;			
			case mxBalanced2 :
			case mxBalanced3 :{
					drmatrix m1,m2;
					m1.add(src1.re,src1.im);
					m2.add(src2.re,src2.im);
					im.tmul(m1,m2,adde);
					drmtmulszal sz1(src1.re,src2.re,m1);
					drmtmulszal sz2(src1.im,src2.im,m2);
					sz1.start(); sz2.start();
					sz1.wait();  sz2.wait();
					im.addsubsubnr(im,m1,m2);
					if(adde){re.addsubnr(re,m1,m2);}
					else re.sub(m1,m2);
				}
				break;
			default:throw hiba("","dcmatrix::tmul => mxOptimize==mxMinSize");
		}
	}
	else{
		drmatrix m1,m2;
		m1.add(src1.re,src1.im);
		m2.add(src2.re,src2.im);
		im.tmul(m1,m2,adde);
		m1.tmul(src1.re,src2.re);
		m2.tmul(src1.im,src2.im);
		im.addsubsubnr(im,m1,m2);
		if(adde){re.addsubnr(re,m1,m2);}
		else re.sub(m1,m2);
	}
}

//***********************************************************************
void dcmatrix::tmul(const dscmatrix & src1,const dcmatrix & src2,const bool adde){
// (a + ib)(c + id) = (ac - bd) + i[(a + b)(c + d) - ac - bd]
//***********************************************************************
	parhuzamosanfut=true;
	if(STRASSEN_MUL_N_LIMIT()<=dbl(src2.getrow())*src1.getrow()*src1.getcol()*2&&mxOptimize!=mxMinSize){
		switch(mxOptimize){
			case mxMaxSpeed :
			case mxBalanced1 :{
					drmatrix m2,m3,m4,m5;
					dsrmatrix s1;
					s1.add(src1.re,src1.im);
					m2.add(src2.re,src2.im);
					drmtmulszal sz0(s1,m2,adde?m5:im);
					drmtmulszal sz1(src1.re,src2.re,m3);
					drmtmulszal sz2(src1.im,src2.im,m4);
					sz0.start(); sz1.start(); sz2.start();
					sz0.wait();  sz1.wait();  sz2.wait();
					if(adde){im.addnr(im,m5);m5.free();}
					im.addsubsubnr(im,m3,m4);
					if(adde){re.addsubnr(re,m3,m4);}
					else re.sub(m3,m4);
				}
				break;			
			case mxBalanced2 :
			case mxBalanced3 :{
					drmatrix m1,m2;
					dsrmatrix s1;
					s1.add(src1.re,src1.im);
					m2.add(src2.re,src2.im);
					im.tmul(s1,m2,adde);
					s1.free();
					drmtmulszal sz1(src1.re,src2.re,m1);
					drmtmulszal sz2(src1.im,src2.im,m2);
					sz1.start(); sz2.start();
					sz1.wait();  sz2.wait();
					im.addsubsubnr(im,m1,m2);
					if(adde){re.addsubnr(re,m1,m2);}
					else re.sub(m1,m2);
				}
				break;
			default:throw hiba("","dcmatrix::tmul => mxOptimize==mxMinSize");
		}
	}
	else{
		drmatrix m1,m2;
		dsrmatrix s1;
		s1.add(src1.re,src1.im);
		m2.add(src2.re,src2.im);
		im.tmul(s1,m2,adde);
		s1.free();
		m1.tmul(src1.re,src2.re);
		m2.tmul(src1.im,src2.im);
		im.addsubsubnr(im,m1,m2);
		if(adde){re.addsubnr(re,m1,m2);}
		else re.sub(m1,m2);
	}
}

//***********************************************************************
void dcmatrix::tmul(const dcmatrix & src1,const dscmatrix & src2,const bool adde){
// (a + ib)(c + id) = (ac - bd) + i[(a + b)(c + d) - ac - bd]
//***********************************************************************
	parhuzamosanfut=true;
	if(STRASSEN_MUL_N_LIMIT()<=dbl(src2.getrow())*src1.getrow()*src1.getcol()*2&&mxOptimize!=mxMinSize){
		switch(mxOptimize){
			case mxMaxSpeed :
			case mxBalanced1 :{
					drmatrix m1,m3,m4,m5;
					dsrmatrix s2;
					m1.add(src1.re,src1.im);
					s2.add(src2.re,src2.im);
					drmtmulszal sz0(m1,s2,adde?m5:im);
					drmtmulszal sz1(src1.re,src2.re,m3);
					drmtmulszal sz2(src1.im,src2.im,m4);
					sz0.start(); sz1.start(); sz2.start();
					sz0.wait();  sz1.wait();  sz2.wait();
					if(adde){im.addnr(im,m5);m5.free();}
					im.addsubsubnr(im,m3,m4);
					if(adde){re.addsubnr(re,m3,m4);}
					else re.sub(m3,m4);
				}
				break;			
			case mxBalanced2 :
			case mxBalanced3 :{
					drmatrix m1,m2;
					dsrmatrix s2;
					m1.add(src1.re,src1.im);
					s2.add(src2.re,src2.im);
					im.tmul(m1,s2,adde);
					s2.free();
					drmtmulszal sz1(src1.re,src2.re,m1);
					drmtmulszal sz2(src1.im,src2.im,m2);
					sz1.start(); sz2.start();
					sz1.wait();  sz2.wait();
					im.addsubsubnr(im,m1,m2);
					if(adde){re.addsubnr(re,m1,m2);}
					else re.sub(m1,m2);
				}
				break;
			default:throw hiba("","dcmatrix::tmul => mxOptimize==mxMinSize");
		}
	}
	else{
		drmatrix m1,m2;
		dsrmatrix s2;
		m1.add(src1.re,src1.im);
		s2.add(src2.re,src2.im);
		im.tmul(m1,s2,adde);
		s2.free();
		m1.tmul(src1.re,src2.re);
		m2.tmul(src1.im,src2.im);
		im.addsubsubnr(im,m1,m2);
		if(adde){re.addsubnr(re,m1,m2);}
		else re.sub(m1,m2);
	}
}

//***********************************************************************
void dscmatrix::tmul(const dcmatrix & src1,const dcmatrix & src2,const bool adde){
// (a + ib)(c + id) = (ac - bd) + i[(a + b)(c + d) - ac - bd]
//***********************************************************************
	parhuzamosanfut=true;
	if(STRASSEN_MUL_N_LIMIT()<=dbl(src2.getrow())*src1.getrow()*src1.getcol()*2&&mxOptimize!=mxMinSize){
		switch(mxOptimize){
			case mxMaxSpeed :
			case mxBalanced1 :{
					drmatrix m1,m2;
					dsrmatrix s1,s2,s5;
					m1.add(src1.re,src1.im);
					m2.add(src2.re,src2.im);
					drmtmulszal sz0(m1,m2,adde?s5:im);
					drmtmulszal sz1(src1.re,src2.re,s1);
					drmtmulszal sz2(src1.im,src2.im,s2);
					sz0.start(); sz1.start(); sz2.start();
					sz0.wait();  sz1.wait();  sz2.wait();
					if(adde){im.addnr(im,s5);s5.free();}
					im.addsubsubnr(im,s1,s2);
					if(adde){re.addsubnr(re,s1,s2);}
					else re.sub(s1,s2);
				}
				break;			
			case mxBalanced2 :
			case mxBalanced3 :{
					drmatrix m1,m2;
					m1.add(src1.re,src1.im);
					m2.add(src2.re,src2.im);
					im.tmul(m1,m2,adde);
					m1.free();
					m2.free();
					dsrmatrix s1,s2;
					drmtmulszal sz1(src1.re,src2.re,s1);
					drmtmulszal sz2(src1.im,src2.im,s2);
					sz1.start(); sz2.start();
					sz1.wait();  sz2.wait();
					im.addsubsubnr(im,s1,s2);
					if(adde){re.addsubnr(re,s1,s2);}
					else re.sub(s1,s2);
				}
				break;
			default:throw hiba("","dcmatrix::tmul => mxOptimize==mxMinSize");
		}
	}
	else{
		drmatrix m1,m2;
		m1.add(src1.re,src1.im);
		m2.add(src2.re,src2.im);
		im.tmul(m1,m2,adde);
		m1.free();
		m2.free();
		dsrmatrix s1,s2;
		s1.tmul(src1.re,src2.re);
		s2.tmul(src1.im,src2.im);
		im.addsubsubnr(im,s1,s2);
		if(adde){re.addsubnr(re,s1,s2);}
		else re.sub(s1,s2);
	}
}

//***********************************************************************
void dscmatrix::tmul_vigyazz(const dscmatrix & src1,const dscmatrix & src2,const bool adde){
// (a + ib)(c + id) = (ac - bd) + i[(a + b)(c + d) - ac - bd]
//***********************************************************************
	parhuzamosanfut=true;
	if(STRASSEN_MUL_N_LIMIT()<=dbl(src2.getrow())*src1.getrow()*src1.getcol()*2&&mxOptimize!=mxMinSize){
		switch(mxOptimize){
			case mxMaxSpeed :
			case mxBalanced1 :{
					dsrmatrix s1,s2,s3,s4,s5;
					s1.add(src1.re,src1.im);
					s2.add(src2.re,src2.im);
					drmtmulszal sz0(s1,s2,adde?s5:im);
					drmtmulszal sz1(src1.re,src2.re,s3);
					drmtmulszal sz2(src1.im,src2.im,s4);
					sz0.start(); sz1.start(); sz2.start();
					sz0.wait();  sz1.wait();  sz2.wait();
					if(adde){im.addnr(im,s5);s5.free();}
					im.addsubsubnr(im,s3,s4);
					if(adde){re.addsubnr(re,s3,s4);}
					else re.sub(s3,s4);
				}
				break;			
			case mxBalanced2 :
			case mxBalanced3 :{
					dsrmatrix s1,s2;
					s1.add(src1.re,src1.im);
					s2.add(src2.re,src2.im);
					im.tmul_vigyazz(s1,s2,adde);
					drmtmulszal sz1(src1.re,src2.re,s1);
					drmtmulszal sz2(src1.im,src2.im,s2);
					sz1.start(); sz2.start();
					sz1.wait();  sz2.wait();
					im.addsubsubnr(im,s1,s2);
					if(adde){re.addsubnr(re,s1,s2);}
					else re.sub(s1,s2);
				}
				break;
			default:throw hiba("","dcmatrix::tmul => mxOptimize==mxMinSize");
		}
	}
	else{
		dsrmatrix s1,s2;
		s1.add(src1.re,src1.im);
		s2.add(src2.re,src2.im);
		im.tmul_vigyazz(s1,s2,adde);
		s1.tmul_vigyazz(src1.re,src2.re);
		s2.tmul_vigyazz(src1.im,src2.im);
		im.addsubsubnr(im,s1,s2);
		if(adde){re.addsubnr(re,s1,s2);}
		else re.sub(s1,s2);
	}
}


//***********************************************************************
void dcmatrix::inv(){
//***********************************************************************
/*
	drmatrix q1,q2,q3;
	q1.transp(re);
	q1.inv();
	q2.tmul(im,q1);
	q1.transp(im);
	q3.tmul(q2,q1);
	q1.free();
	re.pluszegyenlo(q3);
	re.inv();
	q3.transp(q2);
	q2.free();
	q3.neg();
	im.tmul(re,q3);
*/
#ifdef vsundebugmode
	if(getrow()!=getcol())throw hiba("","dcmatrix::inv => re.getrow()!=re.getcol() => %lu!=%lu",re.getrow(),re.getcol());
#endif
	cu32 S=re.getrow(),SS=getrow()*getcol();
	dbl * mr=re.gett();
	dbl * mi=im.gett();
	switch(S){
		case 0: return;
		case 1: separate(1.0/dcomplex(mr[0],mi[0]),mr[0],mi[0]);return;
		case 2: {
					dbl * const pr=mr;
					dbl * const pi=mi;
					cdc det=1.0/(dcomplex(pr[0],pi[0])*dcomplex(pr[3],pi[3])-dcomplex(pr[1],pi[1])*dcomplex(pr[2],pi[2]));
					cdc ndet=-det,m3=dcomplex(pr[0],pi[0])*det;
					separate(dcomplex(pr[3],pi[3])*det,pr[0],pi[0]);
					separate(dcomplex(pr[1],pi[1])*ndet,pr[1],pi[1]);
					separate(dcomplex(pr[2],pi[2])*ndet,pr[2],pi[2]);
					separate(m3,pr[3],pi[3]);
					return;
				}
	}

	if((mxOptimize!=mxMinSize) && ((getrow()&1)==0) && (STRASSEN_INV_N_LIMIT()<=dbl(getrow())*getrow()*getrow())){
		strasseninv();
		return;
	}

	u32 i,j,I,k;
	i32 dum[2*NEWKIVALTO],*x=S>NEWKIVALTO?new i32[2*S]:dum,*y=x+S;
	dbl * const Nr=mr;
	dbl * const Ni=mi;
	dbl *Or,*Oi;
	
	for(i=0;i<S;i++)x[i]=y[i]=-1;
	for(i=0,I=0,Or=Nr,Oi=Ni;i<S;i++,I+=S,Or+=S,Oi+=S){
		// Pivot kiv�laszt�sa
		
		dbl diff=0.0;
		i32 V=-1;
		u32 j,J;
		for(j=0;j<S;j++)if(x[j]==-1){
			dbl temp=fabs(Or[j])+fabs(Oi[j]);
			if(temp>diff){diff=temp;V=j;}//v-edik oszlopot v�lasztjuk
		}

		if((V==-1)||(diff==0))throw hiba("","drmatrix::inv => singular matrix");
		cu32 V2=u32(V);
		x[V2]=i;y[i]=V;

		// Elemcsere

		cdc A=1/dcomplex(Or[V2],Oi[V2]);;

		for(J=0;J<I;J+=S){
			cdc C=-dcomplex(Nr[J+V2],Ni[J+V2])*A;
			dbl *pr1=Nr+J,*pr2=Or;
			dbl *pi1=Ni+J,*pi2=Oi;
			for(u32 kl=0;kl<S;kl++)separate(dcomplex(pr1[kl],pi1[kl])+C*dcomplex(pr2[kl],pi2[kl]),pr1[kl],pi1[kl]);
			separate(C,Nr[J+V2],Ni[J+V2]);
		}
		for(J+=S;J<SS;J+=S){
			cdc C=-dcomplex(Nr[J+V2],Ni[J+V2])*A;
			dbl *pr1=Nr+J,*pr2=Or;
			dbl *pi1=Ni+J,*pi2=Oi;
			for(u32 kl=0;kl<S;kl++)separate(dcomplex(pr1[kl],pi1[kl])+C*dcomplex(pr2[kl],pi2[kl]),pr1[kl],pi1[kl]);
			separate(C,Nr[J+V2],Ni[J+V2]);
		}
		for(j=0;j<S;j++)separate(dcomplex(Or[j],Oi[j])*A,Or[j],Oi[j]);
		separate(A,Or[V2],Oi[V2]);
	}

#ifdef vsundebugmode
	for(i=0;i<S;i++)if(x[i]<0)throw hiba("","drmatrix::inv => -1 in x[]");
	for(i=0;i<S;i++)if(y[i]<0)throw hiba("","drmatrix::inv => -1 in y[]");
#endif

	// Sorok �s oszlopok sorrendbe rak�sa

	for(i=0;i<S;i++){
		for(j=i;y[j]!=i;j++);
		if(i!=j){
			dbl *cSi=Nr+i*S,*cSj=Nr+j*S;
			for(k=0;k<S;k++){cd temp=cSi[k];cSi[k]=cSj[k];cSj[k]=temp;}
			cSi=Ni+i*S; cSj=Ni+j*S;
			for(k=0;k<S;k++){cd temp=cSi[k];cSi[k]=cSj[k];cSj[k]=temp;}
			y[j]=y[i];
		}
	}
	for(i=0;i<S;i++){
		for(j=i;x[j]!=i;j++);
		if(i!=j){
			dbl *cSi=Nr+i,*cSj=Nr+j;
			for(k=0;k<S;k++,cSi+=S,cSj+=S){cd temp=*cSi;*cSi=*cSj;*cSj=temp;}
			cSi=Ni+i; cSj=Ni+j;
			for(k=0;k<S;k++,cSi+=S,cSj+=S){cd temp=*cSi;*cSi=*cSj;*cSj=temp;}
			x[j]=x[i];
		}
	}
	
	if(S>NEWKIVALTO)delete [] x;
}

//***********************************************************************
void dcmatrix::strasseninv(){
//***********************************************************************
	cu32 row=getrow(),col=getcol();
#ifdef vsundebugmode
	if(col!=row)throw hiba("","dcmatrix::strasseninv => col!=row => %lu!=%lu",getcol(),row);
	if(col&1)throw hiba("","dcmatrix::strasseninv => col=%lu => paratlan oldalhosz",col);
#endif
	parhuzamosanfut=true;
	dcmatrix r1,r2,r3,r4,r6,c12,c21,m1,m2; // 7 db, ebb�l egyszerre max 6 van haszn�latban (1,5 m�trix m�ret)
	r1.getsubmatrix(*this,0,0,row/2,col/2);//a11
	r1.inv();

	c12.getsubmatrix(*this,0,col/2,row/2,col/2);//a12
	m1.transp(c12);
	c12.free();
	dcmtmulszal mr3(r1,m1,r3);
	mr3.start();

	c21.getsubmatrix(*this,row/2,0,row/2,col/2);//a21
	m2.transp(r1);
	dcmtmulszal mr2(c21,m2,r2);
	mr2.start();

	mr3.wait();

	m1.transp(r3);
	dcmtmulszal mr4(c21,m1,r4);//r4
	mr4.start();

	mr2.wait();
	mr4.wait();

	c21.free();
	m1.free();
	m2.free();

	r6.getsubmatrix(*this,row/2,col/2,row/2,col/2);//a22
	r6.subnr(r4,r6);

	r4.free();

	r6.inv();

	m1.transp(r2);
	r2.free();
	dcmtmulszal mc21(r6,m1,c21);
	mc21.start();

	m2.transp(r6);
	dcmtmulszal mc12(r3,m2,c12);
	mc12.start();

	setsubmatrixneg(r6,row/2,col/2); // c22 be�r�sa

	mc21.wait();

	m1.transp(c21);
	dcmtmulszal mr7(r3,m1,r2);// r2 lesz r7
	mr7.start();

	setsubmatrix(c21,row/2,0); // c21 be�r�sa

	mc12.wait();

	m2.free();
	setsubmatrix(c12,0,col/2); // c12 be�r�sa

	mr7.wait();

	c12.sub(r1,r2); // r1-r7
	setsubmatrix(c12,0,0); // c11 be�r�sa
}


#endif
