//***********************************************************************
// apa class source
// Creation date:  2009. 07. 11.
// Creator:        Pohl L�szl�
//***********************************************************************


//***********************************************************************
#include "apa.h"
#include "gfunc.h"
#include "masina.h"
//***********************************************************************

//***********************************************************************
uns semiconductor::db=0;
//***********************************************************************
// az extern csom�pontok reduk�l�s�hoz haszn�lt admittanciam�trixok
// extern_y_reduce �ll�tja
dsrmatrix dr_y_extern;
dscmatrix dc_y_extern;
qsrmatrix qr_y_extern;
qscmatrix qc_y_extern;
// az extern csom�pontok reduk�l�s�hoz haszn�lt inhomog�n �ram vektorok
// extern_j_reduce �ll�tja
drvector  dr_j_extern;
dcvector  dc_j_extern;
qrvector  qr_j_extern;
qcvector  qc_j_extern;
// az extern �s compact model �sszerak�s�val kapott u vektorok
// extern_solve_u �ll�tja
drvector  dr_u_extcom;
dcvector  dc_u_extcom;
qrvector  qr_u_extcom;
qcvector  qc_u_extcom;
// az extern �s compact model �sszerak�s�val kapott i vektorok
// extern_solve_u �ll�tja
drvector  dr_i_extcom;
dcvector  dc_i_extcom;
qrvector  qr_i_extcom;
qcvector  qc_i_extcom;
// a compact model csom�pontokhoz tartoz� admittanciam�trixok
// extern_solve_u haszn�lja, ki �ll�tja ?
dsrmatrix dr_y_compmod;
dscmatrix dc_y_compmod;
qsrmatrix qr_y_compmod;
qscmatrix qc_y_compmod;
// a compact model csom�pontokhoz tartoz� inhomog�n �ram vektorok
// extern_solve_u haszn�lja, ki �ll�tja ?
drvector  dr_j_compmod;
dcvector  dc_j_compmod;
qrvector  qr_j_compmod;
qcvector  qc_j_compmod;
//***********************************************************************


//***********************************************************************
apa::apa(const char * ProjectFile):projFile(ProjectFile){
//***********************************************************************
    path=getPath(projFile);
    srfajl fajl;
    resetAnalLevel(4);
    logprint("Open project: %s",ProjectFile);
    fajl.open(projFile);
    if(fajl.lines().size()<1||fajl.lines()[0][0].LowCase()!="vsun3-project")open3d();
    else{
        
        // modellf�jlok nevei
        
        uns db=0,m=0;
        for(uns i=1;i<fajl.lines().size();i++)if(fajl.lines()[i][0].LowCase()=="model")db++;
        if(!db)throw hiba("apa::apa()","no models are defined in %s",ProjectFile);
        tmodels.clear();
        tmodels.resize(db);
        db=0;
        for(uns i=1;i<fajl.lines().size();i++)if(fajl.lines()[i][0].LowCase()=="model"){
            tmodels[db].fileName=fajl.lines()[i][1];
            tmodels[db].simdb=0;
            db++;
        }
        
        // szimul�ci�s f�jlok nevei
        
        db=0;
        for(uns i=1;i<fajl.lines().size();i++)if(fajl.lines()[i][0].LowCase()=="simulation")db++;
        if(!db)throw hiba("apa::apa()","no simulations are defined in %s",ProjectFile);
        tsim.clear();
        tsim.resize(db);
        db=0;
        for(uns i=1;i<fajl.lines().size();i++)if(fajl.lines()[i][0].LowCase()=="simulation"){
            if(m==0)throw hiba("apa::apa()","simulation before model in %s",ProjectFile);
            tsim[db].fileName=fajl.lines()[i][1];
            tmodels[m-1].simdb++;
            tsim[db].pmodel=&tmodels[m-1];
            db++;
        }
        else if(fajl.lines()[i][0].LowCase()=="model")m++;

        // model f�jlok olvas�sa

        for(uns i=0;i<tmodels.size();i++)tmodels[i].read(path);

        // szimul�ci�s f�jlok olvas�sa

        for(uns i=0;i<tsim.size();i++)tsim[i].read(path);
        uns sumdb=0;
        for(uns i=0;i<tsim.size();i++){ // h�ny szimul�ci� van, a t�bbsz�r�s gerjeszt�seket figyelembe v�ve
            uns sim_darab=1,vane=0;
            for(uns j=0;j<4;j++)tsim[i].index_temp[j]=colmax;
            for(uns j=0;j<colmax;j++){
                if(tsim[i].mulE[j].size()>0){
                    vane++;
                    sim_darab*=tsim[i].mulE[j].size();
                    if(tsim[i].index_temp[0]==colmax)tsim[i].index_temp[0]=j;
                    else tsim[i].index_temp[1]=j;
                }
                if(tsim[i].mulT[j].size()>0){
                    vane++;
                    sim_darab*=tsim[i].mulT[j].size();
                    if(tsim[i].index_temp[2]==colmax)tsim[i].index_temp[2]=j;
                    else tsim[i].index_temp[3]=j;
                }
                if(vane>2)throw hiba("apa::apa()","maximum 2 multiple excitation is allowed in %s",ProjectFile);
            }
            if(vane){ sumdb+=sim_darab; tsim[i].db_temp=sim_darab; }
            else { sumdb++; tsim[i].db_temp=1; }
        }
        tomb<simulation> sim_temp;
        sim_temp.resize(sumdb);
        uns j=0;
        for(uns i=0;i<tsim.size();i++){// t�bbsz�r�s szimul�ci�k l�trehoz�sa
            cuns db=tsim[i].db_temp;
            cuns * ix=tsim[i].index_temp;
            for(uns k=0;k<db;k++)sim_temp[j+k]=tsim[i];
            if(db>1){ // van t�bbsz�r�s
                if(ix[0]!=colmax){
                    if(tsim[i].mulE[ix[0]].size()==db){ // egy elektromos gerjeszt�s �rt�ke v�ltozik
                        for(uns k=0;k<db;k++){
                            sim_temp[j+k].texcitE[ix[0]]=tsim[i].mulE[ix[0]][k];
                            char s[100];
                            sprintf(s,"_%g",sim_temp[j+k].texcitE[ix[0]].ertek);
                            sim_temp[j+k].name+=s;
                        }
                    }
                    else if(ix[1]!=colmax){ // k�t elektomos multi gerjeszt�s van
                        uns db1=tsim[i].mulE[ix[0]].size();
                        uns db2=tsim[i].mulE[ix[1]].size();
                        if(db1*db2!=db)throw hiba("apa::apa()","Impossibility happend, program error. (db)");
                        for(uns k=0;k<db1;k++)
                            for(uns L=0;L<db2;L++){
                                sim_temp[j+k*db2+L].texcitE[ix[0]]=tsim[i].mulE[ix[0]][k];
                                sim_temp[j+k*db2+L].texcitE[ix[1]]=tsim[i].mulE[ix[1]][L];
                                char s[100];
                                sprintf(s,"_%g_%g",tsim[i].mulE[ix[0]][k].ertek,tsim[i].mulE[ix[1]][L].ertek);
                                sim_temp[j+k*db2+L].name+=s;
                            }
                    }
                    else if(ix[2]!=colmax){ // vegyes gerjeszt�s van
                        uns db1=tsim[i].mulE[ix[0]].size();
                        uns db2=tsim[i].mulT[ix[2]].size();
                        if(db1*db2!=db)throw hiba("apa::apa()","Impossibility happend, program error. (db)");
                        for(uns k=0;k<db1;k++)
                            for(uns L=0;L<db2;L++){
                                sim_temp[j+k*db2+L].texcitE[ix[0]]=tsim[i].mulE[ix[0]][k];
                                sim_temp[j+k*db2+L].texcitT[ix[2]]=tsim[i].mulT[ix[2]][L];
                                char s[100];
                                sprintf(s,"_%g_%g",tsim[i].mulE[ix[0]][k].ertek,tsim[i].mulT[ix[2]][L].ertek);
                                sim_temp[j+k*db2+L].name+=s;
                            }
                    }
                    else throw hiba("apa::apa()","Impossibility happend, program error. (ix2)");
               }
                else if(ix[2]!=colmax){ // csak termikus gerj van
                    if(tsim[i].mulT[ix[2]].size()==db){ // egy termikus gerjeszt�s �rt�ke v�ltozik
                        for(uns k=0;k<db;k++){
                            sim_temp[j+k].texcitT[ix[2]]=tsim[i].mulT[ix[2]][k];
                            char s[100];
                            sprintf(s,"_%g",sim_temp[j+k].texcitT[ix[0]].ertek);
                            sim_temp[j+k].name+=s;
                        }
                    }
                    else if(ix[3]!=colmax){ // k�t termikus multi gerjeszt�s van
                        uns db1=tsim[i].mulT[ix[2]].size();
                        uns db2=tsim[i].mulT[ix[3]].size();
                        if(db1*db2!=db)throw hiba("apa::apa()","Impossibility happend, program error. (db)");
                        for(uns k=0;k<db1;k++)
                            for(uns L=0;L<db2;L++){
                                sim_temp[j+k*db2+L].texcitT[ix[2]]=tsim[i].mulT[ix[2]][k];
                                sim_temp[j+k*db2+L].texcitT[ix[3]]=tsim[i].mulT[ix[3]][L];
                                char s[100];
                                sprintf(s,"_%g_%g",tsim[i].mulT[ix[2]][k].ertek,tsim[i].mulT[ix[3]][L].ertek);
                                sim_temp[j+k*db2+L].name+=s;
                            }
                    }
                    else throw hiba("apa::apa()","Impossibility happend, program error. (ix3)");
                }
                else throw hiba("apa::apa()","Impossibility happend, program error. (nix)");
            }
            j+=db;
        }
        tsim=sim_temp;
        sumdb=tsim.size();
        for(uns i=0;i<tsim.size();i++) // t�bb ambient h�m�rs�klet van-e?
            sumdb += tsim[i].mulAmbiT.size();
        if(sumdb!=tsim.size()){
            sim_temp.resize(sumdb);
            j=0;
            for(uns i=0;i<tsim.size();i++){// t�bbsz�r�s szimul�ci�k l�trehoz�sa k�l�nb�z� ambient h�m�rs�kletre
                cuns db=tsim[i].mulAmbiT.size()+1;
                for(uns k=0;k<db;k++){
                    sim_temp[j+k]=tsim[i];
                    if(db>1){
                        char s[100];
                        if( k == 0 )
                            sprintf(s,"_%gC",sim_temp[j+k].ambiT);
                        else 
                            sprintf(s,"_%gC",sim_temp[j+k].mulAmbiT[k-1]);
                        sim_temp[j+k].name+=s;
                        if( k > 0 )
                            sim_temp[j+k].ambiT=sim_temp[j+k].mulAmbiT[k-1];
                    }
                }
                j+=db;
            }
        }
        tsim=sim_temp;
//printf("\ndb=%u, ix[0]=%u, ix[1]=%u, ix[2]=%u, ix[3]=%u\n",db,ix[0],ix[1],ix[2],ix[3]);
//printf("\ndb1=%u, db2=%u, db=%u\n",db1,db2,db);

        // setProgressAnaldb() be�ll�t�sa
    }
    logprint("Opening done");
}


//***********************************************************************
void apa::open3d(){
//***********************************************************************
    logprint("Open S3D project in %s",path.c_str());
    throw hiba("apa::open3d","S3D projects are not supported yet");
}


//***********************************************************************
enum sorazon{az_unknown,az_nev,az_dim,az_xres,az_yres,az_zres,az_coor,
             az_rmin,az_rmax,az_xpit,az_ypit,az_zpit,az_bmp,az_mater,
             az_mat,az_color,az_col,az_semicon,az_semi,az_coupled,
             az_end,az_field,az_lin,az_nosemi,az_noseebeck,az_nopeltier,
             az_peltier_center,az_nothomson,az_no_joule,az_noaccelerator,
             az_el_nonlin_subiter,az_comp,az_always_strassen_mul,az_cpu,
             az_hdd,az_bound,az_optimize,az_bou,az_excita,az_fimtxt,
             az_commas,az_nofim,az_ambem,az_excit,az_ambient,az_powermap,
             az_uchannel,az_no_plus_step_data,
             az_probe,az_pro,az_analy,az_convection,az_conv,az_extnods,
             az_footpr,az_admmx,az_inhom,az_cm_temp,az_always_quad,
             az_timestep,az_timelimit,az_change_time,az_electrical,
             az_thermal,az_ndc_miniter,az_FIM_res,az_FIM_diff,
             az_auto_transi_steps_V,az_auto_transi_steps_T,az_auto_transi_steps};
//***********************************************************************


//***********************************************************************
sorazon azonosit(const PLString & szo){
// szo csupa kisbet�s kell legyen
//***********************************************************************
    if(szo=="name")return az_nev;
    if(szo=="dimension")return az_dim;
    if(szo=="x-resolution")return az_xres;
    if(szo=="y-resolution")return az_yres;
    if(szo=="z-resolution")return az_zres;
    if(szo=="coordinates")return az_coor;
    if(szo=="r-min")return az_rmin;
    if(szo=="r-max")return az_rmax;
    if(szo=="x-pitch")return az_xpit;
    if(szo=="y-pitch")return az_ypit;
    if(szo=="z-pitch")return az_zpit;
    if(szo=="bitmap")return az_bmp;
    if(szo=="ambient-emissivity")return az_ambem;
    if(szo=="material")return az_mater;
    if(szo=="mat")return az_mat;
    if(szo=="color")return az_color;
    if(szo=="col")return az_col;
    if(szo=="semiconductor")return az_semicon;
    if(szo=="semi")return az_semi;
    if(szo=="coupled_model")return az_coupled;
    if(szo=="end")return az_end;
    if(szo=="field")return az_field;
    if(szo=="linear")return az_lin;
    if(szo=="no_semicond")return az_nosemi;
    if(szo=="no_seebeck")return az_noseebeck;
    if(szo=="no_peltier")return az_nopeltier;
    if(szo=="peltier_center")return az_peltier_center;
    if(szo=="no_thomson")return az_nothomson;
    if(szo=="no_joule")return az_no_joule;
    if(szo=="no_accelerator")return az_noaccelerator;
    if(szo=="cm_temperature")return az_cm_temp;
    if (szo == "el_nonlin_subiter")return az_el_nonlin_subiter;
    if (szo == "ndc_miniter")return az_ndc_miniter;
    if(szo=="computation")return az_comp;
    if(szo=="always_quad")return az_always_quad;
    if(szo=="always_strassen_mul")return az_always_strassen_mul;
    if(szo=="cpu-threads")return az_cpu;
    if(szo=="hdd-cache")return az_hdd;
    if(szo=="optimize")return az_optimize;
    if(szo=="convection")return az_convection;
    if(szo=="conv")return az_conv;
    if(szo=="boundary")return az_bound;
    if(szo=="bou")return az_bou;
    if(szo=="excitation")return az_excita;
    if(szo=="excit")return az_excit;
    if(szo=="ambient")return az_ambient;
    if(szo=="ambient-temperature")return az_ambient;
    if (szo == "powermap")return az_powermap;
    if (szo == "uchannel")return az_uchannel;
    if(szo == "probe")return az_probe;
    if(szo == "map_to_txt")return az_fimtxt;
    if(szo=="no_images")return az_nofim;
    if (szo == "use_commas")return az_commas;
    if (szo == "fim_res")return az_FIM_res;
    if (szo == "fim_diff")return az_FIM_diff;
    if(szo=="pro")return az_pro;
    if (szo == "no_plus_step_data")return az_no_plus_step_data;
    if (szo == "auto_transi_steps_v")return az_auto_transi_steps_V;
    if (szo == "auto_transi_steps_t")return az_auto_transi_steps_T;
    if (szo == "auto_transi_steps")return az_auto_transi_steps;
    if(szo=="analysis")return az_analy;
    if(szo=="external_nodes")return az_extnods;
    if(szo=="footprint")return az_footpr;
    if(szo=="adm")return az_admmx;
    if(szo=="inhom")return az_inhom;
    if(szo=="timestep")return az_timestep;
    if(szo=="timelimit")return az_timelimit;
    if(szo=="change_time")return az_change_time;
    if(szo=="electrical")return az_electrical;
    if(szo=="thermal")return az_thermal;
    if(szo!="")printf("# unknown parameter is ignored: %s\n",szo.c_str());
    return az_unknown;
}


//***********************************************************************
enum matazon{maz_unknown,maz_heatcond,maz_heatres,maz_heatcap,maz_conduct,
             maz_resistivity,maz_capac,maz_seebeck,maz_disscoeff,
             maz_emissivity,maz_th_transmissivity};
//***********************************************************************


//***********************************************************************
matazon azon_mat(const PLString & szo){
// szo csupa kisbet�s kell legyen
//***********************************************************************
    if(szo=="heatcond")return maz_heatcond;
    if(szo=="heatres")return maz_heatres;
    if(szo=="heatcap")return maz_heatcap;
    if(szo=="conduct")return maz_conduct;
    if(szo=="resistivity")return maz_resistivity;
    if(szo=="capacity")return maz_capac;
    if(szo=="seebeck")return maz_seebeck;
    if(szo=="dissip_coeff")return maz_disscoeff;
    if(szo=="emissivity")return maz_emissivity;
    if(szo=="thermal-transmissivity")return maz_th_transmissivity;
    if(szo!="")printf("# unknown meterial parameter is ignored: %s\n",szo.c_str());
    return maz_unknown;
}


//***********************************************************************
enum convazon{caz_unknown,caz_rad,caz_vertical,caz_upper,caz_lower,
              caz_axis,caz_angle,caz_edge};
//***********************************************************************


//***********************************************************************
convazon azon_conv(const PLString & szo){
// szo csupa kisbet�s kell legyen
//***********************************************************************
    if(szo=="radiation")return caz_rad;
    if(szo=="free-vertical")return caz_vertical;
    if(szo=="free-upper")return caz_upper;
    if(szo=="free-lower")return caz_lower;
    if(szo=="axis")return caz_axis;
    if(szo=="angle")return caz_angle;
    if(szo=="edge")return caz_edge;
    if(szo!="")printf("# unknown convection parameter is ignored: %s\n",szo.c_str());
    return caz_unknown;
}


//***********************************************************************
bool pitchconverter(const tomb<PLString> & be,tomb<z_a_hengerhez> & ki,tomb<dbl> & hossz){
// be[1]-t�l megy, nem 0-t�l!
//***********************************************************************
//    uns j=0;
    ki.clear();
    for(uns i=1;i<be.size();i++){
        int n=be[i].find('*');
        z_a_hengerhez d;
        if(!be[i].todouble(d.ertek))return false;
        if(n!=npos){
            if(!be[i].toint(n,n+1))return false;
            ki.add(d,n);
        }
        else ki.add(d);
    }
    hossz.clear();
    hossz.add(ki[0].ertek*0.5);
    hossz.add(ki[0].ertek);
    for(u32 i=1;i<ki.size();i++){
        hossz.add(hossz.getLast()+ki[i].ertek*0.5);
        hossz.add(hossz.getLast()+ki[i].ertek*0.5); // ez m�r az el�z� sorban kapotthoz adja
    }
    return true;
}


//***********************************************************************
bool pitchconverter(const tomb<PLString> & be, tomb<dbl> & ki) {
// be[1]-t�l megy, nem 0-t�l!
//***********************************************************************
//    uns j=0;
    ki.clear();
    for (uns i = 1;i<be.size();i++) {
        int n = be[i].find('*');
        dbl d;
        if (!be[i].todouble(d))return false;
        if (n != npos) {
            if (!be[i].toint(n, n + 1))return false;
            ki.add(d, n);
        }
        else ki.add(d);
    }
    return true;
}


//***********************************************************************
double interpolalo(tomb2d<dbl> &t, uns x_res, uns y_res, dbl x, dbl y){
//***********************************************************************
    double uj_x = x * ( t.x_size() - 1 ) / ( x_res - 1) ;
    double uj_y = y * ( t.y_size() - 1 ) / ( y_res - 1) ;
    uns also_x = (uns)uj_x;
    uns also_y = (uns)uj_y;
    uns felso_x = ( also_x >= t.x_size() - 1 ) ? also_x : also_x + 1;
    uns felso_y = ( also_y >= t.y_size() - 1 ) ? also_y : also_y + 1;
    double arany_x = uj_x - also_x;
    double arany_y = uj_y - also_y;
    return (1.0 - arany_x) * (1.0 - arany_y) * t.get( also_x,  also_y  )+
           (      arany_x) * (1.0 - arany_y) * t.get( felso_x, also_y  )+
           (1.0 - arany_x) * (      arany_y) * t.get( also_x,  felso_y )+
           (      arany_x) * (      arany_y) * t.get( felso_x, felso_y );
}


//***********************************************************************
bool konv_map_konverter(const PLString fajlnev, tomb2d<dbl> & cel, uns x_res, uns y_res){
// Az x_res �s y_res a c�l k�v�nt m�rete, ekkor�ra sk�l�zza fel a beolvasott mapot
//***********************************************************************
    const char * fvnev="konv_map_konverter()";
    srfajl fajl;
    fajl.open(fajlnev);
    if(fajl.lines().size()<1||fajl.lines()[0][0].LowCase()!="vsun3-convection-map")
        throw hiba(fvnev,"first line is not vsun3-convection-map in %s",fajlnev.c_str());
    if(fajl.lines()[1].size()<2)throw hiba(fvnev,"incomplete line 2 in file: %s",fajlnev.c_str());
    if(fajl.lines()[2].size()<2)throw hiba(fvnev,"incomplete line 3 in file: %s",fajlnev.c_str());
    if(fajl.lines()[1][0].LowCase()!="x-size")throw hiba(fvnev,"line 2 must be \"X-SIZE=<number>\" in %s",fajlnev.c_str());
    if(fajl.lines()[2][0].LowCase()!="y-size")throw hiba(fvnev,"line 3 must be \"Y-SIZE=<number>\" in %s",fajlnev.c_str());
    uns x,y;
    if(!fajl.lines()[1][1].tounsigned(x))throw hiba(fvnev,"X-SIZE is not a number (%s) in %s",fajl.lines()[1][1].c_str(),fajlnev.c_str());
    if(!fajl.lines()[2][1].tounsigned(y))throw hiba(fvnev,"Y-SIZE is not a number (%s) in %s",fajl.lines()[2][1].c_str(),fajlnev.c_str());

    cu32 n=x*y;
    tomb<dbl> t;
    t.clear();
    for(uns i=3; t.size()<n; i++){
        if(fajl.lines().size()<=i)
            throw hiba(fvnev,"unexpected end of file in %s",fajlnev.c_str());
        if(fajl.lines()[i][0].LowCase()=="end")
            throw hiba(fvnev,"less convection value than X-SIZE*Y-SIZE (%u<%u) in %s",t.size(),n,fajlnev.c_str());
        for(uns j=0; j<fajl.lines()[i].size() && t.size()<n; j++){
            int m=fajl.lines()[i][j].find('*');
            dbl d;
            if(!fajl.lines()[i][j].todouble(d))
                throw hiba("konv_map_konverter","cannot be converted to double (%s) in %s",fajl.lines()[i][j].c_str(),fajlnev.c_str());
            if(m!=npos){
                if(!fajl.lines()[i][j].toint(m,m+1))
                    throw hiba("konv_map_konverter","cannot be converted to int (%s) in %s",fajl.lines()[i][j].c_str(),fajlnev.c_str());
                t.add(d,m);
            }
            else t.add(d);
        }
    }

    tomb2d<dbl> t_2d;
    t_2d.free();
    t_2d.resize(x,y);
    uns k=0;
    for(uns i=0; i<y; i++)
        for(uns j=0; j<x; j++,k++)
            t_2d.getref(j,i)=t[k];

    cel.free();
    cel.resize(x_res,y_res);
    for(uns i=0; i<y_res; i++)
        for(uns j=0; j<x_res; j++){
            double d=interpolalo(t_2d,x_res,y_res,j,y_res-i-1)*2.1839; // !!!!!! Fejjel lefel� �s szorozva egy konstanssal! Ha rendes bemenetet kap, ezt kivenni!
            cel.getref(j,i)=d;
        }

    return true;
}


//***********************************************************************
uns function_azonosito(const PLString &nev, monlintipus &tipus) {
// a n�v alapj�n be�ll�tja a t�pust �s visszaadja a beolvasand� val�s 
// �rt�kek sz�m�t, melyek a gg-be ker�lnek
//***********************************************************************
    if (nev == "mizs1") {
        tipus = nlt_mizs1;
        return 7;
    }
    else
        throw hiba("function_azonosito", "unknown material function name (%s)", nev.c_str());
    return 0;
}


//***********************************************************************
bool R_converter(const tomb<PLString> & sor,u32 startindex,vezetes & dest,const material & m,bool is_diode=false){
//***********************************************************************
    const PLString s=sor[startindex].LowCase();
    if (s == "function") {
        cuns db = function_azonosito(sor[startindex + 1], dest.tipus);
        if (db > GGSIZE)
            throw("R_converter", "program error: function parameter number>GGSIZE, GGSIZE increase is required");
        for (uns i = 0;i < db; i++)
            if (!sor[startindex + 2 + i].todouble(dest.gg[i]))
                return false;
        dest.g[0] = dest.g[1] = dest.g[2] = fn_mizs1(25.0, dest.gg[0], dest.gg[1], dest.gg[2], dest.gg[3], dest.gg[4], dest.gg[5], dest.gg[6]);
        dest.T = nulla;
    }
    else if(s[0]!='['){
        int y=s.find('|');
        int z=(y==npos)?npos:s.find('|',y+1);
        int g=s.find('#');
        int g2=(g==npos)?npos:s.find('|',g+1);
        int g3=(g2==npos)?npos:s.find('|',g2+1);
        if(g!=npos){
            if(y!=npos&&y>g)y=npos;
            if(z!=npos&&z>g)z=npos;
        }

        if(!s.todouble(dest.g[0]))return false;
        dest.g[2]=dest.g[1]=dest.g[0];
        dest.T=nulla;

        if(y!=npos)if(!s.todouble(dest.g[1],y+1))return false;
        if(z!=npos)if(!s.todouble(dest.g[2],z+1))return false;
        if(g!=npos){if(!s.todouble(dest.gg[0],g+1))return false;}
        else dest.gg[0]=nulla;
        if(g2!=npos)if(!s.todouble(dest.gg[1],g2+1))return false;
        if(g3!=npos)if(!s.todouble(dest.gg[2],g3+1))return false;
    
        if(g==npos)dest.tipus=nlt_lin;
        else if(g2==npos)dest.tipus=nlt_exp;
        else if(g3==npos)dest.tipus=nlt_diode;
        else dest.tipus = nlt_quadratic;

        if(y==npos)dest.semitip=nlt_exp;
        else if(z==npos){
            dest.semitip=nlt_diode;
            dest.g[2]=0.1;
        }
        else{
            if(is_diode){
                dest.semitip=nlt_diode;
            }
            else dest.semitip=nlt_quadratic;
        }
    }
    else{ // poligonnal megadott param�ter eset�n
        dest.tipus = nlt_szakaszok;
        while(startindex<sor.size()-1){
            if(sor[startindex][0]!='[')return false;
            u32 poliindex=dest.szakaszok.size();
            dest.szakaszok.resize(dest.szakaszok.size()+1);

            // '[' �tugr�sa �s T beolvas�sa

            bool normal=true;
            if(m.is_his){
                bool f=true;
                if(sor[startindex].LowCase()=="[f" || sor[startindex+1].LowCase()=="f")
                    normal=false;
                else if(sor[startindex].LowCase()=="[h" || sor[startindex+1].LowCase()=="h"){
                    normal=false;
                    f=false;
                }
                if(!normal){
                    if(sor[startindex]=="[")
                        startindex++;
                    startindex++;
                    if(!sor[startindex].todouble(dest.his_value_1))return false;
                    if(sor[startindex][sor[startindex].Length()-1]!=']'&& sor[startindex+1]!="]"){
                        startindex++;
                        if(!sor[startindex].todouble(dest.his_value_2))return false;
                    }
                    startindex++;
                    if(startindex<sor.size() && sor[startindex]=="]")startindex++; // ha nem volt sz�k�z, akkor automatikusan �tugrotta
                    if(dest.his_value_2==nulla)
                        dest.szakaszok.resize(dest.szakaszok.size()-1);
                    else{
                        dest.szakaszok[poliindex].T = m.his_T_min-m.his_T_width_fele;
                        dest.szakaszok[poliindex].G[0] = dest.szakaszok[poliindex].G[1] =
                            dest.szakaszok[poliindex].G[2] = dest.his_value_1;
                        dest.szakaszok.resize(dest.szakaszok.size()+1);
                        poliindex++;
                        dest.szakaszok[poliindex].T = m.his_T_max+m.his_T_width_fele;
                        dest.szakaszok[poliindex].G[0] = dest.szakaszok[poliindex].G[1] =
                            dest.szakaszok[poliindex].G[2] = dest.his_value_2;
                    }
                }
            }
            if(normal){
                if(sor[startindex]=="["){// sz�k�z volt a [ ut�n
                    startindex++;
                    if(!sor[startindex].todouble(dest.szakaszok[poliindex].T))return false;
                }
                else{ // a [ k�zvetlen�l a sz�m el�tt van
                    if(!sor[startindex].todouble(dest.szakaszok[poliindex].T,1))return false;
                }

                // G beolvas�sa �s ] �tugr�sa

                startindex++;
                if(startindex>=sor.size())return false;
                int y=sor[startindex].find('|');
                int z=(y==npos)?npos:sor[startindex].find('|',y+1);

                if(!sor[startindex].todouble(dest.szakaszok[poliindex].G[0]))return false;
                dest.szakaszok[poliindex].G[2] = dest.szakaszok[poliindex].G[1] = dest.szakaszok[poliindex].G[0];

                if(y!=npos)if(!sor[startindex].todouble(dest.szakaszok[poliindex].G[1],y+1))return false;
                if(z!=npos)if(!sor[startindex].todouble(dest.szakaszok[poliindex].G[2],z+1))return false;

                startindex++;
                if(startindex<sor.size() && sor[startindex]=="]")startindex++; // ha nem volt sz�k�z, akkor automatikusan �tugrotta
            }
        }
    }

    return true;
}


//***********************************************************************
bool Erno_converter(const PLString & fajlnev,vezetes & dest){
// a f�jlban erno modell van
//***********************************************************************
    const char * fvnev="Erno_converter()";
    dbl A=1.0;
    uns index=0;
    srfajl fajl;
    fajl.open(fajlnev);
    if(fajl.lines().size()<1||fajl.lines()[0][0].LowCase()!="vsun3-ernomodel")
        throw hiba(fvnev,"first line is not vsun3-ernomodel in %s",fajlnev.c_str());
    double d,m[3][3],b[3][3];
    if(fajl.lines().size()<5)throw hiba(fvnev,"incomplete file: %s",fajlnev.c_str());
    if(fajl.lines()[1].size()<2)throw hiba(fvnev,"incomplete line 2 in file: %s",fajlnev.c_str());
    if(fajl.lines()[2].size()<1)throw hiba(fvnev,"incomplete line 3 in file: %s",fajlnev.c_str());
    if(fajl.lines()[2][0].LowCase()=="specific"){ // fajlagos (1 m2-re vonatkoz�)
        dest.specific=true;
        index++;
    }
    if(fajl.lines()[2+index][0].LowCase()=="a"){ // van ar�nyszorz�
        if(!fajl.lines()[2+index][1].todouble(d))throw hiba(fvnev,"line 3 must be \"A=<number>\", not a number in %s",fajlnev.c_str());
        A=d;
        index++;
    }
    if(fajl.lines()[2+index].size()<10)throw hiba(fvnev,"incomplete line %u in file: %s",3+index,fajlnev.c_str());
    if(fajl.lines()[3+index].size()<10)throw hiba(fvnev,"incomplete line %u in file: %s",4+index,fajlnev.c_str());
    if(fajl.lines()[1][0].LowCase()!="d")throw hiba(fvnev,"line 2 must be \"d=<number>\" in %s",fajlnev.c_str());
    if(!fajl.lines()[1][1].todouble(d))throw hiba(fvnev,"line 2 must be \"d=<number>\", not a number in %s",fajlnev.c_str());
    if(fajl.lines()[2+index][0].LowCase()!="b")throw hiba(fvnev,"line %u must be \"B=<9 number>\" in %s",3+index,fajlnev.c_str());
    for(int i=0;i<3;i++)for(int j=0;j<3;j++)
        if(!fajl.lines()[2+index][i*3+j+1].todouble(b[i][j]))throw hiba(fvnev,"in line %u not a number (%s) in %s",3+index,fajl.lines()[2+index][i*3+j].c_str(),fajlnev.c_str());
    if(fajl.lines()[3+index][0].LowCase()!="m")throw hiba(fvnev,"line %u must be \"M=<9 number>\" in %s",4+index,fajlnev.c_str());
    for(int i=0;i<3;i++)for(int j=0;j<3;j++)
        if(!fajl.lines()[3+index][i*3+j+1].todouble(m[i][j]))throw hiba(fvnev,"in line %u not a number (%s) in %s",4+index,fajl.lines()[2][i*3+j].c_str(),fajlnev.c_str());

    for(int i=0;i<3;i++)dest.g[i] =(b[i][0]+b[i][1]*d+b[i][2]*d*d)*A;
    for(int i=0;i<3;i++)dest.gg[i]=m[i][0]+m[i][1]*d+m[i][2]*d*d;
//printf("\n %g\t%g\t%g\t", dest.g[0]/A, dest.g[1]/A, dest.g[2]/A);
//printf("\t %g\t%g\t%g\n", dest.gg[0], dest.gg[1], dest.gg[2]);
    dest.tipus=dest.semitip=nlt_erno;
    return true;
}


//***********************************************************************
bool VR_converter(const PLString & s,vezetes & dest){
// vari-condhoz, ahol az els� a h�m�rs�klet, azt�n a vezet�s
//***********************************************************************
    int T=s.find('|');
    int y=(T==npos)?npos:s.find('|',T+1);
    int z=(y==npos)?npos:s.find('|',y+1);
    int g=s.find('#');
    int g2=(g==npos)?npos:s.find('|',g+1);
    int g3=(g2==npos)?npos:s.find('|',g2+1);
    if(g!=npos){
        if(T!=npos&&T>g)T=npos;
        if(y!=npos&&y>g)y=npos;
        if(z!=npos&&z>g)z=npos;
    }

    if(T==npos)return false;
    if(!s.todouble(dest.T))return false;
    if(!s.todouble(dest.g[0],T+1))return false;
    dest.g[2]=dest.g[1]=dest.g[0];

    if(y!=npos)if(!s.todouble(dest.g[1],y+1))return false;
    if(z!=npos)if(!s.todouble(dest.g[2],z+1))return false;
    if(g!=npos){if(!s.todouble(dest.gg[0],g+1))return false;}
    else dest.gg[0]=nulla;
    if(g2!=npos)if(!s.todouble(dest.gg[1],g2+1))return false;
    if(g3!=npos)if(!s.todouble(dest.gg[2],g3+1))return false;
    
    if(g==npos)dest.tipus=nlt_lin;
    else if(g2==npos)dest.tipus=nlt_exp;
    else if(g3==npos)dest.tipus=nlt_diode;
    else dest.tipus=nlt_quadratic;

    return true;
}

/*
//***********************************************************************
bool eq_converter(const PLString & s,double d[],monlintipus & tipus){
// a d t�mb 3 elem�
//***********************************************************************
    int b=s.find('|');
    int c=(b==npos)?npos:s.find('|',b+1);

    if(!s.todouble(d[0]))return false;
    if(b!=npos)if(!s.todouble(d[1],b+1))return false;
    if(c!=npos)if(!s.todouble(d[2],c+1))return false;
    
    if(b==npos)tipus=nlt_exp;
    else if(c==npos)tipus=nlt_diode;
    else tipus=nlt_quadratic;

    return true;
}
*/


//***********************************************************************
Oldal oldalazonosito(PLString azon){
//***********************************************************************
    Oldal oldal=EXTERNAL;
    if(azon=="west")oldal=WEST;
    else if(azon=="east")oldal=EAST;
    else if(azon=="south")oldal=SOUTH;
    else if(azon=="north")oldal=NORTH;
    else if(azon=="top")oldal=TOP;
    else if(azon=="bottom")oldal=BOTTOM;
    return oldal;
}


//***********************************************************************
void model::read(PLString path){
//***********************************************************************
    const char * fvnev="model::read()";
    logprint("Read %s",fileName.c_str());
    srfajl fajl;
    fajl.open(path+fileName);
    if(fajl.lines().size()<1||fajl.lines()[0][0].LowCase()!="vsun3-model")
        throw hiba(fvnev,"first line is not vsun3-model in %s",fileName.c_str());
    uns i=0;
    sorazon az=az_unknown;

    // name

    try{
        do{
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_nev && az!=az_unknown)throw hiba(fvnev,"name is missing in line %u in %s",i,fileName.c_str());
        }while(az!=az_nev);
        name=fajl.lines()[i][1];

        // dimension

        do{
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_dim && az!=az_unknown)throw hiba(fvnev,"dimension is missing in line %u in %s",i,fileName.c_str());
        }while(az!=az_dim);
        if(!fajl.lines()[i][1].tounsigned(dim))throw hiba(fvnev,"dimension is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());

        // x-resolution

        do{
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_xres && az!=az_unknown)throw hiba(fvnev,"x-resolution is missing in line %u in %s",i,fileName.c_str());
        }while(az!=az_xres);
        if(!fajl.lines()[i][1].tounsigned(x_res))throw hiba(fvnev,"x-resolution is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());

        // y-resolution

        if(dim>1){
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_yres && az!=az_unknown)throw hiba(fvnev,"y-resolution is missing in line %u in %s",i,fileName.c_str());
            }while(az!=az_yres);
            if(!fajl.lines()[i][1].tounsigned(y_res))throw hiba(fvnev,"y-resolution is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
        }
        else y_res=1;

        // z-resolution

        if(dim>2){
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_zres && az!=az_unknown)throw hiba(fvnev,"z-resolution is missing in line %u in %s",i,fileName.c_str());
            }while(az!=az_zres);
            if(!fajl.lines()[i][1].tounsigned(z_res))throw hiba(fvnev,"z-resolution is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
        }
        else z_res=1;

        // coordinates

        do{
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az==az_yres||az==az_zres)continue;
            if(az!=az_coor && az!=az_unknown && az!=az_xpit)throw hiba(fvnev,"coordinates and x-pitch is missing in line %u in %s",i,fileName.c_str());
        }while(az!=az_coor && az!=az_xpit);
        kerekcoord=false;
        if(az==az_coor){
            PLString s=fajl.lines()[i][1].LowCase();
            if(s=="cartesian")kerekcoord=false;
            else if(s=="cylindrical" && dim==2)kerekcoord=true;
            else if(s=="sphere" && dim==1)kerekcoord=true;
            else throw hiba(fvnev,"unknown or not applicable coordinates (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
        }

        // r-min, r-max

        if(kerekcoord){
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_rmin && az!=az_unknown)throw hiba(fvnev,"r-min is missing in line %u in %s",i,fileName.c_str());
            }while(az!=az_rmin);
            if(!fajl.lines()[i][1].todouble(r_min))throw hiba(fvnev,"r-min is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
            
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_rmax && az!=az_unknown)throw hiba(fvnev,"r-max is missing in line %u in %s",i,fileName.c_str());
            }while(az!=az_rmax);
            if(!fajl.lines()[i][1].todouble(r_max))throw hiba(fvnev,"r-max is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
            
            // x_pit �s x_hossz felt�lt�se

            x_pit.clear();
            z_a_hengerhez d;
            d.ertek = (r_max - r_min) / x_res;
            x_pit.add(d,x_res);
            x_hossz.clear();
            x_hossz.add(x_pit[0].ertek*0.5 + r_min);
            x_hossz.add(x_pit[0].ertek + r_min);
            for(u32 i=1;i<x_pit.size();i++){
                x_hossz.add(x_hossz.getLast()+x_pit[i].ertek*0.5);
                x_hossz.add(x_hossz.getLast()+x_pit[i].ertek*0.5); // ez m�r az el�z� sorban kapotthoz adja
            }
        }
        else{

        // x-pitch

            if(az!=az_xpit){
                do{
                    i++;
                    az=azonosit(fajl.lines()[i][0].LowCase());
                    if(az!=az_xpit && az!=az_unknown)throw hiba(fvnev,"x-pitch is missing in line %u in %s",i,fileName.c_str());
                }while(az!=az_xpit);
            }
            if(!pitchconverter(fajl.lines()[i],x_pit,x_hossz))throw hiba(fvnev,"x-pitch data corrupt in line %u in %s",i,fileName.c_str());
            if(x_pit.size()<x_res)throw hiba(fvnev,"fewer x-pitch data than resolution (%u<%u) in line %u in %s",x_pit.size(),x_res,i,fileName.c_str());
            if(x_pit.size()>x_res)logprint("warning: model::read() => more x-pitch data than resolution (%u>%u) in line %u in %s",x_pit.size(),x_res,i,fileName.c_str());
        }

        // y-pitch

        if(dim>1){
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_ypit && az!=az_unknown)throw hiba(fvnev,"y-pitch is missing in line %u in %s",i,fileName.c_str());
            }while(az!=az_ypit);
            if(!pitchconverter(fajl.lines()[i],y_pit,y_hossz))throw hiba(fvnev,"y-pitch data corrupt in line %u in %s",i,fileName.c_str());
            if(y_pit.size()<y_res)throw hiba(fvnev,"fewer y-pitch data than resolution (%u<%u) in line %u in %s",y_pit.size(),y_res,i,fileName.c_str());
            if(y_pit.size()>y_res)logprint("warning: model::read() => more y-pitch data than resolution (%u>%u) in line %u in %s",y_pit.size(),y_res,i,fileName.c_str());
        }

        // z-pitch

        if(dim>2 || !kerekcoord){
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_zpit && az!=az_unknown)throw hiba(fvnev,"z-pitch is missing in line %u in %s",i,fileName.c_str());
            }while(az!=az_zpit);
            if(!pitchconverter(fajl.lines()[i],z_pit,z_hossz))throw hiba(fvnev,"z-pitch data corrupt in line %u in %s",i,fileName.c_str());
            if(z_pit.size()<z_res)throw hiba(fvnev,"fewer z-pitch data than resolution (%u<%u) in line %u in %s",z_pit.size(),z_res,i,fileName.c_str());
            if(z_pit.size()>z_res)logprint("warning: model::read() => more z-pitch data than resolution (%u>%u) in line %u in %s",z_pit.size(),z_res,i,fileName.c_str());
        }
        else{
            z_pit.clear();
            z_a_hengerhez d;
            d.henger_e = true;
            z_pit.add(d);
            z_hossz.clear();
            z_hossz.add(0.0);
            z_hossz.add(0.0);
        }

		sprintf(hibaUzenet,"%ux%ux%u",x_res,y_res,z_res);

        // bitmap

        tbmp.clear();
        tbmp.resize(z_res);
        for(uns j=0;j<z_res;j++){
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_bmp && az!=az_unknown)throw hiba(fvnev,"bitmap is missing in line %u in %s",i,fileName.c_str());
            }while(az!=az_bmp);
            tbmp[j].load(path+fajl.lines()[i][1]);
            if(tbmp[j].getxsize()<x_res)
                throw hiba(fvnev,"bitmap x size < x resolution (%u<%u) in %s in %s",tbmp[j].getxsize(),x_res,fajl.lines()[i][1].c_str(),fileName.c_str());
            if(tbmp[j].getysize()<y_res)
                throw hiba(fvnev,"bitmap y size < y resolution (%u<%u) in %s in %s",tbmp[j].getysize(),y_res,fajl.lines()[i][1].c_str(),fileName.c_str());
        }
        printf("Used colors: ");
        bool elso=true;
        for(uns j=0; j<colmax; j++)
            for(uns k=0; k<tbmp.size(); k++)
                if(tbmp[k].find_also(j)){
                    printf(elso?"%u":", %u",j);
                    elso=false;
                    break;
                }

        // material vagy ambient-emissivity

        tmat.clear();
        material m_dummy;
        do{
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_mater && az!=az_unknown && az!=az_bmp && az!=az_ambem)
                throw hiba(fvnev,"ambient-emissivity or material is missing in line %u in %s",i,fileName.c_str());
        }while(az!=az_mater&&az!=az_ambem); // minimum 1 material sz�ks�ges

        if(az==az_ambem){
            if(!R_converter(fajl.lines()[i],1,amb_emiss,m_dummy))
                throw hiba(fvnev,"ambient-emissivity value wrong in line %u in %s",i,fileName.c_str());
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_mater && az!=az_unknown)
                    throw hiba(fvnev,"material is missing in line %u in %s",i,fileName.c_str());
            }while(az!=az_mater); // minimum 1 material sz�ks�ges
        }

        // material �s mat

        while(az==az_mater){
            tmat.resize(tmat.size()+1);
            material & aktmat=tmat[tmat.size()-1];
            aktmat.nev=fajl.lines()[i][1].LowCase();
            if(fajl.lines()[i].size() > 2){ // F�zisv�lt�s vagy hiszter�zis
                uns melyik=0;
                if(fajl.lines()[i][2].LowCase()=="f")
                    melyik = 1;
                else if(fajl.lines()[i][2].LowCase()=="h")
                    melyik = 2;
                if(melyik==0)
                    throw hiba(fvnev,"unknown identifier (%s found) in line %u (material) in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                aktmat.is_his=true;
                if(fajl.lines()[i].size() < 5)
                    throw hiba(fvnev,"too few parameters in line %u (material) in %s",i,fileName.c_str());
                if(!fajl.lines()[i][3].todouble(aktmat.his_T_min))
                    throw hiba(fvnev,"not a number (%s found) in line %u (material) in %s",fajl.lines()[i][3].c_str(),i,fileName.c_str());
                if(!fajl.lines()[i][4].todouble(aktmat.his_T_max))
                    throw hiba(fvnev,"not a number (%s found) in line %u (material) in %s",fajl.lines()[i][4].c_str(),i,fileName.c_str());
                if(melyik==2){
                    if(fajl.lines()[i].size() < 6)
                        throw hiba(fvnev,"too few parameters in line %u (material) in %s",i,fileName.c_str());
                    if(!fajl.lines()[i][5].todouble(aktmat.his_T_width_fele))
                        throw hiba(fvnev,"not a number (%s found) in line %u (material) in %s",fajl.lines()[i][5].c_str(),i,fileName.c_str());
                    aktmat.his_T_width_fele*=0.5;
                }
                aktmat.set_his_derivaltak();
            }
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_mat && az!=az_mater && az!=az_color && az!=az_unknown)
                    throw hiba(fvnev,"material or mat or color is missing (%s found)in line %u in %s",fajl.lines()[i][0].c_str(),i,fileName.c_str());
                if(az==az_mat){
                    matazon maz=azon_mat(fajl.lines()[i][1].LowCase());
                    //uns kelldb=3;
                    //if(fajl.lines()[i].size()!=kelldb)logprint("warning: model::read() => mat arguments may be in wrong format in line %u in %s",i,fileName.c_str());
                    switch(maz){
                        case maz_heatcond:
                        case maz_heatres:
                            aktmat.is_th=true;
                            if(!R_converter(fajl.lines()[i],2,aktmat.thvez,aktmat))
                                throw hiba(fvnev,"heatcond value wrong in line %u in %s",i,fileName.c_str());
                            aktmat.thvez.is_resistivity = ( maz == maz_heatres );
                            break;
                        case maz_heatcap:
                            if(!R_converter(fajl.lines()[i],2,aktmat.Cth,aktmat))
                                throw hiba(fvnev,"heatcap value wrong in line %u in %s",i,fileName.c_str());
                            break;
                        case maz_conduct:
                        case maz_resistivity:
                            aktmat.is_el=true;
                            if(!R_converter(fajl.lines()[i],2,aktmat.elvez,aktmat))
                                throw hiba(fvnev,"conduct value wrong in line %u in %s",i,fileName.c_str());
                            aktmat.elvez.is_resistivity = ( maz == maz_resistivity );
                            break;
                        case maz_capac:
                            if(!R_converter(fajl.lines()[i],2,aktmat.Ce,aktmat))
                                throw hiba(fvnev,"capacity value wrong in line %u in %s",i,fileName.c_str());
                            break;
                        case maz_seebeck:
                            if(!R_converter(fajl.lines()[i],2,aktmat.S,aktmat))
                                throw hiba(fvnev,"seebeck value wrong in line %u in %s",i,fileName.c_str());
                            break;
                        case maz_disscoeff:
                            if(!R_converter(fajl.lines()[i],2,aktmat.D,aktmat))
                                throw hiba(fvnev,"dissip_coeff value wrong in line %u in %s",i,fileName.c_str());
                            break;
                        case maz_emissivity:
                            if(!R_converter(fajl.lines()[i],2,aktmat.emissivity,aktmat))
                                throw hiba(fvnev,"emissivity value wrong in line %u in %s",i,fileName.c_str());
                            break;
                        case maz_th_transmissivity:
                            if(!R_converter(fajl.lines()[i],2,aktmat.th_transmiss,aktmat))
                                throw hiba(fvnev,"thermal-transmissivity value wrong in line %u in %s",i,fileName.c_str());
                            break;
                    }
                }
            }while(az==az_unknown || az==az_mat);
        } // legal�bb 1 color kell

        // color

        while(az==az_color){
            uns szin;
            if(!fajl.lines()[i][1].tounsigned(szin))
                throw hiba(fvnev,"color value wrong in line %u in %s",i,fileName.c_str());
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_col && az!=az_color && az!=az_semicon && az!=az_end && az!=az_coupled && az!=az_unknown)
                    throw hiba(fvnev,"color or col or semiconductor or coupled_model or end is missing (%s found)in line %u in %s",fajl.lines()[i][0].c_str(),i,fileName.c_str());
                if(az==az_col){
                    PLString aktpar=fajl.lines()[i][2].LowCase();
                    PLString kisb=fajl.lines()[i][1].LowCase();
                    if(kisb=="type"){
                        tcolor[szin].is=true;
                        if(aktpar=="normal")tcolor[szin].tipus=SzinNormal;
                        else if (aktpar == "boundary")tcolor[szin].tipus = SzinBoundary;
                        else if (aktpar == "uchannel")tcolor[szin].tipus = SzinUchannel;
                        else logprint("warning: model::read() => unrecognized color type (%s) in line %u in %s", aktpar.c_str(), i, fileName.c_str());
                    }
                    else if(kisb=="material"){
                        uns j;
                        for(j=0;j<tmat.size() && tmat[j].nev!=aktpar;j++);
                        if(j<tmat.size())tcolor[szin].pmat=&tmat[j];
                        else throw hiba(fvnev,"missing material (%s) in color definition in line %u in %s",aktpar.c_str(),i,fileName.c_str());
                    }
                    else if(kisb=="label"){
                        tcolor[szin].nev=aktpar;
                    }
                }
            }while(az==az_unknown || az==az_col);
        }// semiconductor vagy coupled_model vagy end a v�ge

        // a bmp sz�neinek ellen�rz�se, t�rfogatsz�m�t�s, tseged felt�lt�se

        tseged.resize( x_res * y_res * z_res );
        uns j4 = 0;
        for(uns j1=0;j1<z_res;j1++)
            for(uns j2=0;j2<y_res;j2++)
                for(uns j3=0;j3<x_res;j3++,j4++){
                    color & cakt=tcolor[tbmp[j1].getpixel_also(j3,j2)];
                    if(!cakt.is)
                        throw hiba(fvnev,"undefined color (%u) in bitmap in %s",tbmp[j1].getpixel_also(j3,j2),fileName.c_str());
                    else{
                        cakt.terfogat+=x_pit[j3].get(nulla)*y_pit[j2].get(nulla)*z_pit[j1].get(x_hossz[2*j3]);
                        tseged[j4].mater = cakt.pmat;
                    }
                }

        // semiconductor

        while(az==az_semicon){
           uns col1,col2;
            if(!fajl.lines()[i][1].tounsigned(col1))
                throw hiba(fvnev,"semiconductor color1 value wrong in line %u in %s",i,fileName.c_str());
            if(!fajl.lines()[i][2].tounsigned(col2))
                throw hiba(fvnev,"semiconductor color2 value wrong in line %u in %s",i,fileName.c_str());
            if(col1==col2)
                throw hiba(fvnev," semiconductor colors must be different (%u) in line %u in %s",col1,i,fileName.c_str());
            if(!tcolor[col1].is||!tcolor[col2].is)
                throw hiba(fvnev," undefined semiconductor color (%u or %u) in line %u in %s",col1,col2,i,fileName.c_str());
            tcolor[col1].tsemi.resize(tcolor[col1].tsemi.size()+1);
            semiconductor & aktsemi=tcolor[col1].tsemi[tcolor[col1].tsemi.size()-1];
            aktsemi.col2=col2;
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_semi && az!=az_semicon && az!=az_coupled && az!=az_end && az!=az_unknown)
                    throw hiba(fvnev,"semiconductor or semi or coupled_model or end is missing (%s found)in line %u in %s",fajl.lines()[i][0].c_str(),i,fileName.c_str());
                if(az==az_semi){
                    PLString aktpar=fajl.lines()[i][2].LowCase();
                    PLString kisb=fajl.lines()[i][1].LowCase();
                    if(kisb=="model"){
                        if(aktpar=="equation"){
                            if(!R_converter(fajl.lines()[i],3,aktsemi.par,m_dummy))
                                throw hiba(fvnev,"equation value wrong in line %u in %s",i,fileName.c_str());
                        }
                        else if(aktpar=="diode"){
                            if(!R_converter(fajl.lines()[i],3,aktsemi.par,m_dummy,true))
                                throw hiba(fvnev,"equation value wrong in line %u in %s",i,fileName.c_str());
                        }
                        else if(aktpar=="erno"){
                            if(!Erno_converter(path+fajl.lines()[i][3],aktsemi.par))
                                throw hiba(fvnev,"erno model wrong in line %u in %s",i,fileName.c_str());
                        }
                        else throw hiba(fvnev,"unknown semiconductor model type (%s) in line %u in %s",aktpar.c_str(),i,fileName.c_str());
                    }
                    else if(kisb=="dissip_coeff"){
                        if(!R_converter(fajl.lines()[i],2,aktsemi.D,m_dummy))
                            throw hiba(fvnev,"dissip_coeff value wrong in line %u in %s",i,fileName.c_str());
                    }
                    else if(kisb=="radiation_coeff"){
                        if(!R_converter(fajl.lines()[i],2,aktsemi.R,m_dummy))
                            throw hiba(fvnev,"radiation_coeff value wrong in line %u in %s",i,fileName.c_str());
                    }
                    else if(kisb=="radiance"){
                        if(aktpar=="equation"){
                            if(!R_converter(fajl.lines()[i],3,aktsemi.rad,m_dummy))
                                throw hiba(fvnev,"radiance equation value wrong in line %u in %s",i,fileName.c_str());
                        }
                        else if(aktpar=="erno"){
                            if(!Erno_converter(path+fajl.lines()[i][3],aktsemi.rad))
                                throw hiba(fvnev,"erno radiance model wrong in line %u in %s",i,fileName.c_str());
                        }
                        else throw hiba(fvnev,"unknown semiconductor radiance type (%s) in line %u in %s",aktpar.c_str(),i,fileName.c_str());
                    }
                    else if(kisb=="luminance"){
                        if(aktpar=="equation"){
                            if(!R_converter(fajl.lines()[i],3,aktsemi.lum,m_dummy))
                                throw hiba(fvnev,"luminance equation value wrong in line %u in %s",i,fileName.c_str());
                        }
                        else if(aktpar=="erno"){
                            if(!Erno_converter(path+fajl.lines()[i][3],aktsemi.lum))
                                throw hiba(fvnev,"erno luminance model wrong in line %u in %s",i,fileName.c_str());
                        }
                        else throw hiba(fvnev,"unknown semiconductor luminance type (%s) in line %u in %s",aktpar.c_str(),i,fileName.c_str());
                    }
                     else if(kisb=="equation"){
                        throw hiba(fvnev,"semiconductor farmat is deprecated, use model=equation=... instead of equation=... in line %u in %s",i,fileName.c_str());
                    }
                     else if(kisb!="")printf("# unknown semiconductor parameter is ignored: %s\n",kisb.c_str());
               }
            }while(az==az_unknown || az==az_semi);
        }

        // f�lvezet� fel�letek sz�m�t�sa

        A_semi_full=nulla;
        for(uns ix=0;ix<colmax;ix++)
            if(tcolor[ix].is&&tcolor[ix].tipus==SzinNormal)
                for(uns iy=0;iy<tcolor[ix].tsemi.size();iy++){
                    semiconductor & sakt=tcolor[ix].tsemi[iy];
                    sakt.As=nulla;
                    if(sakt.col2==colmax){ // k�ls� peremhez f�lvezet
                        // alja
                        for(uns j2=0;j2<y_res;j2++)
                            for(uns j3=0;j3<x_res;j3++)
                                if(tbmp[0].getpixel_also(j3,j2)==ix)sakt.As+=x_pit[j3].get(nulla)*y_pit[j2].get(nulla);
                        // teteje
                        for(uns j2=0;j2<y_res;j2++)
                            for(uns j3=0;j3<x_res;j3++)
                                if(tbmp[z_res-1].getpixel_also(j3,j2)==ix)sakt.As+=x_pit[j3].get(nulla)*y_pit[j2].get(nulla);
                        // west
                        for(uns j1=0;j1<z_res;j1++)
                            for(uns j2=0;j2<y_res;j2++)
                                if(tbmp[j1].getpixel_also(0,j2)==ix)sakt.As+=z_pit[j1].get(x_hossz[0])*y_pit[j2].get(nulla);
                        // east
                        for(uns j1=0;j1<z_res;j1++)
                            for(uns j2=0;j2<y_res;j2++)
                                if(tbmp[j1].getpixel_also(x_res-1,j2)==ix)sakt.As+=z_pit[j1].get(x_hossz[2*x_res-2])*y_pit[j2].get(nulla);
                        // south
                        for(uns j1=0;j1<z_res;j1++)
                            for(uns j3=0;j3<x_res;j3++)
                                if(tbmp[j1].getpixel_also(j3,0)==ix)sakt.As+=z_pit[j1].get(x_hossz[j3*2])*x_pit[j3].get(nulla);
                        // north
                        for(uns j1=0;j1<z_res;j1++)
                            for(uns j3=0;j3<x_res;j3++)
                                if(tbmp[j1].getpixel_also(j3,y_res-1)==ix)sakt.As+=z_pit[j1].get(x_hossz[j3*2])*x_pit[j3].get(nulla);
                    }
                    else{ // bel�l f�lvezet
                        cuns szin1=ix,szin2=sakt.col2;
                        // z ir�nyban
                        for(uns j1=0;j1<z_res-1;j1++)
                            for(uns j2=0;j2<y_res;j2++)
                                for(uns j3=0;j3<x_res;j3++){
                                    cuns s1=tbmp[j1].getpixel_also(j3,j2);
                                    cuns s2=tbmp[j1+1].getpixel_also(j3,j2);
                                    if( (s1==szin1&&s2==szin2) || (s1==szin2&&s2==szin1) )sakt.As+=y_pit[j2].get(nulla)*x_pit[j3].get(nulla);
                                }
                        // y ir�nyban
                        for(uns j1=0;j1<z_res;j1++)
                            for(uns j2=0;j2<y_res-1;j2++)
                                for(uns j3=0;j3<x_res;j3++){
                                    cuns s1=tbmp[j1].getpixel_also(j3,j2);
                                    cuns s2=tbmp[j1].getpixel_also(j3,j2+1);
                                    if( (s1==szin1&&s2==szin2) || (s1==szin2&&s2==szin1) )sakt.As+=z_pit[j1].get(x_hossz[j3*2])*x_pit[j3].get(nulla);
                                }
                        // x ir�nyban
                        for(uns j1=0;j1<z_res;j1++)
                            for(uns j2=0;j2<y_res;j2++)
                                for(uns j3=0;j3<x_res-1;j3++){
                                    cuns s1=tbmp[j1].getpixel_also(j3,j2);
                                    cuns s2=tbmp[j1].getpixel_also(j3+1,j2);
                                    if( (s1==szin1&&s2==szin2) || (s1==szin2&&s2==szin1) )sakt.As+=y_pit[j2].get(nulla)*z_pit[j1].get(x_hossz[j3*2]);
                                }
                    }
                    A_semi_full += sakt.As;
        }
        
        // coupled_model

        uns azon_start = 0;
        while(az==az_coupled){
            if(fajl.lines()[i].size()<6)
                throw hiba(fvnev,"coupled_model too few arguments (%u<6)in line %u in %s",fajl.lines()[i].size(),i,fileName.c_str());
            cuns coupindex=tcoupled.incsize()-1;
            tcoupled[coupindex].fileName=fajl.lines()[i][1];
            if(!fajl.lines()[i][2].tounsigned(tcoupled[coupindex].x))
                throw hiba(fvnev,"coupled_model x value wrong in line %u in %s",i,fileName.c_str());
            if(!fajl.lines()[i][3].tounsigned(tcoupled[coupindex].y))
                throw hiba(fvnev,"coupled_model y value wrong in line %u in %s",i,fileName.c_str());
            if(!fajl.lines()[i][4].tounsigned(tcoupled[coupindex].z))
                throw hiba(fvnev,"coupled_model z value wrong in line %u in %s",i,fileName.c_str());
            tcoupled[coupindex].oldal = oldalazonosito(fajl.lines()[i][5].LowCase());
            if(tcoupled[coupindex].oldal==EXTERNAL)
                throw hiba(fvnev,"coupled_model side value wrong (%s) in line %u in %s",fajl.lines()[i][5].c_str(),i,fileName.c_str());
            azon_start += tcoupled[coupindex].read(path,azon_start);
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
        }
        
    }
    catch(const hiba & h){
        PLString s=h.what();
        if(s.find("tomb::operator[]")==npos)throw;
        throw hiba(fvnev,"missing parameters or unexpected end of file in line %u in %s",i,fileName.c_str());
    }
}


//***********************************************************************
uns coupled_model::read(const PLString & path,uns azon_start){
//***********************************************************************
    const char * fvnev = "coupled_model::read";
    srfajl fajl;
    fajl.open(path+fileName);
    if(fajl.lines().size()<1||fajl.lines()[0][0].LowCase()!="vsun3-coupled")
        throw hiba(fvnev,"first line is not vsun3-coupled in %s",fileName.c_str());
    uns i=0;
    sorazon az=az_unknown;

    // external_nodes

    do{
        i++;
        az=azonosit(fajl.lines()[i][0].LowCase());
        if(az!=az_extnods && az!=az_unknown)throw hiba(fvnev,"external_nodes is missing in line %u in %s",i,fileName.c_str());
    }while(az!=az_extnods);

    compmod.tnodes.resize(fajl.lines()[i].size() - 1);
    compmod.texternal_azon.resize(fajl.lines()[i].size() - 1);
    for(uns j=1; j<fajl.lines()[i].size(); j++)
        compmod.tnodes[j-1] = fajl.lines()[i][j].LowCase();
    for(uns j = 0; j<compmod.texternal_azon.size(); j++) 
        compmod.texternal_azon[j] = j + azon_start + 1;

    // footprint

    do{
        do{
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_admmx && az!=az_footpr && az!=az_unknown)
                throw hiba(fvnev,"footprint is missing in line %u in %s",i,fileName.c_str());
        }while(az!=az_admmx && az!=az_footpr);

        if(az==az_footpr){
            if(fajl.lines()[i].size()<6)
                throw hiba(fvnev,"few footprint parameters in line %u in %s",i,fileName.c_str());
            cuns fpindex=compmod.tfootpr.incsize()-1;
            compmod.tfootpr[fpindex].external_azon = 0;
            PLString fpnev=fajl.lines()[i][1].LowCase();
            for(uns j = 0; j<compmod.tnodes.size(); j++)if( compmod.tnodes[j] == fpnev ){
                compmod.tfootpr[fpindex].external_azon = j + azon_start + 1;
                break;
            }
            if( compmod.tfootpr[fpindex].external_azon == 0 )
                throw hiba(fvnev,"unknown footprint node name (%s) in line %u in %s",fpnev.c_str(),i,fileName.c_str());
            if(!fajl.lines()[i][2].tounsigned(compmod.tfootpr[fpindex].x))
                throw hiba(fvnev,"footprint x value wrong in line %u in %s",i,fileName.c_str());
            if(!fajl.lines()[i][3].tounsigned(compmod.tfootpr[fpindex].y))
                throw hiba(fvnev,"footprint y value wrong in line %u in %s",i,fileName.c_str());
            if(!fajl.lines()[i][4].tounsigned(compmod.tfootpr[fpindex].w))
                throw hiba(fvnev,"footprint w value wrong in line %u in %s",i,fileName.c_str());
            if(!fajl.lines()[i][5].tounsigned(compmod.tfootpr[fpindex].h))
                throw hiba(fvnev,"footprint h value wrong in line %u in %s",i,fileName.c_str());
        }
    }while(az!=az_admmx);

    // admittanciam�trix

    if(az==az_admmx){
        compmod.tipus=CompactMatrix;
        cuns elemszam=( compmod.tnodes.size() * (compmod.tnodes.size()+1) / 2 );
        cuns n=compmod.tnodes.size();
        if( fajl.lines()[i].size() < elemszam + 1)
            throw hiba(fvnev,"adm too few adm value in line %u in %s",i,fileName.c_str());
        compmod.y = new dsrmatrix;
        compmod.y->resize(n);
        uns l=1;
        for(uns j=0; j<n; j++)for(uns k=0; k<=j; k++,l++){
            dbl d;
            if(!fajl.lines()[i][l].todouble(d))
                throw hiba(fvnev,"adm value wrong (%s) in line %u in %s",fajl.lines()[i][l].c_str(),i,fileName.c_str());
            compmod.y->set(j,k,d);
        }

        // inhomog�n vektor

        do{
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_inhom && az!=az_unknown)
                throw hiba(fvnev,"inhom is missing in line %u in %s",i,fileName.c_str());
        }while(az!=az_inhom);

        if( fajl.lines()[i].size() < n + 1)
            throw hiba(fvnev,"adm too few inhom value in line %u in %s",i,fileName.c_str());
        compmod.j = new drvector;
        compmod.j->resize(n);
        for(uns j=0; j<n; j++){
            dbl d;
            if(!fajl.lines()[i][j+1].todouble(d))
                throw hiba(fvnev,"inhom value wrong (%s) in line %u in %s",fajl.lines()[i][j+1].c_str(),i,fileName.c_str());
            compmod.j->set(j,d);
        }
    }
    else{
        throw hiba(fvnev,"ha nem admittanciamatrixszal van megadva a csatolot modell... in line %u in %s",i,fileName.c_str());
    }
    return compmod.tnodes.size();
}


//***********************************************************************
bool bouvalue(const tomb<PLString> & be,boundary & cel,const tomb <convection> & tconv,const PLString & path, uns x_res, uns y_res){
//***********************************************************************
    const PLString azon=be[2].LowCase();
    if(azon=="open"||azon=="float"||azon=="adiabatic"){
        cel.tipus=PeremOpen;
        cel.value=nulla;
    }
    else if(azon=="isothermal"||azon=="equipot"){
        cel.tipus=PeremU;
        if(!be[3].todouble(cel.value))return false;
    }
    else if(azon=="convection"){
        cel.tipus=PeremR;
        if(!be[3].todouble(cel.value)){
            PLString nev=be[3].LowCase();
            for(u32 i=0;i<tconv.size();i++)
                if (tconv[i].nev == nev){
                    cel.conv = tconv[i];
                    return true;
                }
            return false;
        }
        else cel.conv.is_defined=false;
    }
    else if (azon == "conv_temp"){
        cel.tipus = PeremRU;
        if (!be[3].todouble(cel.value)){
            PLString nev = be[3].LowCase();
            for (u32 i = 0; i<tconv.size(); i++)
                if (tconv[i].nev == nev){
                    cel.conv = tconv[i];
                    if (!be[4].todouble(cel.value2))
                        return false;
                    return true;
                }
            return false;
        }
        else{
            cel.conv.is_defined = false;
            if (!be[4].todouble(cel.value2))
                return false;
        }
    }
    else if (azon == "convection-map"){
        cel.tipus=PeremR;
        if(!konv_map_konverter(path+be[3],cel.conv_map,x_res,y_res))return false;
    }
    else return false;
    return true;
}


//***********************************************************************
bool bouolvas(const tomb<PLString> & be,csomag & cs,bool & isel,const tomb <convection> & tconv,const PLString & path, uns x_res, uns y_res){
//***********************************************************************
    const PLString azon=be[1].LowCase();
    if(azon=="electrical"){
        isel=true;
        if(!bouvalue(be,cs.el[0],tconv,path,x_res,y_res))return false;
        for(uns j=1;j<BASIC_SIDES;j++){
            cs.el[j]=cs.el[0];
        }
    }
    else if(azon=="thermal"){
        isel=false;
        if(!bouvalue(be,cs.th[0],tconv,path,x_res,y_res))return false;
        for(uns j=1;j<BASIC_SIDES;j++){
            cs.th[j]=cs.th[0];
        }
    }
    else if(azon=="internal"){
        throw hiba("bouolvas()","azon==\"internal\"");
    }
    else{
        Oldal oldal=oldalazonosito(azon);
        if(oldal==EXTERNAL)return false;
        if(isel){
            if(!bouvalue(be,cs.el[oldal],tconv,path,x_res,y_res))return false;
        }
        else{
            if(!bouvalue(be,cs.th[oldal],tconv,path,x_res,y_res))return false;
        }
    }

    return true;
}


//***********************************************************************
bool excitvalue(const tomb<PLString> & be,excitation & ertek,tomb<excitation> & mul){
// Ha t�bb gerjeszt�s �rt�k szerepel, akkor az els� ker�l az ertek-be, a t�bbi a mul-ba
//***********************************************************************
    const PLString azon=be[2].LowCase();
    if(azon=="voltage"||azon=="temp"||azon=="isothermal"){
        ertek.tipus=GerjU;
        if(!be[3].todouble(ertek.ertek))return false;
    }
    else if(azon=="current"||azon=="power"){
        ertek.tipus=GerjI;
        if(!be[3].todouble(ertek.ertek))return false;
    }
    else throw hiba("excitvalue","unknown excitation type (%s)",azon.c_str());
    if(be.size()>4){
        mul.resize(be.size()-3);
        for(uns i=0;i<mul.size();i++){
            mul[i]=ertek;
            if(!be[i+3].todouble(mul[i].ertek))return false;
        }
    }
    return true;
}


//***********************************************************************
bool excitvalue_controlled(const tomb<PLString> & be,excitation_2 & ertek){
// Controlled anal�zis f�jlhoz
//***********************************************************************
    const PLString azon=be[2].LowCase();
    if(azon=="voltage"||azon=="temp"||azon=="isothermal"){
        ertek.tipus=GerjU;
        if(!be[3].todouble(ertek.ertek))return false;
    }
    else if(azon=="current"||azon=="power"){
        ertek.tipus=GerjI;
        if(!be[3].todouble(ertek.ertek))return false;
    }
    else if(azon=="none")
        ertek.tipus = GerjSemmi;
    else throw hiba("excitvalue_controlled","unknown excitation type (%s)",azon.c_str());
    return true;
}


//***********************************************************************
void read_ctrl(const PLString & fileName,analysis & aktanal, model * pmodel){
//***********************************************************************
    const char * fvnev="simulation::read_ctrl()";
    logprint("Read %s",fileName.c_str());
    srfajl fajl;
    fajl.open(fileName);
    if(fajl.lines().size()<1||fajl.lines()[0][0].LowCase()!="vsun3-controlled")
        throw hiba(fvnev,"first line is not vsun3-controlled in %s",fileName.c_str());
    uns i=0;
    sorazon az=az_unknown;

    // timestep

    do{
        i++;
        az=azonosit(fajl.lines()[i][0].LowCase());
        if(az!=az_timestep && az!=az_unknown)
            throw hiba(fvnev,"timestep is missing in line %u in %s",i,fileName.c_str());
    }while(az!=az_timestep);
    if(!fajl.lines()[i][1].todouble(aktanal.from)) // az anal.step uns, ez�rt ebbe tessz�k
        throw hiba(fvnev,"timestep is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());

    // timelimit

    do{
        i++;
        az=azonosit(fajl.lines()[i][0].LowCase());
        if(az!=az_timelimit && az!=az_unknown)
            throw hiba(fvnev,"timelimit is missing in line %u in %s",i,fileName.c_str());
    }while(az!=az_timelimit);
    if(!fajl.lines()[i][1].todouble(aktanal.to))
        throw hiba(fvnev,"timelimit is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());

    // change_time

    do{
        i++;
        az=azonosit(fajl.lines()[i][0].LowCase());
        if(az!=az_change_time && az!=az_end && az!=az_unknown)
            throw hiba(fvnev,"change_time is missing in line %u in %s",i,fileName.c_str());
    }while(az!=az_change_time && az!=az_end);

    aktanal.ctrl.clear();
    uns darab=0;
    while(az!=az_end){

        // change_time

        if(az==az_change_time){
            darab++;
            aktanal.ctrl.resize(darab);
            if(!fajl.lines()[i][1].todouble(aktanal.ctrl[darab-1].time))
                throw hiba(fvnev,"change_time is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
            if(darab>1 && aktanal.ctrl[darab-1].time <= aktanal.ctrl[darab-2].time)
                throw hiba(fvnev,"change_time have to be increasing (%s<=%g) in line %u in %s",
                                  fajl.lines()[i][1].c_str(),aktanal.ctrl[darab-2].time,i,fileName.c_str());
        }

        // electrical vagy thermal

        else if(az==az_electrical || az==az_thermal){
            cuns index=aktanal.ctrl[darab-1].excit.size();
            aktanal.ctrl[darab-1].excit.resize(index+1);

            aktanal.ctrl[darab-1].excit[index].is_el = az==az_electrical;
            
            PLString kisb=fajl.lines()[i][1].LowCase();
            uns szin=colmax;
            for(uns j=0;j<colmax && szin==colmax;j++){
                const color & cc=pmodel->tcolor[j];
                if(cc.is && cc.tipus==SzinNormal && cc.nev==kisb)szin=j;
            }
            if(szin==colmax)
                throw hiba(fvnev,"missing color label (%s) in excitation definition in line %u in %s",fajl.lines()[i][1].c_str(),i,fileName.c_str());
            
            aktanal.ctrl[darab-1].excit[index].color_index=szin;
            if(!excitvalue_controlled(fajl.lines()[i],aktanal.ctrl[darab-1].excit[index]))                        
                throw hiba(fvnev,"missing or bad value in excitation definition (%s) in line %u in %s",fajl.lines()[i][1].c_str(),i,fileName.c_str());
        }
        else if(az==az_timestep){
            if(!fajl.lines()[i][1].todouble(aktanal.ctrl[darab-1].timestep))
                throw hiba(fvnev,"timestep is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
        }
        i++;
        az=azonosit(fajl.lines()[i][0].LowCase());
        if(az!=az_change_time && az!=az_end && az!=az_electrical && az!=az_thermal && az!=az_timestep && az!=az_unknown)
            throw hiba(fvnev,"%s found in line %u in %s",fajl.lines()[i][0].c_str(),i,fileName.c_str());
    }
}


//***********************************************************************
void uchannel::read(PLString path, PLString fajlnev){
//***********************************************************************
    const char * fvnev = "uchannel::init2()";
    logprint("Read %s", fajlnev.c_str());
    srfajl fajl;
    fajl.open(path + fajlnev);

    for (uns i = 0; i < fajl.lines().size(); i++){
        PLString azon = fajl.lines()[i][0].LowCase();
        if (azon == "reverse"){
            is_reverse = true;
            continue;
        }
        if (azon == "segment_number"){
            if (!fajl.lines()[i][1].tounsigned(n))
                throw hiba(fvnev, "segment_number is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "flow_rate"){
            if (!fajl.lines()[i][1].todouble(flow_rate))
                throw hiba(fvnev, "flow_rate is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "fixed_wall_temp"){
            if (!fajl.lines()[i][1].todouble(fixed_wall_temp))
                throw hiba(fvnev, "fixed_wall_temp is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            is_auto_wall_temp = false;
            continue;
        }
        if (azon == "auto_wall_temp"){
            is_auto_wall_temp = true;
            continue;
        }
        if (azon == "fluid_temp"){
            if (!fajl.lines()[i][1].todouble(fluid_temp))
                throw hiba(fvnev, "fluid_temp is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "type"){
            if (fajl.lines()[i][1].LowCase() == "rect")
                tipus = CsatRect;
            else if (fajl.lines()[i][1].LowCase() == "circ")
                tipus = CsatCirc;
            else if (fajl.lines()[i][1].LowCase() == "trap")
                tipus = CsatTrap;
            else
                throw hiba(fvnev, "invalid uchannel type (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "width_bottom"){
            if (!fajl.lines()[i][1].todouble(width_bottom))
                throw hiba(fvnev, "width_bottom is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "width_top"){
            if (!fajl.lines()[i][1].todouble(width_top))
                throw hiba(fvnev, "width_top is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "roughness") {
            if (!fajl.lines()[i][1].todouble(roughness))
                throw hiba(fvnev, "roughness is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "height"){
            if (!fajl.lines()[i][1].todouble(height))
                throw hiba(fvnev, "height is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "density"){
            if (!fajl.lines()[i][1].todouble(density))
                throw hiba(fvnev, "density is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "dynamic_viscosity"){
            if (!fajl.lines()[i][1].todouble(dynamic_visc))
                throw hiba(fvnev, "dynamic_viscosity is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "specific_heat"){
            if (!fajl.lines()[i][1].todouble(spec_heat))
                throw hiba(fvnev, "specific_heat is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "heat_conductivity"){
            if (!fajl.lines()[i][1].todouble(heat_cond))
                throw hiba(fvnev, "heat_conductivity is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        throw hiba(fvnev, "unknown identifier (%s) in %s", fajl.lines()[i][0].c_str(), fajlnev.c_str());
    }
}


//***********************************************************************
void uchannel::set_start_stop_length(){
//***********************************************************************
    if (psim->pmodel->kerekcoord)
        throw hiba("uchannel::set_start_stop_length", "cylindrical coordinata system is mot supported");
    if (szinindex == colmax)
        throw hiba("uchannel::set_start_stop_length", "program error: szinindex == colmax");

    struct pont{ uns x, y, z; pont() :x(0), y(0), z(0){} };
    pont i, min, max;
    pont xmin_ymin, xmin_ymax, xmin_zmin, xmin_zmax;
    pont xmax_ymin, xmax_ymax, xmax_zmin, xmax_zmax;
    pont ymin_xmin, ymin_xmax, ymin_zmin, ymin_zmax;
    pont ymax_xmin, ymax_xmax, ymax_zmin, ymax_zmax;
    pont zmin_xmin, zmin_xmax, zmin_ymin, zmin_ymax;
    pont zmax_xmin, zmax_xmax, zmax_ymin, zmax_ymax;

    bool is_first = true;
    cuns x_res = psim->pmodel->x_res, y_res = psim->pmodel->y_res, z_res = psim->pmodel->z_res;

    // befoglal� t�glatest meghat�roz�sa pixelben
    // a nyolc cs�cshoz legk�zelebbi pixel meghat�roz�sa

    for (i.z = 0; i.z < z_res; i.z++){
        const bitmap & bm = psim->pmodel->tbmp[i.z];
        for (i.y = 0; i.y < y_res; i.y++)
            for (i.x = 0; i.x < x_res; i.x++)
                if (bm.getpixel_also(i.x, i.y) == szinindex)
                    if (is_first){
                        min = max = i;
                        xmin_ymin = xmin_ymax = xmin_zmin = xmin_zmax = xmax_ymin = xmax_ymax = xmax_zmin = xmax_zmax = i;
                        ymin_xmin = ymin_xmax = ymin_zmin = ymin_zmax = ymax_xmin = ymax_xmax = ymax_zmin = ymax_zmax = i;
                        zmin_xmin = zmin_xmax = zmin_ymin = zmin_ymax = zmax_xmin = zmax_xmax = zmax_ymin = zmax_ymax = i;
                        is_first = false;
                    }
                    else{
                        if (i.x < min.x){
                            min.x = i.x;
                            xmin_ymin = xmin_ymax = xmin_zmin = xmin_zmax = i;
                        }
                        else if (i.x == min.x){
                            if (i.y < xmin_ymin.y) xmin_ymin = i;
                            if (i.y > xmin_ymax.y) xmin_ymax = i;
                            if (i.z < xmin_zmin.z) xmin_zmin = i;
                            if (i.z > xmin_zmax.z) xmin_zmax = i;
                        }

                        if (i.x > max.x){
                            max.x = i.x;
                            xmax_ymin = xmax_ymax = xmax_zmin = xmax_zmax = i;
                        }
                        else if (i.x == max.x){
                            if (i.y < xmax_ymin.y) xmax_ymin = i;
                            if (i.y > xmax_ymax.y) xmax_ymax = i;
                            if (i.z < xmax_zmin.z) xmax_zmin = i;
                            if (i.z > xmax_zmax.z) xmax_zmax = i;
                        }

                        if (i.y < min.y){
                            min.y = i.y;
                            ymin_xmin = ymin_xmax = ymin_zmin = ymin_zmax = i;
                        }
                        else if (i.y == min.y){
                            if (i.x < ymin_xmin.x) ymin_xmin = i;
                            if (i.x > ymin_xmax.x) ymin_xmax = i;
                            if (i.z < ymin_zmin.z) ymin_zmin = i;
                            if (i.z > ymin_zmax.z) ymin_zmax = i;
                        }

                        if (i.y > max.y){
                            max.y = i.y;
                            ymax_xmin = ymax_xmax = ymax_zmin = ymax_zmax = i;
                        }
                        else if (i.y == max.y){
                            if (i.x < ymax_xmin.x) ymax_xmin = i;
                            if (i.x > ymax_xmax.x) ymax_xmax = i;
                            if (i.z < ymax_zmin.z) ymax_zmin = i;
                            if (i.z > ymax_zmax.z) ymax_zmax = i;
                        }

                        if (i.z < min.z){
                            min.z = i.z;
                            zmin_xmin = zmin_xmax = zmin_ymin = zmin_ymax = i;
                        }
                        else if (i.z == min.z){
                            if (i.x < zmin_xmin.x) zmin_xmin = i;
                            if (i.x > zmin_xmax.x) zmin_xmax = i;
                            if (i.y < zmin_ymin.y) zmin_ymin = i;
                            if (i.y > zmin_ymax.y) zmin_ymax = i;
                        }

                        if (i.z > max.z){
                            max.z = i.z;
                            zmax_xmin = zmax_xmax = zmax_ymin = zmax_ymax = i;
                        }
                        else if (i.z == max.z){
                            if (i.x < zmax_xmin.x) zmax_xmin = i;
                            if (i.x > zmax_xmax.x) zmax_xmax = i;
                            if (i.y < zmax_ymin.y) zmax_ymin = i;
                            if (i.y > zmax_ymax.y) zmax_ymax = i;
                        }
                    }
    }
 
    // a csatorna kezd�- �s v�gpontj�nak meghat�roz�sa

    const tomb<dbl> & x_hossz = psim->pmodel->x_hossz;
    const tomb<dbl> & y_hossz = psim->pmodel->y_hossz;
    const tomb<dbl> & z_hossz = psim->pmodel->z_hossz;
    dbl W = x_hossz[2 * max.x] - x_hossz[2 * min.x];
    dbl H = y_hossz[2 * max.y] - y_hossz[2 * min.y];
    dbl L = z_hossz[2 * max.z] - z_hossz[2 * min.z];

    if (W >= H && W >= L){ // x ir�ny� az �raml�s
        if (H >= L){ // m�sodlagos ir�ny az y
            
            // meg kell n�zni, hogy fent vagy lent indul-e, �s az egyenes kezd�pontja beefoglal� kocka megfelel� pixel�nek oldalk�zepe lesz
            
            if (y_hossz[2 * xmin_ymin.y] - y_hossz[2 * min.y] <= y_hossz[2 * max.y] - y_hossz[2 * xmin_ymax.y]){ // az als� pixelb�l indul az egyenes
                start.x = (min.x == 0) ? nulla : x_hossz[2 * min.x - 1];
                start.y = y_hossz[2 * min.y];
                start.z = z_hossz[2 * xmin_ymin.z];
            }
            else{ // a fels� pixelb�l indul az egyenes
                start.x = (min.x == 0) ? nulla : x_hossz[2 * min.x - 1];
                start.y = y_hossz[2 * max.y];
                start.z = z_hossz[2 * xmin_ymax.z];
            }

            // az egyenes v�gpontja

            if (y_hossz[2 * xmax_ymin.y] - y_hossz[2 * min.y] <= y_hossz[2 * max.y] - y_hossz[2 * xmax_ymax.y]){ // az als� pixelben v�gz�dik az egyenes
                stop.x = x_hossz[2 * max.x + 1];
                stop.y = y_hossz[2 * min.y];
                stop.z = z_hossz[2 * xmax_ymin.z];
            }
            else{ // a fels� pixelben v�gz�dik az egyenes
                stop.x = x_hossz[2 * max.x + 1];
                stop.y = y_hossz[2 * max.y];
                stop.z = z_hossz[2 * xmax_ymax.z];
            }
        }
        else{ // m�sodlagos ir�ny a z
            if (z_hossz[2 * xmin_zmin.z] - z_hossz[2 * min.z] <= z_hossz[2 * max.z] - z_hossz[2 * xmin_zmax.z]){ // az als� pixelb�l indul az egyenes
                start.x = (min.x == 0) ? nulla : x_hossz[2 * min.x - 1];
                start.y = y_hossz[2 * xmin_zmin.y];
                start.z = z_hossz[2 * min.z];
            }
            else{ // a fels� pixelb�l indul az egyenes
                start.x = (min.x == 0) ? nulla : x_hossz[2 * min.x - 1];
                start.y = y_hossz[2 * xmin_zmax.y];
                start.z = z_hossz[2 * max.z];
            }
            if (z_hossz[2 * xmax_zmin.z] - z_hossz[2 * min.z] <= z_hossz[2 * max.z] - z_hossz[2 * xmax_zmax.z]){ // az als� pixelben v�gz�dik az egyenes
                stop.x = x_hossz[2 * max.x + 1];
                stop.y = y_hossz[2 * xmax_zmin.y];
                stop.z = z_hossz[2 * min.z];
            }
            else{ // a fels� pixelben v�gz�dik az egyenes
                stop.x = x_hossz[2 * max.x + 1];
                stop.y = y_hossz[2 * xmax_zmax.y];
                stop.z = z_hossz[2 * max.z];
            }
        }
    }
    else if (H >= L){ // y ir�ny� az �raml�s
        if (W >= L){ // m�sodlagos ir�ny az x
            if (x_hossz[2 * ymin_xmin.x] - x_hossz[2 * min.x] <= x_hossz[2 * max.x] - x_hossz[2 * ymin_xmax.x]){ // az als� pixelb�l indul az egyenes
                start.x = x_hossz[2 * min.x];
                start.y = (min.y == 0) ? nulla : y_hossz[2 * min.y - 1];
                start.z = z_hossz[2 * ymin_xmin.z];
            }
            else{ // a fels� pixelb�l indul az egyenes
                start.x = x_hossz[2 * max.x];
                start.y = (min.y == 0) ? nulla : y_hossz[2 * min.y - 1];
                start.z = z_hossz[2 * ymin_xmax.z];
            }
            if (x_hossz[2 * ymax_xmin.x] - x_hossz[2 * min.x] <= x_hossz[2 * max.x] - x_hossz[2 * ymax_xmax.x]){ // az als� piyelben v�gz�dik az egyenes
                stop.x = x_hossz[2 * min.x];
                stop.y = y_hossz[2 * max.y + 1];
                stop.z = z_hossz[2 * ymax_xmin.z];
            }
            else{ // a fels� pixelben v�gz�dik az egyenes
                stop.x = x_hossz[2 * max.x];
                stop.y = y_hossz[2 * max.y + 1];
                stop.z = z_hossz[2 * ymax_xmax.z];
            }
        }
        else{ // m�sodlagos ir�ny a z
            if (z_hossz[2 * ymin_zmin.z] - z_hossz[2 * min.z] <= z_hossz[2 * max.z] - z_hossz[2 * ymin_zmax.z]){ // az als� pixelb�l indul az egyenes
                start.x = x_hossz[2 * ymin_zmin.x];
                start.y = (min.y == 0) ? nulla : y_hossz[2 * min.y - 1];
                start.z = z_hossz[2 * min.z];
            }
            else{ // a fels� pixelb�l indul az egyenes
                start.x = x_hossz[2 * ymin_zmax.x];
                start.y = (min.y == 0) ? nulla : y_hossz[2 * min.y - 1];
                start.z = z_hossz[2 * max.z];
            }
            if (z_hossz[2 * ymax_zmin.z] - z_hossz[2 * min.z] <= z_hossz[2 * max.z] - z_hossz[2 * ymax_zmax.z]){ // az als� pixelben v�gz�dik az egyenes
                stop.x = x_hossz[2 * ymax_zmin.x];
                stop.y = y_hossz[2 * max.y + 1];
                stop.z = z_hossz[2 * min.z];
            }
            else{ // a fels� pixelben v�gz�dik az egyenes
                stop.x = x_hossz[2 * ymax_zmax.x];
                stop.y = y_hossz[2 * max.y + 1];
                stop.z = z_hossz[2 * max.z];
            }
        }
    }
    else{ // z ir�ny� az �raml�s
        if (W >= H){ // m�sodlagos ir�ny az x
            if (x_hossz[2 * zmin_xmin.x] - x_hossz[2 * min.x] <= x_hossz[2 * max.x] - x_hossz[2 * zmin_xmax.x]){ // az als� pixelb�l indul az egyenes
                start.x = x_hossz[2 * min.x];
                start.y = y_hossz[2 * zmin_xmin.y];
                start.z = (min.z == 0) ? nulla : z_hossz[2 * min.z - 1];
            }
            else{ // a fels� pixelb�l indul az egyenes
                start.x = x_hossz[2 * max.x];
                start.y = y_hossz[2 * zmin_xmax.y];
                start.z = (min.z == 0) ? nulla : z_hossz[2 * min.z - 1];
            }
            if (x_hossz[2 * zmax_xmin.x] - x_hossz[2 * min.x] <= x_hossz[2 * max.x] - x_hossz[2 * zmax_xmax.x]){ // az als� pizelben v�gy�dik az egyenes
                stop.x = x_hossz[2 * min.x];
                stop.y = y_hossz[2 * zmax_xmin.y];
                stop.z = z_hossz[2 * max.z + 1];
            }
            else{ // a fels� pixelben v�gy�dik az egyenes
                stop.x = x_hossz[2 * max.x];
                stop.y = y_hossz[2 * zmax_xmax.y];
                stop.z = z_hossz[2 * max.z + 1];
            }
        }
        else{ // m�sodlagos ir�ny a y
            if (y_hossz[2 * zmin_ymin.y] - y_hossz[2 * min.y] <= y_hossz[2 * max.y] - y_hossz[2 * zmin_ymax.y]){ // az als� pixelb�l indul az egyenes
                start.x = x_hossz[2 * zmin_ymin.x];
                start.y = y_hossz[2 * min.y];
                start.z = (min.z == 0) ? nulla : z_hossz[2 * min.z - 1];
            }
            else{ // a fels� pixelb�l indul az egyenes
                start.x = x_hossz[2 * zmin_ymax.x];
                start.y = y_hossz[2 * max.y];
                start.z = (min.z == 0) ? nulla : z_hossz[2 * min.z - 1];
            }
            if (y_hossz[2 * zmax_ymin.y] - y_hossz[2 * min.y] <= y_hossz[2 * max.y] - y_hossz[2 * zmax_ymax.y]){ // az als� pixelben v�gy�dik az egyenes
                stop.x = x_hossz[2 * zmax_ymin.x];
                stop.y = y_hossz[2 * min.y];
                stop.z = z_hossz[2 * max.z + 1];
            }
            else{ // a fels� pixelben v�gy�dik az egyenes
                stop.x = x_hossz[2 * zmax_ymax.x];
                stop.y = y_hossz[2 * max.y];
                stop.z = z_hossz[2 * max.z + 1];
            }
        }
    }
    // Mindig x n�veked�se ir�ny�ba folyik. Ha f�gg�leges, akkor y n�veked�se ir�ny�ba.
    if (fabs(start.x - stop.x) < 1e-9){
        if (start.y > stop.y)
            swap(start, stop);
    }
    else if (start.x > stop.x)
        swap(start, stop);

    if (is_reverse)
        swap(start, stop);

    // hossz kisz�m�t�sa
    
    length = Abs(stop - start);
/*
    printf("\n\n");
    printf("hossz=%g\n", length);
    printf("start.x=%g\tstop.x=%g\n", start.x, stop.x);
    printf("start.y=%g\tstop.y=%g\n", start.y, stop.y);
    printf("start.z=%g\tstop.z=%g\n", start.z, stop.z);
    printf("\n\n");
*/
}


//***********************************************************************
void uchannel::set_uchannel_in_bmp(){
//***********************************************************************
    // Minden szinindex sz�n� pixel k�z�pponth�r�l eld�nti, hogy a 
    // csatorna mely szakasz�hoz tartozik az n-b�l, �s be�ll�tja ennek
    // index�t (azaz tucha_perem[szinindex][i]-hez rendeli)

    // ALGORITMUS:
    // 1. Ha van k�t vektorunk: e �s f (k�z�s pontb�l: az orig�b�l indulnak),
    //    akkor skal�ris szorzatuk e*f=|e|*|f|*cos(alfa), alfa az �ltaluk
    //    bez�rt sz�g. Ez az egyik vektor mer�leges vet�let�t jelenti a m�sik
    //    vektorra. Vagyis |f|*cos(alfa)/|e| megmondja, hogy az f �ltal kije-
    //    l�lt ponthoz legk�zelebbi pont az orig�-e szakaszon hol van, milyen
    //    ar�nyban osztja kett� a szakaszt. (�rt�ke lehet negat�v nagy 1-n�l
    //    nagyobb is, ha az f pont nem az origo-e szakasz mellett van.
    // 2. e = stop - start, f = cellakozep - start
    // 3. e*f = |e|*|f|*cos(alfa) => |f|*cos(alfa) = e*f/|e|
    //    A vet�t�si ar�ny teh�t: 
    //    va = |f|*cos(alfa)/|e| = e*f/|e|^2 = (e*f)/(e*e)
    //    if(va<0.0) va = 0; if(va>=1.0) va = 1.0 - 1.0e-010; (a -1e-10 az�rt kell, hogy max. n-1-re kerek�tsen.
    //    A keresett index = (int)(n*va);

    tomb<uns> letezo_indexek;

    cuns x_res = psim->pmodel->x_res, y_res = psim->pmodel->y_res, z_res = psim->pmodel->z_res;
    const tomb<dbl> & x_hossz = psim->pmodel->x_hossz;
    const tomb<dbl> & y_hossz = psim->pmodel->y_hossz;
    const tomb<dbl> & z_hossz = psim->pmodel->z_hossz;
    const Vec3d e = stop - start;
    cd egy_per_hossz_negyzet = egy / (e*e);
    cd majdnemegy = egy - 1.0e-010;

    for (uns z = 0; z < z_res; z++){
        bitmap & bm = psim->pmodel->tbmp[z];
        for (uns y = 0; y < y_res; y++)
            for (uns x = 0; x < x_res; x++)
                if (bm.getpixel_also(x, y) == szinindex){
                    const Vec3d cellakozep(x_hossz[2 * x], y_hossz[2 * y], z_hossz[2 * z]);
                    const Vec3d f = cellakozep - start;
                    cd va = e*f*egy_per_hossz_negyzet;
                    cd vacut = va < nulla ? nulla : va >= egy ? majdnemegy : va;
                    cuns index = (uns)(n*vacut);
                    bm.setpixel_felso(x, y, index);
                    letezo_indexek.insertIfNotExists(index);
                }
    }

    // Ha a felhaszn�l� t�l nagy n-t adott meg, t�r�lj�k a felesleges szegmenseket, �s n �rt�k�t a megfelel� �rt�kre cs�kkentj�k

    for (uns z = 0; z < z_res; z++){
        bitmap & bm = psim->pmodel->tbmp[z];
        for (uns y = 0; y < y_res; y++)
            for (uns x = 0; x < x_res; x++)
                if (bm.getpixel_also(x, y) == szinindex)
                    if (letezo_indexek.findSorted(bm.getpixel_felso(x, y)))
                        bm.setpixel_felso(x, y, letezo_indexek.getfindindex());
                    else
                        throw hiba("uchannel::set_uchannel_in_bmp", "Impossible index");
    }
    n = letezo_indexek.size();
}

//***********************************************************************
void uchannel::init2(PLString path, PLString fajlnev, const PLString &cimke){
//***********************************************************************
// beolvassa a f�jlt, az alapj�n be�ll�tja a param�tereket
// meghat�rozza a csatorna kezd� �s v�gpontj�t, kisz�molja a csatornahosszt, felosztja n r�szre
// be�ll�tja a modell tbmp t�mbj�ben a csatorn�hoz tartoz� sz�n� pixelek fels� �rt�k�nek a csatornar�sz sorsz�m�t
// �tm�retezi tucha_perem t�mb szinindexedik elem�t, a vezet�seknek kezd��rt�ket ad

    // kezdeti �rt�kek

    nev = cimke;
    n = 8;
    tipus = CsatTrap;
    is_auto_wall_temp = true;
    is_reverse = false;
    flow_rate = 1.0;
    fixed_wall_temp = 60.0;
    fluid_temp = 0.0;
    width_bottom = 250e-6;
    width_top = 350e-6;
    roughness = 1.0e-6;
    height = 67e-6;
    density = 1.1614;
    dynamic_visc = 1.84e-5;
    spec_heat = 1005.0;
    heat_cond = 0.0261;

    read(path, fajlnev);
    set_start_stop_length();
    set_uchannel_in_bmp();

    psim->tucha_perem[szinindex].resize(n);
}

//***********************************************************************
void powermap::map_t::read(srfajl & fajl, uns & sor, u32 x, u32 y, u32 z){
//***********************************************************************
    pmap.resize(x, y, z);
    uns sorindex = 0;
    for (uns k = 0; k < z; k++)
        for (uns j = 0; j < y; j++)
            for (uns i = 0; i < x; i++, sorindex++) {
                if (sorindex >= fajl.lines()[sor].size()) {
                    sorindex = 0;
                    sor++;
                }
                if (!fajl.lines()[sor][sorindex].todouble(pmap.getref(i, j, k)))
                    throw hiba("powermap::map_t::read", "power value is not a number (%s) (t=%g)", fajl.lines()[sor][sorindex].c_str(),t);
            }
    sor++;
}


//***********************************************************************
void powermap::read(PLString path, PLString fajlnev) {
//***********************************************************************
    const char * fvnev = "powermap::read()";
    logprint("Read %s", fajlnev.c_str());
    srfajl fajl;
    fajl.open(path + fajlnev);

    for (uns i = 0; i < fajl.lines().size(); i++) {
        PLString azon = fajl.lines()[i][0].LowCase();
        if (azon == "vsun3-power-map")
            continue;
        if (azon == "top") {
            hol = Hol::top;
            continue;
        }
        if (azon == "volume") {
            hol = Hol::volume;
            continue;
        }
        if (azon == "exact") {
            is_exact = true;
            continue;
        }
        if (azon == "x-resolution") {
            if (!fajl.lines()[i][1].tounsigned(x))
                throw hiba(fvnev, "segment_number is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "y-resolution") {
            if (!fajl.lines()[i][1].tounsigned(y))
                throw hiba(fvnev, "segment_number is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "z-resolution") {
            if (!fajl.lines()[i][1].tounsigned(z))
                throw hiba(fvnev, "segment_number is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
            continue;
        }
        if (azon == "x-pitch") {
            if (!pitchconverter(fajl.lines()[i], x_pitch))
                throw hiba(fvnev, "x-pitch data corrupt in line %u in %s", i, fajlnev.c_str());
            if (x_pitch.size()<x)
                throw hiba(fvnev, "fewer x-pitch data than resolution (%u<%u) in line %u in %s", 
                    x_pitch.size(), x, i, fajlnev.c_str());
            if (x_pitch.size()>x)
                logprint("warning: model::read() => more x-pitch data than resolution (%u>%u) in line %u in %s", 
                    x_pitch.size(), x, i, fajlnev.c_str());
            continue;
        }
        if (azon == "y-pitch") {
            if (!pitchconverter(fajl.lines()[i], y_pitch))
                throw hiba(fvnev, "y-pitch data corrupt in line %u in %s", i, fajlnev.c_str());
            if (y_pitch.size()<y)
                throw hiba(fvnev, "fewer y-pitch data than resolution (%u<%u) in line %u in %s", 
                    y_pitch.size(), y, i, fajlnev.c_str());
            if (y_pitch.size()>y)
                logprint("warning: model::read() => more y-pitch data than resolution (%u>%u) in line %u in %s", 
                    y_pitch.size(), y, i, fajlnev.c_str());
            continue;
        }
        if (azon == "z-pitch") {
            if (!pitchconverter(fajl.lines()[i], z_pitch))
                throw hiba(fvnev, "z-pitch data corrupt in line %u in %s", i, fajlnev.c_str());
            if (z_pitch.size()<z)
                throw hiba(fvnev, "fewer z-pitch data than resolution (%u<%u) in line %u in %s", 
                    z_pitch.size(), z, i, fajlnev.c_str());
            if (z_pitch.size()>z)
                logprint("warning: model::read() => more z-pitch data than resolution (%u>%u) in line %u in %s", 
                    z_pitch.size(), z, i, fajlnev.c_str());
            continue;
        }
        if (azon == "t") {
            uns db = 0;
            for (uns j = i; j < fajl.lines().size(); j++) {
                PLString az = fajl.lines()[j][0].LowCase();
                if (az == "t")
                    db++;
            }
            tombok.resize(db);
            if (hol == Hol::top)
                z = 1;
            for (uns j = 0; j < db; j++) {
                PLString az = fajl.lines()[i][0].LowCase();
                if(az!="t")
                    throw hiba(fvnev, "identifier should be \"t\" instead of %s in %u. iteration of %u in line %u in %s", az.c_str(), j, db, i, fajlnev.c_str());
                if (!fajl.lines()[i][1].todouble(tombok[j].t))
                    throw hiba(fvnev, "t is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fajlnev.c_str());
                i++;
                tombok[j].read(fajl, i, x, y, z);
            }
            continue;
        }
        if (azon == "stop") {
            is_stop = true;
            continue;
        }
        if (azon == "end") {
            if (!is_exact) {
                if (x_pitch.size()<x)throw hiba(fvnev, "x-pitch is missing in %s", fajlnev.c_str());
                if (y_pitch.size()<y)throw hiba(fvnev, "y-pitch is missing in %s", fajlnev.c_str());
                if (hol==Hol::volume && z_pitch.size()<z)throw hiba(fvnev, "z-pitch is missing in %s", fajlnev.c_str());
            }
            break;
        }
        throw hiba(fvnev, "unknown identifier (%s) in %s", fajl.lines()[i][0].c_str(), fajlnev.c_str());
    }
    van_e = true;
}


//***********************************************************************
void powermap::get_map(dbl t, dbl * map){
// A map psim->pmodel->x_res * psim->pmodel->y_res * psim->pmodel->z_res
// m�ret� kell legyen. Top map eset�n az als�bbakat null�zza.
//***********************************************************************
    if (psim->pmodel->x_res != x)
        throw hiba("powermap::get_map", "model and powermap x-resolution is not equal (%u!=%u)", psim->pmodel->x_res, x);
    if (psim->pmodel->y_res != y)
        throw hiba("powermap::get_map", "model and powermap y-resolution is not equal (%u!=%u)", psim->pmodel->y_res, y);
    if (hol == Hol::volume && psim->pmodel->z_res != z)
        throw hiba("powermap::get_map", "model and powermap z-resolution is not equal (%u!=%u)", psim->pmodel->z_res, z);

    // Az els� t-ig nincs disszip�ci�, vagy ha stop be van �ll�tva, akkor utols� t ut�n nincs disszip�ci�

    if (tombok[0].t >= t + 1e-12 || (is_stop && tombok[tombok.size()-1].t < t - 1e-12)) {
        cuns db = x*y*psim->pmodel->z_res;
        for (uns i = 0; i < db; i++)
            map[i] = nulla;
        return;
    }    
    
    // amikort�l a disszip�ci� defini�lva van, h�nyadik mape-et haszn�lja

    uns hanyadik = 0;
    for (uns i = 1; i < tombok.size(); i++)
        if (tombok[i].t >= t + 1e-12) {
            hanyadik = i - 1;
            break;
        }
        else if (i == tombok.size() - 1)
            hanyadik = i;

    switch (hol) {
        case Hol::top: {
            int n = 0;
            for (uns k = 0; k < psim->pmodel->z_res - 1; k++)
                for (uns j = 0; j < y; j++)
                    for (uns i = 0; i < x; i++, n++) {
                        map[n] = nulla;
                    }
            cuns eltolas = x * y * (psim->pmodel->z_res - 1);
            for (uns k = psim->pmodel->z_res - 1; k < psim->pmodel->z_res; k++)
                for (uns j = 0; j < y; j++)
                    for (uns i = 0; i < x; i++, n++) {
                        map[n] = tombok[hanyadik].pmap.getconstref(n - eltolas);
                    }
        }
            break;
        case Hol::volume: {
                int n = 0;
                for (uns k = 0; k < z; k++)
                    for (uns j = 0; j < y; j++)
                        for (uns i = 0; i < x; i++, n++) {
                            map[n] = tombok[hanyadik].pmap.getconstref(n);
                        }
            }
            break;
    }
}


//***********************************************************************
void simulation::read(PLString path){
//***********************************************************************
    const char * fvnev="simulation::read()";
    logprint("Read %s",fileName.c_str());
    srfajl fajl;
    fajl.open(path+fileName);
    if(fajl.lines().size()<1||fajl.lines()[0][0].LowCase()!="vsun3-simulation")
        throw hiba(fvnev,"first line is not vsun3-model in %s",fileName.c_str());
    uns i=0;
    sorazon az=az_unknown;

    // name

    try{
        do{
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_nev && az!=az_unknown)throw hiba(fvnev,"name is missing in line %u in %s",i,fileName.c_str());
        }while(az!=az_nev);
        name=fajl.lines()[i][1];

        // field

        do{
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_field&& az!=az_unknown)throw hiba(fvnev,"field is missing in line %u in %s",i,fileName.c_str());
        }while(az!=az_field);
        PLString kisb=fajl.lines()[i][1].LowCase();
        if(kisb=="electrical")mezo=FieldEl;
        else if(kisb=="thermal")mezo=FieldTherm;
        else if(kisb=="eltherm")mezo=FieldElTherm;
        else throw hiba(fvnev,"unknown field type (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());

        // computation

        bool cango=false;
        do{ // az!=az_comp && az!=az_bound && az!=az_convection && az!=az_cpu && az!=az_hdd && az!=az_optimize
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            switch(az){
                case az_lin:       is_lin=true;         break;
                case az_nosemi:    is_no_semi=true;     break;
                case az_noseebeck: is_no_seebeck=true;  break;
                case az_nopeltier: is_no_peltier=true;  break;
                case az_peltier_center: is_peltier_center=true;  break;
                case az_nothomson: is_no_thomson=true;  break;
                case az_no_joule:  is_no_joule=true;    break;
                case az_noaccelerator: is_no_accelerator=true;  break;
                case az_cm_temp:   aramot=false;        break;
                case az_el_nonlin_subiter:
                    if(!fajl.lines()[i][1].tounsigned(el_nonlin_subiter))
                        throw hiba(fvnev,"el_nonlin_subiter is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
                    break;
                case az_ndc_miniter:
                    if (!fajl.lines()[i][1].tounsigned(ndc_miniter))
                        throw hiba(fvnev, "ndc_miniter is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fileName.c_str());
                    break;
                case az_comp:
                    if(!fajl.lines()[i][1].tounsigned(valostipus))
                        throw hiba(fvnev,"computation is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
                    break;
                case az_always_strassen_mul: is_always_strassen_mul=true;  break;
                case az_always_quad: is_always_quad=true;  break;
                case az_bound:      cango=true;   break;
                case az_convection: cango=true;   break;
                case az_cpu:
                    if(!fajl.lines()[i][1].tounsigned(cpu_threads))
                        throw hiba(fvnev,"cpu-threads is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
                    break;
                case az_hdd:
                    if(!fajl.lines()[i][1].tounsigned(hdd))
                        throw hiba(fvnev,"hdd-cache is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
                    break;
                case az_optimize:
                    if(!fajl.lines()[i][1].tounsigned(optimize))
                        throw hiba(fvnev,"optimize is not a number (%s) in %s",fajl.lines()[i][1].c_str(),fileName.c_str());
                    break;
                case az_unknown:                break;
                default: throw hiba(fvnev,"linear or no_semicond or no_seebeck or computation or cpu_threads or hdd-cache or optimize or boundary is missing in line %u in %s",i,fileName.c_str());
            }
        }while(!cango);

        // convection

        material m_dummy;
        bool isel;
        while(az!=az_bound){
            switch(az){
                case az_unknown: break;
                case az_convection:{
                        cu32 n=tconv.incsize();
                        tconv[n-1].nev=fajl.lines()[i][1].LowCase();
                        tconv[n-1].is_defined=true;
                        if(tconv[n-1].nev=="")throw hiba(fvnev,"missing convection name in line %u in %s",i,fileName.c_str());
                    }
                    break;
                case az_conv:{
                        cu32 n=tconv.size();
                        switch(azon_conv(fajl.lines()[i][1].LowCase())){
                            case caz_unknown: break;
                            case caz_rad:
                                if(!R_converter(fajl.lines()[i],2,tconv.getLast().radiation,m_dummy))
                                    throw hiba(fvnev,"conv>radiation missing multiplier value in line %u in %s",i,fileName.c_str());
                                break;
                            case caz_vertical:
                                if(fajl.lines()[i][2].LowCase()=="htc")             tconv[n-1].vertical_tipus=ConvHTC;
                                else if(fajl.lines()[i][2].LowCase()=="vertical-1") tconv[n-1].vertical_tipus=ConvVertical_1; 
                                else if(fajl.lines()[i][2].LowCase()=="churchill-p1") tconv[n-1].vertical_tipus=ConvVerticalChurchill_P_1; 
                                else if(fajl.lines()[i][2].LowCase()=="churchill-p2") tconv[n-1].vertical_tipus=ConvVerticalChurchill_P_2; 
                                else if(fajl.lines()[i][2].LowCase()=="churchill-t") tconv[n-1].vertical_tipus=ConvVerticalChurchill_T; 
                                else if(fajl.lines()[i][2].LowCase()=="churchill-c") tconv[n-1].vertical_tipus=ConvVerticalChurchill_C; 
                                else if(fajl.lines()[i][2].LowCase()=="lee-t") tconv[n-1].vertical_tipus=ConvVerticalLee_T;
                                else if(fajl.lines()[i][2].LowCase()=="lee-p") tconv[n-1].vertical_tipus=ConvVerticalLee_P;
                                else if(fajl.lines()[i][2].LowCase()=="mihajev") tconv[n-1].vertical_tipus=ConvVerticalMihajev;
                                else if(fajl.lines()[i][2].LowCase()=="wei") tconv[n-1].vertical_tipus=ConvWei;
                                else throw hiba(fvnev,"conv>free-vertical unknown type \"%s\" in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                                
                                if(!R_converter(fajl.lines()[i],3,tconv[n-1].vertical_value,m_dummy))
                                    throw hiba(fvnev,"conv>free-vertical multiplier value in line %u in %s",i,fileName.c_str());
                                break;
                            case caz_lower:
                                if(fajl.lines()[i][2].LowCase()=="htc")          tconv[n-1].lower_tipus=ConvHTC;
                                else if(fajl.lines()[i][2].LowCase()=="lower-1") tconv[n-1].lower_tipus=ConvLower_1;
                                else if(fajl.lines()[i][2].LowCase()=="yovanovich-min") tconv[n-1].lower_tipus=ConvYovanovichMin;
                                else if(fajl.lines()[i][2].LowCase()=="yovanovich-max") tconv[n-1].lower_tipus=ConvYovanovichMax;
                                else if(fajl.lines()[i][2].LowCase()=="wei") tconv[n-1].lower_tipus=ConvWei;
                                else throw hiba(fvnev,"conv>free-lower unknown type \"%s\" in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                                
                                if(!R_converter(fajl.lines()[i],3,tconv[n-1].lower_value,m_dummy))
                                    throw hiba(fvnev,"conv>free-lower multiplier value in line %u in %s",i,fileName.c_str());
                                break;
                            case caz_upper:
                                if(fajl.lines()[i][2].LowCase()=="htc")          tconv[n-1].upper_tipus=ConvHTC;
                                else if(fajl.lines()[i][2].LowCase()=="upper-1") tconv[n-1].upper_tipus=ConvUpper_1;
                                else if(fajl.lines()[i][2].LowCase()=="wei") tconv[n-1].upper_tipus=ConvWei;
                                else throw hiba(fvnev,"conv>free-upper unknown type \"%s\" in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                                
                                if(!R_converter(fajl.lines()[i],3,tconv[n-1].lower_value,m_dummy))
                                    throw hiba(fvnev,"conv>rfree-upper multiplier value in line %u in %s",i,fileName.c_str());
                                break;
                            case caz_axis:
                                if(fajl.lines()[i][2].LowCase()=="x")       tconv[n-1].axis=X_IRANY;
                                else if(fajl.lines()[i][2].LowCase()=="y")  tconv[n-1].axis=Y_IRANY;
                                else if(fajl.lines()[i][2].LowCase()=="z")  tconv[n-1].axis=Z_IRANY;
                                else throw hiba(fvnev,"conv>axis unknown axis \"%s\" in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                                break;
                            case caz_angle:
                                if(!fajl.lines()[i][2].todouble(tconv[n-1].angle))
                                    throw hiba(fvnev,"conv>angle not a number \"%s\" in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                                if(tconv[n-1].angle<=-360 || tconv[n-1].angle>=360)
                                    throw hiba(fvnev,"conv>angle is not in -360<\"%s\"<360 range in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                                if(tconv[n-1].angle<0)tconv[n-1].angle+=360;
                                break;
                            case caz_edge:
                                if(fajl.lines()[i][2].LowCase()=="wei-h")        tconv[n-1].edge=ConvEdgeWei_H;
                                else if(fajl.lines()[i][2].LowCase()=="wei-i")   tconv[n-1].edge=ConvEdgeWei_I;
                                else if(fajl.lines()[i][2].LowCase()=="wei-hi")  tconv[n-1].edge=ConvEdgeWei_HI;
                                else if(fajl.lines()[i][2].LowCase()=="wei-hh")  tconv[n-1].edge=ConvEdgeWei_HH;
                                else throw hiba(fvnev,"conv>edge unknown type \"%s\" in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                                break;
                        }
                    }
                    break;
            }
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_bound && az!=az_convection && az!=az_conv && az!=az_unknown)
                throw hiba(fvnev,"convection or conv or boundary is missing in line %u in %s",i,fileName.c_str());
        }

        // boundary, electrical

        if(mezo==FieldEl||mezo==FieldElTherm){
            if(fajl.lines()[i][1].LowCase()!="electrical")
                throw hiba(fvnev,"missing electrical boundary in line %u in %s",i,fileName.c_str());
            if(!bouolvas(fajl.lines()[i],normalperem,isel,tconv,path,pmodel->x_res,pmodel->y_res))
                throw hiba(fvnev,"electrical boundary type (%s) or value is wrong in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_bound && az!=az_bou && az!=az_excita && az!=az_ambient && az!=az_unknown)
                    throw hiba(fvnev,"boundary or bou or excitation or ambient is missing in line %u in %s",i,fileName.c_str());
            }while(az==az_unknown);
            while(az==az_bou){
                if(!bouolvas(fajl.lines()[i],normalperem,isel,tconv,path,pmodel->x_res,pmodel->y_res))
                    throw hiba(fvnev,"electrical boundary type (%s) or value is wrong in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                do{
                    i++;
                    az=azonosit(fajl.lines()[i][0].LowCase());
                    if(az!=az_bound && az!=az_bou && az!=az_excita && az!=az_ambient && az!=az_unknown)
                        throw hiba(fvnev,"boundary or bou or excitation or ambient is missing in line %u in %s",i,fileName.c_str());
                }while(az==az_unknown);
            }
            if(pmodel->dim < 3){
                normalperem.el[BOTTOM].tipus = normalperem.el[TOP].tipus = PeremOpen;
            }
        }

        // boundary, thermal

        if(mezo==FieldTherm||mezo==FieldElTherm){
            while(az!=az_bound){
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_bound && az!=az_unknown)
                    throw hiba(fvnev,"thermal boundary is missing in line %u in %s",i,fileName.c_str());
            }
            if(fajl.lines()[i][1].LowCase()!="thermal")
                throw hiba(fvnev,"missing thermal boundary in line %u in %s",i,fileName.c_str());
            if(!bouolvas(fajl.lines()[i],normalperem,isel,tconv,path,pmodel->x_res,pmodel->y_res))
                throw hiba(fvnev,"thermal boundary type (%s) or value is wrong in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_bound && az!=az_bou && az!=az_excita && az!=az_ambient && az!=az_unknown)
                    throw hiba(fvnev,"boundary or bou or excitation or ambient is missing in line %u in %s",i,fileName.c_str());
            }while(az==az_unknown);
            while(az==az_bou){
                if(!bouolvas(fajl.lines()[i],normalperem,isel,tconv,path,pmodel->x_res,pmodel->y_res))
                    throw hiba(fvnev,"thermal boundary type (%s) or value is wrong in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                do{
                    i++;
                    az=azonosit(fajl.lines()[i][0].LowCase());
                    if(az!=az_bound && az!=az_bou && az!=az_excita && az!=az_ambient && az!=az_unknown)
                        throw hiba(fvnev,"boundary or bou or excitation or ambient is missing in line %u in %s",i,fileName.c_str());
                }while(az==az_unknown);
            }
            if(pmodel->dim < 3){
                normalperem.th[BOTTOM].tipus = normalperem.th[TOP].tipus = PeremOpen;
            }
        }

        // boundary, internal

        tinner.clear();
        uns darab=0;
        for(uns j=0;j<colmax;j++)if(pmodel->tcolor[j].is && pmodel->tcolor[j].tipus==SzinBoundary)darab++;
        for(uns j=0;j<colmax;j++)innerindex[j]=colmax;
        tinner.resize(darab);
        for(;darab>0;darab--){
            while(az!=az_bound){
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_bound && az!=az_unknown)
                    throw hiba(fvnev,"internal boundary is missing in line %u in %s",i,fileName.c_str());
            }
            if(fajl.lines()[i][1].LowCase()!="internal")
                throw hiba(fvnev,"missing internal boundary in line %u in %s",i,fileName.c_str());
            const PLString cimke=fajl.lines()[i][2].LowCase();
            for(uns j=0;j<colmax && tinner[darab-1].szin==colmax;j++)
                if(pmodel->tcolor[j].is && pmodel->tcolor[j].tipus==SzinBoundary && cimke==pmodel->tcolor[j].nev)tinner[darab-1].szin=j;
            if(tinner[darab-1].szin==colmax)
                throw hiba(fvnev,"undefined internal boundary label (%s) in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
            
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_bound && az!=az_bou && az!=az_excita && az!=az_ambient && az!=az_unknown)
                    throw hiba(fvnev,"boundary or bou or excitation or ambient is missing in line %u in %s",i,fileName.c_str());
            }while(az==az_unknown);
            bool voltel=false,voltth=false;
            while(az==az_bou){
                if(fajl.lines()[i][1].LowCase()=="electrical")voltel=true;
                if(fajl.lines()[i][1].LowCase()=="thermal")voltth=true;
                if(!bouolvas(fajl.lines()[i],tinner[darab-1],isel,tconv,path,pmodel->x_res,pmodel->y_res))
                    throw hiba(fvnev,"internal boundary type (%s) or value is wrong in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                do{
                    i++;
                    az=azonosit(fajl.lines()[i][0].LowCase());
                    if(az!=az_bound && az!=az_bou && az!=az_excita && az!=az_ambient && az!=az_unknown)
                        throw hiba(fvnev,"boundary or bou or excitation or ambient is missing in line %u in %s",i,fileName.c_str());
                }while(az==az_unknown);
            }
            if((mezo==FieldEl||mezo==FieldElTherm)&&!voltel)
                throw hiba(fvnev,"missing electrical internal boundary definition in line %u in %s",i,fileName.c_str());
            if((mezo==FieldTherm||mezo==FieldElTherm)&&!voltth)
                throw hiba(fvnev,"missing thermal internal boundary definition in line %u in %s",i,fileName.c_str());
        }
        for(uns j=0;j<tinner.size();j++)innerindex[tinner[j].szin]=j;

        // excitation

        while(az!=az_excita && az!=az_ambient){
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if (az==az_bou || az==az_bound)
                throw hiba(fvnev, "unnecessary boundary conditin in line %u in %s (internal boundary is turned off?)", i, fileName.c_str());
            if (az != az_excita && az != az_ambient && az != az_unknown)
                throw hiba(fvnev,"excitation or ambient is missing in line %u in %s",i,fileName.c_str());
        }
        while(az==az_excita){
            bool isel=false;
            const PLString kisb=fajl.lines()[i][1].LowCase();
            if(kisb=="electrical")isel=true;
            else if(kisb=="thermal")isel=false;
            else throw hiba(fvnev,"excitation type is wrong (%s) in line %u in %s",fajl.lines()[i][1].c_str(),i,fileName.c_str());
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_excit && az!=az_excita && az!=az_ambient && az!=az_unknown)
                    throw hiba(fvnev,"excitation or excit or ambient is missing (%s found)in line %u in %s",fajl.lines()[i][0].c_str(),i,fileName.c_str());
                if(az==az_excit){
                    PLString kisb=fajl.lines()[i][1].LowCase();
                    uns szin=colmax;
                    for(uns j=0;j<colmax && szin==colmax;j++){
                        const color & cc=pmodel->tcolor[j];
                        if(cc.is && cc.tipus==SzinNormal && cc.nev==kisb)szin=j;
                    }
                    if(szin==colmax)
                        throw hiba(fvnev,"missing color label (%s) in excit definition in line %u in %s",fajl.lines()[i][1].c_str(),i,fileName.c_str());
                    if(isel){
                        texcitE[szin].is=true;
                        if(!excitvalue(fajl.lines()[i],texcitE[szin],mulE[szin]))                        
                            throw hiba(fvnev,"missing or bad excitation value in excit definition (%s) in line %u in %s",fajl.lines()[i][1].c_str(),i,fileName.c_str());
                    }
                    else{
                        texcitT[szin].is=true;
                        if(!excitvalue(fajl.lines()[i],texcitT[szin],mulT[szin]))                        
                            throw hiba(fvnev,"missing or bad excitation value in excit definition (%s) in line %u in %s",fajl.lines()[i][1].c_str(),i,fileName.c_str());
                    }
                }
            }while(az==az_unknown || az==az_excit);
        }

        // ambient

        while(az!=az_ambient){
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_ambient && az!=az_unknown)
                throw hiba(fvnev,"ambient is missing in line %u in %s",i,fileName.c_str());
        }
        ambiT=nulla;
        dbl dummy;
        if(mezo==FieldEl?false
            :mezo==FieldTherm?!fajl.lines()[i][1].todouble(ambiT)
            :mezo==FieldElTherm?!fajl.lines()[i][1].to2double(dummy,ambiT,true)
            :true)
            throw hiba(fvnev,"incorrect ambient value (%s) in line %u in %s",fajl.lines()[i][1].c_str(),i,fileName.c_str());
        if(fajl.lines()[i].size()>2){
            // t�bb k�ls� h�m�rs�kleten kell futtatni
            mulAmbiT.resize(fajl.lines()[i].size()-2);
            for(uns j=0; j<mulAmbiT.size(); j++){
                if(!fajl.lines()[i][j+2].todouble(mulAmbiT[j]))
                    throw hiba(fvnev,"incorrect ambient value (%s) in line %u in %s",fajl.lines()[i][1].c_str(),i,fileName.c_str());
            }
        }

        // powermap

        i++;
        az = azonosit(fajl.lines()[i][0].LowCase());
        while (az == az_unknown || az == az_powermap) {
            if (az == az_powermap) {
                pmap.init(this);
                pmap.read(path, fajl.lines()[i][1]);
                pmap.rescale();
            }
            i++;
            az = azonosit(fajl.lines()[i][0].LowCase());
        }

        // uchannel

        //      tucha t�mb inicializ�l�sa

        uns uchadb = 0, ix;
        for (ix = 0; ix < colmax; ix++)
            if (pmodel->tcolor[ix].is && pmodel->tcolor[ix].tipus == SzinUchannel)
                uchadb++;

        tucha.resize(uchadb);

        for (ix = uchadb = 0; ix < colmax; ix++)
            if (pmodel->tcolor[ix].is && pmodel->tcolor[ix].tipus == SzinUchannel){
                tucha[uchadb].init1(this, ix, &pmodel->tcolor[ix]);
                uchadb++;
            }

        //      uchannel f�jl nev�nek beolvas�sa �s tucha t�mb elemeinek inicializ�l�sa

        uchadb = 0;
        while (az == az_unknown || az == az_uchannel){
            if (az == az_uchannel){
                const PLString cimke = fajl.lines()[i][1].LowCase();
                uns j;
                for (j = 0; j < tucha.size() && tucha[j].getLabel() != cimke; j++);
                if (j == tucha.size())
                    throw hiba(fvnev, "unknown uchannel label (%s) in line %u in %s", cimke.c_str(), i, fileName.c_str());
                tucha[j].init2(path, fajl.lines()[i][2], cimke);
                uchadb++;
            }
            i++;
            az = azonosit(fajl.lines()[i][0].LowCase());
        }
        if (uchadb != tucha.size())
            throw hiba(fvnev, "missing uchannel definition (%u!=%u) in line %u in %s", uchadb, tucha.size(), i, fileName.c_str());

        // probe

        while(az!=az_probe && az!=az_fimtxt && az != az_commas && az != az_FIM_res && az != az_FIM_diff && az!=az_nofim 
            && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps 
            && az != az_analy && az!=az_end && az != az_no_plus_step_data){
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_probe && az!=az_analy && az!=az_fimtxt && az != az_commas && az != az_FIM_res 
                && az != az_FIM_diff && az!=az_nofim && az!=az_end && az!=az_unknown && az != az_no_plus_step_data
                && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps)
                throw hiba(fvnev,"probe or map_to_txt or FIM_res or FIM_diff or use_commas or no_images or auto_transi_steps or analysis or end is missing in line %u in %s",i,fileName.c_str());
        }
        tproV.clear();
        tproT.clear();
        while(az==az_probe){
            const PLString kisb=fajl.lines()[i][1].LowCase();
            ProbeTipus pr;
            if(kisb=="voltage")pr=ProbeV;
            else if(kisb=="temperature")pr=ProbeT;
            else if(kisb=="current")pr=ProbeC;
            else if(kisb=="map")pr=ProbeM;
            else if(kisb=="section")pr=ProbeS;
            else throw hiba(fvnev,"unknown probe type (%s) in line %u in %s",fajl.lines()[i][1].c_str(),i,fileName.c_str());
            uns aktindex=0;
            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_pro && az!=az_probe && az!=az_analy && az!=az_fimtxt && az != az_commas && az != az_FIM_res 
                    && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps
                    && az != az_FIM_diff && az!=az_nofim && az!=az_end && az!=az_unknown && az != az_no_plus_step_data)
                    throw hiba(fvnev,"pro or probe or auto_transi_steps or analysis or end is missing (%s found)in line %u in %s",fajl.lines()[i][0].c_str(),i,fileName.c_str());
                if(az==az_pro){
                    if(pr==ProbeV)tproV.resize((aktindex=tproV.size())+1);
                    else if(pr==ProbeT)tproT.resize((aktindex=tproT.size())+1);
                    else if(pr==ProbeC)tproC.resize((aktindex=tproC.size())+1);
                    else if(pr==ProbeM)tproM.resize((aktindex=tproM.size())+1);
                    else if(pr==ProbeS)tproS.resize((aktindex=tproS.size())+1);
                    if(pr==ProbeV)tproV[aktindex].cimke=fajl.lines()[i][1].LowCase();
                    else if(pr==ProbeT)tproT[aktindex].cimke=fajl.lines()[i][1].LowCase();
                    else if(pr==ProbeC)tproC[aktindex].cimke=fajl.lines()[i][1].LowCase();
                    else if(pr==ProbeM)tproM[aktindex].cimke=fajl.lines()[i][1].LowCase();
                    else if(pr==ProbeS)tproS[aktindex].cimke=fajl.lines()[i][1].LowCase();
                    PLString aktpar=fajl.lines()[i][2].LowCase();
                    if(pr==ProbeV||pr==ProbeT||pr==ProbeC||pr==ProbeM){
                        Oldal dal;
                        if(aktpar=="center")dal=CENTER;
                        else if(aktpar=="west")dal=WEST;
                        else if(aktpar=="east")dal=EAST;
                        else if(aktpar=="south")dal=SOUTH;
                        else if(aktpar=="north")dal=NORTH;
                        else if(aktpar=="top")dal=TOP;
                        else if(aktpar=="bottom")dal=BOTTOM;
                        else throw hiba(fvnev,"unknown probe place (%s) in line %u in %s (center, west, etc. needed)",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                        if(pr==ProbeV){
                            tproV[aktindex].oldal=dal;
                            if(!fajl.lines()[i][3].to3uns(tproV[aktindex].x,tproV[aktindex].y,tproV[aktindex].z))
                                throw hiba(fvnev,"bad probe valuse (%s) in line %u in %s",fajl.lines()[i][3].c_str(),i,fileName.c_str());
                        }
                        else if(pr==ProbeT){
                            tproT[aktindex].oldal=dal;
                            if(!fajl.lines()[i][3].to3uns(tproT[aktindex].x,tproT[aktindex].y,tproT[aktindex].z))
                                throw hiba(fvnev,"bad probe valuse (%s) in line %u in %s",fajl.lines()[i][3].c_str(),i,fileName.c_str());
                        }
                        else if(pr==ProbeC){
                            tproC[aktindex].oldal=dal;
                            if(!fajl.lines()[i][3].to3uns(tproC[aktindex].x,tproC[aktindex].y,tproC[aktindex].z))
                                throw hiba(fvnev,"bad probe valuse (%s) in line %u in %s",fajl.lines()[i][3].c_str(),i,fileName.c_str());
                            if(!fajl.lines()[i][4].to3uns(tproC[aktindex].x2,tproC[aktindex].y2,tproC[aktindex].z2))
                                throw hiba(fvnev,"bad probe valuse (%s) in line %u in %s",fajl.lines()[i][4].c_str(),i,fileName.c_str());
                        }
                        else if(pr==ProbeM){
                            tproM[aktindex].oldal=dal;
                            if(fajl.lines()[i][3].LowCase()=="x")tproM[aktindex].x=0;
                            else if(fajl.lines()[i][3].LowCase()=="y")tproM[aktindex].x=1;
                            else if(fajl.lines()[i][3].LowCase()=="z")tproM[aktindex].x=2;
                            else throw hiba(fvnev,"bad map probe direction (%s) in line %u in %s",fajl.lines()[i][3].c_str(),i,fileName.c_str());
                            if(!fajl.lines()[i][4].tounsigned(tproM[aktindex].y))
                                throw hiba(fvnev,"bad map probe valuse (%s) in line %u in %s",fajl.lines()[i][4].c_str(),i,fileName.c_str());
                        }
                    }
                    else if(pr==ProbeS){
                        if(aktpar=="x")     tproS[aktindex].oldal=WEST;
                        else if(aktpar=="y")tproS[aktindex].oldal=SOUTH;
                        else if(aktpar=="z")tproS[aktindex].oldal=BOTTOM;
                        else throw hiba(fvnev,"bad section probe type (%s) in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
                        if(!fajl.lines()[i][3].to2uns(tproS[aktindex].x,tproS[aktindex].y)) // mindig az x-y-ba teszi a koordin�t�kat, akkor is, ha nem az!
                            throw hiba(fvnev,"bad probe valuse (%s) in line %u in %s",fajl.lines()[i][3].c_str(),i,fileName.c_str());
                    }
                }
            }while(az==az_unknown || az==az_pro);
        }

        // map_to_txt

        while(az!=az_fimtxt && az!=az_commas && az != az_FIM_res && az != az_FIM_diff && az!=az_nofim && az!=az_analy 
            && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps 
            && az!=az_end && az != az_no_plus_step_data){
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_fimtxt && az != az_commas && az != az_FIM_res && az != az_FIM_diff && az!=az_nofim 
                && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps 
                && az!=az_analy && az!=az_end && az!=az_unknown && az != az_no_plus_step_data)
                throw hiba(fvnev,"map_to_txt or FIM_res or FIM_diff or use_commas or no_images or auto_transi_steps or analysis or end is missing in line %u in %s",i,fileName.c_str());
        }
        if(az==az_fimtxt)is_fim_txt=true;

        // FIM_res

        FIM_res_xy = FIM_res_z = 0;
        while (az != az_commas && az != az_FIM_res && az != az_FIM_diff && az != az_nofim && az != az_analy 
            && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps 
            && az != az_end && az != az_no_plus_step_data) {
            i++;
            az = azonosit(fajl.lines()[i][0].LowCase());
            if (az != az_commas && az != az_FIM_res && az != az_FIM_diff && az != az_nofim && az != az_analy && az != az_no_plus_step_data
                && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps && az != az_end && az != az_unknown)
                throw hiba(fvnev, "map_to_txt or FIM_res or FIM_diff or use_commas or no_images or auto_transi_steps or analysis or end is missing in line %u in %s", i, fileName.c_str());
        }
        if (az == az_FIM_res) {
            if (!fajl.lines()[i][1].tounsigned(FIM_res_xy))
                throw hiba(fvnev, "FIM_res_xy is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fileName.c_str());
            if (!fajl.lines()[i][2].tounsigned(FIM_res_z))
                throw hiba(fvnev, "FIM_res_z is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fileName.c_str());
        }

        // FIM_diff

        FIM_diff_x = FIM_diff_y = FIM_diff_z = 0;
        while (az != az_commas && az != az_FIM_diff && az != az_nofim && az != az_analy && az != az_end
            && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps
            && az != az_no_plus_step_data) {
            i++;
            az = azonosit(fajl.lines()[i][0].LowCase());
            if (az != az_commas && az != az_FIM_diff && az != az_nofim && az != az_analy && az != az_end && az != az_no_plus_step_data
                && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps && az != az_unknown)
                throw hiba(fvnev, "map_to_txt or FIM_diff or use_commas or no_images or auto_transi_steps or analysis or end is missing in line %u in %s", i, fileName.c_str());
        }
        if (az == az_FIM_diff) {
            if (!fajl.lines()[i][1].tounsigned(FIM_diff_x))
                throw hiba(fvnev, "FIM_diff_x is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fileName.c_str());
            if (!fajl.lines()[i][2].tounsigned(FIM_diff_y))
                throw hiba(fvnev, "FIM_diff_y is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fileName.c_str());
            if (!fajl.lines()[i][3].tounsigned(FIM_diff_z))
                throw hiba(fvnev, "FIM_diff_z is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fileName.c_str());
        }

        // use_commas

        while(az!=az_commas && az!=az_nofim && az!=az_analy && az!=az_end && az != az_no_plus_step_data
            && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps){
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_commas && az!=az_nofim && az!=az_analy && az!=az_end && az!=az_unknown && az != az_no_plus_step_data
                && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps)
                throw hiba(fvnev,"use_commas or no_images or auto_transi_steps or analysis or end is missing in line %u in %s",i,fileName.c_str());
        }
		if(az==az_commas)is_vesszo=true;

        // no_images

        while(az!=az_nofim && az!=az_analy && az!=az_end && az != az_auto_transi_steps_V 
            && az != az_auto_transi_steps_T && az != az_auto_transi_steps && az != az_no_plus_step_data){
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_nofim && az!=az_analy && az!=az_end && az!=az_unknown && az != az_no_plus_step_data
                && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps)
                throw hiba(fvnev,"no_images or auto_transi_steps or analysis or end is missing in line %u in %s",i,fileName.c_str());
        }
		if(az==az_nofim)is_nofim=true;

        // no_plus_step_data

        while (az != az_analy && az != az_end && az != az_auto_transi_steps_V
            && az != az_auto_transi_steps_T && az != az_auto_transi_steps && az != az_no_plus_step_data) {
            i++;
            az = azonosit(fajl.lines()[i][0].LowCase());
            if (az != az_analy && az != az_end && az != az_unknown && az != az_no_plus_step_data
                && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps)
                throw hiba(fvnev, "no_images or auto_transi_steps or analysis or end is missing in line %u in %s", i, fileName.c_str());
        }
        if (az == az_no_plus_step_data)auto_tra.is_no_plus_step_data = true;

        //  az_auto_transi_steps

        while (az != az_analy && az != az_end && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps) {
            i++;
            az = azonosit(fajl.lines()[i][0].LowCase());
            if (az != az_analy && az != az_end && az != az_unknown
                && az != az_auto_transi_steps_V && az != az_auto_transi_steps_T && az != az_auto_transi_steps)
                throw hiba(fvnev, "auto_transi_steps or analysis or end is missing in line %u in %s", i, fileName.c_str());
        }
        if (az == az_auto_transi_steps) {
            if (auto_tra.is_T || auto_tra.is_V)
                throw hiba(fvnev, "auto_transi_steps_T/auto_transi_steps_V/auto_transi_steps redefinition with auto_transi_steps in line %u in %s", i, fileName.c_str());
            if (!fajl.lines()[i][1].todouble(auto_tra.V_max_rel))
                throw hiba(fvnev, "auto_transi_steps par1 is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fileName.c_str());
            if (!fajl.lines()[i][2].todouble(auto_tra.V_min_dt))
                throw hiba(fvnev, "auto_transi_steps par2 is not a number (%s) in %s", fajl.lines()[i][2].c_str(), fileName.c_str());
            if (!fajl.lines()[i][3].tounsigned(auto_tra.V_max_plus_steps))
                throw hiba(fvnev, "auto_transi_steps par3 is not a number (%s) in %s", fajl.lines()[i][3].c_str(), fileName.c_str());
            auto_tra.T_max_rel = auto_tra.V_max_rel;
            auto_tra.T_min_dt = auto_tra.V_min_dt;
            auto_tra.T_max_plus_steps = auto_tra.V_max_plus_steps;
            auto_tra.is_T = auto_tra.is_V = true;
        }
        else  if (az == az_auto_transi_steps_V) {
            if (auto_tra.is_V)
                throw hiba(fvnev, "auto_transi_steps_V/auto_transi_steps redefinition with auto_transi_steps_V in line %u in %s", i, fileName.c_str());
            if (!fajl.lines()[i][1].todouble(auto_tra.V_max_rel))
                throw hiba(fvnev, "auto_transi_steps_V par1 is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fileName.c_str());
            if (!fajl.lines()[i][2].todouble(auto_tra.V_min_dt))
                throw hiba(fvnev, "auto_transi_steps_V par2 is not a number (%s) in %s", fajl.lines()[i][2].c_str(), fileName.c_str());
            if (!fajl.lines()[i][3].tounsigned(auto_tra.V_max_plus_steps))
                throw hiba(fvnev, "auto_transi_steps_V par3 is not a number (%s) in %s", fajl.lines()[i][3].c_str(), fileName.c_str());
            auto_tra.is_V = true;
        }
        else  if (az == az_auto_transi_steps_T) {
            if (auto_tra.is_T)
                throw hiba(fvnev, "auto_transi_steps_T/auto_transi_steps redefinition with auto_transi_steps_T in line %u in %s", i, fileName.c_str());
            if (!fajl.lines()[i][1].todouble(auto_tra.T_max_rel))
                throw hiba(fvnev, "auto_transi_steps_T par1 is not a number (%s) in %s", fajl.lines()[i][1].c_str(), fileName.c_str());
            if (!fajl.lines()[i][2].todouble(auto_tra.T_min_dt))
                throw hiba(fvnev, "auto_transi_steps_T par2 is not a number (%s) in %s", fajl.lines()[i][2].c_str(), fileName.c_str());
            if (!fajl.lines()[i][3].tounsigned(auto_tra.T_max_plus_steps))
                throw hiba(fvnev, "auto_transi_steps_T par3 is not a number (%s) in %s", fajl.lines()[i][3].c_str(), fileName.c_str());
            auto_tra.is_T = true;
        }

        
        // analysis

        while(az!=az_analy && az!=az_end){
            i++;
            az=azonosit(fajl.lines()[i][0].LowCase());
            if(az!=az_analy && az!=az_end && az!=az_unknown)
                throw hiba(fvnev,"analysis or end is missing (%s found) in line %u in %s", fajl.lines()[i][0].c_str(), i,fileName.c_str());
        }
        tanal.clear();
        while(az==az_analy){
            tanal.resize(tanal.size()+1);
            analysis & aktanal=tanal[tanal.size()-1];
            aktanal.nev=fajl.lines()[i][1];
            const PLString kisb=fajl.lines()[i][2].LowCase();
            if(kisb=="dc")aktanal.tipus=AnalDC;
            else if(kisb=="ndc")aktanal.tipus=AnalNDC;
            else if(kisb=="ac")aktanal.tipus=AnalAC;
            else if(kisb=="lintransient")aktanal.tipus=AnalLinTran;
            else if(kisb=="logtransient")aktanal.tipus=AnalLogTran;
            else if(kisb=="bode")aktanal.tipus=AnalBode;
            else if(kisb=="timeconst")aktanal.tipus=AnalIdo;
            else if(kisb=="controlled")aktanal.tipus=AnalCtrl;
            else throw hiba(fvnev,"bad analysis type (%s) in line %u in %s",fajl.lines()[i][2].c_str(),i,fileName.c_str());
            if(aktanal.tipus==AnalAC){
                if(!fajl.lines()[i][3].todouble(aktanal.from))
                    throw hiba(fvnev,"bad analysis format (%s) in line %u in %s",fajl.lines()[i][3].c_str(),i,fileName.c_str());
            }
            else if(aktanal.tipus==AnalLinTran){
                if(!fajl.lines()[i][3].to2double(aktanal.from,aktanal.to))
                    throw hiba(fvnev,"bad analysis format (%s) in line %u in %s",fajl.lines()[i][3].c_str(),i,fileName.c_str());
            }
            else if(aktanal.tipus==AnalLogTran||aktanal.tipus==AnalBode||aktanal.tipus==AnalIdo){
                if(!fajl.lines()[i][3].to2double1uns(aktanal.from,aktanal.to,aktanal.step))
                    throw hiba(fvnev,"bad analysis format (%s) in line %u in %s",fajl.lines()[i][3].c_str(),i,fileName.c_str());
            }
            else if(aktanal.tipus==AnalNDC){
                if(fajl.lines()[i].size()>3&&!fajl.lines()[i][3].to1uns2double(aktanal.ndc_maxiter,aktanal.relhiba,aktanal.ndc_I0))
                    throw hiba(fvnev,"bad analysis format (%s) in line %u in %s",fajl.lines()[i][3].c_str(),i,fileName.c_str());
                aktanal.ndc_I0 /= (pmodel->A_semi_full==nulla) ? 1.0 : pmodel->A_semi_full; // az �ram az eg�sz f�lvezet� fel�letre �rv�nyes
            }
            else if(aktanal.tipus==AnalCtrl){
                read_ctrl(path+fajl.lines()[i][3],aktanal,pmodel);
            }

            do{
                i++;
                az=azonosit(fajl.lines()[i][0].LowCase());
                if(az!=az_analy && az!=az_end && az!=az_unknown)
                    throw hiba(fvnev,"analysis or end is missing in line %u in %s",i,fileName.c_str());
            }while(az!=az_analy && az!=az_end);
        }
    }
    catch(const hiba & h){
        PLString s=h.what();
        if(s.find("tomb::operator[]")==npos)throw;
        throw hiba(fvnev,"missing parameters or unexpected end of file in line %u in %s",i,fileName.c_str());
    }
}

/*
//***********************************************************************
bool model::isCellExist(uns x,uns y,uns z)const{
//***********************************************************************
    if(x>=x_res || y>=y_res || y>=y_res)return false;
    if(tcolor[tbmp[z].getpixel(x,y)].perem)return false;
    return true;
}
*/
//***********************************************************************
dbl vezetes::get_sem_ertek(dbl T,dbl I,dbl A,dbl As,bool init)const{ // irany: 0=x, 1=y, 2=z, As= a f�lvezet� anyag fel�lete
// F�lvezet� vezet�s�t adja vissza
// T: h�m�rs�klet, I: �ram az adott ir�nyban, A: adott ir�nyban a cella fel�lete
// As: a a k�t sz�n teljes k�z�s fel�lete, mert a teljes f�lvezet� �tmenetre n�zz�k a 100 mA-t a nyit�fesz�lts�ghez
//***********************************************************************
    dbl ret;
//if(T<100)T=100;
//if(T>120)T=120;
    cd k_const=1.3806504e-23;
    cd q_const=1.602176487e-19;
    cd xUt=k_const*(T+absT)/q_const;
    cd nxUt=g[1]*xUt;
    cd szazmiliamper=semitip==nlt_diode ? g[2] : 0.1;
    cd Ix=szazmiliamper*A/As; // Az �ram fel�letar�nyos r�sze
    if(I==nulla){
        ret=g_max;
    }
    else{
        bool elojel=((g[0]>=nulla)^(I>=nulla))!=0;
        if(elojel&&init){elojel=false;I=-I;}
        switch(semitip){
            case nlt_lin: 
                throw hiba("vezetes::get_sem_ertek","linear semiconductor type");
            case nlt_exp:{ 
                if(fabs(g[0])>0.01){
                    if(elojel)ret=g_min; // z�r�ir�nyban v�gtelen ellen�ll�s
                    else ret = g[0]<nulla ? -I/(xUt*log((-I/Ix)*(exp(-g[0]/xUt)-egy)+egy)) : I/(xUt*log((I/Ix)*(exp(g[0]/xUt)-egy)+egy));
                }
                else{
                    if(elojel)ret=g_min; // z�r�ir�nyban v�gtelen ellen�ll�s
                    else ret = g[0]<nulla ? -I/(xUt*log((I/g[0]*As/A)+egy)) : I/(xUt*log((-I/g[0]*As/A)+egy));
                }
                break;}
            case nlt_diode:
                if(fabs(g[0])>0.01){
                    if(elojel)ret=g_min; // z�r�ir�nyban v�gtelen ellen�ll�s
                    else ret = g[0]<nulla ? egy/((nxUt/-I)*log((-I/Ix)*(exp(-g[0]/nxUt)-egy)+egy)) : egy/((nxUt/I)*log((I/Ix)*(exp(g[0]/nxUt)-egy)+egy));
                    dbl I0=szazmiliamper/(exp(fabs(g[0]/nxUt))-1.0);
//printf("%g ",I0);
                }
                else{
                    if(elojel)ret=g_min; // z�r�ir�nyban v�gtelen ellen�ll�s
                    else ret = g[0]<nulla ? egy/((nxUt/-I)*log((I/g[0]*As/A)+egy)) : egy/((nxUt/I)*log((-I/g[0]*As/A)+egy));
                }
                break;
            case nlt_quadratic:{ // Nem veszi figyelembe a fel�letet, m�g ki kell jav�tani.
                if( elojel || (g[0]==nulla && g[1]*I<nulla) ){ret=g_min; break;}// z�r�ir�nyban v�gtelen ellen�ll�s
                const bool nege=g[0]<nulla||(g[0]==nulla&&g[1]<0);
                cd a=nege?-g[0]:g[0];
                cd b=nege?-g[1]:g[1];
                cd c=nege?-g[2]:g[2];
                cd i=nege?-I:I;
                if(a!=nulla){
                    cd d=b*b-4.0*a*(c-i);
                    if(d<nulla)throw hiba("vezetes::get_sem_ertek","Semiconductor quadratic parameters result negative discriminant: A=%g, B=%g, C=%g, I=%g",a,b,c,i);
                    cd sqd=sqrt(d);
                    cd u1=(-b+sqd)/(2.0*a);
                    cd u2=(-b-sqd)/(2.0*a);
                    if(u1<=nulla&&u2<=nulla)throw hiba("vezetes::get_sem_ertek","Semiconductor quadratic parameters result negative admittance: A=%g, B=%g, C=%g, I=%g",a,b,c,i);
                    ret=i/(u1>u2?u1:u2);
                }
                else{
                    cd u=(i-c)/b;
                    ret=i/u;
                }
            }
            break;
            case nlt_erno:{
                double b=g[0]+g[1]*T+g[2]*T*T;
                double m=gg[0]+gg[1]*T+gg[2]*T*T;
if(I<0)I=fabs(I); // a z�r�ir�nyt most nem kezelj�k!!!
                elojel=((b>=nulla)^(I>=nulla))!=0;
                if(elojel)ret=g_min; // z�r�ir�nyban v�gtelen ellen�ll�s
                else{
                   I=fabs(I); b=fabs(b)*(specific?As:1.0);
                   double u=pow(I/b*As/A,egy/m);
                   ret=I/u;
                }
            }
            break;
            default: throw hiba("vezetes::get_sem_ertek","program error: unknown nonlinearity type");
        }
        cd szorzat=gg[0]*(T-25.0);
        cd kitevo=szorzat>7.0?7.0:szorzat<-7.0?-7.0:szorzat;
        switch(tipus){
            case nlt_lin: return ret;
            case nlt_exp: return ret*exp(kitevo);
            case nlt_diode: throw hiba("vezetes::get_sem_ertek","diode equation is not applicable");
            case nlt_quadratic: return ret*(gg[0]*T*T+gg[1]*T+gg[2]);
            case nlt_erno: return ret;
        }
        throw hiba("vezetes::get_sem_ertek","unexpected control path");
    }
    return ret;
}


//***********************************************************************
dbl vezetes::get_sem_rad_lum(dbl T,dbl I,uns azon,dbl As)const{
//***********************************************************************
    dbl ret;
    switch(semitip){
        case nlt_lin: 
        case nlt_exp:
        case nlt_diode:
        case nlt_quadratic: // Nem veszi figyelembe a fel�letet, m�g ki kell jav�tani.
           ret=nulla;//egy-r�l jav�tva nulla-ra 2011.08.11.
           break;
        case nlt_erno:{
            double b=(g[0]+g[1]*T+g[2]*T*T)*(specific?As:1.0);
            double m=gg[0]+gg[1]*T+gg[2]*T*T;//printf("\nm=%30g b=%30g\n",m,b);
//m=1,b=0;
            cd absi=fabs(I);
            ret = absi * ( m + b * elozo_full_I_semi.get(azon) ) ;
            //if(ret<0.0)printf("\nabsi=%g, m=%g, b=%g, elozo_full_I_semi.get(azon)=%g, ret=%g\n",absi,m,b,elozo_full_I_semi.get(azon),ret);
            if(ret<0.0)ret=0.0;
        }
        break;
        default: throw hiba("vezetes::get_sem_rad_lum","program error: unknown nonlinearity type");
    }
    cd szorzat=gg[0]*(T-25.0);
    cd kitevo=szorzat>7.0?7.0:szorzat<-7.0?-7.0:szorzat;
    switch(tipus){
        case nlt_lin: return ret;
        case nlt_exp: return ret*exp(kitevo);
        case nlt_diode: throw hiba("vezetes::get_sem_rad_lum","diode equation is not applicable");
        case nlt_quadratic: return ret*(gg[0]*T*T+gg[1]*T+gg[2]);
        case nlt_erno: return ret;
    }
    throw hiba("vezetes::get_sem_rad_lum","unexpected control path");
}

//***********************************************************************
bool model::get_gcsd(const gcsd_par & gp,gcsd & ret, const segedcella * s)const{
// termikusn�l csak g �s c!
// a modellel ellent�tben nem az �rt�kek �tlag�val, hanem az �tlagh�m�r-
// s�kleten vett �rt�kkel sz�mol, ami line�ris anyagparam�terek eset�n
// ugyanaz (ez volt a feltev�s).
//***********************************************************************
    if(gp.x>=x_res || gp.y>=y_res || gp.z>=z_res || gp.cc->tipus!=SzinNormal)return false;

    cd x_mul=2.0*gp.Ax/gp.x_meret;
    cd y_mul=2.0*gp.Ay/gp.y_meret;
    cd z_mul=2.0*gp.Az/gp.z_meret;
    cd terf=gp.Ax*gp.x_meret;
    cd init_I=gp.ndc_init_I;
    cd Tc=gp.Temp[EXTERNAL], Tw=gp.Temp[WEST], Te=gp.Temp[EAST], Ts=gp.Temp[SOUTH], Tn=gp.Temp[NORTH], Tb=gp.Temp[BOTTOM], Tt=gp.Temp[TOP];
    cd Taw=(Tw+Tc)*0.5, Tae=(Te+Tc)*0.5, Tas=(Ts+Tc)*0.5, Tan=(Tn+Tc)*0.5, Tab=(Tb+Tc)*0.5, Tat=(Tt+Tc)*0.5;
    cd T_atl = 0.25 * ( Tw + Te + Ts + Tn + Tb + Tt - 2.0 * Tc ); // disszert�ci� k�plete alapj�n
    ret.V=terf;
    ret.Vszin=gp.cc->terfogat;

    if(gp.ter==0){ // elektromos

        // g

        ret.g[WEST ] = gp.cc->pmat->get_elvez().get_x(Taw,gp.is_lin,s)*x_mul;
        ret.g[EAST ] = gp.cc->pmat->get_elvez().get_x(Tae,gp.is_lin,s)*x_mul;
        ret.g[SOUTH] = gp.cc->pmat->get_elvez().get_y(Tas,gp.is_lin,s)*y_mul;
        ret.g[NORTH] = gp.cc->pmat->get_elvez().get_y(Tan,gp.is_lin,s)*y_mul;
        ret.g[BOTTOM]= gp.cc->pmat->get_elvez().get_z(Tab,gp.is_lin,s)*z_mul;
        ret.g[TOP  ] = gp.cc->pmat->get_elvez().get_z(Tat,gp.is_lin,s)*z_mul;

        // c

        ret.c=gp.cc->pmat->get_Ce().get_x(T_atl,gp.is_lin,s)*terf;

        // s

        ret.s[EXTERNAL] = gp.cc->pmat->get_S().get_x(Tc,gp.is_lin,s);
        ret.s[WEST ] = gp.cc->pmat->get_S().get_x(Taw,gp.is_lin,s);
        ret.s[EAST ] = gp.cc->pmat->get_S().get_x(Tae,gp.is_lin,s);
        ret.s[SOUTH] = gp.cc->pmat->get_S().get_y(Tas,gp.is_lin,s);
        ret.s[NORTH] = gp.cc->pmat->get_S().get_y(Tan,gp.is_lin,s);
        ret.s[BOTTOM]= gp.cc->pmat->get_S().get_z(Tab,gp.is_lin,s);
        ret.s[TOP  ] = gp.cc->pmat->get_S().get_z(Tat,gp.is_lin,s);

        ret.s_valodi[EXTERNAL] = gp.cc->pmat->get_S().get_x(Tc,gp.is_lin,s);
        ret.s_valodi[WEST ] = gp.cc->pmat->get_S().get_x(Tw,gp.is_lin,s);
        ret.s_valodi[EAST ] = gp.cc->pmat->get_S().get_x(Te,gp.is_lin,s);
        ret.s_valodi[SOUTH] = gp.cc->pmat->get_S().get_y(Ts,gp.is_lin,s);
        ret.s_valodi[NORTH] = gp.cc->pmat->get_S().get_y(Tn,gp.is_lin,s);
        ret.s_valodi[BOTTOM]= gp.cc->pmat->get_S().get_z(Tb,gp.is_lin,s);
        ret.s_valodi[TOP  ] = gp.cc->pmat->get_S().get_z(Tt,gp.is_lin,s);

        // d

        ret.d[EXTERNAL] = gp.cc->pmat->get_D().get_x(Tc,gp.is_lin,s);
        ret.d[WEST ] = gp.cc->pmat->get_D().get_x(Taw,gp.is_lin,s);
        ret.d[EAST ] = gp.cc->pmat->get_D().get_x(Tae,gp.is_lin,s);
        ret.d[SOUTH] = gp.cc->pmat->get_D().get_y(Tas,gp.is_lin,s);
        ret.d[NORTH] = gp.cc->pmat->get_D().get_y(Tan,gp.is_lin,s);
        ret.d[BOTTOM]= gp.cc->pmat->get_D().get_z(Tab,gp.is_lin,s);
        ret.d[TOP  ] = gp.cc->pmat->get_D().get_z(Tat,gp.is_lin,s);

        // f�lvezet� g, d

        for(uns i=0;i<BASIC_SIDES;i++)ret.fg[i]=g_max;
        for(uns i=0;i<BASIC_SIDES;i++)ret.fd[i]=ret.dfull[i]=ret.d[i];
        for(uns i=0;i<BASIC_SIDES;i++)ret.f_area[i]=ret.fI[i]=ret.f_rad[i]=ret.f_lum[i]=nulla;
        for(uns i=0;i<BASIC_SIDES;i++)ret.fr[i]=egy;
        if((!gp.is_no_semi || gp.is_semi_init) && gp.cc->tsemi.size()>0){
            uns kulso=colmax+1;
            const bool b=gp.is_semi_init; // cd Ix=0.1*A/As;
            cd A_x=y_pit[gp.y].get(nulla)*z_pit[gp.z].get(x_hossz[gp.x*2]);
            cd A_y=x_pit[gp.x].get(nulla)*z_pit[gp.z].get(x_hossz[gp.x*2]);
            cd A_z=x_pit[gp.x].get(nulla)*y_pit[gp.y].get(nulla);
            for(uns i=0;i<gp.cc->tsemi.size();i++)if(gp.cc->tsemi[i].col2==colmax)kulso=i;

            if(kulso!=colmax+1){ // ha van k�ls� peremhez f�lvezet�se, akkor azt k�l�n n�zz�k
                const semiconductor & sem=gp.cc->tsemi[kulso];
                cd I_mul=init_I;//sem.As;
                if(gp.x==0){
                    dbl I_akt=b?I_mul*A_x:gp.I->t[WEST];
                    ret.fg[WEST]=sem.par.get_sem_ertek(Tw,I_akt,A_x,sem.As,b); // WEST
                    ret.fd[WEST]=sem.D.get_x(Tw,gp.is_lin,s);
                    ret.fr[WEST]=sem.R.get_x(Tw,gp.is_lin,s);
                    ret.fI[WEST]=I_akt;
                    full_I_semi.inc(fabs(I_akt),sem.azon);
                    ret.f_rad[WEST]=sem.rad.get_sem_rad_lum(Tw,I_akt,sem.azon,sem.As);
                    ret.f_lum[WEST]=sem.lum.get_sem_rad_lum(Tw,I_akt,sem.azon,sem.As);
                    ret.f_area[WEST]=A_x;
                }
                if(gp.x==x_res-1){
                    dbl I_akt=b?I_mul*A_x:gp.I->t[EAST];
                    ret.fg[EAST]=sem.par.get_sem_ertek(Te,I_akt,A_x,sem.As,b); // EAST
                    ret.fd[EAST]=sem.D.get_x(Te,gp.is_lin,s);
                    ret.fr[EAST]=sem.R.get_x(Te,gp.is_lin,s);
                    ret.fI[EAST]=I_akt;
                    full_I_semi.inc(fabs(I_akt),sem.azon);
                    ret.f_rad[EAST]=sem.rad.get_sem_rad_lum(Te,I_akt,sem.azon,sem.As);
                    ret.f_lum[EAST]=sem.lum.get_sem_rad_lum(Te,I_akt,sem.azon,sem.As);
                    ret.f_area[EAST]=A_x;
                }
                if(gp.y==0){
                    dbl I_akt=b?I_mul*A_y:gp.I->t[SOUTH];
                    ret.fg[SOUTH]=sem.par.get_sem_ertek(Ts,I_akt,A_y,sem.As,b); // SOUTH
                    ret.fd[SOUTH]=sem.D.get_y(Ts,gp.is_lin,s);
                    ret.fr[SOUTH]=sem.R.get_y(Ts,gp.is_lin,s);
                    ret.fI[SOUTH]=I_akt;
                    full_I_semi.inc(fabs(I_akt),sem.azon);
                    ret.f_rad[SOUTH]=sem.rad.get_sem_rad_lum(Ts,I_akt,sem.azon,sem.As);
                    ret.f_lum[SOUTH]=sem.lum.get_sem_rad_lum(Ts,I_akt,sem.azon,sem.As);
                    ret.f_area[SOUTH]=A_y;
                }
                if(gp.y==y_res-1){
                    dbl I_akt=b?I_mul*A_y:gp.I->t[NORTH];
                    ret.fg[NORTH]=sem.par.get_sem_ertek(Tn,I_akt,A_y,sem.As,b); // NORTH
                    ret.fd[NORTH]=sem.D.get_y(Tn,gp.is_lin,s);
                    ret.fr[NORTH]=sem.R.get_y(Tn,gp.is_lin,s);
                    ret.fI[NORTH]=I_akt;
                    full_I_semi.inc(fabs(I_akt),sem.azon);
                    ret.f_rad[NORTH]=sem.rad.get_sem_rad_lum(Tn,I_akt,sem.azon,sem.As);
                    ret.f_lum[NORTH]=sem.lum.get_sem_rad_lum(Tn,I_akt,sem.azon,sem.As);
                    ret.f_area[NORTH]=A_y;
                }
                if(gp.z==0){
                    dbl I_akt=b?I_mul*A_z:gp.I->t[BOTTOM];
                    ret.fg[BOTTOM]=sem.par.get_sem_ertek(Tb,I_akt,A_z,sem.As,b); // BOTTOM
                    ret.fd[BOTTOM]=sem.D.get_z(Tb,gp.is_lin,s);
                    ret.fr[BOTTOM]=sem.R.get_z(Tb,gp.is_lin,s);
                    ret.fI[BOTTOM]=I_akt;
                    full_I_semi.inc(fabs(I_akt),sem.azon);
                    ret.f_rad[BOTTOM]=sem.rad.get_sem_rad_lum(Tb,I_akt,sem.azon,sem.As);
                    ret.f_lum[BOTTOM]=sem.lum.get_sem_rad_lum(Tb,I_akt,sem.azon,sem.As);
                    ret.f_area[BOTTOM]=A_z;
                }
                if(gp.z==z_res-1){
                    dbl I_akt=b?I_mul*A_z:gp.I->t[TOP];
                    ret.fg[TOP]=sem.par.get_sem_ertek(Tt,I_akt,A_z,sem.As,b); // TOP
                    ret.fd[TOP]=sem.D.get_z(Tt,gp.is_lin,s);
                    ret.fr[TOP]=sem.R.get_z(Tt,gp.is_lin,s);
                    ret.fI[TOP]=I_akt;
                    full_I_semi.inc(fabs(I_akt),sem.azon);
                    ret.f_rad[TOP]=sem.rad.get_sem_rad_lum(Tt,I_akt,sem.azon,sem.As);
                    ret.f_lum[TOP]=sem.lum.get_sem_rad_lum(Tt,I_akt,sem.azon,sem.As);
                    ret.f_area[TOP]=A_z;
                }
            }
 
            // WEST

            if(gp.x!=0){
                uns col2=tbmp[gp.z].getpixel_also(gp.x-1,gp.y);
                for(uns i=0;i<gp.cc->tsemi.size();i++){
                    const semiconductor & sem=gp.cc->tsemi[i];
                    if(sem.col2==col2){
                        cd I_mul=init_I;//sem.As;
                        dbl I_akt=b?I_mul*A_x:gp.I->t[WEST];
                        ret.fg[WEST]=sem.par.get_sem_ertek(Tw,I_akt,A_x,sem.As,b); // WEST
                        ret.fd[WEST]=sem.D.get_x(Tw,gp.is_lin,s);
                        ret.fr[WEST]=sem.R.get_x(Tw,gp.is_lin,s);
                        ret.fI[WEST]=I_akt;
                        full_I_semi.inc(fabs(I_akt),sem.azon);
                        ret.f_rad[WEST]=sem.rad.get_sem_rad_lum(Tw,I_akt,sem.azon,sem.As);
                        ret.f_lum[WEST]=sem.lum.get_sem_rad_lum(Tw,I_akt,sem.azon,sem.As);
                        ret.f_area[WEST]=A_x;
                        break;
                    }
                }
            }

            // EAST

            if(gp.x!=x_res-1){
                uns col2=tbmp[gp.z].getpixel_also(gp.x+1,gp.y);
                for(uns i=0;i<gp.cc->tsemi.size();i++){
                    const semiconductor & sem=gp.cc->tsemi[i];
                    if(sem.col2==col2){
                        cd I_mul=init_I;//sem.As;
                        dbl I_akt=b?I_mul*A_x:gp.I->t[EAST];
                        ret.fg[EAST]=sem.par.get_sem_ertek(Te,I_akt,A_x,sem.As,b); // EAST
                        ret.fd[EAST]=sem.D.get_x(Te,gp.is_lin,s);
                        ret.fr[EAST]=sem.R.get_x(Te,gp.is_lin,s);
                        ret.fI[EAST]=I_akt;
                        full_I_semi.inc(fabs(I_akt),sem.azon);
                        ret.f_rad[EAST]=sem.rad.get_sem_rad_lum(Te,I_akt,sem.azon,sem.As);
                        ret.f_lum[EAST]=sem.lum.get_sem_rad_lum(Te,I_akt,sem.azon,sem.As);
                        ret.f_area[EAST]=A_x;
                        break;
                    }
                }
            }

            // SOUTH

            if(gp.y!=0){
                uns col2=tbmp[gp.z].getpixel_also(gp.x,gp.y-1);
                for(uns i=0;i<gp.cc->tsemi.size();i++){
                    const semiconductor & sem=gp.cc->tsemi[i];
                    if(sem.col2==col2){
                        cd I_mul=init_I;//sem.As;
                        dbl I_akt=b?I_mul*A_y:gp.I->t[SOUTH];
                        ret.fg[SOUTH]=sem.par.get_sem_ertek(Ts,I_akt,A_y,sem.As,b); // SOUTH
                        ret.fd[SOUTH]=sem.D.get_y(Ts,gp.is_lin,s);
                        ret.fr[SOUTH]=sem.R.get_y(Ts,gp.is_lin,s);
                        ret.fI[SOUTH]=I_akt;
                        full_I_semi.inc(fabs(I_akt),sem.azon);
                        ret.f_rad[SOUTH]=sem.rad.get_sem_rad_lum(Ts,I_akt,sem.azon,sem.As);
                        ret.f_lum[SOUTH]=sem.lum.get_sem_rad_lum(Ts,I_akt,sem.azon,sem.As);
                        ret.f_area[SOUTH]=A_y;
                        break;
                    }
                }
            }

            // NORTH

            if(gp.y!=y_res-1){
                uns col2=tbmp[gp.z].getpixel_also(gp.x,gp.y+1);
                for(uns i=0;i<gp.cc->tsemi.size();i++){
                    const semiconductor & sem=gp.cc->tsemi[i];
                    if(sem.col2==col2){
                        cd I_mul=init_I;//sem.As;
                        dbl I_akt=b?I_mul*A_y:gp.I->t[NORTH];
                        ret.fg[NORTH]=sem.par.get_sem_ertek(Tn,I_akt,A_y,sem.As,b); // NORTH
                        ret.fd[NORTH]=sem.D.get_y(Tn,gp.is_lin,s);
                        ret.fr[NORTH]=sem.R.get_y(Tn,gp.is_lin,s);
                        ret.fI[NORTH]=I_akt;
                        full_I_semi.inc(fabs(I_akt),sem.azon);
                        ret.f_rad[NORTH]=sem.rad.get_sem_rad_lum(Tn,I_akt,sem.azon,sem.As);
                        ret.f_lum[NORTH]=sem.lum.get_sem_rad_lum(Tn,I_akt,sem.azon,sem.As);
                        ret.f_area[NORTH]=A_y;
                        break;
                    }
                }
            }

            // BOTTOM

            if(gp.z!=0){
                uns col2=tbmp[gp.z-1].getpixel_also(gp.x,gp.y);
                for (uns i = 0; i<gp.cc->tsemi.size(); i++){
                    const semiconductor & sem=gp.cc->tsemi[i];
                    if(sem.col2==col2){
                        cd I_mul=init_I;//sem.As;
                        dbl I_akt=b?I_mul*A_z:gp.I->t[BOTTOM];
                        ret.fg[BOTTOM] = sem.par.get_sem_ertek(Tb, I_akt, A_z, sem.As, b); // BOTTOM
                        ret.fd[BOTTOM] = sem.D.get_z(Tb, gp.is_lin, s);
                        ret.fr[BOTTOM] = sem.R.get_z(Tb, gp.is_lin, s);
                        ret.fI[BOTTOM] = I_akt;
                        full_I_semi.inc(fabs(I_akt), sem.azon);
                        ret.f_rad[BOTTOM]=sem.rad.get_sem_rad_lum(Tb,I_akt,sem.azon,sem.As);
                        ret.f_lum[BOTTOM]=sem.lum.get_sem_rad_lum(Tb,I_akt,sem.azon,sem.As);
                        ret.f_area[BOTTOM]=A_z;
                        break;
                    }
                }
            }

            // TOP

            if(gp.z!=z_res-1){
                uns col2=tbmp[gp.z+1].getpixel_also(gp.x,gp.y);
                for(uns i=0;i<gp.cc->tsemi.size();i++){
                    const semiconductor & sem=gp.cc->tsemi[i];
                    if(sem.col2==col2){
                        cd I_mul=init_I;//sem.As;
                        dbl I_akt=b?I_mul*A_z:gp.I->t[TOP];
                        ret.fg[TOP]=sem.par.get_sem_ertek(Tt,I_akt,A_z,sem.As,b); // TOP
                        ret.fd[TOP]=sem.D.get_z(Tt,gp.is_lin,s);
                        ret.fr[TOP]=sem.R.get_z(Tt,gp.is_lin,s);
                        ret.fI[TOP]=I_akt;
                        full_I_semi.inc(fabs(I_akt),sem.azon);
                        ret.f_rad[TOP]=sem.rad.get_sem_rad_lum(Tt,I_akt,sem.azon,sem.As);
                        ret.f_lum[TOP]=sem.lum.get_sem_rad_lum(Tt,I_akt,sem.azon,sem.As);
                        ret.f_area[TOP]=A_z;//printf("%u, ret.f_rad[TOP]=%g, sem.rad.g=%g,%g,%g, sem.rad.gg=%g,%g,%g\n",sem.rad.tipus,ret.f_rad[TOP],sem.rad.g[0],sem.rad.g[1],sem.rad.g[2],sem.rad.gg[0],sem.rad.gg[1],sem.rad.gg[2]);
                        break;
                    }
                }
            }
            for (uns i = 0; i<BASIC_SIDES; i++)ret.gfull[i] = replusz(ret.fg[i], ret.g[i]);//ret.fg[i]==g_max?ret.g[i]:ret.fg[i];//
            for(uns i=1;i<BASIC_SIDES;i++)ret.dfull[i]=(ret.fg[i]==g_max) ? ret.d[i] : ((ret.d[i]*ret.fg[i]+ret.fd[i]*ret.g[i])/(ret.g[i]+ret.fg[i]));
        }
        else for(uns i=0;i<BASIC_SIDES;i++)ret.gfull[i]=ret.g[i];
    }
    else if(gp.ter==1){ // termikus

        // g

        ret.g[WEST ] = gp.cc->pmat->get_thvez().get_x(Taw,gp.is_lin,s)*x_mul;
        ret.g[EAST ] = gp.is_lin ? ret.g[WEST ] : gp.cc->pmat->get_thvez().get_x(Tae,gp.is_lin,s)*x_mul;
        ret.g[SOUTH] = gp.cc->pmat->get_thvez().get_y(Tas,gp.is_lin,s)*y_mul;
        ret.g[NORTH] = gp.is_lin ? ret.g[SOUTH] : gp.cc->pmat->get_thvez().get_y(Tan,gp.is_lin,s)*y_mul;
        ret.g[BOTTOM]= gp.cc->pmat->get_thvez().get_z(Tab,gp.is_lin,s)*z_mul;
        ret.g[TOP  ] = gp.is_lin ? ret.g[BOTTOM] : gp.cc->pmat->get_thvez().get_z(Tat,gp.is_lin,s)*z_mul;
        for(uns i=0;i<BASIC_SIDES;i++)ret.gfull[i]=ret.g[i];

        // c

        //ret.c=gp.cc->pmat->get_Cth(Tc).get_x(Tc,gp.is_lin)*terf;
        ret.c=gp.cc->pmat->get_Cth().get_x(T_atl,gp.is_lin,s)*terf;
    }
    else throw hiba("model::get_gcsd","unknown ter value (%u)",gp.ter);
    ret.sum_gfull = ret.gfull[0];
    for (uns i = 1;i<BASIC_SIDES;i++)ret.sum_gfull += ret.gfull[i];
    return true;
}


//***********************************************************************
const csomag & simulation::get_inner_perem(uns x, uns y, uns z)const{
//***********************************************************************
    uns szin = pmodel->tbmp[z].getpixel_also(x, y);
    const color &aktszin = pmodel->tcolor[szin];
    if (!aktszin.is)
        throw hiba("simulation::get_inner_perem", "color not exists (%u)", szin);
    if (aktszin.tipus == SzinBoundary)
        return tinner[innerindex[szin]];
    else if (aktszin.tipus == SzinUchannel){
        uns tucha_index = pmodel->tbmp[z].getpixel_felso(x, y);
        if (tucha_perem[szin].size()<=tucha_index)
            throw hiba("simulation::get_inner_perem", "program error: tucha_perem[%u].size()<=tucha_index (%u<=%u)", szin, tucha_perem[szin].size(), tucha_index);
        return tucha_perem[szin][tucha_index];
    }
    else 
        throw hiba("simulation::get_inner_perem", "program error: impossible boundary type", szin);
}


//***********************************************************************
const boundary * simulation::get_perem(uns x, uns y, uns z, Oldal oldal, bool is_el)const{
//***********************************************************************
        const boundary * b = NULL;
    if(is_el){
        switch(oldal){
            case WEST:   b = x == 0             ? &normalperem.el[WEST]   : &get_inner_perem(x - 1, y, z).el[EAST];   break;
            case EAST:   b = x==pmodel->x_res-1 ? &normalperem.el[EAST]   : &get_inner_perem(x + 1, y, z).el[WEST];   break;
            case SOUTH:  b = y==0               ? &normalperem.el[SOUTH]  : &get_inner_perem(x, y - 1, z).el[NORTH];  break;
            case NORTH:  b = y==pmodel->y_res-1 ? &normalperem.el[NORTH]  : &get_inner_perem(x, y + 1, z).el[SOUTH];  break;
            case BOTTOM: b = z==0               ? &normalperem.el[BOTTOM] : &get_inner_perem(x, y, z - 1).el[TOP];    break;
            case TOP:    b = z==pmodel->z_res-1 ? &normalperem.el[TOP]    : &get_inner_perem(x, y, z + 1).el[BOTTOM]; break;
            default: throw hiba("simulation::get_perem_u","Unknown side.");
        }
    }
    else{
        switch(oldal){
            case WEST:   b = x==0               ? &normalperem.th[WEST]   : &get_inner_perem(x - 1, y, z).th[EAST];   break;
            case EAST:   b = x==pmodel->x_res-1 ? &normalperem.th[EAST]   : &get_inner_perem(x + 1, y, z).th[WEST];   break;
            case SOUTH:  b = y==0               ? &normalperem.th[SOUTH]  : &get_inner_perem(x, y - 1, z).th[NORTH];  break;
            case NORTH:  b = y==pmodel->y_res-1 ? &normalperem.th[NORTH]  : &get_inner_perem(x, y + 1, z).th[SOUTH];  break;
            case BOTTOM: b = z==0               ? &normalperem.th[BOTTOM] : &get_inner_perem(x, y, z - 1).th[TOP];    break;
            case TOP:    b = z==pmodel->z_res-1 ? &normalperem.th[TOP]    : &get_inner_perem(x, y, z + 1).th[BOTTOM]; break;
            default: throw hiba("simulation::get_perem_u","Unknown side.");
        }
    }
    return b;
}


//***********************************************************************
void simulation::run_dc(const PLString & FileName,uns anal_index){
//***********************************************************************
    masina mas;
    mas.del_all();
    mas.par_clear(0);
    mas.par_set_ac(false);
    mas.par_set_quad(false);
    mas.par_set_pot_map(0);
    mas.par_set_temp_map(1);
    mas.par_set_curr_map(2);
//    mas.par_set_start_UT(true); // �ll�tson kezd�h�m�rs�kletet
    mas.par_set_I_for_semi(false);
    mas.par_set_start_from_map(false);
    mas.par_set_el_th_Seebeck(false);
    mas.par_set_el_th_diss(false); // ez most nem elektro-termikus
    mas.par_set_th_el_Seebeck(false);
    mas.par_set_transi(false);
    mas.par_set_timeconst(false);
    mas.par_set_dt(nulla);
    mas.par_set_f(nulla);
    mas.par_set_t(nulla);
    mas.par_set_anal_step(1);
    mas.par_set_s(nulla);
    mas.par_set_sim(this);
    mas.par_set_tipus(FieldTherm);
    mas.par_set_anal_index(anal_index);
    mas.par_set_fim_txt(is_fim_txt);
    is_simple_anal_progress = false;

    dbl *powmap = nullptr;
    if ((mezo==FieldTherm || mezo==FieldElTherm) && pmap.is_exists()) {
        powmap = new dbl[pmodel->x_res*pmodel->y_res*pmodel->z_res];
    }

    if(mezo!=FieldElTherm){
        mas.par_set_quad(valostipus!=0);
        mas.par_set_tipus(mezo);
      resetAnalLevel(2);
      setAnalStepDb(2,1);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,0," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,mezo==FieldEl?"set V0":"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(nulla, powmap);
        mas.fill_step0_curr(4, powmap);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2);
//      mas.forwsubs();
        mas.backsubs(0,aramot);
        if(mezo==FieldEl)mas.get_u_i_matrix(1);
        else mas.get_T_matrix(1);
        mas.write_fim(FileName,2,true,0);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
      setAnalKeszStep(2,1);
    }
    else{
        mas.par_set_quad((valostipus&1)!=0);
        mas.par_set_tipus(FieldEl);
      resetAnalLevel(2);
      setAnalStepDb(2,2);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,0," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        mas.fill_step0_curr(4);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
//      mas.forwsubs();
        mas.backsubs(0,aramot);
        mas.get_u_i_matrix(1);
        mas.write_fim(FileName,2,true,0);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;

        mas.del_azon();
        mas.par_set_quad((valostipus&2)!=0);
        mas.par_set_el_th_diss(true);
        mas.par_set_tipus(FieldTherm);

      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,1," ");
      uchannel_update();
      mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(nulla, powmap);
        mas.fill_step0_curr(4, powmap);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
//      mas.forwsubs();
        mas.backsubs(0,aramot);
        mas.get_T_matrix(1);
        mas.write_fim(FileName,2,true,0);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
      setAnalKeszStep(2,2);
    }
    delete[] powmap;
}

//***********************************************************************
u32 global_akt_iter=0;
//***********************************************************************

//***********************************************************************
void simulation::run_ndc(const PLString & FileName,uns anal_index){
//***********************************************************************
    bool quad_el=((valostipus&1)!=0)&&is_always_quad,quad_th=false,quad_x=is_always_quad;   // uj
//quad_el = ((valostipus&1)!=0);
//quad_th = ((valostipus&2)!=0);
    masina mas;
    mas.del_all();
    mas.par_clear(0);
    mas.par_set_ac(false);
    mas.par_set_quad(false);
    mas.par_set_pot_map(0);
    mas.par_set_temp_map(1);
    mas.par_set_curr_map(2);
    mas.par_set_old_pot_map(3,5);
    mas.par_set_old_temp_map(4,6);
    mas.par_set_old_curr_map(7,8);
//    mas.par_set_start_UT(true); // �ll�tson kezd�h�m�rs�kletet
    mas.par_set_I_for_semi(false);
    mas.par_set_start_from_map(false);
//    mas.par_set_el_th_Seebeck(false);
    mas.par_set_el_th_Seebeck(!this->is_no_seebeck);
    mas.par_set_el_th_diss(false); // ez most nem elektro-termikus
//    mas.par_set_th_el_Seebeck(false); // 2011.03.27. Seebeck-effektus
    mas.par_set_th_el_Seebeck(!this->is_no_seebeck);
    mas.par_set_transi(false);
    mas.par_set_timeconst(false);
    mas.par_set_dt(nulla);
    mas.par_set_f(nulla);
    mas.par_set_t(nulla);
    mas.par_set_anal_step(1);
    mas.par_set_s(nulla);
    mas.par_set_sim(this);
    mas.par_set_tipus(FieldTherm);
    mas.par_set_anal_index(anal_index);
    mas.par_set_fim_txt(is_fim_txt);
    is_simple_anal_progress = true;

    dbl elozo_u_hiba=0.0, elozo_T_hiba=0.0,akt_u_hiba=0.0, akt_T_hiba=0.0;

    dbl *powmap = nullptr;
    if ((mezo == FieldTherm || mezo == FieldElTherm) && pmap.is_exists()) {
        powmap = new dbl[pmodel->x_res*pmodel->y_res*pmodel->z_res];
    }

    if(mezo!=FieldElTherm){
        mas.par_set_quad(quad_x); // uj mas.par_set_quad(valostipus!=0);
        mas.par_set_tipus(mezo);
        mas.par_set_semi_init(true);
        mas.par_set_el_th_Seebeck(false);
      resetAnalLevel(2);
      setAnalStepDb(2,tanal[anal_index].ndc_maxiter);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,0," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,mezo==FieldEl?"set V0":"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(nulla, powmap);
        mas.fill_step0_curr(4, powmap);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2);
        mas.backsubs(0,aramot);
        if(mezo==FieldEl)mas.get_u_i_matrix(1);
        else mas.get_T_matrix(1);
//        mas.write_fim(FileName+"1.",2,0);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
      if(ConsoleText)printf("\r%78f%%",100.0);

        uns aktiter=1;
        mas.par_set_semi_init(false);
        if(mezo==FieldEl)mas.par_set_I_for_semi(true); // f�lvezet� nemlinearit�s�hoz
reiterate_x:
        do{
                mas.del_azon(); // csak a h�l�t t�rli
            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,aktiter,"OK");
            uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                cd vegyes_arany=(elozo_T_hiba==akt_T_hiba) ? 0.0 : ((akt_T_hiba)/(elozo_T_hiba-akt_T_hiba));
                dbl vegyes_szorzo=(elozo_T_hiba==akt_T_hiba) ? 0.5 : (vegyes_arany<0.0 ? 0.0 : (vegyes_arany>20.0 ? 20.0 : vegyes_arany));
                if(akt_T_hiba>0.005)vegyes_szorzo=0.0;
                mas.konvergens_U_T_szamito(vegyes_szorzo);
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,mezo==FieldEl?"set V0":"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                if(mezo==FieldEl)mas.par_set_start_from_map(false); // a fill h�m. t�rk�pk�nt kezeli, de El. szim.-n�l nincs h�m
                mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(nulla, powmap);
                mas.fill_step0_curr(4, powmap);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul);
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2);
                mas.backsubs(0,aramot);
                mas.swap_u(); //mas.swap_UT(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket // A r�gi verzi� lecser�lve 2011.04.07., az older map be�p�t�se miatt
                mas.swap_T(); //mas.swap_UT(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket // A r�gi verzi� lecser�lve 2011.04.07., az older map be�p�t�se miatt
                if(mezo==FieldEl)mas.get_u_i_matrix(1);
                else mas.get_T_matrix(1);
//            mas.write_fim(FileName+(aktiter+1)+'.',2);
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;
            ++aktiter;
            elozo_T_hiba=akt_T_hiba;
            akt_T_hiba=mas.get_max_error((mezo==FieldEl)?mas.get_u_azon():mas.get_T_azon(),(mezo==FieldEl)?mas.get_old_u_azon():mas.get_old_T_azon());
            if(ConsoleText){
                printf("\r%4d. iter   el=%14.7f%%   th=%14.7f%%",aktiter,(mezo==FieldEl)?akt_T_hiba*100.0:0.0,(mezo==FieldEl)?0.0:akt_T_hiba*100.0);
                if(aktiter%3==1)printf("   accelerator = %-12g",vegyes_szorzo);
                else printf("                             ");
                printf("\n");
            }
//        }while(aktiter<10||(aktiter<tanal[anal_index].ndc_maxiter&&!mas.check_error((mezo==FieldEl)?mas.get_u_azon():mas.get_T_azon(),(mezo==FieldEl)?mas.get_old_u_azon():mas.get_old_T_azon(),tanal[anal_index].relhiba)));
        }while(aktiter < ndc_miniter || (aktiter<tanal[anal_index].ndc_maxiter
            && ((mezo==FieldEl) ? !mas.check_error(mas.get_u_azon(),mas.get_old_u_azon(),tanal[anal_index].relhiba)
                               :  !mas.check_error(mas.get_T_azon(),mas.get_old_T_azon(),tanal[anal_index].relhiba))));
        if(quad_x != (valostipus!=0)){ // uj
            quad_x = (valostipus!=0);
            mas.par_set_quad(quad_x);
            goto reiterate_x;
        }
        mas.write_fim(FileName,2,true,0);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      setAnalStepDb(2,aktiter);
      setAnalKeszStep(2,aktiter);
        if(ConsoleText)printf("\n\nmax error=%g%%\n",mas.get_max_error((mezo==FieldEl)?mas.get_u_azon():mas.get_T_azon(),(mezo==FieldEl)?mas.get_old_u_azon():mas.get_old_T_azon())*100.0);
    }
    else{ // sima dc-t m�soltam be
        mas.par_set_quad(quad_el); // mas.par_set_quad((valostipus&1)!=0); uj
        mas.par_set_tipus(FieldEl);
        mas.par_set_semi_init(true);
      resetAnalLevel(2);
      setAnalStepDb(2,tanal[anal_index].ndc_maxiter*2);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,0," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        mas.fill_step0_curr(4);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
        mas.backsubs(0,aramot);
        mas.get_u_i_matrix(1);
//        mas.write_fim(FileName+"1.",2); //!!
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
      if(ConsoleText)printf("\r%74f%%(el)",100.0);

        mas.del_azon();
        mas.par_set_quad(quad_th); // mas.par_set_quad((valostipus&2)!=0); uj
        mas.par_set_el_th_diss(true);
        mas.par_set_tipus(FieldTherm);

      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,1," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(nulla, powmap);
        mas.fill_step0_curr(4, powmap);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
        mas.backsubs(0,aramot);
        mas.get_T_matrix(1);
//        mas.write_fim(FileName+"1.",2); //!!
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
//      if(ConsoleText)printf("\r%74f%%(th)",100.0);
      if(ConsoleText){
          printf("\r%4d. iter   el=%14.7f%%   th=%14.7f%%",1,100.0,100.0);
          printf("                             ");
          printf("\n");
      }
//#define BEALLAS
#ifdef BEALLAS
FILE * fp=fopen("beallas.txt","wt");
#endif
        uns aktiter=1;
        mas.par_set_semi_init(false);
        mas.par_set_I_for_semi(true); // f�lvezet� nemlinearit�s�hoz, termikusn�l is kell a disszip�ci�hoz a gcsd sz�m�t�shoz
reiterate_elth:
        do{
                mas.par_set_quad(quad_el); // mas.par_set_quad((valostipus&1)!=0); uj
                mas.par_set_tipus(FieldEl);
                mas.del_azon();
            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,aktiter*2," ");
            uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
//printf("\nelozo_T_hiba=%g\takt_T_hiba=%g\telozo_u_hiba=%g\takt_u_hiba=%g\n",elozo_T_hiba,akt_T_hiba,elozo_u_hiba,akt_u_hiba);
                //cd T_arany=(akt_T_hiba/(elozo_T_hiba-akt_T_hiba));
                //cd T_szorzo = (elozo_T_hiba==akt_T_hiba) ? 0.5 : (T_arany<0.0 ? 0.0 : (T_arany>100.0 ? 100.0 : T_arany));
                //cd u_arany=(akt_u_hiba/(elozo_u_hiba-akt_u_hiba));
                //cd u_szorzo = (elozo_u_hiba==akt_u_hiba) ? 0.5 : (u_arany<0.0 ? 0.0 : (u_arany>100.0 ? 100.0 : u_arany));
                cd vegyes_arany=((akt_T_hiba+akt_u_hiba)/(elozo_T_hiba-akt_T_hiba+elozo_u_hiba-akt_u_hiba));
                dbl vegyes_szorzo=(elozo_u_hiba+elozo_T_hiba==akt_u_hiba+akt_T_hiba) ? 0.5 : (vegyes_arany<0.0 ? 0.0 : (vegyes_arany>20.0 ? 20.0 : vegyes_arany));
//printf("\nT_a=%g\tT_sz=%g\tU_a=%g\tU_sz=%g\n",T_arany,T_szorzo,u_arany,u_szorzo);
            //vegyes_szorzo=0.0;
                if(akt_T_hiba+akt_u_hiba>0.02)vegyes_szorzo=0.0;
                if(is_no_accelerator)vegyes_szorzo=0.0; // ha a szimul�ci�s f�jlban ki lett kapcsolva, akkor csak a k�z�pre h�z�s megy.
                mas.konvergens_U_T_szamito(vegyes_szorzo);//u_szorzo<T_szorzo?u_szorzo:T_szorzo);
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
// Nem tudom, hogy a k�vetkez� sor mi�rt volt benne, mert ha bent van, Tamb-bel sz�mol, nem a val�s h�m�rs�klettel.
//                mas.par_set_start_from_map(false); // a fill h�m. t�rk�pk�nt kezeli, de El. szim.-n�l nincs h�m
                mas.fill_step0_adm(3);
                mas.fill_step0_curr(4);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul);
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot); // itt a swap_UT kimarad, rem�lhet�leg t�nyleg nem kell p�tolni semmivel
                mas.swap_u(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                mas.get_u_i_matrix(1);
                if(aktiter>tanal[anal_index].ndc_maxiter-4)mas.write_fim(FileName+(aktiter+1)+'.',2,true,0); //!!
#ifdef BEALLAS
fprintf(fp,"%u\t%g\t%.10g\t",aktiter,log(double(aktiter)),mas.get_xyz_r(64,64,1));// 15,13,1 volt a 32-esn�l
//fprintf(fp,"%u\t%g\t%.10g\t",aktiter,log(double(aktiter)),mas.get_xyz_r(15,10,1));// 15,13,1 volt a 32-esn�l
#endif
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;
            elozo_u_hiba=akt_u_hiba;
            akt_u_hiba=mas.get_max_error(mas.get_u_azon(),mas.get_old_u_azon());
            if(ConsoleText)printf("\r%74f%%(el)",akt_u_hiba*100.0);

                mas.del_azon();
                mas.par_set_quad(quad_th); // mas.par_set_quad((valostipus&2)!=0);
                mas.par_set_el_th_diss(true);
                mas.par_set_tipus(FieldTherm);

            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,aktiter*2+1," ");
            uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(nulla, powmap);
                mas.fill_step0_curr(4, powmap);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul);
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot);
                mas.swap_T(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                mas.get_T_matrix(1);
            if(aktiter>tanal[anal_index].ndc_maxiter-4)mas.write_fim(FileName+(aktiter+1)+'.',2,true,0); //!!
#ifdef BEALLAS
fprintf(fp,"%.10g\n",mas.get_xyz_r(64,64,1));// 15,13,1 volt a 32-esn�l
//fprintf(fp,"%.10g\n",mas.get_xyz_r(15,10,1));// 15,13,1 volt a 32-esn�l
#endif
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;

            ++aktiter;
            global_akt_iter=aktiter;
            elozo_T_hiba=akt_T_hiba;
            akt_T_hiba=mas.get_max_error(mas.get_T_azon(),mas.get_old_T_azon());
            if(ConsoleText){
                printf("\r%4d. iter   el=%14.7f%%   th=%14.7f%%",aktiter,akt_u_hiba*100.0,akt_T_hiba*100.0);
                if(aktiter%3==1)printf("   accelerator = %-12g",vegyes_szorzo);
                else printf("                             ");
                printf("\n");
            }
        }while(aktiter<ndc_miniter || (aktiter<tanal[anal_index].ndc_maxiter
            && (!mas.check_error(mas.get_u_azon(),mas.get_old_u_azon(),tanal[anal_index].relhiba)
            ||!mas.check_error(mas.get_T_azon(),mas.get_old_T_azon(),tanal[anal_index].relhiba))));
#ifdef BEALLAS
fclose(fp);
#endif
        if( quad_el != ((valostipus&1)!=0) || quad_th != ((valostipus&2)!=0)){ // uj
            quad_el = ((valostipus&1)!=0);
            quad_th = ((valostipus&2)!=0);
            goto reiterate_elth;
        }
        mas.write_fim(FileName,2,true,0); // termikus
        mas.par_set_tipus(FieldEl); mas.write_fim(FileName,2,aktiter<tanal[anal_index].ndc_maxiter,0); // elektromos
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      setAnalStepDb(2,aktiter*2);
      setAnalKeszStep(2,aktiter*2);
        if(ConsoleText)printf("\n\nmax error=%g%%(el), %g%%(th)\n",
            mas.get_max_error(mas.get_u_azon(),mas.get_old_u_azon())*100.0,
            mas.get_max_error(mas.get_T_azon(),mas.get_old_T_azon())*100.0);
    }
    delete[] powmap;
}

void backup_restore_file(const PLString & FajlNev, bool restore);

//***********************************************************************
void simulation::run_transi(const PLString & FileName,uns anal_index,AnalizisTipus tipus,dbl from, dbl to, dbl step){
//***********************************************************************
    //logitherm_interface.first_step(anal_index,from);
    //for(uns i=uns(to/from)+1; i>0; i--)logitherm_interface.next_step();
    //return;
    masina mas;
    mas.del_all();
    mas.par_clear(0);
    mas.par_set_ac(false);
    mas.par_set_quad(false);
    mas.par_set_pot_map(0);
    mas.par_set_temp_map(1);
    mas.par_set_curr_map(2);
    mas.par_set_old_pot_map(3,5);
    mas.par_set_old_temp_map(4,6);
    mas.par_set_old_curr_map(7,8);
//    mas.par_set_start_UT(true); // �ll�tson kezd�h�m�rs�kletet
    mas.par_set_I_for_semi(false);
    mas.par_set_start_from_map(false);
    mas.par_set_el_th_Seebeck(!this->is_no_seebeck);
    mas.par_set_el_th_diss(false); // ez most nem elektro-termikus
    mas.par_set_th_el_Seebeck(!this->is_no_seebeck);
    mas.par_set_transi(true);
    mas.par_set_timeconst(false);
    mas.par_set_dt(nulla);
    mas.par_set_f(nulla);
    mas.par_set_t(nulla);
    mas.par_set_anal_step(1);
    mas.par_set_s(nulla);
    mas.par_set_sim(this);
    mas.par_set_tipus(FieldTherm);
    mas.par_set_anal_index(anal_index);
    mas.par_set_fim_txt(is_fim_txt);
    is_simple_anal_progress = true;

    dbl *powmap = nullptr;
    if ((mezo == FieldTherm || mezo == FieldElTherm) && pmap.is_exists()) {
        powmap = new dbl[pmodel->x_res*pmodel->y_res*pmodel->z_res];
    }

    if(mezo!=FieldElTherm){
        mas.par_set_quad(valostipus!=0);
        mas.par_set_tipus(mezo);
        mas.par_set_semi_init(true);
        mas.par_set_el_th_Seebeck(false);
        mas.par_set_dt(from);
        mas.par_set_t(from);
        mas.par_set_anal_step(1);
      resetAnalLevel(2);
      setAnalStepDb(2,uns(log10(to/from)*step+2.5));
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,0," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,mezo==FieldEl?"set V0":"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(from, powmap);
        mas.fill_step0_curr(4, powmap);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2);
        mas.backsubs(0,aramot);
        if(mezo==FieldEl)mas.get_u_i_matrix(1);
        else mas.get_T_matrix(1);
        mas.write_fim(FileName,2,true,4);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
        if(ConsoleText){
            printf("\r%4d. step                            ",1);
            printf("   actual time = %-13.11g",from);
            printf("\n");
        }

        uns aktstep=2; // a fim f�jl sz�ma
        cd ddt = (tipus==AnalLogTran) ? pow(10.0,1.0/step) : from; // ((to-from)/step)
        dbl akt_dt = from;
        switch(tipus){
            case AnalLinTran: akt_dt = ddt; break;
            case AnalLogTran: akt_dt=from*(ddt-1.0); break;
            default: throw hiba("simulation::run_transi","impossible Analtipus");
        }
        dbl akt_t = from + akt_dt;
        mas.par_set_dt(akt_dt);
        mas.par_set_t(akt_t);
        mas.par_set_anal_step(aktstep);
        dbl last_t;
        mas.par_set_semi_init(false);
        if(mezo==FieldEl)mas.par_set_I_for_semi(true); // f�lvezet� nemlinearit�s�hoz
        bool kell_nodered = true;
        kell_nodered = !is_lin || tipus!=AnalLinTran;
        do{
//printTime("Runtime","","",1);
                if(kell_nodered)mas.del_azon(); // csak a h�l�t t�rli
            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,aktstep,"OK");
            uchannel_update();
                if (kell_nodered)mas.foglal_halot(0);
                if(kell_nodered)mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,mezo==FieldEl?"set V0":"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                if(mezo==FieldEl)mas.par_set_start_from_map(false); // a fill h�m. t�rk�pk�nt kezeli, de El. szim.-n�l nincs h�m
                if(kell_nodered)mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(akt_t, powmap);
                mas.fill_step0_curr(4, powmap);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                if(kell_nodered)mas.nodered(is_always_strassen_mul);
                else mas.forwsubs(0);
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2);
                mas.backsubs(0,aramot);
                mas.swap_u();
                mas.swap_T();
                if(mezo==FieldEl)mas.get_u_i_matrix(1);
                else mas.get_T_matrix(1);
                mas.par_set_anal_step(aktstep);
                mas.write_fim(FileName,2,true,4);
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;
            if(ConsoleText){
                printf("\r%4d. step                            ",aktstep);
                printf("   actual time = %-13.11g",akt_t);
                printf("\n");
            }
            last_t=akt_t;
            switch(tipus){
                case AnalLinTran: akt_dt  = ddt; akt_t += akt_dt; break;
                case AnalLogTran: akt_dt=akt_t*(ddt-1.0); akt_t+=akt_dt; break;
                default: throw hiba("simulation::run_transi","impossible Analtipus");
            }
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_t);
            aktstep++;
        }while(last_t<=to);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      setAnalStepDb(2,aktstep-1);
      setAnalKeszStep(2,aktstep-1);
    }
    else{

        // auto transi

        if(auto_tra.is_T)prev_T_map.resize(pmodel->x_res*pmodel->y_res*pmodel->z_res);
        if(auto_tra.is_V)prev_U_map.resize(pmodel->x_res*pmodel->y_res*pmodel->z_res);
        if(auto_tra.is_T)akt_T_map.resize(pmodel->x_res*pmodel->y_res*pmodel->z_res);
        if(auto_tra.is_V)akt_U_map.resize(pmodel->x_res*pmodel->y_res*pmodel->z_res);

        // elektromos

        mas.par_set_quad((valostipus&1)!=0);
        mas.par_set_tipus(FieldEl);
        mas.par_set_semi_init(true);
        mas.par_set_dt(from);
        mas.par_set_t(from);
        mas.par_set_anal_step(1);
      resetAnalLevel(2);
      setAnalStepDb(2,uns(log10(to/from)*step+2.5)*2);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,0," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        mas.fill_step0_curr(4);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
        mas.backsubs(0,aramot);
        mas.get_u_i_matrix(1);
        mas.write_fim(FileName,2,true,4);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
        if(ConsoleText){
            printf("\r%4d. step (el)                       ",1);
            printf("   actual time = %-13.11g",from);
            printf("\n");
        }

        // f�lvezet� elektromos tranziens�nek lecsenget�se

        for(uns semi_konv = 0; semi_konv < el_nonlin_subiter-1; semi_konv++){

            if(semi_konv==0){

                // termikus

                mas.del_azon();
                mas.par_set_quad((valostipus&2)!=0);
                mas.par_set_el_th_diss(true);
                mas.par_set_tipus(FieldTherm);

              resetAnalLevel(1);
              setAnalStepDb(1,3);
              resetAnalLevel(0);
              proti=PT_NoderedElott;
              setAnalStepDb(0,5);
              setAnalKeszStep(2,1," ");
              uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(from, powmap);
                mas.fill_step0_curr(4, powmap);
              setAnalKeszStep(0,5);
              setAnalKeszStep(1,1);
              proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul);
              resetAnalLevel(0);
              proti=PT_NoderedUtan;
              setAnalStepDb(0,3);
              setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot);
                mas.get_T_matrix(1);
                //mas.write_fim(FileName,2,true,4);
              setAnalKeszStep(0,3,"OK");
              setAnalKeszStep(1,3);
              proti=PT_Egyenletes;
                if(ConsoleText){
                    printf("\r%4d. step (th)                       ",1);
                    printf("   actual time = %-13.11g",from);
                    printf("\n");
                }
            }

                // elektromos

                mas.par_set_quad((valostipus&1)!=0);
                mas.par_set_tipus(FieldEl);
                mas.del_azon();
            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,0," ");
            uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                mas.fill_step0_curr(4);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul);
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot); // itt a swap_UT kimarad, rem�lhet�leg t�nyleg nem kell p�tolni semmivel
                mas.swap_u(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                mas.get_u_i_matrix(1);
                mas.par_set_anal_step(0);
                if(semi_konv==el_nonlin_subiter-2)
                    mas.write_fim(FileName,2,true,4);
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;
            if(ConsoleText){
                printf("\r%4d. step (el) %2d. substep           ",0,semi_konv+1);
                printf("   actual time = %-13.11g",from);
                printf("\n");
            }
        }

        // termikus

        mas.del_azon();
        mas.par_set_quad((valostipus&2)!=0);
        mas.par_set_el_th_diss(true);
        mas.par_set_tipus(FieldTherm);

      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,1," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(from, powmap);
        mas.fill_step0_curr(4, powmap);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
        mas.backsubs(0,aramot);
        mas.get_T_matrix(1);
        mas.write_fim(FileName,2,true,4);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
        if(ConsoleText){
            printf("\r%4d. step (th)                       ",1);
            printf("   actual time = %-13.11g",from);
            printf("\n");
        }

        // be�ll�t�s

        uns aktstep=2; // a fim f�jl sz�ma
        cd ddt = (tipus==AnalLogTran) ? pow(10.0,1.0/step) : from; // ((to-from)/step)
        dbl akt_dt = from;
        switch(tipus){
            case AnalLinTran: akt_dt = ddt; break;
            case AnalLogTran: akt_dt=from*(ddt-1.0); break;
            default: throw hiba("simulation::run_transi","impossible Analtipus");
        }
        dbl akt_t = from + akt_dt;
        mas.par_set_dt(akt_dt);
        mas.par_set_t(akt_t);
        mas.par_set_anal_step(aktstep);
        dbl last_t;
        mas.par_set_semi_init(false);
        mas.par_set_I_for_semi(true); // f�lvezet� nemlinearit�s�hoz, termikusn�l is kell a disszip�ci�hoz a gcsd sz�m�t�shoz

        do{
            
            // elektromos

            for(uns semi_konv = 0; semi_konv < el_nonlin_subiter; semi_konv++){
                    mas.par_set_quad((valostipus&1)!=0);
                    mas.par_set_tipus(FieldEl);
                    mas.del_azon();
                resetAnalLevel(1);
                setAnalStepDb(1,3);
                resetAnalLevel(0);
                proti=PT_NoderedElott;
                setAnalStepDb(0,5);
                setAnalKeszStep(2,aktstep*2," ");
                uchannel_update();
                    mas.foglal_halot(0);
                    mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                    mas.par_set_start_from_map(true); // fesz �s h�m is
                    mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                    mas.fill_step0_adm(3);
                    mas.fill_step0_curr(4);
                setAnalKeszStep(0,5);
                setAnalKeszStep(1,1);
                proti=PT_Nodered;
                    mas.nodered(is_always_strassen_mul);
                resetAnalLevel(0);
                proti=PT_NoderedUtan;
                setAnalStepDb(0,3);
                setAnalKeszStep(1,2," ");
                    mas.backsubs(0,aramot); // itt a swap_UT kimarad, rem�lhet�leg t�nyleg nem kell p�tolni semmivel
                    mas.swap_u(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                    mas.get_u_i_matrix(1);
                    mas.par_set_anal_step(aktstep);
                    if(semi_konv==el_nonlin_subiter-1)mas.write_fim(FileName,2,true,4);
                setAnalKeszStep(0,3,"OK");
                setAnalKeszStep(1,3);
                proti=PT_Egyenletes;
                if(ConsoleText){
                    printf("\r%4d. step (el) %2d. substep           ",aktstep,semi_konv+1);
                    printf("   actual time = %-13.11g",akt_t);
                    printf("\n");
                }
            }

            // termikus

                mas.del_azon();
                mas.par_set_quad((valostipus&2)!=0);
                mas.par_set_el_th_diss(true);
                mas.par_set_tipus(FieldTherm);

            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,aktstep*2+1," ");
            uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(akt_t, powmap);
                mas.fill_step0_curr(4, powmap);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul);
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot);
                mas.swap_T(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                mas.get_T_matrix(1);
                mas.par_set_anal_step(aktstep);
                mas.write_fim(FileName,2,true,4);
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;

            if(ConsoleText){
                printf("\r%4d. step (th)                       ",aktstep);
                printf("   actual time = %-13.11g",akt_t);
                printf("\n");
            }

            // l�ptet�s

            last_t=akt_t;
            switch(tipus){
                case AnalLinTran: akt_dt  = ddt; akt_t += akt_dt; break;
                case AnalLogTran: akt_dt=akt_t*(ddt-1.0); akt_t+=akt_dt; break;
                default: throw hiba("simulation::run_transi","impossible Analtipus");
            }
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_t);
            aktstep++;
        }while(last_t<=to);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
    }
    delete[] powmap;
}

/*
//***********************************************************************
void simulation::run_controlled(const PLString & FileName,uns anal_index){
//***********************************************************************
    masina mas;
    mas.del_all();
    mas.par_clear(0);
    mas.par_set_ac(false);
    mas.par_set_quad(false);
    mas.par_set_pot_map(0);
    mas.par_set_temp_map(1);
    mas.par_set_curr_map(2);
    mas.par_set_old_pot_map(3,5);
    mas.par_set_old_temp_map(4,6);
    mas.par_set_old_curr_map(7,8);
//    mas.par_set_start_UT(true); // �ll�tson kezd�h�m�rs�kletet
    mas.par_set_I_for_semi(false);
    mas.par_set_start_from_map(false);
    mas.par_set_el_th_Seebeck(!this->is_no_seebeck);
    mas.par_set_el_th_diss(false); // ez most nem elektro-termikus
    mas.par_set_th_el_Seebeck(!this->is_no_seebeck);
    mas.par_set_transi(true);
    mas.par_set_timeconst(false);
    mas.par_set_dt(nulla);
    mas.par_set_f(nulla);
    mas.par_set_t(nulla);
    mas.par_set_anal_step(1);
    mas.par_set_s(nulla);
    mas.par_set_sim(this);
    mas.par_set_tipus(FieldTherm);
    mas.par_set_anal_index(anal_index);
    mas.par_set_fim_txt(is_fim_txt);
    pmodel->clear_tseged();
    is_simple_anal_progress = true;

    is_transi_step_as_dc = true; // ezt majd a simulation::read �ll�tja be!

    uns step = 1;
    analysis & akt_anal = tanal[anal_index];
    tomb<change_time> & ctrl = akt_anal.ctrl;
    akt_anal.fill_times();

    dbl *powmap = nullptr;
    if ((mezo == FieldTherm || mezo == FieldElTherm) && pmap.is_exists()) {
        powmap = new dbl[pmodel->x_res*pmodel->y_res*pmodel->z_res];
    }

    if(mezo!=FieldElTherm){
        mas.par_set_quad(valostipus!=0);
        mas.par_set_tipus(mezo);
        mas.par_set_semi_init(true);
        mas.par_set_el_th_Seebeck(false);
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step - 1].time );
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(1);
      resetAnalLevel(2);
      setAnalStepDb(2,akt_anal.times.size()-1);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,0," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,mezo==FieldEl?"set V0":"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(akt_anal.times[step].time, powmap);
        mas.fill_step0_curr(4, powmap);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2);
        mas.backsubs(0,aramot);
        if(mezo==FieldEl)mas.get_u_i_matrix(1);
        else mas.get_T_matrix(1);
        mas.write_fim(FileName,2,true,4);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
        if(ConsoleText){
            printf("\r%4d. step                            ",1);
            printf("   actual time = %-13.11g",akt_anal.times[step].time);
            printf("\n");
        }

        // Gerjeszt�sek �t�ll�t�sa

        time_and_change & change = akt_anal.times[step];
        if(change.change_index>=0){
            for(u32 xi=0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++){
                excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                if(akt_excit.is_el){
                    texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                }
                else{
                    texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                }
            }
        }


        step=2; // a fim f�jl sz�ma
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step-1].time);
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(step);
        mas.par_set_semi_init(false);
        if(mezo==FieldEl)mas.par_set_I_for_semi(true); // f�lvezet� nemlinearit�s�hoz
        bool kell_nodered = true;
        do{
            mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step-1].time);
            mas.par_set_t(akt_anal.times[step].time);
                if(kell_nodered)mas.del_azon(); // csak a h�l�t t�rli
            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,step,"OK");
            uchannel_update();
                if (kell_nodered)mas.foglal_halot(0);
                if(kell_nodered)mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,mezo==FieldEl?"set V0":"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                if(mezo==FieldEl)mas.par_set_start_from_map(false); // a fill h�m. t�rk�pk�nt kezeli, de El. szim.-n�l nincs h�m
                if(kell_nodered)mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(akt_anal.times[step].time, powmap);
                mas.fill_step0_curr(4, powmap);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                if(kell_nodered)mas.nodered(is_always_strassen_mul);
                else mas.forwsubs(0);
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2);
                mas.backsubs(0,aramot);
                mas.swap_u();
                mas.swap_T();
                if(mezo==FieldEl)mas.get_u_i_matrix(1);
                else mas.get_T_matrix(1);
                mas.par_set_anal_step(step);
                mas.write_fim(FileName,2,true,4);
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;
            if(ConsoleText){
                printf("\r%4d. step                            ",step);
                printf("   actual time = %-13.11g",akt_anal.times[step].time);
                printf("\n");
            }

                // Gerjeszt�sek �t�ll�t�sa

                kell_nodered = !is_lin;
                time_and_change & change = akt_anal.times[step];
                if(change.change_index>=0){
                    kell_nodered = true;
                    for(u32 xi=0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++){
                        excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                        if(akt_excit.is_el){
                            texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                            texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                        }
                        else{
                            texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                            texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                        }
                    }
                }

            step++;
        }while(step<akt_anal.times.size());
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      setAnalStepDb(2,step-1);
      setAnalKeszStep(2,step-1);
    }
    else{

        // elektromos

        mas.par_set_quad((valostipus&1)!=0);
        mas.par_set_tipus(FieldEl);
        mas.par_set_semi_init(true);
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step - 1].time );
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(1);
      resetAnalLevel(2);
      setAnalStepDb(2,akt_anal.times.size()*2-2);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,1," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        mas.fill_step0_curr(4);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
        mas.backsubs(0,aramot);
        mas.get_u_i_matrix(1);
        if (auto_tra.is_V) // rendes tranziensn�l in-lin eset�n ne legyen
            mas.get_center_values(prev_U_map); // auto transi steps
        mas.write_fim(FileName,2,true,4);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
        if(ConsoleText){
            printf("\r%4d. step (el)                       ",1);
            printf("   actual time = %-13.11g",akt_anal.times[step].time);
            printf("\n");
        }

        // f�lvezet� elektromos tranziens�nek lecsenget�se

        for(uns semi_konv = 0; semi_konv < el_nonlin_subiter-1; semi_konv++){

            if(semi_konv==0){

                // termikus

                mas.del_azon();
                mas.par_set_quad((valostipus&2)!=0);
                mas.par_set_el_th_diss(true);
                mas.par_set_tipus(FieldTherm);

              resetAnalLevel(1);
              setAnalStepDb(1,3);
              resetAnalLevel(0);
              proti=PT_NoderedElott;
              setAnalStepDb(0,5);
              setAnalKeszStep(2,2," ");
              uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(akt_anal.times[step].time, powmap);
                mas.fill_step0_curr(4, powmap);
              setAnalKeszStep(0,5);
              setAnalKeszStep(1,1);
              proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul);
              resetAnalLevel(0);
              proti=PT_NoderedUtan;
              setAnalStepDb(0,3);
              setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot);
                mas.get_T_matrix(1);
                //mas.write_fim(FileName,2,true,4);
              setAnalKeszStep(0,3,"OK");
              setAnalKeszStep(1,3);
              proti=PT_Egyenletes;
                if(ConsoleText){
                    printf("\r%4d. step (th)                       ",1);
                    printf("   actual time = %-13.11g",akt_anal.times[step].time);
                    printf("\n");
                }
            }

                // elektromos

                mas.par_set_quad((valostipus&1)!=0);
                mas.par_set_tipus(FieldEl);
                mas.del_azon();
            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,1," ");
            uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                mas.fill_step0_curr(4);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul);
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot); // itt a swap_UT kimarad, rem�lhet�leg t�nyleg nem kell p�tolni semmivel
                mas.swap_u(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                mas.get_u_i_matrix(1);
                if (auto_tra.is_V && semi_konv == el_nonlin_subiter - 2)
                    mas.get_center_values(prev_U_map); // auto transi steps
                mas.par_set_anal_step(0);
                if(semi_konv==el_nonlin_subiter-2)
                    mas.write_fim(FileName,2,true,4);
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;
            if(ConsoleText){
                printf("\r%4d. step (el) %2d. substep           ",0,semi_konv+1);
                printf("   actual time = %-13.11g",akt_anal.times[step].time);
                printf("\n");
            }
        }

        // termikus

        mas.del_azon();
        mas.par_set_quad((valostipus&2)!=0);
        mas.par_set_el_th_diss(true);
        mas.par_set_tipus(FieldTherm);

      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,2," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(akt_anal.times[step].time, powmap);
        mas.fill_step0_curr(4, powmap);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
        mas.backsubs(0,aramot);
        mas.get_T_matrix(1);
        if (auto_tra.is_T)
            mas.get_center_values(prev_T_map); // auto transi steps
        mas.write_fim(FileName,2,true,4);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
        if(ConsoleText){
            printf("\r%4d. step (th)                       ",1);
            printf("   actual time = %-13.11g",akt_anal.times[step].time);
            printf("\n");
        }

        // Gerjeszt�sek �t�ll�t�sa

        time_and_change & change = akt_anal.times[step];
        if(change.change_index>=0){
            for(u32 xi=0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++){
                excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                if(akt_excit.is_el){
                    texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                }
                else{
                    texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                }
            }
        }

        // be�ll�t�s

        step=2; // a fim f�jl sz�ma
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step-1].time);
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(step);
        mas.par_set_semi_init(false);
        mas.par_set_I_for_semi(true); // f�lvezet� nemlinearit�s�hoz, termikusn�l is kell a disszip�ci�hoz a gcsd sz�m�t�shoz

        uns ossz_plusz_lepes=0,sikeres_plusz_lepes=0;
        bool restore=false;
        dbl akt_time=akt_anal.times[step].time, akt_dt=akt_anal.times[step].time - akt_anal.times[step-1].time;

        // auto transi steps
        // - a prev_U_map / prev_T_map m�r inicializ�lva kell legyen, akt_U_map / akt_T_map inicializ�latlan

        autotr_state auto_allapot = atr_normal;
        dbl sikeres_t = akt_anal.times[1].time; // Az els� l�p�s id�pontja, ezt sikeresnek tekintj�k
        dbl auto_dt = -1.0; // ha nem auto m�dban vagyunk, akkor ezt a -1 jelzi
        bool auto_ellenorzes = change.change_index < 0, is_el_hiba_volt = true;
        bool his_ellenorzes = true;
        uns V_plus_steps = 0, T_plus_steps = 0, H_plus_steps = 0, hanyadik_koztes_lepes = 0;
        const char * lepesnev = "normal";
        char s_auto[21]="";
        bool is_elfogad_elozo_elektromos = false;
        mas.swap_Ge();

        do{
            //mas.push();
            if(auto_allapot == atr_normal){
                mas.par_set_dt( akt_dt = akt_anal.times[step].time - akt_anal.times[step-1].time );
                mas.par_set_t( akt_time = akt_anal.times[step].time );
                V_plus_steps = T_plus_steps = 0;
                hanyadik_koztes_lepes = 0;
            }
            else{
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_time);
                hanyadik_koztes_lepes++;
            }
            
            // elektromos, ha nem lehet elfogadni a tesztel�s eredm�ny�t (pl. v�ltozott a gerjeszt�s)
            // (szerintem nem baj, ha nincs elektromos swap back el�tte, a ciklus v�g�r�l j�n: id�f�ggetlen �s legal�bb simul a nonlinearit�s, ha van)

            if(!is_elfogad_elozo_elektromos){
                bool is_abs_needed_V = false; // auto transi steps
                for(uns semi_konv = 0; semi_konv < el_nonlin_subiter; semi_konv++){
                        mas.par_set_quad((valostipus&1)!=0);
                        mas.par_set_tipus(FieldEl);
                        mas.del_azon();
                    resetAnalLevel(1);
                    setAnalStepDb(1,3);
                    resetAnalLevel(0);
                    proti=PT_NoderedElott;
                    setAnalStepDb(0,5);
                    setAnalStepDb(2,akt_anal.times.size()*2-2+ossz_plusz_lepes*2);
                    setAnalKeszStep(2,step*2-1+ossz_plusz_lepes*2," ");
                    uchannel_update();
                        mas.foglal_halot(0);
                        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                        mas.par_set_start_from_map(true); // fesz �s h�m is
                        mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                        mas.fill_step0_adm(3);
                        mas.fill_step0_curr(4);
                    setAnalKeszStep(0,5);
                    setAnalKeszStep(1,1);
                    proti=PT_Nodered;
                        mas.nodered(is_always_strassen_mul);
                    resetAnalLevel(0);
                    proti=PT_NoderedUtan;
                    setAnalStepDb(0,3);
                    setAnalKeszStep(1,2," ");
                        mas.backsubs(0,aramot); // itt a swap_UT kimarad, rem�lhet�leg t�nyleg nem kell p�tolni semmivel
                        mas.swap_u(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                        mas.get_u_i_matrix(1);
                        if (auto_tra.is_V && semi_konv == el_nonlin_subiter - 1)
                            mas.get_center_values(akt_U_map); // auto transi steps
                        mas.par_set_anal_step(step+sikeres_plusz_lepes);
                        if (semi_konv == el_nonlin_subiter - 1) {
                            if (auto_ellenorzes)
                                is_abs_needed_V = is_auto_back_step_needed_U(akt_dt); // auto transi steps
                                //is_abs_needed_V = auto_tra.is_V && akt_dt > auto_tra.V_min_dt && mas.is_Ge_error(0.01); // auto transi steps
                            if (!is_abs_needed_V && ( !auto_tra.is_no_plus_step_data || akt_time == akt_anal.times[step].time))
                                mas.write_fim(FileName, 2, true, 4, restore);
                        }
                    setAnalKeszStep(0,3,"OK");
                    setAnalKeszStep(1,3);
                    proti=PT_Egyenletes;
                    if(ConsoleText){
                        if(el_nonlin_subiter>1)printf("\r%4d. step (el) %2d. substep           ",step+ossz_plusz_lepes,semi_konv+1);
                        else printf("\r%4d. step (el)  %-20s ", step + ossz_plusz_lepes, lepesnev);
                        printf("   actual time = %-13.11g",akt_time);
                        printf("\n");
                    }
                }
                if (auto_ellenorzes && is_abs_needed_V) { // auto transi steps
                    restore = true;
                    is_el_hiba_volt = true;
                    akt_time = sikeres_t;

                    auto_dt = akt_dt = auto_allapot == atr_auto ? akt_dt/2 : auto_tra.V_min_dt; // Az elektromosn�l �gyis mindig ide dob vissza, nem �rdemes felezgetni
                    if (akt_dt < auto_tra.V_min_dt)
                        auto_dt = akt_dt = auto_tra.V_min_dt;

//                    auto_dt = akt_dt = auto_tra.V_min_dt;
                    //auto_dt = akt_dt /= 2; // nem kell ellen�rizni, hogy a megengedettn�l kisebb-e, mert az is_auto_back_step_needed_T �gy is megfogja
                    //while(akt_dt>auto_tra.V_min_dt)auto_dt = akt_dt /= 2;
                    akt_time += akt_dt;
                    // az utols� �rv�nyes h�m�rs�kleti adatot is backupolni kell (e n�lk�l nem t�rt�nik meg, mert a 
                    // termikus tranziens l�p�st m�r nem sz�m�tjuk, ami ezt h�vn� a write_fim-en kereszt�l)
                    backup_restore_file(FileName + "thermal/probe.trz", false);
                    backup_restore_file(FileName + "thermal/szimpla.trz", false);
                    auto_allapot = atr_auto;
                    lepesnev = akt_dt > auto_tra.V_min_dt ? "auto back V" : "auto back V (min)";
                    ossz_plusz_lepes++;
// Mivel az elektromos t�r reziszt�v, felesleges �jrasz�molni az elektromos l�p�st. A kor�bbi verzi�ban ez volt, az �j verzi�ban a komment ut�ni r�szek lesznek.
//                    mas.pop();
//                    continue;
//
                    mas.par_set_dt(akt_dt);
                    mas.par_set_t(akt_time);
                    hanyadik_koztes_lepes++;
                }
            }

            // termikus

termikus_ujra: // ha nem akarjuk �jrafuttatni az elektromos r�szt visszal�p�sn�l

                mas.del_azon();
                mas.par_set_quad((valostipus&2)!=0);
                mas.par_set_el_th_diss(true);
                mas.par_set_tipus(FieldTherm);

            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,step*2+ossz_plusz_lepes*2," ");
            uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(akt_anal.times[step].time, powmap);
                mas.fill_step0_curr(4, powmap);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul); // mi�rt lass�, ha dt = 1e-10 ?
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot);
                mas.swap_T(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                mas.get_T_matrix(1);
                if (auto_tra.is_T)
                    mas.get_center_values(akt_T_map); // auto transi steps
                mas.par_set_anal_step(step+sikeres_plusz_lepes);
                bool is_abs_needed_T = false; // auto transi steps
                if (auto_ellenorzes)
                    is_abs_needed_T = is_auto_back_step_needed_T(akt_dt); // auto transi steps
                if (!is_abs_needed_T && (!auto_tra.is_no_plus_step_data || akt_time == akt_anal.times[step].time))
                    mas.write_fim(FileName,2,true,4,restore);
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;

            if(ConsoleText){
                printf("\r%4d. step (th)  %-20s ", step+ossz_plusz_lepes, lepesnev);
                printf("   actual time = %-13.11g",akt_time);
                printf("\n");
            }
            if (auto_ellenorzes && is_abs_needed_T) { // auto transi steps
                restore = true;
                is_el_hiba_volt = false;
                akt_time = sikeres_t;

                auto_dt = akt_dt = auto_allapot == atr_auto ? akt_dt / 2 : auto_tra.T_min_dt;
                if (akt_dt < auto_tra.T_min_dt)
                    auto_dt = akt_dt = auto_tra.T_min_dt;
                
//                auto_dt = akt_dt = auto_tra.T_min_dt;
                //auto_dt = akt_dt /= 2; // nem kell ellen�rizni, hogy a megengedettn�l kisebb-e, mert az is_auto_back_step_needed_T �gy is megfogja
                akt_time += akt_dt;
                ossz_plusz_lepes++;
                lepesnev = akt_dt > auto_tra.V_min_dt ? "auto back T" : "auto back T (min)";
                auto_allapot = atr_auto;
// Mivel az elektromos t�r reziszt�v, felesleges �jrasz�molni az elektromos l�p�st. A kor�bbi verzi�ban ez volt, az �j verzi�ban a komment ut�ni r�szek lesznek.
//                mas.pop();
//                continue;
//
                mas.swap_back_T();
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_time);
                hanyadik_koztes_lepes++;
                goto termikus_ujra;
            }

            // Hiszter�zis ellen�rz�se

            dbl min_ido_arany=mas.get_min_ido_arany();
            if(his_ellenorzes && min_ido_arany<egy){ // kell visszal�p�s
                lepesnev = "his back";
                auto_allapot = atr_hisz_back;
                akt_time = sikeres_t;
                akt_dt *= min_ido_arany;
                akt_time += akt_dt;
                ossz_plusz_lepes++;
// Mivel az elektromos t�r reziszt�v, felesleges �jrasz�molni az elektromos l�p�st. A kor�bbi verzi�ban ez volt, az �j verzi�ban a komment ut�ni r�szek lesznek.
//                mas.pop();
//                continue;
//
                mas.swap_back_T();
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_time);
                hanyadik_koztes_lepes++;
                goto termikus_ujra;
            }

            // elektromos (teszteli, hogy a termikus l�p�s okozott e t�l nagy elektromos ugr�st)

            bool is_abs_needed_V = false; // auto transi steps
            for(uns semi_konv = 0; semi_konv < el_nonlin_subiter; semi_konv++){
                    mas.par_set_quad((valostipus&1)!=0);
                    mas.par_set_tipus(FieldEl);
                    mas.del_azon();
                resetAnalLevel(1);
                setAnalStepDb(1,3);
                resetAnalLevel(0);
                proti=PT_NoderedElott;
                setAnalStepDb(0,5);
                setAnalStepDb(2,akt_anal.times.size()*2-2+ossz_plusz_lepes*2);
                setAnalKeszStep(2,step*2-1+ossz_plusz_lepes*2," ");
                uchannel_update();
                    mas.foglal_halot(0);
                    mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                    mas.par_set_start_from_map(true); // fesz �s h�m is
                    mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                    mas.fill_step0_adm(3);
                    mas.fill_step0_curr(4);
                setAnalKeszStep(0,5);
                setAnalKeszStep(1,1);
                proti=PT_Nodered;
                    mas.nodered(is_always_strassen_mul);
                resetAnalLevel(0);
                proti=PT_NoderedUtan;
                setAnalStepDb(0,3);
                setAnalKeszStep(1,2," ");
                    mas.backsubs(0,aramot); // itt a swap_UT kimarad, rem�lhet�leg t�nyleg nem kell p�tolni semmivel
                    mas.swap_u(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                    mas.get_u_i_matrix(1);
                    if (auto_tra.is_V && semi_konv == el_nonlin_subiter - 1)
                        mas.get_center_values(akt_U_map); // auto transi steps
                    mas.par_set_anal_step(step+sikeres_plusz_lepes);
                    if (semi_konv == el_nonlin_subiter - 1) {
                        if (auto_ellenorzes)
                            is_abs_needed_V = is_auto_back_step_needed_U(akt_dt); // auto transi steps
                            //is_abs_needed_V = auto_tra.is_V && akt_dt > auto_tra.V_min_dt && mas.is_Ge_error(0.01); // auto transi steps
                        if (!is_abs_needed_V && ( !auto_tra.is_no_plus_step_data || akt_time == akt_anal.times[step].time))
                            mas.write_fim(FileName, 2, true, 4, restore);
                    }
                setAnalKeszStep(0,3,"OK");
                setAnalKeszStep(1,3);
                proti=PT_Egyenletes;
                if(ConsoleText){
                    if(el_nonlin_subiter>1)printf("\r%4d. step (el) %2d. substep           ",step+ossz_plusz_lepes,semi_konv+1);
                    else printf("\r%4d. step (el)  %-20s ", step + ossz_plusz_lepes, lepesnev);
                    printf("   actual time = %-13.11g",akt_time);
                    printf("\n");
                }
            }
            if (auto_ellenorzes && is_abs_needed_V) { // auto transi steps
                restore = true;
                is_el_hiba_volt = true;
                akt_time = sikeres_t;
 
                if (!is_elfogad_elozo_elektromos || auto_allapot == atr_hisz_back) { // Ha gerjeszt�sv�ltoz�s volt, akkor ugorhat a fesz, teh�t felesleges felezgetni.
                    auto_dt = akt_dt = auto_tra.V_min_dt;
                }
                else {
                    auto_dt = akt_dt = akt_dt / 8;
                    if (akt_dt < auto_tra.V_min_dt)
                        auto_dt = akt_dt = auto_tra.V_min_dt;
                }

//                auto_dt = akt_dt = auto_tra.V_min_dt;
                //auto_dt = akt_dt /= 2; // nem kell ellen�rizni, hogy a megengedettn�l kisebb-e, mert az is_auto_back_step_needed_T �gy is megfogja
                //while(akt_dt>auto_tra.V_min_dt)auto_dt = akt_dt /= 2;
                akt_time += akt_dt;
                // az utols� �rv�nyes h�m�rs�kleti adatot is backupolni kell (e n�lk�l nem t�rt�nik meg, mert a 
                // termikus tranziens l�p�st m�r nem sz�m�tjuk, ami ezt h�vn� a write_fim-en kereszt�l)
                backup_restore_file(FileName + "thermal/probe.trz", false);
                backup_restore_file(FileName + "thermal/szimpla.trz", false);
                auto_allapot = atr_auto;
                ossz_plusz_lepes++;
                lepesnev = akt_dt > auto_tra.V_min_dt ? "auto back V" : "auto back V (min)";
                mas.swap_back_T();
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_time);
                hanyadik_koztes_lepes++;
                goto termikus_ujra;
            }

            // j�v�hagyjuk az aktu�lis l�p�st

            sikeres_t = akt_time;       // auto transi steps
            if(akt_time >= akt_anal.times[step].time)
                V_plus_steps = T_plus_steps = H_plus_steps = 0;
            prev_T_map.swap(akt_T_map); // auto transi steps
            prev_U_map.swap(akt_U_map); // auto transi steps
            mas.swap_Ge();
            restore = false;
            auto_ellenorzes = true;     // auto transi steps, ha sikeres volt a l�p�s, akkor a k�v. l�p�sben m�r ellen�rizhet�nk
                                        // (Ha hiszter�zis miatt volt visszal�p�s auto_ellenorzes=false mellett, 
                                        // akkor k�zben nem volt auto ellen�rz�s, de ez pont �gy kell.)
            his_ellenorzes = true;
            is_elfogad_elozo_elektromos = true;
            cuns db = pmodel->x_res * pmodel->y_res * pmodel->z_res;
            for( uns it = 0; it < db; it++ )
                pmodel->tseged[it].update_his();

            switch (auto_allapot) {     // auto transi steps
                case atr_auto: // vissza kellett l�pni vagy l�peget�nk dt dupl�z�ssal
                    lepesnev = "auto";
                    sikeres_plusz_lepes++;
                    ossz_plusz_lepes++;
                    auto_dt = akt_dt = hanyadik_koztes_lepes == 1 
                        ? akt_anal.times[step].time - akt_anal.times[step - 1].time
                        : akt_dt * 8;
//                    auto_dt = akt_dt *= 8;
                    if (akt_time + akt_dt >= akt_anal.times[step].time) {
                        auto_allapot = atr_auto_fixpont;
                        lepesnev = "auto fixpont";
                        akt_dt = akt_anal.times[step].time - akt_time;
                        akt_time = akt_anal.times[step].time;
                    }
                    else {
                        akt_time += akt_dt;
                    }
                    if (is_el_hiba_volt) {
                        V_plus_steps++;
                        sprintf(s_auto, "auto (V: +%u)", V_plus_steps);
                        lepesnev = s_auto;
                        if (auto_tra.V_max_plus_steps > 0) {
                            if (V_plus_steps >= auto_tra.V_max_plus_steps) {
                                //auto_allapot = atr_normal;
                                auto_allapot = atr_auto_fixpont;
                                lepesnev = "auto fixpont V";
                                akt_dt = akt_anal.times[step].time - akt_time;
                                akt_time = akt_anal.times[step].time;
                                //V_plus_steps = T_plus_steps = 0;
                                auto_ellenorzes = false;
                                //sikeres_t = akt_time - akt_dt;
                            }
                        }
                    }
                    else {
                        T_plus_steps++;
                        sprintf(s_auto, "auto (T: +%u)", T_plus_steps);
                        lepesnev = s_auto;
                        if (auto_tra.T_max_plus_steps > 0) {
                            if (T_plus_steps >= auto_tra.T_max_plus_steps) {
                                //auto_allapot = atr_normal;
                                auto_allapot = atr_auto_fixpont;
                                lepesnev = "auto fixpont T";
                                akt_dt = akt_anal.times[step].time - akt_time;
                                akt_time = akt_anal.times[step].time;
                                //V_plus_steps = T_plus_steps = 0;
                                auto_ellenorzes = false;
                                //sikeres_t = akt_time - akt_dt;
                            }
                        }
                    }
                    continue;
                    break;
                case atr_hisz_back: // vissza kellett l�pni
                    lepesnev = "his back";
                    sikeres_plusz_lepes++;
                    ossz_plusz_lepes++;
                    H_plus_steps++;
                    sprintf(s_auto, "his back (+%u)", H_plus_steps);
                    lepesnev = s_auto;
                    if (H_plus_steps > 20) {
                        lepesnev = "his back jump 10";
                        auto_ellenorzes = his_ellenorzes = false;
                        H_plus_steps = 0;
                        dbl dholtart = 10.0 * (akt_time - akt_anal.times[step - 1].time) / (akt_anal.times[step].time - akt_anal.times[step - 1].time);
                        uns uholtart = (uns)dholtart;
                        if (uholtart < 9) {
                            lepesnev = "his back jump";
                            dbl uj_time = (uholtart + 1) * 0.1 * (akt_anal.times[step].time - akt_anal.times[step - 1].time);
                            akt_dt = akt_anal.times[step - 1].time + uj_time - akt_time;
                            akt_time = akt_anal.times[step - 1].time + uj_time;
                            continue;
                        }
                    }
                    if (auto_dt < nulla) {  // nem auto transi �zemm�dban vagyunk
                        akt_dt = akt_anal.times[step].time - akt_time;
                        akt_time = akt_anal.times[step].time;
                        auto_allapot = atr_normal;
                        //lepesnev = "normal";
                        sprintf(s_auto, "normal (H: +%u)", H_plus_steps);
                        lepesnev = s_auto;
                    }
                    else {              // auto transi �zemm�dban vagyunk
                        akt_dt = auto_dt;
                        if (akt_time + akt_dt >= akt_anal.times[step].time) {
                            auto_allapot = atr_auto_fixpont;
                            //lepesnev = "auto fixpont";
                            sprintf(s_auto, "auto fix (H: +%u)", H_plus_steps);
                            lepesnev = s_auto;
                            akt_dt = akt_anal.times[step].time - akt_time;
                            akt_time = akt_anal.times[step].time;
                        }
                        else {
                            auto_allapot = atr_auto;
                            //lepesnev = "auto";
                            sprintf(s_auto, "auto (H: +%u)", H_plus_steps);
                            lepesnev = s_auto;
                            akt_time += akt_dt;
                        }
                    }
                    continue;
                    break;
                case atr_auto_fixpont:
                    akt_dt = auto_dt;
                    if (akt_dt >= akt_anal.times[step].time - akt_anal.times[step - 1].time) {
                        auto_allapot = atr_normal;
                        lepesnev = "normal";
                        auto_dt = -1.0;
                    }
                    else {
                        auto_allapot = atr_auto;
                        lepesnev = "auto";
                        akt_time += akt_dt;
                    }
                    // nincs break!
                case atr_normal:
                    // Gerjeszt�sek �t�ll�t�sa

                    time_and_change & change = akt_anal.times[step];
                    if (change.change_index >= 0) {
                        is_elfogad_elozo_elektromos = false;
                        //auto_ellenorzes = false; // auto transi steps
                        for (u32 xi = 0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++) {
                            excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                            if (akt_excit.is_el) {
                                texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                                texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                            }
                            else {
                                texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                                texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                            }
                        }
                    }

                    step++;
                    break;
            }
            //auto_allapot = atr_normal;

        }while(step<akt_anal.times.size());
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
    }
    delete[] powmap;
}

*/
//*

//***********************************************************************
void simulation::run_controlled(const PLString & FileName,uns anal_index){
//***********************************************************************
    masina mas;
    mas.del_all();
    mas.par_clear(0);
    mas.par_set_ac(false);
    mas.par_set_quad(false);
    mas.par_set_pot_map(0);
    mas.par_set_temp_map(1);
    mas.par_set_curr_map(2);
    mas.par_set_old_pot_map(3,5);
    mas.par_set_old_temp_map(4,6);
    mas.par_set_old_curr_map(7,8);
//    mas.par_set_start_UT(true); // �ll�tson kezd�h�m�rs�kletet
    mas.par_set_I_for_semi(false);
    mas.par_set_start_from_map(false);
    mas.par_set_el_th_Seebeck(!this->is_no_seebeck);
    mas.par_set_el_th_diss(false); // ez most nem elektro-termikus
    mas.par_set_th_el_Seebeck(!this->is_no_seebeck);
    mas.par_set_transi(true);
    mas.par_set_timeconst(false);
    mas.par_set_dt(nulla);
    mas.par_set_f(nulla);
    mas.par_set_t(nulla);
    mas.par_set_anal_step(1);
    mas.par_set_s(nulla);
    mas.par_set_sim(this);
    mas.par_set_tipus(FieldTherm);
    mas.par_set_anal_index(anal_index);
    mas.par_set_fim_txt(is_fim_txt);
    pmodel->clear_tseged();
    is_simple_anal_progress = true;

    is_transi_step_as_dc = true; // ezt majd a simulation::read �ll�tja be!

    uns step = 1;
    analysis & akt_anal = tanal[anal_index];
    tomb<change_time> & ctrl = akt_anal.ctrl;
    akt_anal.fill_times();

    dbl *powmap = nullptr;
    if ((mezo == FieldTherm || mezo == FieldElTherm) && pmap.is_exists()) {
        powmap = new dbl[pmodel->x_res*pmodel->y_res*pmodel->z_res];
    }

    if(mezo!=FieldElTherm){
        mas.par_set_quad(valostipus!=0);
        mas.par_set_tipus(mezo);
        mas.par_set_semi_init(true);
        mas.par_set_el_th_Seebeck(false);
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step - 1].time );
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(1);
      resetAnalLevel(2);
      setAnalStepDb(2,akt_anal.times.size()-1);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,0," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,mezo==FieldEl?"set V0":"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(akt_anal.times[step].time, powmap);
        mas.fill_step0_curr(4, powmap);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2);
        mas.backsubs(0,aramot);
        if(mezo==FieldEl)mas.get_u_i_matrix(1);
        else mas.get_T_matrix(1);
        mas.write_fim(FileName,2,true,4);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
        if(ConsoleText){
            printf("\r%4d. step                            ",1);
            printf("   actual time = %-13.11g",akt_anal.times[step].time);
            printf("\n");
        }

        // Gerjeszt�sek �t�ll�t�sa

        time_and_change & change = akt_anal.times[step];
        if(change.change_index>=0){
            for(u32 xi=0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++){
                excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                if(akt_excit.is_el){
                    texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                }
                else{
                    texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                }
            }
        }


        step=2; // a fim f�jl sz�ma
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step-1].time);
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(step);
        mas.par_set_semi_init(false);
        if(mezo==FieldEl)mas.par_set_I_for_semi(true); // f�lvezet� nemlinearit�s�hoz
        bool kell_nodered = true;
        do{
            mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step-1].time);
            mas.par_set_t(akt_anal.times[step].time);
                if(kell_nodered)mas.del_azon(); // csak a h�l�t t�rli
            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,step,"OK");
            uchannel_update();
                if (kell_nodered)mas.foglal_halot(0);
                if(kell_nodered)mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,mezo==FieldEl?"set V0":"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                if(mezo==FieldEl)mas.par_set_start_from_map(false); // a fill h�m. t�rk�pk�nt kezeli, de El. szim.-n�l nincs h�m
                if(kell_nodered)mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(akt_anal.times[step].time, powmap);
                mas.fill_step0_curr(4, powmap);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                if(kell_nodered)mas.nodered(is_always_strassen_mul);
                else mas.forwsubs(0);
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2);
                mas.backsubs(0,aramot);
                mas.swap_u();
                mas.swap_T();
                if(mezo==FieldEl)mas.get_u_i_matrix(1);
                else mas.get_T_matrix(1);
                mas.par_set_anal_step(step);
                mas.write_fim(FileName,2,true,4);
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;
            if(ConsoleText){
                printf("\r%4d. step                            ",step);
                printf("   actual time = %-13.11g",akt_anal.times[step].time);
                printf("\n");
            }

                // Gerjeszt�sek �t�ll�t�sa

                kell_nodered = !is_lin;
                time_and_change & change = akt_anal.times[step];
                if(change.change_index>=0){
                    kell_nodered = true;
                    for(u32 xi=0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++){
                        excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                        if(akt_excit.is_el){
                            texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                            texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                        }
                        else{
                            texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                            texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                        }
                    }
                }

            step++;
        }while(step<akt_anal.times.size());
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      setAnalStepDb(2,step-1);
      setAnalKeszStep(2,step-1);
    }
    else{

        // elektromos

        mas.par_set_quad((valostipus&1)!=0);
        mas.par_set_tipus(FieldEl);
        mas.par_set_semi_init(true);
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step - 1].time );
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(1);
      resetAnalLevel(2);
      setAnalStepDb(2,akt_anal.times.size()*2-2);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,1," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        mas.fill_step0_curr(4);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
        mas.backsubs(0,aramot);
        mas.get_u_i_matrix(1);
        if (auto_tra.is_V) // rendes tranziensn�l in-lin eset�n ne legyen
            mas.get_center_values(prev_U_map); // auto transi steps
        mas.write_fim(FileName,2,true,4);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
        if(ConsoleText){
            printf("\r%4d. step (el)                       ",1);
            printf("   actual time = %-13.11g",akt_anal.times[step].time);
            printf("\n");
        }

        // f�lvezet� elektromos tranziens�nek lecsenget�se

        for(uns semi_konv = 0; semi_konv < el_nonlin_subiter-1; semi_konv++){

            if(semi_konv==0){

                // termikus

                mas.del_azon();
                mas.par_set_quad((valostipus&2)!=0);
                mas.par_set_el_th_diss(true);
                mas.par_set_tipus(FieldTherm);

              resetAnalLevel(1);
              setAnalStepDb(1,3);
              resetAnalLevel(0);
              proti=PT_NoderedElott;
              setAnalStepDb(0,5);
              setAnalKeszStep(2,2," ");
              uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(akt_anal.times[step].time, powmap);
                mas.fill_step0_curr(4, powmap);
              setAnalKeszStep(0,5);
              setAnalKeszStep(1,1);
              proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul);
              resetAnalLevel(0);
              proti=PT_NoderedUtan;
              setAnalStepDb(0,3);
              setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot);
                mas.get_T_matrix(1);
                //mas.write_fim(FileName,2,true,4);
              setAnalKeszStep(0,3,"OK");
              setAnalKeszStep(1,3);
              proti=PT_Egyenletes;
                if(ConsoleText){
                    printf("\r%4d. step (th)                       ",1);
                    printf("   actual time = %-13.11g",akt_anal.times[step].time);
                    printf("\n");
                }
            }

                // elektromos

                mas.par_set_quad((valostipus&1)!=0);
                mas.par_set_tipus(FieldEl);
                mas.del_azon();
            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,1," ");
            uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                mas.fill_step0_curr(4);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
                mas.nodered(is_always_strassen_mul);
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot); // itt a swap_UT kimarad, rem�lhet�leg t�nyleg nem kell p�tolni semmivel
                mas.swap_u(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                mas.get_u_i_matrix(1);
                if (auto_tra.is_V && semi_konv == el_nonlin_subiter - 2)
                    mas.get_center_values(prev_U_map); // auto transi steps
                mas.par_set_anal_step(0);
                if(semi_konv==el_nonlin_subiter-2)
                    mas.write_fim(FileName,2,true,4);
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;
            if(ConsoleText){
                printf("\r%4d. step (el) %2d. substep           ",0,semi_konv+1);
                printf("   actual time = %-13.11g",akt_anal.times[step].time);
                printf("\n");
            }
        }

        // termikus

        mas.del_azon();
        mas.par_set_quad((valostipus&2)!=0);
        mas.par_set_el_th_diss(true);
        mas.par_set_tipus(FieldTherm);

      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,2," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(akt_anal.times[step].time, powmap);
        mas.fill_step0_curr(4, powmap);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
        mas.backsubs(0,aramot);
        mas.get_T_matrix(1);
        if (auto_tra.is_T)
            mas.get_center_values(prev_T_map); // auto transi steps
        mas.write_fim(FileName,2,true,4);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
        if(ConsoleText){
            printf("\r%4d. step (th)                       ",1);
            printf("   actual time = %-13.11g",akt_anal.times[step].time);
            printf("\n");
        }

        // Gerjeszt�sek �t�ll�t�sa

        time_and_change & change = akt_anal.times[step];
        if(change.change_index>=0){
            for(u32 xi=0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++){
                excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                if(akt_excit.is_el){
                    texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                }
                else{
                    texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                }
            }
        }

        // be�ll�t�s

        step=2; // a fim f�jl sz�ma
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step-1].time);
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(step);
        mas.par_set_semi_init(false);
        mas.par_set_I_for_semi(true); // f�lvezet� nemlinearit�s�hoz, termikusn�l is kell a disszip�ci�hoz a gcsd sz�m�t�shoz

        uns ossz_plusz_lepes=0,sikeres_plusz_lepes=0;
        bool restore=false;
        dbl akt_time=akt_anal.times[step].time, akt_dt=akt_anal.times[step].time - akt_anal.times[step-1].time;

        // auto transi steps
        // - a prev_U_map / prev_T_map m�r inicializ�lva kell legyen, akt_U_map / akt_T_map inicializ�latlan

        autotr_state auto_allapot = atr_normal;
        dbl sikeres_t = akt_anal.times[1].time; // Az els� l�p�s id�pontja, ezt sikeresnek tekintj�k
        dbl auto_dt = -1.0; // ha nem auto m�dban vagyunk, akkor ezt a -1 jelzi
        bool auto_ellenorzes = change.change_index < 0, is_el_hiba_volt = true;
        bool his_ellenorzes = true;
        uns V_plus_steps = 0, T_plus_steps = 0, H_plus_steps = 0, hanyadik_koztes_lepes = 0;
        const char * lepesnev = "normal";
        char s_auto[21]="";
        mas.swap_Ge();

        do{
            mas.push();
            if(auto_allapot == atr_normal){
                mas.par_set_dt( akt_dt = akt_anal.times[step].time - akt_anal.times[step-1].time );
                mas.par_set_t( akt_time = akt_anal.times[step].time );
                V_plus_steps = T_plus_steps = 0;
                hanyadik_koztes_lepes = 0;
            }
            else{
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_time);
                hanyadik_koztes_lepes++;
            }
            
            // elektromos

//printf("\nel start\n");
            bool is_abs_needed_V = false; // auto transi steps
            for(uns semi_konv = 0; semi_konv < el_nonlin_subiter; semi_konv++){
                    mas.par_set_quad((valostipus&1)!=0);
                    mas.par_set_tipus(FieldEl);
                    mas.del_azon();
                resetAnalLevel(1);
                setAnalStepDb(1,3);
                resetAnalLevel(0);
                proti=PT_NoderedElott;
                setAnalStepDb(0,5);
                setAnalStepDb(2,akt_anal.times.size()*2-2+ossz_plusz_lepes*2);
                setAnalKeszStep(2,step*2-1+ossz_plusz_lepes*2," ");
                uchannel_update();
                    mas.foglal_halot(0);
                    mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                    mas.par_set_start_from_map(true); // fesz �s h�m is
                    mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                    mas.fill_step0_adm(3);
                    mas.fill_step0_curr(4);
                setAnalKeszStep(0,5);
                setAnalKeszStep(1,1);
                proti=PT_Nodered;
//printf("\nel nodered %g\n", akt_dt);
                    mas.nodered(is_always_strassen_mul);
//printf("\nel kesz\n");
                resetAnalLevel(0);
                proti=PT_NoderedUtan;
                setAnalStepDb(0,3);
                setAnalKeszStep(1,2," ");
                    mas.backsubs(0,aramot); // itt a swap_UT kimarad, rem�lhet�leg t�nyleg nem kell p�tolni semmivel
                    mas.swap_u(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                    mas.get_u_i_matrix(1);
//printf("\nbacksubs... kesz\n");
                    if (auto_tra.is_V && semi_konv == el_nonlin_subiter - 1)
                        mas.get_center_values(akt_U_map); // auto transi steps
                    mas.par_set_anal_step(step+sikeres_plusz_lepes);
                    if (semi_konv == el_nonlin_subiter - 1) {
                        if (auto_ellenorzes)
                            is_abs_needed_V = is_auto_back_step_needed_U(akt_dt); // auto transi steps
                            //is_abs_needed_V = auto_tra.is_V && akt_dt > auto_tra.V_min_dt && mas.is_Ge_error(0.01);
                        if (!is_abs_needed_V && ( !auto_tra.is_no_plus_step_data || akt_time == akt_anal.times[step].time))
                            mas.write_fim(FileName, 2, true, 4, restore);
                    }
                setAnalKeszStep(0,3,"OK");
                setAnalKeszStep(1,3);
                proti=PT_Egyenletes;
                if(ConsoleText){
                    if(el_nonlin_subiter>1)printf("\r%4d. step (el) %2d. substep           ",step+ossz_plusz_lepes,semi_konv+1);
                    else printf("\r%4d. step (el)  %-20s ", step + ossz_plusz_lepes, lepesnev);
                    printf("   actual time = %-13.11g",akt_time);
                    printf("\n");
                }
            }
//printf("\nV kiir kesz\n");
            if (auto_ellenorzes && is_abs_needed_V) { // auto transi steps
                restore = true;
                is_el_hiba_volt = true;
                akt_time = sikeres_t;
//
//                auto_dt = akt_dt = auto_allapot == atr_auto ? akt_dt/2 : auto_tra.V_min_dt; // Az elektromosn�l �gyis mindig ide dob vissza, nem �rdemes felezgetni
//                if (akt_dt < auto_tra.V_min_dt)
//                    auto_dt = akt_dt = auto_tra.V_min_dt;
//
                auto_dt = akt_dt = auto_tra.V_min_dt;
                //auto_dt = akt_dt /= 2; // nem kell ellen�rizni, hogy a megengedettn�l kisebb-e, mert az is_auto_back_step_needed_T �gy is megfogja
                //while(akt_dt>auto_tra.V_min_dt)auto_dt = akt_dt /= 2;
                akt_time += akt_dt;
                // az utols� �rv�nyes h�m�rs�kleti adatot is backupolni kell (e n�lk�l nem t�rt�nik meg, mert a 
                // termikus tranziens l�p�st m�r nem sz�m�tjuk, ami ezt h�vn� a write_fim-en kereszt�l)
                backup_restore_file(FileName + "thermal/probe.trz", false);
                backup_restore_file(FileName + "thermal/szimpla.trz", false);
                //printf("\n**** Sikertelen U dt=%g ****\n", akt_dt);
                auto_allapot = atr_auto;
                lepesnev = "auto back V";
// Mivel az elektromos t�r reziszt�v, felesleges �jrasz�molni az elektromos l�p�st. A kor�bbi verzi�ban ez volt, az �j verzi�ban a komment ut�ni r�szek lesznek.
//                ossz_plusz_lepes++;
//                mas.pop();
//                continue;
//
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_time);
                hanyadik_koztes_lepes++;
            }

            // termikus

termikus_ujra: // ha nem akarjuk �jrafuttatni az elektromos r�szt visszal�p�sn�l

//printf("\ntermikus start\n");
                mas.del_azon();
                mas.par_set_quad((valostipus&2)!=0);
                mas.par_set_el_th_diss(true);
                mas.par_set_tipus(FieldTherm);

            resetAnalLevel(1);
            setAnalStepDb(1,3);
            resetAnalLevel(0);
            proti=PT_NoderedElott;
            setAnalStepDb(0,5);
            setAnalKeszStep(2,step*2+ossz_plusz_lepes*2," ");
            uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(akt_anal.times[step].time, powmap);
                mas.fill_step0_curr(4, powmap);
            setAnalKeszStep(0,5);
            setAnalKeszStep(1,1);
            proti=PT_Nodered;
//printf("\nth nodered %g\n", akt_dt);
                mas.nodered(is_always_strassen_mul); // mi�rt lass�, ha dt = 1e-10 ?
//printf("\nth kesz\n");
            resetAnalLevel(0);
            proti=PT_NoderedUtan;
            setAnalStepDb(0,3);
            setAnalKeszStep(1,2," ");
                mas.backsubs(0,aramot);
                mas.swap_T(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                mas.get_T_matrix(1);
//printf("\nbacksubs/swap/getT kesz\n");
                if (auto_tra.is_T)
                    mas.get_center_values(akt_T_map); // auto transi steps
                mas.par_set_anal_step(step+sikeres_plusz_lepes);
                bool is_abs_needed_T = false; // auto transi steps
                if (auto_ellenorzes)
                    is_abs_needed_T = is_auto_back_step_needed_T(akt_dt); // auto transi steps
                if (!is_abs_needed_T && (!auto_tra.is_no_plus_step_data || akt_time == akt_anal.times[step].time))
                    mas.write_fim(FileName,2,true,4,restore);
            setAnalKeszStep(0,3,"OK");
            setAnalKeszStep(1,3);
            proti=PT_Egyenletes;
//printf("\naztan\n");

            if(ConsoleText){
                printf("\r%4d. step (th)  %-20s ", step+ossz_plusz_lepes, lepesnev);
                printf("   actual time = %-13.11g",akt_time);
                printf("\n");
            }
            if (auto_ellenorzes && is_abs_needed_T) { // auto transi steps
                restore = true;
                is_el_hiba_volt = false;
                akt_time = sikeres_t;
//
//                auto_dt = akt_dt = auto_allapot == atr_auto ? akt_dt / 2 : auto_tra.T_min_dt;
//                if (akt_dt < auto_tra.T_min_dt)
//                    auto_dt = akt_dt = auto_tra.T_min_dt;
//                
                auto_dt = akt_dt = auto_tra.T_min_dt;
                //auto_dt = akt_dt /= 2; // nem kell ellen�rizni, hogy a megengedettn�l kisebb-e, mert az is_auto_back_step_needed_T �gy is megfogja
                akt_time += akt_dt;
                ossz_plusz_lepes++;
                //printf("\n**** Sikertelen T dt=%g ****\n", akt_dt);
                lepesnev = "auto back T";
                auto_allapot = atr_auto;
// Mivel az elektromos t�r reziszt�v, felesleges �jrasz�molni az elektromos l�p�st. A kor�bbi verzi�ban ez volt, az �j verzi�ban a komment ut�ni r�szek lesznek.
//                mas.pop();
//                continue;
//
                mas.swap_back_T();
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_time);
                hanyadik_koztes_lepes++;
                goto termikus_ujra;
            }

            // Hiszter�zis ellen�rz�se

            dbl min_ido_arany=mas.get_min_ido_arany();
            if(his_ellenorzes && min_ido_arany<egy){ // kell visszal�p�s
                lepesnev = "his back";
                auto_allapot = atr_hisz_back;
                akt_time = sikeres_t;
                akt_dt *= min_ido_arany;
                akt_time += akt_dt;
                ossz_plusz_lepes++;
                //printf("\n**** Sikertelen hisz dt=%g ****\n", akt_dt);
// Mivel az elektromos t�r reziszt�v, felesleges �jrasz�molni az elektromos l�p�st. A kor�bbi verzi�ban ez volt, az �j verzi�ban a komment ut�ni r�szek lesznek.
//                mas.pop();
//                continue;
//
                mas.swap_back_T();
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_time);
                hanyadik_koztes_lepes++;
                goto termikus_ujra;
            }

            // j�v�hagyjuk az aktu�lis l�p�st

            //printf("\n**** Sikeres lepes ****\n");
            sikeres_t = akt_time;       // auto transi steps
            if(akt_time >= akt_anal.times[step].time)
                V_plus_steps = T_plus_steps = H_plus_steps = 0;
            prev_T_map.swap(akt_T_map); // auto transi steps
            prev_U_map.swap(akt_U_map); // auto transi steps
            mas.swap_Ge();
            restore = false;
            auto_ellenorzes = true;     // auto transi steps, ha sikeres volt a l�p�s, akkor a k�v. l�p�sben m�r ellen�rizhet�nk
                                        // (Ha hiszter�zis miatt volt visszal�p�s auto_ellenorzes=false mellett, 
                                        // akkor k�zben nem volt auto ellen�rz�s, de ez pont �gy kell.)
            his_ellenorzes = true;
            cuns db = pmodel->x_res * pmodel->y_res * pmodel->z_res;
            for( uns it = 0; it < db; it++ )
                pmodel->tseged[it].update_his();

            switch (auto_allapot) {     // auto transi steps
                case atr_auto: // vissza kellett l�pni vagy l�peget�nk dt dupl�z�ssal
                    lepesnev = "auto";
                    sikeres_plusz_lepes++;
                    ossz_plusz_lepes++;
//                    auto_dt = akt_dt = hanyadik_koztes_lepes == 1 
//                        ? akt_anal.times[step].time - akt_anal.times[step - 1].time
//                        : akt_dt * 8;
                    auto_dt = akt_dt *= 8;
                    if (akt_time + akt_dt >= akt_anal.times[step].time) {
                        auto_allapot = atr_auto_fixpont;
                        lepesnev = "auto fixpont";
                        akt_dt = akt_anal.times[step].time - akt_time;
                        akt_time = akt_anal.times[step].time;
                    }
                    else {
                        akt_time += akt_dt;
                    }
                    //printf("\n**** auto dt=%g ****\n", akt_dt);
                    if (is_el_hiba_volt) {
                        if (auto_tra.V_max_plus_steps > 0) {
                            V_plus_steps++;
                            sprintf(s_auto, "auto (V: +%u)", V_plus_steps);
                            lepesnev = s_auto;
                            if (V_plus_steps >= auto_tra.V_max_plus_steps) {
                                //auto_allapot = atr_normal;
                                auto_allapot = atr_auto_fixpont;
                                lepesnev = "auto fixpont V";
                                akt_dt = akt_anal.times[step].time - akt_time;
                                akt_time = akt_anal.times[step].time;
                                //V_plus_steps = T_plus_steps = 0;
                                auto_ellenorzes = false;
                                //sikeres_t = akt_time - akt_dt;
                            }
                        }
                    }
                    else {
                        if (auto_tra.T_max_plus_steps > 0) {
                            T_plus_steps++;
                            sprintf(s_auto, "auto (T: +%u)", T_plus_steps);
                            lepesnev = s_auto;
                            if (T_plus_steps >= auto_tra.T_max_plus_steps) {
                                //auto_allapot = atr_normal;
                                auto_allapot = atr_auto_fixpont;
                                lepesnev = "auto fixpont T";
                                akt_dt = akt_anal.times[step].time - akt_time;
                                akt_time = akt_anal.times[step].time;
                                //V_plus_steps = T_plus_steps = 0;
                                auto_ellenorzes = false;
                                //sikeres_t = akt_time - akt_dt;
                            }
                        }
                    }
                    continue;
                    break;
                case atr_hisz_back: // vissza kellett l�pni
                    //printf("\n**** hisz back ****\n");
                    lepesnev = "his back";
                    sikeres_plusz_lepes++;
                    ossz_plusz_lepes++;
                    H_plus_steps++;
//printf("\nH_plus_steps: %u\n", H_plus_steps);
                    sprintf(s_auto, "his back (+%u)", H_plus_steps);
                    lepesnev = s_auto;
                    if (H_plus_steps > 20) {
                        lepesnev = "his back jump 10";
                        auto_ellenorzes = his_ellenorzes = false;
                        H_plus_steps = 0;
                        dbl dholtart = 10.0 * (akt_time - akt_anal.times[step - 1].time) / (akt_anal.times[step].time - akt_anal.times[step - 1].time);
//printf("\nakt_anal.times[step].time: %g, akt_time: %g, akt_anal.times[step - 1].time: %g\n", akt_anal.times[step].time, akt_time, akt_anal.times[step - 1].time);
                        uns uholtart = (uns)dholtart;
//printf("\ndholtart: %g, uholtart: %u\n", dholtart, uholtart);
                        if (uholtart < 9) {
                            lepesnev = "his back jump";
                            dbl uj_time = (uholtart + 1) * 0.1 * (akt_anal.times[step].time - akt_anal.times[step - 1].time);
//printf("\nuj_time: %g\n", uj_time);
                            akt_dt = akt_anal.times[step - 1].time + uj_time - akt_time;
//printf("\nakt_dt: %g\n", akt_dt);
                            akt_time = akt_anal.times[step - 1].time + uj_time;
//printf("\nakt_time: %g\n", akt_time);
                            continue;
                        }
                    }
                    if (auto_dt < nulla) {  // nem auto transi �zemm�dban vagyunk
                        akt_dt = akt_anal.times[step].time - akt_time;
                        akt_time = akt_anal.times[step].time;
                        auto_allapot = atr_normal;
                        //lepesnev = "normal";
                        sprintf(s_auto, "normal (H: +%u)", H_plus_steps);
                        lepesnev = s_auto;
                    }
                    else {              // auto transi �zemm�dban vagyunk
                        akt_dt = auto_dt;
                        if (akt_time + akt_dt >= akt_anal.times[step].time) {
                            auto_allapot = atr_auto_fixpont;
                            //lepesnev = "auto fixpont";
                            sprintf(s_auto, "auto fix (H: +%u)", H_plus_steps);
                            lepesnev = s_auto;
                            akt_dt = akt_anal.times[step].time - akt_time;
                            akt_time = akt_anal.times[step].time;
                        }
                        else {
                            auto_allapot = atr_auto;
                            //lepesnev = "auto";
                            sprintf(s_auto, "auto (H: +%u)", H_plus_steps);
                            lepesnev = s_auto;
                            akt_time += akt_dt;
                        }
                    }
                    continue;
                    break;
                case atr_auto_fixpont:
                    //printf("\n**** fixpont ****\n");
                    akt_dt = auto_dt;
                    if (akt_dt >= akt_anal.times[step].time - akt_anal.times[step - 1].time) {
                        auto_allapot = atr_normal;
                        lepesnev = "normal";
                        auto_dt = -1.0;
                    }
                    else {
                        auto_allapot = atr_auto;
                        lepesnev = "auto";
                        akt_time += akt_dt;
                    }
                    // nincs break!
                case atr_normal:
                    //printf("\n**** fixpont vagy normal ****\n");
                    // Gerjeszt�sek �t�ll�t�sa

                    time_and_change & change = akt_anal.times[step];
                    if (change.change_index >= 0) {
                        //auto_ellenorzes = false; // auto transi steps
                        for (u32 xi = 0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++) {
                            excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                            if (akt_excit.is_el) {
                                texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                                texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                            }
                            else {
                                texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                                texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                            }
                        }
                    }

                    step++;
                    break;
            }
            //auto_allapot = atr_normal;

        }while(step<akt_anal.times.size());
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
    }
    delete[] powmap;
}

//*/

/*
//***********************************************************************
void simulation::run_controlled(const PLString & FileName, uns anal_index) {
//***********************************************************************
    masina mas;
    mas.del_all();
    mas.par_clear(0);
    mas.par_set_ac(false);
    mas.par_set_quad(false);
    mas.par_set_pot_map(0);
    mas.par_set_temp_map(1);
    mas.par_set_curr_map(2);
    mas.par_set_old_pot_map(3, 5);
    mas.par_set_old_temp_map(4, 6);
    mas.par_set_old_curr_map(7, 8);
    //    mas.par_set_start_UT(true); // �ll�tson kezd�h�m�rs�kletet
    mas.par_set_I_for_semi(false);
    mas.par_set_start_from_map(false);
    mas.par_set_el_th_Seebeck(!this->is_no_seebeck);
    mas.par_set_el_th_diss(false); // ez most nem elektro-termikus
    mas.par_set_th_el_Seebeck(!this->is_no_seebeck);
    mas.par_set_transi(true);
    mas.par_set_timeconst(false);
    mas.par_set_dt(nulla);
    mas.par_set_f(nulla);
    mas.par_set_t(nulla);
    mas.par_set_anal_step(1);
    mas.par_set_s(nulla);
    mas.par_set_sim(this);
    mas.par_set_tipus(FieldTherm);
    mas.par_set_anal_index(anal_index);
    mas.par_set_fim_txt(is_fim_txt);
    pmodel->clear_tseged();
    is_simple_anal_progress = true;


    uns step = 1;
    analysis & akt_anal = tanal[anal_index];
    tomb<change_time> & ctrl = akt_anal.ctrl;
    akt_anal.fill_times();

    dbl *powmap = nullptr;
    if ((mezo == FieldTherm || mezo == FieldElTherm) && pmap.is_exists()) {
        powmap = new dbl[pmodel->x_res*pmodel->y_res*pmodel->z_res];
    }

    if (mezo != FieldElTherm) {
        mas.par_set_quad(valostipus != 0);
        mas.par_set_tipus(mezo);
        mas.par_set_semi_init(true);
        mas.par_set_el_th_Seebeck(false);
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step - 1].time);
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(1);
        resetAnalLevel(2);
        setAnalStepDb(2, akt_anal.times.size() - 1);
        resetAnalLevel(1);
        setAnalStepDb(1, 3);
        resetAnalLevel(0);
        proti = PT_NoderedElott;
        setAnalStepDb(0, 5);
        setAnalKeszStep(2, 0, " ");
        uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2, mezo == FieldEl ? "set V0" : "set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(akt_anal.times[step].time, powmap);
        mas.fill_step0_curr(4, powmap);
        setAnalKeszStep(0, 5);
        setAnalKeszStep(1, 1);
        proti = PT_Nodered;
        mas.nodered(is_always_strassen_mul);
        resetAnalLevel(0);
        proti = PT_NoderedUtan;
        setAnalStepDb(0, 3);
        setAnalKeszStep(1, 2);
        mas.backsubs(0, aramot);
        if (mezo == FieldEl)mas.get_u_i_matrix(1);
        else mas.get_T_matrix(1);
        mas.write_fim(FileName, 2, true, 4);
        setAnalKeszStep(0, 3, "OK");
        setAnalKeszStep(1, 3);
        proti = PT_Egyenletes;
        if (ConsoleText) {
            printf("\r%4d. step                            ", 1);
            printf("   actual time = %-12g", akt_anal.times[step].time);
            printf("\n");
        }

        // Gerjeszt�sek �t�ll�t�sa

        time_and_change & change = akt_anal.times[step];
        if (change.change_index >= 0) {
            for (u32 xi = 0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++) {
                excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                if (akt_excit.is_el) {
                    texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                }
                else {
                    texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                }
            }
        }


        step = 2; // a fim f�jl sz�ma
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step - 1].time);
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(step);
        mas.par_set_semi_init(false);
        if (mezo == FieldEl)mas.par_set_I_for_semi(true); // f�lvezet� nemlinearit�s�hoz
        bool kell_nodered = true;
        do {
            mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step - 1].time);
            mas.par_set_t(akt_anal.times[step].time);
            if (kell_nodered)mas.del_azon(); // csak a h�l�t t�rli
            resetAnalLevel(1);
            setAnalStepDb(1, 3);
            resetAnalLevel(0);
            proti = PT_NoderedElott;
            setAnalStepDb(0, 5);
            setAnalKeszStep(2, step, "OK");
            uchannel_update();
            if (kell_nodered)mas.foglal_halot(0);
            if (kell_nodered)mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
            mas.par_set_start_from_map(true); // fesz �s h�m is
            mas.kezdo_UT(2, mezo == FieldEl ? "set V0" : "set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
            if (mezo == FieldEl)mas.par_set_start_from_map(false); // a fill h�m. t�rk�pk�nt kezeli, de El. szim.-n�l nincs h�m
            if (kell_nodered)mas.fill_step0_adm(3);
            if (powmap != nullptr)
                pmap.get_map(akt_anal.times[step].time, powmap);
            mas.fill_step0_curr(4, powmap);
            setAnalKeszStep(0, 5);
            setAnalKeszStep(1, 1);
            proti = PT_Nodered;
            if (kell_nodered)mas.nodered(is_always_strassen_mul);
            else mas.forwsubs(0);
            resetAnalLevel(0);
            proti = PT_NoderedUtan;
            setAnalStepDb(0, 3);
            setAnalKeszStep(1, 2);
            mas.backsubs(0, aramot);
            mas.swap_u();
            mas.swap_T();
            if (mezo == FieldEl)mas.get_u_i_matrix(1);
            else mas.get_T_matrix(1);
            mas.par_set_anal_step(step);
            mas.write_fim(FileName, 2, true, 4);
            setAnalKeszStep(0, 3, "OK");
            setAnalKeszStep(1, 3);
            proti = PT_Egyenletes;
            if (ConsoleText) {
                printf("\r%4d. step                            ", step);
                printf("   actual time = %-12g", akt_anal.times[step].time);
                printf("\n");
            }

            // Gerjeszt�sek �t�ll�t�sa

            kell_nodered = !is_lin;
            time_and_change & change = akt_anal.times[step];
            if (change.change_index >= 0) {
                kell_nodered = true;
                for (u32 xi = 0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++) {
                    excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                    if (akt_excit.is_el) {
                        texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                        texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                    }
                    else {
                        texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                        texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                    }
                }
            }

            step++;
        } while (step<akt_anal.times.size());
        setAnalKeszStep(0, 3, "OK");
        setAnalKeszStep(1, 3);
        setAnalStepDb(2, step - 1);
        setAnalKeszStep(2, step - 1);
    }
    else {

        // elektromos

        mas.par_set_quad((valostipus & 1) != 0);
        mas.par_set_tipus(FieldEl);
        mas.par_set_semi_init(true);
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step - 1].time);
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(1);
        resetAnalLevel(2);
        setAnalStepDb(2, akt_anal.times.size() * 2 - 2);
        resetAnalLevel(1);
        setAnalStepDb(1, 3);
        resetAnalLevel(0);
        proti = PT_NoderedElott;
        setAnalStepDb(0, 5);
        setAnalKeszStep(2, 1, " ");
        uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2, "set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        mas.fill_step0_curr(4);
        setAnalKeszStep(0, 5);
        setAnalKeszStep(1, 1);
        proti = PT_Nodered;
        mas.nodered(is_always_strassen_mul);
        resetAnalLevel(0);
        proti = PT_NoderedUtan;
        setAnalStepDb(0, 3);
        setAnalKeszStep(1, 2, " ");
        mas.backsubs(0, aramot);
        mas.get_u_i_matrix(1);
        mas.write_fim(FileName, 2, true, 4);
        setAnalKeszStep(0, 3, "OK");
        setAnalKeszStep(1, 3);
        proti = PT_Egyenletes;
        if (ConsoleText) {
            printf("\r%4d. step (el)                       ", 1);
            printf("   actual time = %-12g", akt_anal.times[step].time);
            printf("\n");
        }

        // f�lvezet� elektromos tranziens�nek lecsenget�se

        for (uns semi_konv = 0; semi_konv < el_nonlin_subiter - 1; semi_konv++) {

            if (semi_konv == 0) {

                // termikus

                mas.del_azon();
                mas.par_set_quad((valostipus & 2) != 0);
                mas.par_set_el_th_diss(true);
                mas.par_set_tipus(FieldTherm);

                resetAnalLevel(1);
                setAnalStepDb(1, 3);
                resetAnalLevel(0);
                proti = PT_NoderedElott;
                setAnalStepDb(0, 5);
                setAnalKeszStep(2, 2, " ");
                uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.kezdo_UT(2, "set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                if (powmap != nullptr)
                    pmap.get_map(akt_anal.times[step].time, powmap);
                mas.fill_step0_curr(4, powmap);
                setAnalKeszStep(0, 5);
                setAnalKeszStep(1, 1);
                proti = PT_Nodered;
                mas.nodered(is_always_strassen_mul);
                resetAnalLevel(0);
                proti = PT_NoderedUtan;
                setAnalStepDb(0, 3);
                setAnalKeszStep(1, 2, " ");
                mas.backsubs(0, aramot);
                mas.get_T_matrix(1);
                //mas.write_fim(FileName,2,true,4);
                setAnalKeszStep(0, 3, "OK");
                setAnalKeszStep(1, 3);
                proti = PT_Egyenletes;
                if (ConsoleText) {
                    printf("\r%4d. step (th)                       ", 1);
                    printf("   actual time = %-12g", akt_anal.times[step].time);
                    printf("\n");
                }
            }

            // elektromos

            mas.par_set_quad((valostipus & 1) != 0);
            mas.par_set_tipus(FieldEl);
            mas.del_azon();
            resetAnalLevel(1);
            setAnalStepDb(1, 3);
            resetAnalLevel(0);
            proti = PT_NoderedElott;
            setAnalStepDb(0, 5);
            setAnalKeszStep(2, 1, " ");
            uchannel_update();
            mas.foglal_halot(0);
            mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
            mas.par_set_start_from_map(true); // fesz �s h�m is
            mas.kezdo_UT(2, "set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
            mas.fill_step0_adm(3);
            mas.fill_step0_curr(4);
            setAnalKeszStep(0, 5);
            setAnalKeszStep(1, 1);
            proti = PT_Nodered;
            mas.nodered(is_always_strassen_mul);
            resetAnalLevel(0);
            proti = PT_NoderedUtan;
            setAnalStepDb(0, 3);
            setAnalKeszStep(1, 2, " ");
            mas.backsubs(0, aramot); // itt a swap_UT kimarad, rem�lhet�leg t�nyleg nem kell p�tolni semmivel
            mas.swap_u(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
            mas.get_u_i_matrix(1);
            mas.par_set_anal_step(0);
            if (semi_konv == el_nonlin_subiter - 2)
                mas.write_fim(FileName, 2, true, 4);
            setAnalKeszStep(0, 3, "OK");
            setAnalKeszStep(1, 3);
            proti = PT_Egyenletes;
            if (ConsoleText) {
                printf("\r%4d. step (el) %2d. substep           ", 0, semi_konv + 1);
                printf("   actual time = %-12g", akt_anal.times[step].time);
                printf("\n");
            }
        }

        // termikus

        mas.del_azon();
        mas.par_set_quad((valostipus & 2) != 0);
        mas.par_set_el_th_diss(true);
        mas.par_set_tipus(FieldTherm);

        resetAnalLevel(1);
        setAnalStepDb(1, 3);
        resetAnalLevel(0);
        proti = PT_NoderedElott;
        setAnalStepDb(0, 5);
        setAnalKeszStep(2, 2, " ");
        uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2, "set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        if (powmap != nullptr)
            pmap.get_map(akt_anal.times[step].time, powmap);
        mas.fill_step0_curr(4, powmap);
        setAnalKeszStep(0, 5);
        setAnalKeszStep(1, 1);
        proti = PT_Nodered;
        mas.nodered(is_always_strassen_mul);
        resetAnalLevel(0);
        proti = PT_NoderedUtan;
        setAnalStepDb(0, 3);
        setAnalKeszStep(1, 2, " ");
        mas.backsubs(0, aramot);
        mas.get_T_matrix(1);
        mas.write_fim(FileName, 2, true, 4);
        setAnalKeszStep(0, 3, "OK");
        setAnalKeszStep(1, 3);
        proti = PT_Egyenletes;
        if (ConsoleText) {
            printf("\r%4d. step (th)                       ", 1);
            printf("   actual time = %-12g", akt_anal.times[step].time);
            printf("\n");
        }

        // Gerjeszt�sek �t�ll�t�sa

        time_and_change & change = akt_anal.times[step];
        if (change.change_index >= 0) {
            for (u32 xi = 0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++) {
                excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                if (akt_excit.is_el) {
                    texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                }
                else {
                    texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                    texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                }
            }
        }

        // be�ll�t�s

        step = 2; // a fim f�jl sz�ma
        mas.par_set_dt(akt_anal.times[step].time - akt_anal.times[step - 1].time);
        mas.par_set_t(akt_anal.times[step].time);
        mas.par_set_anal_step(step);
        mas.par_set_semi_init(false);
        mas.par_set_I_for_semi(true); // f�lvezet� nemlinearit�s�hoz, termikusn�l is kell a disszip�ci�hoz a gcsd sz�m�t�shoz

        uns ossz_plusz_lepes = 0, sikeres_plusz_lepes = 0;
        bool normal_tovabblepes = true;
        bool restore = false;
        dbl akt_time = akt_anal.times[step].time, akt_dt = akt_anal.times[step].time - akt_anal.times[step - 1].time;
        do {
            mas.push();
            if (normal_tovabblepes) {
                mas.par_set_dt(akt_dt = akt_anal.times[step].time - akt_anal.times[step - 1].time);
                mas.par_set_t(akt_time = akt_anal.times[step].time);
            }
            else {
                mas.par_set_dt(akt_dt);
                mas.par_set_t(akt_time);
            }

            // elektromos

            for (uns semi_konv = 0; semi_konv < el_nonlin_subiter; semi_konv++) {
                mas.par_set_quad((valostipus & 1) != 0);
                mas.par_set_tipus(FieldEl);
                mas.del_azon();
                resetAnalLevel(1);
                setAnalStepDb(1, 3);
                resetAnalLevel(0);
                proti = PT_NoderedElott;
                setAnalStepDb(0, 5);
                setAnalStepDb(2, akt_anal.times.size() * 2 - 2 + ossz_plusz_lepes * 2);
                setAnalKeszStep(2, step * 2 - 1 + ossz_plusz_lepes * 2, " ");
                uchannel_update();
                mas.foglal_halot(0);
                mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
                mas.par_set_start_from_map(true); // fesz �s h�m is
                mas.kezdo_UT(2, "set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
                mas.fill_step0_adm(3);
                mas.fill_step0_curr(4);
                setAnalKeszStep(0, 5);
                setAnalKeszStep(1, 1);
                proti = PT_Nodered;
                mas.nodered(is_always_strassen_mul);
                resetAnalLevel(0);
                proti = PT_NoderedUtan;
                setAnalStepDb(0, 3);
                setAnalKeszStep(1, 2, " ");
                mas.backsubs(0, aramot); // itt a swap_UT kimarad, rem�lhet�leg t�nyleg nem kell p�tolni semmivel
                mas.swap_u(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
                mas.get_u_i_matrix(1);
                mas.par_set_anal_step(step + sikeres_plusz_lepes);
                if (semi_konv == el_nonlin_subiter - 1)mas.write_fim(FileName, 2, true, 4, restore);
                setAnalKeszStep(0, 3, "OK");
                setAnalKeszStep(1, 3);
                proti = PT_Egyenletes;
                if (ConsoleText) {
                    printf("\r%4d. step (el) %2d. substep           ", step + ossz_plusz_lepes, semi_konv + 1);
                    printf("   actual time = %-12g", akt_time);
                    printf("\n");
                }
            }

            // termikus

            mas.del_azon();
            mas.par_set_quad((valostipus & 2) != 0);
            mas.par_set_el_th_diss(true);
            mas.par_set_tipus(FieldTherm);

            resetAnalLevel(1);
            setAnalStepDb(1, 3);
            resetAnalLevel(0);
            proti = PT_NoderedElott;
            setAnalStepDb(0, 5);
            setAnalKeszStep(2, step * 2 + ossz_plusz_lepes * 2, " ");
            uchannel_update();
            mas.foglal_halot(0);
            mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
            mas.par_set_start_from_map(true); // fesz �s h�m is
            mas.kezdo_UT(2, "set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
            mas.fill_step0_adm(3);
            if (powmap != nullptr)
                pmap.get_map(akt_anal.times[step].time, powmap);
            mas.fill_step0_curr(4, powmap);
            setAnalKeszStep(0, 5);
            setAnalKeszStep(1, 1);
            proti = PT_Nodered;
            mas.nodered(is_always_strassen_mul);
            resetAnalLevel(0);
            proti = PT_NoderedUtan;
            setAnalStepDb(0, 3);
            setAnalKeszStep(1, 2, " ");
            mas.backsubs(0, aramot);
            mas.swap_T(); // eddig haszn�lhatta a kor�bban kisz�molt �rt�keket
            mas.get_T_matrix(1);
            mas.par_set_anal_step(step + sikeres_plusz_lepes);
            mas.write_fim(FileName, 2, true, 4, restore);
            setAnalKeszStep(0, 3, "OK");
            setAnalKeszStep(1, 3);
            proti = PT_Egyenletes;

            if (ConsoleText) {
                printf("\r%4d. step (th)                       ", step + ossz_plusz_lepes);
                printf("   actual time = %-12g", akt_time);
                printf("\n");
            }

            // Hiszter�zis ellen�rz�se

            restore = true;
            dbl min_ido_arany = mas.get_min_ido_arany();
            if (min_ido_arany<egy) { // kell visszal�p�s
                normal_tovabblepes = false;
                akt_time -= akt_dt;
                akt_dt *= min_ido_arany;
                akt_time += akt_dt;
                ossz_plusz_lepes++;
                mas.pop();
                continue;
            }
            else { // j�v�hagyjuk az aktu�lis l�p�st
                cuns db = pmodel->x_res * pmodel->y_res * pmodel->z_res;
                for (uns it = 0; it < db; it++)
                    pmodel->tseged[it].update_his();
                if (akt_time < akt_anal.times[step].time) { // vissza kellett l�pni
                    sikeres_plusz_lepes++;
                    restore = false;
                    akt_dt = akt_anal.times[step].time - akt_time;
                    akt_time = akt_anal.times[step].time;
                    continue;
                }
                else {
                    normal_tovabblepes = true;
                }
            }

            // Gerjeszt�sek �t�ll�t�sa

            time_and_change & change = akt_anal.times[step];
            if (change.change_index >= 0) {
                for (u32 xi = 0; xi<akt_anal.ctrl[change.change_index].excit.size(); xi++) {
                    excitation_2 & akt_excit = akt_anal.ctrl[change.change_index].excit[xi];
                    if (akt_excit.is_el) {
                        texcitE[akt_excit.color_index].tipus = akt_excit.tipus;
                        texcitE[akt_excit.color_index].ertek = akt_excit.ertek;
                    }
                    else {
                        texcitT[akt_excit.color_index].tipus = akt_excit.tipus;
                        texcitT[akt_excit.color_index].ertek = akt_excit.ertek;
                    }
                }
            }

            step++;
            restore = false;
        } while (step<akt_anal.times.size());
        setAnalKeszStep(0, 3, "OK");
        setAnalKeszStep(1, 3);
    }
    delete[] powmap;
}
*/

//***********************************************************************
simulation::Logitherm_interface::~Logitherm_interface(){
//***********************************************************************
    delete mass;
}


//***********************************************************************
void simulation::Logitherm_interface::first_step(uns anal_index, dbl from,const double * p_map,double * T_map){
//***********************************************************************
    delete mass;
    mass = new masina;
    simulation & sim=*simm;
    masina & mas = *mass;
    bool ct = ConsoleText;
    ConsoleText = false;
    mas.del_all();
    mas.par_clear(0);
    mas.par_set_ac(false);
    mas.par_set_pot_map(0);
    mas.par_set_temp_map(1);
    mas.par_set_curr_map(2);
    mas.par_set_old_pot_map(3,5);
    mas.par_set_old_temp_map(4,6);
    mas.par_set_old_curr_map(7,8);
//    mas.par_set_start_UT(true); // �ll�tson kezd�h�m�rs�kletet
    mas.par_set_I_for_semi(false);
    mas.par_set_start_from_map(false);
    mas.par_set_el_th_diss(false); // ez most nem elektro-termikus
    mas.par_set_th_el_Seebeck(!sim.is_no_seebeck);
    mas.par_set_transi(true);
    mas.par_set_timeconst(false);
    mas.par_set_f(nulla);
    mas.par_set_s(nulla);
    mas.par_set_sim(&sim);
    mas.par_set_tipus(FieldTherm);
    mas.par_set_anal_index(anal_index);
    mas.par_set_fim_txt(sim.is_fim_txt);
    mas.par_set_quad(sim.valostipus!=0);
    mas.par_set_semi_init(true);
    mas.par_set_el_th_Seebeck(false);
    mas.par_set_dt(from);
    mas.par_set_t(from);
    mas.par_set_anal_step(1);
    sim.uchannel_update();
    mas.foglal_halot(0);
    mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
    mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
    mas.fill_step0_adm(3);
    mas.fill_step0_curr(4,p_map);
    mas.nodered(sim.is_always_strassen_mul);
    mas.backsubs(0,sim.aramot);
    mas.get_T_matrix(1);
    mas.logitherm_get_top_T_map(T_map);
        //PLString path="d:\\Webs\\temp\\";
        //mas.write_fim(path,2,true,4);
    aktstep=1; // a fim f�jl sz�ma
    akt_t = dt = from;
    mas.par_set_semi_init(false);
    ConsoleText = ct;
}


//***********************************************************************
void simulation::Logitherm_interface::next_step(const double * p_map,double * T_map){
//***********************************************************************
    masina & mas = *mass;
    simulation & sim=*simm;
    bool ct = ConsoleText;
    ConsoleText = false;
    bool kell_nodered = true;
    kell_nodered = !sim.is_lin;
    akt_t += dt;
    mas.par_set_t(akt_t);
    mas.par_set_anal_step(++aktstep);
    if (kell_nodered)mas.del_azon(); // csak a h�l�t t�rli
    sim.uchannel_update();
    if (kell_nodered)mas.foglal_halot(0);
    if (kell_nodered)mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
//        printf("%u ",aktstep);
    mas.par_set_start_from_map(true);
    mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
    if (kell_nodered)mas.fill_step0_adm(3);
    mas.fill_step0_curr(4,p_map);
    if (kell_nodered)mas.nodered(sim.is_always_strassen_mul);
    else mas.forwsubs(0);
    mas.backsubs(0,sim.aramot);
    mas.swap_u();
    mas.swap_T();
    mas.get_T_matrix(1);
    mas.logitherm_get_top_T_map(T_map);
        //PLString path="d:\\Webs\\temp\\";
        //mas.write_fim(path,2,true,4);
    ConsoleText = ct;
}

//***********************************************************************
void simulation::Logitherm_interface::next_step(const double * p_map,double * T_map, double timestep){
//***********************************************************************
    // en szartam ide -----------------------------
    dt = timestep;
    // --------------------------------------------
    
    masina & mas = *mass;
    simulation & sim=*simm;
    bool ct = ConsoleText;
    ConsoleText = false;
    bool kell_nodered = true;
    kell_nodered = !sim.is_lin;
    akt_t += dt;
    mas.par_set_t(akt_t);
    mas.par_set_anal_step(++aktstep);
    if (kell_nodered)mas.del_azon(); // csak a h�l�t t�rli
    sim.uchannel_update();
    if (kell_nodered)mas.foglal_halot(0);
    if (kell_nodered)mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
//        printf("%u ",aktstep);
    mas.par_set_start_from_map(true);
    mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
    if (kell_nodered)mas.fill_step0_adm(3);
    mas.fill_step0_curr(4,p_map);
    if (kell_nodered)mas.nodered(sim.is_always_strassen_mul);
    else mas.forwsubs(0);
    mas.backsubs(0,sim.aramot);
    mas.swap_u();
    mas.swap_T();
    mas.get_T_matrix(1);
    mas.logitherm_get_top_T_map(T_map);
        //PLString path="d:\\Webs\\temp\\";
        //mas.write_fim(path,2,true,4);
    ConsoleText = ct;
}

//***********************************************************************
void simulation::run_ac(const PLString & FileName,uns anal_index,dbl f,uns tipus,uns analstep,bool is_timeconst,dcomplex s,uns stepdb){
//***********************************************************************
    masina mas;
    mas.del_all();
    mas.par_clear(0);
    mas.par_set_ac(true);
    mas.par_set_timeconst(is_timeconst);
    mas.par_set_s(s); // timeconsthoz kell
    mas.par_set_f(f);
    mas.par_set_quad(false);
    mas.par_set_pot_map(0);
    mas.par_set_temp_map(1);
    mas.par_set_curr_map(2);
//    mas.par_set_start_UT(true); // �ll�tson kezd�h�m�rs�kletet
    mas.par_set_I_for_semi(false);
    mas.par_set_start_from_map(false);
    mas.par_set_el_th_Seebeck(false);
    mas.par_set_el_th_diss(false); // ez most nem elektro-termikus
    mas.par_set_th_el_Seebeck(false);
    mas.par_set_transi(false);
    mas.par_set_dt(nulla);
    mas.par_set_sim(this);
    mas.par_set_t(nulla);
    mas.par_set_anal_step(analstep);
    mas.par_set_tipus(FieldTherm);
    mas.par_set_fim_txt(is_fim_txt);
    mas.par_set_anal_index(anal_index);
    is_simple_anal_progress = false;
    if(mezo!=FieldElTherm){
        mas.par_set_quad(valostipus!=0);
        mas.par_set_tipus(mezo);
      resetAnalLevel(2);
      setAnalStepDb(2,stepdb);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,analstep-1," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,mezo==FieldEl?"set V0":"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        mas.fill_step0_curr(4);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2);
//      mas.forwsubs();
        mas.backsubs(0,aramot);
        if(mezo==FieldEl)mas.get_u_i_matrix(1);
        else mas.get_T_matrix(1);
        mas.write_fim(FileName,2,true,tipus);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
      setAnalKeszStep(2,analstep);
    }
    else{
        mas.par_set_quad((valostipus&1)!=0);
        mas.par_set_tipus(FieldEl);
      resetAnalLevel(2);
      setAnalStepDb(2,2*stepdb);
      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,analstep*2-2," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set V0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        mas.fill_step0_curr(4);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
//      mas.forwsubs();
        mas.backsubs(0,aramot);
        mas.get_u_i_matrix(1);
        mas.write_fim(FileName,2,true,tipus);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;

        mas.del_azon();
        mas.par_set_quad((valostipus&2)!=0);
        mas.par_set_el_th_diss(true);
        mas.par_set_tipus(FieldTherm);

      resetAnalLevel(1);
      setAnalStepDb(1,3);
      resetAnalLevel(0);
      proti=PT_NoderedElott;
      setAnalStepDb(0,5);
      setAnalKeszStep(2,analstep*2-1," ");
      uchannel_update();
        mas.foglal_halot(0);
        mas.foglal_matrixot(1); // a h�l� nem foglalja ezeket
        mas.kezdo_UT(2,"set T0");        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
        mas.fill_step0_adm(3);
        mas.fill_step0_curr(4);
      setAnalKeszStep(0,5);
      setAnalKeszStep(1,1);
      proti=PT_Nodered;
        mas.nodered(is_always_strassen_mul);
      resetAnalLevel(0);
      proti=PT_NoderedUtan;
      setAnalStepDb(0,3);
      setAnalKeszStep(1,2," ");
//      mas.forwsubs();
        mas.backsubs(0,aramot);
        mas.get_T_matrix(1);
        mas.write_fim(FileName,2,true,tipus);
      setAnalKeszStep(0,3,"OK");
      setAnalKeszStep(1,3);
      proti=PT_Egyenletes;
      setAnalKeszStep(2,analstep*2);
    }
}


//***********************************************************************
void simulation::run_bode(const PLString & FileName,uns anal_index,dbl from, dbl to, dbl step){
//***********************************************************************
    cd ddf = pow(10.0,1.0/step);
    dbl akt_f = from;
    uns analstep=1;
    is_simple_anal_progress = true;

    do{ akt_f*=ddf; analstep++; }while(akt_f/ddf<=to);
    cuns stepdb=analstep-1;
    
    akt_f = from;
    analstep=1;
    do{
        printf("\r%4u. f= %-60f\n",analstep,akt_f);
        run_ac(FileName,anal_index,akt_f,2,analstep,false,dcomplex(0.0),stepdb);
        akt_f*=ddf;
        analstep++;
    }while(akt_f/ddf<=to);
}


//***********************************************************************
void simulation::run_timeconst(const PLString & FileName,uns anal_index,dbl from, dbl to, dbl step){
//***********************************************************************
    cd ddf = pow(10.0,1.0/step);
    dbl akt_f = from;
    uns analstep=1;
    is_simple_anal_progress = true;

    do{ akt_f*=ddf; analstep++; }while(akt_f/ddf<=to);
    cuns stepdb=analstep-1;
    
    akt_f = from;
    analstep=1;
    do{
        printf("\r%4u. f= %12.6f Hz, tau= %12.6f s%25s\n",analstep,akt_f,1.0/(2.0*M_PI*akt_f),"");
        cd angle=M_PI+1.5*log(10.0)/step;
        cd omega=2*M_PI*akt_f; 
        dcomplex s=dcomplex(omega*cos(angle),omega*sin(angle));
        run_ac(FileName,anal_index,akt_f,3,analstep,true,s,stepdb);
        akt_f*=ddf;
        analstep++;
    }while(akt_f/ddf<=to);
}

// WINFOSON
#ifdef _WIN32
    #include <direct.h>

//UNIX/POSIX
#else
    #include <sys/stat.h>
    
    //hogy ne kelljen a kodon belul vacakolni a mode_t-vel
    static int mkdir(const char *path)
    {
        return mkdir(path, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    }

#endif

FILE * fpsok = NULL;
PLString subpath;
//***********************************************************************
void simulation::run(const PLString & path, bool is_uj_model){
//***********************************************************************
    VSUN_CPU_Thread=cpu_threads;
    resetAnalLevel(3);
    setAnalStepDb(3,tanal.size());
    mkdir((path+"results").c_str());
    PLString nev_eleje=path+"results/"+pmodel->name;
    subpath = nev_eleje;
    mkdir(nev_eleje.c_str());
    PLString sok=nev_eleje+"/sokprobe.txt";
    fpsok = fopen(sok.c_str(),is_uj_model ? "wt" : "at");
    fprintf(fpsok,"%s ",name.c_str());
    nev_eleje+="/"+name;
    mkdir(nev_eleje.c_str());
    for(uns i=0;i<tanal.size();i++){
        if(ConsoleText){
            printf("\n* model:         %s\n* simulation:    %s\n",pmodel->name.c_str(),name.c_str());
            switch(tanal[i].tipus){
            case AnalNDC:   printf("* analysis:      %s  max-itar=%u, rel-err=%g%%, semi-I0=%gA\n",tanal[i].nev.c_str(),tanal[i].ndc_maxiter,tanal[i].relhiba*100.0,tanal[i].ndc_I0*pmodel->A_semi_full); break;
                default:        printf("* analysis:      %s\n",tanal[i].nev.c_str());
            }
        }
        PLString nev;
        //nev=path+"results/"+pmodel->name;
        //mkdir(nev.c_str());
        //nev+="/"+name;
        //mkdir(nev.c_str());
        nev = nev_eleje;
        nev+="/"+tanal[i].nev;
        mkdir(nev.c_str());
        nev+="/";
        switch(tanal[i].tipus){
//            case AnalDC: run_dc(path+"results/"+pmodel->name+'.'+name+'.'+tanal[i].nev+'.',i); break;
//            case AnalNDC:  run_ndc(path+"results/"+pmodel->name+'.'+name+'.'+tanal[i].nev+'.',i); break;
            case AnalDC:        run_dc(nev,i); break;
            case AnalNDC:       run_ndc(nev,i); break;
            case AnalAC:        run_ac(nev,i,tanal[i].from,1,1,false,dcomplex(nulla),1); break;
            case AnalLinTran:
            case AnalLogTran:   run_transi(nev,i,tanal[i].tipus,tanal[i].from,tanal[i].to,tanal[i].step); break;
            case AnalBode:      run_bode(nev,i,tanal[i].from,tanal[i].to,tanal[i].step); break;
            case AnalIdo:       run_timeconst(nev,i,tanal[i].from,tanal[i].to,tanal[i].step); break;
            case AnalCtrl:      run_controlled(nev,i); break;
            default: throw hiba("simulation::run","Unsupported analysis type");
        }
        logprint("\n");
        setAnalKeszStep(3,i+1);
    }
    fclose(fpsok);
}

/*
//***********************************************************************
double Gyuri_uchannel::nusselt_calculation(enum Channel::geometry shape, double dynamic_diameter, double Reynolds_number, double Prandtl_number, double Length){
//***********************************************************************
    double calculated_result;
    if (Length <= 0) return(0);
    switch (shape) {
        case Channel::rect:
            calculated_result = 
                5.14 + (0.065*dynamic_diameter / Length*Reynolds_number*Prandtl_number)
                / (1 + 0.04*pow(dynamic_diameter / Length*Reynolds_number*Prandtl_number, 2.0 / 3.0));
            break;
        case Channel::circ:
            calculated_result = 
                3.66 + (0.065*dynamic_diameter / Length*Reynolds_number*Prandtl_number) 
                / (1 + 0.04*pow(dynamic_diameter / Length*Reynolds_number*Prandtl_number, 2.0 / 3.0));
            break;
        case Channel::trap:
            calculated_result = 
                5.14 + (0.065*dynamic_diameter / Length*Reynolds_number*Prandtl_number) 
                / (1 + 0.04*pow(dynamic_diameter / Length*Reynolds_number*Prandtl_number, 2.0 / 3.0));
            break;
    }
    return calculated_result;
}
*/

//***********************************************************************
double Gyuri_uchannel::nusselt_calculation(struct Channel * chx, double calculated_length){
//***********************************************************************
    double developed_Nu_value;
    double calculated_result;

    if (chx->length <= 0) return 0;

    if (calculated_length <= 0) { calculated_length = chx->length; }

    if (chx->laminar_or_turbulent == Channel::lam)
    {
        switch (chx->cross_section_type) {

        case Channel::rect:
            switch ((int)round(chx->side_ratio)) {
                case 1: {developed_Nu_value = 2.98; break;}
                case 2: {developed_Nu_value = 3.39; break;}
                case 3: {developed_Nu_value = 3.96; break;}
                case 4: {developed_Nu_value = 4.44; break;}
                case 5: {developed_Nu_value = 4.79; break;}
                case 6: {developed_Nu_value = 5.14; break;}
                case 7: {developed_Nu_value = 5.37; break;}
                case 8: {developed_Nu_value = 5.60; break;}
                default: {developed_Nu_value = 7.54; break;}
            }

            calculated_result = developed_Nu_value 
                + (0.065*chx->hydraulic_diameter / calculated_length*chx->Reynolds_number*chx->Prandtl_number) 
                / (1 + 0.04*pow(chx->hydraulic_diameter / calculated_length*chx->Reynolds_number*chx->Prandtl_number, 0.6666666667));

            break;

        case Channel::circ:
            calculated_result = 3.66 
                + (0.065*chx->hydraulic_diameter / calculated_length*chx->Reynolds_number*chx->Prandtl_number) 
                / (1 + 0.04*pow(chx->hydraulic_diameter / calculated_length*chx->Reynolds_number*chx->Prandtl_number, 0.6666666667));
            break;

        case Channel::trap:
            // printf("----- %lf \t %lf \t %lf \t %lf  -----  ",chx->hydraulic_diameter, Reynolds_number, Prandtl_number, Length);
            calculated_result = 5.14 
                + (0.065*chx->hydraulic_diameter / calculated_length*chx->Reynolds_number*chx->Prandtl_number) 
                / (1 + 0.04*pow(chx->hydraulic_diameter / calculated_length*chx->Reynolds_number*chx->Prandtl_number, 0.6666666667));
            // printf("%lf\n",calculated_result);
            break;
        }
    }
    if (chx->laminar_or_turbulent == Channel::turb){
        calculated_result = 0.023*pow(chx->Reynolds_number, 0.8)*pow(chx->Prandtl_number, (1.0 / 3.0));
    }
    if (chx->laminar_or_turbulent == Channel::mixed){
        calculated_result = ((chx->friction_factor / 8)*(chx->Reynolds_number - 1000)*chx->Prandtl_number) 
            / (1 + 12.7*pow(chx->friction_factor / 8, 1.0 / 2.0)*(pow(chx->Prandtl_number, 2.0 / 3.0) - 1));
    }

    return calculated_result;
}


//***********************************************************************
int Gyuri_uchannel::Local_Parameters_calculation(struct Channel * chx, uns segment_number){
//***********************************************************************
    //Only the Nusselt number depends of the length of the segment (thus the local heat transfer coeff. also)
    uns i;
    double segment_length = chx->length / segment_number;
    double a = 0, b = 0;

    chx->segments = new Segment[segment_number + 1];

    for (i = 1; i <= segment_number; i++){
        a = nusselt_calculation(chx, i*segment_length);
        if (i > 1)
            b = nusselt_calculation(chx, (i - 1)*segment_length);
        chx->segments[i].Nusselt_number = a * i - b * (i - 1);
        chx->segments[i].local_heat_transfer_coeff = chx->heat_conductivity*chx->segments[i].Nusselt_number / chx->hydraulic_diameter;
    }
    return 0;
}


//***********************************************************************
int Gyuri_uchannel::Rladder_stage_calculation(struct Channel * chx, uns segment_number, const uchannel &ucha) {
//***********************************************************************
    uns i;

    chx->stages = new Rladder[segment_number + 1];

    for (i = 1; i <= segment_number; i++) {

        chx->stages[i].resistance_along = 1 / (chx->mass_flow_rate*chx->specific_heat);
        chx->stages[i].resistance_cross = 1 / (chx->specific_heat*chx->mass_flow_rate
            * (1 - exp(-chx->segments[i].local_heat_transfer_coeff*chx->length
            / ucha.n*chx->perimeter / (chx->specific_heat*chx->mass_flow_rate))));
        chx->stages[i].Twall = ucha.is_auto_wall_temp ? ucha.psim->tucha_perem[ucha.szinindex][i-1].getT(ucha.psim->ambiT) : ucha.fixed_wall_temp; // !!!

        if (i == 1){
            chx->stages[i].Tout = chx->fluid_inlet_temperature + (chx->stages[i].Twall
                - chx->fluid_inlet_temperature)*(1 - exp(-chx->segments[i].local_heat_transfer_coeff*chx->length
                / ucha.n*chx->perimeter / (chx->specific_heat*chx->mass_flow_rate)));
            chx->stages[i].heat_exchange = 1 / chx->stages[i].resistance_cross * (chx->stages[i].Twall - chx->fluid_inlet_temperature);
        }
        else{
            chx->stages[i].Tout = chx->stages[i - 1].Tout + (chx->stages[i].Twall
                - chx->stages[i - 1].Tout)*(1 - exp(-chx->segments[i].local_heat_transfer_coeff*chx->length
                / ucha.n*chx->perimeter / (chx->specific_heat*chx->mass_flow_rate)));
            chx->stages[i].heat_exchange = 1 / chx->stages[i].resistance_cross * (chx->stages[i].Twall - chx->stages[i - 1].Tout);
        }
    }
    return(0);
}


//***********************************************************************
double Gyuri_uchannel::Rladder_to_Spice(struct Channel * chx, FILE * spice_file, FILE * fu, uns ch_number, uns wished_segment_number, uchannel &ucha) {
//***********************************************************************
    double summa_heat_exchange = 0;

    // I would like to add date and time tag to the file in the future...

    if (fu != NULL)fprintf(fu, "\nThe values of the R ladder are the followings: \n");

    for (uns j = 1;j <= wished_segment_number;j++) {
        if (j == 1)
        {
            if (spice_file != NULL)fprintf(spice_file, "### Compact model of microscale integrated channel structures ###\n");
            if (spice_file != NULL)fprintf(spice_file, "V0 %i 0 %.8lf\n", 4 * j, chx->fluid_inlet_temperature);
            if (fu != NULL)fprintf(fu, "The heat resistance in one segment along the channel is: %f \n", chx->stages[j].resistance_along);
            if (fu != NULL)fprintf(fu, "The heat resistance through the channel walls in the segments and the exit temperatures are the following:\n");
        }
        if (fu != NULL)fprintf(fu, "\tRth_cross= %.4f\tTwall = %3.0f C\tx[m]= %.4f\tTout= %.2f \tdQ= %.4f\n", chx->stages[j].resistance_cross, chx->stages[j].Twall, j*chx->length / wished_segment_number, chx->stages[j].Tout, chx->stages[j].heat_exchange);

        ucha.psim->tucha_perem[ucha.szinindex][j - 1].set_uchannel_segment(j == 1 ? chx->fluid_inlet_temperature : chx->stages[j - 1].Tout, chx->stages[j].resistance_cross);
        summa_heat_exchange = summa_heat_exchange + chx->stages[j].heat_exchange;

        if (spice_file != NULL)fprintf(spice_file, "R%i %i %i %.8lf\n", j, 4 * j, 4 * j + 1, chx->stages[j].resistance_cross);
        if (spice_file != NULL)fprintf(spice_file, "V%i %i %i DC %.8lf\n", 4 * j + 1, 4 * j + 1, 4 * j + 2, chx->stages[j].Twall);
        if (spice_file != NULL)fprintf(spice_file, "V%i %i %i DC 0\n", 4 * j + 2, 4 * j + 2, 0);
        if (spice_file != NULL)fprintf(spice_file, "H%i %i %i V%i %.8lf\n\n", j, 4 * j, 4 * (j + 1), 4 * j + 2, chx->stages[j].resistance_along);
        //  if (spice_file != NULL)fprintf(spice_file,"E%i %i %i %i %i 1\n\n",j,4*(j+1),4*j+3,4*j,0);
    }

    if (fu != NULL)fprintf(fu, "\t\t\t\t\t\tTotal dQ=%.4f\n", summa_heat_exchange);
    if (spice_file != NULL)fprintf(spice_file, "\n\n.op\n.end");

    return summa_heat_exchange;
}


//***********************************************************************
int Gyuri_uchannel::Thermal_prop_calculation(struct Channel *chx, double global_temp, FILE *fu) {
//***********************************************************************
    double e1, e3, e5;

    chx->average_Nusselt_number = nusselt_calculation(chx, 0);
    if (fu != NULL)fprintf(fu, "The AVERAGE Nusselt Number is %4.4f \n", chx->average_Nusselt_number);
    chx->global_heat_transfer_coefficient = chx->heat_conductivity*chx->average_Nusselt_number / chx->hydraulic_diameter;
    if (fu != NULL)fprintf(fu, "The global heat transfer coefficient is %5.4f \n", chx->global_heat_transfer_coefficient);
    chx->global_thermal_conductivity = chx->mass_flow_rate*chx->specific_heat*(1 - exp(-chx->global_heat_transfer_coefficient*chx->perimeter*chx->length / (chx->mass_flow_rate*chx->specific_heat)));
    if (fu != NULL)fprintf(fu, "The calculated global thermal resistance is %f \n", 1 / chx->global_thermal_conductivity);
    if (fu != NULL)fprintf(fu, "The inlet temperature of the fluid [C] > %.1f \n", chx->fluid_inlet_temperature);
    if (fu != NULL)fprintf(fu, "The NTU is %f [W]\t", chx->specific_heat*chx->mass_flow_rate*(global_temp - chx->fluid_inlet_temperature));
    if (fu != NULL)fprintf(fu, "and the Rth_NTU %f [K/W]\n", 1 / (chx->specific_heat*chx->mass_flow_rate));
    if (fu != NULL)fprintf(fu, "The output temperature of the fluid at the and of the channel if Twall is constant %.1f [C] > %.4f \n", global_temp, global_temp - (global_temp - chx->fluid_inlet_temperature) * 1 / exp(chx->global_heat_transfer_coefficient*chx->perimeter*chx->length / (chx->mass_flow_rate*chx->specific_heat)));
    if (fu != NULL)fprintf(fu, "The length of the channel is %.4e \n", chx->length);

    if (chx->laminar_or_turbulent == Channel::lam) {
        if (fu != NULL)fprintf(fu, "The length of the thermal entry region [laminar flow] is %.4e\n", 0.05*chx->Reynolds_number*chx->Prandtl_number*chx->hydraulic_diameter);
    }
    else {
        if (fu != NULL)fprintf(fu, "The length of the thermal entry region [turbulent flow] is %.4e\n", 10 * chx->hydraulic_diameter);
    }

    if (fu != NULL)fprintf(fu, "The Characteristic length parameters (where Tfluid is equal to the x%% of (Tw-Ti) AND Tw is supposed to be constant) are:\n");

    e1 = chx->mass_flow_rate*chx->specific_heat / chx->global_heat_transfer_coefficient / chx->perimeter;
    e3 = log((60.0 - chx->fluid_inlet_temperature)*exp(-3.0) / ((60.0 - chx->fluid_inlet_temperature))) / (-chx->global_heat_transfer_coefficient*chx->perimeter / (chx->mass_flow_rate*chx->specific_heat));
    e5 = log((60.0 - chx->fluid_inlet_temperature)*exp(-5.0) / ((60.0 - chx->fluid_inlet_temperature))) / (-chx->global_heat_transfer_coefficient*chx->perimeter / (chx->mass_flow_rate*chx->specific_heat));

    if (chx->length >= e1) if (fu != NULL)fprintf(fu, "The 1/e^1 (36.7%%) Lchar is %.4e - The 63.3%% of the heat transfer occurs in the first %.1f %% of the channel\n", e1, e1 / chx->length * 100);
    else if (fu != NULL)fprintf(fu, "The 1/e^1 (36.7%%) Lchar is %.4e \n", e1);

    if (chx->length >= e3) if (fu != NULL)fprintf(fu, "The 1/e^3 (5%%) Lchar is %.4e - The 95%% of the heat transfer occurs in the first %.1f %% of the channel\n", e3, e3 / chx->length * 100);
    else if (fu != NULL)fprintf(fu, "The 1/e^3 (5%%) Lchar is %.4e \n", e3);

    if (chx->length >= e5) if (fu != NULL)fprintf(fu, "The 1/e^5 (1%%) Lchar is %.4e - The 99%% of the heat transfer occurs in the first %.1f %% of the channel\n", e5, e5 / chx->length * 100);
    else if (fu != NULL)fprintf(fu, "The 1/e^5 (1%%) Lchar is %.4e \n", e5);

    return 0;
}


//***********************************************************************
int Gyuri_uchannel::Hydrodynamic_calculation(struct Channel * chx, double vol_flow_rate) {
//***********************************************************************
    const double gravity_value = 9.81;
    //if (chx->width_bottom == chx->width_top) { chx->cross_section_type = Channel::rect; }
    //else { chx->cross_section_type = Channel::trap; }

    switch (chx->cross_section_type){
    case Channel::rect:
        chx->perimeter = 2 * chx->width_top + 2 * chx->height;
        chx->area = chx->width_top*chx->height;
        chx->side_ratio = chx->width_top / chx->height;
        break;

    case Channel::circ:
        chx->perimeter = chx->width_top*3.14159265359;
        chx->area = pow(chx->width_top / 2, 2)*3.14159265359;
        chx->side_ratio = 1; //Not used in the case of circular cross sectional shape, but not wanted a variable to leave undefinied
        break;

    case Channel::trap:
        chx->perimeter = chx->width_bottom + chx->width_top + 2 * pow(pow(chx->height, 2) + pow(chx->width_top - chx->width_bottom, 2), 0.5);
        chx->area = (chx->width_bottom + chx->width_top) / 2 * chx->height;
        chx->side_ratio = chx->width_top / chx->height;
        break;

    }

    chx->hydraulic_diameter = 4 * chx->area / chx->perimeter;
    chx->Prandtl_number = chx->dynamic_viscosity*chx->specific_heat / chx->heat_conductivity;

    // In Reynolds number calculation [m/s] must use instead of volumetric flow rate!!!!
    // The original ucooler structure consists of 48 channels, but in this function only one selected channel is under investigation
    // The other channels are supposed to have the same parameters like the selected one (in the estimation phase)!
    // [l/h] /3600 results [l/sec] /1000 results [m^3/sec] /cross_sectional_area results [m/sec]

    chx->volumetric_flow_rate = vol_flow_rate; // !!! a kor�bbi verzi�ban = volumetric_flow_rate / 3600 / 1000; volt
    chx->velocity_of_flow = chx->volumetric_flow_rate / chx->area;
    chx->mass_flow_rate = chx->density*chx->volumetric_flow_rate;
    chx->Reynolds_number = chx->density * chx->velocity_of_flow * chx->hydraulic_diameter / chx->dynamic_viscosity;
    if (chx->Reynolds_number<2400) {
        chx->laminar_or_turbulent = Channel::lam;
        if (chx->cross_section_type == Channel::circ) {
            chx->friction_factor = 64.0 / chx->Reynolds_number;
            chx->press_drop = 64.0 / 32.0 * chx->dynamic_viscosity*chx->length*pow(chx->perimeter, 2.0) / pow(chx->area, 3.0)*chx->volumetric_flow_rate;
        }
        if (chx->cross_section_type == Channel::trap || chx->cross_section_type == Channel::rect) {
            switch ((int)round(chx->side_ratio)) {
            case 1: {chx->friction_factor = 56.92 / chx->Reynolds_number; chx->press_drop = 56.92 / 32 * chx->dynamic_viscosity*chx->length*pow(chx->perimeter, 2) / pow(chx->area, 3)*chx->volumetric_flow_rate; /*chx->press_drop_dV=56.92/32*chx->dynamic_viscosity*chx->length*pow(chx->perimeter,2)/pow(chx->area,3); */ break;}
            case 2: {chx->friction_factor = 62.20 / chx->Reynolds_number; chx->press_drop = 62.20 / 32 * chx->dynamic_viscosity*chx->length*pow(chx->perimeter, 2) / pow(chx->area, 3)*chx->volumetric_flow_rate; /*chx->press_drop_dV=62.20/32*chx->dynamic_viscosity*chx->length*pow(chx->perimeter,2)/pow(chx->area,3); */ break;}
            case 3: {chx->friction_factor = 68.36 / chx->Reynolds_number; chx->press_drop = 68.36 / 32 * chx->dynamic_viscosity*chx->length*pow(chx->perimeter, 2) / pow(chx->area, 3)*chx->volumetric_flow_rate; /*chx->press_drop_dV=68.36/32*chx->dynamic_viscosity*chx->length*pow(chx->perimeter,2)/pow(chx->area,3); */ break;}
            case 4: {chx->friction_factor = 72.92 / chx->Reynolds_number; chx->press_drop = 72.92 / 32 * chx->dynamic_viscosity*chx->length*pow(chx->perimeter, 2) / pow(chx->area, 3)*chx->volumetric_flow_rate; /*chx->press_drop_dV=72.92/32*chx->dynamic_viscosity*chx->length*pow(chx->perimeter,2)/pow(chx->area,3); */ break;}
            case 5: {chx->friction_factor = 75.86 / chx->Reynolds_number; chx->press_drop = 75.86 / 32 * chx->dynamic_viscosity*chx->length*pow(chx->perimeter, 2) / pow(chx->area, 3)*chx->volumetric_flow_rate; /*chx->press_drop_dV=75.86/32*chx->dynamic_viscosity*chx->length*pow(chx->perimeter,2)/pow(chx->area,3); */ break;}
            case 6: {chx->friction_factor = 78.80 / chx->Reynolds_number; chx->press_drop = 78.80 / 32 * chx->dynamic_viscosity*chx->length*pow(chx->perimeter, 2) / pow(chx->area, 3)*chx->volumetric_flow_rate; /*chx->press_drop_dV=78.80/32*chx->dynamic_viscosity*chx->length*pow(chx->perimeter,2)/pow(chx->area,3); */ break;}
            case 7: {chx->friction_factor = 80.56 / chx->Reynolds_number; chx->press_drop = 80.56 / 32 * chx->dynamic_viscosity*chx->length*pow(chx->perimeter, 2) / pow(chx->area, 3)*chx->volumetric_flow_rate; /*chx->press_drop_dV=80.56/32*chx->dynamic_viscosity*chx->length*pow(chx->perimeter,2)/pow(chx->area,3); */ break;}
            case 8: {chx->friction_factor = 82.32 / chx->Reynolds_number; chx->press_drop = 82.32 / 32 * chx->dynamic_viscosity*chx->length*pow(chx->perimeter, 2) / pow(chx->area, 3)*chx->volumetric_flow_rate; /*chx->press_drop_dV=82.32/32*chx->dynamic_viscosity*chx->length*pow(chx->perimeter,2)/pow(chx->area,3); */ break;}
            default: {chx->friction_factor = 96.00 / chx->Reynolds_number; chx->press_drop = 96.00 / 32 * chx->dynamic_viscosity*chx->length*pow(chx->perimeter, 2) / pow(chx->area, 3)*chx->volumetric_flow_rate; /*chx->press_drop_dV=96.00/32*chx->dynamic_viscosity*chx->length*pow(chx->perimeter,2)/pow(chx->area,3); */ break; }
            }
        }
    }

    if (chx->Reynolds_number>4000) {
        // From Colebrook equation of friction force S.E.Haaland create a non implicit version which gives results within 2% uncertainty.
        //chx->friction_factor=pow(1/(-1.8*log10(6.9/chx->Reynolds_number+pow(chx->roughness/chx->hydraulic_diameter/3.7,1.11))),2);
        //chx->press_drop=chx->friction_factor*chx->length/chx->hydraulic_diameter*chx->density/2*pow(chx->volumetric_flow_rate,2)/pow(chx->area,2);

        // FROM MAPLE DERIVATION
        chx->laminar_or_turbulent = Channel::turb;
        chx->friction_factor = 0.3086419754e0 * pow(log(0.1725000000e1 / chx->density * chx->dynamic_viscosity * chx->perimeter / chx->volumetric_flow_rate + 0.5023535735e-1 * pow(chx->roughness * chx->perimeter / chx->area, 0.111e1)), -0.2e1) * pow(log(0.10e2), 0.2e1);
        chx->press_drop = 0.3858024692e-1 * pow(log(0.1725000000e1 / chx->density * chx->dynamic_viscosity * chx->perimeter / chx->volumetric_flow_rate + 0.5023535735e-1 * pow(chx->roughness * chx->perimeter / chx->area, 0.111e1)), -0.2e1) * pow(log(0.10e2), 0.2e1) * chx->length * chx->density * chx->volumetric_flow_rate * chx->volumetric_flow_rate * pow(chx->area, -0.3e1) * chx->perimeter;
        //chx->press_drop_dV=0.1331018519e0 * pow(log(0.1725000000e1 / chx->density * chx->dynamic_viscosity* chx->perimeter / chx->volumetric_flow_rate + 0.5023535735e-1 * pow(chx->roughness * chx->perimeter / chx->area, 0.111e1)), -0.3e1) * pow(log(0.10e2), 0.2e1) * chx->length * pow(chx->area, -0.3e1) * chx->perimeter * chx->perimeter * chx->dynamic_viscosity / (0.1725000000e1 / chx->density * chx->dynamic_viscosity * chx->perimeter / chx->volumetric_flow_rate + 0.5023535735e-1 * pow(chx->roughness * chx->perimeter / chx->area, 0.111e1)) + 0.7716049384e-1 * pow(log(0.1725000000e1 / chx->density * chx->dynamic_viscosity * chx->perimeter / chx->volumetric_flow_rate + 0.5023535735e-1 * pow(chx->roughness * chx->perimeter / chx->area, 0.111e1)), -0.2e1) * pow(log(0.10e2), 0.2e1) * chx->length * chx->density * chx->volumetric_flow_rate * pow(chx->area, -0.3e1) * chx->perimeter;

    }

    if (chx->Reynolds_number >= 2400 && chx->Reynolds_number <= 4000) {
        chx->laminar_or_turbulent = Channel::mixed;
        //For transition flow Swamee and Jaim declared an equation to determine the head loss. Based on it the friction factor can be determined within 2% uncertainty
        chx->friction_factor = (1.07*pow(chx->volumetric_flow_rate, 2)*chx->length / (gravity_value*pow(chx->hydraulic_diameter, 5))*pow(log(chx->roughness / (3.7*chx->hydraulic_diameter) + 4.62*pow(chx->dynamic_viscosity*chx->hydraulic_diameter / chx->volumetric_flow_rate, 0.9)), -2))*chx->hydraulic_diameter / chx->length * 2 / pow(chx->velocity_of_flow, 2);
        chx->press_drop = chx->friction_factor*chx->length / chx->hydraulic_diameter*chx->density / 2 * pow(chx->volumetric_flow_rate, 2) / pow(chx->area, 2);

    }

    chx->head_loss = chx->press_drop / (chx->density*gravity_value);

    if (chx->Prandtl_number<0.7 && chx->laminar_or_turbulent == Channel::turb) { printf("!! Prandtl number is smaller than 0.7! Equations of Nusselt number calculation are not valid !!"); }

    return 0;
}


//***********************************************************************
int Gyuri_uchannel::main_old(uchannel &ucha){
//***********************************************************************

    // Defining the first channel and giving the constant values - in the near future it will be uploaded from some text files
    uns j, wished_segment_number;
    struct Channel ch1;
    // the type of the fluid is AIR
    double volumetric_flow_rate; //[l/h] unit
    double summa_heat_exchange = 0;
    cd PI = acos(0.0)*2.0;

    FILE * spice_file;
    FILE *fu = fopen((subpath + "/uchannel_" + ucha.nev + ".txt").c_str(), "wt");

    ch1.density = ucha.density;
    ch1.dynamic_viscosity = ucha.dynamic_visc;
    ch1.specific_heat = ucha.spec_heat;
    ch1.heat_conductivity = ucha.heat_cond;
    if (ucha.tipus == CsatRect)
        ch1.cross_section_type = Channel::rect;
    else if (ucha.tipus == CsatCirc)
        ch1.cross_section_type = Channel::circ;
    else if (ucha.tipus == CsatTrap)
        ch1.cross_section_type = Channel::trap;
    else
        throw hiba("Gyuri_uchannel::main", "program error: ucha.tipus");
    ch1.length = ucha.length;
    ch1.height = ucha.height;
    ch1.roughness = ucha.roughness;
    ch1.width_bottom = ucha.width_bottom;
    ch1.width_top = ucha.width_top;
    ch1.fluid_inlet_temperature = ucha.fluid_temp;

    volumetric_flow_rate = ucha.flow_rate; // / 48; // !!!

    if (fu!=NULL)fprintf(fu,"Based on the given data the calculated values are the following:\n");

    switch (ch1.cross_section_type)
    {
    case Channel::rect:
        ch1.perimeter = 2 * ch1.width_top + 2 * ch1.height;
        ch1.area = ch1.width_top*ch1.height;
        break;

    case Channel::circ:
        ch1.perimeter = ch1.width_top*3.14159265359;
        ch1.area = pow(ch1.width_top / 2, 2)*PI;
        break;

    case Channel::trap:
        ch1.perimeter = ch1.width_bottom + ch1.width_top + 2 * sqrt(pow(ch1.height, 2) + pow(ch1.width_top - ch1.width_bottom, 2));
        ch1.area = (ch1.width_bottom + ch1.width_top) / 2 * ch1.height;
        break;

    }

    if (fu != NULL)fprintf(fu, "The P perimeter is %.4e \n", ch1.perimeter);
    if (fu != NULL)fprintf(fu, "The A area is %.4e \n", ch1.area);

    ch1.hydraulic_diameter = 4 * ch1.area / ch1.perimeter;
    if (fu != NULL)fprintf(fu, "The Dh is %.4e \n", ch1.hydraulic_diameter);

    ch1.Prandtl_number = ch1.dynamic_viscosity*ch1.specific_heat / ch1.heat_conductivity;
    if (fu != NULL)fprintf(fu, "The Prandtl Number is %4.4lf \n", ch1.Prandtl_number);

    //if (ch1.cross_section_type == Channel::trap){
    //    ch1.perimeter = ch1.width_bottom + ch1.width_top + 2 * pow(pow(ch1.height, 2) + pow(ch1.width_top - ch1.width_bottom, 2), 0.5);
    //    if (fu != NULL)fprintf(fu, "The P perimeter is %.4e \n", ch1.perimeter);
    //    ch1.area = (ch1.width_bottom + ch1.width_top) / 2 * ch1.height;
    //    if (fu != NULL)fprintf(fu, "The A area is %.4e \n", ch1.area);
    //    ch1.hydraulic_diameter = 4 * ch1.area / ch1.perimeter;
    //    if (fu != NULL)fprintf(fu, "The Dh is %.4e \n", ch1.hydraulic_diameter);

    //}

    //ch1.Prandtl_number = ch1.dynamic_viscosity*ch1.specific_heat / ch1.heat_conductivity;
    //if (fu != NULL)fprintf(fu, "The Prandtl Number is %4.4lf \n", ch1.Prandtl_number);

    // [m/s] must use instead of volumetric flow rate!!!!
    // The original ucooler structure consists of 48 channels, but now only one selected channel is under investigation
    // [l/h] /3600 results [l/sec] /1000 results [m^3/sec] /cross_sectional_area results [m/sec]
    ch1.volumetric_flow_rate = volumetric_flow_rate / 3600 / 1000; // BEWARE THE NUMBER OF CHANNEL MUST BE DETERMINED IN THE FUTURE
    ch1.velocity_of_flow = ch1.volumetric_flow_rate / ch1.area;
    ch1.Reynolds_number = (volumetric_flow_rate / 3600 / 1000) / ch1.area * ch1.hydraulic_diameter / ch1.dynamic_viscosity;
    if (fu != NULL)fprintf(fu, "The mean velocity [m/s] is %e \n", volumetric_flow_rate / 3600 / 1000 / ch1.area);
    ch1.mass_flow_rate = ch1.density*volumetric_flow_rate / 3600 / 1000;
    if (fu != NULL)fprintf(fu, "The mass-flow rate [kg/s] is %e \n", ch1.mass_flow_rate);
    if (fu != NULL)fprintf(fu, "The Reynolds Number is %4.4lf \n", ch1.Reynolds_number);

    if (ch1.Reynolds_number<2400) { ch1.laminar_or_turbulent = Channel::lam; if (fu != NULL)fprintf(fu, "Laminar flow type is determined\n"); }
    else { ch1.laminar_or_turbulent = Channel::turb; if (fu != NULL)fprintf(fu, "This is definitely a turbulent flow... BEWARE!\n"); }

    //ch1.average_Nusselt_number=5.14+(0.065*ch1.hydraulic_diameter*ch1.Reynolds_number*ch1.Prandtl_number)/(ch1.length*(1+0.04*pow(ch1.hydraulic_diameter*ch1.Reynolds_number*ch1.Prandtl_number/ch1.length,2/3)));
    ch1.average_Nusselt_number = nusselt_calculation(&ch1, ch1.length);
    if (fu != NULL)fprintf(fu, "The AVERAGE Nusselt Number is %4.4lf \n", ch1.average_Nusselt_number);

    ch1.global_heat_transfer_coefficient = ch1.heat_conductivity*ch1.average_Nusselt_number / ch1.hydraulic_diameter;
    if (fu != NULL)fprintf(fu, "The global heat transfer coefficient is %5.4lf \n", ch1.global_heat_transfer_coefficient);

    if (fu != NULL)fprintf(fu, "The calculated global thermal resistance is %lf \n", 1 / (ch1.mass_flow_rate*ch1.specific_heat*(1 - exp(-ch1.global_heat_transfer_coefficient*ch1.perimeter*ch1.length / (ch1.mass_flow_rate*ch1.specific_heat)))));

    if (fu != NULL)fprintf(fu, "The output temperature of the fluid at the and of the channel %.4lf \n", 60 - (60 - ch1.fluid_inlet_temperature) * 1 / exp(ch1.global_heat_transfer_coefficient*ch1.perimeter*ch1.length / (ch1.mass_flow_rate*ch1.specific_heat)));
    if (fu != NULL)fprintf(fu, "(%g, %g, %g, %g, %g, %g)\n", ch1.fluid_inlet_temperature,ch1.global_heat_transfer_coefficient,ch1.perimeter,ch1.length,ch1.mass_flow_rate,ch1.specific_heat);
    if (fu != NULL)fprintf(fu, "The length of the channel is %.4e \n", ch1.length);
    if (fu != NULL)fprintf(fu, "The length of the thermal entry region is %.4e\n", 0.05*ch1.Reynolds_number*ch1.Prandtl_number*ch1.hydraulic_diameter);
    if (fu != NULL)fprintf(fu, "The Characteristic length (where Tfluid is equal to the 63.2%% of (Tw-Ti) is %.4e\n", ch1.mass_flow_rate*ch1.specific_heat / ch1.global_heat_transfer_coefficient / ch1.perimeter);

    if (fu != NULL)fprintf(fu, "The 1/e^3 (5%%) Lchar is %.4e\n", log((60 - ch1.fluid_inlet_temperature)*exp(-3.0) / ((60 - ch1.fluid_inlet_temperature))) / (-ch1.global_heat_transfer_coefficient*ch1.perimeter / (ch1.mass_flow_rate*ch1.specific_heat)));

    if (fu != NULL)fprintf(fu, "The 1/e^5 (1%%) Lchar is %.4e\n", log((60 - ch1.fluid_inlet_temperature)*exp(-5.0) / ((60 - ch1.fluid_inlet_temperature))) / (-ch1.global_heat_transfer_coefficient*ch1.perimeter / (ch1.mass_flow_rate*ch1.specific_heat)));


    wished_segment_number = ucha.n; // !!!

    Local_Parameters_calculation(&ch1, wished_segment_number);

    if (fu != NULL)fprintf(fu, "\n The LOCAL Nusselt Numbers of each segments are the followings: \n");
    //segment_pointer=ch1.first_segment;
    for (j = 1; j <= wished_segment_number; j++) {
        if (fu != NULL)fprintf(fu, "\t%4.4lf\t", ch1.segments[j].Nusselt_number);
        if (j % 4 == 0) if (fu != NULL)fprintf(fu, "\n");
    }
    if (fu != NULL)fprintf(fu, "\n The LOCAL heat transfer coefficient of each segments are the followings: \n");
    for (j = 1; j <= wished_segment_number; j++) {
        if (fu != NULL)fprintf(fu, "\t%4.2lf\t", ch1.segments[j].local_heat_transfer_coeff);
        if (j % 4 == 0) if (fu != NULL)fprintf(fu, "\n");
    }

    Rladder_stage_calculation(&ch1, wished_segment_number, ucha);

    char file_name[64] = "uchannel_spice.cir";

    // I would like to add date and time tag to the file in the future...

    if (fu != NULL)fprintf(fu, "\nThe values of the R ladder are the followings: \n");

    spice_file = fopen((subpath + "/uchannel_" + ucha.nev + "_spice.cir").c_str(), "wt");

    for (j = 1; j <= wished_segment_number; j++) {
        if (j == 1){
            if (spice_file != NULL)fprintf(spice_file, "### Compact model of microscale integrated channel structures ###\n");
            if (spice_file != NULL)fprintf(spice_file, "V0 %i 0 %.8f\n", 4 * j, ch1.fluid_inlet_temperature);
            if (fu != NULL)fprintf(fu, "The heat resistance in one segment along the channel is: %f \n", ch1.stages[j].resistance_along);
            if (fu != NULL)fprintf(fu, "The heat resistance through the channel walls in the segments and the exit temperatures are the following:\n The WALL temperature is %3.0f Celsius degree\n\n", ch1.stages[j].Twall);
        }
        //if (fu != NULL)fprintf(fu, "%3u\tRth_cross= %.4f\tTout= %.2f \tdQ= %.4f\n", j, ch1.stages[j].resistance_cross, ch1.stages[j].Tout, ch1.stages[j].heat_exchange);
        if (fu != NULL)fprintf(fu, "%3u\tRth_cross= %.4f\tTwall = %3.2f C\tTout= %.2f \tdQ= %.4f\n", j, ch1.stages[j].resistance_cross, ch1.stages[j].Twall, ch1.stages[j].Tout, ch1.stages[j].heat_exchange);

        ucha.psim->tucha_perem[ucha.szinindex][j - 1].set_uchannel_segment(j == 1 ? ch1.fluid_inlet_temperature : ch1.stages[j-1].Tout, ch1.stages[j].resistance_cross);

        summa_heat_exchange = summa_heat_exchange + ch1.stages[j].heat_exchange;

        if (spice_file != NULL)fprintf(spice_file, "R%i %i %i %.8f\n", j, 4 * j, 4 * j + 1, ch1.stages[j].resistance_cross);
        if (spice_file != NULL)fprintf(spice_file, "V%i %i %i DC %.8f\n", 4 * j + 1, 4 * j + 1, 4 * j + 2, ch1.stages[j].Twall);
        if (spice_file != NULL)fprintf(spice_file, "V%i %i %i DC 0\n", 4 * j + 2, 4 * j + 2, 0);
        if (spice_file != NULL)fprintf(spice_file, "H%i %i %i V%i %.8f\n\n", j, 4 * j, 4 * (j + 1), 4 * j + 2, ch1.stages[j].resistance_along);
        //  if (spice_file != NULL)fprintf(spice_file,"E%i %i %i %i %i 1\n\n",j,4*(j+1),4*j+3,4*j,0);
    }

    if (fu != NULL)fprintf(fu, "\t\t\t\t\t\tTotal dQ=%.4f\n", summa_heat_exchange);

    if (spice_file != NULL)fprintf(spice_file, "\n\n.op\n.end");

    if (spice_file != NULL)fclose(spice_file);
    if (fu != NULL)fclose(fu);

    return(0);
}

//***********************************************************************
int Gyuri_uchannel::main(uchannel &ucha) {
//***********************************************************************
    uns jj, wished_segment_number;
    double tot_heat_exchange = 0;
    double tot_conduction = 0;
    double estimated_volum_flow_rate;
    double global_wall_temp;
    struct Channel ch1;

    FILE *spice_file;
    FILE *fu = fopen((subpath + "/uchannel_" + ucha.nev + ".txt").c_str(), "wt");

    ch1.density = ucha.density;
    ch1.dynamic_viscosity = ucha.dynamic_visc;
    ch1.specific_heat = ucha.spec_heat;
    ch1.heat_conductivity = ucha.heat_cond;
    if (ucha.tipus == CsatRect)
        ch1.cross_section_type = Channel::rect;
    else if (ucha.tipus == CsatCirc)
        ch1.cross_section_type = Channel::circ;
    else if (ucha.tipus == CsatTrap)
        ch1.cross_section_type = Channel::trap;
    else
        throw hiba("Gyuri_uchannel::main", "program error: ucha.tipus");
    ch1.length = ucha.length;
    ch1.height = ucha.height;
    ch1.roughness = ucha.roughness;
    ch1.width_bottom = ucha.width_bottom;
    ch1.width_top = ucha.width_top;
    ch1.fluid_inlet_temperature = ucha.fluid_temp;

    estimated_volum_flow_rate = ucha.flow_rate; // / 48; // !!!
    estimated_volum_flow_rate = estimated_volum_flow_rate / 3600 / 1000;

    Hydrodynamic_calculation(&ch1, estimated_volum_flow_rate);

    if (fu != NULL)fprintf(fu, "The calculated friction factor is %f \n", ch1.friction_factor);
    if (fu != NULL)fprintf(fu, "Fd %e L %e Dh %e Ro %e v2 %e\n", ch1.friction_factor, ch1.length, ch1.hydraulic_diameter, ch1.density, pow(ch1.velocity_of_flow, 2));
    if (fu != NULL)fprintf(fu, "The calculated Pressure drop [kPa] %e \n", ch1.press_drop);
    if (fu != NULL)fprintf(fu, "Pressure drop [kPa] - laminar case, fd=64/Re: %e \n\n", 32 * ch1.dynamic_viscosity*ch1.length / pow(ch1.hydraulic_diameter, 2)*ch1.velocity_of_flow);
    if (fu != NULL)fprintf(fu, "The calculated Head loss [m] %f \n", ch1.head_loss);


    if (fu != NULL)fprintf(fu, "Based on the given data the calculated values are the following:\n");
    if (fu != NULL)fprintf(fu, "The P perimeter is %.4e \n", ch1.perimeter);
    if (fu != NULL)fprintf(fu, "The A area is %.4e \n", ch1.area);
    if (fu != NULL)fprintf(fu, "The a/b ratio is %f \n", ch1.side_ratio);
    if (fu != NULL)fprintf(fu, "The Dh is %.4e \n", ch1.hydraulic_diameter);
    if (fu != NULL)fprintf(fu, "The Prandtl Number is %4.4f \n", ch1.Prandtl_number);
    if (fu != NULL)fprintf(fu, "The volumetric flow rate [m3/sec] is %e \n", ch1.volumetric_flow_rate);
    if (fu != NULL)fprintf(fu, "The volumetric flow rate [l/h] is %4.4f \n", ch1.volumetric_flow_rate * 1000 * 3600);
    if (fu != NULL)fprintf(fu, "The mean velocity [m/s] is %e \n", ch1.velocity_of_flow);
    // It is not sure... 2/3 or 1/3 times of 1 mach define the boundary of the usage...
    if (ch1.velocity_of_flow>340 * 0.3) if (fu != NULL)fprintf(fu, "!! In case of compressible gaseous coolant the non-Newtonian aspects should be investigated !!\n");
    if (fu != NULL)fprintf(fu, "The mass-flow rate [kg/s] is %e \n", ch1.mass_flow_rate);
    if (fu != NULL)fprintf(fu, "The Reynolds Number is %4.4f \n", ch1.Reynolds_number);
    switch (ch1.laminar_or_turbulent)
    {
        case Channel::lam: if (fu != NULL)fprintf(fu, "Laminar flow type is determined...\n"); break;
        case Channel::turb: if (fu != NULL)fprintf(fu, "This is definitely a turbulent flow...\n"); break;
        case Channel::mixed: if (fu != NULL)fprintf(fu, "Transition type from laminar to turbulent...\n"); break;
    }

    if (fu != NULL)fprintf(fu, "The Pressure drop is %4.4f [bar]\n", ch1.press_drop / 1e5);

    wished_segment_number = ucha.n; // !!!

    global_wall_temp = 60.0;

    Thermal_prop_calculation(&ch1, global_wall_temp, fu);
    Local_Parameters_calculation(&ch1, wished_segment_number);
    if (fu != NULL)fprintf(fu, "\n The LOCAL Nusselt Numbers of each segments are the followings: \n");
    for (jj = 1;jj <= wished_segment_number;jj++) {
        if (fu != NULL)fprintf(fu, "\t%4.4lf\t", ch1.segments[jj].Nusselt_number);
        if (jj % 4 == 0) if (fu != NULL)fprintf(fu, "\n");
    }
    if (fu != NULL)fprintf(fu, "\n The LOCAL heat transfer coefficient of each segments are the followings: \n");
    for (jj = 1;jj <= wished_segment_number;jj++) {
        if (fu != NULL)fprintf(fu, "\t%4.2lf\t", ch1.segments[jj].local_heat_transfer_coeff);
        if (jj % 4 == 0) if (fu != NULL)fprintf(fu, "\n");
    }
    Rladder_stage_calculation(&ch1, wished_segment_number, ucha);
    spice_file = fopen((subpath + "/uchannel_" + ucha.nev + "_spice.cir").c_str(), "wt");
    tot_heat_exchange = tot_heat_exchange + Rladder_to_Spice(&ch1, spice_file, fu, 1, wished_segment_number, ucha);
    if (spice_file != NULL)fclose(spice_file);
    tot_conduction = tot_conduction + ch1.global_thermal_conductivity;

    if (fu != NULL)fprintf(fu, "\n\n THE CUMULATIVE RTHja is: %4.4f [K/W]", 1 / tot_conduction);
    if (fu != NULL)fprintf(fu, "\n THE SUM OF HEAT EXCHANGE is: %4.4f [W]", tot_heat_exchange);

    if (fu != NULL)fclose(fu);

    return(0);
}