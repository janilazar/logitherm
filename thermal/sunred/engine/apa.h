//***********************************************************************
// apa class header
// Creation date:  2009. 07. 11.
// Creator:        Pohl L�szl�
//***********************************************************************


//***********************************************************************
#ifndef VSUN3_APA_HEADER
#define	VSUN3_APA_HEADER
//***********************************************************************


//***********************************************************************
#include "tipusok.h"
#include "gfunc.h"
#include "dmatrix.h"
#include "qmatrix.h"
//***********************************************************************


//***********************************************************************
struct real_cell_res{ // potenci�lok ill. �ramok visszaad�s�ra szolg�l
//***********************************************************************
    dbl t[BASIC_SIDES]; // EXTERNAL=CENTER m�don haszn�ljuk helytakar�koss�g miatt
    real_cell_res(){for(uns i=0;i<BASIC_SIDES;i++)t[i]=nulla;}
};


//***********************************************************************
struct dcomplex_cell_res{ // potenci�lok ill. �ramok visszaad�s�ra szolg�l
//***********************************************************************
    dcomplex t[BASIC_SIDES]; // EXTERNAL=CENTER m�don haszn�ljuk helytakar�koss�g miatt
    dcomplex_cell_res(){for(uns i=0;i<BASIC_SIDES;i++)t[i]=nulla;}
};


//***********************************************************************
struct GTpar{
//***********************************************************************
    dbl G[3],T; // dbl G,T;
};

struct material;

//***********************************************************************
struct segedcella{
//***********************************************************************
    dbl T_elozo,T_akt;
    dbl feltoltottseg;
    bool is_push_up,is_push_down;
    const material *mater;
    segedcella():T_elozo(nulla),T_akt(nulla),feltoltottseg(nulla),
                 is_push_up(false),is_push_down(false),mater(nullptr){}
    //***********************************************************************
    dbl get_ido_arany(dbl T_akt); // Ha nem kell visszal�pni, akkor 1 (csak hiszter�zises/f�zisv�lt�s esetben kell)
   //***********************************************************************
    void update_his();
    //***********************************************************************
    void clear(){ T_elozo = T_akt = feltoltottseg = nulla; is_push_up = is_push_down = false; }
};

#define GGSIZE 8
//***********************************************************************
struct vezetes{
//***********************************************************************
    monlintipus tipus,semitip;  // enum monlintipus{nlt_lin,nlt_exp,nlt_diode,nlt_quadratic};
    dbl g[3],gg[GGSIZE],T;           // dbl g[3],gg[GGSIZE],T;
    tomb<GTpar> szakaszok;      // tomb<GZpar> szakaszok;
    bool specific;              // bool, Ern� t�pusn�l ha a m�trix 1 m2-re vonatkozik, azaz a f�lvezet� fel�let�vel szorzand�
    bool is_resistivity;        // ha true, a felhaszn�l� fajlagos ellen�ll�st adott meg vezet�s helyett, a h�m�rs�kletf�gg�s f�gggv�ny�t erre kell alkalmazni
    dbl his_value_1,his_value_2; // A hiszter�zises tartom�nyban az �rt�k. Ha value_2=0, akkor kapacit�s, azaz a visszaadott �rt�k value_1/(T_max-T_min), egy�bk�nt vezet�s, ami line�risan sk�l�z�dik min �s max k�z�tt
    // nlt_erno eset�n g lesz B�d, gg pedig M�d
    vezetes(dbl default_g=nulla):tipus(nlt_lin),semitip(nlt_lin),T(nulla),specific(false),is_resistivity(false)
        , his_value_2(nulla) {
        g[0] = g[1] = g[2] = default_g; 
        for (uns i = 0; i < GGSIZE; i++) 
            gg[i] = nulla;
    }
    //***********************************************************************
    dbl get_ertek(uns irany,dbl T,bool is_lin, const segedcella * s)const{
        return is_lin ? (is_resistivity ? egy / g[irany] : g[irany]) : get_ertek_belso(irany,T,is_lin,s);
    }
    //***********************************************************************
    dbl get_ertek_belso(uns irany,dbl T,bool is_lin, const segedcella * s)const; // irany: 0=x, 1=y, 2=z
    //***********************************************************************
    dbl get_x(dbl T,bool is_lin, const segedcella * s)const{return get_ertek(0,T,is_lin,s);}
    //***********************************************************************
    dbl get_y(dbl T,bool is_lin, const segedcella * s)const{return get_ertek(1,T,is_lin,s);}
    //***********************************************************************
    dbl get_z(dbl T,bool is_lin, const segedcella * s)const{return get_ertek(2,T,is_lin,s);}
    //***********************************************************************
    dbl get_sem_ertek(dbl T,dbl I,dbl A,dbl As,bool init)const; // irany: 0=x, 1=y, 2=z, As= a f�lvezet� anyag fel�lete, init: iniciliz�l�skor nem sz�m�t az �ram ir�nya
    //***********************************************************************
    dbl get_sem_rad_lum(dbl T,dbl I,uns azon,dbl As)const; // A vezet�s most csak az Ern�-m�trixot t�rolja, teh�t val�j�ban nem is vezet�s. A h�m�rs�klethez tartoz� sug�rs�r�s�g vagy lumen �rt�ket adja
    //***********************************************************************
    void print(void)const{ // diagnosztikai c�lra
    //***********************************************************************
        printf("\ng[0]=%g, g[1]=%g, g[2]=%g\n",g[0],g[1],g[2]);
        for (uns i = 0;i < GGSIZE; i++)
            printf("gg[%u]=%g,",i, gg[i]);
        printf("\nszakaszok.size()=%u\n",szakaszok.size());
        for(u32 i=0; i<szakaszok.size(); i++)
            printf("T=%g, G[0]=%g, G[1]=%g, G[2]=%g\n",szakaszok[i].T,szakaszok[i].G[0],szakaszok[i].G[1],szakaszok[i].G[2]);
    }
};


//***********************************************************************
struct material{
//***********************************************************************
    PLString nev;               // PLString
    bool is_el,is_th,is_his;    // bool, l�tezik-e a t�r, van-e hiszter�zis
    vezetes elvez;              // vezetes elvez;
    vezetes thvez;              // vezetes
    vezetes Ce,Cth,S;           // vezetes
    vezetes D;                  // vezetes, 0..1, az es� elektromos telj mekkora r�sze f�t
    vezetes emissivity;         // vezet�s, 0..1, def 1.
    vezetes th_transmiss;       // vezet�s, 0..1, m�s anyaggal �rintkez� fel�letein keletkez� h�m�rs�kleti sug�rz�s ekkora r�sz�t engedi �t, def 0
    dbl his_T_min, his_T_max, his_T_width_fele; // A hiszter�zis s�vj�nak als� �s fels� k�z�ph�m�rs�klete, valamit a s�v sz�less�ge (a sz�less�g fele-fele van a k�z�p k�t oldal�n)
    dbl his_T1,his_T2,his_T3,his_T4,his_delta,his_inv_delta;

    //***********************************************************************
    void set_his_derivaltak(){
     //***********************************************************************
       his_T1 = his_T_min - his_T_width_fele;
        his_T2 = his_T_min + his_T_width_fele;
        his_T3 = his_T_max - his_T_width_fele;
        his_T4 = his_T_max + his_T_width_fele;
        his_delta = his_T_max - his_T_min;
        his_inv_delta = egy / his_delta;
    }
    //***********************************************************************

    // A redukci� csak a f�ggv�nyek seg�ts�g�vel k�rdezheti le a mennyis�geket!

    vezetes & get_elvez(void){return elvez;}
    vezetes & get_thvez(void){return thvez;}
    vezetes & get_Ce(void){return Ce;}
    vezetes & get_Cth(void){return Cth;}
    vezetes & get_S(void){return S;}
    vezetes & get_D(void){return D;}

    material():is_el(false),is_th(false),is_his(false),D(egy),
        emissivity(egy),his_T_min(nulla),his_T_max(egy),his_T_width_fele(nulla){set_his_derivaltak();}
};


//***********************************************************************
inline dbl vezetes::get_ertek_belso(uns irany,dbl T,bool is_lin, const segedcella * s)const{ // irany: 0=x, 1=y, 2=z
// H�m�rs�kletf�gg� vezet�st ad vissza
//***********************************************************************
    if(is_resistivity){ // ha a g m�trixban nem vezet�s, hanem ellen�ll�s van
        if(is_lin)return egy / g[irany];
//            if(gg[0]!=0.0)printf("T=%10g, gg=%10g, limit=%10g, g=%10g, ret=%10g\n",T,gg[0],limit,g[irany],g[irany]*(limit>1.0?exp(1.0):exp(limit)));
        switch(tipus){
            case nlt_lin: return egy / g[irany];
            case nlt_exp: {
                cd szorzat = gg[0] * ( T - 25.0 );
                cd kitevo = szorzat > 7.0 ? 7.0 : szorzat < -7.0 ? -7.0 : szorzat;
                return egy / (g[irany] * exp(kitevo));
            }
            case nlt_diode: throw hiba("vezetes::get_ertek","diode equation is not applicable");
            case nlt_quadratic: return egy / ( g[irany] * ( gg[0] * T * T + gg[1] * T + gg[2] ) );
            case nlt_szakaszok: {
                cu32 n = szakaszok.size();
                dbl G0, G1, T0, T1;
                if (s != nullptr &&  s->feltoltottseg != nulla && s->feltoltottseg != egy) { // hiszter�zis
                    if (his_value_2 == 0.0)
                        throw hiba("vezetes::get_ertek", "heat capacity cannot be resistivity");
                    return egy / (his_value_1 + (his_value_2 - his_value_1) * s->feltoltottseg);
                }
                if (n == 0)throw hiba("vezetes::get_ertek", "0 vertice polygon");
                if (n == 1)return egy / szakaszok[0].G[irany];
                //                    if(T<=szakaszok[0].T){          G0=szakaszok[  0].G[irany];  G1=szakaszok[  1].G[irany]; T0=szakaszok[  0].T;  T1=szakaszok[  1].T; }
                //                    else if(T>=szakaszok[n-1].T){   G0=szakaszok[n-2].G[irany];  G1=szakaszok[n-1].G[irany]; T0=szakaszok[n-2].T;  T1=szakaszok[n-1].T; }
                if (T <= szakaszok[0].T) { 
                    //G0 = szakaszok[0].G[irany];  G1 = szakaszok[0].G[irany]; T0 = szakaszok[0].T;  T1 = szakaszok[1].T; 
                    return egy / szakaszok[0].G[irany];
                }
                else if (T >= szakaszok[n - 1].T) { 
                    //G0 = szakaszok[n - 1].G[irany];  G1 = szakaszok[n - 1].G[irany]; T0 = szakaszok[n - 2].T;  T1 = szakaszok[n - 1].T; 
                    return egy / szakaszok[n - 1].G[irany];
                }
                else {
                    u32 i = 1;
                    while (T > szakaszok[i].T) i++;
                    G0 = szakaszok[i - 1].G[irany];  G1 = szakaszok[i].G[irany];
                    T0 = szakaszok[i - 1].T;         T1 = szakaszok[i].T;
                    return egy / (G0 + (G1 - G0) * (T - T0) / (T1 - T0));
                }
            }
            case nlt_mizs1:
                return egy / fn_mizs1(T, gg[0], gg[1], gg[2], gg[3], gg[4], gg[5], gg[6]);
        }
    }
    else{
        if(is_lin)return g[irany];
//            if(gg[0]!=0.0)printf("T=%10g, gg=%10g, limit=%10g, g=%10g, ret=%10g\n",T,gg[0],limit,g[irany],g[irany]*(limit>1.0?exp(1.0):exp(limit)));
        switch(tipus){
            case nlt_lin: return g[irany];
            case nlt_exp: {
                cd szorzat=gg[0]*(T-25.0);
                cd kitevo=szorzat>7.0?7.0:szorzat<-7.0?-7.0:szorzat;
                return g[irany] * exp(kitevo);
            }
            case nlt_diode: throw hiba("vezetes::get_ertek","diode equation is not applicable");
            case nlt_quadratic: return g[irany]*(gg[0]*T*T+gg[1]*T+gg[2]);
            case nlt_szakaszok: {
                cu32 n = szakaszok.size();
                dbl G0, G1, T0, T1, dC = nulla;
                if (s != nullptr && s->mater->is_his && T > s->mater->his_T1 && T < s->mater->his_T4) { // hiszter�zis
                    //printf("@");
                    if (his_value_2 != 0.0)
                        return his_value_1 + (his_value_2 - his_value_1) * s->feltoltottseg;
                    if (s->is_push_up || s->is_push_down)
                        dC = his_value_1 * s->mater->his_inv_delta; // kapacit�s
                }
                if (n == 0)throw hiba("vezetes::get_ertek", "0 vertice polygon");
                if (n == 1)return szakaszok[0].G[irany];
                //                    if(T<=szakaszok[0].T){          G0=szakaszok[  0].G[irany];  G1=szakaszok[  1].G[irany]; T0=szakaszok[  0].T;  T1=szakaszok[  1].T; }
                //                    else if(T>=szakaszok[n-1].T){   G0=szakaszok[n-2].G[irany];  G1=szakaszok[n-1].G[irany]; T0=szakaszok[n-2].T;  T1=szakaszok[n-1].T; }
                if (T <= szakaszok[0].T) { 
                    //G0 = G1 = szakaszok[0].G[irany]; T0 = szakaszok[0].T;  T1 = szakaszok[1].T;
                    return dC + szakaszok[0].G[irany];
                }
                else if (T >= szakaszok[n - 1].T) { 
                    //G0 = G1 = szakaszok[n - 1].G[irany]; T0 = szakaszok[n - 2].T;  T1 = szakaszok[n - 1].T; 
                    return dC + szakaszok[n - 1].G[irany];
                }
                else {
                    u32 i = 1;
                    while (T > szakaszok[i].T) i++;
                    G0 = szakaszok[i - 1].G[irany];  G1 = szakaszok[i].G[irany];
                    T0 = szakaszok[i - 1].T;         T1 = szakaszok[i].T;
                    return dC + G0 + (G1 - G0) * (T - T0) / (T1 - T0);
                }
            }
            case nlt_mizs1:
                return fn_mizs1(T, gg[0], gg[1], gg[2], gg[3], gg[4], gg[5], gg[6]);
        }
    }
    throw hiba("vezetes::get_ertek","unexpected control path (%u)",tipus);
}


//***********************************************************************
struct semiconductor{
//***********************************************************************
    static uns db;              // ststic uns, a l�tez� f�lvezet�k darabsz�ma, az azonos�t� meghat�roz�s�hoz kell.
    uns azon;                   // uns, a f�lvezet� azonos�t�ja, a full_I_semi t�mb indexel�s�hez kell
    uns col2;                   // uns, a m�sik sz�n
    vezetes par;                // vezetes par, par.g[0]=Vt, par.g[1]=I0, vigy�zat I0 1 n�gyzetm�terre!!!, 3 par eset�n m�sodfok� egyenlet, gg a h�m f�gg�s
    vezetes D;                  // vezetes, 0..1, az es� elektromos telj mekkora r�sze f�t
    vezetes R;                  // vezetes, >=1, a sug�rzott teljes�tm�ny h�nyszorosa von�djon le a sz�m�tott disszip�ci�b�l Dissz=U^2*G*D-F*R, F a radianciatomb megfelel� eleme
    vezetes rad,lum;            // vezetes, az adott �tmenethez tartoz� radiancia ill luminancia �rt�keket tartalmazza
    dbl As;                     // dbl, mekkora fel�leten �rintkezik a k�t anyag egym�ssal
    semiconductor():col2(0),As(nulla),D(egy),R(egy){azon=db++;}
};


//***********************************************************************
struct color{
//***********************************************************************
    PLString nev;               // PLString
    bool is;                    // bool, ha van defini�lva, true
    SzinTipus tipus;            // SzinTipus: SzinNormal,SzinBoundary,SzinUchannel
    dbl terfogat;               // dbl
    material * pmat;            // material *, peremn�l NULL, a sz�nhez tartoz� anyagra mutat
    tomb<semiconductor> tsemi;  // tomb<semiconductor>, mely anyagok fel� f�lvezet�
    color() :is(false), tipus(SzinNormal), pmat(NULL), terfogat(0.0){}
};


//***********************************************************************
struct gcsd{
// vezet�s, kapacit�s, seebeck, dissz. koeff
//***********************************************************************
    dbl g[BASIC_SIDES];         // dbl g[BASIC_SIDES]
    dbl gfull[BASIC_SIDES];     // dbl gfull[BASIC_SIDES], g�fg
    dbl c,V,Vszin;              // dbl, V: kocka t�rfogat, Vszin: sz�n t�rfogat mat-b�l
    dbl s[BASIC_SIDES];         // dbl s[BASIC_SIDES] => ( Sc + Soldal ) / 2 => Seebeck-effektushoz
    dbl s_valodi[BASIC_SIDES];  // dbl s_valodi[BASIC_SIDES] => itt t�nyleg a sz�l�n l�v� �rt�k, Peltier-Thomson-effektushoz
    dbl d[BASIC_SIDES];         // dbl d[BASIC_SIDES]
    dbl dfull[BASIC_SIDES];     // dbl dfull[BASIC_SIDES], (d*fg+df*g)/(g+gf)
    dbl fg[BASIC_SIDES];        // dbl fg[BASIC_SIDES]; f�lvezet� r�sz�
    dbl fd[BASIC_SIDES];        // dbl fd[BASIC_SIDES]; f�lvezet� r�sz�
    dbl fI[BASIC_SIDES];        // dbl fI[BASIC_SIDES]; a f�lvezet�n sz�molt �ram a f�nysz�m�t�shoz (jelenleg nem haszn�ljuk, csak felt�ltj�k)
    dbl fr[BASIC_SIDES];        // dbl fr[BASIC_SIDES]; radiance factor, a f�lvezet�n kisug�rzott f�ny h�nyszorosa a kisug�rzott teljes�tm�ny, ami nem meleg�ti az anyagot
    dbl f_rad[BASIC_SIDES];     // dbl f_rad[BASIC_SIDES]; radiancia szorz�
    dbl f_lum[BASIC_SIDES];     // dbl f_lum[BASIC_SIDES]; luminancia szorz�
    dbl f_area[BASIC_SIDES];    // dbl f_area[BASIC_SIDES]; f�lvezet� ter�let
    bool f_van[BASIC_SIDES];    // bool f_van[BASIC_SIDES]; van-e itt f�lvezet� �tmenet?
    dbl sum_gfull;              // dbl sum_gfull; automatikus tranziensn�l seg�ti az ellen�ll�sv�ltoz�st figyelj�k
};


//***********************************************************************
struct gcsd_par{
//***********************************************************************
    uns x,y,z;
    dbl ndc_init_I;
    dbl Temp[BASIC_SIDES]; // EXTERN=center, WEST,EAST,...
    bool is_lin,is_no_semi,is_semi_init;
    const real_cell_res * I;
    uns ter;
    dbl x_meret,y_meret,z_meret;
    dbl Ax,Ay,Az;
    const color * cc;
    gcsd_par():is_semi_init(false){}
};


//***********************************************************************
struct footprint{
//***********************************************************************
    uns external_azon;          // uns
    uns x,y,w,h;                // uns
};


//***********************************************************************
struct compact_model{
//***********************************************************************
    tomb<PLString> tnodes;      // tomb<PLString>
    tomb<uns> texternal_azon;   // tomb<uns>
    tomb<footprint> tfootpr;    // tomb<footprint>
    CompactTipus tipus;         // enum CompactTipus{CompactMatrix};, ks��bbi b�v�thet�s�gre
    dsrmatrix *y;               // dsrmatrix*
    drvector *j;                // drvector*
    compact_model():y(NULL),j(NULL){} // = oper�tor k�ne neki, de nincs
};


//***********************************************************************
class drcella;
class dccella;
class qrcella;
class qccella;
struct model;
//***********************************************************************


//***********************************************************************
struct coupled_model{
//***********************************************************************
    PLString fileName;          // PLString
    uns x,y,z;                  // uns
    Oldal oldal;                // Oldal
    compact_model compmod;      // compact_model
    coupled_model():x(0),y(0),z(0),oldal(BOTTOM){}
    uns read(const PLString & path,uns azon_start);// beolvassa a fileName f�jlb�l a kompakt modellt, az azonos�t�k sz�moz�s�t azon_start+1-n�l kezdi, visszaadja a beolvasott azonos�t�k sz�m�t
    void set_externals(uns x0, uns y0, uns z0, drcella ** p, const model & mod)const;
    void set_externals(uns x0, uns y0, uns z0, dccella ** p, const model & mod)const;
    void set_externals(uns x0, uns y0, uns z0, qrcella ** p, const model & mod)const;
    void set_externals(uns x0, uns y0, uns z0, qccella ** p, const model & mod)const;
};


//***********************************************************************
struct z_a_hengerhez{
//***********************************************************************
    dbl ertek;      // dbl, a z ir�ny� vastags�g, ha nem henger, egy�bk�nt nem defini�lt
    bool henger_e;  // bool, ha henger koordin�tarendszer, true
    z_a_hengerhez():ertek(nulla),henger_e(false){}
    dbl get(dbl sugar)const{ return henger_e ? 2*M_PI*sugar : ertek; }
};


//***********************************************************************
inline dbl segedcella::get_ido_arany(dbl T_akt){ // Ha nem kell visszal�pni, akkor 1 (csak hiszter�zises/f�zisv�lt�s esetben kell)
//***********************************************************************
    dbl dT=0.0;
    this->T_akt = T_akt;
    if(!mater->is_his)return egy;
    if( T_elozo < mater->his_T1 && T_akt < mater->his_T1 )
        return egy;
    if( T_elozo > mater->his_T4 && T_akt > mater->his_T4 )
        return egy;
    if(is_push_up)
        dT = T_akt > T_elozo ? T_akt - mater->his_T4 : T_elozo - T_akt;
    else if(is_push_down){
        dT = T_akt > T_elozo ? T_akt - T_elozo : mater->his_T1 - T_akt;
    }
    else{ // dT = T_akt - T_push_2 vagy dT = T_push_1 - T_akt
        dT = T_akt > T_elozo 
            ? T_akt - ( mater->his_T2 + mater->his_delta * feltoltottseg )
            :       + ( mater->his_T1 + mater->his_delta * feltoltottseg ) - T_akt;
    }
    cd megengedett_tullepes = mater->his_delta * his_megengedett_arany;
    if( dT <= megengedett_tullepes )
        return egy;
    cd szamitott_tullepes = mater->his_delta * his_szamitott_arany;
    cd ido_arany = egy - fabs( ( dT - szamitott_tullepes ) / ( T_akt - T_elozo ) );
    if( ido_arany < 0 ){
        throw hiba("segedcella::get_ido_arany","program error: ido_arany < 0 (%g)",ido_arany);
    }
    return ido_arany / 2; // a /2 nem k�ne, gyors�t�s miatt tettem be. rem�lhet�leg nem okoz gondot
}


//***********************************************************************
inline void segedcella::update_his(){
//***********************************************************************
    if (!mater->is_his)return;
    if( T_akt <= mater->his_T1 ){ // hiszter�zis el�tt
        is_push_up = is_push_down = false;
        feltoltottseg = nulla;
    }
    else if( T_akt < mater->his_T1 + ( mater->his_delta ) * feltoltottseg ){ // push down tartom�ny
        is_push_down = true;
        is_push_up = false;
        feltoltottseg = ( T_akt - mater->his_T1 ) * mater->his_inv_delta;
    }
    else if( T_akt < mater->his_T2 + ( mater->his_delta ) * feltoltottseg ){ // a k�t push k�z�tt
        is_push_up = is_push_down = false;
    }
    else if( T_akt < mater->his_T4 ){ // push up tartom�ny
        is_push_down = false;
        is_push_up = true;
        feltoltottseg = ( T_akt - mater->his_T2 ) * mater->his_inv_delta;
    }
    else{ // hiszter�zis ut�n
        is_push_up = is_push_down = false;
        feltoltottseg = egy;
    }
    T_elozo = T_akt;
}
//***********************************************************************

//***********************************************************************
struct model{
//***********************************************************************
    PLString fileName,name;     // PLString
    uns simdb;                  // uns, h�ny szimul�ci�s f�jl tartozik a modellhez
    uns dim;                    // uns, 1, 2 v 3.
    uns x_res,y_res,z_res;      // uns, 1..2048
    bool kerekcoord;            // bool, cartesian: false, sphere v. cylindrical: true
    dbl r_min,r_max;            // dbl, ha kerekcoord==true
    tomb<z_a_hengerhez> x_pit,y_pit,z_pit;  // tomb<z_a_hengerhez>, a henger_e csak z ir�nyban lehet be�ll�tva
    tomb<dbl> x_hossz,y_hossz,z_hossz;// tomb<dbl>, k�tszer hosszabb, mint a pit, minden cella k�zep�nek �s tetej�nek a t�vols�g�t is t�rolja, 2*x a k�z�p, 2*x+1 a tet�
    tomb<bitmap> tbmp;          // tomb<bitmap>
    tomb<segedcella> tseged;    // tomb<segedcella*>, minden r�cselemnek megfelelhet egy elem, jelenleg csak a hiszter�zisesek lesznek foglalva
    vezetes amb_emiss;          // vezetes, a k�rnyezet emisszi�s t�nyez�je, 0..1, default=1
    tomb<material> tmat;        // tomb<material>
    color tcolor[colmax+1];     // color tcolor[colmax]
    dbl A_semi_full;            // dbl, az �sszes f�lvezet� fel�lete
    uns coupled_azon_start;     // 
    tomb<coupled_model> tcoupled;// tomb<coupled_model>
    model() :simdb(0), A_semi_full(nulla){ tcolor[colmax].is = true; tcolor[colmax].tipus = SzinBoundary; amb_emiss.g[0] = amb_emiss.g[1] = amb_emiss.g[2] = egy; }
    // Ide kellenek, mert a f�lvezet�n�l innen k�nnyebb sz�molni, meg a fel�letet is itt ismeri.
    bool isCellExist(uns x,uns y,uns z)const; // a megadott koordin�t�kkal l�tezik-e cella (ha bels� perem, akkor nem)
    bool get_gcsd(const gcsd_par & gp,gcsd & ret, const segedcella * s)const; // T h�m�rs�kletn�l �s I �ramn�l a vezet�s, kapacit�s (, Seebeck, Diss coeff); ter=0 elektromos, ter=1 termikus
    void read(PLString path);   // beolvassa a fileName nev� modelf�jlt

    //***********************************************************************
    void set_to_logitherm(unsigned X_res,unsigned Y_res,unsigned Z_res,double x,double y,double z,
                          double Si_hovez=156.3,double Si_hokapac=1.596e+006){
    //***********************************************************************

        name        = "logitherm";
        simdb       = 1;
        dim         = 3;
        x_res       = X_res;
        y_res       = Y_res;
        z_res       = Z_res;
        kerekcoord  = false;
        r_min       = nulla;
        r_max       = nulla;

        x_pit.resize(x_res);
        for(uns i = 0; i < x_res; i++)
            x_pit[i].ertek = x / x_res;
        
        y_pit.resize(y_res);
        for(uns i = 0; i < y_res; i++)
            y_pit[i].ertek = y / y_res;
        
        z_pit.resize(z_res);
        for(uns i = 0; i < z_res; i++)
            z_pit[i].ertek = z / z_res;

        x_hossz.resize(2 * x_res);
        for(uns i = 0; i < 2 * x_res; i++)
            x_hossz[i] = ( i + 1 ) * 0.5 * x / x_res;
        
        y_hossz.resize(2 * y_res);
        for(uns i = 0; i < 2 * y_res; i++)
            y_hossz[i] = ( i + 1 ) * 0.5 * y / y_res;
        
        z_hossz.resize(2 * z_res);
        for(uns i = 0; i < 2 * z_res; i++)
            z_hossz[i] = ( i + 1 ) * 0.5 * z / z_res;

        tbmp.resize(z_res);
        for(uns i = 0; i < z_res; i++)
            tbmp[i].resize(y_res,x_res); // null�zza is, �s a null�s sz�n lesz a szil�cium

        tmat.resize(1);
        tmat[0].nev     = "Si";
        tmat[0].is_th   = true;
        tmat[0].thvez.g[0] = tmat[0].thvez.g[1] = tmat[0].thvez.g[2]    = Si_hovez;
        tmat[0].Cth.g[0] = tmat[0].Cth.g[1] = tmat[0].Cth.g[2]          = Si_hokapac;

        tcolor[0].is    = true;
        tcolor[0].nev   = "Si";
        tcolor[0].tipus = SzinNormal;
        tcolor[0].pmat  = &tmat[0];
        tcolor[0].terfogat = x * y * z;

        // a bmp sz�neinek ellen�rz�se, t�rfogatsz�m�t�s, tseged felt�lt�se

        tseged.resize(x_res * y_res * z_res);
        uns j4 = 0;
        for (uns j1 = 0;j1<z_res;j1++)
            for (uns j2 = 0;j2<y_res;j2++)
                for (uns j3 = 0;j3<x_res;j3++, j4++) {
                    color & cakt = tcolor[tbmp[j1].getpixel_also(j3, j2)];
                    if (!cakt.is)
                        throw hiba("model::set_to_logitherm", "undefined color (%u) in automaticly generated bitmap", tbmp[j1].getpixel_also(j3, j2));
                    else {
                        cakt.terfogat += x_pit[j3].get(nulla)*y_pit[j2].get(nulla)*z_pit[j1].get(x_hossz[2 * j3]);
                        tseged[j4].mater = cakt.pmat;
                    }
                }

    }
    //***********************************************************************

    //***********************************************************************
    void set_to_logitherm(const std::vector<double> & x_pitch, const std::vector<double> & y_pitch,
        const std::vector<double> & z_pitch, const std::vector<stackedMaterial> & mats,
        const std::vector<unsigned char> & mat_map) {
        //***********************************************************************

        name = "logitherm3D";
        simdb = 1;
        dim = 3;
        x_res = (uns)x_pitch.size();
        y_res = (uns)y_pitch.size();
        z_res = (uns)z_pitch.size();
        kerekcoord = false;
        r_min = nulla;
        r_max = nulla;

        x_pit.resize(x_res);
        for (uns i = 0; i < x_res; i++)
            x_pit[i].ertek = x_pitch[i];

        y_pit.resize(y_res);
        for (uns i = 0; i < y_res; i++)
            y_pit[i].ertek = y_pitch[i];

        z_pit.resize(z_res);
        for (uns i = 0; i < z_res; i++)
            z_pit[i].ertek = z_pitch[i]; // hengern�l nem lesz j�, de akar-e valaki logitermikust hengeren?


        x_hossz.clear();
        x_hossz.add(x_pit[0].get(nulla) * 0.5);
        x_hossz.add(x_pit[0].get(nulla));
        for (u32 i = 1;i<x_pit.size();i++) {
            x_hossz.add(x_hossz.getLast() + x_pit[i].get(nulla) * 0.5);
            x_hossz.add(x_hossz.getLast() + x_pit[i].get(nulla) * 0.5); // ez m�r az el�z� sorban kapotthoz adja
        }

        y_hossz.clear();
        y_hossz.add(y_pit[0].get(nulla) * 0.5);
        y_hossz.add(y_pit[0].get(nulla));
        for (u32 i = 1;i<y_pit.size();i++) {
            y_hossz.add(y_hossz.getLast() + y_pit[i].get(nulla) * 0.5);
            y_hossz.add(y_hossz.getLast() + y_pit[i].get(nulla) * 0.5); // ez m�r az el�z� sorban kapotthoz adja
        }

        z_hossz.clear();
        z_hossz.add(z_pit[0].get(nulla) * 0.5);
        z_hossz.add(z_pit[0].get(nulla));
        for (u32 i = 1;i<z_pit.size();i++) {
            z_hossz.add(z_hossz.getLast() + z_pit[i].get(nulla) * 0.5);
            z_hossz.add(z_hossz.getLast() + z_pit[i].get(nulla) * 0.5); // ez m�r az el�z� sorban kapotthoz adja
        }

        tmat.resize((uns)mats.size());
        for (uns i = 0; i<tmat.size(); i++) {
            tmat[i].nev = mats[i].name.c_str();
            tmat[i].is_th = true;
            tmat[i].thvez.g[0] = tmat[i].thvez.g[1] = tmat[i].thvez.g[2] = mats[i].gTh;
            tmat[i].Cth.g[0] = tmat[i].Cth.g[1] = tmat[i].Cth.g[2] = mats[i].cTh;

            tcolor[i].is = true;
            tcolor[i].nev = mats[i].name.c_str();
            tcolor[i].tipus = SzinNormal;
            tcolor[i].pmat = &tmat[i];
        }

        tbmp.resize(z_res);
        for (uns z = 0; z < z_res; z++) {
            tbmp[z].resize(y_res, x_res);
            for (uns y = 0; y < y_res; y++)
                for (uns x = 0; x < x_res; x++)
                    tbmp[z].setpixel_also(x, y, mat_map[z*x_res*y_res + y*x_res + x]);
        }

//        // t�rfogatsz�m�t�s
//
//        for (uns z = 0;z<z_res;z++)
//            for (uns y = 0;y<y_res;y++)
//                for (uns x = 0;x<x_res;x++)
//                    tcolor[tbmp[z].getpixel_also(x, y)].terfogat += x_pit[x].get(nulla) * y_pit[y].get(nulla) * z_pit[z].get(x_hossz[2 * x]);
//
        // a bmp sz�neinek ellen�rz�se, t�rfogatsz�m�t�s, tseged felt�lt�se

        tseged.resize(x_res * y_res * z_res);
        uns j4 = 0;
        for (uns j1 = 0;j1<z_res;j1++)
            for (uns j2 = 0;j2<y_res;j2++)
                for (uns j3 = 0;j3<x_res;j3++, j4++) {
                    color & cakt = tcolor[tbmp[j1].getpixel_also(j3, j2)];
                    if (!cakt.is)
                        throw hiba("model::set_to_logitherm", "undefined color (%u) in automaticly generated bitmap", tbmp[j1].getpixel_also(j3, j2));
                    else {
                        cakt.terfogat += x_pit[j3].get(nulla)*y_pit[j2].get(nulla)*z_pit[j1].get(x_hossz[2 * j3]);
                        tseged[j4].mater = cakt.pmat;
                    }
                }
    }
    //***********************************************************************
    void clear_tseged(){ for (u32 i = 0; i < tseged.size(); i++) tseged[i].clear(); }
};


//***********************************************************************
struct convection{
//***********************************************************************
    bool is_defined;
    PLString nev;               // PLString
    vezetes radiation;          // vezetes, a sug�rz�sra sz�molt HTC mekkora r�sz�t vegye figyelembe, default=1
    ConvTipus vertical_tipus,lower_tipus,upper_tipus; // ConvTipus
    vezetes vertical_value,lower_value,upper_value;   // vezetes
    irany axis;                 // melyik tengely ment�n forog, X_IRANY vagy Y_IRANY vagy Z_IRANY
    dbl angle;                  // dbl, fokban!
    ConvTipus edge;             // ConvHTC,ConvEdgeWei_H,ConvEdgeWei_I,ConvEdgeWei_HI, ha HTC, akkor nem sz�mol �lt.
    convection():is_defined(false),radiation(egy),vertical_tipus(ConvHTC),lower_tipus(ConvHTC)
        ,upper_tipus(ConvHTC),vertical_value(10.0),lower_value(10.0),upper_value(10.0),axis(X_IRANY),angle(90.0),edge(ConvHTC){}
};


//***********************************************************************
inline dbl air_suruseg(dbl absT){
//***********************************************************************
    return 1000.0 / ( 2.876107*absT - 2.388246 );
}


//***********************************************************************
inline dbl air_fajho(dbl absT){
//***********************************************************************
    return -3.771932e-07*absT*absT*absT + 8.344608e-04*absT*absT - 0.3664997*absT + 1052.207;
}


//***********************************************************************
inline dbl air_hotagulas(dbl absT){
//***********************************************************************
    return egy / ( 1.003248*absT - 2.621933 );
}


//***********************************************************************
inline dbl air_hovezetes(dbl absT){
//***********************************************************************
    return 1e-7*(-0.2754376*absT*absT + 955.6073*absT + 3735.8);
}


//***********************************************************************
inline dbl air_dinamikai_viszkozitas(dbl absT){
//***********************************************************************
//    return 1.846044E-14*absT*absT*absT - 4.651960E-11*absT*absT + 6.964771E-08*absT + 1.105083E-06;
    return 1e-10*(1.319648e-4*absT*absT*absT - 0.4268410*absT*absT + 702.0689*absT + 6498.404);
}


//***********************************************************************
inline dbl air_kinematikai_viszkozitas(dbl absT){
//***********************************************************************
//    return -2.586515E-14*absT*absT*absT + 1.214603E-10*absT*absT + 2.626366E-08*absT - 2.177191E-06;
//    return ((-4.3151665E-14*absT + 1.4495412E-10)*absT + 1.7382972E-08)*absT - 1.3107223E-06;
    return air_dinamikai_viszkozitas(absT)/air_suruseg(absT);
}


//***********************************************************************
inline dbl Grashof(dbl g,dbl Tw,dbl Ta,dbl x){
// Tw: wall temp, Ta: ambient temp, abszol�t h�m, x: karakterisztikus t�v
// g: gravit�ci� �raml�si ir�ny� komponense
//***********************************************************************
    cd Tc=(Tw+Ta)*0.5;
    cd sur=air_suruseg(Tc);
    cd mu=air_dinamikai_viszkozitas(Tc);
    return fabs(g*sur*sur*air_hotagulas(Tc)*(Tw-Ta)*x*x*x/(mu*mu));
}


//***********************************************************************
inline dbl mod_Grashof(dbl g,dbl Tw,dbl Ta,dbl qav, dbl x){
// Tw: wall temp, Ta: ambient temp, abszol�t h�m, x: karakterisztikus t�v
// g: gravit�ci� �raml�si ir�ny� komponense
//***********************************************************************
    cd Tc=(Tw+Ta)*0.5;
    cd sur=air_suruseg(Tc);
    cd mu=air_dinamikai_viszkozitas(Tc);
    cd lamda = air_hovezetes(Tc);
    return fabs(g*sur*sur*air_hotagulas(Tc)*qav*x*x*x*x/(mu*mu*lamda));
}


//***********************************************************************
inline dbl Prandtl(dbl Tw,dbl Ta){
//***********************************************************************
    cd Tc=(Tw+Ta)*0.5;
    return air_fajho(Tc)*air_dinamikai_viszkozitas(Tc)/air_hovezetes(Tc);
}


//***********************************************************************
inline dbl Rayleigh(dbl g,dbl Tw,dbl Ta,dbl x){
//***********************************************************************
    cd Tc=(Tw+Ta)*0.5;
    cd sur=air_suruseg(Tc);
    cd mu=air_dinamikai_viszkozitas(Tc);
    return g*sur*sur*air_fajho(Tc)*air_hotagulas(Tc)*(Tw-Ta)*x*x*x/(mu*air_hovezetes(Tc));
}


//***********************************************************************
inline dbl mod_Rayleigh(dbl g,dbl Tw,dbl Ta,dbl p,dbl A,dbl x){
//***********************************************************************
    cd Tc=(Tw+Ta)*0.5;
    cd q=p/A;
    cd sur=air_suruseg(Tc);
    cd mu=air_dinamikai_viszkozitas(Tc);
    cd lamda=air_hovezetes(Tc);
    return g*sur*sur*air_fajho(Tc)*air_hotagulas(Tc)*q*x*x*x*x/(mu*lamda*lamda);
}


//***********************************************************************
inline dbl Nu_atl_vertical_1(dbl g,dbl Tw,dbl Ta,dbl x){
//***********************************************************************
    cd ertek=0.825+0.387*pow(Rayleigh(g,Tw,Ta,x),egy/6.0)
             /pow(egy+pow(0.492/Prandtl(Tw,Ta),9.0/16.0),8.0/27.0);
    return ertek*ertek;
}


//***********************************************************************
inline dbl Nu_atl_Churchill_C(dbl g,dbl Tw,dbl Ta,dbl x){
//***********************************************************************
    cd fi = egy / pow(egy+pow(0.492/Prandtl(Tw,Ta),9.0/16.0),16.0/9.0); 
    cd Ra = Rayleigh(g,Tw,Ta,x);
    return 0.68+0.67*pow(Ra*fi,0.25)*pow(1+1.6e-8*Ra*fi,egy/12.0);
}



//***********************************************************************
inline dbl Alfa_atl_Mihajev(dbl g,dbl Tw,dbl Ta,dbl x){
//***********************************************************************
    cd Ra=Rayleigh(g,Tw,Ta,x);
    cd Tc=0.5*(Tw+Ta);
    cd lamda=air_hovezetes(Tc);
    cd nu=air_kinematikai_viszkozitas(Tc);
    cd bgprdtnu2=air_hotagulas(Tc)*g*Prandtl(Tw,Ta)*(Tw-Ta)/(nu*nu);
    if(Ra<1e-3){
        return 0.68; // saj�t �tlet, b�rmi j�, ami nem 0, ez a m�ret �gyis elhanyagolhat�
    }
    if(Ra<5e2){
        return 1.18*lamda*pow(bgprdtnu2/(x*x*x*x*x),0.125);
    }
    else if(Ra<2e7){
        return 0.54*lamda*pow(bgprdtnu2/x,0.25);
    }
    else{
        return 0.135*lamda*pow(bgprdtnu2,egy/3);
    }
}


//***********************************************************************
inline dbl LeeC_T(dbl Tw,dbl Ta){
//***********************************************************************
    cd Pr=Prandtl(Tw,Ta);
    return 0.503*pow(Pr,0.25)/pow(egy+pow(0.492/Pr,9.0/16.0),4.0/9.0);
}


//***********************************************************************
inline dbl LeeC_P(dbl Tw,dbl Ta){
//***********************************************************************
    cd Pr=Prandtl(Tw,Ta);
    return pow(Pr*Pr/(4.0+9*sqrt(Pr)+10.0*Pr),0.2);
}


//***********************************************************************
inline uns kettohatvany(uns n){
//***********************************************************************
    for(uns i=0; ; i++)
        if( (1U<<i) >= n )return 1U<<i;
}


//***********************************************************************
struct boundary{
//***********************************************************************
    PeremTipus tipus;           // PeremTipus: PeremOpen,PeremU,PeremR,PeremRU. A nonuniformot most nem implement�lom, majd ha sz�ks�g lesz r�.
    dbl value, value2;          // dbl, a value2 csak conv_temp peremfelt�tel eset�n kap �rt�ket, ez a csatolt h�m�rs�klet
    convection conv;            // convection
    tomb2d<dbl> conv_map;       // tomb2d<dbl>
    const tomb3d<real_cell_res> *Ttomb,*Ptomb; // const tomb3d<real_cell_res> *
    dbl A,TA,P,Agyujt,TAgyujt,Pgyujt,ambiT; // dbl, A: fel�let, TA: h�m�rs�klet*ter�let, P teljes�tm�ny, gyujt: a masina::get_all_matrix az ertek() seg�ts�g�vel ezekbe gy�jti az aktu�lis �rt�keket
    dbl rHTCA,rHTCAgyujt,cHTCA,cHTCAgyujt;
    pthread_mutex_t bou_Mutex;
    //***********************************************************************
    void reset(dbl ambi_T){ 
    //***********************************************************************
        Agyujt=TAgyujt=Pgyujt=rHTCAgyujt=cHTCAgyujt=nulla;
        ambiT=ambi_T;//printf("%g, %g\n",Prandtl(353.15,293.15),Rayleigh(9.81,353.15,293.15,0.6));
//        if(tipus==PeremR)printf("\npconv=%s,Ttomb=%s,Ptomb=%s\t",pconv?"NEMNULL":"NULL",Ttomb?"NEMNULL":"NULL",Ptomb?"NEMNULL":"NULL");
//        if(tipus==PeremR&&conv.is_defined)printf("\nA=%g,TA=%g,P=%g,T=%g,rHTC=%g,cHTC=%g,angle=%g\n",A,TA,P,A!=0?TA/A:0.0,A!=0?rHTCA/A:0.0,A!=0?cHTCA/A:0.0,conv.angle);
    } // a masina::get_all_matrix reseteli az �rt�keket, majd �sszegy�jti, �s 
    //***********************************************************************
    void update(const tomb3d<real_cell_res> *Tt,const tomb3d<real_cell_res> *Pt){
    //***********************************************************************
        Ttomb=Tt; 
        Ptomb=Pt; 
        A=Agyujt; TA=TAgyujt; P=Pgyujt; 
        rHTCA=rHTCAgyujt; cHTCA=cHTCAgyujt;
        reset(ambiT);
    } // az update seg�ts�g�vel a k�vetkez� menetre be�ll�tja
    //***********************************************************************
    dbl RadFlux(const model &mod, cuns irany,dbl Ta,dbl Tf,u32 x, u32 y, u32 z)const{
    //***********************************************************************
        cd Tr=Tf-absT;
        cuns cella_szin=mod.tbmp[z].getpixel_also(x,y);
        const color & cella_color=mod.tcolor[cella_szin];
        const material & mat=*cella_color.pmat;
        cd epszilonA=mod.amb_emiss.get_ertek(irany,Tr,false,nullptr);
        cd epszilonf=mat.emissivity.get_ertek(irany,Tr,false,nullptr);
        cd epszilon = egy/(egy/epszilonA+egy/epszilonf-egy);
        return szigma*epszilon*(Tf*Tf*Tf*Tf-Ta*Ta*Ta*Ta); // fel�leti teljes�tm�nys�r�s�g, a teljes�tm�ny = p*aktA;
    }
    //***********************************************************************
    dbl ertek2()const{ return value2; }
    //***********************************************************************
    dbl ertek(u32 x,u32 y,u32 z,Oldal oldal,bool is_el,const model &mod,cd aktA,bool ujat_szamol)const{
    // aktA az aktu�lis fel�let, hogy ne kelljen annyiszor kisz�molni
    //***********************************************************************
        if(is_el)return value;

        // Gy�jt�s

		pthread_mutex_lock(const_cast<pthread_mutex_t*>(&bou_Mutex)); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        dbl &pAgyujt = const_cast<dbl&>(Agyujt),&rTAgyujt = const_cast<dbl&>(TAgyujt),&rPgyujt = const_cast<dbl&>(Pgyujt);
        dbl aktT = g_min, aktP = g_min; // oszt�sn�l ne legyen gond
        pAgyujt += aktA;
        if(Ttomb)rTAgyujt += (aktT =  Ttomb->getconstref(x,y,z).t[oldal])*aktA;
        if(Ptomb)rPgyujt  += aktP  = -Ptomb->getconstref(x,y,z).t[oldal]; // negat�v ir�nyba folyik, ez�rt -1*-es�t kell venni
		pthread_mutex_unlock(const_cast<pthread_mutex_t*>(&bou_Mutex)); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!

        // Nem sp�ci peremfelt�telek

        if ((tipus != PeremR && tipus != PeremRU) || !conv.is_defined){
            if(conv_map.x_size()>0){
                if(oldal==TOP || oldal==BOTTOM)         return conv_map.getconstref(x,y);
                else if(oldal==NORTH || oldal==SOUTH)   return conv_map.getconstref(x,z);
                else                                    return conv_map.getconstref(y,z);
            }
            else return value;
        }

        // Sp�ci peremfelt�telek

        bool is_x=oldal == WEST   || oldal == EAST;
        bool is_y=oldal == SOUTH  || oldal == NORTH;
        bool is_z=oldal == BOTTOM || oldal == TOP;
        cuns irany = is_x ? 0 : is_y ? 1 : 2 ;
        cd Tr=aktT+ambiT;
        cd Tf=Tr+absT;
        cd Ta=ambiT+absT;

        // sug�rz�s miatti HTC

        cd q_rad=RadFlux(mod,irany,Ta,Tf,x,y,z);
        cd rHTC=conv.radiation.get_ertek(irany,Tr,false,nullptr)*q_rad/aktT;
        const_cast<dbl&>(rHTCAgyujt)+=rHTC*aktA;
            
        // konvekt�v HTC

        // ir�ny �s sz�g meghat�roz�sa

        ::irany merre,meroleges;
        bool is_lentrol;
        dbl szog;

        if(conv.axis==X_IRANY){
            switch(oldal){
                case TOP:       merre = Y_IRANY;    meroleges=X_IRANY;  is_lentrol = true;  szog = conv.angle;        break;
                case BOTTOM:    merre = Y_IRANY;    meroleges=X_IRANY;  is_lentrol = false; szog = conv.angle + 180;  break;
                case NORTH:     merre = Z_IRANY;    meroleges=X_IRANY;  is_lentrol = false; szog = conv.angle + 270;  break;
                case SOUTH:     merre = Z_IRANY;    meroleges=X_IRANY;  is_lentrol = true;  szog = conv.angle + 90;   break;
                case WEST:
                case EAST:      szog = 90;       if( conv.angle<=45 || conv.angle>315 ){ merre = Z_IRANY;    meroleges=Y_IRANY;  is_lentrol = true;  }
                                            else if( conv.angle>135 && conv.angle<=225){ merre = Z_IRANY;    meroleges=Y_IRANY;  is_lentrol = false; }
                                            else if( conv.angle>45  && conv.angle<=135){ merre = Y_IRANY;    meroleges=Z_IRANY;  is_lentrol = true;  }
                                            else if( conv.angle>225 && conv.angle<=315){ merre = Y_IRANY;    meroleges=Z_IRANY;  is_lentrol = false; }
                                            else throw hiba("boundary::ertek","impossible angle (%g)",conv.angle);
            }
        }
        else if(conv.axis==Y_IRANY){
            switch(oldal){
                case TOP:       merre = X_IRANY;    meroleges=Y_IRANY;  is_lentrol = true;  szog = conv.angle;        break;
                case BOTTOM:    merre = X_IRANY;    meroleges=Y_IRANY;  is_lentrol = false; szog = conv.angle + 180;  break;
                case EAST:      merre = Z_IRANY;    meroleges=Y_IRANY;  is_lentrol = false; szog = conv.angle + 270;  break;
                case WEST:      merre = Z_IRANY;    meroleges=Y_IRANY;  is_lentrol = true;  szog = conv.angle + 90;   break;
                case SOUTH:
                case NORTH:     szog = 90;       if( conv.angle<=45 || conv.angle>315 ){ merre = Z_IRANY;    meroleges=X_IRANY;  is_lentrol = true;  }
                                            else if( conv.angle>135 && conv.angle<=225){ merre = Z_IRANY;    meroleges=X_IRANY;  is_lentrol = false; }
                                            else if( conv.angle>45  && conv.angle<=135){ merre = X_IRANY;    meroleges=Z_IRANY;  is_lentrol = true;  }
                                            else if( conv.angle>225 && conv.angle<=315){ merre = X_IRANY;    meroleges=Z_IRANY;  is_lentrol = false; }
                                            else throw hiba("boundary::ertek","impossible angle (%g)",conv.angle);
            }
        }
        else throw hiba("boundary::ertek","impossible axis");
            
        // 0-360� k�z� vissz�k

        if(szog>=360)szog-=360;
        if(szog>=360||szog<0)hiba("boundary::ertek","impossible szog (%g)",szog);

        // 0-180� k�z� vissz�k

        if(szog>180){
            szog = 360 - szog;
            is_lentrol = !is_lentrol;
        }
        
        // Az ir�nynak megfelel� cHTC kisz�m�t�sa

        dbl cHTC=5;
        const tomb<dbl> *L=NULL;
        const tomb<z_a_hengerhez> *W=NULL;
        u32 koord=0;
        if(merre==X_IRANY)      { L = &mod.x_hossz;     W = &mod.x_pit;     koord = x; }
        else if(merre==Y_IRANY) { L = &mod.y_hossz;     W = &mod.y_pit;     koord = y; }
        else                    { L = &mod.z_hossz;     W = &mod.z_pit;     koord = z; }

        if(fabs(TA)>1e-10){ // Ha nem ez az els� iter�ci�
            if( szog>=45 && szog<=135 ){ // f�gg�leges tartom�ny
                switch(conv.vertical_tipus){
                    case ConvHTC: cHTC = conv.vertical_value.get_ertek(irany,Tr,false,nullptr); break;
                    case ConvVertical_1:{ // Cengel, 9-21
                            cd Tw=fabs(TA/A)+Ta; // ha agyors�t� beler�g, negat�vba mehet a h�m�rs�klet, amit nem szeret
                            cd L1  = (*L)[2*koord];
                            cd L0  = (koord==0) ? 0 : (*L)[2*koord-2];
                            cd Nu1 = Nu_atl_vertical_1( 9.81*sin(M_PI*szog/180.0), Tw, Ta, L1 );
                            cd Nu0 = Nu_atl_vertical_1( 9.81*sin(M_PI*szog/180.0), Tw, Ta, L0 );
                            //cd Nu  = ( L1*Nu1 - L0*Nu0) / ( L1-L0 );
                            //cHTC=conv.vertical_value.get_ertek(irany,Tr,false)*Nu*air_hovezetes(0.5*(Tw+Ta))/L1;
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*air_hovezetes(0.5*(Tw+Ta))*(Nu1-Nu0)/(L1-L0);
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvVerticalChurchill_P_1:{ // Churchill (30)
                            cd fi = egy / pow(egy+pow(0.437/Prandtl(Tf,Ta),9.0/16.0),16.0/9.0);
                            cd L1 = (*L)[2*koord];
                            cd Nu=0.563*pow(Rayleigh( 9.81*sin(M_PI*szog/180.0), Tf, Ta, L1)*fi,0.25);
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*Nu*air_hovezetes(0.5*(Tf+Ta))/L1;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvVerticalChurchill_P_2:{ // Churchill (37)
                            cd fi = egy / pow(egy+pow(0.437/Prandtl(Tf,Ta),9.0/16.0),16.0/9.0);
                            cd L1 = (*L)[2*koord];
                            cd Nu=0.631*pow(mod_Rayleigh( 9.81*sin(M_PI*szog/180.0), Tf, Ta, aktP, aktA, L1)*fi,0.2);
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*Nu*air_hovezetes(0.5*(Tf+Ta))/L1;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvVerticalChurchill_T:{ // Churchill (19)
                            cd fi = egy / pow(egy+pow(0.492/Prandtl(Tf,Ta),9.0/16.0),16.0/9.0);
                            cd L1 = (*L)[2*koord];
                            cd Nu=0.503*pow(Rayleigh( 9.81*sin(M_PI*szog/180.0), Tf, Ta, L1)*fi,0.25);
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*Nu*air_hovezetes(0.5*(Tf+Ta))/L1;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvVerticalChurchill_C:{ //Churchill (27)
                            cd Tw=TA/A+Ta;
                            cd L1  = (*L)[2*koord];
                            cd L0  = (koord==0) ? 0 : (*L)[2*koord-2];
                            cd Nu1 = Nu_atl_Churchill_C( 9.81*sin(M_PI*szog/180.0), Tw, Ta, L1 );
                            cd Nu0 = Nu_atl_Churchill_C( 9.81*sin(M_PI*szog/180.0), Tw, Ta, L0 );
                            //cd Nu  = ( L1*Nu1 - L0*Nu0) / ( L1-L0 );
                            //cHTC=conv.vertical_value.get_ertek(irany,Tr,false)*Nu*air_hovezetes(0.5*(Tw+Ta))/L1;
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*air_hovezetes(0.5*(Tw+Ta))*(Nu1-Nu0)/(L1-L0);
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvVerticalMihajev:{
                            cd Tw=TA/A+Ta;
                            cd L1  = (*L)[2*koord];
                            cd L0  = (koord==0) ? 0 : (*L)[2*koord-2];
                            cd alfa1 = Alfa_atl_Mihajev( 9.81*sin(M_PI*szog/180.0), Tw, Ta, L1 );
                            cd alfa0 = Alfa_atl_Mihajev( 9.81*sin(M_PI*szog/180.0), Tw, Ta, L0 );
                            cd alfa  = ( L1*alfa1 - L0*alfa0) / ( L1-L0 );
                            //printf("%g, %g, %g, %g, %g\n",alfa1, alfa0, alfa, Tw, L1);
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*alfa;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvVerticalLee_T:{
                            dbl sum=0, TX=0, Tav=0;
                            dbl Tlast=Ta;
                            dbl dTXlast=0;
                            dbl X=0;
                            if(is_lentrol){
                                X  = (*L)[2*koord];
                                for(u32 i=0; i<=koord; i++){
                                    cd Xi = (i==0) ? 0 : (*L)[2*i-1];
                                    cd pit2 = (*W)[i].get(Xi)*0.5; // A pixel f�lmagass�ga
                                    cd Takt = Ta + Ttomb->getconstref( ((merre==X_IRANY) ? i : x), ((merre==Y_IRANY) ? i : y), ((merre==Z_IRANY) ? i : z) ).t[oldal];
                                    TX += dTXlast;
                                    dTXlast = Takt * pit2;
                                    TX += dTXlast;
                                    Tav = TX / (Xi + pit2);
                                    cd dTwi = Takt - Tlast;
                                    sum += dTwi * egy / pow(egy-pow(Xi/X,9.0/8.0),egy/3.0+egy/8.0);
                                    Tlast = Takt;
                                }
                            }
                            else{
                                X  = L->getLast() - (*L)[2*koord];//j�-e a tartom�ny?
                                for(u32 i=W->size(); i>koord; i--){ // unsigned miatt >=0 nem lehet!
                                    cd Xi = L->getLast() - (*L)[2*i-1];
                                    cd pit2 = (*W)[i-1].get(Xi)*0.5; // A pixel f�lmagass�ga
                                    cd Takt = Ta + Ttomb->getconstref( ((merre==X_IRANY) ? i-1 : x), ((merre==Y_IRANY) ? i-1 : y), ((merre==Z_IRANY) ? i-1 : z) ).t[oldal];
                                    TX += dTXlast;
                                    dTXlast = Takt * pit2;
                                    TX += dTXlast;
                                    Tav = TX / (Xi + pit2);
                                    cd dTwi = Takt - Tlast;
                                    sum += dTwi * egy / pow(egy-pow(Xi/X,9.0/8.0),egy/3.0+egy/8.0);
                                    Tlast = Takt;
                                }
                            }
                            cd Tc = 0.5*(Tav + Ta);
                            cd qwX = LeeC_T(Tav,Ta) * air_hovezetes(Tc) * pow(Grashof(9.81*sin(M_PI*szog/180.0), Tav, Ta, X ),0.25) * sum;
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr) * qwX / ((Tlast-Ta)*X);
                            //if(koord==50)printf("TTT cHTC=%g, qw=%g, Tw=%g\n",cHTC,qwX/X,Tlast-Ta);
                            //if(koord==51)exit(1);
                            //if(cHTC<=0){printf("\n\nkoord=%u\n",koord);cHTC=1;}
                            if(cHTC<=0)cHTC=g_min; // ha negat�vnak hagyjuk, rosszul konverg�l
                            //if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvVerticalLee_P:{
                            dbl sum=0, p=0, Aoszlop=0; 
                            dbl plast=0,Alast=0,qlast=0;
                            dbl dplast=0;
                            dbl X=0;
                            dbl TX=0, Tav=0, dTXlast=0;
                            if(is_lentrol){
                                X  = (*L)[2*koord];
                                for(u32 i=0; i<=koord; i++){
                                    cd Xi = (i==0) ? 0 : (*L)[2*i-1];
                                    cd Aakt = is_x ? (mod.y_pit[(merre==Y_IRANY) ? i : y].get(nulla) * mod.z_pit[(merre==Z_IRANY) ? i : z].get(Xi))
                                                :is_y ? (mod.x_pit[(merre==X_IRANY) ? i : x].get(nulla) * mod.z_pit[(merre==Z_IRANY) ? i : z].get(Xi))
                                                :       (mod.x_pit[(merre==X_IRANY) ? i : x].get(nulla) * mod.y_pit[(merre==Y_IRANY) ? i : y].get(nulla));
                                    cd Takt = Ta + Ttomb->getconstref( ((merre==X_IRANY) ? i : x), ((merre==Y_IRANY) ? i : y), ((merre==Z_IRANY) ? i : z) ).t[oldal];
                                    cd pakt = -Ptomb->getconstref( ((merre==X_IRANY) ? i : x), ((merre==Y_IRANY) ? i : y), ((merre==Z_IRANY) ? i : z) ).t[oldal]
                                                - RadFlux(mod,irany,Ta,Takt,((merre==X_IRANY) ? i : x), ((merre==Y_IRANY) ? i : y), ((merre==Z_IRANY) ? i : z)) * Aakt;
                                        
                                    cd qakt = pakt / Aakt;
                                        
                                    Aoszlop += Alast;
                                    Alast = Aakt * 0.5;
                                    Aoszlop += Alast;

                                    p += dplast;
                                    dplast = pakt * 0.5;
                                    p += dplast;

                                    cd dqwi = qakt - qlast;
                                    sum += dqwi * pow( egy-Xi/X, egy/3.0+0.1 );
                                    plast = pakt;
                                    qlast=qakt;

                                    cd pit2 = (*W)[i].get(Xi)*0.5; // A pixel f�lmagass�ga
                                    TX += dTXlast;
                                    dTXlast = Takt * pit2;
                                    TX += dTXlast;
                                    Tav = TX / (Xi + pit2);
                                }
                            }
                            else{
                                X  = L->getLast() - (*L)[2*koord];
                                for(u32 j=W->size(); j>koord; j--){ // unsigned miatt >=0 nem lehet!
                                    cu32 i = j - 1;
                                    cd Xi = L->getLast() - (*L)[2*i+1];
                                    cd Aakt = is_x ? (mod.y_pit[(merre==Y_IRANY) ? i : y].get(nulla) * mod.z_pit[(merre==Z_IRANY) ? i : z].get(Xi))
                                                :is_y ? (mod.x_pit[(merre==X_IRANY) ? i : x].get(nulla) * mod.z_pit[(merre==Z_IRANY) ? i : z].get(Xi))
                                                :       (mod.x_pit[(merre==X_IRANY) ? i : x].get(nulla) * mod.y_pit[(merre==Y_IRANY) ? i : y].get(nulla));
                                    cd Takt = Ta + Ttomb->getconstref( ((merre==X_IRANY) ? i : x), ((merre==Y_IRANY) ? i : y), ((merre==Z_IRANY) ? i : z) ).t[oldal];
                                    cd pakt = -Ptomb->getconstref( ((merre==X_IRANY) ? i : x), ((merre==Y_IRANY) ? i : y), ((merre==Z_IRANY) ? i : z) ).t[oldal]
                                                - RadFlux(mod,irany,Ta,Takt,((merre==X_IRANY) ? i : x), ((merre==Y_IRANY) ? i : y), ((merre==Z_IRANY) ? i : z)) * Aakt;
                                        
                                    cd qakt = pakt / Aakt;
                                        
                                    Aoszlop += Alast;
                                    Alast = Aakt * 0.5;
                                    Aoszlop += Alast;

                                    p += dplast;
                                    dplast = pakt * 0.5;
                                    p += dplast;

                                    cd dqwi = qakt - qlast;
                                    sum += dqwi * pow( egy-Xi/X, egy/3.0+0.1 );
                                    plast = pakt;
                                    qlast=qakt;

                                    cd pit2 = (*W)[i].get(Xi)*0.5; // A pixel f�lmagass�ga
                                    TX += dTXlast;
                                    dTXlast = Takt * pit2;
                                    TX += dTXlast;
                                    Tav = TX / (Xi + pit2);
                                }
                            }
                            cd Tc = 0.5*(Tav + Ta);
                            cd dTw = X/(LeeC_P(Tav,Ta) * air_hovezetes(Tc) * pow(mod_Grashof(9.81*sin(M_PI*szog/180.0), Tav, Ta, p/Aoszlop, X ),0.2)) * sum;
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr) * qlast / dTw; 
                            //if(koord==50)printf("PPP  cHTC=%g, qw=%g, Tw=%g\n",cHTC,qlast,dTw);
                            //if(koord==51)exit(1);
//if(koord==0)printf("Tav=%g, Ta=%g, X=%g, LeeC_P=%g, mod_grasshof=%g, p=%g, Aoszlop=%g, q=%g, sum=%g\n",Tav,Ta, X, LeeC_P(Tav,Ta), mod_Grashof(9.81*sin(M_PI*szog/180.0), Tav, Ta, p/Aoszlop, X ), p, Aoszlop, p/Aoszlop, sum);
                            //if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvWei:{
                            cd Tw=fabs(TA/A)+Ta;
                            cd L1 = L->getLast();
                            cd Nu=0.565*pow(mod_Rayleigh( 9.81, Tw, Ta, P, A, L1)*sin(M_PI*szog/180.0),0.2);
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*Nu*air_hovezetes(0.5*(Tf+Ta))/L1;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    default: throw hiba("boundary::ertek","not supported free-vertical convection type");
                }
            }
            else if(szog<45){ // felfel� tartom�ny
                switch(conv.upper_tipus){
                    case ConvHTC: cHTC = conv.upper_value.get_ertek(irany,Tr,false,nullptr); break;
                    case ConvUpper_1: { // �tlagost sz�mol, Cengel 9-22
                            cd Tw=fabs(TA/A)+Ta;
                            cd L1=L->getLast();
                            cd Nu = 0.54 * pow(Rayleigh(9.81, Tw, Ta, L1),0.25);
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*Nu*air_hovezetes(0.5*(Tw+Ta))/L1;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvWei:{
                            cd Tw=fabs(TA/A)+Ta;
                            cd L1 = L->getLast();
                            cd Ra=mod_Rayleigh( 9.81, Tw, Ta, P, A, L1);
                            cd Nu= szog<10 ? (0.317+0.645*pow(sin(M_PI*szog/180.0),1.18))*pow(Ra,0.2)
                                            : 0.565*pow(mod_Rayleigh( 9.81, Tw, Ta, P, A, L1)*sin(M_PI*szog/180.0),0.2);
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*Nu*air_hovezetes(0.5*(Tf+Ta))/L1;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    default: throw hiba("boundary::ertek","not supported free-upper convection type (%u)",conv.upper_tipus);
                }
            }
            else{ // lefel� tartom�ny
                switch(conv.lower_tipus){
                    case ConvHTC: cHTC = conv.lower_value.get_ertek(irany,Tr,false,nullptr); break;
                    case ConvLower_1: { // �tlagost sz�mol, Cengel 9-24
                            cd Tw=fabs(TA/A)+Ta;
                            cd L1=L->getLast();
                            cd Nu = 0.27 * pow(Rayleigh(9.81, Tw, Ta, L1),0.25);
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*Nu*air_hovezetes(0.5*(Tw+Ta))/L1;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvYovanovichMin: { // a bounds-1.pdf n�ven fut� cikk (26)-os k�plete
                            cd Tw=fabs(TA/A)+Ta;
                            cd L1=L->getLast();
                            cd Nu = 0.5*(2.257+ 0.382 * pow(Rayleigh(9.81, Tw, Ta, L1),0.25)); // a 0.5 az�rt, mert lower => nem k�ne a 0,5-es szorz�!!
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*Nu*air_hovezetes(0.5*(Tw+Ta))/L1;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvYovanovichMax: { // a bounds-1.pdf n�ven fut� cikk (25)-�s k�plete
                            cd Tw=fabs(TA/A)+Ta;
                            cd L1=L->getLast();
                            cd Nu = 0.5*(3.545+ 0.571 * pow(Rayleigh(9.81, Tw, Ta, L1),0.25)); // a 0.5 az�rt, mert lower => nem k�ne a 0,5-es szorz�!!
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*Nu*air_hovezetes(0.5*(Tw+Ta))/L1;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    case ConvWei:{
                            cd Tw=fabs(TA/A)+Ta;
                            cd L1 = L->getLast();
                            cd Ra=mod_Rayleigh( 9.81, Tw, Ta, P, A, L1);
                            cd Nu= szog<10 ? (0.675/pow(Ra,0.04)+(0.00293*pow(Ra,0.256)+0.158)*sin(M_PI*szog/180.0))*pow(Ra,0.2)
                                            : 0.565*pow(mod_Rayleigh( 9.81, Tw, Ta, P, A, L1)*sin(M_PI*szog/180.0),0.2);
                            cHTC=conv.vertical_value.get_ertek(irany,Tr,false,nullptr)*Nu*air_hovezetes(0.5*(Tf+Ta))/L1;
                            if(cHTC<=0||cHTC>=100)throw hiba("boundary::ertek","cHTC<=0||cHTC>=100 (%g)",cHTC);
                        }
                        break;
                    default: throw hiba("boundary::ertek","not supported free-lower convection type");
                }
            }
        }

//            printf("%u\n",conv.edge);
        if((conv.edge==ConvEdgeWei_HH && (szog==0 || szog==180)) || conv.edge==ConvEdgeWei_H || conv.edge==ConvEdgeWei_HI){
//                printf("*");
            // A ferd�re mer�leges ir�yban
            cd x_per_W = (meroleges==X_IRANY) ? mod.x_hossz[x*2]/mod.x_hossz.getLast() : 
                        ((meroleges==Y_IRANY) ? mod.y_hossz[y*2]/mod.y_hossz.getLast() :
                                                mod.z_hossz[z*2]/mod.z_hossz.getLast());
            if(szog<=135){ // felfel� tekint�
                if(x_per_W<0.5)cHTC *= 0.67 / pow(x_per_W,0.254);
                else cHTC *= 0.67 / pow(egy-x_per_W,0.254);
            }
            else{ // lefel� tekint�
                if(x_per_W<0.5)cHTC *= 0.856 / pow(x_per_W,0.11);
                else cHTC *= 0.856 / pow(egy-x_per_W,0.11);
            }
        }
        if((conv.edge==ConvEdgeWei_HH && (szog==0 || szog==180)) || conv.edge==ConvEdgeWei_I || conv.edge==ConvEdgeWei_HI){
            // A ferde ir�nyban
            cd x_per_W = (*L)[2*koord] / L->getLast();
            if(szog<0.1 && x_per_W<0.5) cHTC *= 0.67  / pow(x_per_W,0.254);
            else if(szog<0.1)           cHTC *= 0.67  / pow(egy-x_per_W,0.254);
            else if(szog<=20)           cHTC *= 0.77  / pow(x_per_W,0.282);
            else if(szog<160)           cHTC *= 0.795 / pow(x_per_W,0.25);
            else if(x_per_W<0.5)        cHTC *= 0.856 / pow(x_per_W,0.11);
            else                        cHTC *= 0.856 / pow(egy-x_per_W,0.11);
        }

        const_cast<dbl&>(cHTCAgyujt)+=cHTC*aktA;

    	pthread_mutex_lock(const_cast<pthread_mutex_t*>(&bou_Mutex)); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		pthread_mutex_unlock(const_cast<pthread_mutex_t*>(&bou_Mutex)); // !!!!!!!!!!!!!!!!!!!!!!!!!!!!!
        return rHTC+cHTC;
    }
    boundary():value(nulla),value2(nulla),Ttomb(NULL),Ptomb(NULL),ambiT(nulla),bou_Mutex(PTHREAD_MUTEX_INITIALIZER){reset(ambiT);update(Ttomb,Ptomb);}
};


//***********************************************************************
struct csomag{
//***********************************************************************
    uns szin;                   // uns
    boundary el[BASIC_SIDES];   // boundary el[BASIC_SIDES];
    boundary th[BASIC_SIDES];   // boundary th[BASIC_SIDES];
    void reset(dbl ambi_T){for(u32 i=0; i<BASIC_SIDES; i++){el[i].reset(ambi_T);th[i].reset(ambi_T);}} // igaz�b�l csak a termikus convection felt�telekn�l van �rtelme
    void update(const tomb3d<real_cell_res> *Tt,const tomb3d<real_cell_res> *Pt){for(u32 i=0; i<BASIC_SIDES; i++){el[i].update(Tt,Pt);th[i].update(Tt,Pt);}} // igaz�b�l csak a termikus convection felt�telekn�l van �rtelme

    // A termikus peremfelt�telek ertek f�ggv�nye gy�jti A, TA �s P �rt�keket
    // Mikrocsatorn�n�l a csatornamodellhez sz�ks�g van ezekre, ez�rt lek�rdezhet� a k�v. fv-ekkel.
    // reset/update �ll�tja

    dbl getA()const{ dbl sum = nulla;  for (uns i = 0; i < BASIC_SIDES; i++)sum += th[i].A;  return sum; }
    dbl getTA()const{ dbl sum = nulla; for (uns i = 0; i < BASIC_SIDES; i++)sum += th[i].TA; return sum; }
    dbl getP()const{ dbl sum = nulla;  for (uns i = 0; i < BASIC_SIDES; i++)sum += th[i].P;  return sum; }
    dbl getT(dbl ambiT)const { cd A = getA(); return A > 0 ? ambiT + getTA() / getA() : 0.0; }
    void set_uchannel_segment(dbl T, dbl Rth){
        cd A = getA();
        cd HTC = A > 0 ? egy / (Rth*A) : 10.0;
        for (uns i = 0; i < BASIC_SIDES; i++){
            th[i].tipus = PeremRU;
            th[i].value = HTC;
            th[i].value2 = T;
        }
        //printf("\nA=%g, Rth=%g, HTC=%g, T=%g\n", A, Rth, HTC, T);
    }

    csomag():szin(colmax){}     
};


//***********************************************************************
struct excitation{
//***********************************************************************
    bool is;                    // bool
    GerjTipus tipus;            // GerjSemmi,GerjU,GerjI
    dbl ertek;                  // dbl
    excitation():is(false){}
};


//***********************************************************************
struct probeT{
//***********************************************************************
    PLString cimke;             // PLString
    Oldal oldal;                // Oldal: WEST=1,EAST=2,SOUTH=3,NORTH=4,BOTTOM=5,TOP=6,CENTER=19
    uns x,y,z;                  // uns, map eset�n x=0..2, ahol 0=x, 1=y, 2=z, az �rt�k pedig y-ba ker�l
    uns x2,y2,z2;               // uns
};


//***********************************************************************
struct excitation_2{
//***********************************************************************
    bool is_el;                 // elektromos vagy termikus
    uns color_index;            // uns
    GerjTipus tipus;            // GerjSemmi,GerjU,GerjI
    dbl ertek;                  // dbl
    excitation_2():color_index(colmax){}
};


//***********************************************************************
struct change_time{
//***********************************************************************
    dbl time;                   // dbl
    dbl timestep;               // dbl, ha 0, akkor az adott id�pontban nem v�ltozik
    tomb<excitation_2> excit;   // tomb<excitation_2>, a megv�ltoz� gerjeszt�sek
    change_time():timestep(nulla){}
};
    

//***********************************************************************
struct time_and_change{
//***********************************************************************
    i32 change_index;           // i32, Ha az id�ponthoz nem tartozik change: -1, egy�bk�nt a ctrl t�mb indexe.
    dbl time;                   // Szimul�ci�s id�pont.
    time_and_change():change_index(-1),time(nulla){}
};

//***********************************************************************
struct analysis{
//***********************************************************************
    PLString nev;               // PLString
    AnalizisTipus tipus;        // AnalizisTipus {AnalDC,AnalNDC,AnalAC,AnalLinTran,AnalLogTran,AnalBode,AnalIdo,AnalCtrl}
    dbl from,to;                // dbl
    uns step;                   // uns
    uns ndc_maxiter;            // uns
    dbl relhiba,ndc_I0;         // dbl
    tomb<change_time> ctrl;     // tomb<change_time>
    tomb<time_and_change> times;// tomb<time_and_change>, controlled anal�zisn�l ebbe t�roljuk el a szimul�ci�s id�pontokat
    
    //****************************************************************
    void fill_times(){          // a times t�mbot felt�lti a be�ll�tott adatok alapj�n
    //****************************************************************
        uns i_ctrl = 0, i_times = 0;
        dbl akt_time = nulla; // csak a l�p�sk�z szerint v�ltozik, a change_time szerint nem
        dbl akt_step = from;
        times.clear();
        time_and_change tc;
        
        for( i_times = 0; akt_time <= to + akt_step / 1000.0; i_times++ ){
            dbl dt = akt_step / 1000.0;
            if( ctrl.size() > i_ctrl && ctrl[i_ctrl].time < akt_time + dt ){
                // Ha a change_time kisebb az akt-n�l, vagy egybeesik vele, majdnem ugyanazt kell csin�lni.
                tc.change_index = i_ctrl;
                tc.time = ctrl[i_ctrl].time;
                times.puffer_add( i_times, tc );
                if(ctrl[i_ctrl].timestep > nulla){
                    akt_step = ctrl[i_ctrl].timestep;
                    akt_time = ctrl[i_ctrl].time;
                }
                if(ctrl[i_ctrl].time >= akt_time - dt) // egybeesik a change_time �s az akt_time
                    akt_time += akt_step;
                 i_ctrl++;
           }
            else{
                tc.change_index = -1;
                tc.time = akt_time;
                times.puffer_add( i_times, tc );
                akt_time += akt_step;
            }
            if (i_times == 0){ // Betesz egy els� szimul�ci�s id�pontot
                tc.change_index = -1;
                tc.time = akt_step*1e-6;
                times.puffer_add(1, tc);
                i_times++;
            }
        }
        times.resize(i_times);
    }
    analysis():step(0),ndc_maxiter(10),from(nulla),to(nulla),relhiba(0.005),ndc_I0(0.1){}
};

struct simulation;

//***********************************************************************
class uchannel{
//***********************************************************************
    friend class Gyuri_uchannel;
    const color * pszin;        // const color, pointer a modell::tcolor t�mb elem�re
    simulation * psim;          // az �t tartalmaz� szimul�ci� c�me
    uns szinindex;              // uns, a modell::tcolor t�mb elem�nek indexe
    uns n;                      // uns, ennyi r�szre van osztva a csatorna
    CsatornaTipus tipus;        // CsatornaTipus: CsatRect, CsatCirc, CsatTrap
    bool is_auto_wall_temp;     // bool
    bool is_reverse;            // bool, a csatorna melyik v�g�n f�junk be?
    dbl flow_rate;              // dbl, l/h
    dbl fixed_wall_temp;        // dbl, if(!is_auto_wall_temp) 
    dbl fluid_temp;             // dbl
    dbl width_bottom, width_top;// dbl
    dbl roughness;              // dbl
    dbl height;                 // dbl
    dbl density, dynamic_visc;  // dbl
    dbl spec_heat, heat_cond;   // dbl
    PLString nev;               // PLString
    Vec3d start, stop;          // Vec3d, a csatorna eleje �s v�ge [m], m�r figyelembe van v�ve is_reverse
    dbl length;                 // dbl
public:
    void init1(simulation *psim, uns szinindex, const color *pszin){ this->psim = psim; this->szinindex = szinindex; this->pszin = pszin; }
    void init2(PLString path, PLString fajlnev, const PLString &cimke);
    void read(PLString path, PLString fajlnev);
    void set_start_stop_length();
    void set_uchannel_in_bmp();
    const PLString & getLabel()const{
        if (pszin == nullptr)
            throw hiba("uchannel::getLabel", "pszin==nullptr (using uninitialized object)");
        return pszin->nev;
    }
    uchannel() :pszin(nullptr), szinindex(colmax), n(8), tipus(CsatTrap), is_auto_wall_temp(true), is_reverse(false), 
        flow_rate(1.0), fixed_wall_temp(60.0), fluid_temp(nulla), width_bottom(250e-6), width_top(350e-6), height(67e-6),
        density(1.1614), dynamic_visc(1.84e-5), spec_heat(1005.0), heat_cond(0.0261), nev("undefined-channel-label"),
        length(nulla), roughness(1.0e-6){}
};


//***********************************************************************
class Gyuri_uchannel{
    //***********************************************************************
    struct Rladder{
        double resistance_along;
        double resistance_cross;
        double heat_exchange;
        double Twall;
        double Tout;
    };

    struct Segment{
        double mass_flow_rate;
        double whole_area;
        double Nusselt_number;
        double local_heat_transfer_coeff;
    };

    struct Channel{
        //Channel geometries and properties
        double length;
        double height;
        double width_top;
        double width_bottom;
        double roughness;
        enum geometry { rect, circ, trap } cross_section_type; // 0 Rectangular, 1 circle, 2 trapezoid, etc.

        //Fluid properties, mechanical & thermal parameters
//        PLString fluid_mat;
        double density;
        double specific_heat; // J/(Kg*K)
        double heat_conductivity;
        double dynamic_viscosity;
        double fluid_inlet_temperature;

        //Flow properties
        double mass_flow_rate;
        double volumetric_flow_rate;
        double velocity_of_flow;
        enum type_of_flow { lam, turb, mixed } laminar_or_turbulent; 

        //Calculated values
        double area;
        double perimeter;
        double side_ratio;
        double hydraulic_diameter; //4*A/P
        double Prandtl_number;
        double Reynolds_number;
        double friction_factor;
        double head_loss;
        double press_drop;
        double press_drop_dV;
        double average_Nusselt_number;
        double global_heat_transfer_coefficient;
        double global_thermal_conductivity;

        Segment * segments;
        Rladder * stages;
        Channel() :segments(nullptr), stages(nullptr){}
        ~Channel(){ delete[] segments; delete[] stages; }
    };

    //static double nusselt_calculation(enum Channel::geometry shape, double dynamic_diameter, double Reynolds_number, double Prandtl_number, double Length);
    static double nusselt_calculation(struct Channel * chx, double calculated_length);
    static int Local_Parameters_calculation(struct Channel * chx, uns segment_number);
    static int Rladder_stage_calculation(struct Channel * chx, uns segment_number, const uchannel &ucha);
    static double Rladder_to_Spice(struct Channel * chx, FILE * spice_file, FILE * fu, uns ch_number, uns wished_segment_number, uchannel &ucha);
    static int Thermal_prop_calculation(Channel * chx, double global_temp, FILE *fu);
    static int Hydrodynamic_calculation(Channel * chx, double vol_flow_rate);
public:
    static int main(uchannel & uch);
    static int main_old(uchannel & uch);
};


/*
//***********************************************************************
struct analparancs{
//***********************************************************************
    bool continued;             // bool, csak az inhom. �ramokat sz�molja �jra, a m�trixokat nem
    dbl t0,dt;                  // dbl
};
*/


//***********************************************************************
class powermap {
//***********************************************************************
    simulation *psim;
    bool van_e;

    enum Hol { top, volume };
    Hol hol;

    bool is_exact, is_stop;
    u32 x, y, z; // u32, resolution
    tomb<dbl> x_pitch, y_pitch, z_pitch;

    struct map_t {
        double t;
        tomb3d<dbl> pmap;
        void read(srfajl &fajl, uns &sor, u32 x, u32 y, u32 z);
    };
    tomb<map_t> tombok;

public:
    void init(simulation *psim) { this->psim = psim; }
    void read(PLString path, PLString fajlnev);
    bool is_exists()const { return van_e; }
    void rescale() { // a beolvasott felbont�s� mapet �tsk�l�zza a modellben megadott felbont�shoz
        if (!is_exact)
            throw hiba("powermap::rescale", "not exact powermap is not supported"); 
    }
    void get_map(dbl t, dbl * map); // t id�pontban �rv�nyes disszip�ci�t�rk�pet adja (Wattban, nem s�r�s�gben)
    powermap() :psim(nullptr), van_e(false), hol(top), is_exact(false), is_stop(false),
        x(1), y(1), z(1) {}
};


//***********************************************************************
struct auto_transi {
//***********************************************************************
    bool is_V, is_T, is_no_plus_step_data;
    dbl V_max_rel, V_min_dt;
    dbl T_max_rel, T_min_dt;
    uns V_max_plus_steps, T_max_plus_steps; // ennyi plusz sikeres l�p�st enged k�t el�re megadott id�pont k�z�tt; ha 0, b�rmennyit
    auto_transi() :is_V(false), is_T(false), is_no_plus_step_data(false), V_max_plus_steps(0), T_max_plus_steps(0) {}
};


class masina;
#include <fstream>
#include <iostream>
#include <string>

//***********************************************************************
struct simulation{
//***********************************************************************
    enum autotr_state{atr_normal, atr_auto, atr_auto_fixpont, atr_hisz_back};
    PLString fileName,name;     // PLString
    model * pmodel;             // model * pmodel;
    MezoTipus mezo;             // MezoTipus mezo: FieldEl,FieldTherm,FieldElTherm
    bool is_lin,is_no_semi,is_no_seebeck,is_no_peltier,is_peltier_center,is_no_thomson,is_no_joule,is_no_accelerator,is_fim_txt; // bool
	bool is_vesszo,is_nofim,aramot,is_always_quad,is_always_strassen_mul;	// bool
    uns valostipus;             // uns: 0: double, 1: quad_float; elthermn�l: 1: el=quad, 2: th=quad, 3: mindkett� quad
    uns el_nonlin_subiter;      // uns, default 1
    uns ndc_miniter;            // uns, default 0
    uns cpu_threads;            // uns
    uns hdd;                    // uns
    uns optimize;               // uns, egyel�re nem defini�lt, hogy mire akarom haszn�lni
    uns FIM_res_xy,FIM_res_z;   // uns, FIM k�nyszer�t�se adott felbont�sra (nem lehet kisebb, mint a t�nyleges)
    uns FIM_diff_x, FIM_diff_y, FIM_diff_z; // uns, eltol�s a FIM f�jlon bel�l.
    tomb<convection> tconv;     // tomb<convection> tconv;
    csomag normalperem;         // csomag, a sz�nt nem haszn�ljuk
    tomb<csomag> tinner;        // tomb<csomag>, innerindexszel indexelni!!!
    uns innerindex[colmax];     // uns[colmax], seg�dt�mb a tinner keres�shez
    powermap pmap;              // powermap, k�ls� disszip�ci�t�rk�p alkalmaz�sa eset�n
    tomb<dbl> prev_T_map, prev_U_map, akt_T_map, akt_U_map; // cellak�z�ppontok h�m�rs�klete/fesz�lts�ge az el�z� �s az aktu�lis tranziens/controlled l�p�s ut�n, az automatikus l�ptet�shez haszn�ljuk. A run_transi/run_controlled resize-zolja
    auto_transi auto_tra;       // auto_transi, automatikus tranziens l�p�sk�z param�terei
    tomb<uchannel> tucha;       // tomb<uchannel>, a mikrocsatorn�k param�terei
    tomb<csomag> tucha_perem[colmax]; // tomb<csomag> tucha_perem[colmax], minden uchannel sz�nhez van csomagt�mb, egy t�mb annyi elem�, ah�ny darabra osztott a csatorna
    excitation texcitE[colmax]; // excitation texcitE[colmax], elektromos gerj
    tomb<excitation> mulE[colmax]; // tomb<excitation> mulE[colmax], ha t�bb gerjeszt�s van
    excitation texcitT[colmax]; // excitation texcitT[colmax], h� gerj
    tomb<excitation> mulT[colmax]; // tomb<excitation> mulT[colmax], ha t�bb gerjeszt�s van
    uns db_temp;                // uns, beolvas�skor haszn�lt ideiglenes v�ltoz�
    uns index_temp[4];          // uns[4], beolvas�skor haszn�lt ideiglenes v�ltoz�
    dbl ambiT;                  // dbl
    tomb<dbl> mulAmbiT;         // tomb<dbl>, ha t�bb k�ls� h�m�rs�klet van megadva
    tomb<probeT> tproV;         // tomb<probeT>
    tomb<probeT> tproT;         // tomb<probeT>
    tomb<probeT> tproC;         // tomb<probeT>, �ram probe, elektromos �s h��ramra egyar�nt
    tomb<probeT> tproM;         // tomb<probeT>, map
    tomb<probeT> tproS;         // tomb<probeT>, section
    tomb<analysis> tanal;       // tomb<analysis> tanal;
    simulation():pmodel(NULL),is_lin(false),is_no_semi(false),is_fim_txt(false),is_no_seebeck(false),is_no_peltier(false),
        is_peltier_center(false),is_no_thomson(false),is_no_joule(false),is_no_accelerator(false),el_nonlin_subiter(1),ndc_miniter(0),valostipus(0),is_always_strassen_mul(false),
        cpu_threads(16),hdd(0),optimize(0),ambiT(nulla),is_vesszo(false),is_nofim(false),aramot(true),is_always_quad(false),
        FIM_res_xy(0),FIM_res_z(0), FIM_diff_x(0),FIM_diff_y(0),FIM_diff_z(0){logitherm_interface.set_sim(*this);}
    void read(PLString path);   // beolvassa a fileName nev� szimul�ci�s f�jlt
    void run_dc(const PLString & FileName,uns anal_index);
    void run_ndc(const PLString & FileName,uns anal_index);
    void run_ac(const PLString & FileName,uns anal_index,dbl f,uns tipus,uns analstep,bool is_timeconst,dcomplex s,uns stepdb);
    void run_transi(const PLString & FileName,uns anal_index,AnalizisTipus tipus,dbl from, dbl to, dbl step);
    void run_bode(const PLString & FileName,uns anal_index,dbl from, dbl to, dbl step);
    void run_timeconst(const PLString & FileName,uns anal_index,dbl from, dbl to, dbl step);
    void run_controlled(const PLString & FileName,uns anal_index);
    void run(const PLString & path, bool is_uj_model);
    const boundary * get_perem(uns x, uns y, uns z, Oldal oldal, bool is_el)const;
    const csomag & get_inner_perem(uns x, uns y, uns z)const;
    //***********************************************************************
    void bou_reset(dbl ambi_T){
    //***********************************************************************
        normalperem.reset(ambi_T);
        for(u32 i=0;i<tinner.size();i++)
            tinner[i].reset(ambi_T);
        for (u32 i = 0; i < colmax; i++)
            for (uns j = 0; j < tucha_perem[i].size(); j++)
                tucha_perem[i][j].reset(ambi_T);
    }
    //***********************************************************************
    void bou_update(const tomb3d<real_cell_res> *Tt,const tomb3d<real_cell_res> *Pt){
    //***********************************************************************
        normalperem.update(Tt,Pt);
        for(u32 i=0;i<tinner.size();i++)
            tinner[i].update(Tt,Pt);
        for (u32 i = 0; i < colmax; i++)
            for (uns j = 0; j < tucha_perem[i].size(); j++)
                tucha_perem[i][j].update(Tt,Pt);
    }
    //***********************************************************************
    void uchannel_update(){
    //***********************************************************************
        for (uns i = 0; i < tucha.size(); i++)
            Gyuri_uchannel::main(tucha[i]);
    }
    //***********************************************************************
    bool is_auto_back_step_needed_T(dbl akt_dt)const {
    //***********************************************************************
        if (!auto_tra.is_T || akt_dt <= auto_tra.T_min_dt)
            return false;
        for (uns i = 0; i < prev_T_map.size(); i++)
            if (fabs((akt_T_map[i] - prev_T_map[i]) / (fabs(prev_T_map[i]) + 1.0e-6))>auto_tra.T_max_rel) {
                //printf("\n**** Hiba. i=%u akt_T=%g prev_T=%g ****\n", i, akt_T_map[i], prev_T_map[i]);
                return true;
            }
        return false;
    }
    //***********************************************************************
    bool is_auto_back_step_needed_U(dbl akt_dt)const {
    //***********************************************************************
        if (!auto_tra.is_V || akt_dt <= auto_tra.V_min_dt)
            return false;
        for (uns i = 0; i < prev_U_map.size(); i++) 
            if (fabs((akt_U_map[i] - prev_U_map[i]) / (fabs(prev_U_map[i]) + 1.0e-6))>auto_tra.V_max_rel){
                //printf("\n**** Hiba. i=%u akt_U=%g prev_U=%g ****\n", i, akt_U_map[i], prev_U_map[i]);
                return true;
            }
        return false;
    }
    //***********************************************************************
    bool is_auto_back_step_needed(dbl akt_dt)const {
    //***********************************************************************
        return is_auto_back_step_needed_T(akt_dt) || is_auto_back_step_needed_U(akt_dt);
    }

    //***********************************************************************
    void set_to_logitherm(model * pm,PeremTipus Wt,double Wv,PeremTipus Et,double Ev,PeremTipus St,double Sv,
                                     PeremTipus Nt,double Nv,PeremTipus Bt,double Bv,PeremTipus Tt,double Tv,
                                     double ambient_temp, double From, bool linear){
    //***********************************************************************
        name                        = "logitherm";
        pmodel                      = pm;
        mezo                        = FieldTherm;
        is_lin                      = linear; //2017.03.07. 
        valostipus                  = 0;
        cpu_threads                 = 8;
        normalperem.szin            = 0;
        normalperem.th[WEST ].tipus = Wt;
        normalperem.th[WEST ].value = Wv;
        normalperem.th[EAST ].tipus = Et;
        normalperem.th[EAST ].value = Ev;
        normalperem.th[SOUTH].tipus = St;
        normalperem.th[SOUTH].value = Sv;
        normalperem.th[NORTH].tipus = Nt;
        normalperem.th[NORTH].value = Nv;
        normalperem.th[BOTTOM].tipus= Bt;
        normalperem.th[BOTTOM].value= Bv;
        normalperem.th[TOP  ].tipus = Tt;
        normalperem.th[TOP  ].value = Tv;
        ambiT                       = ambient_temp;
        tanal.resize(1);
        tanal[0].tipus              = AnalLinTran;
        tanal[0].from               = From;
    }

    void set_to_logitherm(const char* path, const char* UchannelFileName, model * pm,PeremTipus Wt,double Wv,PeremTipus Et,double Ev,PeremTipus St,double Sv,
                                     PeremTipus Nt,double Nv,PeremTipus Bt,double Bv,PeremTipus Tt,double Tv,
                                     double ambient_temp, double From, bool linear){
    //***********************************************************************
        name                        = "logitherm";
        pmodel                      = pm;
        mezo                        = FieldTherm;
        is_lin                      = false; //2017.03.07. true-ra raktam, igy gyorsabb, de a mikrocsatorna nem jo
        valostipus                  = 0;
        cpu_threads                 = 8;
        normalperem.szin            = 0;
        normalperem.th[WEST ].tipus = Wt;
        normalperem.th[WEST ].value = Wv;
        normalperem.th[EAST ].tipus = Et;
        normalperem.th[EAST ].value = Ev;
        normalperem.th[SOUTH].tipus = St;
        normalperem.th[SOUTH].value = Sv;
        normalperem.th[NORTH].tipus = Nt;
        normalperem.th[NORTH].value = Nv;
        normalperem.th[BOTTOM].tipus= Bt;
        normalperem.th[BOTTOM].value= Bv;
        normalperem.th[TOP  ].tipus = Tt;
        normalperem.th[TOP  ].value = Tv;
        ambiT                       = ambient_temp;
        tanal.resize(1);
        tanal[0].tipus              = AnalLinTran;
        tanal[0].from               = From;


        /** mikrocsatorna inicializalas, simulation::read()-bol kopiztam ide **/
        uns uchadb = 0, ix;
        for (ix = 0; ix < colmax; ix++)
            if (pmodel->tcolor[ix].is && pmodel->tcolor[ix].tipus == SzinUchannel)
                uchadb++;

        tucha.resize(uchadb);

        for (ix = uchadb = 0; ix < colmax; ix++)
            if (pmodel->tcolor[ix].is && pmodel->tcolor[ix].tipus == SzinUchannel){
                tucha[uchadb].init1(this, ix, &pmodel->tcolor[ix]);
                uchadb++;
            }

        //      uchannel f�jl nev�nek beolvas�sa �s tucha t�mb elemeinek inicializ�l�sa

        std::ifstream uchannelfile(UchannelFileName);
        std::string label, initfilename;
        if(uchannelfile.is_open())
        {
            uchadb = 0;
            while(uchannelfile >> label >> initfilename)
            {
                PLString cimke = label.c_str();
                uns j;
                for (j = 0; j < tucha.size() && tucha[j].getLabel() != cimke; j++);
                if (j == tucha.size()) throw hiba("simulation::set_to_logitherm", "unknown uchannel label (%s) in %s", cimke.c_str(), UchannelFileName);
                tucha[j].init2(path, PLString(initfilename.c_str()), cimke);
                uchadb++;
            }
            if(!uchannelfile.eof()) throw hiba("simulation::set_to_logitherm", "unexpected end of file %s", UchannelFileName);
            if (uchadb != tucha.size()) throw hiba("simulation::set_to_logitherm", "missing uchannel definition (%u!=%u) in %s", uchadb, tucha.size(), UchannelFileName);
                
        }
        /** mikrocsatorna inicializalas, simulation::read()-bol kopiztam ide **/
    }

    //***********************************************************************
    struct Logitherm_interface{
    //***********************************************************************
        simulation *simm;
        masina *mass;
        uns aktstep;
        dbl akt_t, dt;
        
        Logitherm_interface():simm(NULL),mass(NULL),aktstep(0),akt_t(nulla), dt(nulla){}
        ~Logitherm_interface();

        void set_sim(simulation &sim){ simm = &sim; }
        void first_step(uns anal_index, dbl from,const double * p_map,double * T_map);
        void next_step(const double * p_map,double * T_map);
        void next_step(const double * p_map,double * T_map, double timestep);
    };

    //***********************************************************************
    Logitherm_interface logitherm_interface;
    //***********************************************************************

};


//***********************************************************************
class apa{
//***********************************************************************
    PLString path,projFile;     // projFile tartalmazza az �tvonalat is
    tomb<model> tmodels;        // tomb<model> tmodels;
    tomb<simulation> tsim;      // tomb<simulation> tsim;
    void open3d();
public:
    apa(const char * ProjectFile);

    /* LogiTherm-hez, struktura file-bol, a tobbi a megszokott modon parameterlistan **/
    apa(const char* path, const char* ModelFile, const char* UchannelFile, PeremTipus Wt,double Wv,PeremTipus Et,double Ev,PeremTipus St,double Sv,PeremTipus Nt,double Nv,
        PeremTipus Bt,double Bv,PeremTipus Tt,double Tv,double ambient_temp, double time_step, bool linear, const double * p_map,double * T_map)
    {
        tmodels.resize(1);
        tmodels[0].fileName = ModelFile;
        tmodels[0].read(path);

        //itt kene a uchannel fajlt is beolvasni
        tsim.resize(1);
        if(UchannelFile == nullptr)
        {
            tsim[0].set_to_logitherm(&tmodels[0],Wt,Wv,Et,Ev,St,Sv,Nt,Nv,Bt,Bv,Tt,Tv,ambient_temp,time_step, linear);
        }
        else
        {
            tsim[0].set_to_logitherm(path, UchannelFile, &tmodels[0],Wt,Wv,Et,Ev,St,Sv,Nt,Nv,Bt,Bv,Tt,Tv,ambient_temp,time_step, linear);
        }

        tsim[0].logitherm_interface.first_step(0,time_step,p_map,T_map);
    }

    //***********************************************************************
    apa(unsigned X_res,unsigned Y_res,unsigned Z_res,double x,double y,double z,double Si_hovez,double Si_hokapac,
        PeremTipus Wt,double Wv,PeremTipus Et,double Ev,PeremTipus St,double Sv,PeremTipus Nt,double Nv,
        PeremTipus Bt,double Bv,PeremTipus Tt,double Tv,double ambient_temp, double time_step, bool linear, const double * p_map,double * T_map){ // Logithermhez
    //***********************************************************************
        
        tmodels.resize(1);
        tmodels[0].set_to_logitherm(X_res, Y_res, Z_res, x, y, z, Si_hovez, Si_hokapac);

        tsim.resize(1);
        tsim[0].set_to_logitherm(&tmodels[0],Wt,Wv,Et,Ev,St,Sv,Nt,Nv,Bt,Bv,Tt,Tv,ambient_temp,time_step, linear);

        tsim[0].logitherm_interface.first_step(0,time_step,p_map,T_map);
    }
    //***********************************************************************

    //***********************************************************************
    apa(const std::vector<double> & x_pitch, const std::vector<double> & y_pitch, const std::vector<double> & z_pitch,
        const std::vector<stackedMaterial> & mats, const std::vector<unsigned char> & mat_map,
        PeremTipus Wt, double Wv, PeremTipus Et, double Ev, PeremTipus St, double Sv, PeremTipus Nt, double Nv,
        PeremTipus Bt, double Bv, PeremTipus Tt, double Tv, double ambient_temp, double time_step, bool linear, const double * p_map, double * T_map) { // Logithermhez
    //***********************************************************************

        tmodels.resize(1);
        tmodels[0].set_to_logitherm(x_pitch, y_pitch, z_pitch, mats, mat_map);

        tsim.resize(1);
        tsim[0].set_to_logitherm(&tmodels[0], Wt, Wv, Et, Ev, St, Sv, Nt, Nv, Bt, Bv, Tt, Tv, ambient_temp, time_step, linear);

        tsim[0].logitherm_interface.first_step(0, time_step, p_map, T_map);
    }
    //***********************************************************************

    //***********************************************************************
    void logitherm_next_step(const double * p_map,double * T_map){
    //***********************************************************************
        tsim[0].logitherm_interface.next_step(p_map,T_map);
    }

    //***********************************************************************
    void logitherm_next_step(const double * p_map,double * T_map, double timestep){
    //***********************************************************************
        tsim[0].logitherm_interface.next_step(p_map,T_map, timestep);
    }

    //***********************************************************************
    void run(){
    //***********************************************************************
        if(ConsoleText){
            printf("\nVector SUNRED 3 project started\n\n* project:    %s\n",projFile.c_str());
        }
        resetAnalLevel(4);
        setAnalStepDb(4,tsim.size());
        for(uns i=0;i<tsim.size();i++){
            tsim[i].run(path, i==0 || tsim[i].pmodel != tsim[i-1].pmodel);
            setAnalKeszStep(4,i+1);
        }
    }
    //***********************************************************************
};


#endif
