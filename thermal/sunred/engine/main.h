#ifndef _MAIN_H_
#define _MAIN_H_

#include "tipusok.h"
#include "apa.h"
#include "gfunc.h"

extern bool GlobalUseTmp; //ezentúl itt kell állítani
extern cd STRASSEN_INV_DEFAULT;
extern cd STRASSEN_MUL_DEFAULT;
extern cd MUL_DEFAULT;
extern dbl STRASSEN_INV_LIMIT;	// ennél nagyobb, kettõvel osztható oldalhisszúságú mátrixokat strassen algoritmussal invertál
extern dbl STRASSEN_MUL_LIMIT;	// ennél nagyobb, kettõvel osztható oldalhisszúságú mátrixokat strassen algoritmussal szoroz
extern dbl MUL_LIMIT;	                    // ennél nagyobb, kettõvel osztható oldalhisszúságú mátrixokat több szálon szoroz
//***********************************************************************


//***********************************************************************
extern char hibaUzenet[1024];
const char * getHiba();
//***********************************************************************


extern apa *logitherm_apa;

void logitherm_init(unsigned X_res,unsigned Y_res,unsigned Z_res,double x,double y,double z,double Si_hovez,double Si_hokapac,
        PeremTipus Wt,double Wv,PeremTipus Et,double Ev,PeremTipus St,double Sv,PeremTipus Nt,double Nv,
        PeremTipus Bt,double Bv,PeremTipus Tt,double Tv,double ambient_temp, double time_step, const double * p_map,double * T_map);

void logitherm_step(const double * p_map,double * T_map);
void logitherm_destruct();

class stackedInterface{
    std::vector<double> x_pitch, y_pitch, z_pitch;
    std::vector<stackedMaterial> mats;
    std::vector<unsigned char> mat_map;
public:
    stackedInterface(size_t x,size_t y, size_t z,size_t material_num)
        :x_pitch(x,0.0),y_pitch(y,3.14),z_pitch(z,0.0),mats(material_num),mat_map(x*y*z,0){}
    
    // méretek beállítása
    void set_x_pitch(size_t i, double value){x_pitch[i] = value;}
    void set_y_pitch(size_t i, double value){y_pitch[i] = value;}
    void set_z_pitch(size_t i, double value){z_pitch[i] = value;}
    
    // anyagok beállítása
    void set_mat(size_t i, const char *name="Si", double gth=156.3, double cth=1.596e+006){
        mats[i].name = name;
        mats[i].gTh = gth;
        mats[i].cTh = cth;
    }
    
    // teljes réteg kitöltése egy anyaggal
    void set_layer(size_t i,unsigned char material_num){
        // a material_num az anyag indexe a mats tömbben
        // i a réteg indexe, i=0 a legalsó, i=z_pitch.size()-1 a legfelsõ réteg
        size_t min= i   *x_pitch.size()*y_pitch.size();
        size_t max=(i+1)*x_pitch.size()*y_pitch.size();
        for(size_t index=min; index<max; index++)
            mat_map[index] = material_num;
    }

    // adott koordinátájú kocka kitöltése adott indexû anyaggal
    void set_elem(size_t x, size_t y, size_t z, unsigned char material_num){
        mat_map[ x + y*x_pitch.size() + z*x_pitch.size()*y_pitch.size() ] = material_num;
    }
    
    apa* new_apa(PeremTipus Wt,double Wv,PeremTipus Et,double Ev,PeremTipus St,double Sv,PeremTipus Nt,double Nv,
                 PeremTipus Bt,double Bv,PeremTipus Tt,double Tv,double ambient_temp, double time_step, bool linear,
                 const double * p_map,double * T_map){
        return new apa(x_pitch,y_pitch,z_pitch,mats,mat_map,Wt,Wv,Et,Ev,St,Sv,Nt,Nv,Bt,Bv,Tt,Tv,ambient_temp,time_step, linear,p_map,T_map);
    }
};

#endif //_MAIN_H_