//***********************************************************************
// Vector SUNRED 2 matrix class header
// Creation date:	2008. 03. 14.
// Creator:			Pohl L�szl�
//***********************************************************************


//***********************************************************************
#ifndef VSUN2_QMATRIX_HEADER
#define	VSUN2_QMATRIX_HEADER
//***********************************************************************


//***********************************************************************
#include "tipusok.h"
#include "gfunc.h"
//***********************************************************************

//***********************************************************************
class qdinmem{ // NEM FIBERSAFE!!!
//***********************************************************************
	quad_float * t[V_DIN_MAX_BLOKK]; // itt vannak a t�nyleges adatok
	u32 e[V_DIN_MAX_BLOKK]; // a blokkon h�ny eleme van bet�ltve
	u32 g[V_DIN_MAX_BLOKK]; // a blokkban most h�ny m�trix van (free-n�l cs�kken)
	u32 n,akt; // aktu�lis blokk
	void init(cu32 i){t[i]=new quad_float[EGYMEGA];e[i]=0;g[i]=0;}
	void __free(cu32 i){delete[]t[i];t[i]=NULL;e[i]=0;}
	//***********************************************************************
	void setakt(cu32 db){
	//***********************************************************************
		for(;akt<n&&db+e[akt]>EGYMEGA;akt++);
		if(akt>=n)for(akt=0;akt<n&&db+e[akt]>EGYMEGA;akt++);
		if(akt>=n){
			n=akt+1;
			init(akt);
		}
		else if(t[akt]==NULL)init(akt);
	}
public:
	//***********************************************************************
	qdinmem():n(1),akt(0){init(0);}
	~qdinmem(){for(u32 i=0;i<n;i++)__free(i);}
	//***********************************************************************
	quad_float * alloc(cu32 db,u32 & azonosito){
	//***********************************************************************
#ifdef vsundebugmode
		if(db>EGYMEGA)throw hiba("","qdinmem::alloc => db>EGYMEGA => %lu>%lu",db,EGYMEGA);
		if(n>=V_DIN_MAX_BLOKK-1)throw hiba("","qdinmem::alloc => n>=V_DIN_MAX_BLOKK-1 => %lu>%lu",n,V_DIN_MAX_BLOKK-1);
#endif
		if(db==0){azonosito=V_DIN_MAX_BLOKK;return NULL;} // ilyenkor az azonos�t�t nem �ll�tjuk!!!
		if(db+e[akt]>EGYMEGA)setakt(db);// �res helyet keres�nk
		azonosito=akt;
		g[akt]++;
		cu32 temp=e[akt];
		e[akt]+=db;
		return t[akt]+temp;
	}
	//***********************************************************************
	void free(cu32 i){
	// az azonos�t� �ltal jelzett blokkban szabad�tunk fel
	// nem ellen�rzi, hogy t�bbsz�r szabad�tunk-e fel
	//***********************************************************************
		if(i==V_DIN_MAX_BLOKK)return;// NULL pointer volt
		if(!--g[i]){
			e[i]=0;
			if(i>0&&akt!=i){__free(i);akt=0;}
		}
	}
	//***********************************************************************
	void print(){
	//***********************************************************************
		printf("\nn=%lu\n",n);
		for(u32 i=0;i<n;i++)printf("blokk[%02lu]=%lu\n",i,g[i]);
	}
};

extern qdinmem * qvsun_memoria;
extern bool parhuzamosanfut;

//***********************************************************************
class qmBase{
//***********************************************************************
private:
	//***********************************************************************
	qmBase(const qmBase & src);
	//***********************************************************************
	void operator=(const qmBase & src);
	//***********************************************************************
protected:
	//***********************************************************************
	quad_float * t;	// A t�mb
	u32 siz;		// A t�nyleges m�ret, row*col*dim*blokk
	u32 row,col;	// sor, oszlop
	u32 azonosito;  // dinmemhez
	//***********************************************************************
	void resize(cu32 newsiz){
	//***********************************************************************
		if(siz==newsiz)return;
		siz=newsiz;
		if(azonosito==V_DIN_MAX_BLOKK+1)delete[]t;
		else qvsun_memoria->free(azonosito);
		if(parhuzamosanfut||newsiz>1024){
			t=new quad_float[newsiz];
			azonosito=V_DIN_MAX_BLOKK+1;
		}
		else t=qvsun_memoria->alloc(newsiz,azonosito);
        //zero();
	}
	//***********************************************************************
	void set(cu32 n,cq v){
	//***********************************************************************
#ifdef vsundebugmode
		if(n>=siz)throw hiba("","qmBase::set => n>siz => %lu!=%lu",n,siz);
#endif
		t[n]=v;
	}
	//***********************************************************************
	quad_float get(cu32 n)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(n>=siz)throw hiba("","qmBase::get => n>siz => %lu!=%lu",n,siz);
#endif
		return t[n];
	}
	//***********************************************************************
	virtual quad_float get(cu32 r,cu32 c)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(r>=siz)throw hiba("","qmBase::get => r>siz => %lu!=%lu",r,siz);
#endif
		return t[r];
	}
	//***********************************************************************
	void inc(cu32 n,cq v){
	//***********************************************************************
#ifdef vsundebugmode
		if(n>=siz)throw hiba("","qmBase::inc => n>siz => %lu!=%lu",n,siz);
#endif
		t[n]+=v;
	}
public:
	//***********************************************************************
	qmBase():t(NULL),siz(0),row(0),col(0),azonosito(V_DIN_MAX_BLOKK+1){}
	//***********************************************************************
	~qmBase(){free();}
	//***********************************************************************
	void free(){
	//***********************************************************************
		if(azonosito==V_DIN_MAX_BLOKK+1)delete[]t;
		else {if(qvsun_memoria)qvsun_memoria->free(azonosito);azonosito=V_DIN_MAX_BLOKK+1;}
		t=NULL;siz=col=row=0;
	}
	//***********************************************************************
	virtual void resize(cu32 r,cu32 c)=0;
	//***********************************************************************
	u32 getsiz()const{return siz;}
	u32 getrow()const{return row;}
	u32 getcol()const{return col;}
	u32 getMemUsage(){return siz+sizeof(*this);}
	quad_float * gett(){return t;}
	cq * gett()const{return t;}
	//***********************************************************************

	//***********************************************************************
	void copy(const qmBase & src){
	//***********************************************************************
		if(&src==this)return;
		resize(src.row,src.col);
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src.t[i];
	}

	//***********************************************************************
	void swap(qmBase & src){
	//***********************************************************************
		if(&src==this)return;
		quad_float*vt=src.t;src.t=t;t=vt;
		u32 ut=src.siz;src.siz=siz;siz=ut;
		ut=src.row;src.row=row;row=ut;
		ut=src.col;src.col=col;col=ut;
	}
	//***********************************************************************
	void print(bool matrixkent=false)const{
	//***********************************************************************
        printf("siz=%u\n",siz);
		if(!matrixkent)for(u32 i=0;i<siz;i++)printf("%10g\t",q2d(t[i]));
        else for(uns i=0; i<row; i++,printf("\n"))for(uns j=0;j<col;j++)printf("%10g\t",q2d(get(i,j)));
		printf("\n");
	}
	//***********************************************************************
	void zero(cd value=0.0){
	//***********************************************************************
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=value;
	}
	//***********************************************************************
	bool is_zero(){
	//***********************************************************************
		cu32 n=siz;
        bool res=true;
		for(u32 i=0;i<n;i++)res = res && t[i]==0;
        return res;
	}
	//***********************************************************************
	void neg(){
	//***********************************************************************
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=-t[i];
	}
	//***********************************************************************
	void random(){
	//***********************************************************************
		cu32 n=siz;
//		srand((unsigned)time(NULL));
		for(u32 i=0;i<n;i++)t[i]=rand()+2;
	}
	//***********************************************************************
	void novekvo(){
	//***********************************************************************
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=i;
	}
	//***********************************************************************
	void add(const qmBase & src1,const qmBase & src2){
	//***********************************************************************
#ifdef vsundebugmode
		if(this==&src1||this==&src2)throw hiba("","qmBase::add => this==&src1||this==&src2");
		if(src1.siz!=src2.siz)throw hiba("","qmBase::add => src1.siz!=src2.siz => %lu!=%lu",src1.siz,src2.siz);
		if(src1.col!=src2.col)throw hiba("","qmBase::add => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
		if(src1.row!=src2.row)throw hiba("","qmBase::add => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
#endif
		resize(src1.row,src1.col);
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]+src2.t[i];
	}
	//***********************************************************************
	void addnr(const qmBase & src1,const qmBase & src2){
	// nem m�retezi �t, lehet azonos a forr�s �s a c�l
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=src2.siz)throw hiba("","qmBase::add => src1.siz!=src2.siz => %lu!=%lu",src1.siz,src2.siz);
		if(src1.col!=src2.col)throw hiba("","qmBase::add => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
		if(src1.row!=src2.row)throw hiba("","qmBase::add => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
		if(col!=src2.col)throw hiba("","qmBase::addnr => col!=src2.col => %lu!=%lu",col,src2.col);
		if(row!=src2.row)throw hiba("","qmBase::addnr => row!=src2.row => %lu!=%lu",row,src2.row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]+src2.t[i];
	}
	//***********************************************************************
	void pluszegyenlo(const qmBase & src1){
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=siz)throw hiba("","qmBase::pluszegyenlo => src1.siz!=siz => %lu!=%lu",src1.siz,siz);
		if(src1.col!=col)throw hiba("","qmBase::pluszegyenlo => src1.col!=col => %lu!=%lu",src1.col,col);
		if(src1.row!=row)throw hiba("","qmBase::pluszegyenlo => src1.row!=row => %lu!=%lu",src1.row,row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]+=src1.t[i];
	}
	//***********************************************************************
	void sub(const qmBase & src1,const qmBase & src2){
	//***********************************************************************
#ifdef vsundebugmode
		if(&src1==this||&src2==this)throw hiba("","qmBase::sub => &src1==this||&src2==this");
		if(src1.siz!=src2.siz)throw hiba("","qmBase::sub => src1.siz!=src2.siz => %lu!=%lu",src1.siz,src2.siz);
		if(src1.col!=src2.col)throw hiba("","qmBase::sub => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
		if(src1.row!=src2.row)throw hiba("","qmBase::sub => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
#endif
		resize(src1.row,src1.col);
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]-src2.t[i];
	}
	//***********************************************************************
	void subnr(const qmBase & src1,const qmBase & src2){
	// nem m�retezi �t, lehet azonos a forr�s �s a c�l
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=src2.siz)throw hiba("","qmBase::subnr => src1.siz!=src2.siz => %lu!=%lu",src1.siz,src2.siz);
		if(src1.col!=src2.col)throw hiba("","qmBase::subnr => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
		if(src1.row!=src2.row)throw hiba("","qmBase::subnr => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
		if(col!=src2.col)throw hiba("","qmBase::subnr => col!=src2.col => %lu!=%lu",col,src2.col);
		if(row!=src2.row)throw hiba("","qmBase::subnr => row!=src2.row => %lu!=%lu",row,src2.row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]-src2.t[i];
	}
	//***********************************************************************
	void addsubnr(const qmBase & src1,const qmBase & src2,const qmBase & src3){
	// nem m�retezi �t, lehet azonos a forr�s �s a c�l
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=src2.siz)throw hiba("","qmBase::subnr => src1.siz!=src2.siz => %lu!=%lu",src1.siz,src2.siz);
		if(src1.col!=src2.col)throw hiba("","qmBase::subnr => src1.col!=src2.col => %lu!=%lu",src1.col,src2.col);
		if(src1.row!=src2.row)throw hiba("","qmBase::subnr => src1.row!=src2.row => %lu!=%lu",src1.row,src2.row);
		if(src1.siz!=src3.siz)throw hiba("","qmBase::subnr => src1.siz!=src3.siz => %lu!=%lu",src1.siz,src3.siz);
		if(src1.col!=src3.col)throw hiba("","qmBase::subnr => src1.col!=src3.col => %lu!=%lu",src1.col,src3.col);
		if(src1.row!=src3.row)throw hiba("","qmBase::subnr => src1.row!=src3.row => %lu!=%lu",src1.row,src3.row);
		if(col!=src2.col)throw hiba("","qmBase::subnr => col!=src2.col => %lu!=%lu",col,src2.col);
		if(row!=src2.row)throw hiba("","qmBase::subnr => row!=src2.row => %lu!=%lu",row,src2.row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]+src2.t[i]-src3.t[i];
	}
	//***********************************************************************
	void minuszegyenlo(const qmBase & src1){
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=siz)throw hiba("","qmBase::minuszegyenlo => src1.siz!=siz => %lu!=%lu",src1.siz,siz);
		if(src1.col!=col)throw hiba("","qmBase::minuszegyenlo => src1.col!=col => %lu!=%lu",src1.col,col);
		if(src1.row!=row)throw hiba("","qmBase::minuszegyenlo => src1.row!=row => %lu!=%lu",src1.row,row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]-=src1.t[i];
	}
	//***********************************************************************
	void addaddsubadd(const qmBase & src1,const qmBase & src2,const qmBase & src3,const qmBase & src4){
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=src2.siz||src2.siz!=src3.siz||src3.siz!=src4.siz)
			throw hiba("","qmBase::addaddsubadd => src1.siz!=src2.siz!=src3.siz!=src4.siz => %lu!=%lu!=%lu!=%lu",src1.siz,src2.siz,src3.siz,src4.siz);
		if(src1.col!=src2.col||src2.col!=src3.col||src3.col!=src4.col)
			throw hiba("","qmBase::addaddsubadd => src1.col!=src2.col!=src3.col!=src4.col => %lu!=%lu!=%lu!=%lu",src1.col,src2.col,src3.col,src4.col);
		if(src1.row!=src2.row||src2.row!=src3.row||src3.row!=src4.row)
			throw hiba("","qmBase::addaddsubadd => src1.row!=src2.row!=src3.row!=src4.row => %lu!=%lu!=%lu!=%lu",src1.row,src2.row,src3.row,src4.row);
#endif
		resize(src1.row,src1.col);
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]+src2.t[i]-src3.t[i]+src4.t[i];
	}
	//***********************************************************************
	void addsubsubnr(const qmBase & src1,const qmBase & src2,const qmBase & src3){
	//***********************************************************************
#ifdef vsundebugmode
		if(src1.siz!=src2.siz||src2.siz!=src3.siz||src3.siz!=siz)
			throw hiba("","qmBase::addsumsubnr => src1.siz!=src2.siz!=src3.siz!=siz => %lu!=%lu!=%lu!=%lu",src1.siz,src2.siz,src3.siz,siz);
		if(src1.col!=src2.col||src2.col!=src3.col||src3.col!=col)
			throw hiba("","qmBase::addsumsubnr => src1.col!=src2.col!=src3.col!=col => %lu!=%lu!=%lu!=%lu",src1.col,src2.col,src3.col,col);
		if(src1.row!=src2.row||src2.row!=src3.row||src3.row!=row)
			throw hiba("","qmBase::addsumsubnr => src1.row!=src2.row!=src3.row!=row => %lu!=%lu!=%lu!=%lu",src1.row,src2.row,src3.row,row);
#endif
		cu32 n=siz;
		for(u32 i=0;i<n;i++)t[i]=src1.t[i]-src2.t[i]-src3.t[i];
	}
	//***********************************************************************
	virtual void save(FILE *fp)const;
	virtual void savetext(const char * FileName)const;
	virtual void load(FILE *fp);
	//***********************************************************************
	void store(FILE *fp){
	// Lemezre menti �s t�rli a mem�ri�b�l
	//***********************************************************************
		save(fp);
		free();
	}
};

//###################################################################################################x
//###################################################################################################x
//###################################################################################################x

//***********************************************************************
class qrmatrix;
class qsrmatrix;
class qrvector;
//***********************************************************************

//***********************************************************************
class qrvector:public qmBase{
//***********************************************************************
	friend class qrmatrix;
	friend class qsrmatrix;
public:
	//***********************************************************************
	void resize(cu32 r,cu32 c){
	//***********************************************************************
#ifdef vsundebugmode
		if(r!=1&&c!=1)
			throw hiba("","qrvector::resize => r!=1&&c!=1 => %lu,%lu", r,c);
#endif
		resize(r==1?c:r);
	}
	//***********************************************************************
	void resize(cu32 n){
	//***********************************************************************
#ifdef vsundebugmode2
		if(n==0)logprint("qsrmatrix::resize => n==0");
#endif
		row=1;
		col=n;
		qmBase::resize(n);
	}
	//***********************************************************************
	void set(cu32 i,cq v){t[i]=v;}
	quad_float get(cu32 i)const{return t[i];}
	void inc(cu32 i,cq v){t[i]+=v;}
	void inc0(cq v){t[0]+=v;}
	//***********************************************************************
	void mul(const qrmatrix & src1,const qrvector & src2){tmul(src1,src2);}
	void mult(const qrmatrix & src1,const qrvector & src2){tmult(src1,src2);}
	void mul(const qsrmatrix & src1,const qrvector & src2){tmul(src1,src2);}
	//***********************************************************************
	void tmul(const qrmatrix & src1,const qrvector & src2,const bool adde=false);
	void tmult(const qrmatrix & src1,const qrvector & src2,const bool adde=false);
	void tmul(const qsrmatrix & src1,const qrvector & src2,const bool adde=false);
	void tmuladd(const qrmatrix & src1,const qrvector & src2);
	void tmuladd(const qsrmatrix & src1,const qrvector & src2);
	void getsubvector(const qrvector & src,cu32 n0,cu32 n);
	void setsubvector(const qrvector & src,cu32 n0);
	void getsetsubvector(const qrvector & src,cu32 from_n0,cu32 to_n0,cu32 n);
	void getsetaddsubvector(const qrvector & src,cu32 from_n0,cu32 to_n0,cu32 n);
	//***********************************************************************
};


//***********************************************************************
class qrmatrix:public qmBase{
//***********************************************************************
	friend class qsrmatrix;
	friend class qrvector;
protected:
	//***********************************************************************
	void strasseninv();
	void strassentmul(const qrmatrix & src1,const qrmatrix & src2);//val�j�ba += !!!
	void strassentmul(const qsrmatrix & src1,const qrmatrix & src2);//rBlokksz� konvert�lja//val�j�ba += !!!
	//***********************************************************************
	void getLine(quad_float * dest,cu32 sor)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","qrmatrix::getLine => sor>row => %lu>%lu",sor,row);
#endif
		cq*p=t+sor*col;
		for(u32 i=0;i<col;i++)dest[i]=p[i];
	}
	//***********************************************************************
	void getLineadd(quad_float * dest,cu32 sor)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","qrmatrix::getLineadd => sor>row => %lu>%lu",sor,row);
#endif
		cq*p=t+sor*col;
		for(u32 i=0;i<col;i++)dest[i]+=p[i];
	}
	//***********************************************************************
	void getLine(quad_float * dest,cu32 sor,cu32 oszl,cu32 db)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","qrmatrix::getLine => sor>row => %lu>%lu",sor,row);
		if(oszl+db>col)throw hiba("","qrmatrix::getLine => oszl+db>col => %lu+%lu>%lu",oszl,db,col);
#endif
		cq*p=t+sor*col+oszl;
		for(u32 i=0;i<db;i++)dest[i]=p[i];
	}
	//***********************************************************************
	void getLineadd(quad_float * dest,cu32 sor,cu32 oszl,cu32 db)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","qrmatrix::getLineadd => sor>row => %lu>%lu",sor,row);
		if(oszl+db>col)throw hiba("","qrmatrix::getLineadd => oszl+db>col => %lu+%lu>%lu",oszl,db,col);
#endif
		cq*p=t+sor*col+oszl;
		for(u32 i=0;i<db;i++)dest[i]+=p[i];
	}
public:
	//***********************************************************************
	void resize(cu32 r,cu32 c){
	//***********************************************************************
#ifdef vsundebugmode2
		if(r==0||c==0)logprint("qrmatrix::resize => r==0||c==0 => %lu,%lu",r,c);
#endif
		row=r;
		col=c;
		qmBase::resize(r*c);
	}

	//***********************************************************************
	void egyseg(){
	//***********************************************************************
		cu32 n=col<row?col:row;
		zero();
		for(u32 i=0;i<n;i++)set(i,i,qegy);
	}

	//***********************************************************************
	void set(cu32 r,cu32 c,cq v){
	//***********************************************************************
#ifdef vsundebugmode
		if(r>=row)throw hiba("","qrmatrix::set => r>=row => %lu!=%lu",r,row);
		if(c>=col)throw hiba("","qrmatrix::set => c>=col => %lu!=%lu",c,col);
#endif
		t[r*col+c]=v;
	}

	//***********************************************************************
	quad_float get(cu32 r,cu32 c)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(r>=row)throw hiba("","qrmatrix::get => r>=row => %lu!=%lu",r,row);
		if(c>=col)throw hiba("","qrmatrix::get => c>=col => %lu!=%lu",c,col);
#endif
		return t[r*col+c];
	}

	//***********************************************************************
	void inc(cu32 r,cu32 c,cq v){
	//***********************************************************************
#ifdef vsundebugmode
		if(r>=row)throw hiba("","qrmatrix::inc => r>=row => %lu!=%lu",r,row);
		if(c>=col)throw hiba("","qrmatrix::inc => c>=col => %lu!=%lu",c,col);
#endif
		t[r*col+c]+=v;
	}

	//***********************************************************************
	void inc00(cq v){t[0]+=v;}
	//***********************************************************************

	//***********************************************************************
	void transp(const qrmatrix & src){
	//***********************************************************************
#ifdef vsundebugmode
		if(&src==this)throw hiba("","qrmatrix::transp => &src==this");
#endif
		resize(src.col,src.row); // transzpon�l�shoz!
		for(u32 i=0;i<row;i++)for(u32 j=0;j<col;j++)set(i,j,src.get(j,i));
	}

	//***********************************************************************
	void tmul(const qrmatrix & src1,const qrmatrix & src2,const bool adde=false);
	void tmul(const qsrmatrix & src1,const qrmatrix & src2,const bool adde=false);
	void tmul(const qrmatrix & src1,const qsrmatrix & src2,const bool adde=false);
	void inv();
	void submatrixadd(const qrmatrix & src1,cu32 y1,cu32 x1,const qrmatrix & src2,cu32 y2,cu32 x2,cu32 y,cu32 x);
	void submatrixsub(const qrmatrix & src1,cu32 y1,cu32 x1,const qrmatrix & src2,cu32 y2,cu32 x2,cu32 y,cu32 x);
	void getsubmatrix(const qrmatrix & src,cu32 y0,cu32 x0,cu32 y,cu32 x);
	void getsetsubmatrix(const qsrmatrix & src,cu32 y0_from,cu32 x0_from,cu32 y0_to,cu32 x0_to,cu32 y,cu32 x);
	void setsubmatrix(const qrmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixadd(const qrmatrix & src,cu32 y0,cu32 x0);
	void getsubmatrix(const qsrmatrix & src,cu32 y0,cu32 x0,cu32 y,cu32 x);
	void setsubmatrix(const qsrmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixadd(const qsrmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixneg(const qrmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixnegadd(const qrmatrix & src,cu32 y0,cu32 x0);
	//***********************************************************************

	//***********************************************************************
	void mul(const qrmatrix & src1,const qrmatrix & src2){
	//***********************************************************************
#ifdef vsundebugmode
		logprint("qrmatrix::mul => nem hatekony");
#endif
		qrmatrix r;
		r.transp(src2);
		tmul(src1,r);
	}
	//***********************************************************************
	void convert(const qsrmatrix & src);
	//***********************************************************************

};

//###################################################################################################x
//###################################################################################################x
//###################################################################################################x

//***********************************************************************
class qsrmatrix:public qmBase{
//***********************************************************************
	friend class qrmatrix;
	friend class qrvector;
	//***********************************************************************
	cu32 getcimunsafe(cu32 r,cu32 c)const{
	// Az r<=c biztos!
	//***********************************************************************
		return r*col+c-(r+1)*r/2;
	}
public:
	//***********************************************************************
	cu32 getcim(cu32 r,cu32 c)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(r>=row)throw hiba("","qsrmatrix::getcim => r>=row => %lu!=%lu",r,row);
		if(c>=col)throw hiba("","qsrmatrix::getcim => c>=col => %lu!=%lu",c,col);
#endif
		return (r<=c)?getcimunsafe(r,c):getcimunsafe(c,r);
	}
	//***********************************************************************
	void getLine(quad_float * dest,cu32 sor)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","qsrmatrix::getLine => sor>row => %lu>%lu",sor,row);
#endif
		cq*p=t+sor;
		for(u32 i=0;i<sor;++i,p+=col-i,dest++)*dest=*p;
		for(u32 i=sor;i<col;i++,p++,dest++)*dest=*p;
	}
	//***********************************************************************
	void getLineadd(quad_float * dest,cu32 sor)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","qsrmatrix::getLineadd => sor>row => %lu>%lu",sor,row);
#endif
		cq*p=t+sor;
		for(u32 i=0;i<sor;++i,p+=col-i,dest++)*dest+=*p;
		for(u32 i=sor;i<col;i++,p++,dest++)*dest+=*p;
	}
	//***********************************************************************
	void getLine(quad_float * dest,cu32 sor,cu32 oszl,cu32 db)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","qsrmatrix::getLine => sor>row => %lu>%lu",sor,row);
		if(oszl+db>col)throw hiba("","qsrmatrix::getLine => oszl+db>col => %lu+%lu>%lu",oszl,db,col);
#endif
		if(oszl<sor){
			cq*p=t+getcimunsafe(oszl,sor);
			u32 n=0;
			for(u32 i=oszl;i<sor&&n<db;++i,p+=col-i,n++)dest[n]=*p;
			for(;n<db;p++,n++)dest[n]=*p;
		}
		else{
			cq*p=t+getcimunsafe(sor,oszl);
			for(u32 i=0;i<db;i++)dest[i]=p[i];
		}
	}
	//***********************************************************************
	void getLineadd(quad_float * dest,cu32 sor,cu32 oszl,cu32 db)const{
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","qsrmatrix::getLineadd => sor>row => %lu>%lu",sor,row);
		if(oszl+db>col)throw hiba("","qsrmatrix::getLineadd => oszl+db>col => %lu+%lu>%lu",oszl,db,col);
#endif
		if(oszl<sor){
			cq*p=t+getcimunsafe(oszl,sor);
			u32 n=0;
			for(u32 i=oszl;i<sor&&n<db;++i,p+=col-i,n++)dest[n]+=*p;
			for(;n<db;p++,n++)dest[n]+=*p;
		}
		else{
			cq*p=t+getcimunsafe(sor,oszl);
			for(u32 i=0;i<db;i++)dest[i]+=p[i];
		}
	}
	//***********************************************************************
	void setLine(cq*src,cu32 sor){
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","qsrmatrix::setLine => sor>row => %lu>%lu",sor,row);
#endif
		quad_float*p=t+sor;
		for(u32 i=0;i<sor;++i,p+=col-i,src++)*p=*src;
		for(u32 i=sor;i<col;i++,p++,src++)*p=*src;
	}
	//***********************************************************************
	void setLine(cq*src,cu32 sor,cu32 oszl,cu32 db){
	//***********************************************************************
#ifdef vsundebugmode
		if(sor>row)throw hiba("","qsrmatrix::setLine => sor>row => %lu>%lu",sor,row);
		if(oszl+db>col)throw hiba("","qsrmatrix::setLine => oszl+db>col => %lu+%lu>%lu",oszl,db,col);
#endif
		if(oszl<sor){
			quad_float*p=t+getcimunsafe(oszl,sor);
			u32 n=0;
			for(u32 i=oszl;i<sor&&n<db;++i,p+=col-i,n++)*p=src[n];
			for(;n<db;p++,n++)*p=src[n];
		}
		else{
			quad_float*p=t+getcimunsafe(sor,oszl);
			for(u32 i=0;i<db;i++)p[i]=src[i];
		}
	}
	//***********************************************************************
	void resize(cu32 n){
	//***********************************************************************
#ifdef vsundebugmode2
		if(n==0)
            logprint("qsrmatrix::resize => n==0");
#endif
		row=col=n;
		qmBase::resize(n*(n+1)/2);
	}
	//***********************************************************************
	void resize(cu32 r,cu32 c){
	//***********************************************************************
#ifdef vsundebugmode
		if(r!=c)
			throw hiba("","qsrmatrix::resize => r!=c => %lu!=%lu", r,c);
#endif
		resize(r);
	}
	//***********************************************************************
	void egyseg(){
	//***********************************************************************
		cu32 n=col;
		zero();
		for(u32 i=0;i<n;i++)set(i,i,qegy);
	}
	//***********************************************************************
	void set(cu32 r,cu32 c,cq v){t[getcim(r,c)]=v;}
	//***********************************************************************
	quad_float get(cu32 r,cu32 c)const{return t[getcim(r,c)];}
	//***********************************************************************
	void inc(cu32 r,cu32 c,cq v){t[getcim(r,c)]+=v;}
	//***********************************************************************
	void inc00(cq v){t[0]+=v;}
	//***********************************************************************
	void transp(const qsrmatrix & src){
	//***********************************************************************
#ifdef vsundebugmode
		if(&src==this)throw hiba("","qsrmatrix::transp => &src==this");
		logprint("qsrmatrix::transp => felelsleges muvelet");
#endif
		throw hiba("","qsrmatrix::transp => t�pusfelismer�ssel megcsin�lni!");
		copy(src);
	}
	//***********************************************************************
	void mul(const qrmatrix & src1,const qrmatrix & src2){
	//***********************************************************************
#ifdef vsundebugmode2
		logprint("qsrmatrix::mul => nem hatekony");
#endif
		qrmatrix r;
		r.transp(src2);
		tmul(src1,r);
	}
	//***********************************************************************
	void mul_vigyazz(const qsrmatrix & src1,const qsrmatrix & src2){
	// �ltal�ban k�t symm szorzata NEM symm!!!
	//***********************************************************************
		tmul_vigyazz(src1,src2);
	}
	//***********************************************************************
	void inv(){
	//***********************************************************************
		ninv();
		neg();
	}
	//***********************************************************************
	void tmul(const qrmatrix & src1,const qrmatrix & src2,const bool adde=false);
	void tmul_vigyazz(const qsrmatrix & src1,const qsrmatrix & src2,const bool adde=false);// �ltal�ban l�t symm szorzata NEM symm!!!
	void ninv();
	void getsubmatrix(const qsrmatrix & src,cu32 n0,cu32 n);
	void getsetsubmatrix(const qsrmatrix & src,cu32 n0_from,cu32 n0_to,cu32 n);
	void getsetsubmatrix(const qsrmatrix & src,cu32 y0_from,cu32 x0_from,cu32 y0_to,cu32 x0_to,cu32 y,cu32 x);
	void getsetsubmatrixadd(const qsrmatrix & src,cu32 n0_from,cu32 n0_to,cu32 n);
	void setsubmatrix(const qsrmatrix & src,cu32 n0);
	void setsubmatrixadd(const qsrmatrix & src,cu32 n0);
	void setsubmatrix(const qrmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixadd(const qrmatrix & src,cu32 y0,cu32 x0);
	//***********************************************************************
protected:
	//***********************************************************************
	void strasseninv();
	void strassentmul(const qrmatrix & src1,const qrmatrix & src2);//val�j�ba += !!!
	void strassentmul(const qsrmatrix & src1,const qsrmatrix & src2);//val�j�ba += !!!
	//***********************************************************************
};

//***********************************************************************
class qrmtmulszal:public PLThread{
//***********************************************************************
	static qrmatrix rdummy;
	static qsrmatrix srdummy;
	const qrmatrix &s1,&s2;
	const qsrmatrix &s3,&s4;
	qrmatrix &d;
	qsrmatrix &d2;
	cu32 mode;
public:
	qrmtmulszal(const qrmatrix &src1,const qrmatrix &src2,qrmatrix &dest)
		:s1(src1),s2(src2),d(dest),mode(1),s3(srdummy),s4(srdummy),d2(srdummy){}

	qrmtmulszal(const qsrmatrix &src1,const qrmatrix &src2,qrmatrix &dest)
		:s3(src1),s2(src2),d(dest),mode(2),s1(rdummy),s4(srdummy),d2(srdummy){}

	qrmtmulszal(const qrmatrix &src1,const qrmatrix &src2,qsrmatrix &dest)
		:s1(src1),s2(src2),d2(dest),mode(3),s3(srdummy),s4(srdummy),d(rdummy){}

	qrmtmulszal(const qrmatrix &src1,const qsrmatrix &src2,qrmatrix &dest)
		:s1(src1),s3(src2),d(dest),mode(4),s2(rdummy),s4(srdummy),d2(srdummy){}

	qrmtmulszal(const qsrmatrix &src1,const qsrmatrix &src2,qsrmatrix &dest)
		:s3(src1),s4(src2),d2(dest),mode(5),s1(rdummy),s2(rdummy),d(rdummy){}


	void run(){
		switch(mode){
			case 1: d.tmul(s1,s2); break;
			case 2: d.tmul(s3,s2); break;
			case 3: d2.tmul(s1,s2); break;
			case 4: d.tmul(s1,s3); break;
			case 5: d2.tmul_vigyazz(s3,s4); break;
		}
	}
};


//***********************************************************************
class qrminvszal:public PLThread{
//***********************************************************************
	static qrmatrix rdummy;
	static qsrmatrix srdummy;
	qrmatrix &d;
	qsrmatrix &d2;
	cu32 mode;
public:
	qrminvszal(qrmatrix &srcdest):d(srcdest),d2(srdummy),mode(1){}
	qrminvszal(qsrmatrix &srcdest):d2(srcdest),d(rdummy),mode(2){}
	void run(){if(mode==1)d.inv();else d2.ninv();}
};

//***********************************************************************
class qcmatrix;
class qscmatrix;
//***********************************************************************
//***********************************************************************
class qcvector{
//***********************************************************************
	qrvector re,im;
	friend class qcmatrix;
	friend class qscmatrix;
	void strasseninv();
public:
	void free(){re.free();im.free();}
	u32 getsiz()const{return re.getsiz();}
	u32 getrow()const{return re.getrow();}
	u32 getcol()const{return re.getcol();}
	u32 getMemUsage(){return re.getMemUsage()+im.getMemUsage()+sizeof(*this);;}
	void copy(const qcvector & src){re.copy(src.re);im.copy(src.im);}
	void swap(qcvector & src){re.swap(src.re);im.swap(src.im);}
	void print(bool matrixkent=false)const{re.print(matrixkent);im.print(matrixkent);}
	void zero(cd Re=0.0,cd Im=0.0){re.zero(Re);im.zero(Im);}
	void neg(){re.neg();im.neg();}
	void random(){re.random();im.random();}
	void novekvo(){re.novekvo();im.novekvo();}
	void add(const qcvector & src1,const qcvector & src2){re.add(src1.re,src2.re);im.add(src1.im,src2.im);}
	void addnr(const qcvector & src1,const qcvector & src2){re.addnr(src1.re,src2.re);im.addnr(src1.im,src2.im);}
	void pluszegyenlo(const qcvector & src1){re.pluszegyenlo(src1.re);im.pluszegyenlo(src1.im);}
	void sub(const qcvector & src1,const qcvector & src2){re.sub(src1.re,src2.re);im.sub(src1.im,src2.im);}
	void subnr(const qcvector & src1,const qcvector & src2){re.subnr(src1.re,src2.re);im.subnr(src1.im,src2.im);}
	void minuszegyenlo(const qcvector & src1){re.minuszegyenlo(src1.re);im.minuszegyenlo(src1.im);}
	void save(FILE *fp)const{re.save(fp);im.save(fp);}
	void savetext(const char * reFileName,const char * imFileName)const{re.savetext(reFileName);im.savetext(imFileName);}
	void load(FILE *fp){re.load(fp);im.load(fp);}
	void store(FILE *fp){re.store(fp);im.store(fp);}
	//***********************************************************************
	void resize(cu32 n){re.resize(n);im.resize(n);}
	void set(cu32 i,cqc v){re.set(i,v.re);im.set(i,v.im);}
	qcomplex get(cu32 i)const{return qcomplex(re.get(i),im.get(i));}
	void inc(cu32 i,cqc v){re.inc(i,v.re);im.inc(i,v.im);}
	void inc0(cqc v){re.inc0(v.re);im.inc0(v.im);}
	//***********************************************************************
	void mul(const qcmatrix & src1,const qcvector & src2){tmul(src1,src2);}
	void mult(const qcmatrix & src1,const qcvector & src2){tmult(src1,src2);}
	void mul(const qscmatrix & src1,const qcvector & src2){tmul(src1,src2);}
	void tmul(const qcmatrix & src1,const qcvector & src2,const bool adde=false);
	void tmult(const qcmatrix & src1,const qcvector & src2,const bool adde=false);
	void tmul(const qscmatrix & src1,const qcvector & src2,const bool adde=false);
	void tmuladd(const qcmatrix & src1,const qcvector & src2);
	void tmuladd(const qscmatrix & src1,const qcvector & src2);
	//***********************************************************************
	void getsubvector(const qcvector & src,cu32 n0,cu32 n){re.getsubvector(src.re,n0,n);im.getsubvector(src.im,n0,n);}
	void setsubvector(const qcvector & src,cu32 n0){re.setsubvector(src.re,n0);im.setsubvector(src.im,n0);}
	void getsetsubvector(const qcvector & src,cu32 from_n0,cu32 to_n0,cu32 n){re.getsetsubvector(src.re,from_n0,to_n0,n);im.getsetsubvector(src.im,from_n0,to_n0,n);}
	void getsetaddsubvector(const qcvector & src,cu32 from_n0,cu32 to_n0,cu32 n){re.getsetaddsubvector(src.re,from_n0,to_n0,n);im.getsetaddsubvector(src.im,from_n0,to_n0,n);}
};


//***********************************************************************
class qscmatrix{
//***********************************************************************
	qsrmatrix re,im;
	friend class qcmatrix;
	friend class qcvector;
	void strasseninv();
public:
	void free(){re.free();im.free();}
	void resize(cu32 r,cu32 c){re.resize(r,c);im.resize(r,c);}
	u32 getsiz()const{return re.getsiz();}
	u32 getrow()const{return re.getrow();}
	u32 getcol()const{return re.getcol();}
	u32 getMemUsage(){return re.getMemUsage()+im.getMemUsage()+sizeof(*this);;}
	void copy(const qscmatrix & src){re.copy(src.re);im.copy(src.im);}
	void swap(qscmatrix & src){re.swap(src.re);im.swap(src.im);}
	void print(bool matrixkent=false)const{re.print(matrixkent);im.print(matrixkent);}
	void zero(cd Re=0.0,cd Im=0.0){re.zero(Re);im.zero(Im);}
	void neg(){re.neg();im.neg();}
	void random(){re.random();im.random();}
	void novekvo(){re.novekvo();im.novekvo();}
	void add(const qscmatrix & src1,const qscmatrix & src2){re.add(src1.re,src2.re);im.add(src1.im,src2.im);}
	void addnr(const qscmatrix & src1,const qscmatrix & src2){re.addnr(src1.re,src2.re);im.addnr(src1.im,src2.im);}
	void pluszegyenlo(const qscmatrix & src1){re.pluszegyenlo(src1.re);im.pluszegyenlo(src1.im);}
	void sub(const qscmatrix & src1,const qscmatrix & src2){re.sub(src1.re,src2.re);im.sub(src1.im,src2.im);}
	void subnr(const qscmatrix & src1,const qscmatrix & src2){re.subnr(src1.re,src2.re);im.subnr(src1.im,src2.im);}
	void minuszegyenlo(const qscmatrix & src1){re.minuszegyenlo(src1.re);im.minuszegyenlo(src1.im);}
	void save(FILE *fp)const{re.save(fp);im.save(fp);}
	void savetext(const char * reFileName,const char * imFileName)const{re.savetext(reFileName);im.savetext(imFileName);}
	void load(FILE *fp){re.load(fp);im.load(fp);}
	void store(FILE *fp){re.store(fp);im.store(fp);}
	//***********************************************************************
	void resize(cu32 n){resize(n,n);}
	void egyseg(){re.egyseg();im.egyseg();}
	void set(cu32 r,cu32 c,cqc v){re.set(r,c,v.re);im.set(r,c,v.im);}
	qcomplex get(cu32 r,cu32 c)const{return qcomplex(re.get(r,c),im.get(r,c));}
	void inc(cu32 r,cu32 c,cqc v){re.inc(r,c,v.re);im.inc(r,c,v.im);}
	void inc00(cqc v){re.inc00(v.re);im.inc00(v.im);}
	void transp(const qscmatrix & src){re.transp(src.re);im.transp(src.im);}
	void tmul(const qcmatrix & src1,const qcmatrix & src2,const bool adde=false);
	void tmul_vigyazz(const qscmatrix & src1,const qscmatrix & src2,const bool adde=false);// �ltal�ban l�t symm szorzata NEM symm!!!
	void ninv();
	void mul(const qcmatrix & src1,const qcmatrix & src2);
	//***********************************************************************
	void mul_vigyazz(const qscmatrix & src1,const qscmatrix & src2){
	// �ltal�ban k�t symm szorzata NEM symm!!!
	//***********************************************************************
		tmul_vigyazz(src1,src2);
	}
	//***********************************************************************
	void inv(){
	//***********************************************************************
		ninv();
		neg();
	}
	void getsubmatrix(const qscmatrix & src,cu32 n0,cu32 n){
		re.getsubmatrix(src.re,n0,n);im.getsubmatrix(src.im,n0,n);
	}
	void getsetsubmatrix(const qscmatrix & src,cu32 n0_from,cu32 n0_to,cu32 n){
		re.getsetsubmatrix(src.re,n0_from,n0_to,n);im.getsetsubmatrix(src.im,n0_from,n0_to,n);
	}
	void getsetsubmatrix(const qscmatrix & src,cu32 y0_from,cu32 x0_from,cu32 y0_to,cu32 x0_to,cu32 y,cu32 x){
		re.getsetsubmatrix(src.re,y0_from,x0_from,y0_to,x0_to,y,x);im.getsetsubmatrix(src.im,y0_from,x0_from,y0_to,x0_to,y,x);
	}
	void getsetsubmatrixadd(const qscmatrix & src,cu32 n0_from,cu32 n0_to,cu32 n){
		re.getsetsubmatrixadd(src.re,n0_from,n0_to,n);im.getsetsubmatrixadd(src.im,n0_from,n0_to,n);
	}
	void setsubmatrix(const qscmatrix & src,cu32 n0){
		re.setsubmatrix(src.re,n0);im.setsubmatrix(src.im,n0);
	}
	void setsubmatrixadd(const qscmatrix & src,cu32 n0){
		re.setsubmatrixadd(src.re,n0);im.setsubmatrixadd(src.im,n0);
	}
	void setsubmatrix(const qcmatrix & src,cu32 y0,cu32 x0);
	void setsubmatrixadd(const qcmatrix & src,cu32 y0,cu32 x0);
};


//***********************************************************************
class qcmatrix{
//***********************************************************************
	friend class qscmatrix;
	friend class qcvector;
	qrmatrix re,im;
	void strasseninv();
public:
	void free(){re.free();im.free();}
	void resize(cu32 r,cu32 c){re.resize(r,c);im.resize(r,c);}
	u32 getsiz()const{return re.getsiz();}
	u32 getrow()const{return re.getrow();}
	u32 getcol()const{return re.getcol();}
	u32 getMemUsage(){return re.getMemUsage()+im.getMemUsage()+sizeof(*this);;}
	void copy(const qcmatrix & src){re.copy(src.re);im.copy(src.im);}
	void swap(qcmatrix & src){re.swap(src.re);im.swap(src.im);}
	void print(bool matrixkent=false)const{re.print(matrixkent);im.print(matrixkent);}
	void zero(cd Re=0.0,cd Im=0.0){re.zero(Re);im.zero(Im);}
	void neg(){re.neg();im.neg();}
	void random(){re.random();im.random();}
	void novekvo(){re.novekvo();im.novekvo();}
	void add(const qcmatrix & src1,const qcmatrix & src2){re.add(src1.re,src2.re);im.add(src1.im,src2.im);}
	void addnr(const qcmatrix & src1,const qcmatrix & src2){re.addnr(src1.re,src2.re);im.addnr(src1.im,src2.im);}
	void pluszegyenlo(const qcmatrix & src1){re.pluszegyenlo(src1.re);im.pluszegyenlo(src1.im);}
	void sub(const qcmatrix & src1,const qcmatrix & src2){re.sub(src1.re,src2.re);im.sub(src1.im,src2.im);}
	void subnr(const qcmatrix & src1,const qcmatrix & src2){re.subnr(src1.re,src2.re);im.subnr(src1.im,src2.im);}
	void minuszegyenlo(const qcmatrix & src1){re.minuszegyenlo(src1.re);im.minuszegyenlo(src1.im);}
	void save(FILE *fp)const{re.save(fp);im.save(fp);}
	void savetext(const char * reFileName,const char * imFileName)const{re.savetext(reFileName);im.savetext(imFileName);}
	void load(FILE *fp){re.load(fp);im.load(fp);}
	void store(FILE *fp){re.store(fp);im.store(fp);}
	//***********************************************************************
	void egyseg(){re.egyseg();im.egyseg();}
	void set(cu32 r,cu32 c,cqc v){re.set(r,c,v.re);im.set(r,c,v.im);}
	qcomplex get(cu32 r,cu32 c)const{return qcomplex(re.get(r,c),im.get(r,c));}
	void inc(cu32 r,cu32 c,cqc v){re.inc(r,c,v.re);im.inc(r,c,v.im);}
	void inc00(cqc v){re.inc00(v.re);im.inc00(v.im);}
	void transp(const qcmatrix & src){re.transp(src.re);im.transp(src.im);}
	void convert(const qscmatrix & src){re.convert(src.re);im.convert(src.im);}
	void tmul(const qcmatrix & src1,const qcmatrix & src2,const bool adde=false);
	void tmul(const qscmatrix & src1,const qcmatrix & src2,const bool adde=false);
	void tmul(const qcmatrix & src1,const qscmatrix & src2,const bool adde=false);
	void inv();
	void submatrixadd(const qcmatrix & src1,cu32 y1,cu32 x1,const qcmatrix & src2,cu32 y2,cu32 x2,cu32 y,cu32 x){
		re.submatrixadd(src1.re,y1,x1,src2.re,y2,x2,y,x);im.submatrixadd(src1.im,y1,x1,src2.im,y2,x2,y,x);
	}
	void submatrixsub(const qcmatrix & src1,cu32 y1,cu32 x1,const qcmatrix & src2,cu32 y2,cu32 x2,cu32 y,cu32 x){
		re.submatrixsub(src1.re,y1,x1,src2.re,y2,x2,y,x);im.submatrixadd(src1.im,y1,x1,src2.im,y2,x2,y,x);
	}
	void getsubmatrix(const qcmatrix & src,cu32 y0,cu32 x0,cu32 y,cu32 x){
		re.getsubmatrix(src.re,y0,x0,y,x);im.getsubmatrix(src.im,y0,x0,y,x);
	}
	void getsetsubmatrix(const qscmatrix & src,cu32 y0_from,cu32 x0_from,cu32 y0_to,cu32 x0_to,cu32 y,cu32 x){
		re.getsetsubmatrix(src.re,y0_from,x0_from,y0_to,x0_to,y,x);im.getsetsubmatrix(src.im,y0_from,x0_from,y0_to,x0_to,y,x);
	}
	void setsubmatrix(const qcmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrix(src.re,y0,x0);im.setsubmatrix(src.im,y0,x0);
	}
	void setsubmatrixadd(const qcmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrixadd(src.re,y0,x0);im.setsubmatrixadd(src.im,y0,x0);
	}
	void getsubmatrix(const qscmatrix & src,cu32 y0,cu32 x0,cu32 y,cu32 x){
		re.getsubmatrix(src.re,y0,x0,y,x);im.getsubmatrix(src.im,y0,x0,y,x);
	}
	void setsubmatrix(const qscmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrix(src.re,y0,x0);im.setsubmatrix(src.im,y0,x0);
	}
	void setsubmatrixadd(const qscmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrixadd(src.re,y0,x0);im.setsubmatrixadd(src.im,y0,x0);
	}
	void setsubmatrixneg(const qcmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrixneg(src.re,y0,x0);im.setsubmatrixneg(src.im,y0,x0);
	}
	void setsubmatrixnegadd(const qcmatrix & src,cu32 y0,cu32 x0){
		re.setsubmatrixnegadd(src.re,y0,x0);im.setsubmatrixnegadd(src.im,y0,x0);
	}
	//***********************************************************************

	//***********************************************************************
	void mul(const qcmatrix & src1,const qcmatrix & src2){
	//***********************************************************************
#ifdef vsundebugmode
		logprint("qcmatrix::mul => nem hatekony");
#endif
		qcmatrix r;
		r.transp(src2);
		tmul(src1,r);
	}

};

//***********************************************************************
inline void qscmatrix::mul(const qcmatrix & src1,const qcmatrix & src2){
//***********************************************************************
#ifdef vsundebugmode2
	logprint("qscmatrix::mul => nem hatekony");
#endif
	qcmatrix r;
	r.transp(src2);
	tmul(src1,r);
}

//***********************************************************************
inline void qscmatrix::setsubmatrix(const qcmatrix & src,cu32 y0,cu32 x0){
//***********************************************************************
	re.setsubmatrix(src.re,y0,x0);im.setsubmatrix(src.im,y0,x0);
}


//***********************************************************************
inline void qscmatrix::setsubmatrixadd(const qcmatrix & src,cu32 y0,cu32 x0){
//***********************************************************************
	re.setsubmatrixadd(src.re,y0,x0);im.setsubmatrixadd(src.im,y0,x0);
}


//***********************************************************************
class qcmtmulszal:public PLThread{
//***********************************************************************
	static qcmatrix cdummy;
	static qscmatrix scdummy;
	const qcmatrix &s1,&s2;
	const qscmatrix &s3,&s4;
	qcmatrix &d;
	qscmatrix &d2;
	cu32 mode;
public:
	qcmtmulszal(const qcmatrix &src1,const qcmatrix &src2,qcmatrix &dest)
		:s1(src1),s2(src2),d(dest),mode(1),s3(scdummy),s4(scdummy),d2(scdummy){}

	qcmtmulszal(const qscmatrix &src1,const qcmatrix &src2,qcmatrix &dest)
		:s3(src1),s2(src2),d(dest),mode(2),s1(cdummy),s4(scdummy),d2(scdummy){}

	qcmtmulszal(const qcmatrix &src1,const qcmatrix &src2,qscmatrix &dest)
		:s1(src1),s2(src2),d2(dest),mode(3),s3(scdummy),s4(scdummy),d(cdummy){}

	qcmtmulszal(const qcmatrix &src1,const qscmatrix &src2,qcmatrix &dest)
		:s1(src1),s3(src2),d(dest),mode(4),s2(cdummy),s4(scdummy),d2(scdummy){}

	qcmtmulszal(const qscmatrix &src1,const qscmatrix &src2,qscmatrix &dest)
		:s3(src1),s4(src2),d2(dest),mode(5),s1(cdummy),s2(cdummy),d(cdummy){}


	void run(){
		switch(mode){
			case 1: d.tmul(s1,s2); break;
			case 2: d.tmul(s3,s2); break;
			case 3: d2.tmul(s1,s2); break;
			case 4: d.tmul(s1,s3); break;
			case 5: d2.tmul_vigyazz(s3,s4); break;
		}
	}
};


#endif
