//***********************************************************************
// h�l�felt�lt� �s futtat� g�p header
// Creation date:  2009. 08. 07.
// Creator:        Pohl L�szl�
//***********************************************************************


//***********************************************************************
#ifndef VSUN3_MASINA_HEADER
#define	VSUN3_MASINA_HEADER
//***********************************************************************


//***********************************************************************
#include "tipusok.h"
#include "apa.h"
//***********************************************************************

//***********************************************************************
class drhalozat;
class qrhalozat;
class dchalozat;
class qchalozat;
class drcella;
class qrcella;
class dccella;
class qccella;
//***********************************************************************


//***********************************************************************
struct fillpar{
//***********************************************************************
    uns azon;                 // uns
    bool is_ac;               // bool, dc vagy ac
    bool is_quad;             // bool, sim-b�l kinyerhet�, de �gy egyszer�bb
    uns u_azon,i_azon,T_azon; // uns
    uns old_u_azon,old_T_azon,old_i_azon,older_u_azon,older_T_azon,older_i_azon;// uns
    uns anal_index;           // a sim hanyadik anal�zise
//    bool is_set_uT;           // bool, csak akkor �ll�tja be a kezd�cell�k u �s ub vektorait, ha true (iter�ci�n�l nem kell)
    bool is_current;          // bool, ha kor�bban kisz�molt �ramokat akarok haszn�lni a f�lvezet�shez
    bool is_semi_init;        // bool, ha ez egy f�lvezet�t tartalmaz� strukt�ra inicializ�ci�ja
    bool is_start_map;        // bool, ha a kezd��rt�ket m�trixb�l kell venni, vagy k�ls� h�m/ground
    bool is_el_th_Seebeck;    // bool, elektromos->termikus csatol�st sz�moljon (disszip�ci� n�lk�li r�sz)
    bool is_el_th_diss;       // bool, disszip�ci�t sz�moljon
    bool is_th_el_Seebeck;    // bool, termikus->elektromos csatol�st sz�moljon
    bool is_transi;           // bool, DC esetben true, ha tranzienset kell sz�m�tani
    bool is_timeconst;        // bool, ha false: akkor bode vagy ac, ha true: id��lland� spektrum
    bool is_fim_text;         // bool, ha true, akkor a fimet txt-be is menti
    dbl t,dt;                 // dbl, tranziensn�l
    dbl f;                    // dbl, ac,bode
    uns anal_step;            // uns, tranziens, bode, id��lland� hanyadik l�p�se van (a sre000x.fum-hez a sorsz�m)
    dcomplex s;               // dcomplex, komplex frekvencia id��lland� spektrumhoz
    MezoTipus tipus;          // MezoTipus, FieldEl �s FieldTherm lehet, FieldElTherm nem!
    simulation * sim;         // simulation *, peremfelt�telek miatt, az anal�zist a szimul�ci� szedi sz�t
    void clear(){
        is_start_map=is_current=is_semi_init=is_quad=/*is_set_uT=*/is_el_th_diss=is_el_th_Seebeck=is_th_el_Seebeck=is_ac=is_transi=is_timeconst=false;
        older_u_azon=older_T_azon=older_i_azon=old_u_azon=old_T_azon=old_i_azon=u_azon=i_azon=T_azon=anal_index=0;
        s=dt=t=f=nulla;
        anal_step=1;
        tipus=FieldEl;
        sim=NULL;
    }
    fillpar(){clear();}
};

//***********************************************************************
class AramKep;
//***********************************************************************

//***********************************************************************
class masina{
//***********************************************************************
    tomb<drhalozat*> t_dr;
    tomb<dchalozat*> t_dc;
    tomb<qrhalozat*> t_qr;
    tomb<qchalozat*> t_qc;
    tomb<tomb3d<real_cell_res>*> t_drmap;
    tomb3d<real_cell_res> t_drI_temp,t_drP_temp; // �ramt�rk�phez
    tomb<tomb3d<dcomplex_cell_res>*> t_dcmap;
    fillpar par,old_par;
    tomb<dbl> Ge_map, Ge_old_map; // automatikus tranzienshez

    void foglal_halot_dr(); // par alapj�n
    void foglal_halot_dc(); // par alapj�n
    void foglal_halot_qr(); // par alapj�n
    void foglal_halot_qc(); // par alapj�n
    void kezdo_UT_dr();     // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
    void kezdo_UT_dc();     // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
    void kezdo_UT_qr();     // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
    void kezdo_UT_qc();     // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
    void fill_step0_adm_dr();  // a 0. szint� admittanciam�trixok felt�lt�se a par tartalma alapj�n
    void fill_step0_adm_dc();  // a 0. szint� admittanciam�trixok felt�lt�se a par tartalma alapj�n
    void fill_step0_adm_qr();  // a 0. szint� admittanciam�trixok felt�lt�se a par tartalma alapj�n
    void fill_step0_adm_qc();  // a 0. szint� admittanciam�trixok felt�lt�se a par tartalma alapj�n
    void fill_step0_curr_dr(const double * p_map = NULL); // a 0. szint� inhom �ram vektorok felt�lt�se a par tartalma alapj�n, p_map logitermn�l a gerjeszt�seket tartalmazza
    void fill_step0_curr_dc(); // a 0. szint� inhom �ram vektorok felt�lt�se a par tartalma alapj�n
    void fill_step0_curr_qr(); // a 0. szint� inhom �ram vektorok felt�lt�se a par tartalma alapj�n
    void fill_step0_curr_qc(); // a 0. szint� inhom �ram vektorok felt�lt�se a par tartalma alapj�n
    void get_csakcenter_matrix(uns mx_azon);
    void get_all_matrix_dr(uns u_mx_azon,bool is_i,uns i_mx_azon=0);
    void get_all_matrix_dc(uns u_mx_azon,bool is_i,uns i_mx_azon=0);
    void get_all_matrix_qr(uns u_mx_azon,bool is_i,uns i_mx_azon=0);
    void get_all_matrix_qc(uns u_mx_azon,bool is_i,uns i_mx_azon=0);
    void get_all_matrix(uns u_mx_azon,bool is_i,uns i_mx_azon=0);
    void fill_extern_adm_dr();  // az external csom�pontok dr_y_extern m�trix�t t�lti fel
    void fill_extern_adm_dc();  // az external csom�pontok dc_y_extern m�trix�t t�lti fel
    void fill_extern_adm_qr();  // az external csom�pontok qr_y_extern m�trix�t t�lti fel
    void fill_extern_adm_qc();  // az external csom�pontok qc_y_extern m�trix�t t�lti fel
    void fill_extern_curr_dr(); // az external csom�pontok dr_j_extern vektor�t t�lti fel
    void fill_extern_curr_dc(); // az external csom�pontok dc_j_extern vektor�t t�lti fel
    void fill_extern_curr_qr(); // az external csom�pontok qr_j_extern vektor�t t�lti fel
    void fill_extern_curr_qc(); // az external csom�pontok qc_j_extern vektor�t t�lti fel
public:
    ~masina();
    //***********************************************************************
    void push(){ old_par = par; }
    void pop(){ par=old_par; }
    //***********************************************************************
    void par_clear(uns ujazon){par.clear();par.azon=ujazon;}
    //void par_set_azon(uns ujazon){par.azon=ujazon;}
    void par_set_ac(bool b){par.is_ac=b;}                       // ha false, dc
    void par_set_quad(bool b){par.is_quad=b;}                   // ha false, double
    void par_set_pot_map(uns mapazon){par.u_azon=mapazon;}      // ha k�l�nb�z� fajt�j�ak, lehet ugyanaz az azonos�t�juk (double, quad, ac, dc)
    void par_set_old_pot_map(uns mapazon,uns older_mapazon){par.old_u_azon=mapazon;par.older_u_azon=older_mapazon;}// ha k�l�nb�z� fajt�j�ak, lehet ugyanaz az azonos�t�juk (double, quad, ac, dc)
    void par_set_temp_map(uns mapazon){par.T_azon=mapazon;}     // ha k�l�nb�z� fajt�j�ak, lehet ugyanaz az azonos�t�juk (double, quad, ac, dc)
    void par_set_old_temp_map(uns mapazon,uns older_mapazon){par.old_T_azon=mapazon;par.older_T_azon=older_mapazon;}// ha k�l�nb�z� fajt�j�ak, lehet ugyanaz az azonos�t�juk (double, quad, ac, dc)
    void par_set_curr_map(uns mapazon){par.i_azon=mapazon;}     // ha k�l�nb�z� fajt�j�ak, lehet ugyanaz az azonos�t�juk (double, quad, ac, dc)
    void par_set_old_curr_map(uns mapazon,uns older_mapazon){par.old_i_azon=mapazon;par.older_i_azon=older_mapazon;}
//    void par_set_start_UT(bool b){/*par.is_set_uT=b;*/}             // ha az el�z� iter�ci�ban kapott u/T �rt�keket haszn�ljuk, false!!! (ami u, ub vektorban maradt)
    void par_set_I_for_semi(bool b){par.is_current=b;}          // ha az el�z� iter�ci�ban kapott I �rt�keket haszn�ljuk f�lvezet�sn�l, true!!! (i_azon t�rol�b�l)
    void par_set_semi_init(bool b){par.is_semi_init=b;}         // ha most f�lvezet�s strukt�r�t inicializ�lunk
    void par_set_start_from_map(bool b){par.is_start_map=b;}    // ha true, akkor mapb�l�ll�tja a kezd��rt�ket, fill is haszn�lja, false eset�n ground/ambient �rt�ket �ll�t
    void par_set_el_th_Seebeck(bool b){par.is_el_th_Seebeck=b;} // elektromos->termikus csatol�st sz�moljon (disszip�ci� n�lk�li r�sz)
    void par_set_el_th_diss(bool b){par.is_el_th_diss=b;}       // disszip�ci�t sz�moljon
    void par_set_th_el_Seebeck(bool b){par.is_th_el_Seebeck=b;} // ermikus->elektromos csatol�st sz�moljon
    void par_set_transi(bool b){par.is_transi=b;}               // dc esetben tranzienset sz�molunk
    void par_set_timeconst(bool b){par.is_timeconst=b;}         // ac esetben id��lland�t sz�molunk
    void par_set_t(dbl t){par.t=t;}                             // a tranziens dt-je
    void par_set_dt(dbl dt){par.dt=dt;}                         // a tranziens dt-je
    void par_set_anal_step(uns n){par.anal_step=n;}             // a tranziens, bode, id��lland� aktu�lis l�p�s�nek sorsz�ma
    void par_set_f(dbl f){par.f=f;}                             // ac frekvencia
    void par_set_s(dcomplex s){par.s=s;}                        // timeconst komplex frekvencia
    void par_set_sim(simulation * sim){par.sim=sim;}
    void par_set_tipus(MezoTipus tipus){par.tipus=tipus;}
    void par_set_anal_index(uns anal_index){par.anal_index=anal_index;} // A tanal t�mb h�nyas index� anal�zise fut
    void par_set_fim_txt(bool b){par.is_fim_text=b;}            // �rjunk ki txt-be is h�m/fesz t�rk�pet
    //***********************************************************************
    void del_all();
    void del_azon();        // par alapj�n d�l el, hogy melyik t�mbben t�rli!
    void foglal_halot(uns step);    // par alapj�n
    void foglal_matrixot(uns step); // a h�l� nem foglalja ezeket
    void kezdo_UT(uns step,const char * s);        // par alapj�n be�ll�tja a kezd��rt�ket, csak a k�z�ppont fesz�lts�g�t �ll�tja be, a t�bbit null�zza
    void fill_step0_adm(uns step);  // a 0. szint� admittanciam�trixok felt�lt�se a par tartalma alapj�n
    void fill_step0_curr(uns step,const double * p_map = NULL); // a 0. szint� inhom �ram vektorok felt�lt�se a par tartalma alapj�n
    void nodered(bool is_always_strassen_mul);         // par.azon, a cella automatikusan h�vja forwsubsot is
    void forwsubs(uns step);        // par.azon
    void backsubs(uns step,bool aramot);        // par.azon
    void get_u_csakcenter_matrix(){get_csakcenter_matrix(par.u_azon);} // par.azon, par.u_azon, csak a k�z�ppont fesz�lts�g�t olvassa ki a map-be, a t�bbi �rt�ket nem v�ltoztatja
    void get_T_csakcenter_matrix(){get_csakcenter_matrix(par.T_azon);} // par.azon, par.T_azon, csak a k�z�ppont h�m�rs�klet�t olvassa ki a map-be, a t�bbi �rt�ket nem v�ltoztatja
    void get_u_matrix(uns step){setAnalKeszStep(0,step,"get V");get_all_matrix(par.u_azon,false);} // par.azon, par.u_azon
    void get_T_matrix(uns step){setAnalKeszStep(0,step,"get T");get_all_matrix(par.T_azon,false);} // par.azon, par.T_azon
    void get_u_i_matrix(uns step){setAnalKeszStep(0,step,"get V and I");get_all_matrix(par.u_azon,true,par.i_azon);} // par.azon, par.u_azon, par.i_azon, egy�tt sz�m�tja ki a kett�t, mert �gy egyszer�
    void write_fim(const PLString & path,uns step,bool iternormal,uns analtype,bool restore=false); // A r�gi sunred fim f�jljait �rja
    dbl get_xyz_r(uns x,uns y,uns z){return t_drmap[par.tipus==FieldEl?par.u_azon:par.T_azon]->getconstref(x,y,z).t[EXTERNAL];} // U vagy T kiolvas�sa adott 
    dcomplex get_xyz_c(uns x,uns y,uns z){return t_dcmap[par.tipus==FieldEl?par.u_azon:par.T_azon]->getconstref(x,y,z).t[EXTERNAL];} // U vagy T kiolvas�sa adott 
    bool check_error(uns drmap_azon_1,uns drmap_azon_2,dbl maxerror); // ellen�rzi, hogy a k�t (csakcenter) map relat�v k�l�nbs�ge hibahat�ron bel�l van-e, false: k�v�l van, true: j�
    dbl  get_max_error(uns drmap_azon_1,uns drmap_azon_2); // visszaadja a k�t (csakcenter) map maxim�lis relat�v k�l�nbs�g�t
//    void swap_UT(){cuns swp=par.u_azon;par.u_azon=par.T_azon;par.T_azon=swp;} // egyter� nonlinn�l cser�lgetj�k, hogy ne keljen fejben tartani az azonos�t�t
    void swap_u(){cuns swp=par.u_azon;par.u_azon=par.older_u_azon;par.older_u_azon=par.old_u_azon;par.old_u_azon=swp;} // elterm nonlinn�l cser�lgetj�k, hogy ne keljen fejben tartani az azonos�t�t
    void swap_T(){cuns swp=par.T_azon;par.T_azon=par.older_T_azon;par.older_T_azon=par.old_T_azon;par.old_T_azon=swp;} // elterm nonlinn�l cser�lgetj�k, hogy ne keljen fejben tartani az azonos�t�t
    void swap_back_T() { swap_T(); swap_T(); } // vissza�ll�tjuk az el�z� T-t tranziensn�l. Az older_T k�zben elromlott, de itt nincs jelent�s�ge.
    // nincs �ram swap, mert a get_all_matrix fv. haszn�lja az �ramot �s egyb�l fel�l is �rja, ez�rt a konvergens_U_T_szamito m�solja �t az �ramokat megfelel� m�don
    uns get_u_azon()const {return par.u_azon;}
    uns get_T_azon()const {return par.T_azon;}
    uns get_old_u_azon()const {return par.old_u_azon;}
    uns get_old_T_azon()const {return par.old_T_azon;}
    uns get_old_i_azon()const {return par.old_i_azon;}
    uns get_older_u_azon()const {return par.older_u_azon;}
    uns get_older_T_azon()const {return par.older_T_azon;}
    uns get_older_i_azon()const {return par.older_i_azon;}
    void konvergens_U_T_szamito(cd szorzo); // a t_konv_map-ba teszi az u vagy T el�z� h�rom iter�ci�b�l saccolt k�zel�t� �rt�k�t
    void set_aramkep_fej(AramKep &k)const;
    void get_center_values(tomb<dbl> & cel)const; // a megfele� t_drmap-b�l cel-ba olvassa a k�z�ppontok m�reteit. cel-t �tm�retezi

    //***********************************************************************
    dbl get_min_ido_arany()const{
    //***********************************************************************
        model & mod=*par.sim->pmodel;
        const tomb3d<real_cell_res> *ptemp = t_drmap[ par.T_azon ];
        cd fold = par.sim->ambiT;
        cuns db = mod.x_res * mod.y_res * mod.z_res;
        dbl min_ido_arany=egy, akt_ido_arany;
        for( uns i = 0; i < db; i++ ){
            akt_ido_arany = mod.tseged[i].get_ido_arany( ptemp->getconstref(i).t[EXTERNAL] + fold );
            if( akt_ido_arany < min_ido_arany )
                min_ido_arany = akt_ido_arany;
        }
        return min_ido_arany;
    }
    //***********************************************************************
    
    //***********************************************************************
    bool is_Ge_error(dbl max_error) {
    //***********************************************************************
        for (uns i = 0; i < Ge_map.size(); i++) {
            if (Ge_old_map[i] == nulla)
                continue;
            if (fabs((Ge_map[i] - Ge_old_map[i]) / fabs(Ge_old_map[i]))>max_error)
                return true;
        }
        return false;
    }
    //***********************************************************************
    void swap_Ge() { Ge_old_map.swap(Ge_map); }
    //***********************************************************************

    //***********************************************************************
    void logitherm_get_top_T_map(double * t_map){ // t_map-ba ker�l az eredm�ny, el�tte le kell futtatni a get_T_matrix()-ot
    //***********************************************************************
        const simulation & sim=*par.sim;
        const model & mod=*par.sim->pmodel;
        cuns x=mod.x_res, y=mod.y_res, z=mod.z_res;
        const tomb3d<real_cell_res> *ptemp = t_drmap[ par.T_azon ];
        cd fold = sim.ambiT;
        cuns db = x * y * z; // �t�rva innen 2014.06.20., kor�bban csak a legfels� r�teg h�m�rs�klet�t mentette
        for (uns i = 0; i < db; i++)
            t_map[i] = ptemp->getconstref(i).t[TOP] + fold;
//        cuns db = x * y;
//        cuns dr_start = ( z - 1) * db;
//        for( uns i = 0; i < db; i++ )
//            t_map[i] = ptemp->getconstref(dr_start + i).t[TOP] + fold;
    }
    //***********************************************************************

    //************** Eddig k�sz *********************************************
    void dcmap2drmap(uns dcmap_azon,uns drmap_azon); // dcsd bemenete drmap, sz�m�ra �t kell konvert�lni
};


//***********************************************************************
#endif
//***********************************************************************
