//***********************************************************************
// general function header
// Creation date:  2009. 07. 12.
// Creator:        Pohl L�szl�
//***********************************************************************


//***********************************************************************
#ifndef VSUN3_GFUNC_HEADER
#define	VSUN3_GFUNC_HEADER
//***********************************************************************


//***********************************************************************
#include "tipusok.h"
//***********************************************************************


//***********************************************************************
inline double replusz(const double a,const double b){return a==nulla ? nulla : b==nulla ? nulla : a==g_max ? b : b==g_max ? a : a*b/(a+b);}
inline dcomplex replusz(const dcomplex a,const dcomplex b)
    {return (a.re==nulla&&a.im==nulla) ? nulla : (b.re==nulla&&b.im==nulla) ? nulla : a.re==g_max ? b : b.re==g_max ? a : a*b/(a+b);}
inline qcomplex replusz(const qcomplex a,const qcomplex b)
    {return (a.re==qnulla&&a.im==qnulla) ? qnulla : (b.re==qnulla&&b.im==qnulla) ? qnulla : a.re==g_max ? b : b.re==g_max ? a : a*b/(a+b);}
inline dbl negyzet(dbl a){return a*a;}
void GetTime(PLString & ret);
PLString GetTmpName();
void printTime(const char * muvelet,const char * fajlnev,const char *projektnev,unsigned db);
void logprint(const char * s,...);
const char * getlogline(); // GUI-b�l h�vhat�
void resetAnalLevel(unsigned level);
void setAnalStepDb(unsigned level,unsigned db);
void setAnalKeszStep(unsigned level,unsigned n,const char * kovlepesnev="");
unsigned getAnalProgress(unsigned level);
inline dbl fn_mizs1(dbl T, dbl a, dbl b, dbl c, dbl d, dbl e, dbl f, dbl g) {
    T += absT;
    cd Tbc = pow(T - b, c);
    cd signa = T > b ? -a : a;
    cd tag1 = signa * Tbc / (Tbc + d) + a + f;
    return 0.01*(tag1*exp(e / T) + g);
}
//***********************************************************************


//***********************************************************************
#endif
//***********************************************************************
