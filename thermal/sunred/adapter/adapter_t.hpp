#ifndef _SUNRED_ADAPTER_
#define _SUNRED_ADAPTER_

#include <tuple>
#include <string>
#include <memory>
#include <unordered_map>
#include <unordered_set>
#include <functional>
#include <sstream>
#include <iomanip>
#include <algorithm>

// #include "thermal/sunred/engine/sunred.h"

#include "thermal/base/adapter_t.hpp"
#include "thermal/sunred/adapter/material_t.hpp"
#include "thermal/sunred/adapter/layout/adapter_t.hpp"
#include "thermal/sunred/engine/tipusok.h"

#include "unit/all_units.hpp"

namespace thermal
{
	namespace sunred
	{
		enum class boundary_t{temperature, current, adiabatic, conductive};
		enum class side_t{east, south, west, north, top, bottom};
		//extern const char* side_t_string [];
	}
}

/** hashelheto legyen a fentebbi side_t **/
namespace std
{
	template<>
	struct hash<thermal::sunred::side_t>
	{
		size_t operator() (thermal::sunred::side_t side) const
		{
			return static_cast<std::underlying_type<thermal::sunred::side_t>::type>(side);
		}
		
	};

	template<>
	struct hash<thermal::sunred::boundary_t>
	{
		size_t operator() (thermal::sunred::boundary_t boundary) const
		{
			return static_cast<std::underlying_type<thermal::sunred::boundary_t>::type>(boundary);
		}
		
	};
}

/** nehany sunred osztaly forward decl. **/
class stackedInterface;
class apa;


namespace thermal
{
	namespace sunred
	{
		class adapter_t : public thermal::adapter_t
		{
			public:
				using xy_length_t = layout::xy_length_t;
				using xy_pitch_t = layout::xy_pitch_t;
			
			private:				
				unit::temperature_t ambient_temperature;
				
				double* temperature_map;
				double* dissipation_map;
				
				unit::length_t x_length;
				unit::length_t y_length;

				size_t x_pitch;
				size_t y_pitch;
				size_t z_pitch;
				size_t size;
				
				
				/** layout x es y iranyu merete **/ 
				xy_length_t layout_size;
				
				/** cella x es y iranyu merete **/
				xy_length_t cell_size;

				unit::length_t pixel_size;
								
				apa *logitherm_apa_; /**< The simulator engine instance (dynamically allocated and freed by \ref thermal::sunred::thermal_simulator). **/
				
				std::string model_file_path;
				std::string model_file_name;
				std::string uchannel_file_name;
				bool init_sunred_from_file;
				
				std::unordered_map<std::string, std::string> layers_materials; /** a struktura layer azonositoi, es a hozzajuk tartozo anyagok. **/
				std::unordered_map<std::string, material_t> materials_container; /** az anyagok tulajdonsagait tarolo valtozo **/
				std::unordered_map<std::string, std::string> floorplans;  /** key==floorplan neve, tartalom== floorplan_file **/

				std::unordered_map<side_t,std::tuple<PeremTipus, double>> boundary_conditions;

				static std::unordered_map<side_t, std::string> side_t_2_string;				
				static std::unordered_map<std::string, side_t> string_2_side_t;
				static std::unordered_map<boundary_t, PeremTipus> boundary_t_2_sunred;
				static std::unordered_map<std::string, boundary_t> string_2_boundary_t;

			private:
				/**
				 * ez fuggveny a szimulalando (3D) IC felepitese alapjan a termikus motor igenyei
				 * szerint lefoglalja a szukseges eroforrasokat, beallitja a the_layout, the_dissipation_map
				 * es a the_temperature_map pointereket
				**/
				void create_structure() override;

				/**
				 * ez a fuggveny egy szimulacios lepesnel a logitherm_proxy adataibol kiszamitja, hogy milyen pozicioban mennyit disszipalt a chip
				**/ 
				void refresh_dissipation_map(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>&) override;
				
				/**
				 * ez a fuggveny kinyeri a termikus motor altal kiszamitott homerseklet eloszlasbol az egyes modulok homersekletet
				**/
				void refresh_component_temperature(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>&) override;
			
			public:
				adapter_t() = delete;
				adapter_t(logitherm::manager_t* manager, util::log_t* log, const std::string& id, const util::timestep_t& timestep);
				adapter_t(const adapter_t& other, const std::string& id, const util::timestep_t& timestep);

				~adapter_t();

				void initialize_sunred_from_file(const std::string& path, const std::string& sunredfile, const xy_length_t& size, const xy_pitch_t& resolution);
				void initialize_uchannel_from_file(const std::string& path, const std::string& uchannelfile);
				
				void add_boundary_condition(side_t, boundary_t type, double value);

				/**
				 * ennek a fuggvenynek kell kiszamitania az idolepesben az uj homerseklet adatokat
				**/ 
				void calculate_temperatures() override;
				
				void set_ambient_temperature(const ::unit::temperature_t& amb);

				void set_pixel_size(::unit::length_t size);
				
				/**
				 * ez a fuggveny kinyeri a komponensek homersekletet (min, max, avg, center)
				**/
				unit::temperature_t get_min_temperature(layout::component_t* component) override;
				unit::temperature_t get_max_temperature(layout::component_t* component) override;
				unit::temperature_t get_avg_temperature(layout::component_t* component) override;
				std::ostream& get_layer_temperature(std::ostream& os, const std::string& layer_id) override;
				
				void create_layout(const xy_length_t&, const xy_pitch_t&);
				
				/**
				 * uj layert ad a strukturahoz, anyag azonositoval es vastagsaggal
				**/				
				void add_layer(const std::string& layer_name, const std::string& material_name, const unit::length_t& thickness);
				
				/**
				 * ezzel a fv-el lehet beallitani a 'kockak' meretet, illetve a timestep-et
				**/ 
				//void set_layout_size(const std::tuple<::unit::length_t, ::unit::length_t>& size);
				
				//void set_layout_resolution(const std::tuple<size_t, size_t>& resolution);
				
				//const std::tuple<::unit::length_t, ::unit::length_t>& get_layout_size () const;
				
				//const std::tuple<size_t, size_t>& get_layout_resolution () const;

				//const std::tuple<::unit::length_t, ::unit::length_t> get_cell_size () const;
				
				/**
				 * hozzaad egy uj materialt a termikus motorhoz
				 * a leszarmazott termikus motornak ezt implementalnia kell
				 * ellenorzes: material nev egyedi
				**/
				void add_material(std::string const &name, double thermal_conductivity, double thermal_capacity);


				//visszater side_t tipussal boundary string alapjan
				//hibat dob, ha nem tudja a stringet megfeleltetni side_t-nek
				side_t get_side(const std::string& side);

				//visszater stringgel side_t alapjan, hibak kiirasahoz hasznalatos
				const std::string& get_side(side_t side);

				//visszater a string alapjan a boundary-vel
				//hibat dob, ha ismeretlen a boundary
				boundary_t get_boundary(const std::string& boundary);
				
				std::ostream& print_temperature_map_in_svg(std::ostream& os, std::unordered_set<layout::component_t*, std::hash<layout::component_t*>, layout::components_equal>& components) override;
				
				std::ostream& print_temperature_map(std::ostream& os) override;
				
				std::tuple<double, double> get_temperature_range(const std::string&) const;
				
				std::ostream& print_dissipation_map(std::ostream& os) override;

				void read_files(const std::string& path, const std::string& initfile) override;
		};
	}
}

#endif // _SUNRED_ADAPTER_

