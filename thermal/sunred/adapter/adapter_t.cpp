#include "thermal/sunred/adapter/adapter_t.hpp"
#include "manager/manager_t.hpp"

#include "thermal/sunred/engine/sunred.h"

#include "file_io/sloth_sunred/adapter_t.hpp"

namespace thermal
{
	namespace sunred
	{		

		//const char* side_t_string [] = {"east", "south", "west", "north", "top", "bottom"};

		std::unordered_map<side_t, std::string> adapter_t::side_t_2_string = {{side_t::east, "east"},
																		{side_t::south, "south"},
																		{side_t::west, "west"},
																		{side_t::north, "north"},
																		{side_t::top, "top"},
																		{side_t::bottom,"bottom"}};
				
		std::unordered_map<std::string, side_t> adapter_t::string_2_side_t = {{"east",side_t::east},
																		{"south", side_t::south},
																		{"west", side_t::west},
																		{"north", side_t::north},
																		{"top", side_t::top},
																		{"bottom", side_t::bottom}};

		std::unordered_map<boundary_t, PeremTipus> adapter_t::boundary_t_2_sunred = {{boundary_t::adiabatic, PeremOpen},
																					   {boundary_t::temperature, PeremU},
																					   {boundary_t::conductive, PeremR}};
		
		std::unordered_map<std::string, boundary_t> adapter_t::string_2_boundary_t = {{"adiabatic", boundary_t::adiabatic},
																				{"temperature", boundary_t::temperature},
																				{"conductive", boundary_t::conductive}};

		void adapter_t::create_layout(const xy_length_t& size, const xy_pitch_t& pitch)
		{
			layout_structure = new layout::sunred::adapter_t(this, log, size, pitch);

			auto* sunred_layout = dynamic_cast<layout::sunred::adapter_t*>(layout_structure);
			
			layout_size = xy_length_t(sunred_layout->get_length()[0],sunred_layout->get_length()[1]);
			cell_size = xy_length_t(layout_size[0]/pitch[0],layout_size[1]/pitch[1]);
		}
		
		void adapter_t::add_boundary_condition(side_t side, boundary_t type, double value)
		{
			if(boundary_conditions.find(side) != boundary_conditions.end())
			{
				warning("add_boundary_condition(): boundary condition to '" + get_side(side) + "' is already set");
			}
			else
			{
				boundary_conditions.insert(std::make_pair(side,std::make_tuple(boundary_t_2_sunred.at(type), value)));
			}
		}
		
		/**
		 * ez fuggveny a szimulalando (3D) IC felepitese alapjan a termikus motor igenyei
		 * szerint lefoglalja a szukseges eroforrasokat, beallitja a fenti 3 pointert
		**/ 
		void adapter_t::create_structure()
		{
			layout::sunred::adapter_t* layout_ptr = dynamic_cast<layout::sunred::adapter_t*>(layout_structure);
			if(nullptr == layout_ptr) error("create_structure(): invalid layout format or layout has not been created.");
			
			x_pitch = layout_ptr->get_pitch()[0];
			y_pitch = layout_ptr->get_pitch()[1];
			z_pitch = layout_ptr->get_pitch()[2];

			if((x_pitch == 0) || (y_pitch == 0)) error("create_structure(): layout size must be greater then 0,0.");
			if (z_pitch == 0) error("create_structure(): there must be at least 1 layer");
			
			size = x_pitch*y_pitch*z_pitch;

			// Allocating and initializing the dissipation and temperature maps
			dissipation_map = new double[size];
			std::fill(dissipation_map, dissipation_map + size, 0.0);
			
			temperature_map = new double[size];
			std::fill(temperature_map, temperature_map + size, static_cast<double>(ambient_temperature));

			// Creating SUNRED boundary conditions
			std::tuple<PeremTipus, double> West_boundary = (boundary_conditions.find(side_t::west) != boundary_conditions.end()) ? boundary_conditions.at(side_t::west) : std::make_tuple(PeremOpen, 0.0);
			std::tuple<PeremTipus, double> East_boundary = (boundary_conditions.find(side_t::east) != boundary_conditions.end()) ? boundary_conditions.at(side_t::east) : std::make_tuple(PeremOpen, 0.0);
			std::tuple<PeremTipus, double> South_boundary = (boundary_conditions.find(side_t::south) != boundary_conditions.end()) ? boundary_conditions.at(side_t::south) : std::make_tuple(PeremOpen, 0.0);;
			std::tuple<PeremTipus, double> North_boundary = (boundary_conditions.find(side_t::north) != boundary_conditions.end()) ? boundary_conditions.at(side_t::north) : std::make_tuple(PeremOpen, 0.0);
			std::tuple<PeremTipus, double> Bottom_boundary = (boundary_conditions.find(side_t::bottom) != boundary_conditions.end()) ? boundary_conditions.at(side_t::bottom) : std::make_tuple(PeremOpen, 0.0);
			std::tuple<PeremTipus, double> Top_boundary = (boundary_conditions.find(side_t::top) != boundary_conditions.end()) ? boundary_conditions.at(side_t::top) : std::make_tuple(PeremOpen, 0.0);

			if(init_sunred_from_file)
			{
				/** 
				 * sunred inicializalasa sunred modelfile-on keresztul
				 * a fv apin keresztul beallitott anyagparameterek nincsenek figyelembeveve
				**/
				try
				{
					logitherm_apa_ = new apa(model_file_path.c_str(),
											 model_file_name.c_str(),
											 uchannel_file_name.empty() ? nullptr : uchannel_file_name.c_str(),
											 std::get<0>(West_boundary),	std::get<1>(West_boundary),
											 std::get<0>(East_boundary),	std::get<1>(East_boundary),
											 std::get<0>(South_boundary),	std::get<1>(South_boundary),
											 std::get<0>(North_boundary),	std::get<1>(North_boundary),
											 std::get<0>(Bottom_boundary),	std::get<1>(Bottom_boundary),
											 std::get<0>(Top_boundary),		std::get<1>(Top_boundary),
											 static_cast<double>(ambient_temperature),
											 static_cast<double>(timestep.current_timestep()),
											 timestep.is_linear(),
											 dissipation_map,
											 temperature_map);
				}
				catch(const hiba& error_msg)
				{
					error("create_structure(): error occured in sunred engine '" + std::string(error_msg.what()) + "'");
				}
			}
			else
			{
				/** sunred inicializalasa az adapter fv api-jan keresztul **/

				/** stacked interface segedosztaly a sunred peldanyositasahoz**/
				std::unique_ptr<stackedInterface> stacked_interface_;

				// Creating the stacked_interface dynamically
				if(layers_materials.size() != z_pitch) error("thermal::sunred::adapter_t::create_structure(): not every layer has material definition.");
				stacked_interface_ = std::unique_ptr<stackedInterface>(new stackedInterface(x_pitch, y_pitch, z_pitch, layers_materials.size()));
				
				/** grid meretek beallitasa **/
				for(size_t i = 0; i < x_pitch; i++) stacked_interface_->set_x_pitch(i, static_cast<double>(cell_size[0]));
				for(size_t i = 0; i < y_pitch; i++) stacked_interface_->set_y_pitch(i, static_cast<double>(cell_size[1]));
				for(size_t i = 0; i < z_pitch; i++) stacked_interface_->set_z_pitch(i, static_cast<double>(layout_ptr->get_layer_thickness(i)));

				/**
				 * anyagok hozzaadasa a sunred-hez
				 * elso parameter az anyag ID-je (size_t), utana az anyag neve, majd a hovezetese es hokapacitasa
				**/ 
				for (auto material_description: materials_container)
				{
					const material_t& a_material = material_description.second;
					stacked_interface_->set_mat(a_material.material_number, a_material.id.c_str(), a_material.thermal_conductivity, a_material.thermal_capacity);
				}
				
				/**
				 * layer hozzaadasa a sunred-hez
				 * a layert es a materialt is szam alapjan azonositja a sunred
				**/
				size_t layer_number = 0;
				for (auto a_layer_material: layers_materials)
				{
					size_t material_number = materials_container.at(a_layer_material.second).material_number;
					stacked_interface_->set_layer(layer_number++, material_number);
				}
				
				try
				{	
					// Creating the thermal engine (Sunred)
					logitherm_apa_ = stacked_interface_->new_apa(
						std::get<0>(West_boundary),		std::get<1>(West_boundary),
						std::get<0>(East_boundary),		std::get<1>(East_boundary),
						std::get<0>(South_boundary),	std::get<1>(South_boundary),
						std::get<0>(North_boundary),	std::get<1>(North_boundary),
						std::get<0>(Bottom_boundary),	std::get<1>(Bottom_boundary),
						std::get<0>(Top_boundary),		std::get<1>(Top_boundary),
						static_cast<double>(ambient_temperature), //ambient, egyenlore ennyi, majd kesobb dolgozom rajta
						static_cast<double>(timestep.current_timestep()),
						timestep.is_linear(),
						dissipation_map,
						temperature_map
					);

				}
				catch(const hiba& error_msg)
				{
					error("create_structure(): error occured in sunred engine '" + std::string(error_msg.what()) + "'");
				}
			}
		}

		void adapter_t::initialize_sunred_from_file(const std::string& filepath, const std::string& filename, const xy_length_t& size, const xy_pitch_t& resolution)
		{
			model_file_path = filepath;
			model_file_name = filename;
			init_sunred_from_file = true;
		}

		void adapter_t::initialize_uchannel_from_file(const std::string& path, const std::string& uchannelfile)
		{
			uchannel_file_name = uchannelfile;
		}
		
		adapter_t::adapter_t(logitherm::manager_t* manager, util::log_t* log, const std::string& id, const util::timestep_t& timestep)
		:
			thermal::adapter_t(manager, log, id, util::thermal_engine_t::sunred, timestep),
			ambient_temperature(300_K),
			temperature_map(nullptr),
			dissipation_map(nullptr),
			logitherm_apa_(nullptr),
			init_sunred_from_file(false),
			pixel_size(0.5_um)
		{}

		adapter_t::adapter_t(const adapter_t& other, const std::string& id, const util::timestep_t& timestep)
		:
			thermal::adapter_t(other.manager, other.log, id, util::thermal_engine_t::sunred, timestep),
			ambient_temperature(other.ambient_temperature),
			temperature_map(nullptr),
			dissipation_map(nullptr),
			x_length(other.x_length),
			y_length(other.y_length),
			x_pitch(other.x_pitch),
			y_pitch(other.y_pitch),
			z_pitch(other.z_pitch),
			size(other.size),
			layout_size(other.layout_size),
			cell_size(other.cell_size),
			pixel_size(other.pixel_size),
			logitherm_apa_(nullptr),
			model_file_path(other.model_file_path),
			model_file_name(other.model_file_name),
			uchannel_file_name(other.uchannel_file_name),
			init_sunred_from_file(other.init_sunred_from_file),
			layers_materials(other.layers_materials),
			materials_container(other.materials_container),
			floorplans(other.floorplans),
			boundary_conditions(other.boundary_conditions)
		{
			// create_layout_structure
			auto* layout_ptr = dynamic_cast<layout::sunred::adapter_t*>(other.layout_structure);
			if(nullptr == layout_ptr) error("create_structure(): invalid layout format or layout has not been created.");

			layout_structure = new layout::sunred::adapter_t(*layout_ptr, this, this->log);

			if(other.initialized() == false) error("adapter_t(other): other adapter was not initialized");

			initialize();
		}

		adapter_t::~adapter_t()
		{
			delete[] temperature_map;
			delete[] dissipation_map;
			delete logitherm_apa_;
		}
		
		/**
		 * ennek a fuggvenynek kell kiszamitania az idolepesben az uj homerseklet adatokat
		 * ebbol az osztalybol leszarmazo termikus motor adapter osztalynak ezt a fuggvenyt
		 * meg kell valositania
		**/ 
		void adapter_t::calculate_temperatures()
		{
			try
			{
				if(timestep.is_linear())
				{
					logitherm_apa_->logitherm_next_step(dissipation_map,temperature_map);
				}
				else
				{
					logitherm_apa_->logitherm_next_step(dissipation_map,temperature_map, static_cast<double>(timestep.current_timestep()));
				}
			}
			catch(const hiba& error_msg)
			{
				error("calculate_temperatures(): error occured in sunred engine '" + std::string(error_msg.what()) + "'");
			}
		}		
		
		void adapter_t::set_ambient_temperature(const unit::temperature_t& amb)
		{
			ambient_temperature = amb;
		}

		void adapter_t::set_pixel_size(unit::length_t size)
		{
			pixel_size = size;
		}

		void adapter_t::add_layer(const std::string& layer_name, const std::string& material_name, const unit::length_t& thickness)
		{
			::layout::sunred::adapter_t* layout_ptr = dynamic_cast<layout::sunred::adapter_t*>(layout_structure);
			if(nullptr == layout_ptr) error("add_layer(): invalid layout format or layout has not been created.");
			
			if(layers_materials.find(layer_name) != layers_materials.end())
			{
				warning("add_layer(): layer material is already defined.");
			}
			else
			{
				if(layout_ptr->find_layer(layer_name))
				{
					error("add_layer(): layer name '" + layer_name + "' is ambigous.");
				}
				if(materials_container.find(material_name) == materials_container.end())
				{
					error("add_layer(): unkown material '" + material_name + "'.");
				}
				
				layout_ptr->add_layer(layer_name, thickness);
				layers_materials.insert(std::make_pair(layer_name, material_name));
			}
			
		}
		
		void adapter_t::add_material(std::string const &name, double thermal_conductivity, double thermal_capacity)
		{
			if (materials_container.find(name) != materials_container.end())
			{
				warning("add_material(): material '" + name + "' already in the materials container.");
			}
			else
			{
				materials_container.insert(std::make_pair(name,material_t(name,materials_container.size(),thermal_conductivity,thermal_capacity)));
			}
		}
		
		side_t adapter_t::get_side(const std::string& side)
		{
			if(string_2_side_t.find(side) == string_2_side_t.end()) error("get_side(): unkown side '" + side + "'");
			return string_2_side_t.at(side);
		}

		const std::string& adapter_t::get_side(side_t side)
		{
			return side_t_2_string.at(side);
		}

		boundary_t adapter_t::get_boundary(const std::string& boundary)
		{
			if(string_2_boundary_t.find(boundary) == string_2_boundary_t.end()) error("get_boundary(): unkown boundary '" + boundary + "'");
			return string_2_boundary_t.at(boundary);
		}
		
		std::ostream& adapter_t::print_temperature_map_in_svg(std::ostream& os, std::unordered_set<layout::component_t*, std::hash<layout::component_t*>, layout::components_equal>& components)
		{
			if(nullptr != temperature_map)
			{
						
				struct color {
					unsigned red;
					unsigned green;
					unsigned blue;

					color(unsigned red = 0, unsigned green = 0, unsigned blue = 0) : red(red), green(green), blue(blue) {}

					std::string to_hex() const {
						std::stringstream stream;

						stream << std::setfill('0');
						stream << std::hex << std::setw(2) << red << std::setw(2) << green << std::setw(2) << blue;
						return stream.str();
					}
				};

				//ez egy nem tul elegans megoldas, be kellene vezetni valamilyen unit-okat (um, mm stb, ugyanez wattra es sec-re is)
				size_t picture_grid_x =  static_cast<size_t>(cell_size[0]/pixel_size); //14; // horizontal size of one grid in pixels static_cast<size_t>(std::get<0>(get_cell_size())*2e6);
				size_t picture_grid_y = static_cast<size_t>(cell_size[1]/pixel_size); //52; // vertical size of one grid in pixels static_cast<size_t>(std::get<0>(get_cell_size())*2e6);

				xy_length_t actual_pixel_size(cell_size[0]/picture_grid_x, cell_size[1]/picture_grid_y);
				layout::sunred::adapter_t* layout_ptr = dynamic_cast<::layout::sunred::adapter_t*>(layout_structure);

				for(auto const& active_layer: layout_ptr->get_active_layers())
				{
					//ezt mar csak az eppen printelt layer-re kene...
					auto min_max = get_temperature_range(active_layer.first);
					double temperature_range = std::get<1>(min_max) - std::get<0>(min_max);

					unsigned const max_colour = 255;

					size_t font_size = std::min(picture_grid_x, picture_grid_y) * 0.6;
					size_t label_font_size = 2.5 * font_size;
					
					size_t longest_name = 0;
					size_t component_number = components.size();

					for (auto const &a_component : components) 
					{
						size_t size = a_component->id.size();
						if (size > longest_name) longest_name = size;
					}

					auto number_of_digits = [](size_t value){size_t digits = 1; while ((value /= 10) != 0) ++digits; return digits;};
					size_t largest_id_size = number_of_digits(components.size());

					size_t picture_width = ((x_pitch + 1.0) * picture_grid_x + (longest_name + largest_id_size + 3.0) * label_font_size * 0.8);
					size_t picture_height = std::max((z_pitch * y_pitch * picture_grid_y + (z_pitch - 1) * picture_grid_y), (2 * component_number) * label_font_size);

					os << "<?xml version=\"1.0\" standalone=\"no\"?>" << std::endl;
					os << "<!DOCTYPE svg PUBLIC \"-//W3C//DTD SVG 1.1//EN\" \"http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd\">" << std::endl;
					os << "<svg width=\"" << picture_width << "\" height=\"" << picture_height << "\" version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">" << std::endl;

					//Creating the thermal map layer
					os << "\t<g id=\"thermal_map\">" << std::endl;
					
					// TODO csak az aktiv layert rajzoljuk ki, erre kellene valamilyen megoldas...
					for (size_t i = x_pitch*y_pitch*active_layer.second; i < x_pitch*y_pitch*(active_layer.second+1); ++i)
					{
						double temperature = temperature_map[i];
						double value = (temperature - std::get<0>(min_max)) / temperature_range;

						size_t position_x = i % x_pitch;
						size_t position_y = i / x_pitch + (i / x_pitch / y_pitch); // The second part creates a picture_grid_y high gap between layers.

						unsigned red = ((value < 0.3) ? 0.0 : (1.0 / 0.7) * (value - 0.3)) * max_colour;
						unsigned green = (0.5 * value * value * value * value) * max_colour;
						unsigned blue = (0.5 * (1 - value * value * value)) * max_colour;
						color the_color(red, green, blue);

						os << "\t\t<rect x=\"" << (position_x * picture_grid_x) << "\" y=\"" << (position_y * picture_grid_y) << "\" width=\"" << picture_grid_x << "\" height=\"" << picture_grid_y << "\" style=\"fill:#" << the_color.to_hex() << ";stroke-width:0;\"/>" << std::endl;
					}

					os << "\t</g>" << std::endl;

					//Creating the label_shapes_layer
					os << "\t<g id=\"label_shapes_layer\">" << std::endl;

					//	Adding the bounding boxes of the components
					for (auto const &a_component : components)
					{
						auto component = dynamic_cast<::layout::sunred::component_t*>(a_component);
						if (!component)
						{
							error("print_temperature_map_in_svg(): layout component '" + a_component->id + "' has not valid type.");
						}

						for(auto &shape: component->get_shapes())
						{

							//size_t const topleft_x = static_cast<size_t>(component->get_absolute_position()[0]/static_cast<double>(std::get<0>(cell_size)));
							//size_t const topleft_y = static_cast<size_t>(component->get_absolute_position()[1]/static_cast<double>(std::get<1>(cell_size)));

							size_t const size_x = static_cast<size_t>(shape->get_side_lengths()[0]/actual_pixel_size[0]);//std::get<0>(layout->get_size());
							size_t const size_y = static_cast<size_t>(shape->get_side_lengths()[1]/actual_pixel_size[1]);//std::get<1>(layout->get_size());

							size_t const pixel_x = static_cast<size_t>(shape->get_absolute_position()[0]/actual_pixel_size[0]);//topleft_x * picture_grid_x; 
							size_t const pixel_y = static_cast<size_t>(shape->get_absolute_position()[1]/actual_pixel_size[1])+ active_layer.second * (y_pitch + 1) * picture_grid_y;//(topleft_y + layer * (y_pitch + 1)) * picture_grid_y;
							size_t const width = size_x; //* picture_grid_x;
							size_t const height = size_y;// * picture_grid_y;

							os << "\t\t<rect x=\"" << pixel_x << "\" y=\"" << pixel_y << "\" width=\"" << width << "\" height=\"" << height << "\" style=\"fill:#434343;fill-opacity:0.25;stroke:#cccccc;stroke-width:1;stroke-opacity:0.6\"/>" << std::endl;
						}
					}

					os << "\t</g>" << std::endl;

					//Creating the label_text_layer
					os << "\t<g id=\"label_text_layer\">" << std::endl;

					//	Adding the number of components on their bounding boxes
					size_t component_id = 0;

					for (auto const &a_component : components)
					{
						
						for(auto &component: a_component->get_shapes())
						{
							auto shape = dynamic_cast<layout::sunred::component_t*>(component);
							if (!shape)
							{
								error("print_temperature_map_in_svg(): layout component '" + a_component->id + "' has not valid type.");
							}

							size_t const topleft_x = static_cast<size_t>(shape->get_absolute_position()[0]/actual_pixel_size[0]);
							size_t const topleft_y = static_cast<size_t>(shape->get_absolute_position()[1]/actual_pixel_size[1]);

							size_t const size_x = static_cast<size_t>(shape->get_side_lengths()[0]/actual_pixel_size[0]);//std::get<0>(layout->get_size());
							size_t const size_y = static_cast<size_t>(shape->get_side_lengths()[1]/actual_pixel_size[1]);

							size_t const font_size_coefficient = std::min(shape->pitch_size[0], shape->pitch_size[1]);
							size_t const local_font_size = font_size * font_size_coefficient;

							size_t const pixel_x = topleft_x + 0.5 * size_x - (number_of_digits(component_id) / 4.0 * local_font_size); 
							size_t const pixel_y = topleft_y + active_layer.second * (y_pitch + 1) * picture_grid_y + 0.5 * size_y  + (local_font_size / 4.0);


							os << "\t\t<text x=\"" << pixel_x << "\" y=\"" << pixel_y << "\" style=\"font-size:" << local_font_size << ";fill:#ffffff;fill-opacity:0.7;\">" << component_id << "</text>" << std::endl;
						}
						++component_id;
					}

					//	Adding the component names as labels on the right side
					component_id = 0;
					for (auto const &a_component : components) {
						os << "\t\t<text x=\"" << ((x_pitch + 1) * picture_grid_x) << "\" y=\"" << ((2 * component_id + 1) * label_font_size) << "\" style=\"font-size:" << label_font_size << ";fill:#323232;\">" << 
							component_id << ": " << a_component->id << "</text>" << std::endl;
						++component_id;
					}

					os << "\t</g>" << std::endl;

					os << "</svg>" << std::endl;
				}
			}
			return os;
		}
		
		std::ostream& adapter_t::print_temperature_map(std::ostream& os)
		{
			if(nullptr != temperature_map)
			{
				for(size_t k = 0; k < z_pitch; ++k)
				{
					os << "---------- layer [" << k << "] ----------" << std::endl;
					for(size_t j = 0; j < y_pitch; ++j)
					{
						for(size_t i = 0; i < x_pitch; ++i)
						{
							os << temperature_map[i + j * x_pitch + k * x_pitch * y_pitch] << " ";
						}
						os << std::endl;
					}		
				}
			}
			return os;
		}
		
		
		/**
		 * ez a fuggveny kinyeri a termikus motor altal kiszamitott homerseklet eloszlasbol az egyes modulok homersekletet
		**/
		void adapter_t::refresh_component_temperature(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipator_components)
		{			
			for(auto &it: dissipator_components)
			{
				/** valtozok, amikkel atlathatobb a homersekleti mintavetel pozicioja **/
				layout::sunred::component_t* layout_component = dynamic_cast<layout::sunred::component_t*>(it.second);
				if (!layout_component)
				{
					error("refresh_component_temperature(): layout component '" + it.second->id + "' has invalid type.");
				}
				
				size_t x = layout_component->pitch_center[0];//static_cast<size_t>((component_x_position + component_center_x)/std::get<0>(cell_size));
				size_t y = layout_component->pitch_center[1];//static_cast<size_t>((component_y_position + component_center_y)/std::get<1>(cell_size));
				size_t z = dynamic_cast<layout::sunred::adapter_t*>(layout_structure)->get_layer_index(layout_component->layer_id);
				
				/** a modul homersekletenek beallitasa **/
				it.first->temperature = static_cast<unit::temperature_t>(temperature_map[x + y * x_pitch + z * x_pitch * y_pitch]);
			}
		}
		
		/**
		 * ezek a fuggvenyek kinyerik a komponens homersekletet (min, max, avg, center)
		**/
		unit::temperature_t adapter_t::get_min_temperature(layout::component_t* component)
		{			
			layout::sunred::component_t* ptr = dynamic_cast<layout::sunred::component_t*>(component);
			if(ptr == nullptr) error("get_min_temperature(): layout component '" + ptr->id + "' has invalid type");

			size_t x_start = ptr->pitch_position[0];
			size_t x_end = x_start + ptr->pitch_size[0];

			size_t y_start = ptr->pitch_position[1];
			size_t y_end = y_start + ptr->pitch_size[1];

			size_t z_index = dynamic_cast<layout::sunred::adapter_t*>(layout_structure)->get_layer_index(ptr->layer_id);
			
			double min = temperature_map[x_start + y_start * x_pitch + z_index * x_pitch * y_pitch];
			
			for(size_t x_index = x_start; x_index < x_end; ++x_index)
			{
				for(size_t y_index = y_start; y_index < y_end; ++y_index)
				{
					if(min > temperature_map[x_index + y_index * x_pitch + z_index * x_pitch * y_pitch]) min = temperature_map[x_index + y_index * x_pitch + z_index * x_pitch * y_pitch];
				}
			}
			return static_cast<unit::temperature_t>(min);
		}
		
		unit::temperature_t adapter_t::get_max_temperature(layout::component_t* component)
		{
			layout::sunred::component_t* ptr = dynamic_cast<layout::sunred::component_t*>(component);
			if(ptr == nullptr) error("get_max_temperature(): layout component '" + ptr->id + "' has invalid type");

			size_t x_start = ptr->pitch_position[0];
			size_t x_end = x_start + ptr->pitch_size[0];

			size_t y_start = ptr->pitch_position[1];
			size_t y_end = y_start + ptr->pitch_size[1];

			size_t z_index = dynamic_cast<layout::sunred::adapter_t*>(layout_structure)->get_layer_index(ptr->layer_id);
			
			double max = temperature_map[x_start + y_start * x_pitch + z_index * x_pitch * y_pitch];
			
			for(size_t x_index = x_start; x_index < x_end; ++x_index)
			{
				for(size_t y_index = y_start; y_index < y_end; ++y_index)
				{
					if(max < temperature_map[x_index + y_index * x_pitch + z_index * x_pitch * y_pitch]) max = temperature_map[x_index + y_index * x_pitch + z_index * x_pitch * y_pitch];
				}
			}
			return static_cast<unit::temperature_t>(max);
		}
		
		unit::temperature_t adapter_t::get_avg_temperature(layout::component_t* component)
		{
			layout::sunred::component_t* ptr = dynamic_cast<layout::sunred::component_t*>(component);
			if(ptr == nullptr) error("get_avg_temperature(): layout component '" + ptr->id + "' has invalid type");

			size_t x_start = ptr->pitch_position[0];
			size_t x_end = x_start + ptr->pitch_size[0];

			size_t y_start = ptr->pitch_position[1];
			size_t y_end = y_start + ptr->pitch_size[1];

			size_t z_index = dynamic_cast<layout::sunred::adapter_t*>(layout_structure)->get_layer_index(ptr->layer_id);
			
			double avg = 0.0;
			
			size_t cell_count = 0;
			for(size_t x_index = x_start; x_index < x_end; ++x_index)
			{
				for(size_t y_index = y_start; y_index < y_end; ++y_index)
				{
					++cell_count;
					avg += temperature_map[x_index + y_index * x_pitch + z_index * x_pitch * y_pitch];
				}
			}
			avg = avg / static_cast<double>(cell_count);
			return static_cast<unit::temperature_t>(avg);
		}
		
		// std::ostream& adapter_t::print_component_temperature(std::ostream& os, layout::sunred::component_t* component)
		// {
		// 	size_t x_start = component->pitch_position[0];
		// 	size_t x_end = x_start + component->pitch_size[0];

		// 	size_t y_start = component->pitch_position[1];
		// 	size_t y_end = y_start + component->pitch_size[1];

		// 	size_t z_index = dynamic_cast<::layout::sunred::adapter_t*>(layout_structure)->get_layer_index(component->layer_id);
			
			
		// 	for(size_t x_index = x_start; x_index < x_end; ++x_index)
		// 	{
		// 		for(size_t y_index = y_start; y_index <  y_end; ++y_index)
		// 		{
		// 			os.write(reinterpret_cast<const char*>(temperature_map+x_index + y_index * x_pitch + z_index * x_pitch * y_pitch), sizeof(double));
		// 		}
		// 	}
		// }
		
		std::ostream& adapter_t::get_layer_temperature(std::ostream& os, const std::string& layer_id)
		{
			size_t z_index = dynamic_cast<layout::sunred::adapter_t*>(layout_structure)->get_layer_index(layer_id);
			
			
			for(size_t x_index = 0; x_index < x_pitch; ++x_index)
			{
				for(size_t y_index = 0; y_index <  y_pitch; ++y_index)
				{
					float temperature = temperature_map[x_index + y_index * x_pitch + z_index * x_pitch * y_pitch];
					os.write(reinterpret_cast<const char*>(&temperature), sizeof(float));
				}
			}
			return os;
		}

		std::tuple<double, double> adapter_t::get_temperature_range(const std::string& layer_id) const
		{
			if(nullptr == temperature_map)
			{
				error("get_temperature_range(): temperature map has not been initialized.");
			}

			size_t layer_index = dynamic_cast<::layout::sunred::adapter_t*>(layout_structure)->get_layer_index(layer_id);
			
			auto minmax_iterators = std::minmax_element(temperature_map + x_pitch*y_pitch*layer_index, temperature_map + x_pitch*y_pitch*(layer_index+1));
			return std::make_tuple(*minmax_iterators.first, *minmax_iterators.second);
		}
		
		std::ostream& adapter_t::print_dissipation_map(std::ostream& os)
		{
			if(nullptr != dissipation_map)
			{
				for(size_t k = 0; k < z_pitch; ++k)
				{
					os << "---------- layer [" << k << "] ----------" << std::endl;
					for(size_t j = 0; j < y_pitch; ++j)
					{
						for(size_t i = 0; i < x_pitch; ++i)
						{
							os << dissipation_map[i + j * x_pitch + k * x_pitch * y_pitch] << " ";
						}
						os << std::endl;
					}		
				}
			}
			return os;
		}
		
		/**
		 * ez a fuggveny egy szimulacios lepesnel a logitherm_proxy adataibol kiszamitja, hogy milyen pozicioban mennyit disszipalt a chip
		 * ezt a fuggvenyt a thermal_base refresh_temperatures fv-e hivja meg
		**/ 
		void adapter_t::refresh_dissipation_map(std::unordered_map<logic::component_t*, layout::component_t*, std::hash<logic::component_t*>, logic::components_equal>& dissipator_components)
		{
			/** kinullazzuk a distribution tombot **/
			std::fill(dissipation_map, dissipation_map + size, 0.0);
			
			/** vegigiteralunk a dissipatorok tombjen **/
			for(auto &it: dissipator_components)
			{
				/** fogyasztas meghatarozasa **/
				double dissipation = static_cast<double>(it.first->get_dissipation());
				// unit::power_t miafasz = it.first->static_dissipation() + it.first->dynamic_dissipation();
				
				// if(std::isnan(dissipation))
				// {
				// 	std::cout << it.first->id << " static=" << it.first->static_dissipation() << " dynamic=" << it.first->dynamic_dissipation() << " miafasz=" << miafasz;
				// 	std::cout << " sum=" << static_cast<double>(it.first->static_dissipation() + it.first->dynamic_dissipation()) << " total=" << dissipation << std::endl;
				// }
												
				/** terulet meghatarozasa **/
				double total_area = static_cast<double>(it.second->calculate_area());

				double power_density = dissipation/total_area;

				// if(std::isnan(power_density))
				// {
				// 	std::cout << "power density=" << power_density << std::endl;
				// 	std::cout << "dissipation=" << dissipation << std::endl;
				// 	std::cout << "total_area=" << total_area << std::endl;
				// }

				for(auto &shape: it.second->get_shapes())
				{
					auto* layout_component = dynamic_cast<layout::sunred::component_t*>(shape);
					if (!layout_component)
					{
						error("refresh_dissipation_map(): layout component '" + it.second->id + "' has not valid type.");
					}
				
					unit::length_t x_pos = layout_component->get_absolute_position()[0];
					unit::length_t y_pos = layout_component->get_absolute_position()[1];	

					/** valtozok, amikkel egyszerubbe valnak az egymasba agyazott ciklusok **/
					size_t x_start = layout_component->pitch_position[0];
					size_t x_end = x_start + layout_component->pitch_size[0];

					size_t y_start = layout_component->pitch_position[1];
					size_t y_end = y_start + layout_component->pitch_size[1];

					layout::sunred::adapter_t* layout_ptr = dynamic_cast<::layout::sunred::adapter_t*>(layout_structure);
					if(layout_ptr == nullptr ) error("refresh_dissipation_map(): layout adapter  has not valid type.");

					size_t z_index = layout_ptr->get_layer_index(layout_component->layer_id);
					
					for(size_t y_index = y_start; y_index < y_end; ++y_index)
					{
						for(size_t x_index = x_start; x_index < x_end; ++x_index)
						{
//							::unit::length_t x_size(cell_size[0]);
//							::unit::length_t y_size(cell_size[1]);
//
//							::unit::length_t cell_x_pos(cell_size[0]*static_cast<double>(x_index));
//							::unit::length_t cell_y_pos(cell_size[1]*static_cast<double>(y_index));
//
//							/** ki kell szamitani, hogy a component teruletenek mekkora resze esik az adott cellaba **/
//							if(cell_x_pos < x_pos) x_size = cell_size[0] - (x_pos - cell_x_pos); //bal szel
//							else if (x_pos + layout_component->get_side_lengths()[0] < cell_x_pos + cell_size[0]) x_size = x_pos + layout_component->get_side_lengths()[0] - cell_x_pos; //jobb szel
//							else x_size = cell_size[0];
//							
//							if(cell_y_pos < y_pos) y_size = cell_size[1] - (y_pos - cell_y_pos); //felso szel
//							else if (y_pos + layout_component->get_side_lengths()[1] < cell_y_pos + cell_size[1]) y_size = y_pos + layout_component->get_side_lengths()[1] - cell_y_pos; //also szel
//							else y_size = cell_size[1];

							/** disszipacio novelese  dissipation/area ertekkel (egyenletes disszipaciosuruseg a teruleten) **/
							size_t x = x_index-x_start;
							size_t y = y_index-y_start;

							unit::area_t area = layout_component->area_array[x + y * layout_component->pitch_size[0]];
							dissipation_map[x_index + y_index * x_pitch + z_index * x_pitch * y_pitch] += static_cast<double>(area*power_density);
						}
					}
				}			
				
			}
		}

		void adapter_t::read_files(const std::string& path, const std::string& initfile)
		{
			file_io::sunred::adapter_t parser(this, log, path, initfile);
			parser.read_files();
		}
	} //namespace sunred
	
} //namespace thermal
