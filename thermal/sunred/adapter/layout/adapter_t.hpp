#ifndef _LAYOUT_SUNRED_ADAPTER_
#define _LAYOUT_SUNRED_ADAPTER_

#include <unordered_map>
#include <string>
#include "thermal/base/adapter_t.hpp"
#include "thermal/sunred/adapter/layout/component_t.hpp"
#include "thermal/sunred/adapter/layout/layers_information_t.hpp"
#include "unit/all_units.hpp"


namespace layout
{
	namespace sunred
	{		
		/**
		 * az osszes layout_component-et tarolo osztaly
		 * a dissipator-ok es a megjelenitendo elemek az itt
		 * tarolt elemek egy reszhalmaza
		**/ 
		class adapter_t	:	public	layout::adapter_t
		{
			private:
				friend class layout::sunred::component_t;
				/**
				 * a layout x es y iranyu merete
				**/ 
				const xy_length_t layout_size;

				/**
				 * a layout x es y iranyu merete
				**/ 
				const xy_pitch_t resolution;

				const xy_length_t cell_size;
				
				/**
				 * a layereket tarolo valtozo
				 * ez felelos az azonositok egyediseget ellenorizni
				**/ 
				layers_information_t layers;
				
				
			private:
				/**
				 * ellenorzi, hogy a component elfer-e a layout-on
				 * true-val ter vissza, ha elfer
				**/
				bool component_fits(const component_t* component);
				
			public:	
				/**
				 * default konstruktor
				 * nincs default konstruktor
				**/ 
				adapter_t() = delete;
				
				/**
				 * meret alapjan inicializalodik a layout, nem lehet ezutan modositani a meretet
				**/ 
				adapter_t(thermal::adapter_t* adapter, util::log_t* log, const xy_length_t&, const xy_pitch_t&);
				
				adapter_t(const adapter_t& other, thermal::adapter_t* adapter, util::log_t* log);

				~adapter_t();

				/**
				 * egy layout komponenst ad hozza a layout_components_tree-hez
				 * hibaellenorzes: a layout component letezo layer-en van
				 * hibaellenorzes: a layout_component kilog a layout-rol
				**/ 
				//void check_component(::layout::component_t*) override; //ellenorzes, hogy van-e olyan layer, amilyen kell
				
				/**
				 * hozzaad egy layert a layout-hoz
				 * ellenorzes: layer azonositoja (neve) egyedi
				**/ 
				void add_layer(const std::string&, const unit::length_t&);
				
				bool find_layer(const std::string&);
				
				size_t get_layer_index(const std::string&);

				unit::length_t get_layer_thickness(const std::string&);
				unit::length_t get_layer_thickness(size_t);
				
				
				void set_layer_active(const std::string&);
				const std::unordered_map<std::string, size_t>& get_active_layers();

				/**
				 * visszater a layout x, y, es z iranyu meretevel
				**/ 
				xyz_pitch_t get_pitch() override;

				xyz_length_t get_length() override;
		};
	}
} // namespace layout
#endif //_LAYOUT_ADAPTER_
