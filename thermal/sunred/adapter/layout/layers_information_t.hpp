#ifndef _LAYOUT_SUNRED_LAYERS_INFORMATION_
#define _LAYOUT_SUNRED_LAYERS_INFORMATION_


#include <string>
#include <unordered_map>
#include "unit/all_units.hpp"
#include "util/log_t.hpp"

namespace layout
{
	namespace sunred
	{
		class layers_information_t //layout::sunred::adapter_t miert nem oroklodik ebbol???
		{
			private:
				/** dependency injection **/
				layout::adapter_t* const adapter;
				util::log_t* const log;
				
				/** layer id, index**/
				std::unordered_map<std::string, size_t> layers;
				
				/** layer thickness in the index order **/
				std::vector<unit::length_t> layer_thickness;

				/** layer id, index**/
				std::unordered_map<std::string, size_t> active_layers;
				
				unit::length_t total_thickness;
			
			public:
				
				layers_information_t() = delete;

				layers_information_t(layout::adapter_t* adapter, util::log_t* log)
				:
					adapter(adapter),
					log(log),
					total_thickness(0_m)
				{}
				
				/**
				 * ez a fuggveny hozzaad egy layert a hierarchiahoz
				 * ellenorzes: a kapott azonosito egyedi, meg nincs eltarolva
				**/ 
				void add_layer(const std::string& an_id, const unit::length_t& thickness)
				{
					if(layers.find(an_id) != layers.end())
					{
						log->warning("layout::sunred::add_layer(): layer '" + an_id + "' is already in the layout layer's hierarchy");
					}
					else
					{
						layers.insert(std::make_pair(an_id,layers.size())); //ez ugye nem undefined behavior???
						layer_thickness.push_back(thickness);
						total_thickness += thickness;
					}
				}
				
				bool find_layer(const std::string& an_id)
				{
					if(layers.find(an_id) == layers.end()) return false;
					else return true;
				}
				
				size_t get_layer_index(const std::string& an_id)
				{
					if(layers.find(an_id) == layers.end())
					{
						log->error("layout::sunred::get_layer_index(): layer '" + an_id + "' is not found");
					}
					return layers.find(an_id)->second;
				}

				void set_layer_active(const std::string& an_id)
				{
					if(layers.find(an_id) == layers.end())
					{
						log->warning("layout::sunred::get_layer_thickness(): layer '" + an_id + "' is not found");
					}
					else
					{
						active_layers.insert(std::make_pair(an_id, layers.find(an_id)->second));
					}
				}

				const std::unordered_map<std::string, size_t>& get_active_layers()
				{
					return active_layers;
				}
				
				unit::length_t get_layer_thickness(const std::string& an_id)
				{
					if(layers.find(an_id) == layers.end())
					{
						log->error("layout::sunred::get_layer_thickness(): layer '" + an_id + "' is not found");
					}
					return layer_thickness.at(layers.find(an_id)->second);
				}
				
				unit::length_t get_layer_thickness(size_t an_index)
				{
					if(layer_thickness.size() < an_index + 1)
					{
						log->error("layout::sunred::get_layer_thickness(): invalid layer index.");
					}
					return layer_thickness.at(an_index);
				}
				
				size_t get_size()
				{
					return layers.size();
				}

				unit::length_t get_thickness()
				{
					return total_thickness;
				}
		};
	} //namespace sunred
} //namespace layout
#endif //_LAYOUT_SUNRED_LAYERS_INFORMATION_
