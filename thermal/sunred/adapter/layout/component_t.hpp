#ifndef _LAYOUT_SUNRED_COMPONENT_
#define _LAYOUT_SUNRED_COMPONENT_

#include "thermal/base/layout/component_t.hpp"
#include "thermal/base/layout/shape_t.hpp"
#include "unit/all_units.hpp"

namespace layout
{
	class layer_temperature_trace_t;

	namespace sunred
	{
		/** forward decl **/
		class adapter_t;
		
		class component_t	:	public layout::component_t//,
								//public ::layout::sunred::component_information_t
		{
			public:
				/**
				 * a layout_layer azonositoja, amin a component megtalalhato
				**/
				const std::string layer_id;

			public:
				xy_pitch_t pitch_position;
				xy_pitch_t pitch_size;
				xy_pitch_t pitch_center;
				
				unit::area_t* area_array;
				
			public:
				component_t() = delete;
				
				/**
				 * konstruktor, ami a nev, relativ pozicio, meret es a layer szama alapjan hoz letre egy layout component-et
				**/ 
				component_t(const std::string& id, const std::string& layer_id, xy_length_t position, xy_length_t sides, component_t* parent, layout::sunred::adapter_t* adapter = nullptr);

				virtual ~component_t();
				
				layer_temperature_trace_t* add_layer_temperature_trace(const std::string& path, const std::string& postfix = "") override;

				/**
				 * visszaadja az adott komponens abszolut poziciojat a layout-on (xy pozicio)
				**/	
				//xy_length_t get_absolute_position() const;

				/**
				 * vizszintes tengelyre tukrozi a layout_component-et
				 * ez vonatkozik az osszes benne levo layout_component-re is
				**/
				//void reflect_horizontal();

				/**
				 * fuggoleges tengelyre tukrozi a layout_component-et
				 * ez vonatkozik az osszes benne levo layout_component-re is
				**/
				//void reflect_vertical();
			
				/**
				 * kiszamitja a meretet a layout_component-nek
				 * ehhez vegigmegy az osszes child-on, es azoknak is kiszamitja
				 * ha children ures, akkor a size-zal ter vissza
				 * warning message: ha egy child kilog a layout-bol
				**/ 
				//::unit::area_t calculate_area();
				
				/**
				 * homerseklet adatok trace-elesehez
				**/ 
			
				/**
				 * kapott ostream-re kirakja a component es leszarmazottak nevet
				**/ 
				std::ostream& print_component(std::ostream& os, std::string indent = "") override;
			
		};
		
	} //namespace sunred
} //namespace layout

#endif //_LAYOUT_SUNRED_COMPONENT_
