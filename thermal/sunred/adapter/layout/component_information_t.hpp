#ifndef _LAYOUT_SUNRED_COMPONENT_INFORMATION_
#define _LAYOUT_SUNRED_COMPONENT_INFORMATION_

#include <tuple>
#include <string>
#include "thermal/sunred/adapter/layout/layers_information_t.hpp"
#include "thermal/base/layout/coordinates_t.hpp"

namespace layout
{
	namespace sunred
	{

		using xy_length_t	= ::layout::coordinates_t<::unit::length_t, 2>;
		using xy_pitch_t	= ::layout::coordinates_t<size_t, 2>;
		using xyz_length_t	= ::layout::coordinates_t<::unit::length_t, 3>;
		using xyz_pitch_t	= ::layout::coordinates_t<size_t, 3>;
	
		/**
		 * egy layout elem leirasahoz szukseges informaciokat tarolo osztaly
		 **/ 
		class component_information_t
		{
			public:
			
				/**
				 * a layout_layer azonositoja, amin a component megtalalhato
				**/
				const std::string layer_id;
				

				
			private:
				/**
				 * relativ pozicioja a layout elemnek
				 * pozicio == bal felso sarok
				**/ 
				xy_length_t position;
			
			public:
				/**
				 * a layout elem merete
				**/
				const xy_length_t size;

				xy_pitch_t pitch_position;
				xy_pitch_t pitch_size;
				xy_pitch_t pitch_center;
					
			public:
			
				component_information_t
				(
					const std::string& layer_id,
					const xy_length_t& position,
					const xy_length_t& size
				):
					layer_id(layer_id),
					position(position),
					size(size)

				{}
				
				/**
				 * visszaadja a layout komponens relativ poziciojat
				**/
				const xy_length_t& get_relative_position() const
				{
					return position;
				}
				
				/**
				 * layout komponens relativ poziciojat atirja az uj ertekre
				**/
				void set_relative_position(const xy_length_t& new_position) 
				{
					position = new_position;
				}
				
		};
	} //namespace sunred
	
} // namespace layout


#endif // _LAYOUT_SUNRED_COMPONENT_INFORMATION_
