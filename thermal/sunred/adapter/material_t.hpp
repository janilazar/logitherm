#ifndef _SUNRED_MATERIAL_
#define _SUNRED_MATERIAL_


#include <string>

namespace thermal
{
	namespace sunred
	{
		class material_t
		{
			public:
			
				/**
				 * nev, ami egyertelmuen azonositja az anyagot
				**/ 
				const std::string id;
				
				/**
				 * anyag sorszama, sunred-nek kell, a thermal_adapter hatarozza meg
				**/ 
				const size_t material_number;
				
				/**
				 * hovezetes
				**/ 
				const float thermal_conductivity;
				
				/**
				 * hokapacitas
				**/ 
				const float thermal_capacity;
				
			public:	
				material_t() = delete;
				material_t(const std::string& nm, size_t mat_num, float thermal_conductivity, float thermal_capacity)
				:
					id(nm),
					material_number(mat_num),
					thermal_conductivity(thermal_conductivity),
					thermal_capacity(thermal_capacity)
				{}
		};
	} //namespace sunred
} //namespace thermal

#endif //_SUNRED_MATERIAL_
