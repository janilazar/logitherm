# Előkészületek

A **LogiTherm** keretrendszer több logikai és termikus motort is tartalmaz, amelyek további
program könyvtárakra támaszkodnak. Ez a *README* fájl rövid áttekintést nyújt fordításhoz
és telepítéshez szükséges teendőkről. Tesztelés 32 és 64 bites Xubuntu 16.04-en és 64 bites 14.04.5-ön, 4.9.4, 5.4.0 és 6.2.0 verziójú gcc-vel g++-szal történt (g++-6.2.0 használata javasolt).

A keretrendszer lefordításához szükséges telepíteni az alábbi függőségeket:

	$ sudo apt install automake autoconf pkg-config flex bison lzip cmake gperf zlib1g-dev mercurial pv
	
A **SystemC C++** osztálykönyvtár fordításához szükség van az *autoconf*, *automake* és *pkg-config* csomagokra.
	
A **Sniper** processzor szimulátor és a **Verilog** front-end használatához a toplevel Makefile-ban
meg kell adni a Sniper és a VPI library-k elérési útvonalát. A projektet újra kell fordítani, hogy
ez a két modul is benne legyen a binárisban.

A **3D-ICE** termikus szimulátor SuperLU és BLAS könyvtárakat használ a számításokhoz, amik
megtalálhatóak a 3dice könyvtárán belül. Az IC-k struktúrájának leírása szöveges
formátumú fájlokkal történik, ezek parse-olásához szükséges a *flex* és *bison* csomagokat
a lenti paranccsal lehet telepíteni.

A **SUNRED** termikus szimulátor használja a gmp-6.1.2 és ntl-10.3.0 library-ket, amik automatikusan
letöltődnek. Mivel a gmp *lzip*-pel tömörített állományban letölthető, ezt is fel kell telepíteni.

A **SloTh** termikus motor a *PARALUTION* lineáris algebra könytárat használha, aminek a lefordításához szükség van a *cmake* build rendszerre.

A **Liberty** standard cella adatbázist beolvasó parserhez fel kell telepíteni a *gperf* csomagot.

A **LEF/DEF** parser lefordításához szükséges a *zlib.h* header, ami a *zlib1g-dev* csomagban van.

Az **Eigen** könyvtár letöltése a *mercurial* verziókövető rendszerrel történik.

A "build rendszer" kitömörítéskor *pv*-vel illusztrálja a folyamat állapotát.

#	Fordítás és telepítés
	
A keretrendszer fordítását egy több makefile-ból álló rendszer végzi. A
gyökérkönyvtárban kiadott fordítási parancs hatására meghívodnak az egyes
alkönyvtárakban található makefile-ok amik kiadják a megfelelő fordítási
parancsokat. Ennek az egyszerű rendszernek a hátránya a különböző könyvtárakban
található fájlfüggőségek kezelésének hiánya (egy header módosítása miatt szükséges
lehet a teljes projekt újrafordítása).
A **SNIPER_ROOT** változót beállítva a **Sniper** processzorszimulátor gyökérkönyvtárára a keretrendszerbe belefordul az 
ehhez szükséges interfész osztályok is.
Verilog frontendet a **VPI_INCDIR** és **VPI_LIBDIR** változók beállításával lehet használni.
A teljes keretrendszer lefordítása egy egyszerű parancs kiadásával tehető meg.
	
    $ make

Ennek hatására a először a dependency target fut le, amely letölti a **SloTh**-hoz 
felhasznált Paralution könyvtárat, valamint a **SUNRED**-hez használt gmp-t, és ntl-t. Ezt követően a 
config target konfigurálja a felhasznált eszközöket. Végül a logitherm target sorban lefordítja a logikai szimulátorokat, a
**LogiTherm** osztályait, és a termikus szimulátorokat. Forrásfájl módosítása után elég csak a logitherm target-et újrafuttatni.

A telepítés útvonalát a gyökérkönyvtárban található Makefile-ban az **INSTDIR** változó
módosításával változtathatjuk meg. Figyeljünk, hogy olyan könyvtárat adjunk meg, ahol
van írási/módosítási joga a felhasználónknak.

    $ sudo make install

A dokumentáció fordítása is a gyökérkönyvtárban található Makefile segítségével
történik. Mivel a rendszeren nem feltétlen áll rendelkezésre a LaTeX csomag, így
ezt a targetet igény szerint külön kell lefuttatni.

    $ make doc
